#!/usr/bin/perl -w

## regmonitor.pl
##
## Used to monitor state of registers in the store - kdg
##
## 2011-08-04 - v1.0.0 - created - kdg
## 2011-12-14 - v1.0.1 - Changed some warnings to system info - kdg
## 2012-02-13 - v1.0.2 - More revisions for collect system info - kdg
## 2012-03-03 - v1.0.3 - Adding check_pcAnywhere - kdg
## 2012-04-17 - v1.0.4 - Added more logging - kdg
## 2012-04-30 - v1.0.5 - Added fix_file to check_pcAnywhere - kdg
## 2012-05-29 - v1.0.6 - Added checking registry - kdg
## 2012-06-01 - v1.0.7 - Expanded checking software - kdg
## 2012-06-11 - v1.0.8 - Finding the OS - kdg
## 2012-06-18 - v1.1.0 - Added check_for_update and check_scheduled_tasks - kdg
## 2012-06-19 - v1.1.1 - Added save_log - kdg
## 2012-06-28 - v1.1.2 - Added save_record - kdg
## 2012-06-29 - v1.1.3 - Revised save_log to account for multiple registers - kdg
## 2012-07-02 - v1.1.4 - Added checking antivirus - kdg
## 2012-07-09 - v1.1.5 - Set absolute path for reg_fix - kdg
## 2012-07-11 - v1.1.6 - Revised path to fix_file for setpcAnywhere.bat - kdg
## 2012-07-25 - v1.1.7 - Revised check_antivirus to reduce unneccessary warnings. Added registry only - kdg
## 2012-07-25 - v1.2.0 - Added checking for StartScan for Villages - kdg
## 2012-07-30 - v1.2.1 - Added reset_record - kdg
## 2012-08-22 - v1.2.2 - Changed warnings in save log to NOTICE.  Separated reading of most recent antivirus log - kdg
## 2012-09-27 - v1.2.3 - Added checking groups that Villages is in - kdg
## 2012-10-02 - v1.2.4 - Added check_eventlog - kdg
## 2012-10-12 - v1.2.5 - Changed WARNING about no scan to NOTICE - kdg
## 2012-10-15 - v1.2.6 - Disabled checking scripts - kdg
## 2012-10-18 - v1.2.7 - Set antivirus warning at 15 days - kdg
## 2012-10-31 - v1.2.8 - Correcting some labels and warns increments - kdg
## 2012-11-07 - v1.2.9 - Added collecting amount of memory - kdg
## 2012-11-09 - v1.3.0 - Added check_memory - kdg
## 2012-11-20 - v1.3.1 - Added checking for proxyserver - kdg
## 2012-11-27 - v1.3.2 - Modified check_quickscan - kdg
## 2012-12-03 - v1.3.3 - Some more work with check_registry_daily and checking for Mozilla - kdg
## 2012-12-04 - v1.3.4 - Check virus def update time - kdg
## 2012-12-06 - v1.3.5 - Refined checking for FireFox in registry - kdg
## 2012-12-10 - v1.3.6 - Added set_virus_update - kdg
## 2012-12-17 - v1.4.0 - Added check_reboots & check_minidump - kdg
## 2012-12-18 - v1.4.1 - Added TotalPhyMemory - kdg
## 2013-01-08 - v1.4.2 - Added dumpcount -kdg
## 2013-01-09 - v1.4.3 - Added check_disk.  Specifically checking the size of posw - kdg
## 2013-01-10 - v1.4.4 - Added gather_sysinfo - kdg
## 2013-01-13 - v1.4.5 - Change WARNING on size of posw to NOTICE - kdg
## 2013-01-15 - v1.4.6 - Remove increment of warns when checking posw size - kdg
## 2013-01-16 - v1.5.0 - Added check_running_services and check_tasks - kdg
## 2013-01-22 - v1.5.1 - Some additions to services & tasks.  Added run_defrag - kdg
## 2013-01-29 - v1.5.2 - Collecting info about windows uninstall directories - kdg
## 2013-01-31 - v1.5.3 - Added check for bluescreenview & temporarily disabled defrag.  Added need_to_defrag variable - kdg
## 2013-02-01 - v1.5.4 - Revisions to usage function - kdg
## 2013-02-05 - v1.5.5 - Created enable_delete_uninstall_folder variable and code to delete old Windows Update folders - kdg
## 2013-02-12 - v1.5.6 - Progressively enabling delete uninstall feature - kdg
## 2013-02-18 - v1.5.7 - Added ISSUE for privoxy not being set in browser.  Enabling more uninstall folders to be deleted.  Updated task & service lists - kdg
## 2013-02-19 - v1.5.8 - Fully enabled defrag and delete of old Uninstall folders - kdg
## 2013-02-25 - v1.6.0 - Added uninstall_app.  Revised correcting quarantine settings.  - kdg
## 2013-02-26 - v1.6.1 - Added stop_service - kdg
## 2013-02-27 - v1.6.2 - Added --no option - kdg
## 2013-02-28 - v1.7.0 - Added check_software_daily - kdg
## 2013-03-01 - v1.7.1 - Updates to checking tasks and services and registry - kdg
## 2013-03-05 - v1.7.2 - Adjusting QuarantinePurgeAgeLimit and BackupPurgeAgeLimit - kdg
## 2013-03-07 - v1.7.3 - Added  ssmarque.scr and removed some debug statements - kdg
## 2013-03-14 - v1.7.4 - Added NOTICE to disk defragment task so I know when it has run - kdg
## 2013-03-20 - v1.7.5 - Work with checking services and software installed - kdg
## 2013-03-22 - v1.7.6 - Updated checking antivirus to send warning based on event level - kdg
## 2013-04-01 - v1.7.7 - Reformatting ISSUE: for antivirus issues - kdg
## 2013-04-02 - v1.7.8 - Added pca option.  Revised check_pcAnywhere to remove name restrictions.  Added SAV tag to all antivirus warnings - kdg
## 2013-04-03 - v1.7.9 - Check that some VNC is installed - kdg
## 2013-04-04 - v1.7.10 - Updated valid_task_hash - kdg
## 2013-04-08 - v1.7.11 - Checking antivirus will now change the updates to daily if they are not keeping current - kdg
## 2013-04-08 - v1.8.0 - Added install_tightVNC - kdg
## 2013-04-16 - v1.8.1 - Added lxee tasks and services as valid - kdg
## 2013-04-22 - v1.8.2 - Added Suggested Sites to cleanup function - kdg
## 2013-04-23 - v1.8.3 - Added check_perl_updates - kdg
## 2013-04-29 - v1.8.4 - Relaxed the virus definitions date a week - kdg
## 2013-04-30 - v1.8.5 - Added call to vpdn_lu.exe - kdg
## 2013-05-06 - v1.8.6 - Added schedule_update function - kdg
## 2013-05-07 - v1.8.7 - Moved UltraVNC to the unwanted list.  Improved checking of event log - kdg
## 2013-05-09 - v1.8.8 - Added Warning to event log printout so that it gets reported back - kdg
## 2013-05-15 - v1.8.9 - Added exclude_phrases - kdg
## 2013-05-16 - v1.9.0 - Added source to check_eventlog.  Added check_poswin - kdg
## 2013-05-17 - v1.9.1 - Refining details in reporting events in the event log.  Added  fix_srtpv - kdg
## 2013-05-20 - v1.9.2 - Added evaluate_phrases_hash - kdg
## 2013-05-22 - v1.9.3 - Updated check_poswin to create a new profilereport.log - kdg
## 2013-05-23 - v1.9.4 - Added notice_hash to check_events.  Added expected_service_list - kdg
## 2013-05-24 - v1.9.5 - Added provision to stop ultra VNC if both TightVNC and Ultra VNC are running - kdg
## 2013-05-28 - v1.9.6 - Some corrections to checking expected software - kdg
## 2013-05-30 - v1.9.7 - Some updates to the recognized phrases in the event log - kdg
## 2013-06-04 - v1.9.8 - Removed phrase about pcAnywhere login failed from check_eventlog - Preparation to scan for viruses - kdg
## 2013-06-05 - v1.9.9 - Added cleanup() to the main. - kdg
## 2013-06-06 - v1.9.10 - Added Automatic Updates to expected_service_list - kdg 
## 2013-06-21 - v1.9.11 - Added ss3dfo.scr to valid_task_hash - kdg
## 2013-06-24 - v1.9.12 - Revised check_eventlog to not complain when Symantec Decomposter Engine failed  - kdg
## 2013-06-27 - v1.10.0 - Added run_cleanup & check_windows_updates - kdg
## 2013-06-28 - v1.10.1 - Excluded a few more phrases in eventlogs - kdg
## 2013-07-02 - v1.10.2 - Added ose.exe and OfficeSourceEngine to valid task & service - kdg
## 2013-07-03 - v1.10.3 - Removed noticed that bluescreenview is not installed - kdg
## 2013-07-31 - v1.10.4 - Disabled schedule updates - kdg
## 2013-08-05 - v1.10.5 - Send a notice about old SAV defs - we know we need to update this - kdg
## 2013-08-05 - v1.10.6 - Added kill_task_list - kdg
## 2013-08-06 - v1.10.7 - Added checking for unwanted local applications - kdg
## 2013-08-12 - v1.10.8 - Added collecting serial number - kdg
## 2013-08-14 - v1.10.9 - Added $install_enabled for SEP11 - kdg
## 2013-08-15 - v1.10.10 - Added check_misc().  Stop pcAnywhere before installing SEP 11 - kdg
## 2013-08-15 - v1.10.11 - Checking pcAnywhere for autostart issue - kdg
## 2013-08-18 - v1.10.12 - Numerious updates in light of SEP 11 being installed - kdg
## 2013-08-19 - v1.11.0 - Added check_firewall.  Added schedule_reboot - kdg
## 2013-08-19 - v2.0.0 - Modified precheck to save a temporary version of this script and check the syntax before using it - kdg
## 2013-08-20 - v2.0.1 - Added exclusion for pcAnywhere - kdg
## 2013-08-23 - v2.0.2 - More work on checking Symantec DLLs and correcting them - kdg
## 2013-08-28 - v2.0.3 - Added check_printers - kdg
## 2013-08-29 - v2.0.4 - Added check_permissions - kdg
## 2013-09-04 - v2.0.5 - Enable SEP_INSTALL for any system any day - kdg
## 2013-09-10 - v2.0.6 - Revised check_windows_updates to check for date inside windows update log - kdg
## 2013-09-11 - v2.0.7 - Recording the version of FireFox found - kdg
## 2013-09-13 - v2.0.8 - Updated valid tasks - kdg
## 2013-09-17 - v2.0.9 - Updated known_phrases - kdg
## 2013-09-23 - v2.0.10 - Updated valid_task_hash - kdg
## 2013-09-23 - v2.1.0 - Removed Library.pm module - kdg
## 2013-09-26 - v2.1.1 - Updated known_phrases - kdg
## 2013-09-27 - v2.1.2 - Added get_villages_user and check for foxit and .pdf default program - kdg
## 2013-09-30 - v2.1.3 - Added RasMan to list of services to disable.  Allowed for disabling services set to manual - kdg
## 2013-10-01 - v2.1.4 - Revisions to checking that Foxit is the default .pdf reader - kdg
## 2013-10-03 - v2.1.5 - Clear the Memory Dump settings for cCleaner - kdg
## 2013-10-08 - v2.1.6 - Added alternative location for Foxit Reader - kdg
## 2013-10-10 - v2.1.7 - Added a second alternative location for Foxit Reader - kdg
## 2013-10-10 - v2.1.8 - Checking Store Manager setting in registry - kdg
## 2013-10-11 - v2.1.9 - Save a local copy of the monitor log file - kdg
## 2013-10-14 - v2.1.10 - Re-worked checking for cCleaner settings - kdg
## 2013-10-16 - v2.1.11 - Checking LineDisplay Settings -kdg
## 2013-10-18 - v2.2.0 - Added check_log - kdg
## 2013-10-30 - v2.2.1 - Checking time of virus definitions update.  Added cCleaner to run_cleanup - kdg
## 2013-11-05 - v2.2.2 - More work checking antivirus scans and updates.  Still trying to get updates to occur when we want them. - kdg
## 2013-11-06 - v2.2.3 - Corrected a problem with saving local log - kdg
## 2013-11-08 - v2.2.4 - Small adjustments to checking cCleaner settings - kdg
## 2013-11-11 - v2.2.5 - More small adjustments to checking cCleaner settings - kdg
## 2013-11-12 - v2.3.0 - Added check_script_updates but enabled only for stores > 7000 - kdg
## 2013-11-14 - v2.3.1 - Added update_action_log.  Updated checking eventlog for Antivirus Scans - kdg
## 2013-11-15 - v2.3.2 - Updated check_reboots to show last boot time - kdg
## 2013-11-18 - v2.3.3 - Create memory dump setting if it is not found for cCleaner - kdg
## 2013-11-19 - v2.3.4 - Report total physical memory when there is a memory dump - kdg
## 2013-11-22 - v2.3.5 - Added cleaner option - kdg
## 2013-11-26 - v2.3.6 - Updated exclude_phrases in check_eventlog - kdg
## 2013-12-02 - v2.3.7 - Cleaned up some logging for entering check_disk - kdg
## 2013-12-03 - v2.3.8 - Added some logging for updating scripts - kdg
## 2013-12-04 - v2.4.0 - Added install_chrome.  enabled only for stores > 7000 at present, Reg1, Company Stores - kdg
## 2013-12-06 - v2.4.1 - Provision for Nashville Reg2 checking cCleaner for Administrator.  Tweak to checking FireFox version - kdg
## 2013-12-10 - v2.4.2 - Added reset_profilereport variable - kdg
## 2013-12-11 - v2.4.3 - Added tvn_count.  Disabling automatic updates.  Added typeperf to check selected processes for high CPU usage  - kdg
## 2013-12-13 - v2.4.4 - Added temp_dump_dir in check_minidump - kdg
## 2013-12-13 - v2.4.5 - Added check for Word, Excel, and PowerPoint viewers (Do we want to check this for Contract Stores as well?) - kdg
## 2013-12-17 - v2.4.6 - Enabled Chrome for all company stores - kdg
## 2013-12-18 - v2.4.7 - Added check_viewer - kdg
## 2013-12-18 - v2.4.8 - Updated check_windows_updates to check if AutomaticUpdates are enabled - kdg
## 2013-12-20 - v2.4.9 - Added check for pcAnywhere host name - kdg
## 2013-12-26 - v2.4.10 - Enabled installing viewers in 2014.  Updated checking antivirus scan events - kdg
## 2013-12-27 - v2.4.11 - Added check_antivirus_autoupdates - kdg
## 2013-12-30 - v2.4.12 - Correction to checking pcAnywhere name - kdg
## 2014-01-02 - v2.4.13 - Stale Proactive Threat definitions now schedule an update.  Force running of check_poswin if need to reset log - kdg
## 2014-01-09 - v2.4.14 - Added dump_count - kdg
## 2014-01-11 - v2.4.15 - Re-enabling wuauserv - kdg
## 2014-01-13 - v2.4.16 - Disabling installation of Chrome.  Enabling installation of MS Viewers for Contract stores - kdg
## 2014-01-17 - v2.4.17 - Found a place where wuauserv was getting disabled.  Corrected that.  Cleaned out some old code - kdg
## 2014-01-21 - v2.4.18 - Updated install_microsoft_viewer to create the temp/install folder if it does not exist - kdg
## 2014-01-22 - v2.4.19 - Added provision for copying transnet config files over to support upgrade to SAP POS 2.3 - kdg
## 2014-01-29 - v2.4.20 - Cleanup now checks for files in the polling folder - kdg
## 2014-01-30 - v2.4.21 - Cleanup now purges files in the polling folder more than 10 days old.  SAP POS 2.3 logs errors are no longer warnings - kdg
## 2014-01-31 - v2.4.22 - Put a retry attempt into save_log - kdg
## 2014-02-16 - v2.4.23 - Revisions to checking Mozilla FireFox version. - kdg
## 2014-02-17 - v2.4.24 - Added SAP_POS_VERSION variable - kdg
## 2014-02-18 - v2.4.25 - Added support for syncing rdp & docx files from Edna - kdg
## 2014-02-23 - v2.4.26 - Corrections to check_registry to account for changes made to get_user - kdg
## 2014-02-25 - v2.5.0 - Added checking for vnc service.  Started working on unpacking SAP POS files - kdg
## 2014-02-26 - v2.5.1 - Added create_tvnserver_service.  Finished the unpack_sap_pos function - kdg
## 2014-03-03 - v2.5.2 - Updated Checking cCleaner settings.  Corrections to copy_install_misc - kdg
## 2014-03-06 - v2.6.0 - Adding check_arf - kdg
## 2014-03-12 - v2.6.1 - Revisions to check_arf & update_arf - kdg
## 2014-03-13 - v2.6.2 - Added misc_only & benchmark_only - kdg
## 2014-03-22 - v2.6.3 - Collecting Mac Address.  Modified check_arf to report only at this time - kdg
## 2014-03-24 - v2.6.4 - Some cleanup checking for firefox version - kdg
## 2014-04-01 - v2.6.5 - Updated running benchmarks - testing with ^5 stores.  Corrected logging. - kdg
## 2014-04-02 - v2.6.6 - Removed Windows XP End of Service notification task - kdg
## 2014-04-03 - v2.7.0 - Some revisions to check_arf.  Revised location of POSWIN in check_poswin. Added check_user - kdg
## 2014-04-07 - v2.7.1 - Updated check_installed_software to show warnings only for Company stores - kdg
## 2014-04-14 - v2.7.2 - Enable correcting ARF for limited number of stores - kdg
## 2014-04-16 - v2.7.3 - Disabling wuauserv (Automatic Updates) - kdg
## 2014-04-23 - v2.7.4 - Enabling check_arf progressively by day of week - kdg
## 2014-04-30 - v2.7.5 - Enabling update_arf for all stores - kdg
## 2014-05-01 - v2.8.0 - Collecting NIC info - kdg
## 2014-05-02 - v2.8.1 - Re-enabling automatic updates (IE Patch came out) - kdg
## 2014-05-05 - v2.8.2 - Revised checking for default browser.  Make FoxitReader the default using registry file - kdg
## 2014-05-06 - v2.8.3 - Removed WARNING for IE being default browser - kdg
## 2014-05-08 - v2.8.4 - Added warning for state.arf not being present - kdg
## 2014-05-14 - v2.8.5 - Disabled the routine to fix the customer number.  Just alert that the state.arf file is not present. - kdg
## 2014-05-15 - v2.9.0 - Added check_windows_updates to enable the automatic updates (the wuauserv was already handled)
##						 Also, exit the initial script if the script is updated and running the updated version - kdg
## 2014-05-19 - v2.9.1 - Added check_web_access
## 2014-05-20 - v2.9.2 - Enabled checking arf but not updating - kdg
## 2014-05-21 - v2.9.3 - Some adjustments to check_arf to eliminate not numberic data - kdg
## 2014-05-22 - v2.9.4 - Some revisions to check_pcAnywhere to eliminate false errors - kdg
## 2014-05-29 - v2.9.5 - Lowered the importance of the check_installed_software function - kdg
## 2014-06-09 - v2.9.6 - Added reboot_requested_flag_alt - kdg
## 2014-06-10 - v2.9.7 - Added accessing grmanage to freshen cookie - kdg
## 2014-07-08 - v2.9.8 - Updated valid_task_hash - kdg
## 2014-07-10 - v2.9.9 - Reset ProfileReport weekly - kdg
## 2014-07-21 - v2.9.10 - Disabled checking time of virus def update. - kdg
## 2014-07-22 - v2.9.11 - Added JavaQuickStarterService to the list of services to stop and disable - kdg
## 2014-07-23 - v2.9.12 - More work with restarting system when POS is using too much memory - kdg
## 2014-07-25 - v2.9.13 - Set request to reboot if POSW is using too much memory - kdg
## 2014-07-25 - v2.10.0 - Added check_posmonitor_task - kdg
## 2014-07-28 - v2.10.1 - Enabled updating .job files from Edna - kdg
## 2014-07-31 - v2.10.2 - Updated reading event log - kdg
## 2014-08-01 - v2.10.3 - Added SymantecManagementClient to Start_Service_Hash - kdg
## 2014-08-05 - v2.10.4 - Added check_timezone.  Disabling SmartCard - kdg
## 2014-08-25 - v2.10.5 - More logging for when the POS is restarting - kdg
## 2014-08-28 - v2.10.6 - Collecting stats for POS restarting - kdg
## 2014-09-09 - v2.10.7 - Added check_specific_files - kdg
## 2014-09-17 - v2.11.0 - Correction to checking timezone.  Added check_dns - kdg
## 2014-09-24 - v2.11.1 - Updated check_arf to allow number to be a little higher than expected - kdg
## 2014-09-25 - v2.11.2 - Added $new argument.  Updated check_software for SAP POS 2.3 - kdg
## 2014-09-30 - v2.11.3 - Updated schedule_update to not create a second schedule if one exists - kdg
## 2014-10-01 - v2.11.4 - Some corrections to finding Transnet and SAP POS version - kdg
## 2014-10-03 - v2.11.5 - Revised checking for TimeStar link on desktop.  Corrected System Stats punctuation.  Adjusted memory limit to 35000 - kdg
## 2014-10-09 - v2.11.6 - Revised check_dns to set alternative dns even if existing dns servers respond to ping if still cannot ping google - kdg
## 2014-10-10 - v2.11.7 - Revised check_web_access since I don't think it is valid for the revised network scheme. - kdg
## 2014-10-27 - v3.0.0 - Revising for Windows 7 support - kdg
## 2014-10-31 - v3.0.1 - More revisions for Win7 support - kdg
## 2014-11-03 - v3.0.2 - Added time_sensitive_phrases array - kdg
## 2014-11-04 - v3.0.3 - Refining time_sensitive_phrases - kdg
## 2014-11-20 - v3.0.4 - Bumped the memory usage limit to 40000 - kdg
## 2014-12-10 - v3.0.5 - Adjustments to check_tasks and disabled checking CPU usage - kdg
## 2014-12-11 - v3.1.0 - Updated valid task list. Added check_shadowstorage - kdg
## 2014-12-19 - v3.1.1 - Added eftdevicedriver to valid task list - kdg
## 2015-01-07 - v3.1.2 - Added checking for network discovery and file sharing on Win 7 - kdg
## 2015-01-15 - v3.1.3 - Adding day_request option.  Some additional phrases to checking events - kdg
## 2015-01-20 - v3.1.4 - Added disable_privoxy - kdg
## 2015-01-22 - v3.2.0 - Updated to install SEP12 - kdg
## 2015-01-23 - v3.2.1 - Updated check_for_update to collect .ini files - kdg
## 2015-01-30 - v3.2.2 - More work on installing SEP12 and revised scheduling a reboot - kdg
## 2015-02-03 - v3.2.3 - Enabled the disabling of privoxy - kdg
## 2015-02-05 - v3.2.4 - Added backup_S32.  Corrections to checking new regmonitor.pl script - kdg
## 2015-02-06 - v3.2.5 - Updated check_S32 to replace sync_dlls - kdg
## 2015-02-06 - v3.3.0 - Added check_dlls.  Added check_dotNet - kdg
## 2015-02-12 - v3.4.0 - Renamed check_S32 to check_correct_S32 patterned after monitor.pl.  Major revisions to handling DLL issues.  Enabling install of SEP12 - kdg
## 2015-02-13 - v3.4.1 - Some logging for statis of proxy configuration - kdg
## 2015-02-16 - v3.4.2 - Setting need_to_reboot variable at end of SEP12 upgrade.  Added reboot option - kdg
## 2015-02-17 - v3.4.3 - Enabled SEP12 upgrade on all registers - kdg
## 2015-02-18 - v3.4.4 - Updated expected services for SEP12 - kdg
## 2015-02-23 - v3.4.5 - Updated schedule_update to run SepLiveUpdate.exe to update SEP12 definitions - kdg
## 2015-02-25 - v3.4.6 - Updated check_misc to add tail - kdg
## 2015-02-26 - v3.4.7 - Updated schedule_update with tool_exe - kdg
## 2015-03-01 - v3.4.8 - Corrected an issue with installing tail - kdg
## 2015-03-02 - v3.5.0 - Still found SEP12 not enabled for all registers and corrected that.  Added check_date and correct_date - kdg
## 2015-03-03 - v3.5.1 - Set reboot flag if install of SEP has previously been completed.  Modified installing tail.exe - kdg
## 2015-03-04 - v3.5.2 - Renamed net_upgrade.pl to net_install.pl.  Corrections to check_antivirus_log - kdg
## 2015-03-09 - v3.5.3 - Updated check_for_updates to not run after 7:00.  Some adjustments to checking eventlog for SEP events - kdg
## 2015-03-10 - v3.5.4 - Added basic option.  Imported code from monitor.pl for checking antivirus definitions - kdg
## 2015-03-11 - v3.6.0 - Corrections to checking for customscheduledscan.  (Need to look in local machine.)  Added do_scan - kdg
## 2015-03-13 - v3.6.1 - Renamed do_scan schedule_do_scan - kdg
## 2015-03-16 - v3.6.2 - Revisions to schedule_do_scan.  Enable .Net for KoP reg1 - kdg
## 2015-03-17 - v3.6.3 - Moved date_check into precheck function.  Revised correct_date function to check Edna - kdg
## 2015-03-18 - v3.7.0 - Added disable_privoxy_firefox.  Added check_eft - kdg
## 2015-03-20 - v3.7.1 - Running schedule_do_scan if no scan has been found in the past 7 days.  Some adjustments to allowing check_eft - kdg
## 2015-03-23 - v3.7.2 - Added doscan.exe to the valid task list - kdg
## 2015-03-24 - v3.7.3 - Enabled check_dotnet on a few more stores - kdg
## 2015-03-25 - v3.7.4 - Added check_eft_log and more work with updating eft - kdg
## 2015-03-27 - v3.8.0 - Added check_antivirus_scan_results - kdg
## 2015-03-30 - v3.8.1 - Enabling check_dotnet for all registers - kdg
## 2015-04-09 - v3.9.0 - Added checking virtual memory of posw.  Added check_registry_win7 - kdg
## 2015-04-10 - v3.9.1 - Enabled check_eft for all stores starting with 5 - kdg
## 2015-04-10 - v3.9.2 - Added check_perflib - kdg
## 2015-04-13 - v3.9.3 - Revised check_perflib to use a fork to determine if pslist functions successfully - kdg
## 2015-04-14 - v3.9.4 - Added gather_eft_info - kdg
## 2015-04-15 - v3.9.5 - Revised file_hash in eft_ini_bat_install - kdg
## 2015-04-20 - v3.9.6 - Collecting Performance Option Setting.  Checking page file size in check_memory - kdg
## 2015-04-23 - v3.9.7 - Some adjustments to check_pagefile - kdg
## 2015-04-28 - v3.9.8 - A few more minor adjustments.  Don't check page file unless it is XP - kdg
## 2015-04-29 - v3.9.9 - Allow page file adjustment for all reg2 - kdg
## 2015-05-03 - v3.9.10 - Allow page file adjustment for all reg - kdg
## 2015-05-04 - v3.9.11 - Check psinfo results and run again if necessary in check_software - kdg
## 2015-05-05 - v3.9.12 - Added eft_code_beta_source_dir - kdg
## 2015-05-06 - v3.9.13 - Updated gather_eft_info - kdg
## 2015-05-07 - v3.9.14 - Added check for logit.exe - kdg
## 2015-05-11 - v3.9.15 - Adjustment to disabling privoxy in FireFox - kdg
## 2015-05-11 - v3.9.16 - Correction to eft_code_beta_source_dir - kdg
## 2015-05-12 - v3.9.17 - Added reset_eft_log - kdg
## 2015-05-13 - v3.9.18 - added alternate source for logit.exe - kdg
## 2015-05-14 - v3.9.19 - Revised check_eventlog to exclude APPCRASH during reboot for Windows Updates - kdg
## 2015-05-18 - v3.9.20 - Added install_beta option - kdg
## 2015-05-19 - v3.9.21 - Updates to check_tasks (added exe's for several stores) - kdg
## 2015-05-22 - v3.9.22 - Collecting TTVEFT and TTVLib version info - kdg
## 2015-06-02 - v3.9.23 - Added ping_test and using it to ping VeriFone - kdg
## 2015-06-04 - v3.9.24 - Check for IP address before pinging VeriFone. Added SepMasterService to start_service_hash - kdg
## 2015-06-05 - v3.9.25 - Added --chrome to enable installing Chrome browser - kdg
## 2015-06-10 - v3.9.26 - Updated check_web_access - kdg
## 2015-06-16 - v3.9.27 - Added checking for memory usage of EFTDeviceDriver - kdg
## 2015-06-18 - v3.9.28 - Added SEPMasterService to start_service_hash - kdg
## 2015-06-19 - v3.10.0 - Added update_flag_log - kdg
## 2015-07-07 - v3.10.1 - Revised check_antivirus_scan_results to show the last scan completed - kdg
## 2015-07-09 - v3.11.0 - Consolidated eft variables.  Added code to create TTVGateway.ini and code to check it - kdg
## 2015-07-13 - v3.11.1 - Added some checks for register's hostname in configuing the EFT - kdg
## 2015-07-15 - v3.11.2 - Updated check_eft_ini to not check if the code is not yet enabled - kdg
## 2015-07-16 - v3.11.3 - Added perms_only - kdg
## 2015-07-22 - v3.11.4 - Added eft_IP_exception_store_hash to configuring TTVGateway.ini - kdg
## 2015-07-23 - v3.11.5 - Added -defrag option - kdg
## 2015-07-28 - v3.11.6 - Updated specific_file_list.  Corrected dir_list in check_permissions - kdg
## 2015-07-29 - v3.11.7 - Added routing to purge Symantec .dmp files in cleanup() - kdg
## 2015-08-03 - v3.11.8 - Added function to install the reboot utility - kdg
## 2015-08-06 - v3.11.9 - Added get_register_info - kdg
## 2015-08-10 - v3.11.10 - Create target dir if it does not exist - kdg
## 2015-08-11 - v3.11.11 - Added scan_starts_found to check_antivirus_scan_results - kdg
## 2015-08-14 - v3.12.0 - Updated kill_task_list.  Added configure_VeriFone_ini and check_VeriFone_ini - kdg
## 2015-08-17 - v3.12.1 - Clear System Event log with mrt.  Allow updates to get .exe files - kdg
## 2015-08-18 - v3.12.2 - Collecting Chrome and FireFox Versions - kdg
## 2015-08-20 - v3.12.3 - Removed some warnings for checking Chrome version.  Updated valid_task_hash - kdg
## 2015-09-02 - v3.12.4 - Updated some logging with regards to checking for file updates - kdg
## 2015-09-09 - v3.12.5 - Some improvements to check_perflib - kdg
## 2015-09-10 - v3.12.6 - Corrected initial value of perflib variable from 1 to 0.  Also some adjustments to check_registry_daily - kdg
## 2015-09-11 - v3.12.7 - Added reboot_phrases to check_eventlog - kdg
## 2015-09-14 - v3.12.8 - Running correct_date in precheck() - kdg
## 2015-09-15 - v3.12.9 - More work with correct_date.  Added sync_time - kdg
## 2015-09-22 - v3.12.10 - Adding revised_network as a global variable.  Some improvements to collecting serial number. - kdg
## 2015-09-24 - v3.12.11 - Updated task list for win 7 - kdg
## 2015-09-25 - v3.12.12 - Display IP in check_eft - kdg
## 2015-09-25 - v3.12.13 - Adjusting VeriFone IP address for revised network - kdg
## 2015-09-28 - v3.12.14 - Some adjustments to checking for script updates - kdg
## 2015-09-29 - v3.12.15 - Added -no_edna, -as_admin, and -storenum arguments for when reg is a server - kdg
## 2015-10-01 - v3.12.16 - Added check_firewall to the main checklist - kdg
## 2015-10-05 - v3.12.17 - Removed remote desktop from check_firewall - kdg
## 2015-10-07 - v3.12.18 - Added check_bkoff_ini.  Revised scheduling scan.  Updated check_web_access - kdg
## 2015-10-07 - v3.13.0 - More updating to run on a register server - kdg
## 2015-10-08 - v3.14.0 - Added check_scheduled_tasks_win7.  Added /accepteula.  Updated check_firewall to enable if disabled in WinXP - kdg
## 2015-10-12 - v3.15.0 - Added check_firewall_win7 - kdg
## 2015-10-14 - v3.16.0 - Revised check_log to take out "TLOG Data Error" Added check_eft_driver - kdg
## 2015-10-15 - v3.17.0 - Added configure_firewall_rule.  Added @win7_exclude_phrases.  Updated check_arf - kdg
## 2015-10-19 - v3.17.1 - Added $accepteula.  Updated @expected_service_list and @start_service_hash - kdg
## 2015-10-20 - v3.17.2 - Added getting ini files from Edna - kdg
## 2015-10-21 - v3.17.3 - Updated exclude_phrases in check_eventlog - kdg
## 2015-10-25 - v3.18.0 - Added check_eft_status - kdg
## 2015-10-27 - v3.18.1 - Added a check for windows backup in check_registry - kdg
## 2015-10-28 - v3.18.2 - Added -temp option - kdg
## 2015-10-28 - v3.18.3 - Added -allow option and some work on installing EFT for new registers - kdg
## 2015-10-30 - v3.18.4 - Start pcAnywhere on WinXP if it is not running. Added need_perflib_fix to check_eventlog - kdg
## 2015-11-11 - v3.19.0 - Added check_pos_link - kdg
## 2015-11-12 - v3.19.1 - Updated check_eft_log to read the SAP eft.log - kdg
## 2015-11-17 - v3.19.2 - Added local_target_file3.  Added check for store manager settings in registry - kdg
## 2015-11-19 - v3.19.3 - Archive the EFT_LOG file to Edna - kdg
## 2015-11-20 - v3.19.4 - Added set_store_manager_path.  Added cuttoff_hour - kdg
## 2015-11-24 - v3.19.5 - Revised logging for checking EFT LOG - kdg
## 2015-11-25 - v3.19.6 - Revised some code around EFT updating and finding the version - kdg
## 2015-11-25 - v3.20.0 - Added check_local_ini - kdg
## 2015-12-01 - v3.20.1 - Changed warnings about EFT_LOG to NOTE - kdg
## 2015-12-08 - v3.20.2 - Added check_gwx - kdg
## 2015-12-11 - v3.20.3 - Updated check_local_ini.  Updated exit_pos to kill EFTDeviceDriver as well - kdg
## 2016-01-06 - v3.20.4 - Disabled check_software, check_tasks, and check_arf and event log for Win 7 due to resource constraints - kdg
## 2016-01-07 - v3.20.5 - Added my_current_month to check_eft_log.  Disabled warning about virtual memory - kdg
## 2016-01-11 - v3.20.6 - Changed check_software_daily to check_software_weekly.  Re-enabled check_tasks.  Check_arf once a week. - kdg
## 2016-02-01 - v3.20.7 - Updated checking eft log to warn about problematic EFT transactions with txn and datetime - kdg
## 2016-02-04 - v3.20.8 - Revised error_string_list in check_eft_log - kdg
## 2016-02-05 - v3.20.9 - Replacing error_string_list with error_string_hash - kdg
## 2016-02-10 - v3.20.10 - Revised error_string_hash - kdg
## 2016-02-12 - v3.20.11 - Added Returned respCode=3 - kdg
## 2016-02-19 - v3.20.12 - Added eft_beta_store_reg_list.  Disabled check_VeriFone_ini & check_MxReboot_install. Added txnNumber to eft_ini. - kdg
## 2016-02-19 - v3.21.0 - Major cleanup of checking and updating ini files.  Now setting the txnnumber in the ini file - kdg
## 2016-02-23 - v3.21.1 - Checking TxnNumber is in ini file only if driver is not version 1. - kdg
## 2016-02-24 - v3.21.2 - Added update_eft_ini - kdg
## 2016-02-25 - v3.21.3 - Corrected handling of day_of_week variable - kdg
## 2016-02-26 - v3.21.4 - Some cleanup and revision to check_eft_log.  Updated eft_beta_store_reg_list - kdg
## 2016-03-02 - v3.21.5 - Revised check_for_updates by adding @arguments - kdg
## 2016-03-03 - v3.21.6 - Updated eft_beta_store_reg_list - kdg
## 2016-03-04 - v3.21.7 - Updated check_eft_ini to permit TxnNumber even for older drivers - kdg
## 2016-03-09 - v3.21.8 - More logic to roll out eftdevice driver progressively (need to test) - kdg
## 2016-03-11 - v3.21.9 - Added alternateCheck - kdg
## 2016-03-14 - v3.21.10 - Updated accepted_software_win7_list - kdg
## 2016-03-15 - v3.22.0 - Added check_antivirus_win7 - kdg
## 2016-03-16 - v3.22.1 - Added warning if posmonitor.pl task is not present - kdg
## 2016-03-22 - v3.22.2 - Added some more logging to when the code determines if the eft device driver can be installed.
## 2016-03-24 - v3.22.3 - Updated error_string_hash - kdg
## 2016-03-28 - v3.22.4 - HP registers can call schedule_update - kdg
## 2016-03-30 - v3.22.5 - Warning about CustomScheduledScan run by Villages - kdg

use constant SCRIPT_VERSION 	=> "3.22.5";
use Cwd;
use File::Copy;
use File::Basename;
use Getopt::Long;
use Net::SMTP;
use DBI;
use TTV::utilities qw(openODBCDriver dequote execQuery_arrayref recordCount trim LogMsg indent setConfig);
use TTV::lookupdata;
use strict;
use IO::Socket;
use Time::Local;  
use File::Path qw(rmtree);
 

################
## Variables
################
my $help=0;
my $proto=0;
my $force=0;
my $fix=0;
my $verbose=0;
my $revised_network=0;
my $warning_count=0;
my $registry_only=0;
my $antivirus_only=0;
my $software_only=0;
my $event_only=0;
my $memory_only=0;
my $daily_only=0;
my $disk_only=0;
my $check_disk_ran=0;
my $need_to_defrag=0;
my $no_action=0;
my $tasks_only=0;
my $services_only=0;
my $printers_only=0;
my $pcAnywhere_only=0;
my $poswin_only=0;
my $webaccess_only=0;
my $no_precheck=0;
my $schedule_only=0;
my $firewall_only=0;
my $checks_enabled=0;
my $precheck_only = 0;
my $disc_cleanup_only = 0;
my $benchmark_only=0;
my $misc_only=0;
my $various=0;
my $log_only=0;
my $arf_only=0;
my $get_temp=0;
my $allow_regx=0;
my $cuttoff_hour=7;	# The cuttoff time for installation of the EFT driver
my $total_memory = 0;
my $scheduled_tasks_only=0;
my $dotnet_only=0;
my $day_request=1;
my $OS = "";
my $arch=0;
my $accepteula='';
my $monitor_record = "c:/temp/scripts/monitor_record.txt";
my $action_log = "c:/temp/scripts/monitor_action.txt";
my $system_info_file="//Edna/temp/install/register_utilities/site_info.txt";
my $local_system_info="c:/temp/scripts/registerInfo.txt";
my $reboot_requested_flag="c:/temp/reboot_request.flg";	
my $pos_instruction_file="c:/temp/scripts/pos_instructions.txt";
my $perflib_flag="c:/temp/scripts/481033H_safe.flg";
my $register_server=0; 	# This variable is for when the script itself discovered that it is a register/server
my $as_server=0;		# Tell the script that it is running on a system that is a register/server
my $is_admin=0;			# Is the current user in the administrators group?
my $storeStatus="unknown";	
my $privoxy_running=0;
my $customer_number=0;
my $timezone=0;
my %monitor_record_hash;
my $debug_mode=0;
my $general_enable=0;
my $enable_eft_ini_bat_install=0;
my $update_eft_ini=0;
my $check_eft=0;
my $reset_profilereport=0;
my $need_to_reboot=0;
my $check_for_update=0;
my $restart_pos=0;
my $new_register_test=0;
my $install_beta=0;
my $chrome_requested=0;
my $perms_only=0;
my $defrag_only=0;
my $perflib_only=0;
my $as_admin=0;
my $no_edna=0;
my $storenum=0;
# Get the current time in seconds
my $timeseconds=time();
my @ltm = localtime();
my $day_of_week=$ltm[6];
my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);
my $current_year=$ltm[5]+1900;
my $current_month=$ltm[4]+1;
my $current_day=$ltm[3];

my @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
my $previous_date_label = sprintf("%04d-%02d-%02d", $previous_day_ltm[5]+1900, $previous_day_ltm[4]+1, $previous_day_ltm[3]);
my @previous_previous_day_ltm=localtime($timeseconds - (86400 * 2)); # Two days ago
my $previous_previous_date_label = sprintf("%04d-%02d-%02d", $previous_previous_day_ltm[5]+1900, $previous_previous_day_ltm[4]+1, $previous_previous_day_ltm[3]);
my $previous_day_year=$previous_day_ltm[5]+1900;
my $previous_day_month=sprintf("%02d",$previous_day_ltm[4]+1);
my $previous_day_day=sprintf("%02d",$previous_day_ltm[3]);
my $current_hour = $ltm[2];

# This is the new task which is run by scheduled tasks
my $new_task="c:/temp/scripts/regmonitor.pl";
# This is what the old task ran:
my $old_task="c:/rmonitor.bat";


my %services_hash = (
    'apache' => '0',
    'transnet' => '0',
    'Xpress Server' => '0',
    'SQLAnys_SQL_ENG' => '0'    
);
my $TRANSNET_BUILD = "599";
my $TRANSNET_VERSION = "1.5";
my $APACHE_VERSION = "2.0.59";
my $PERL_VERSION = "v5.8.8";
my $TRIVERSITY_VERSION = "Version 9.5.0.0Build390";
my $SAP_POS_VERSION;
my $SAP_POS_VERSION_INFO;
my $SAP_POS_VERSION_EXPECTED = "10.3.0Build740";
chomp(my $hostname = `hostname`); 
# Get the reg's IP address
my $myip=inet_ntoa((gethostbyname($hostname))[4]);  
my $instruction_file = lc($hostname);
$instruction_file.="_instructions.txt";
$instruction_file = "//Edna/temp/install/register_utilities/$instruction_file";
my $register_model='';
my $pslist_safe=0;	# Some 33H registers will not run pslist without being fixed.

# EFT variables
my $eft_code_dir = "C:/Program Files/SAP/Retail Systems/Point of Sale/EFT_CODE";
my $eft_destination_dir = "C:/Program Files/SAP/Retail Systems/Point of Sale/TTVEFT";
my $eftlib_destination_dir = "C:/Program Files/SAP/Retail Systems/Point of Sale/TTVLib";
my $eft_code_source_dir = "//Edna/temp/install/EFT_CODE";
my $eft_code_beta_source_dir = "//Edna/temp/install/EFT_CODE_BETA";	
my $eft_local_ini="C:/Program Files/SAP/Retail Systems/Point of Sale/parm/local.ini";
my $eft_version_ini="C:/Program Files/SAP/Retail Systems/Point of Sale/TTVEFT/version.ini";
my $lib_version_ini="C:/Program Files/SAP/Retail Systems/Point of Sale/TTVLib/version.ini";
my $eft_version=0;
my $lib_version=0;
my $eft_ini = "${eft_destination_dir}/TTVGateway.ini";
my $found_eft_status = "unknown";
my $EFT_status = "unknown";

my %eft_plus2_store_hash=(
	"5263"=>'1',
	"5120"=>'1',
	"3125"=>'1',
	"4673"=>'1'
);

# The first stores to have VeriFone installed did not necessarily follow the 
# IP scheme that was later implemented.  Also, Ephrata is going to be different
# so for now at least, except it as well. - kdg
my %eft_IP_exception_store_hash=(
	"7037"=>'1',
	"5263"=>'1',
	"5120"=>'1',
	"3125"=>'1',
	"4673"=>'1'	
);

# These stores and registers will load the beta version of the EFTDeviceDriver
my @eft_beta_store_reg_list=(
	'2408-reg1',
	'2408-reg2',
	'2765-reg1',
	'2765-reg2',
	'2765-reg3',
	'3074-reg1',
	'3074-reg2',
	'3125-reg1',
	'3125-reg2',
	'3785-reg1',
	'3785-reg2',
	'3964-reg1',
	'3964-reg2',
	'4673-reg1',	
	'4673-reg2',	
	'5120-reg1',	
	'5120-reg2',	
	'5263-reg1',
	'5263-reg2', 	
	'99-reg2',
);
 
my %correct_date_store_hash=(
	'2098' => '1'
);

# Tasks monitor will attempt to end
my @kill_task_list=(
   "pcA_LU.exe",
   "msmsgs.exe",
   "GWX.exe",
   "gwxux.exe",
);

my %valid_task_hash=(
	'smss.exe'=>'1',
	'ss3dfo.scr'=>'1',
	'brccmctl.exe'=>'1',
	'taskmgr.exe'=>'1',
	'bkoff.exe'=>'1',
	'dbclient.exe'=>'1',
	'foxit'=>'1',
	'lxeemon.exe'=>'1',
	'ezprint.exe'=>'1',
	'wordview.exe'=>'1',
	'winlogon.exe'=>'1',
	'services.exe'=>'1',
	'lsass.exe'=>'1',
	'svchost.exe'=>'1',
	'ccsetmgr.exe'=>'1',
	'explorer.exe'=>'1',
	'ccevtmgr.exe'=>'1',
	'spbbcsvc.exe'=>'1',
	'spoolsv.exe'=>'1',
	'aipstart.exe'=>'1',
	'awhost32.exe'=>'1',
	'DefWatch.exe'=>'1', 
	'aipctrl.exe'=>'1',
	'tlntsvr.exe'=>'1',
	'winvnc.exe'=>'1',
	'alg.exe'=>'1',
	'ccapp.exe'=>'1',
	'vptray.exe'=>'1',
	'poswstart.exe'=>'1',
	'rs232manager.exe'=>'1',
	'trvlpo~1.exe'=>'1',
	'iexplore.exe'=>'1',
	'ctfmon.exe'=>'1',
	'perl.exe'=>'1',
	'cmd.exe'=>'1',
	'csrss.exe'=>'1',
	'defwatch.exe'=>'1',
	'rtvscan.exe'=>'1',
	'posw.exe'=>'1',
	'tlist.exe'=>'1',
	'system'=>'1',
	'wmiprvse.exe'=>'1',
	'hkcmd.exe'=>'1',
	'afaagent.exe'=>'1',
	'iomgr.exe'=>'1',
	'arcpd.exe'=>'1',
	'notify.exe'=>'1',
	'locator.exe'=>'1',
	'firefox.exe'=>'1',
 
	'lucoms~1.exe'=>'1',
	'vpc32.exe'=>'1',
	'vpdn_lu.exe'=>'1',
	'luall.exe'=>'1',
	'hpzipm12.exe'=>'1',
	'logon.scr' => '1',
	'foxitr~1.exe'=> '1',
	'update.exe'=> '1',
	'wuauclt.exe'=> '1',
	'tvnserver.exe'=> '1', 
	'aipstartd.exe'=> '1',
	'aiptraced.exe'=> '1',
	'aipupdevd.exe'=> '1',
	'aipanposd.exe'=> '1',
	'aipctrld.exe'=> '1',
	'ssmypics.scr'=> '1',
	'ssstars.scr'=> '1',
	'bjmyprt.exe'=> '1',
	'cnmnsut.exe'=> '1',
	'acrord32.exe'=> '1',  	 	
	'tasklist.exe'=>'1',
	'chrome.exe'=>'1',
	'e_fatiada.exe'=>'1',
	'ssmarque.scr'=>'1',
	'rundll32.exe'=>'1',
	'plugin-container.exe'=>'1',
	'hpboid.exe'=>'1',
	'hpbpro.exe'=>'1',
	'lxeecoms.exe'=>'1',
	'aluschedulersvc.exe'=>'1',
	'brmfcwnd.exe'=>'1',
	'brmfimon.exe'=>'1',
	'wscntfy.exe'=>'1',
	'defrag.exe'=>'1',
	'dfrgntfs.exe'=>'1',
	'ose.exe'=>'1',
	'xlview.exe'=>'1',
	"ccsvchst.exe" => '1',
	"smc.exe" => '1',
	"unsecapp.exe" => '1',
	"smcgui.exe" => '1',
	"aiptouchd.exe" => '1',
	'trvlpoints.exe' => '1',
	'helpsvc.exe' => '1',
	'savui.exe' => '1',
	'sstext3d.scr' => '1',
	'coh32.exe' => '1',
	'sesclu.exe' => '1',
	'lucallbackproxy.exe' => '1',
	'e_famtada.exe' => '1',
	'e_farnada.exe' => '1',
	'sagent4.exe' => '1',
	'wordpad.exe' => '1',
	'notepad.exe' => '1' ,
	'scrnsave.scr' => '1',
	'mstsc.exe' => '1',
	'windowssearch.exe' => '1',
	'searchindexer.exe' => '1',
	'googleupdate.exe' => '1',
	'mrtstub.exe' => '1',
	'mrt.exe' => '1',
	'eftdevicedriver.exe' => '1',
	'doscan.exe' => '1',
	'foxitreader.exe' => '1',
	'msiexec.exe' => '1',
	
	
	);
	
my %valid_win7_task_hash=(
	'wininit.exe' => '1',
	'lsm.exe' => '1',
	'rtkaudioservice.exe' => '1',
	'rthdvbg.exe' => '1',
	'rthdvbg.exe' => '1',
	'aertsrv.exe' => '1',
	'cdi.exe' => '1',
	'wisptis.exe' => '1',
	'wisptis.exe' => '1',
	'tabtip.exe' => '1',
	'taskhost.exe' => '1',
	'dwm.exe' => '1',
	'wmpnetwk.exe' => '1',
	'iastordatamgrsvc.exe' => '1',
	'lms.exe' => '1',
	'uns.exe' => '1',
	'inputpersonalization.exe' => '1',
	'conhost.exe' => '1',
	'trustedinstaller.exe' => '1',
	'audiodg.exe' => '1',
	'searchprotocolhost.exe' => '1',
	'searchfilterhost.exe' => '1',
	'dllhost.exe' => '1',
	'iastoricon.exe' => '1',
	'privacyiconclient.exe' => '1',
	'logonui.exe' => '1',
	'taskeng.exe' => '1',
	'igfxtray.exe' => '1',
	'igfxpers.exe' => '1',
	'rthdvcpl.exe' => '1',
	'tlntsess.exe' => '1',
	'igfxsrvc.exe' => '1',
	'sppsvc.exe' => '1',
 
);
	
my %valid_services_hash=(
	'TheseWindowsservicesarestarted:' => '1',
	'AIPCTRLStart' => '1',
	'ApplicationLayerGatewayService' => '1',
	'AutomaticUpdates' => '1',
	'COM+EventSystem' => '1',
	'ComputerBrowser' => '1',
	'CryptographicServices' => '1',
	'DCOMServerProcessLauncher' => '1',
	'DHCPClient' => '1',
	'DNSClient' => '1',
	'ErrorReportingService' => '1',
	'EventLog' => '1',
	'HelpandSupport' => '1',
	'LogicalDiskManager' => '1',
	'NetworkConnections' => '1',
	'NetworkLocationAwareness(NLA)' => '1',	
	'NTLMSecuritySupportProvider' => '1',
	'pcAnywhereHostService' => '1',
	'PlugandPlay' => '1',
	'PrintSpooler' => '1',
	'ProtectedStorage' => '1',
	'RemoteProcedureCall(RPC)' => '1',
	'SecondaryLogon' => '1',
	'SecurityAccountsManager' => '1',
	'SecurityCenter' => '1',
	'Server' => '1',
	'ShellHardwareDetection' => '1',
	'SSDPDiscoveryService' => '1',
	'SymantecAntiVirus' => '1',
	"SymantecEndpointProtection" => '1',
	"SymantecManagementClient" => '1',
	'SymantecAntiVirusDefinitionWatcher' => '1',

	'SymantecSPBBCSvc' => '1',
	'SystemEventNotification' => '1',
	'SystemRestoreService' => '1',
	'TaskScheduler' => '1',
	'TCP/IPNetBIOSHelper' => '1',
	'Telnet' => '1',
	'TerminalServices' => '1',
	'uvnc_service' => '1',
	'WebClient' => '1',
	'WindowsAudio' => '1',
	'WindowsFirewall/InternetConnectionSharing(ICS)' => '1',
	'WindowsImageAcquisition(WIA)' => '1',
	'WindowsManagementInstrumentation' => '1',
	'WindowsTime' => '1',
	'WirelessZeroConfiguration' => '1',
	'Workstation' => '1', 
	'AdaptecI/OManagerServer' => '1', 
	'AdaptecRAIDRemoteServicesAgent' => '1', 
	'AdaptecStorageManagerNotifier' => '1', 
	'AdaptecWebServer' => '1',
	'RemoteProcedureCall(RPC)Locator' => '1', 
  	'HPNetworkDevicesSupport' => '1',
	'NetDriverHPZ12' => '1',
	'PmlDriverHPZ12' => '1',	
	'Thecommandcompletedsuccessfully.' => '1',
	'HPCUEDeviceDiscoveryService' => '1',	
	'hpqcxs08' => '1',	
	'LiveUpdate' => '1',
	'Themes' => '1',		
	'TightVNCServer' => '1',	
	'BackgroundIntelligentTransferService' => '1',	
	'IBMPOSAutomaticDeviceUpdate' => '1',	
	'IBMPOSControl' => '1',	
	'IBMPOSKeyboardUtility' => '1',	
	'IBMPOSStartupControl' => '1',	
	'IBMPOSTraceUtility' => '1',
	'AutomaticLiveUpdateScheduler' => '1',
 	'lxee_device' => '1' ,
	 
	);
	#'SymantecEventManager' => '1',
	#'SymantecSettingsManager' => '1',
my %valid_win7_services_hash=(
	'DiagnosticsTrackingService' => '1',
	'SecondaryLogon' => '1',
	'SoftwareProtection' => '1',
	'WindowsBackup' => '1',
	
	
	'TheseWindowsservicesarestarted:' => '1',
	'ApplicationInformation' => '1',
	'BackgroundIntelligentTransferService' => '1',
	'COM+EventSystem' => '1',
	'ComputerBrowser' => '1',
	'CryptographicServices' => '1',
	'DCOMServerProcessLauncher' => '1',
	'DHCPClient' => '1',
	'DNSClient' => '1',
	'NetworkConnections' => '1',
	'PlugandPlay' => '1',
	'PrintSpooler' => '1',
	'RemoteProcedureCall(RPC)' => '1',
	'SecurityAccountsManager' => '1',
	'SecurityCenter' => '1',
	'Server' => '1',
	'ShellHardwareDetection' => '1',
	'SSDPDiscovery' => '1',
	'Superfetch' => '1',
	'SymantecEndpointProtection' => '1',
	'SystemEventNotificationService' => '1',
	'TabletPCInputService' => '1',
	'TaskScheduler' => '1',
	'TCP/IPNetBIOSHelper' => '1',
	'Themes' => '1',
	'TightVNCServer' => '1',
	'UserProfileService' => '1',
	'WindowsAudio' => '1',
	'WindowsAudioEndpointBuilder' => '1',
	'WindowsEventLog' => '1',
	'WindowsFirewall' => '1',
	'WindowsFontCacheService' => '1',
	'WindowsManagementInstrumentation' => '1',
	'WindowsMediaPlayerNetworkSharingService' => '1',
	'WindowsModulesInstaller' => '1',
	'WindowsSearch' => '1',
	'WindowsUpdate' => '1',
	'Workstation' => '1',   	
	'AndreaRTFiltersService' => '1',
	'ApplicationExperience' => '1',	  
	'BaseFilteringEngine' => '1',	  
	'CherryDeviceInterface' => '1',	  
	'DesktopWindowManagerSessionManager' => '1',	  
	'DiagnosticPolicyService' => '1',	  
	'DiagnosticServiceHost' => '1',	
	'DistributedLinkTrackingClient' => '1',	
	'FunctionDiscoveryResourcePublication' => '1',	
	'GroupPolicyClient' => '1',	
	'IKEandAuthIPIPsecKeyingModules' => '1',	
	'Intel(R)ManagementandSecurityApplicationLocalManagementService' => '1',	
	'Intel(R)ManagementandSecurityApplicationUserNotificationService' => '1',	
	'Intel(R)RapidStorageTechnology' => '1',	
	'IPHelper' => '1',	
	'IPsecPolicyAgent' => '1',	
	'NetworkListService' => '1',	
	'NetworkLocationAwareness' => '1',	
	'NetworkStoreInterfaceService' => '1',	
	'OfflineFiles' => '1',	
	'Power' => '1',	
	'ProgramCompatibilityAssistantService' => '1',	
	'RealtekAudioService' => '1',	
	'RPCEndpointMapper' => '1',	
	'SymantecManagementClient' => '1',	
	'DiskDefragmenter' => '1',
	'MultimediaClassScheduler' => '1',
	'Telnet' => '1',
	
	'Thecommandcompletedsuccessfully' => '1',	
 
	);	
# In the array below, remove spaces
my @expected_service_list=(
	'SymantecEndpointProtection',	
	'ApplicationLayerGatewayService',
	'pcAnywhereHostService',
	'AutomaticUpdates',
	 
	'ComputerBrowser',
	'CryptographicServices',
	'DCOMServerProcessLauncher',
	'DHCPClient',
	'LogicalDiskManager',
	'DNSClient',
	'ErrorReportingService',
	'EventLog',
	'COM+EventSystem',
	'HelpandSupport',
	'Server',
	'Workstation',
	'TCP/IPNetBIOSHelper',
	'NetworkConnections',
	'NetworkLocationAwareness(NLA)',
	'NTLMSecuritySupportProvider',
	'PlugandPlay',
	'ProtectedStorage',
	'RemoteProcedureCall(RPC)',
	'SecurityAccountsManager',
	'TaskScheduler',
	'SecondaryLogon',
	'SystemEventNotification',
	'WindowsFirewall/InternetConnectionSharing(ICS)',
	'ShellHardwareDetection',
	'PrintSpooler',
	'SystemRestoreService',
	'SSDPDiscoveryService',
	'TerminalServices',
 
	'Telnet',
	'TightVNCServer',
	'WindowsTime',
	'WebClient',
	'WindowsManagementInstrumentation',
	'SecurityCenter',		
 
);	
	#'SymantecSettingsManager',
	#'SymantecEventManager',
	#'SymantecManagementClient',

my @expected_services_win7_list=(
);
 

my %enable_service_hash = (
	'wuauserv' => 'wuauserv'
);	 
my %start_service_hash=(
	'wuauserv' => 'wuauserv',
	'AutomaticUpdates' => 'wuauserv',
	'SepMasterService' => 'SepMasterService',
	'SymantecEndpointProtection' => 'SepMasterService',
	'PrintSpooler' => 'Spooler',	

);


	#'SymantecManagementClient' => 'SmcService'
my %start_service_win7_hash=(
 
	 
);

my %manual_service_hash=(	
	'HIDInputService' => 'HidServ',
	'Alerter' => 'Alerter',
	'NFMClientService' => 'NFM Client service',
	'NFM Client service' => 'NFM Client service',
	'NFM Client service' => 'NFMClientService',
);

my %uninstall_hash=(
	'googletoolbarnotifier.exe' => '1'
);

my %stop_service_hash=(
	'WindowsInstaller' => 'msiserver',
	'HIDInputService' => 'HidServ',
	'Alerter' => 'Alerter',
	'NFMClientService' => 'NFM Client service',
	'NFM Client service' => 'NFM Client service',
	'NFM Client service' => 'NFMClientService',	
	'lxee_device' => 'lxee_device',
	'HPSIService' => 'HPSIService',	
	'OfficeSourceEngine' => 'ose',
	'Telephony' => 'TapiSrv',
	'RemoteAccessConnectionManager' => 'RasMan',
	'JavaQuickStarterService' => 'JavaQuickStarterService'
	
);

my %stop_win7_service_hash=();

#'wuauserv' => 'wuauserv'

	# The following services have been tested but found to have access denied - kdg
	#'DistributedTransactionCoordinator' => 'MSDTC',
	#'COM+SystemApplication' => 'COMSysApp',

my %month_name_hash=(
	'01' => 'Jan',
	'02' => 'Feb',
	'03' => 'Mar',
	'04' => 'Apr',
	'05' => 'May',
	'06' => 'Jun',
	'07' => 'Jul',
	'08' => 'Aug',
	'09' => 'Sep',
	'10' => 'Oct',
	'11' => 'Nov',
	'12' => 'Dec'	
);	

my %month_num_hash=(
	'Jan' => '01',
	'Feb' => '02',
	'Mar' => '03',
	'Apr' => '04',
	'May' => '05',
	'Jun' => '06',
	'Jul' => '07',
	'Aug' => '08',
	'Sep' => '09',
	'Oct' => '10',
	'Nov' => '11',
	'Dec' => '12'	
);		

my @arguments=@ARGV;
 

my $goRC = GetOptions(
	"help"=>\$help,
	"proto"=>\$proto,
	"verbose"=>\$verbose,	
	"debug"=>\$debug_mode,
	"dotnet"=>\$dotnet_only,
	"registry"=>\$registry_only,
	"daily"=>\$daily_only,
	"antivirus"=>\$antivirus_only,
	"software"=>\$software_only,
	"event"=>\$event_only,
	"mem"=>\$memory_only,
	"disk"=>\$disk_only,
	"no"=>\$no_action,
	"tasks"=>\$tasks_only,
	"proc"=>\$tasks_only,
	"services"=>\$services_only,	
	"printer" => \$printers_only,
	"pca"=>\$pcAnywhere_only,
	"poswin"=>\$poswin_only,
	"web"=>\$webaccess_only,
	"firewall"=>\$firewall_only,
    "checks" => \$checks_enabled,
	"log"=>\$log_only,
	"precheck" => \$precheck_only,
	"force" => \$force,
	"fix" => \$fix,
	"cleaner" => \$disc_cleanup_only,
	"benchmark" => \$benchmark_only,
	"misc" => \$misc_only,
	"various" => \$misc_only,
	"arf" => \$arf_only,
	"schedule" => \$schedule_only,
    "new" => \$new_register_test,
	"day:i"=>\$day_request,
	"reboot"=>\$need_to_reboot,
	"restart"=>\$restart_pos,
	"update"=>\$check_for_update,
	"basic"=>\$no_precheck,
	"eft"=>\$check_eft,
	"enable"=>\$general_enable,
	"install_beta"=>\$install_beta,
	"chrome"=>\$chrome_requested,
	"perms"=>\$perms_only,
	"defrag"=>\$defrag_only,
	"perflib"=>\$perflib_only,
	"as_admin"=>\$as_admin,
	"no_edna"=>\$no_edna,
	"storenum:i"=>\$storenum,
	"as_server"=>\$as_server,
	"temp"=>\$get_temp,
	"allow"=>\$allow_regx,
	"hour:i"=>\$cuttoff_hour,
	
);
 

if ($goRC == 0) {
   LogMsg(indent(1)."Errors parsing command parameters");
 
   usage();
   exit;
}
if ($help) {
	usage();
   exit;
}
unless ($hostname =~ /^Reg/i) {    
    print "Sorry, this utility is only for registers.\n";
	exit;
} 
LogMsg("$0 started version:".SCRIPT_VERSION. (@arguments ? "(arguments = @arguments)" : "") );
if ($new_register_test) {
    reset_record();
}
check_user();
 
if ($proto) {

	precheck();
	#$force=1;
	#check_dotnet();
	#check_antivirus_scan_results();
	#correct_date();
	#sync_time();
	#check_misc();
	#check_eft_ini();
	#cleanup();
	#check_eft();
	#reset_eft_log();
	#check_posmonitor_task();
	#schedule_do_scan();
	#schedule_update();
	#check_permissions();
	#check_misc();
	#run_cpu_benchmark();
	#run_hd_benchmark();
	#check_script_updates();
	#gather_sysinfo();
	#check_register_server();
	#check_bkoff_ini();
	#exit_pos();
	#check_local_ini();
	#check_posmonitor_task();
	#check_eft_log();
	#get_temp();
	#check_eft_status();
	#precheck();
	#check_dns();
	check_antivirus_scan_win7();
	#check_timezone();
	#check_web_access();
	#check_shadowstorage();
	#check_arf();
	#check_installed_software();
 	#unpack_sap_pos();
	#check_registry_daily();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	#save_log();	
	exit;
}
if ($arf_only) {

	$force=1;
	#precheck();
	if (-f $system_info_file) {
		# Read in the site info
		LogMsg("Reading $system_info_file...");
		open(REC,"$system_info_file");
		my @site_info=(<REC>);
		close REC;
		my $regnum=lc($hostname);
		foreach my $r (@site_info) {
			chomp($r);
			my @tmp=split(/\s+/,$r);
 
			if ($tmp[0] =~ /storenum/) {
				$storenum=$tmp[1];
				LogMsg("Running on store $storenum");
			}
			if ($tmp[0] =~ /storeStatus/) {
				$storeStatus=$tmp[1];
				LogMsg("Running on a $storeStatus store");
			}	
			if ($tmp[0] =~ /privoxy/) {
				$privoxy_running=$tmp[1];
			}
 
			if ($r =~ /customer_number.*$regnum/)  {
				my @ttmp=split(/:/,$r);
				$customer_number=$ttmp[-1];				
			}				
		}
	} else {
		LogMsg("Did not find $system_info_file");
	}		
	LogMsg("Checking ARF...");
	check_arf();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($misc_only) {
	$force=1;
    $checks_enabled=1;
	precheck();
	check_misc();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($benchmark_only) {
	$force=1;
    $checks_enabled=1;
	precheck();
	run_benchmarks();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($firewall_only) {	
	$force=1;
    $checks_enabled=1;
	precheck();
	check_firewall();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($pcAnywhere_only) {
	$force=1;
	precheck();
	check_pcAnywhere();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($schedule_only) {
	$force=1;
	precheck();
	check_scheduled_tasks();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	#save_log();	
	exit;
}
if ($poswin_only) {
	$force=1;
	precheck();
	check_poswin();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($webaccess_only) {
	$force=1;
	precheck();
	check_web_access();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}

if ($registry_only) {
	$force=1;
	precheck();
	check_registry();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($daily_only) {
	$force=1;
	precheck();
	check_registry_daily();
    check_software_weekly();
	check_minidump();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($software_only) {
	$force=1;
	precheck();
	check_software();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	exit;
}
if ($antivirus_only) {
	#$force=1;
	precheck();
	check_antivirus();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($event_only) {
	$force=1;
	precheck();
	check_eventlog();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($memory_only) {
	$force=1;
	precheck(); 
	check_memory();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($disk_only) {
	$force=1;
	precheck();
	check_disk();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($tasks_only) {
	$force=1;
	precheck();
	check_tasks();
	
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
 
if ($services_only) {
	$force=1;
	precheck();
	check_services(); 
	check_running_services();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($printers_only) {
	$force=1;
	precheck();
	check_printers();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($log_only) {
	$force=1;
	precheck();
	check_log();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($dotnet_only) {
	$force=1;
	precheck();
	check_dotnet();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}
if ($disc_cleanup_only) {
	$force=1;
	precheck();
	run_cleanup();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();
	exit;
}

if ($need_to_reboot) { 
	LogMsg("Found request to reboot");
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
 
	exit_pos();
	reboot(); 	
	exit;
}
if ($check_eft) {
	precheck();	
	if ($general_enable) {
		# This will install the EFT batch file
		$enable_eft_ini_bat_install=1;
	}
	check_eft();

	if ($restart_pos) {
		# For this request, we stop the POS and rely on the posmonitor (which is run by Villages) to restart the POS
		# This is done because it is important that it is Villages that starts the POS - kdg
		LogMsg("Found request to restart POS");
		exit_pos("restart"); 
	}	
	LogMsg("$0 completed");
	save_log();	 	
	exit;
}

if ($check_for_update) {
	precheck();
	check_script_updates();
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
	save_log();	
	save_record();	
	exit;
}
if ($perms_only) {
	precheck();
	check_permissions(); 
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
 	
	exit;
}
if ($defrag_only) {
	precheck();
	run_defrag(); 
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
 	
	exit;
}
if ($perflib_only) {
	precheck();
	check_perflib(); 
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
 	
	exit;
}
if ($get_temp) {
	precheck();
	get_temp(); 
	LogMsg("RESULTS: $warning_count issues found");
	LogMsg("$0 completed");
 	
	exit;
} 

################
## Main
################
 
precheck();
 
if ($checks_enabled) {
    gather_sysinfo();
    check_reboots();
    check_antivirus();
    check_eventlog();
    check_software();	
    check_services(); 
    check_scripts();
    check_pcAnywhere();
    check_registry();
    check_registry_daily();
    check_software_weekly();
    check_windows_updates();
    check_memory();
    check_disk();
    check_minidump();
    if ($need_to_defrag) {
    	run_defrag();
    } else {
    	run_cleanup();
    }
    check_tasks(); 
    check_running_services();
    #check_for_update();
    check_poswin();
	check_printers();
	check_permissions();
    check_misc();
	check_firewall();
	#run_benchmarks();
	check_arf(); 
	check_web_access();
	check_shadowstorage();
	check_dotnet();	
	check_eft();
    cleanup();
    save_record();
	check_log();
	check_perflib();
}
 
LogMsg("RESULTS: $warning_count issues found");
if ($need_to_reboot) {
	# Log this before we finish
	LogMsg("Found request to reboot");
}
if ($restart_pos) {
	# Log this before we finish
	LogMsg("Found request to restart POS"); 
}
LogMsg("$0 completed");
save_log();
if ($need_to_reboot) {
	LogMsg("Found request to reboot");
	exit_pos();
	reboot();
}
if ($restart_pos) {
	# For this request, we stop the POS and rely on the posmonitor (which is run by Villages) to restart the POS
	# This is done because it is important that it is Villages that starts the POS - kdg
	LogMsg("Found request to restart POS");
	exit_pos("restart"); 
}

################
## Functions
################

sub usage {
	print "Version: ".SCRIPT_VERSION. "\n";
	print "\nUSAGE: regmonitor.pl [options] version:  \n";
    print "\nA tool to monitor the register in the store.\n";
	print "Optional Parameters:\n";
	print "  --help         Show this help. \n";
	print "  --registry     Check registry only.\n";
	print "  --software     Check software only.\n";
	print "  --daily        Run the daily checks only.\n";
	print "  --antivirus    Check antivirus only.\n";
	print "  --mem          Check memory only.\n";
	print "  --disk         Check disk only.\n";
	print "  --no           No Actions (Defrag, Virus Def Update, etc.).\n";
	print "  --task         Check running tasks.\n";
	print "  --service      Check running services.\n";
	print "  --pca          Check pcAnywhere.\n";
	print "  --sched        Check scheduled tasks.\n";
	print "  --event        Check event log.\n";
	print "  --log          Check POS logs.\n";
	print "  --precheck     Run precheck only (updating scripts).\n";
	print "  --cleaner      Run disk cleanup only.\n";
	print "  --benchmark    Run benchmarks only.\n";
	print "  --misc         Run misc checks only.\n";
	print "  --web          Check webaccess only.\n";
	print "  --reboot       Reboot the register.\n";
	print "  --update       Check for software updates only.\n";
	print "  --basic        Skip the more intensive parts of precheck.\n";
	print "  --eft          Install basic EFT support\n";
	print "  --enable       Enables full support.  For example, if used with --eft\n";
	print "                 it will install the full EFT support so be certain \n";
	print "                 this is what you want.\n";
	print "  --install_beta Install the beta EFT code\n";
	print "  --chrome       Enables chrome installation when used with --daily.\n";
	print "  --defrag       Run the defrag function.\n";
	print "  --perflib      Run the check_perflib function.\n";	
	print "  --as_admin		Bypass the normal check that the script be run as admin.\n";
	print "  --no_edna		Do not try to save to Edna.\n";
	print "  --storenum=<i>	Specify the store number.\n";
 
	 
}

sub precheck {
	# Check that the date is correct first of all.
	unless (check_date()) {
		correct_date();
		if (check_date()) {
			LogMsg("Date has been corrected.");
		} else {
			LogMsg("WARNING: Date may be incorrect");
			$warning_count++; 
		}
	}	
	if ($hostname =~ /RegX/i) {
		if ($allow_regx) {
			LogMsg("Notice: Hostname for this system is ($hostname)");
		} else {
			LogMsg("WARNING: Hostname for this system is ($hostname)");
			LogMsg("This script cannot be run until the hostname is changed.");
			LogMsg("(Running this script with the -allow argument will allow it to be run.)");
			exit;
		}
	}
	# Determine if the store is on the revised network or not
	# The revised network has a subnet mask of 240	
	my @ip_info=`ipconfig /all`;
	foreach my $i (@ip_info) {
		 
		if ($i =~ /subnet mask/i) {
			my @tmp=split(/: /,$i);
			my $subnetMask=$tmp[1];
			if ($subnetMask =~ /255.255.255.240/) {
				$revised_network=1;				
			}
		}
	}	

	# Find the OS
	my @os_info=`ver`;
	foreach my $o (@os_info) {
		if ($o =~ /Windows 2000/) {
			$OS = "Win2000";
		}
		if ($o =~ /Windows XP/) {
			$OS = "WinXP";
			$arch="32-bit";	
		}		
	} 
	unless ($OS) {
		my @system_info=`wmic os get Caption /value`;		
		foreach my $si (@system_info) {				
			if ($si =~ /Windows 7/) {
				$OS="Win7";	
				$accepteula="/accepteula";
				# Find the architecture
				$arch=get_arch();					
			}			
			if ($si =~ /Windows XP/) {
				$OS="WinXP";	
				$arch="32-bit";				
			}					
		}
		if ($OS) {
			LogMsg("System Info: OS: $OS"); 	
		}	
	}
	if ($OS) {
		LogMsg("System Info: OS: $OS"); 	
	} else {
		LogMsg("WARNING: Failed to determine Operating System");
		$warning_count++;
	}	

	
	# Are we running as a server/register or just a register
	$register_server=check_register_server();
	
	unless ($register_server) {
		if (-f $instruction_file) {		
			LogMsg("Found instruction file");
			open(INSTR,"$instruction_file");
			my @instructions=(<INSTR>);
			close INSTR;
			foreach my $i (@instructions) {
				if ($i =~ /RESET PROFILEREPORT/) {				
					$reset_profilereport = 1;
					LogMsg(indent(1)."Found instructions to reset profile report");
					$restart_pos = 1;	# I added this because I have a theory that a POS restart may also address the
											# issue where a register does not report during the Authorization reports - kdg
											# 2014-04-03
					LogMsg(indent(1)."WARNING: Set flag to restart POS due to NOT REPORTING");
					$warning_count+=update_flag_log("Set flag to restart POS due to NOT REPORTING");
					LogMsg(indent(1)."System Stats: Restart POS NotReporting: 1"); 
				}
			}
			unlink $instruction_file;
		} else {
			if ($verbose) {
				LogMsg(indent(1)."Did not find $instruction_file");
			}
		}	
	}

 	if ($register_server) {
		my $sql="dbsrv12.exe";
		if (checkPID($sql)) {
			my $SQL_ENG = TTV::utilities::setSql();
			#$sql_running=1;        
			my $dsn = "DSN=BACKOFF;ENG=$SQL_ENG;UID=****;PWD=****;";		
			my $dbh = TTV::utilities::openODBCDriver($dsn);        		
			#$db_version = TTV::utilities::checkDBVersion($dbh,"full");
			$storenum = TTV::utilities::getStoreNum($dbh); 
		} else {
			# Get the storenum from an alternative source   		 
			$storenum=getStoreNum_from_Registry(); 
		}		
	} elsif (-f $system_info_file) {
		# Read in the site info
		LogMsg("Reading $system_info_file...");
		open(REC,"$system_info_file");
		my @site_info=(<REC>);
		close REC;
		my $regnum=lc($hostname);
		foreach my $r (@site_info) {
			chomp($r);
			my @tmp=split(/\s+/,$r);
			if ($tmp[0] =~ /storenum/) {
				$storenum=$tmp[1];
				LogMsg("Running on store $storenum");
			}
			if ($tmp[0] =~ /storeStatus/) {
				$storeStatus=$tmp[1];
				LogMsg("Running on a $storeStatus store");
			}	
			if ($tmp[0] =~ /privoxy/) {
				$privoxy_running=$tmp[1];
			}
			if ($r =~ /customer_number.*$regnum/)  {
				my @ttmp=split(/:/,$r);
				$customer_number=$ttmp[-1];				
			}		
			if ($tmp[0] =~ /timezone/) {
				$timezone=$tmp[1];
			}			
			if ($tmp[0] =~ /EFT_status/) {                
				$EFT_status=$tmp[1];                
			}			            
		}
	} else {
		LogMsg("Did not find $system_info_file");
	}	
	if (-f $monitor_record) {
		# Read in the record of previous tests
		LogMsg("Reading $monitor_record...");
		open(REC,"$monitor_record");
		my @record_info=(<REC>);
		close REC;
		foreach my $r (@record_info) {
			my @tmp=split(/\s+/,$r);
			$monitor_record_hash{$tmp[0]}=$tmp[1];
		}
	} else {
		LogMsg("Did not find $monitor_record");
	}
	# Check the user villages
	my @account_info=`net user villages`;
	foreach my $a (@account_info) {
		if ($a =~ /Local Group Membership/i) {
			if ($a =~ /Administrators/i) {
				unless ($register_server) {
					LogMsg("WARNING: User Villages is in the Administrator group!");
					$warning_count++;
				}
				$is_admin=1;
			}
			if ($a =~ /Power Users/i) {
				LogMsg("WARNING: User Villages is in the Power Users group!");
				$warning_count++;
			}			
		}
	}
	if ($register_server) {
		unless ($is_admin) {
			LogMsg("WARNING: System is a server but user Villages is NOT in the Administrator group!");
			$warning_count++;		
		}
	}
	# Check the POS version
	$SAP_POS_VERSION=get_pos_version();
	if ($OS eq "WinXP") {
		# Get the model of the register 			
		$register_model=get_register_info("Model");
		
		if ($register_model) {
			LogMsg(indent(1)."Register Model: $register_model");
			if ($register_model eq "481033H") {
				# If this is a 33H, check that it is safe to run pslist
				if (-f $perflib_flag) {
					$pslist_safe=1;
				}			
			} else {
				# Assume that it is safe for other registers
				$pslist_safe=1;			
			}		
		}	
	}
	
	unless ($no_precheck) {
		# Skip the more intensive parts of precheck
		if ($OS eq "WinXP") {
			# memlogical is not in Win7
			# Get the amount of memory
			my $cmd = "wmic memlogical list /format:list";
			my @mem_info=`$cmd`;
			my $conventional;
			my $contiguous;	
			foreach my $m (@mem_info) {
				chomp($m);		
				if ($m =~ /TotalPhysicalMemory/i) {
					my @tmp=split(/=/,$m);
					$total_memory=$tmp[1];
					$total_memory =~ s/\D//g;			 
				} 	
			} 
			if ($total_memory) {
				$total_memory = sprintf("%.2f",($total_memory / 1024));
				LogMsg(indent(1)."Total Memory: $total_memory");
			}		
		}
		unless (($checks_enabled) || ($no_action)) {
			# Unless this script is called with the -checks argument, it will look for updated scripts and then call the updated scripts
			check_for_update();
		}
	}
	if ($precheck_only) {	
		# Disble further checks
		$checks_enabled=0;
	}
 
}

sub check_perflib {
	# This is all to check that the performance counters on a 33H are working.
	# If they are corrupted, then attempting to run pslist causes a dwwin.
	# This function will test if pslist is crashing and causing a dwwin and then
	# attempt to correct that situation by rebuilding the counters if such a situation
	# is detected.  It does this by running pslist in another process and seeing if it crashed. - kdg
 	
	if (-f $perflib_flag) {
		unless ($perflib_only) {
			return 1;
		} else {
			LogMsg("Perflib flag found");
			LogMsg("Proceeding with test...");
		}
		
	}
		
	if ($register_model) {
		if ($perflib_only) {
			LogMsg("Register model: ($register_model)");
		}
	    if ($register_model eq "481033H") {
			LogMsg("Checking that pslist works...");
		} else {
			LogMsg("Just checking 33H at present");
			return;
		}
	} else {
		LogMsg("Do not know the model of this register");
		return;
	}	
	my $counter_file="c:/temp/scripts/counters.txt";	
	unless (-f $counter_file) { 
		LogMsg("WARNING: Failed to locate $counter_file");
		$warning_count++;
		return 0;
	}		
	my $need_perflib_fix=1;	# Assume we need to fix the counters
 
	
	my $pslist_present=0;
	my @tasklist_info=`tasklist`;
	foreach my $t (@tasklist_info) {
		chomp($t);
		if ($t =~ /pslist.exe/) {
			$pslist_present=1;
		}
	}
	if ($pslist_present) {
		LogMsg("pslist.exe is already running.  This is unexpected.");
		return;
	}
	# Run pslist in another process and see if it is successful

	my $pid=fork();

	if ((defined($pid))&& ($pid == 0)) {
		# Running pslist in another process
		my @meminfo=`pslist -m`; 
	}		
	sleep 4;
	# See if pslist is still running:
	@tasklist_info=`tasklist`;
	foreach my $t (@tasklist_info) {
		chomp($t);
		if ($t =~ /pslist.exe/) {
			$pslist_present=1;
		}
	}	
	if ($pslist_present) {		
		my $p="pslist.exe";
		my $pid=checkPID("$p");		 
		if ($pid) {				
			LogMsg("Killing $p");
			LogMsg("PID is $pid");
			`pskill $accepteula $pid`;
			sleep 1;
			$pid=checkPID("$p");
			if ($pid) {
				LogMsg("WARNING: Failed to kill $p");
				return 0;
			} else {
			   LogMsg("killed $p");
			}			
			$need_perflib_fix=1;		 	
		}
		sleep 2;
		LogMsg("Check for pslist again...");
		$pid=checkPID("$p");
	 
		if ($pid) {				
			LogMsg("Killing $p");
			LogMsg("PID is $pid");
			`pskill $pid`;
			sleep 1;
			$pid=checkPID("$p");
			if ($pid) {
				LogMsg("WARNING: Failed to kill $p");
				return 0;
			} else {
			   LogMsg("killed $p");
			}			
			$need_perflib_fix=1;		 	
		} else {
			LogMsg(indent(1)."pslist not found");
		}		
		
		$p="dwwin.exe";
		LogMsg("Check for $p...");
		$pid=checkPID("$p");
	 
		if ($pid) {				
			LogMsg("Killing $p");
			LogMsg("PID is $pid");
			`pskill $accepteula $pid`;
			sleep 1;
			$pid=checkPID("$p");
			if ($pid) {
				LogMsg("WARNING: Failed to kill $p");
				return 0;
			} else {
			   LogMsg("killed $p");
			}			
			$need_perflib_fix=1;		 	
		} else {
			LogMsg(indent(1)."pslist not found");
		}			
	} else {
		# pslist seems to have completed successfully
		$need_perflib_fix=0;
	}	
	 
	if ($need_perflib_fix) {
		LogMsg("Need to fix perflib");
		if (-f $counter_file) {
			LogMsg("Refreshing counters using lodctr");
			my $cmd="lodctr /r:$counter_file";							 
			system($cmd);							 
		} else {
			LogMsg("WARNING: Failed to locate $counter_file");
			$warning_count++;
			return 0;
		}			
	} else {
		LogMsg("Do Not Need to fix perflib");
		# Setting this flag avoids all of this work in the future
		LogMsg("Setting $perflib_flag");
		open (FLAG,">$perflib_flag");
		close FLAG;		
	}
	 
}

sub get_pos_version {
 
	my $cmd="reg query \"HKLM\\SOFTWARE\" ";    
	my @info=`$cmd`;
	my $found_sap=0;
	my $build_version=0;
	
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /SAP/) {
			$found_sap=1;
		}
	}
	if ($found_sap) {
		$cmd="reg query \"HKLM\\SOFTWARE\\SAP\\Retail Systems\\Point of Sale\\Release Information\" /s";    
		@info=`$cmd`;
		foreach my $i (@info) {
			chomp($i);
			if ($i =~ /Build_Version/) {
				my @tmp=split(/\s+/,$i);				
				$build_version=$tmp[-1];				
			}
		}		
	}
	return $build_version;
}

sub check_software {
	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="software";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}
	LogMsg("Checking software...");

	
    # Check that the expected software is installed
    my $file="C:/Program Files/SQL Anywhere 12/BIN32/dbsrv12.exe";
	
    if (-e "$file") { 
        LogMsg("Found SQL 12");     
		LogMsg("System Info: SQL 12: Installed"); 
    } else {
		LogMsg("System Info: SQL 12: Not_Found");  
    }    
     
    $file="C:/Program Files/SAP/Retail Systems/Xpress Server/xpsctrl.exe";
    if (-e "$file") { 
        LogMsg("Found Xpress Server Controller"); 
        # Check the version 	
        my $version_file="C:/Program Files/SAP/Retail Systems/Xpress Server/VERSION.TXT";
        if (-f "$version_file") {
            open(INPUT,"$version_file");
            my @version_info=(<INPUT>);
            close INPUT;
			my $found_version_info;
			foreach my $line (@version_info) {
				chomp($line);	
				if ($line =~ /SAP POS.*$SAP_POS_VERSION_EXPECTED/)  {
					LogMsg("Found $line"); 
					$found_version_info=1;              
				} 		
			}
			unless ($found_version_info) {
				LogMsg("WARNING: Failed to find Xpress Server Version info");
				$warning_count++;			
			}
        } else {			
            LogMsg("WARNING: Failed to find Xpress Server Version info");
            $warning_count++;
        }
 		
    } else {
		LogMsg("System Info: Xpress Server Controller: Not_Found"); 
        #LogMsg("WARNING: Failed to find Xpress Server Controller - Is XPS installed?");
        #$warning_count++;
    }        
    $file="C:/Program Files/SAP/Retail Systems/Store Manager/bkoff.exe";
    if (-e "$file") { 
        LogMsg("Found Store Manager");     
    } else {
		LogMsg("System Info: Store Manager: Not_Found"); 
        #LogMsg("WARNING: Failed to find Store Manager - Is Store Manager installed?");
        #$warning_count++;
    }    
    $file="C:/Program Files/SAP/Retail Systems/Sales Journal Viewer/journal.exe";
    if (-e "$file") { 
        LogMsg("Found Electronic Journal");     
    } else {
		LogMsg("System Info: Electronic Journal: Not_Found"); 
        #LogMsg("WARNING: Failed to Electronic Journal - Is Electronic Journal installed?");
        #$warning_count++;
    }       
    $file="C:/Program Files/SAP/Retail Systems/DBTools/tmxpurge.exe";
    if (-e "$file") { 
        LogMsg("Found DBTools purge utility");     
    } else {
		LogMsg("System Info: DBTools purge utility: Not_Found");
        #LogMsg("WARNING: Failed to find DBTools purge utility - Is DBTools installed?");
        #$warning_count++;
    } 
    $file="c:/Progra~1/Apache~1/Apache/bin/apache.exe";
	my $file2="c:/Progra~1/Apache~1/Apache/apache.exe";	
    if (-e "$file") { 
        LogMsg("Found Apache");   
        # Check version
        my $cmd="$file -v";
        my @version_info=`$cmd`;
		if (($version_info[0] =~ /APACHE/i) && ($version_info[0] =~ /VERSION/i)) {
            chomp(my $v=$version_info[0]);            
			my @tmp=split(/\s+/,$v);
			$v=$tmp[2];
			@tmp=split(/\//,$v);
			my $found_version=$tmp[1];	
			LogMsg("System Info: Apache Version: $found_version");			
        } else {	
			LogMsg("System Info: Apache Version: Undetermined");		
            LogMsg("WARNING: Found ($version_info[0])");
            $warning_count++;
        }
    } else {
		###
		$file=$file2;
		if (-e "$file") { 			
			LogMsg("Found Apache");   
			# Check version
			my $cmd="$file -v";
			my @version_info=`$cmd`;
			if (($version_info[0] =~ /APACHE/i) && ($version_info[0] =~ /VERSION/i)) {
				chomp(my $v=$version_info[0]);
				my @tmp=split(/\s+/,$v);
				$v=$tmp[2];
				@tmp=split(/\//,$v);
				my $found_version=$tmp[1];
				LogMsg("System Info: Apache Version: $found_version");
				#LogMsg("Found ($v)");
			} else {	
				LogMsg("System Info: Apache Version: Undetermined");			
				LogMsg("WARNING: Found ($version_info[0])");
				$warning_count++;
			}
		} else {
			LogMsg("System Info: Apache: Not_Found");
			#LogMsg("WARNING: Failed to find Apache - Is Apache installed?");
			#$warning_count++;
		} 		
    } 
    # Check the version of perl
    my @version_info=`perl -v`;
    foreach my $v (@version_info) {
        if ($v =~ /This is perl/) {
            my @tmp=split(/\s+/,$v);
			my $found_version=$tmp[3];			
			LogMsg("System Info: Perl Version: $found_version");
			if (0) {
				if ($tmp[3] eq "$PERL_VERSION") {            
					LogMsg("Found Perl $tmp[3]");
				} else {
					LogMsg("WARNING: Found perl $tmp[3]");
					$warning_count++;
				}
			}
        }
    }
    $file="C:/Program Files/SAP/Transnet/bin/transnet.exe";
    if (-e "$file") { 
        LogMsg("Found Transnet");  
        # Check the version
        my $version_file="c:/Program Files/SAP/Transnet/version.ini";
        if (-f $version_file) {
            open(INPUT,"$version_file");
            my @version_info=(<INPUT>);
            close INPUT;
            foreach my $v (@version_info) {
                if ($v =~ /VERSION/) {
                    my @tmp=split(/\s+/,$v);
                    my $v=$tmp[-1];
					
					LogMsg("System Info: Transnet Version: $v");
					if (0) {
						if ($v eq "1.5") {
							LogMsg("Found Transnet version $v");
						} else {
							LogMsg("WARNING: Found Transnet version $v");
							$warning_count++;
						}
					}
                }
                if ($v =~ /BUILD_NUM/) {
                    my @tmp=split(/\s+/,$v);
                    my $v=$tmp[-1];
					LogMsg("System Info: Transnet Build: $v");
					if (0) {
						if ($v eq "$TRANSNET_BUILD") {
							LogMsg("Found Transnet build $v");
						} else {
							LogMsg("WARNING: Found Transnet build $v");
							$warning_count++;
						}
					}
                }                
            }
        } else {
            LogMsg("WARNING: Failed to find Transnet version file");
            $warning_count++;
        }
    } else {
		LogMsg("System Info: Transnet: Not_Found");
        #LogMsg("WARNING: Failed to find Transnet - Is Transnet installed?");
        #$warning_count++;
    }
	
    $file="C:/Documents and Settings/Villages/Local Settings/Application Data/Mozilla Firefox/firefox.exe";
    if (-e "$file") { 		
		LogMsg("System Info: Firefox: Found");		
	}
	
	
	# Checking with psinfo
	my @software;
    # The key is the expected sw, the value is the alternative
    my %expectedSW_hash=(
        'Symantec Endpoint Protection'=>'1',
        'CCleaner'=>'1',
		'LiveReg'=>'1',
		'LiveUpdate'=>'1',
		'Notepad\+\+'=>'1',  		
		'7-Zip'=>'1',  	
		'Internet Explorer'=>'1',	 
		'TightVNC'=>'1', 
		'Microsoft Office Word Viewer'=>'1', 
		'Microsoft Office PowerPoint Viewer'=>'1', 
		'Microsoft Office Excel Viewer'=>'1', 
    );
	if ($OS eq "Win7") {
		%expectedSW_hash=(
			'Symantec Endpoint Protection'=>'1',
			'CCleaner'=>'1', 
			'Notepad\+\+'=>'1',  		
			'7-Zip'=>'1',  				 	 
			'TightVNC'=>'1', 
			'Microsoft Office Word Viewer'=>'1', 
			'Microsoft Office PowerPoint Viewer'=>'1', 
			'Microsoft Office Excel Viewer'=>'1', 
		);
	}
    #my %expectedSW_hash2=(
    #    'Symantec AntiVirus'=>'avast',       
    #);  
    my %expectedSW_hash2=(
        'Symantec AntiVirus'=>'Symantec Endpoint Protection',       
    );   	
    my %acceptedSW_hash=(
        'Privoxy'=>'0',
        'IBackup'=>'0',
		
    );
    my %unexpectedSW_hash=(
        'DynDns Updater 3'=>'0',  
        'Symantec Antivirus 9'=>'0',        
    );    
	my @software_of_interest = (
		"Mozilla Firefox",
	);
	my $found_firefox = 0;	
	my $found_chrome = 0;
	my $default_browser;
	my $firefox_version = '';
	my $found_KB2879017=0;
    my $install_tightvnc=0;
	unless ($OS eq "Win2000") {
				
		# Check for specific updates
		my @systeminfo = `psinfo -s`;
		my $count=$#systeminfo;
		# Occasionally, psinfo -s does not give us the data
		# so here is a basic check and a second attempt.
		if ($count < 100) {
			LogMsg(indent(1)."Running psinfo a second time");
			sleep 5;
			@systeminfo = `psinfo -s`;
		} 
		foreach my $s (@systeminfo) {
			chomp($s);				
			if ($s =~ /KB2879017/) {
				$found_KB2879017++;
			}
			foreach my $soi (@software_of_interest) {			
				if ($s =~ /$soi/i) {				
					LogMsg("Found $s installed");
					my @tmp=split(/\s+/,$s);
					for (my $x=0; $x<=$#tmp; $x++)  {
						my $word = $tmp[$x];						
						if ($word =~ /FireFox/i) {
							$firefox_version = $tmp[($x+1)];		
						}
					}						 
					if ($soi =~ /FireFox/i) {
						$found_firefox=1;						
					}
				}
			}	
		}	
		# Check for chrome
		my $installed_Chrome_file="C:/Program Files/Google/Chrome/Application/chrome.exe";

		if (-f $installed_Chrome_file) {
			$found_chrome=1;
		} 		
		if ($found_firefox) {
			LogMsg(indent(1)."System Info: FireFox: Installed");		 
		}			
		if ($found_chrome) {
			LogMsg(indent(1)."System Info: Chrome: Installed");		 
		}	
		unless (($found_firefox) || ($found_chrome)) {
			LogMsg(indent(1)."Found neither Chrome or FireFox installed");
			#LogMsg(indent(1)."WARNING: Found neither Chrome or FireFox installed");
			#$warns++;
		}		
 
 
		my $uptime;
		# Get some additional info from the systeminfo command.
		my @tmp = grep /Version:/, `systeminfo`;  
		# Since we have this info, get uptime too.
		foreach my $s (@systeminfo) {		
			if ($s =~ /Uptime:/) {
				my @t=split(/:/,$s);
				$uptime=$t[1];
				chomp($uptime);
				$uptime=~s/ //g;
				@t=split(/\,/,$uptime);
				$uptime="Uptime: $t[0]";
				last;
			}      

		}
		LogMsg("Uptime: $uptime");
		# Look for software
		foreach my $sw (sort keys(%expectedSW_hash)) {	
			if ($verbose) {
				print "Checking for $sw\n";
			}
			if (grep /$sw/i,@systeminfo) {
				if ($verbose) {
					print "$sw found in system info\n";
				}					
				my @s=grep /$sw/i,@systeminfo;   
				chomp(@s);				
				foreach my $ss (@s) {
					next if (($ss =~ /Update/) && ($ss !~ /LiveUpdate/));
					push (@software, "$ss");	
					
					if ($verbose) {
						print "Found $ss\n";
					}
				}
				
			} else {
				if ($verbose) {
					print "$sw not found in system info\n";
				}				
				if ($expectedSW_hash{$sw}) {
					if ($verbose) {
						print "Checking further for $sw\n";
					}				
					# An alternative to the expected software (typically used for alternative virus scan software)
					if (chomp(my @alt=grep /$expectedSW_hash{$sw}/i,@systeminfo)) {                    
						LogMsg(indent(1)."Note: Found @alt but not $sw");
					} elsif ($expectedSW_hash2{$sw}) {
						# A second alternative software
						if (chomp(my @alt=grep /$expectedSW_hash2{$sw}/i,@systeminfo)) {                    
							LogMsg(indent(1)."Note: Found @alt but not $sw");
						} else {         														
							LogMsg(indent(1)."WARNING: $sw not installed");                        
							$warns++;  
						}
					} else {						
						if ($sw =~ /Viewer/i) {
							LogMsg(indent(1)."NOTICE: $sw not installed");                 
						} else {
							LogMsg(indent(1)."WARNING: $sw not installed");                 
							$warns++;   			
						}
                        if ($sw eq "TightVNC") {
							LogMsg(indent(1)."Set flag to install tightvnc");
                            $install_tightvnc=1;
                        }
					}
				} else {   
					if ($sw =~ /Viewer/i) {
						LogMsg(indent(1)."NOTICE: $sw not installed");                 
					} else {
						LogMsg(indent(1)."WARNING: $sw not installed");                 
						$warns++;   			
					}
				}			
			}
		}	
		foreach my $sw (sort keys(%acceptedSW_hash)) {
			if (grep /$sw/,@systeminfo) {
				my @s=grep /$sw/,@systeminfo;   
				chomp(@s);
				push (@software, "@s");    
			}
		} 
		# Check for unexpected software    
		foreach my $sw (sort keys(%unexpectedSW_hash)) {    
			if (grep /$sw/i,@systeminfo) {                    
				LogMsg(indent(1)."WARNING: $sw installed");           
				$warns++;                
			}
		}  	
	}
    # Check perl modules
    my $perl_module="c:/perl/site/lib/Net/SFTP/Foreign";
    if (-d $perl_module) {
    } else {
       LogMsg(indent(1)."NOTE: SFTP Perl module is not installed");
       #$warns++;
    }    
    $perl_module="c:/perl/site/lib/Device/Modem.pm";
    if (-f $perl_module) {
    } else {
       LogMsg(indent(1)."NOTE: Modem Perl module is not installed");
       #$warns++;
    }             
    $perl_module="c:/perl/site/lib/Win32/TieRegistry.pm";
    if (-f $perl_module) {
    } else {
       LogMsg(indent(1)."NOTE: TieRegistry Perl module is not installed");
       #$warns++;
    } 
    $perl_module="c:/perl/site/lib/Win32/SystemInfo.pm";
    if (-f $perl_module) {
    } else {
       LogMsg(indent(1)."NOTE: SystemInfo Perl module is not installed");
       #$warns++;
    }          
	unless ($OS eq "Win2000") {	
		# Check the version of Internet Explorer
		my $cmd="reg query \"HKLM\\SOFTWARE\\Microsoft\\Internet Explorer\\Version Vector\" ";    
		my @info=`$cmd`;  
		foreach my $i (@info) {
			chomp($i);        
			if ($i =~ /IE/) {
				if ($verbose) {
					LogMsg(indent(1)."$i");
				}
				my @tmp=split(/\s+/,$i);
				
				# The version of IE is at the end of this line            
				if (($tmp[$#tmp] =~ /^6/) || ($tmp[$#tmp] =~ /^7/)) {
					LogMsg(indent(1)."WARNING: Internet Explorer needs to be upgraded.");
					$warns++;                
				}
			}
		}        			
	}
	# Specific check for Mozilla FireFox and the version
	my $user1="All Users";
	my $user2="Administrator";
	my $user3="Villages";
	my $user="";
	my $firefox1="";
	my $firefox_ini="";
	#my $firefox_version=0;
	foreach $user ("$user1","$user2","$user3") {
		$firefox1="C:/Documents and Settings/$user/Application Data/Mozilla Firefox/firefox.exe";
		if (-f $firefox1) {
			# We found FireFox
			$firefox_ini="C:/Documents and Settings/$user/Application Data/Mozilla Firefox/application.ini";
			last;
		}
	}
	if ($firefox_ini) {
		LogMsg(indent(1)."Found $firefox_ini");
		open(INI,"$firefox_ini");
		my @ini_info=(<INI>);
		close INI;
		foreach my $i (@ini_info) {
			chomp($i);
			if ($i =~ /^Version/) {
				my @tmp=split(/=/,$i);
				my $version=$tmp[1];
				@tmp=split(/\./,$version);
				
				if ($tmp[0] < 20) {
					LogMsg(indent(1)."WARNING: Firefox version $version");
					$warns++;
				}
				push(@software,"  Mozilla Firefox $version");				
				LogMsg("System Info: Mozilla Firefox Version: $version");				
			}
		}
	} else {
		if ($found_firefox) {
			if ($firefox_version) {
				my @tmp=split(/\./,$firefox_version);
				
				if ($tmp[0] < 20) {
					LogMsg(indent(1)."WARNING: Firefox version $firefox_version");
					$warns++;
				}
				push(@software,"  Mozilla Firefox $firefox_version");
				LogMsg("System Info: Mozilla Firefox Version: $firefox_version");				
			} else {
				LogMsg(indent(1)."WARNING: FireFox is installed but failed to determine version");
				$warns++;
			}
		}
	}
	# Specific Check to find foxit and version
	 
	my $foxit="C:/Program Files/Foxit Reader/Foxit Reader.exe";	 
	my $foxit_alt="C:/Program Files/Foxit Software/Foxit Reader/Foxit Reader.exe";
	my $foxit_alt2="C:/Program Files/Foxit/Foxit Reader.exe";
	my $foxit_alt3="C:/Program Files/Foxit Software/Foxit Reader/FoxitReader.exe";
 
	unless (-f $foxit) {		 
		if (-f $foxit_alt) {	 
			$foxit=$foxit_alt;
		} elsif (-f $foxit_alt2) {	 
			$foxit=$foxit_alt2;
		} elsif (-f $foxit_alt3) {	 
			$foxit=$foxit_alt3;
		}		
	}
	if ($OS eq "WinXP") {
		if (-f $foxit)   {
			# Find the version 
			
			my $cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Foxit Software\\Foxit Reader\" /v Version";

			my @info=`$cmd`; 
			foreach my $i (@info) {			 
				if ($i =~ /Version/) {
					my @tmp=split(/\s+/,$i);
					my $version=$tmp[-1];
					LogMsg("System Info: Foxit Version: $version");
				}
			}
			# Check if it is the default
			my $found_foxit=0;
			my $found_pdf=0;
			$cmd="reg query \"HKLM\\Software\\Classes\" /s";
			@info=`$cmd`;  	
			foreach my $i (@info) {
				if ($i =~ /\.pdf/) {
					$found_pdf=1;
				}
			}	
			if ($found_pdf) {		
				$cmd="reg query \"HKLM\\Software\\Classes\\.pdf\" ";
				@info=`$cmd`;  	
				foreach my $i (@info) {			 
					chomp($i);
					if ($i =~ /FoxitReader.Document/i) {
						$found_foxit=1;
						LogMsg(indent(1)."Foxit appears to be the default for .pdf files for IE.");
					}				
				}	
				unless ($found_foxit) {
					if ($OS eq "WinXP") {
						LogMsg("WARNING: Foxit does not appear to be the default for .pdf files for IE.");
						$warns++;	
						my $local_reg="c:/temp/scripts/foxitreader.reg";
						if (-f $local_reg) {
							LogMsg("Updating the registry to make Foxit the default reader.");
							$cmd="regedit -s $local_reg";            
							`$cmd`;				
						} else {
							LogMsg("WARNING: Failed to find $local_reg");
						}
					}
				}
			} else {
				LogMsg("WARNING: Found no default for .pdf files for IE.");
				$warns++;				
			}			
		
			
		} else {
			LogMsg("WARNING: Failed to locate Foxit Reader.");
			$warns++;		
		}
	}
	
	if ($OS eq "Win7") { 
		if (-f $foxit)   {
			# Find the version 			
			my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Foxit Reader_is1\" /s";
			my @info=`$cmd`; 
			foreach my $i (@info) {			 
				if ($i =~ /DisplayVersion/) { 
					my @tmp=split(/\s+/,$i);
					my $version=$tmp[-1];
					LogMsg("System Info: Foxit Version: $version");
				}
			}
			# Check if it is the default
			my $found_foxit=0;
			my $found_pdf=0;
			$cmd="reg query \"HKLM\\Software\\Classes\" /s";
			@info=`$cmd`;  	
			foreach my $i (@info) {
				if ($i =~ /\.pdf/) {
					$found_pdf=1;
				}
			}	
			if ($found_pdf) {		
				$cmd="reg query \"HKLM\\Software\\Classes\\.pdf\" ";
				@info=`$cmd`;  	
				foreach my $i (@info) {			 
					chomp($i);
					if ($i =~ /FoxitReader.Document/i) {
						$found_foxit=1;
						LogMsg(indent(1)."Foxit appears to be the default for .pdf files for IE.");
					}				
				}	
				unless ($found_foxit) {
					if ($OS eq "WinXP") {
						LogMsg("WARNING: Foxit does not appear to be the default for .pdf files for IE.");
						$warns++;	
						my $local_reg="c:/temp/scripts/foxitreader.reg";
						if (-f $local_reg) {
							LogMsg("Updating the registry to make Foxit the default reader.");
							$cmd="regedit -s $local_reg";            
							`$cmd`;				
						} else {
							LogMsg("WARNING: Failed to find $local_reg");
						}
					}
				}
			} else {
				LogMsg("WARNING: Found no default for .pdf files for IE.");
				$warns++;				
			}			
		
			
		} else {		
			LogMsg("WARNING: Failed to locate Foxit Reader.");
			$warns++;		
		}		
	}	
 
    # Report Sofware Found
    foreach my $s (@software) {
		LogMsg("Software Found: $s");    
    }  	
    # Are there any items that need to be installed?
    if ($install_tightvnc) {
		LogMsg(indent(1)."Calling for installation of TightVNC");
        unless (install_tightvnc()) {
            LogMsg("WARNING: Error trying to install tightvnc");
            $warns++;
        }
    }	
	update_action_log($check,$warns);
    if ($warns) {
        LogMsg(indent(1)."WARNING: $warns software issues found");          
        $warning_count++;
    } else {
		$monitor_record_hash{$check}=$timeseconds;
	}	
  
}

sub install_kb {
	my $kb = shift;
	LogMsg("Installing $kb here...");
}

sub install_chrome {
    my $install_dir="//edna/temp/install/chrome";
	my $zip="Chrome.zip";
	my $source_zip="${install_dir}/${zip}";
 
    my $installed_file="C:/Program Files/Google/Chrome/Application/chrome.exe";
    my $preferences_file='Preferences';
	my $preferences_source="c:/temp/scripts/Preferences";
	my $preferences_location="C:/Documents and Settings/Villages/Local Settings/Application Data/Google/Chrome/User Data/Default";
	my $local_preferences="C:/Documents and Settings/Villages/Local Settings/Application Data/Google/Chrome/User Data/Default/Preferences";
	my $preferences_location_base="C:/Documents and Settings/Villages/Local Settings/Application Data";
	my $first_run="C:/Documents and Settings/Villages/Local Settings/Application Data/Google/Chrome/User Data/First Run";
	my @preferences_subs=("Google","Chrome","User Data","Default");
    my $installer='';
	my $local_install="c:/temp";
	my $local_zip="${local_install}/${zip}";
    my $local_installer="c:/temp/GoogleChromeStandaloneEnterprise.msi";
 
    if (-f $installed_file) {
        LogMsg("Found $installed_file already installed.");
        LogMsg("Installation aborted");
        return 0;
    }    
    if (-d $install_dir) {   
		 
		if (-f $source_zip) {
			# Copy the zip file to the local install folder
			LogMsg(indent(1)."Copying $source_zip to $local_zip...");
			copy("$source_zip","$local_zip");
			if (-f $local_zip) {
				unless (unzip($local_install,$zip)) {
					LogMsg(indent(1)."WARNING: Failed to unzip $zip file");
					return 0;					
				}
			} else {
				LogMsg(indent(1)."WARNING: Failed to copy $source_zip to $local_zip");
				return 0;
			}
		}
	}
 
    if (-d $local_install) { 	
    	opendir(SOURCE,"$local_install");
    	my @dir_list=readdir(SOURCE);
    	close SOURCE;   
        foreach my $d (@dir_list) {
            if ($d =~ /msi$/i) {
                $installer=$d;
            }            
               
        }
    }
    if ($installer) {

		
		if ($verbose) {
			LogMsg(indent(1)."Found $installer");
		}
 
        # Copy the installer to the local installer
       # copy("${install_dir}/${installer}","$local_installer");

        #my $cmd="${tightvnc_dir}/${installer} /passive /quiet";
        my $cmd="$local_installer /passive /quiet";
        
       
		if (-f $local_installer) {
			LogMsg("Installing $local_installer...");
			if ($verbose) {
				LogMsg(indent(1)."Found $local_installer");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $local_installer");
			return 0;
		}
 
        `$cmd`;

        # Check that it was indeed installed        
        if (-f $installed_file) {
            LogMsg("Found $installed_file");
			# Install the preferences
			# Check the directory is there:
			if (-d $preferences_location_base) {
				my $dir=$preferences_location_base;
				foreach my $s (@preferences_subs) {
					$dir.="/$s";
					unless (-d $dir) {
						LogMsg(indent(1)."Creating $dir");
						mkdir $dir;	
						unless (-d $dir) {						
							LogMsg(indent(2)."WARNING: Failed to create $dir");
							return 0;
						}
					}
				}
			}
 	
			# Copy the preferences file
			if (-f $preferences_source) {
				LogMsg(indent(1)."Installing preferences...");
				copy("$preferences_source","$local_preferences");
				print "copy $preferences_source to $local_preferences\n";
				if (-f "$local_preferences") {
					LogMsg(indent(2)."Successfully installed preferences");
				} else {
					LogMsg(indent(2)."WARNING: Failed to create preferences file");
				}				
			} else {
				LogMsg(indent(2)."WARNING: Cannot find preferences source file ($preferences_source)");			
			}
 
			# Create the first run file
			unless (-f $first_run) {
				open(FIRSTRUN,">$first_run");
				close FIRSTRUN;
				unless (-f $first_run) {
					LogMsg(indent(2)."WARNING: Failed to create $first_run");
					#return 0;				
				}
			}
			# Cleanup
			if (-f $local_installer) {
				LogMsg("Cleaning up...remove $local_installer");
				unlink $local_installer;
			}
		
 		
        } else {
            LogMsg("Error: Failed to find $installed_file after installation");
            return 0;
        }
    } else {
        LogMsg("Failed to locate an installer for Chrome");
        return 0;
    }
    return 1;
}

sub check_chrome_preferences {
	return;
	# Currently, this function is disabled - kdg
	my $local_preferences="C:/Documents and Settings/Villages/Local Settings/Application Data/Google/Chrome/User Data/Default/Preferences";	
	my $preferences="C:/Documents and Settings/Villages/Local Settings/Application Data/Google/Chrome/User Data/Default/Preferences";
	my $set_preference = 0;
	LogMsg("Checking preferences...");
	if (-f $local_preferences) {
		LogMsg(indent(1)."Checking $local_preferences...");
		# Current preference: Open only the Ten Thousand Villages Website
		my $home_all = '"urls_to_restore_on_startup": [ "http://edna/", "http://thewire.tenthousandvillages.com/user", "http://www.tenthousandvillages.com/" ]';
		my $home = '"urls_to_restore_on_startup": [ "http://www.tenthousandvillages.com/" ]';
		open(PREF,"$local_preferences");
		my @preference_info=(<PREF>);
		close PREF;
		
		foreach my $p (@preference_info) {
			chomp($p);
			if ($p =~ /urls_to_restore_on_startup/) {
				if ($p eq $home) {
					LogMsg(indent(1)."Preferences are set correctly");
				} else {
					LogMsg(indent(1)."Need to set preferences");
					$set_preference=1;
				}
			}
		}	
		if ($set_preference) {
			open(PREF,">$local_preferences");
			foreach my $p (@preference_info) {
				if ($p =~ /urls_to_restore_on_startup/) {
					# Replace current Home settings with our preference
					print PREF "$home\n";				
				} else {
					print PREF "$p";
				}
			}
		}
	} else {
		 
		LogMsg(indent(1)."WARNING: Failed to locate preferences.");
		 
		my $install_dir="//edna/temp/install/chrome";
		my $local_install="c:/temp";
		my $preferences_file="Preferences";	
		
		# Copy the preferences file
		LogMsg(indent(1)."Installing preferences...");
		if ( -f "${local_install}/${preferences_file}") {
			copy("${local_install}/${preferences_file}","$local_preferences");
			print "copy(${install_dir}/${preferences_file},$local_preferences)\n";
			if (-f "$local_preferences") {
				LogMsg(indent(2)."Successfully installed preferences");
			} else {
				LogMsg(indent(2)."WARNING: Failed to create preferences file");
			}
		} else {
			LogMsg(indent(2)."WARNING: Cannot find preferences source file");
		}
	}
}
sub check_viewer{
	my $app=shift;
	my $installed_file="C:/Program Files/Microsoft Office/Office12/${app}.exe";     
	my $installed_file_alt="C:/Program Files/Microsoft Office/Office11/${app}.exe"; 		 
	my $installer_enabled = 1;
 
	# Start installing in the new year
	if ($current_year == 2014) {
		if ($storenum =~ /^$day_of_week/) {			
			if ($hostname =~ /Reg1/i) {		 			
				$installer_enabled = 1;				
			}  
		}
	}	
	if ((-f $installed_file) || (-f $installed_file_alt)) {
		LogMsg(indent(1)."Found $app installed"); 
	} else {
		LogMsg(indent(1)."Notice: Did not find $app installed");
		#if (($storeStatus eq "Company") && ($installer_enabled)) {
		if ($installer_enabled) {
			LogMsg(indent(2)."Installing $app...");
			unless(install_microsoft_viewer($app)) {
				return 0;
			}
			if ((-f $installed_file) || (-f $installed_file_alt)) {
				LogMsg(indent(1)."$app installed successfully"); 			
			} else {
				LogMsg(indent(1)."WARNING: $app failed to install successfully"); 			
				$warning_count++;
				return 0;
			}
		} else {
			if ($verbose) {
				print "Store Status: $storeStatus\n";
				print "Installer enabled: $installer_enabled\n";
			}
		}
	}	
	return 1;
}

 

sub install_microsoft_viewer {
	my $app=shift;
	my $install_dir="//edna/temp/install/MicrosoftOfficeViewers";
	my $zip="MicrosoftOfficeViewers.zip";
	my $source_zip="${install_dir}/${zip}";
    my $installed_file="C:/Program Files/Microsoft Office/Office12/${app}.exe"; 
	my $installed_file_alt="C:/Program Files/Microsoft Office/Office11/${app}.exe"; 
    my $installer='ExcelViewer.exe';
	my $temp_install="c:/temp/install";
	my $local_install="c:/temp/install/MicrosoftViewers";
	my $local_zip="${local_install}/${zip}";
	my %installer_hash=(
		'XLVIEW' => 'ExcelViewer.exe',
		'PPTVIEW' => 'PowerPointViewer.exe',
		'WORDVIEW' => 'Wordview_en-us.exe',
		'WordConv' => 'FileFormatConverters.exe',
	);
    my $local_installer="${local_install}/${installer_hash{$app}}";
	unless (-d $temp_install) {
		mkdir $temp_install;
	}	
	unless (-d $temp_install) {
		LogMsg("WARNING: Failed to create $temp_install directory");
		$warning_count++;
		return 0;
	}	
	unless (-d $local_install) {
		mkdir $local_install;
	}
	unless (-d $local_install) {
		LogMsg("WARNING: Failed to create $local_install directory");
		$warning_count++;
		return 0;
	}	
 
    if ((-f $installed_file) || (-f $installed_file_alt)) {
        LogMsg("Found $installed_file already installed.");
        LogMsg("Installation aborted");
        return 0;
    }    
    if (-d $install_dir) {   
		if (-f $local_installer) {
			LogMsg(indent(1)."Found $local_installer");
		} else {
			if (-f $source_zip) {
				# Copy the zip file to the local install folder
				LogMsg(indent(1)."Copying $source_zip to $local_zip...");
				copy("$source_zip","$local_zip");
				if (-f $local_zip) {
					unless (unzip($local_install,$zip)) {
						LogMsg(indent(1)."WARNING: Failed to unzip $zip file");
						return 0;					
					}
				} else {
					LogMsg(indent(1)."WARNING: Failed to copy $source_zip to $local_zip");
					return 0;
				}
			} else {
				LogMsg(indent(1)."WARING: Failed to locate $source_zip");
				return 0;
			}
		}
	}
 
    if (-f $local_installer) {		
		if ($verbose) {
			LogMsg(indent(1)."Found $installer");
		} 
        my $cmd="$local_installer /passive /quiet /norestart";              
		if (-f $local_installer) {
			LogMsg("Installing $local_installer...");
			if ($verbose) {
				LogMsg(indent(1)."Found $local_installer");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $local_installer");
			return 0;
		}
 
        `$cmd`;

        # Check that it was indeed installed        
        if ((-f $installed_file) || (-f $installed_file_alt)) {
            LogMsg("Found $installed_file"); 	
			# Cleanup
			if (-f $local_zip) {
				LogMsg("Cleaning up...remove $local_zip");
				unlink $local_zip;
			}		
			# Create an reboot request flag
			#LogMsg(indent(2)."Setting $reboot_requested_flag");
			#open(FLAG, ">$reboot_requested_flag");
			#close FLAG;					
        } else {
            LogMsg("Error: Failed to find $installed_file after installation");
            return 0;
        }
    } else {
        LogMsg("Failed to locate an installer for $installer");
        return 0;
    }
    return 1;
}

sub unzip {
	my $dir=shift;
	my $file=shift;
	my $sourcefile="${dir}/${file}";
	use Archive::Zip;	
	if (-f $sourcefile) {
		LogMsg("Extracting $sourcefile...");
 
		# Need to unzip the source file
		 
		my $zip = Archive::Zip->new($sourcefile);
		my @members = $zip->members();		

		foreach my $member (@members) {
			my $filename = $member->fileName();         
			$filename = substr($filename, rindex($filename, "/")+1);
			next unless ($filename);
			LogMsg(indent(1)."extracting file ". $member->fileName() . " to ". "$filename");
			#$zip->extractMember($member, "$filename");
			if ($zip->extractMember($member, "$dir\\$filename")) {
			# The result codes are opposite what you might expect.
				LogMsg("ERROR: Failed to extract $filename");
				return 0;
			} else {					
				if (-f "$dir/$filename") {
					LogMsg("Extracted $filename");
				} else {
					LogMsg("WARNING: Failed to locate $filename in $dir after the extract!");
					return 0;
				}
			}        
		} 
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $sourcefile");
		return 0;
	}
    return 1; 	
}

sub install_tightvnc {
	unless ($OS eq "WinXP") {
		LogMsg("The install_tightvnc function does not support $OS");
		return;
	}
    my $tightvnc_dir="//edna/temp/install/tightvnc";
    my $installed_file="c:/program files/tightvnc/tvnserver.exe";
    my $reg_file='';
    my $installer='';
    my $local_installer="c:/temp/vnc.msi";
	my $local_reg="c:/temp/vnc.reg";
    if (-f $installed_file) {
        LogMsg("Found $installed_file already installed.");
        LogMsg("Installation aborted");
        return 0;
    }    
    if (-d $tightvnc_dir) {    	
    	opendir(VNC,"$tightvnc_dir");
    	my @dir_list=readdir(VNC);
    	close VNC;   
        foreach my $d (@dir_list) {
            if ($d =~ /^tightvnc.*32.*msi$/i) {
                $installer=$d;
            }            
            if ($d =~ /^tightvncserver.*reg$/i) {
                $reg_file=$d;
            }                  
        }
    }
    if ($installer) {
		if ($verbose) {
			LogMsg(indent(1)."Found $installer");
		}
		unless ($reg_file) {
			LogMsg(indent(1)."Warning: No reg file for TightVNC found");
			return 0;
		}
        # Copy the installer to the local installer
        copy("${tightvnc_dir}/${installer}","$local_installer");
        my $cmd="${tightvnc_dir}/${installer} /passive /quiet";
        $cmd="$local_installer /passive /quiet";
        
        LogMsg("Installing $local_installer...");
		if (-f $local_installer) {
			if ($verbose) {
				LogMsg(indent(1)."Found $local_installer");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $local_installer");
			return 0;
		}
        `$cmd`;

        # Check that it was indeed installed        
        if (-f $installed_file) {
            LogMsg("Found $installed_file");
            # Now install the registry file
			copy("${tightvnc_dir}/${reg_file}","$local_reg");
            LogMsg("Setting the registry for tightVncServer.");
            $cmd="regedit -s $local_reg";            
            `$cmd`;
			restart_service("tvnserver");	# The tightvnc service is stopped and started
			# Cleanup
			if (-f $local_installer) {
				LogMsg("Cleaning up...remove $local_installer");
				unlink $local_installer;
			}			
	        if (-f $local_reg) {
	            LogMsg("Cleaning up...remove $local_reg");
	            unlink $local_reg;
	        }			
        } else {
            LogMsg("Error: Failed to find $installed_file after installation");
            return 0;
        }
    } else {
        LogMsg("Failed to locate an installer for tightvnc");
        return 0;
    }
    return 1;
}

sub sep_install {
	unless ($OS eq "WinXP") {
		LogMsg("The sep_install function does not support $OS");
		return;
	}
	if (($no_action) || ($debug_mode)) {
		LogMsg("Skipping sep install as requested.");
		return 1;
	}
	 
	my $installer="//Edna/temp/install/SAVCorp/sep_upgrade.pl";
	my $reg_mod="//Edna/temp/install/reg/awhost32_exclusion.reg";
    my $local_installer="c:/temp/scripts/sep_upgrade.pl";
	my $local_reg_mod="c:/temp/scripts/awhost32_exclusion.reg";
	my $failed_flag="c:/temp/sep12_install_failed.txt";
	my $success_flag="c:/temp/sep12_installed.txt";	
 		

	if (-f $failed_flag) {
		LogMsg("WARNING: Detected the install has previously failed");
		return 0;
	}
	if (-f $success_flag) {
		LogMsg("WARNING: Detected the install has previously been completed.");
		# Need to reboot the register when we are finished.
		LogMsg("Setting flag to reboot");
		$need_to_reboot=1;		
		return 0;
	}	
    if (-f $reg_mod) {
		if ($verbose) {
			LogMsg(indent(1)."Found $reg_mod");
		}	
        # Copy the reg_mod to the local reg_mod
        copy("$reg_mod","$local_reg_mod");
		if (-f $local_reg_mod) {
			if ($verbose) {
				LogMsg(indent(1)."Found $local_reg_mod");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $local_reg_mod");
			return 0;
		}		
	} else {
		LogMsg(indent(1)."ERROR: Failed to locate $reg_mod");
		return 0;	
	}
		
 
    if (-f $installer) {
		if ($verbose) {
			LogMsg(indent(1)."Found $installer");
		}
		if (1) {	 # I thought this might be important but it does not appear to be
			# First need to stop pcAnywhere
			my $service="awhost32";
			if (stop_service($service)) {
				LogMsg(indent(1)."Stopped $service");
			} else {
				LogMsg("WARNING: Failed to stop $service");
				return 0;
			}
		}
        # Copy the installer to the local installer
        copy("$installer","$local_installer");
        my $cmd="$local_installer";
            
       
		if (-f $local_installer) {
			if ($verbose) {
				LogMsg(indent(1)."Found $local_installer");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $local_installer");
			return 0;
		}
		LogMsg("Installing $local_installer...");
        `$cmd`;
		LogMsg("Back from $local_installer...");
        # Check that it was indeed installed  
		if (-f $failed_flag) {
			LogMsg("WARNING: Detected the install has failed");
			return 0;
		} elsif (-f $success_flag) {
	 
			LogMsg("Detected the install has been completed.");	
			# Adjust the registry to exclude pcAnywhere			
			my $fixcmd = "regedit.exe /s $local_reg_mod";
			`$fixcmd`;
			
			# Check the DLL is in sync	
			unless (check_correct_S32()) {
				LogMsg("Need to sync symantec DLL's");
				if (check_correct_S32("correct")) {
					LogMsg("S32EVNT1.DLL looks good");
				} else {
					LogMsg("WARNING: S32EVNT1.DLL may not be correct");					 
				}		
			} else {
				LogMsg(indent(1)."Symantec DLL is in sync");
			}		
			# Check that the DLL is a good version
			if (check_correct_S32()) {
				LogMsg("S32EVNT1.DLL looks good");
			} else {
				LogMsg("WARNING: S32EVNT1.DLL may not be correct");					 
			}			
 
			# Need to reboot the register when we are finished.
			LogMsg("Setting flag to reboot");
			$need_to_reboot=1;			
 			
		} else {
			LogMsg(indent(1)."WARNING: Results of install are undetermined");			
		}		
			
		if (1) {
			# Start pcAnywhere
			my $service="awhost32";
			if (start_service($service)) {
				LogMsg(indent(1)."Started $service");
			} else {
				LogMsg("WARNING: Failed to start $service");			 
			}	
		}
 
    } else {
        LogMsg("Failed to locate $installer");
        return 0;
    }
		
    return 1;
}

sub restart_service {
	my $service=shift;
	stop_service($service);
	start_service($service);
}

sub stop_service_alt {
    my $service=shift;
	my $cmd="sc qc \"$service\"";	
    my @service_info=`$cmd`;    
    foreach my $s (@service_info) {
        if ($s =~ /The specified service does not exist/i) {
            return 1;
        }        
    }    
    #my $state=query_service($service);
	my $state=get_state($service); 
    my $count=15;
	$cmd="sc stop \"$service\"";
    unless  ($state =~ /STOPPED/) {
        print "Stopping $service...\n";
        print "Current state: $state\n";
		`$cmd`;
	}
	$state=query_service($service);
	while ($state !~ /STOPPED/) {
        sleep $count;
		$state=get_state($service); 
		#$state=query_service($service);
		print "Current state: $state\n";
        if ($count) {
            $count--;
        } else {
            print "Gave up trying to stop $service\n";
            return 0;
        }	
	}
    
    return 1;
}
sub start_service {
    my $service=shift;
    print "Starting $service...\n";
    my $cmd="sc start \"$service\"";  
	#$cmd="net start \"$service\""; 
 
    if ($verbose) {
        print "$cmd\n";
    }
	
    `$cmd`;
	if ($?) {
		LogMsg("Error running $cmd");
		LogMsg("$?");
	} else {
		LogMsg("Ran: $cmd");
	}
    my $return=1;
    my $pause = 4;
    my $count=0;
  
    my $state=get_state($service);    
    while (1) {            
        if ($state =~ /RUNNING/) {            
            last
        } elsif  ($count > 10) {
            $return=0;
            last
        }
        LogMsg("The service $service is $state");
        if ($state =~ /unknown/i) {
			$return=0;		
            last
        }		
        $count++;
        sleep $pause;
        sleep $count;        
        $state=get_state($service); 
    }  
    return $return;
}

sub get_state {
    my $service=shift;
    my $state="unknown";
    # Find the status of the service
    my $cmd="sc query \"$service\"";     
    
	my @result=`$cmd`;     
	
    foreach my $r (@result) {		
        if ($r =~ /STATE/) {              
            my @tmp=split(/\s+/,$r);
            $state=$tmp[-1];
        }  
    }    
    return $state;
}

sub query_service_alt {
    my $service=shift;       
    my $state="";
    my $attempt=0;
    my $querycmd="sc query \"$service\"";
    my @service_info=`$querycmd`;   
    my $found=0;
	foreach my $s (@service_info) {       
        if ($s =~ /SERVICE_NAME/) {
            if ($s =~ /$service/) {
                $found=1;
            }
        }
		if ($s =~ /STATE/)  {    
			my @tmp=split(/\s+/,$s);            
			chomp($state=$tmp[-1]);            
			return $state;
		}
	}
    if ($found) {
        return $state;
    } else {
        return "not_found";
    }
}

sub check_disk {
	if ($OS eq "Win2000") {
		return;
	}
	my $request=shift;
	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="disk";	
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {						
			unless (($force) || ($request)) {
				# We do not have to run this test again 
				LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
				return;
			}
		}
	}	
	# See how many drives are present
	LogMsg("Checking disk...");
	my %drive_hash=();
	my $cmd="wmic diskdrive list brief";
	my @disk_info=`$cmd`;
	foreach my $drive (@disk_info) {	
		my @data=split(/\s+/,$drive);		
		for my $d (@data) {			
			my $size;
			if ($d =~ /PHYSICAL/) {
				my @tmp=split(/\\/,$d);
				my $disk=$tmp[-1];	
				
				$size=$data[-1];
				if ($size =~ /\d/) {					
					$size=($size / 1024);
					$size=($size / 1024);
					$size=sprintf("%.2f",($size / 1024));
					$drive_hash{$disk}="${size}GB";
				}
			}					 		
		}
	}
	my $drive_count=keys(%drive_hash);	
	#LogMsg("Found $drive_count hard drives");
	LogMsg("System Info: DriveCount: $drive_count");
	foreach my $drive (sort keys(%drive_hash)) {		
		my $size=$drive_hash{$drive};
		LogMsg("System Info: $drive: $size");	
	}
	# Get defrag info
	$cmd="defrag c: -a -v";
	@disk_info=`$cmd`;
	my $tfrag=0;
	my $ffrag=0;

	foreach my $d (@disk_info) {	
		chomp($d);
		if ($d =~ /Total fragmentation .*=/) {
			my @tmp=split(/=/,$d);
			$tfrag=$tmp[-1];
			$tfrag=~ s/%//g;
			$tfrag=~ s/ //g;			
		}
		if ($d =~ /File fragmentation .*=/) {
			my @tmp=split(/=/,$d);
			$ffrag=$tmp[-1];
			$ffrag=~ s/%//g;
			$ffrag=~ s/ //g;			
		}		
		if ($d =~ /You should defragment/) {
			$need_to_defrag=1;			
		}
	}
	if ($tfrag > 20) {
		LogMsg("WARNING: Total Fragmentation for Drive is $tfrag percent");
		$warns++;
	}
	if ($ffrag > 50) {
		LogMsg("WARNING: File Fragmentation for Drive is $ffrag percent");
		$warns++;
	}	
	# Check for old Windows update folders that can be cleaned up
	my $dir="c:/windows";
	opendir(UPLOAD,"$dir");
	my @dir_list=readdir(UPLOAD);
	close UPLOAD;
	my $uninstall_dir_count=0;
	my $old_uninstall_dir_count=0;
	my $deleted_count=0;
	my $current_timeseconds=time();		
	my $enable_delete_uninstall_folder=1;	
 
	foreach my $d (@dir_list) {
		if ($d =~ /\$NtUninstall/) {
			$uninstall_dir_count++;
			my $dir_name="${dir}/$d";
			my @dir_info=stat($dir_name);
			my $dir_date=$dir_info[9];
			
			my @dir_date_info=localtime($dir_date); 
			my $directory_date = sprintf("%04d-%02d-%02d", $dir_date_info[5]+1900, $dir_date_info[4]+1, $dir_date_info[3]);										
			my $dir_age = (($current_timeseconds - $dir_info[9]) / 86400);				
			$dir_age = sprintf("%.2f", $dir_age);	
			if ($dir_age > 365) {	
				my $delete_dir="${dir}/${d}";								
				if ($enable_delete_uninstall_folder) {					
					if (-d $delete_dir) {						 	
						rmtree("$delete_dir");
						if (-d $delete_dir) {
							LogMsg(indent(1)."WARNING: Failed to delete $delete_dir.");
							$warns++;
						} else {
							LogMsg(indent(1)."$delete_dir has been removed.");
							$deleted_count++;

						}		
						# For now, there is a limit on how many directories can be deleted at once. 
						#if ($deleted_count > 49) {
						#	$enable_delete_uninstall_folder=0;						
						#}
					}
				}
				if (-d $delete_dir) {	
					$old_uninstall_dir_count++;					
				}
			}
		}
	}
	LogMsg("Found $uninstall_dir_count Uninstall Windows Updates directories in c:/Windows");
	if ($old_uninstall_dir_count) {
		LogMsg("WARNING: Found $old_uninstall_dir_count old Windows Update uninstall directories.");
		$warns++;
	}	
	if ($deleted_count) {
		LogMsg("NOTICE: $deleted_count old Uninstall Windows Update directories have been removed.");
	}
	
	unless ($need_to_defrag) {
		LogMsg("Found no need to run defrag");
	}
	update_action_log($check,$warns);
	if ($warns) {
		LogMsg("Found $warns issues checking disk");
		$warning_count++;
    } else {
		
		$monitor_record_hash{$check}=$timeseconds;
	}	
	$check_disk_ran=1; 
}

sub run_defrag {
	if (($no_action) || ($debug_mode)) {
		LogMsg("Skipping disk defrag as requested.");
		return;
	}	
	unless ($current_hour < 7) {
		LogMsg("Disk defrag would be here...");
		return;
	}
	LogMsg("Running disk defrag");
	my $warns=0;
	# The one concern I have is about running defrag if the system is also doing
	# a windows update.  How do I determine if a windows update is in session?
	# I think that the wuauclt.exe is seen to be running for both SYSTEM and 
	# the user when the windows updates are being downloaded.  So, I am going to
	# use this as a check and avoid running defrag if this is seen.  -kdg
	my $cmd="tasklist";
	my @taskinfo=`$cmd`;
	my $wuauclt_count=0;
	foreach my $t (@taskinfo) {
		if ($t =~ /wuauclt/) {
			$wuauclt_count++;
		}		
	}
	if ($wuauclt_count) {
		LogMsg("Not running disk defrag because windows updates may be running.");
		return 0;
	}
	my $enable_defrag=0;	
 
	 
	$enable_defrag=1;
	#}
	unless ($enable_defrag) {
		LogMsg("WARNING: regmonitor.pl wants to run a defrag but it is disabled");
		return;
	}
	LogMsg("Running Defrag...");

	
	$cmd="defrag c:";
	my $defrag_completed=0;
	my $fragmented=0;
	my @disk_info=`$cmd`;
	foreach my $d (@disk_info) {	
		chomp($d);
		LogMsg("$d");
		if ($d =~ /Defragmentation Report/ ) {
			$defrag_completed=1;
		}
		if ($defrag_completed) {
			if ($d =~ /Fragmented/) {
				my @tmp=split(/\s+/,$d);
				foreach (my $x=0; $x<=$#tmp; $x++) {
					my $word=$tmp[$x];
					if ($word =~ /Fragmented/) {
						# When iterating through the line, the
						# percent of fragmentation is the found
						# just before the word "Fragmented."
						$fragmented=$tmp[($x-1)];
					}
				}
			}
		}
	}
	# Now run the defrag check again
	$cmd="defrag c: -a";
	@disk_info=`$cmd`;
	my $tfrag=0;
	my $ffrag=0;
	foreach my $d (@disk_info) {	
		chomp($d); 	
		if ($d =~ /You should defragment/) {
			if ($defrag_completed) {
				# We have just run a defrag and the system is still saying we should 
				# run a defrag.
				LogMsg("WARNING: Ran defrag but drive is still $fragmented fragmented.");
				$warns++;			
			}
				 
		}
		if ($d =~ /You do not need to defragment/) {
			LogMsg("Defragment was successful");
		}
	}	
	if ($warns) {
		LogMsg("WARNING: Found $warns issues defragmenting volume");
		$warning_count++;	
	} 
	LogMsg("NOTICE: Disk Defragment Task complete");
	
}

sub run_cleanup {
	if (($no_action) || ($debug_mode)) {
		LogMsg("Skipping disk cleanup as requested.");
		return;
	}
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="diskcleanup";	
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");		
			unless ($force)  {return;}
		}
	}		
 
	LogMsg("Running disk cleanup"); 
	my $run_alt_cleaner = 0;
	if (1) {
		# Run cCleaner
 
		my $tool="c:/Program Files/cCleaner/cCleaner.exe";
		if (-f $tool) {
			LogMsg(indent(1)."Running cCleaner...");
			my $cmd="\"$tool\" /AUTO";
			`$cmd`;
			if ($?) {
				LogMsg("WARNING: possible error running $cmd");  
				$warns++;        
			} else { 
				LogMsg(indent(1)."NOTICE: Ran cCleaner");
			}
		} else {
			LogMsg(indent(1)."WARNING: Did not locate cCleaner.exe");
			$warns++;
			$run_alt_cleaner++;
		}
	}
	if ($run_alt_cleaner) {
		# Run the Windows Cleanup tool
		LogMsg(indent(1)."Running Windows Cleanup tool...");
		my $cmd="cleanmgr /sagerun:0";
		`$cmd`;
	}	
	update_action_log($check,$warns);
	if ($warns) {
		LogMsg("Found $warns issues cleaning up volume");
		$warning_count++;	
    } else {	 
		$monitor_record_hash{$check}=$timeseconds;
	}	
	LogMsg("Disk Cleanup Task complete");	 
	
}
	
sub check_memory {
	if ($OS eq "Win2000") {
		return;
	}
	my $warns=0;
	# Checking memory from systeminfo
	LogMsg("Getting memory...");
	my @meminfo=grep/Memory/,`systeminfo`;
	my $total_mem=0;
	my $tpm=0;	
	foreach my $m (@meminfo) {	
		chomp($m);
		LogMsg(indent(1)."$m");
		if ($m =~ /Total Physical Memory/) {
			my @tmp=split(/\s+/,$m);			
			$total_mem="$tmp[3]"."$tmp[4]";
			$tpm=$tmp[-2];
		}
	}	
	if ($total_mem) {
		LogMsg("System Info: TotalPhyMemory: $total_mem");	
	}
	LogMsg("Getting info from tasklist...");
	@meminfo=`tasklist`;
	my $total_mem_used=0;
	my $proc_count=0;
	my $target="=";
	my $target_found=0;
	my $pos_size=0;
	my $pos_vm_size=0;	
	my $eft_size=0;
	my $eft_vm_size=0;	
	foreach my $m (@meminfo) {	
		chomp($m);
		if ($target_found) {
			my @tmp=split(/\s+/,$m);
			my $mem=$tmp[-2];
			my $label=$tmp[-1];
			my $name=$tmp[0];
			
			$mem=~ s/,//g;
			if ($label eq "K") {
				$total_mem_used+=$mem;
				$proc_count++;
			} else {
				LogMsg(indent(1)."WARNING: Don't recognize memory label $label");
				LogMsg("$m");
				$warning_count++
			}
			if ($name =~ /posw/) {
				$pos_size=$mem;
			}
			if ($name =~ /EFTDeviceDriver/) {
				$eft_size=$mem;
			}			
			#LogMsg(indent(1)."$m");
		}
		if ($m =~ /$target/) {
			$target_found=1;
		}		
	}	
	$total_mem_used=sprintf("%.2f",($total_mem_used / 1024));
	LogMsg(indent(1)."Total Memory Used by $proc_count Processes: $total_mem_used MB");
	
	if ($pos_size > 40000) {
		LogMsg("WARNING: POS is $pos_size K");
		$warns++;
		$restart_pos = 1;	 
		LogMsg(indent(1)."WARNING: Set flag to request a pos restart due to memory usage");
		$warns+=update_flag_log("Set flag to request a pos restart due to memory usage");
		LogMsg(indent(1)."System Stats: Restart POS MemoryUsage: 1");
	} else {
		LogMsg(indent(1)."POS is $pos_size K");
	}
	if ($eft_size) {
		LogMsg(indent(1)."EFT is $eft_size K");
	}
 
	if ($pslist_safe) {
 
		# Check virtual memory
		@meminfo=`pslist -m`;
		foreach my $m (@meminfo) {	
			chomp($m);
			if ($m =~ /posw/) {
				my @tmp=split(/\s+/,$m);
				$pos_vm_size=$tmp[5];
			}
			if ($m =~ /EFTDeviceDr/) {
				my @tmp=split(/\s+/,$m);
				$eft_vm_size=$tmp[5];
			}			
		}		
		
		if ($pos_vm_size > 99000) {
			#LogMsg("WARNING: POS Virtual Memory is $pos_vm_size K");	Seems no reason to warn about this since it is fairly routine.
			#$warns++;
			$restart_pos = 1;	 
			#LogMsg(indent(1)."WARNING: Set flag to request a pos restart due to virtual memory usage");
			$warns+=update_flag_log("Set flag to request a pos restart due to virtual memory usage");			
			LogMsg(indent(1)."System Stats: Restart POS VMemoryUsage: 1");
		} else {
			LogMsg(indent(1)."POS Virtual Memory is $pos_vm_size K");
		}
		if ($eft_vm_size) {
			LogMsg(indent(1)."EFT Virtual Memory is $eft_vm_size K");
		}
	}
 

	#####
	#	Check Advanced Performance Options
	#####
	my $cmd="reg query \"HKLM\\System\\CurrentControlSet\\Control\\PriorityControl\"";
	
    my @info=`$cmd`; 
	
	my %value_label_hash=(
		'undetermined' => 'undetermined',
		'0x26' => 'BackGround Services',
		'0x18' => 'BackGround Services',
		'0x38' => 'Programs',
		'0x2' => 'Programs'
	);
	my $value="undetermined";
	my $value_label=$value_label_hash{$value};
    foreach my $i (@info) {
        chomp($i); 
		next unless (defined($i));
		if ($i =~ /PrioritySeparation/) {
			my @tmp=split(/\s+/,$i);
			$value=$tmp[-1];
			if ($verbose) {
				print "Priority Value: ($value)\n";
			}
			unless ($value_label=$value_label_hash{$value}) {
				$value="undetermined";
				$value_label=$value_label_hash{$value};			
			}			
		}  
    }	
	 
	#LogMsg(indent(1)."Peformance Options: $value_label ($value)");
	LogMsg("System Info - Peformance Options: $value_label ($value)");    
	if ($OS eq "WinXP") {
		if (check_pagefile("check",$tpm)) {
			LogMsg("Adjusting pagefile...");
			check_pagefile("adjust",$tpm);
			check_pagefile("report",$tpm);
		}		

	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking memory");
		$warning_count++;
	}
	LogMsg("Finished Checking Memory");				
 
}

sub check_pagefile {

	# Determine whether the page file is managed by Windows or specifically set
	# Directive will control the behavior.  There are three options:
	# check - Will simply check whether the settings are acceptable.
	# adjust - Will adjust the settings to acceptable values.
	# report - Will check whether the settings are accpetable and report if they are not.
	my $directive=shift;
	my $tpm=shift;
	# 
    my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management\"";    
    my @info=`$cmd`;  
	my $page_file_setting;
	my $needs_adjustment=0;
	my $page_file_adjust_enabled=0;
	my $warns=0;
 
	if ($directive eq "adjust") {
		unless ($force) {
			if ($hostname =~ /Reg/i) {
				#if ($storenum =~ /^4/) {
					if ( ($day_of_week > 0) && ($day_of_week < 6) ) {			 
						if ($current_hour < 7) {							
							$page_file_adjust_enabled=1; 
						}
					}		
				#}
			}
		} else {
			$page_file_adjust_enabled=1;
		}
		if ($page_file_adjust_enabled) {
			LogMsg(indent(1)."Adjustment of the page file is currently ENABLED for this system."); 
		} else {
			LogMsg(indent(1)."Adjustment of the page file is currently DISABLED for this system.");					 
		}
	}

	foreach my $i (@info) { 
		if ($i =~ /PagingFiles/i) {
			my @tmp=split(/\s+/,$i);
			my $start=$tmp[-2];
			my $final=$tmp[-1];
			my $location=lc($tmp[-3]);
			unless ($location eq 'c:\pagefile.sys') {
				if ($page_file_adjust_enabled) {
					LogMsg("WARNING: Adjustment has been disabled because page file is $location");
					$warns++;
					$page_file_adjust_enabled=0;
				}
			}			
			@tmp=split(/\\/,$final);
			$final=$tmp[0];
 
			if ($final eq "0") {
				$page_file_setting = "Windows Managed";
			} else {
				$tpm=~ s/,//g;
				$page_file_setting = "Set: $start $final";
				my $preferred=sprintf("%d",($tpm * 1.5));
				if ($preferred > 4096) {
					$preferred = 4096;
				}
				unless (($preferred < $start) || ($preferred == $start))  {
					if ($directive eq "report") {			
						LogMsg("WARNING: Page file minimum is $start but should be $preferred");
						
					}				
					$warns++;
					$needs_adjustment=1;
				} else {
					LogMsg("Page file minimum is $start - Preferred: $preferred");
				}
				my $preferred_final=($preferred * 2);
				if ($preferred_final > 4096) {
					$preferred_final = 4096;
				}				
				if ($final < $preferred_final) {
					my $diff=($preferred_final - $final);
					my $percent=(($diff / $preferred_final) * 100); 
					if ($percent > 5) {
						if ($directive eq "report") {	
							LogMsg("WARNING: Page file limit is $final but should be $preferred_final - pf0");								
						}
						$warns++;
						$needs_adjustment=1;
					} else {
						LogMsg("Page file limit is $final - Preferred: $preferred_final - pf1");
					}				
				} else {
					LogMsg("Page file limit is $final - Preferred: $preferred_final - pf2");
				}
				if (($page_file_adjust_enabled) && ($needs_adjustment)){ 			
					if (adjust_pagefile("$preferred")) {
						# Need to reboot the register when we are finished.
						LogMsg("Setting flag to reboot");
						$need_to_reboot=1;	
					} else {
						LogMsg("WARNING: Failed to adjust pagefile");
						$warns++;
					} 
				}				
			}						
		}		
	}	
	if ($directive eq "check") {	
		LogMsg("Page File Setting: $page_file_setting");
	}
	return $warns;
}
 

sub adjust_pagefile {
	# I could not find a reliable way to set the page file size so simply set
	# it to System Managed with this reg file:
	my $start=shift;
	if (($start > 3056) && ($start < 3059)) {
		#my $fixfile="c:/temp/scripts/PF_SystemManaged.reg";
		my $fixfile="c:/temp/scripts/MemoryManagementPF3058.reg";
		if (-f $fixfile) {			 
			my $cmd="regedit.exe -s $fixfile";
			LogMsg("Running $cmd");
			`$cmd`;
			return 1;
		} else {
			LogMsg("WARNING: Unable to locate $fixfile"); 
		}	
	
	} else {
		LogMsg("WARNING: No fix file for start of $start");
		return 0;
	}
	return 0;
}

sub check_services {
	if ($OS eq "Win2000") {
		return;
	}

	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="services";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless (((($timeseconds - $last_pass)/86400) > $freq) || ($services_only)) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			return;
		}
	}	
	LogMsg("Checking services ...");	

    # Check that the services that typically run for the store's server are installed but are not running
    my @service_info=`sc query state= all`;
    my $service;
    foreach my $line (@service_info) {
        chomp($line);
        if ($line =~ /^SERVICE_NAME:/) {
            my @tmp=split(/:/,$line);
            $service=lc($tmp[1]);
            $service=~s/ //g;            
            foreach my $key (sort keys(%services_hash)) {
                # I am using the variable "name" to match the service so that I don't change the variable "key"
                my $name=lc($key);
                $name=~s/ //g;                
                if ("$service" eq "$name") {                    
                    $services_hash{$key}=1;
                }
            }            
        }
    }
    foreach $service (sort keys(%services_hash)) {
        if ($services_hash{$service}) {
            #LogMsg("Found service $service"); 
			LogMsg("System Info: Service $service: Present"); 
        } else {
			LogMsg("System Info: Service $service: Not_Found"); 
            #LogMsg("WARNING: Failed to find service $service installed");
            #$warning_count++;        
        }
    }  
	# Check that tightVNC is configured as a service
	my $vnc_service=check_vnc_services();
	if ($vnc_service) {
		LogMsg("System Info: VNC Service: $vnc_service");	
	} else {
		LogMsg(indent(1)."No VNC service configured");
		$warns+=create_tvnserver_service();
		$vnc_service=check_vnc_services();
		
		if ($vnc_service) {
			LogMsg("Successfully created tvnserver - Restarting service...");
			restart_service("tvnserver");	# The tightvnc service is stopped and started
		} else {
			LogMsg("WARNING: Failed to configure TightVNC service ");
			LogMsg("WARNING: No VNC service configured");
			$warns++;
		}
	}
	if ($warns) {
		LogMsg("WARNING: $warns issues detected checking services");
		$warning_count++;
	} else {
		LogMsg("No issues found checking services");
	}
	
	update_action_log($check,$warns);
	$monitor_record_hash{$check}=$timeseconds;	
}

sub check_vnc_services {	
	my $vnc_service=0;
	my $cmd="sc qc tvnserver";
	my @info=`$cmd`;
	foreach my $i (@info) {
		if (($i =~ /START_TYPE/) && ($i =~ /AUTO_START/)) {
			$vnc_service="TightVNC";
		}		
	}
	$cmd="sc qc uvnc_service";
	@info=`$cmd`;
	foreach my $i (@info) {
		if (($i =~ /START_TYPE/) && ($i =~ /AUTO_START/)) {
			$vnc_service="UltraVNC";
		}		
	}
	return $vnc_service;
}

sub create_tvnserver_service {
	my $warns=0;
	LogMsg("Creating tvnserver service");
	my $app="C:/Program Files/TightVNC/tvnserver.exe";
	if (-f $app) { 
		my $cmd="\"$app\" -install -silent";		
		my @info=`$cmd`;
		if ($?) {
			LogMsg("ERROR: Error detected creating tvnserver service");
			$warns++;
		} else {
			LogMsg("Created tvnserver service");
		}
 
	} else {
		LogMsg("WARNING: Failed to locate $app");
		$warns++;
	}
	return $warns;

}

sub check_scripts {
	return;	# Currently disabled - I never finished this functionality
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="scripts";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			return;
		}
	}	
	LogMsg("Checking scripts...");
	# Get the checksum of the important files and scripts and save them in a file.  The monitor
	# script that runs on Edna will eventually consult this file and determine if any files need to be 
	# copied over to the register.
	my @folder_list=("c:/program files/apache group/apache/cgi-bin","c:/mlink/akron","c:/program files/apache group/apache/htdocs/pdf");
	my $manifest="c:/temp/install/scripts/reg_manifest.txt";
	my @master_sum_listing;
	# Make certain that the install/scripts folder exists
	my $start_dir=cwd();	
	if (-d "/temp") {
		chdir("/temp");
		unless (-d "install") {
			mkdir("install");
		}
		if (-d "install") {
			chdir("install");
			unless (-d "scripts") {
				mkdir("scripts");
				unless (-d "scripts") {
					LogMsg("WARNING: Failed to create install folder in temp.");
					return;
				}
			}	
		} else {
			LogMsg("WARNING: Failed to create install folder in temp.");
			return;
		}
	}
	chdir($start_dir);

	foreach my $f (@folder_list) {		
		if (-d $f) {
			# Get a listing of all files in this folder
			unless (opendir(INPUT,"$f")) {
				LogMsg("Failed to read directory $f");
				next;
			}
			my @dir_listing=readdir INPUT;
			closedir INPUT;
			my @sub_dir_listing;	# Some folders will have sub folders that we will need to get sums for as well
			# Get the md5 sum of all of the file in this folder
			chdir($f);
			LogMsg("Getting sums in $f");
			foreach my $d (@dir_listing) {			
				next if ($d eq ".");
				next if ($d eq "..");
				if (-d $d) {
					# This is a sub folder
					push(@sub_dir_listing,$d);
				} elsif (-f $d) {
					# This is a file so save the sum
					my $cmd = "md5 $d";
					my @sum_info=`$cmd`;
					push(@master_sum_listing,@sum_info);					
				}
			}
			if ($f =~ /pdf/) {
				# The pdf folder has sub folders we need to get sums for
				foreach my $sf (@sub_dir_listing) {						
					LogMsg("Getting sums in $sf");				
					chdir($sf);
					my $cmd = "md5 *";
					my @sum_info=`$cmd`;
					push(@master_sum_listing,@sum_info);					
					chdir($start_dir);
					chdir($f);				
				}				
			}			
			chdir($start_dir);
		} else {
			LogMsg("Failed to find $f as a directory.");
		}
	}
	chdir($start_dir);	
	# Now save the master sum listing
	open(OUTPUT,">$manifest");
	foreach my $m (@master_sum_listing) {
		print OUTPUT "$m";		
	}
	close OUTPUT;
	update_action_log($check,$warns);
	$monitor_record_hash{$check}=$timeseconds;		
}

sub check_registry_win7 {
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=30;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="registry";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days=(($timeseconds - $last_pass)/86400);
		unless ($days > $freq) {
			# We do not have to run this test again 
			if ($verbose) {				
				print "Days: $days is less than $freq - skipping $check check\n";
			}
			unless ($force) {return;}
		}
	}		
	LogMsg("Checking registry for various settings...");
	# Make certain that the install/scripts folder exists
	my $start_dir=cwd();
	my $script_dir="/temp/scripts";	
	if (-d "$script_dir") {
		chdir("$script_dir");
	} else {
		LogMsg("WARNING: Failed to locate $script_dir");
		$warning_count++;
		return;
	}	
	# Check sysinternals
	my $local_reg = "${script_dir}/hp_sysinternals.reg";
	if (-f $local_reg) {
		# Read the current settings in the .reg file
		my %expected_setting_hash=();
		my %found_setting_hash=();
		my $need_to_update=0;
		open (INPUT,"$local_reg");
		my @input_info=(<INPUT>);
		close INPUT;
		for (my $x=0; $x<=$#input_info; $x++) { 
			my $line=$input_info[$x];
			my $next_line=$input_info[($x + 1)];
			my $key;
			 
			if ($line =~ /S.y.s.i.n.t.e.r.n.a.l.s/) {
				my @tmp=split(/\\/,$line);
				$key=$tmp[-1];
				$key =~ s/\W//g;
				if ($next_line =~ /E.u.l.a.A.c.c.e.p.t.e.d/) {
					if ($next_line =~ /0.0.0.0.0.0.0.1/) {
						$expected_setting_hash{$key}=1;
					} 
				}
			} 
		}
 	
		my $cmd="reg query \"HKCU\\Software\\Sysinternals\" /s";
		my @info=`$cmd`;  	
			
		unless ($?) {
			for (my $x=0; $x<=$#info; $x++) { 
				my $line=$info[$x];
				my $next_line=$info[($x + 1)]; 
				my $key;				
				chomp($line);				 			
				if ($line =~ /Sysinternals/) {
					my @tmp=split(/\\/,$line);
					$key=$tmp[-1];
					$key =~ s/\W//g;					
					if ($next_line =~ /EulaAccepted/) {						
						if ($next_line =~ /0x1/) {
							$found_setting_hash{$key}=1;							
						} 					
					}
				} 	
			}
		}
		foreach my $key (sort keys(%expected_setting_hash)) {
			unless ($found_setting_hash{$key}) {
				$need_to_update=1;
				LogMsg("Eula Not Yet Accepted for $key");
				LogMsg("Installing EulaAccepted to $key");	 
				$cmd="reg add \"HKCU\\Software\\Sysinternals\\$key\" /v EulaAccepted /t REG_DWORD /d 1 /f";					           
				my @r=`$cmd`;	
				unless ($r[0] =~ /The operation completed successfully/) {
					LogMsg("WARNING: $[0]");
					$warns++;
				}							
			}
		}
		unless ($need_to_update) {
			LogMsg("EulaAccepted for SysInternals");
		}
		
	} else {
		LogMsg("WARNING: Failed to find $local_reg");
		$warns++;
	}	
	# Check windows backup settings
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsBackup\" /s";
	my @info=`$cmd`;  	
		
	unless ($?) {
		for (my $x=0; $x<=$#info; $x++) { 
			my $line=$info[$x];
 			
			chomp($line);	
			if ($line =~ /LastBackupDrive/) {
				my @tmp=split(/\s+/,$line);
				my $setting=$tmp[-1];
				unless ($setting eq "D:") {
					LogMsg("WARNING: Backup are configured for $setting rather than D: as expected");
					$warns++;
				}
			}
			
		}
	}	
 
	###
	# Check store manager settings
	###
	$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\SAP\\Retail Systems\\Store Manager\"";		
	@info=`$cmd`; 
 
	my $path='';
	if ($?) {
		LogMsg("WARNING: Could not find Store Manager Settings in the registry");  
		$warns++;        
	} else {            
		foreach my $i (@info) {
			chomp($i);  
			if ($i =~ /POS Server Path/i) {
				my @tmp=split(/REG_SZ/,$i);
				$path=$tmp[-1];
				while ($path =~ /^ /) {
					$path =~ s/^ //;
				}				 
			}
		}
	}
	if ($path) {
		if ($register_server) {
			my $expected="C:\\Program Files\\SAP\\Retail Systems\\Xpress Server";
			if ($path eq $expected) {
				LogMsg("Found the correct path for Store Manager");
			} else {
				LogMsg("WARNING: The path for Store Manager in the registry may be incorrect.");
				LogMsg("Path Found: ($path)");
				LogMsg("Path Expected: ($expected)");
				$warns++;
				$warns+=set_store_manager_path("server");
			}
		} else {
			my $expected="\\\\Edna\\XPS\\";
			if ($path eq $expected) {
				LogMsg("Found the correct path for Store Manager");
			} else {
				LogMsg("WARNING: The path for Store Manager in the registry may be incorrect.");
				LogMsg("Path Found: ($path)");
				LogMsg("Path Expected: ($expected)");				
				$warns++;
				$warns+=set_store_manager_path("register");
			}			
		}
	} else {
		LogMsg("WARNING: Failed to determine path for Store Manager");
		$warns++;
	}
 
	
	LogMsg("There have been $warns issues found checking the registry");
	update_action_log($check,$warns);
	if ($warns) {
		$warning_count++;
	} else {		
		$monitor_record_hash{$check}=$timeseconds;		
	}
 	chdir($start_dir);		
}

sub set_store_manager_path {
	my $request=shift;
	my $location="\"HKEY_LOCAL_MACHINE\\SOFTWARE\\SAP\\Retail Systems\\Store Manager\"";
	my $setting=0;
	my $warns=0;
	if ($request eq "server") {		
		$setting="C:\\Program Files\\SAP\\Retail Systems\\Xpress Server";
	} elsif ($request eq "register") {
		$setting="\\\\Edna\\XPS\\";
	} else {
		LogMsg("ERROR: Failed to understand request ($request)");
		$warns++
	}
	if ($setting) {
		# The fix is made here
		my $cmd="reg add $location /v \"POS Server Path\" /t REG_SZ /d $setting /f";		
		system("$cmd");	 
		if ($?) {
			LogMsg(indent(1)."WARNING: Failed to set path for Store Manager");  
			$warns++;        
		} else {  
			LogMsg(indent(1)."Set path for Store Manager.");
		}
	
	}
	return $warns;
	
}

sub check_registry {
	if ($OS eq "Win2000") {
		return;
	}
	if ($OS eq "Win7") {
		check_registry_win7();		
		return;
	}	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=30;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="registry";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days=(($timeseconds - $last_pass)/86400);
		unless ($days > $freq) {
			# We do not have to run this test again 
			if ($verbose) {				
				print "Days: $days is less than $freq - skipping $check check\n";
			}
			unless ($force) {return;}
		}
	}		
	LogMsg("Checking registry for various settings...");
	# Check auto-generated quick scan
	# Make certain that the install/scripts folder exists
	my $start_dir=cwd();
	my $script_dir="/temp/scripts";	
	if (-d "$script_dir") {
		chdir("$script_dir");
	} else {
		LogMsg("WARNING: Failed to locate $script_dir");
	}
 
	# Checking quickscan for other users
	 	
	###
	# Search other users
	###
	my $cmd;
	my @info;

	# Check a pcAnywhere setting (I have seen this incorrect and want to catch any such instances);
	my $found_symantec=0;
	my $found_pcAnywhere=0;	
 
	my $pcAnywhere="C:/Documents and Settings/All Users/Application Data/Symantec/pcAnywhere/Hosts";
	if (-d $pcAnywhere) {
		$found_pcAnywhere = 1;		
	}
	my $connection;
	my $autostart;
	if ($found_pcAnywhere) {
		$cmd="reg query \"HKLM\\Software\\Symantec\\pcAnywhere\\CurrentVersion\\Host\" /s";
	    @info=`$cmd`;  	
		
	    unless ($?) {
			foreach my $i (@info) {	   
				chomp($i);
				if ($i =~ /Connection/i) {
					my @tmp=split(/REG_SZ/,$i);
					$connection = $tmp[1];
					$connection =~ s/\"//g;
				}
				if ($i =~ /AutoStart/i) {
					my @tmp=split(/REG_SZ/,$i);
					$autostart = $tmp[1];										
				}				
			}
	    }	
		unless ($connection eq $autostart) {
			if ($verbose) {
				LogMsg("Connection: ($connection)");
				LogMsg("AutoStart : ($autostart)");
			}
			LogMsg(indent(1)."WARNING: May be a problem restarting pcAnywhere");
			$warns++;
		}		
	}
	# Check cCleaner setting for Memory Dumps
	LogMsg(indent(1)."Checking cCleaner settings");
	foreach my $user ("Administrator","Villages","HKEY_CURRENT_USER") {		
		my $expected_setting="False";
		my $need_to_fix=0;	
		my $found_setting=0;	
		my $uid = get_user("$user");		
		my @tmp=split(/\\/,$uid);
		$uid=$tmp[1];	
		if ($user eq "HKEY_CURRENT_USER") {			
			$uid = $user;
		}
		if ($verbose) {
			LogMsg("User: $user - UID: $uid");
		}
		if ($uid) {	 
			my $hcu_software="\"HKEY_USERS\\$uid\\Software";	
			if ($user eq "HKEY_CURRENT_USER") {
				$hcu_software="\"HKEY_CURRENT_USER\\Software";	
			}
			$cmd="reg query $hcu_software";					
			my $location = '';
			@info=`$cmd`; 				
			if ($?) {		
				LogMsg("WARNING: Could not find CCleaner Settings in the registry at $hcu_software for $user");  
				$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 						
				if ($i =~ /VB and VBA Program Settings/) {						
					unless ($location =~ /Piriform.*CCleaner/) {
						# Give priority to the Piriform location.  The VB and VBA location may be residual from a previous installation
						$location="\"HKEY_USERS\\$uid\\Software\\VB and VBA Program Settings\\CCleaner\\Options\"";
						$location="$hcu_software\\VB and VBA Program Settings\\CCleaner\\Options\"";												
					}
				}	
				if ($i =~ /Piriform/) {		 
					$location="\"HKEY_USERS\\$uid\\Software\\Piriform\\CCleaner\"";
					$location="$hcu_software\\Piriform\\CCleaner\"";										
				}				
			}					
			if ($location) {
				$cmd="reg query $location";	
				@info=`$cmd`; 					
				if ($?) {		
					LogMsg("WARNING: Could not find CCleaner Settings in the registry at $location");  
					$warns++;        
				} else {    				 
					foreach my $i (@info) {
						chomp($i);  						 
						if ($i =~ /Memory Dumps/i) {  							
							$found_setting=1;					
							my @tmp=split(/\s+/,$i);
							my $setting=$tmp[-1];
							unless ($setting eq $expected_setting) {
								LogMsg(indent(1)."Need to change the cCleaner settings for clearing Memory Dumps");
								$need_to_fix=1;
							}
						}
     
					}
					unless ($found_setting) {
						# If the setting is not found, we need to set it.
						$need_to_fix=1;
					}
				}
			} else {				
				my $show_warning = 1;
				# I have trouble getting this to work in Nashville on Reg2 so I am explicitly excluding it
				if ($storenum == 2793) {
					if ($hostname =~ /reg2/i) {
						if ($user eq "Administrator") {
							$show_warning = 0;
						}
					}
				}	
				if ($show_warning) {
					LogMsg(indent(1)."WARNING: Failed to locate cCleaner settings in the registry for the $user");
					$warns++;
				} else {
					LogMsg(indent(1)."Known Issue: Failed to locate cCleaner settings in the registry for the $user");
				}
			}		
			if (1) {
				if ($need_to_fix) {
					
					# The fix is made here
					$cmd="reg add $location /v \"(App)Memory Dumps\" /t REG_SZ /d False /f";	
					system("$cmd");	 
					if ($?) {
						LogMsg(indent(1)."WARNING: Failed to clear the Memory Dumps setting for CCleaner in the registry for $user");  
						$warns++;        
					} else {         
						###
						$need_to_fix=0;
						$cmd="reg query $location";
						@info=`$cmd`;  	
						
						if ($?) {
							LogMsg("WARNING: Could not find CCleaner Settings in the registry");  
							$warns++;        
						} else {            
							foreach my $i (@info) {
								chomp($i);  
								if ($i =~ /Memory Dumps/i) {            
									my @tmp=split(/\s+/,$i);
									my $setting=$tmp[-1];
									unless ($setting eq $expected_setting) {
										LogMsg(indent(1)."Need to change the cCleaner settings for clearing Memory Dumps for $user");
										$need_to_fix=1;
									}
								}
   
							}
						}            
						###
						if ($need_to_fix) {
							LogMsg(indent(1)."WARNING: Failed to correct cCleaner settings for clearing Memory Dumps for $user!");
							$warns++;
						} else {
						   LogMsg(indent(1)."NOTICE: Successfully cleared cCleaner settings for Memory Dumps for $user.");
						}
					}
				} else {
					if ($verbose) {
						LogMsg(indent(1)."cCleaner settings for Memory Dumps is correct for $user");
					}
				}
			} else {
				LogMsg("Fix for cCleaner setting is disabled");
			}				
			
		} else {
			LogMsg("Failed to determine UID for $user in the registry");
			$warns++;
		}
	}
 
	###
	# Check store manager settings
	###
	my $file="c:/Program Files/POS Manager/bkoff.ini";
	if (-f $file) {
		my $server='';
		my $path='';
		# Check the settings
		open (INI,"$file");
		my @ini_info=(<INI>);
		close INI;
		foreach my $i (@ini_info) {
			chomp($i);
			if ($i =~ /^server/) {				 
				my @tmp=split(/=/,$i);
				$server=$tmp[1];				 
			}
		}
		if ($server) {
			# Get the registry setting
			 
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\SAP\\Store Manager\"";		
			@info=`$cmd`; 
			if ($?) {
				LogMsg("WARNING: Could not find Store Manager Settings in the registry");  
				$warns++;        
			} else {            
				foreach my $i (@info) {
					chomp($i);  
					if ($i =~ /POS Server Path/i) {
						my @tmp=split(/\s+/,$i);
						$path=$tmp[-1];
						 
					}
				}
			}
			###
		}
		if ($server) {
			if ($server =~ /EDNA/i) {
				unless ($path =~ /EDNA/i) {
					LogMsg(indent(1)."NOTICE: Store Manager is set to $server but path is set to $path.");
					#$warns++;
				}
			} elsif ($server =~ /192.168/) {
				my $host=gethost($server);
				if ($host =~ /EDNA/i) {
					unless ($path =~ /EDNA/i) {
						LogMsg(indent(1)."NOTICE: Store Manager is set to $server but path is set to $path.");
						#$warns++;
					}				
				}
			} else {				 
				LogMsg(indent(1)."NOTICE: Failed to understand the server setting for Store Manager.");
			}
		} else {
			LogMsg(indent(1)."WARNING: Failed to determine server setting for Store Manager");
			$warns++;
		}
		
	} else {
		LogMsg("Did not locate $file");
	}
 	# Check which register this is
 
	$register_model=get_register_info("Model");
 
    if ($register_model) {
        LogMsg(indent(1)."Register Model: $register_model");
    }
    if ($register_model eq "481033H") {
     	# Check LineDisplay port for IBM OPOS drivers
        my $expected_setting=3;
        $cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\OLEforRetail\\ServiceOPOS\\LineDisplay\" /s";		
		#$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\OLEforRetail\\ServiceOPOS\\LineDisplay\\Customer Display\"";	
        @info=`$cmd`; 
        if ($?) {
            LogMsg("WARNING: Could not find Line Display Settings in the registry");  
            $warns++;        
        } else {            
            foreach my $i (@info) {
                chomp($i);              
                if ($i =~ /ComPort/i) {
                    my @tmp=split(/\s+/,$i);
                    my $setting=$tmp[-1];
                    unless ($setting == $expected_setting) {
                        LogMsg(indent(1)."Line Display is on Com $setting but expected it to be on $expected_setting");
                    } else {
                        if ($verbose) {
							LogMsg(indent(1)."Line Display on Com: $setting");
                            #print "Line Display on Com: $setting\n";
                        }
                    }
                }
            }
        }    
    }

	LogMsg("There have been $warns issues found checking the registry");
	update_action_log($check,$warns);
	if ($warns) {
		$warning_count++;
	} else {		
		$monitor_record_hash{$check}=$timeseconds;		
	}
 	chdir($start_dir);	
}

sub check_software_weekly {
	unless (($day_of_week == 0) || ($daily_only)) {

		return;	
	}
    # This is checking that certain software is not present
    my $warns=0;
	LogMsg("Daily Software Check...");
 
	my $installed_Chrome_file="C:/Program Files/Google/Chrome/Application/chrome.exe";
	# Check for expected Chrome installations
	my $check_chrome_preferences = 0;
	my $chrome_enabled = 0;
	LogMsg(indent(1)."Store: $storenum");
	if ($storenum > 9999) {
		LogMsg(indent(1)."Hostname: $hostname");
		if ($hostname =~ /Reg1/i) {		 			
			$chrome_enabled = 1;
		}  
	}
	if ($chrome_requested) {
		$chrome_enabled = 1;
	}
	if (-f $installed_Chrome_file) {
		LogMsg(indent(1)."Found Chrome installed");		 
		if (($storeStatus eq "Company") && ($chrome_enabled)) {
			$check_chrome_preferences=1;
		}
	} else {
		#LogMsg(indent(1)."Notice: Did not find Chrome installed");
		if (($storeStatus eq "Company") && ($chrome_enabled)) {
			LogMsg(indent(2)."Installing Chrome...");
			install_chrome();
			if (-f $installed_Chrome_file) {
				LogMsg(indent(1)."Chrome installed successfully"); 	
				$check_chrome_preferences=1;
			} else {
				LogMsg(indent(1)."WARNING: Failed to install Chrome");
			}
		} else {
			if  (!$chrome_enabled) {
				LogMsg(indent(1)."(Chrome installation is NOT enabled for this system)");
			}
			unless ($storeStatus eq "Company") {
				LogMsg(indent(1)."(Chrome installation is NOT enabled for this $storeStatus stores yet.)");
			}
		}
		
	}
	if ($check_chrome_preferences) {
		check_chrome_preferences();
	}
	# Check other software installations
	foreach my $app ("XLVIEW","PPTVIEW","WORDVIEW","WordConv") {
		unless(check_viewer($app)) {
			$warns++;
		}
	}
	

	# Check for unwanted Chrome installations
    my @unwanted_list=(        
		"Chrome",		
    );
	
    my $program_files_dir="C:/program files";
    opendir(PROGS,"$program_files_dir");
    my @prog_list=readdir(PROGS);
    close(PROGS);
    foreach my $p (@prog_list) {
        foreach my $u (@unwanted_list) {
            if ($p =~ /$u/i) {
				LogMsg("WARNING: Found $p installed in $program_files_dir");             
				$warns++;      
            }
        }
    }
    $program_files_dir="C:/Documents and Settings/Villages/Local Settings/Application Data";
    opendir(PROGS,"$program_files_dir");
    @prog_list=readdir(PROGS);
    close(PROGS);
    foreach my $p (@prog_list) {
        foreach my $u (@unwanted_list) {
            if ($p =~ /$u/i) {
				LogMsg("WARNING: Found $p installed in $program_files_dir");              
				$warns++;   			
				# See what is in this folder
				my $program_files_subdir="C:/Documents and Settings/Villages/Local Settings/Application Data/$p";
				opendir(PROGS,"$program_files_subdir");
				my @subprog_list=readdir(PROGS);	
				close(PROGS);
				foreach my $s (@subprog_list) {
					foreach my $u (@unwanted_list) {
						if ($s =~ /$u/i) {
							LogMsg("WARNING: Found $p installed in $program_files_subdir");             
							$warns++;      
						}
					} 
				}
 
            }
        }
    }	
	
	# Check that certain services are not running
	my @unwanted_services_list=(
		"NFM Client Service",
		"SQLAnys_SQL_ENG",
		"Transnet",
		"Apache",
		"Xpress Server",
		"lxee_device",
		'RasMan',
		'JavaQuickStarterService',
		'idsvc',
		'ScardSvr'			 
	);
	if (($register_server) || ($as_server)) {
		# If this system is running as a register/server
		# there are some services we do not want to stop
		# so here is a trimmed down list.
		@unwanted_services_list=(
			"NFM Client Service",								
			"lxee_device",
			'RasMan',
			'JavaQuickStarterService',
			'idsvc',
			'ScardSvr'			 
		);	
	}
	#'wuauserv'
	foreach my $u (@unwanted_services_list) { 
		my $querycmd="sc qc \"$u\"";	
		my @service_info=`$querycmd`; 
		foreach my $s (@service_info) {
			if (($s =~ /START_TYPE/i) && (($s =~ /AUTO_START/i) ||($s =~ /DEMAND_START/i)) ) {
				LogMsg("NOTICE: $u service is set to auto or demand start");
				if(disable_service($u)) {
					LogMsg("NOTICE: Disabled $u service");
				} else {
					LogMsg("WARNING: $u is set to auto start and failed to disable it.");
					$warns++				
				}
			}		
		}	
	}
	$warns+=check_installed_software();  	
	if ($warns) {
        LogMsg("WARNING: $warns issues found checking software daily");
		$warning_count++;
    } else {
	   LogMsg("$warns issues found checking software daily");
	}
}
sub check_installed_software {	
 
	my $show_warning=0;
	if ($storeStatus eq "Company") {
		# Temporarily disabled this.  Reducing the importance of this check for now. - 2014-05-29
		#$show_warning=1;
	}
	if ($OS eq "Win7") {
		$show_warning=1;
	} 
	# This will take some time to develop so for now, just give NOTICE rather than WARNING - kdg
	my $warns=0;
	###
	# Check to see what software is installed
  
	my @software_list;
	my @accepted_software_list=(
		"CCleaner",
		"7-Zip",
		"Brother MFL-Pro Suite MFC-7460DN",
		"Brother HL-3045CN",
		"Mozilla Maintenance Service",
		"VideoLAN VLC media player",
		"HP Client Services",
		"Symantec Endpoint Protection",		
		"Microsoft Silverlight",
		"HP Auto",
		"PlayReady PC Runtime amd64",
		"Windows Mobile Device Center",
		"64 Bit HP CIO Components Installer",
		"32 Bit HP CIO Components Installer",
		"Symantec pcAnywhere",
		"WebFldrs XP",
		"Foxit Reader",
		"Transnet 1.5",
		"Compatibility Pack for the 2007 Office System",
		"Apache HTTP Server 2.0.59",
		"Visual FoxPro ODBC Driver",
		"Google Update Helper",
		"Microsoft .NET Framework",
		"IBM 4810-34x",
		"Graphics Media Accelerator Driver",
		"CPUID CPU-Z",
		"Microsoft SQL Server 2005 Backward compatibility",
		"Java 7 Update 40",
		"Java SE Development Kit 7 Update 40",
		"SAP POS Store Data Transfer for Retail",
		"SQL Anywhere 12 Deployment",
		"Store Manager",
		"Xpress Server",
		"Database Tools",
		"Electronic Journal Viewer",
		"Point of Sale",
		"Xpress Server Database",
		"Electronic Journal Database",
		"CAPICOM",
		"Java Auto Updater",		
		"Microsoft Internationalized Domain Names Mitigation APIs",
		"Microsoft National Language Support Downlevel APIs",
		"M5000 Programmer",
		"NirSoft BlueScreenView",
		"TreeSize Free",
		"UltraVnc",
		"Apache HTTP Server",		 
		"Malwarebytes",	
		"Brother BRAdmin Light",
		"HL-3170CDW",
		"SoundMAX",
		"HP Officejet 6500 E710n-z Basic Device Software",
		"SyncToy 2.1",
		"Microsoft Sync Framework 2.0 Provider Services",
		"Microsoft Sync Framework 2.0 Core",
        "System Explorer 4.1.0",
        "Microsoft Kernel-Mode Driver Framework Feature Pack",
        "Windows Driver Package - ASIX .* Net",
		"Cherry Configuration Software",
		"Configuration Tools for the HP USB POS Keyboard V5.7"

	);
	if ($storeStatus eq "Contract") {
		#push(@accepted_software_list,"Adobe Reader");
	}
	# Some exceptions
	if ($storenum == 3979) {
		push(@accepted_software_list,"PL-2303 USB-to-Serial");
	}
	# Windows 7 applications
	my @accepted_software_win7_list=(
		"OPOS for HP LineDisplay",
		"HP USB Barcode Scanner",
		"OPOS Support for Hewlett-Packard Printers",
		"HP Line Display USB Driver For Vista and Win7 32bit",
		"HP Integrated JavaPOS Cash Drawer",
		"Intel.*Rapid Storage Technology",
		"Recovery Manager",
		"OPOS HP USB Cash Drawer",
		"Microsoft POS for .NET 1.12",
		"Intel.*Management Engine Components",
		"HP Integrated JavaPOS Cash Drawer",
		"OPOS for HP LineDisplay",
		"OPOS HP Integrated Cash Drawer",
		"OPOS HP Barcode Scanner",
		"OPOS Support for HP POS Keyboard V5.8",
		"HP USB Barcode Scanner",
		"OPOS HP USB Cash Drawer",
		"DirectX for Managed Code Update .Summer 2004",
		"HP Integrated Cash Drawer Driver",
		"Intel.*Processor Graphics",
		"Realtek High Definition Audio Driver",
		"Intel.*Control Center",
		"OPOS for HP Mini MSR",
		"OPOS Support for Hewlett-Packard Printers",
		"HPLineDisplayUSBDriverWin7Vista32bit",
		"HPSER2PL",
		"HP2SER2PL_x86",
		"GWX Control Panel"
		
 
	);
	if ($OS eq "Win7") {		
		push(@accepted_software_list,@accepted_software_win7_list);		
	}
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\" /s";
	my @registry_info=`$cmd`;
 
	my $target="Uninstall";
	my $target_found=0;
	
	foreach my $r (@registry_info) {
		chomp($r);
		if ($target_found) {				
			if ($r =~ /DisplayName/) {
				my @tmp=split(/REG_SZ/,$r);
				my $sw=$tmp[-1]; 
				
				while ($sw =~ /^ /) {
					$sw =~ s/^ //g;
				}
				while ($sw =~ /^\s/) {
					$sw =~ s/^\s//g;
				}				
				next if ($sw =~ /^Update/i);
				next if ($sw =~ /^Security Update/i);
				next if ($sw =~ /^Service Pack/i);
				next if ($sw =~ /^Microsoft Office/i);
				next if ($sw =~ /^Definition Update for Microsoft Office/i);
				next if ($sw =~ /^Microsoft Office/i);
				next if ($sw =~ /^Microsoft .* Redistributable/i);
				next if ($sw =~ /^MSXML/i);
				#next if ($sw =~ /^HP Officejet/i);
				next if ($sw =~ /^ActivePerl/i);
				next if ($sw =~ /^Triversity/i);
				next if ($sw =~ /^Windows Internet Explorer/i);
				next if ($sw =~ /^Windows XP/i);
				next if ($sw =~ /^Windows Genuine Advantage/i);
				next if ($sw =~ /^Hotfix for Windows XP/i);
				next if ($sw =~ /^IBM.*UnifiedPOS/i);
				next if ($sw =~ /^Intel.*Extreme Graphics/i);
				next if ($sw =~ /^Intel.*Network Adapter/i);
				next if ($sw =~ /^Google Chrome/i);
				next if ($sw =~ /^Mozilla Firefox/i);
				next if ($sw =~ /^TightVNC/i);
				next if ($sw =~ /^Tweak UI/i);
				next if ($sw =~ /^LiveReg/i);
				next if ($sw =~ /^LiveUpdate/i);
				next if ($sw =~ /^OPOS Common Control/i);
				next if ($sw =~ /^TPS.*NFM/i);
				next if ($sw =~ /^Notepad/i);
 
				push(@software_list,$sw);
			}
		
		}
		if ($r =~ /HKEY_LOCAL_MACHINE/) {
			$target_found=0;
		}
		if ($r =~ /$target/) {
			$target_found=1;
		}
	}
	foreach my $s (@software_list) {
		my $accepted=0;
		foreach my $a (@accepted_software_list) {			
			if ($s =~ /$a/i) {
				$accepted=1;
				if ($verbose) {
					print "Found $s installed - accepted\n";
				}
			}
		}
		unless ($accepted) {
			if ($show_warning) {
				LogMsg("WARNING: ($s) is installed but is not expected");
				$warns++;
			} else {
				LogMsg("Notice: ($s) is installed but is not expected");
			}
		}			
	}
	return $warns;
	###
} 
sub uninstall {
	my $sw=shift;
	my %uninstall_tool_hash=(
		'UltraVNC'=>'c:/program files/ultravnc/unins000.exe',
	);
	my %uninstall_instructions_hash=(
		'UltraVNC'=>"\"c:/program files/ultravnc/unins000.exe\" /SILENT",
	);	
	if (-f $uninstall_tool_hash{$sw}) {
		LogMsg("Found uninstall tool for $sw");
	} else {
		LogMsg("Failed to locate $uninstall_tool_hash{$sw}");
		return 0;
	}
	# This uninstall is not sorted out yet - kdg
	return 0;
	
	my $cmd=$uninstall_instructions_hash{$sw};
	my %results=`$cmd`;
	if ($?) {
		LogMsg("Error attempting to unistall $sw");
	} else {
		LogMsg("Uninstall of $sw appears successful");
	}
	return 1;
}
 
sub get_villages_user {
 	my $cmd="reg query \"HKEY_USERS\"";
    my @info=`$cmd`;  	
	my @users;
	my $villages_user='';
    if ($?) {
        LogMsg("Could not find HKEY_USERS in the registry ");  
       
    } else { 		
        foreach my $i (@info) {
			# Get a list of users
            chomp($i); 
			unless ($i =~ /_Classes/) {
				if ($i =~ /S-1/) {
					#print "Registry: ($i)\n";
					push(@users,$i);
				}
			}
			if ($verbose) {
				print "Registry: ($i)\n";
			} 
		}
    } 
 
	foreach my $u (@users) {		
		my $tmp=find_user($u, "Villages");
		if ($tmp) {
			$villages_user=$tmp;			 
		} 			
	}
	return $villages_user;
} 

sub get_user {
	my $requested_user=shift;
	if ($requested_user eq "HKEY_CURRENT_USER") {
		# This may seem silly but it allows us to use this function for any user including the current user
		return $requested_user;
	}
 	my $cmd="reg query \"HKEY_USERS\"";
    my @info=`$cmd`;  	
	my @users;
	my $uid='';
    if ($?) {
        LogMsg("Could not find HKEY_USERS in the registry ");  
       
    } else { 		
        foreach my $i (@info) {
			# Get a list of users
            chomp($i); 
			unless ($i =~ /_Classes/) {
				if ($i =~ /S-1/) {
					#print "Registry: ($i)\n";
					push(@users,$i);
				}
			}
			if ($verbose) {
				#print "Registry: ($i)\n";
			} 
		}
    } 
 
	foreach my $u (@users) {	
		
		my $tmp=find_user($u, "$requested_user");
		if ($tmp) {
			$uid=$tmp;			 			
		} 			
	}
	
	return $uid;
} 

sub check_registry_daily {
	if ($OS eq "Win2000") {
		return;
	} 
	if ($OS eq "Win7") {
		LogMsg("The check_registry_daily function is not supported by $OS");
		return;
	} 	
	my $warns=0;
	###
 	my $cmd="reg query \"HKEY_USERS\"";
    my @info=`$cmd`;  	
	my @users;
	my $villages_user='';
    if ($?) {
        LogMsg("Could not find HKEY_USERS in the registry ");  
       
    } else { 		
        foreach my $i (@info) {
			# Get a list of users
            chomp($i); 
			unless ($i =~ /_Classes/) {
				if ($i =~ /S-1/) {
					#print "Registry: ($i)\n";
					push(@users,$i);
				}
			}
			if ($verbose) {
				print "Registry: ($i)\n";
			} 
		}
    } 
 
	foreach my $u (@users) {		
		my $tmp=find_user($u, "Villages");
		if ($tmp) {
			$villages_user=$tmp;			 
		} 			
	}
	if ($verbose) {
		LogMsg(indent(1)."Determining Villages user...");
	}
	# Determine the Villages user in the registry
	$cmd="reg query \"HKEY_USERS\"";
    @info=`$cmd`;  		 		
	# Check if IE has a proxy server configured.  This would be our privoxy most likely
 
	if ($villages_user) {
		LogMsg(indent(1)."Villages User: $villages_user");
		$cmd="reg query \"$villages_user\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\"";
		@info=`$cmd`;  	
		my $privoxy_found=0;
		my $proxy_enable=0;
		my $agent="unknown";
		LogMsg("Checking for proxy server configured...");
		foreach my $i (@info) {
			chomp($i);  			 
			if ($i =~ /ProxyServer/) {				
				my @tmp=split(/\s+/,$i);
				LogMsg("Found ProxyServer: $tmp[-1]");	
				LogMsg("System Info: PrivoxyConf: Found");	
				if ($agent) {
					LogMsg("System Info: Privoxy $agent: Found");	
				}
				$privoxy_found++;
			} 
			if   ($i =~ /User Agent/) {	
				my @tmp=split(/\s+/,$i);
				for (my $x=0; $x<=$#tmp; $x++) {
					my $item=$tmp[$x];
					if ($item =~ /REG_SZ/) {
						$agent=$tmp[$x + 1];
					}
				}
				if ($agent) {
					LogMsg("Found Proxy User Agent: $agent");					
				} 								 
			}	
			if ($i =~ /ProxyEnable/) {				
				my @tmp=split(/\s+/,$i);
				LogMsg("Found ProxyEnable: $tmp[-1]");
				if ($tmp[-1] eq "0x1") {
					$proxy_enable=1;
			
				}
				
 
			} 			
		}
		unless ($privoxy_found) {		 
			LogMsg("System Info: PrivoxyConf: NotFound");	
		}  
        unless ($new_register_test) {
    		if ($privoxy_running) {
    			unless ($proxy_enable) {
    				#LogMsg("ISSUE: Proxy is not enabled in Internet Explorer");
    				LogMsg("Notice: Proxy is not enabled in Internet Explorer");
    				#$warns++;					 
					LogMsg("System Info: PrivoxyEnabled: NO");
    			} else {
    				LogMsg("WARNING: Proxy is enabled in Internet Explorer");
					LogMsg("System Info: PrivoxyEnabled: YES");
    				$warns++;					
    			} 
    			unless ($privoxy_found) {		 
    				#LogMsg("ISSUE: Privoxy is not configured in Internet Explorer");
    				LogMsg("Notice: Privoxy is not configured in Internet Explorer");
    				#$warns++;
				
				}				
    		}
        }
		if ($privoxy_found) {
			if ($proxy_enable) {
				#
				#if (($storenum =~ /^7060/)  && ($day_of_week == 1)) {
				if (1) {
					disable_privoxy();
					 
				} else {
					LogMsg("Disable Privoxy is currently not enabled.");
				}		
			}
		}
		# Determine default browser
 
 
		# Check for the default browser	for Villages
 		
		my $default_chrome=0;
		my $default_firefox=0;
		my $default_ie=0;
		my $found_http=0;
		$cmd="reg query \"$villages_user\\Software\\Classes\" /s";
		@info=`$cmd`;
		
		foreach my $i (@info) {
			chomp($i);
			if ($i =~ /http.*shell.*open/i) {
				$found_http=1;
			} 
		}		
		
		if ($found_http) {
			$cmd="reg query \"$villages_user\\Software\\Classes\\http\\shell\\open\\command\"";			
			@info=`$cmd`;					
			foreach my $i (@info) {
				chomp($i);
				if ($i =~ /internet explorer/i) {
					$default_ie=1;
				}
				if ($i =~ /iexplorer/i) {
					$default_ie=1;
				}			
				if ($i =~ /firefox/i) {
					$default_firefox=1;
				}				
				if ($i =~ /chrome/i) {
					$default_chrome=1;
				}				
				
			}
		}
 		
 
		if ($default_chrome) {
			LogMsg(indent(1)."System Info: Default Browser: Chrome");		 
		}
		if ($default_ie) {
			LogMsg(indent(1)."System Info: Default Browser: Internet Explorer");
			#LogMsg(indent(1)."WARNING: Default Browser appears to be Internet Explorer");
			#$warns++;
		}	
		if ($default_firefox) {
			LogMsg(indent(1)."System Info: Default Browser: FireFox");
		}		
		###

	}
	if ($verbose) {
		LogMsg(indent(1)."Determining FireFox version...");
	}	
	###		
	# Check for FireFox and the version
 
	$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\"";
	my $found_mozilla=0;	
    my $found_java=0;
	@info=`$cmd`;  

 
	if ($?) {
		LogMsg("WARNING: Could not find Local_Machine Software settings in the registry");  
		$warns++;     
	} else {   		
		foreach my $i (@info) { 
			if ($i =~ /mozilla/i) {
				unless ($i =~ /mozillaplugins/i) {					
					$found_mozilla=1;
				}
			}
			if ($i =~ /javasoft/i) {
					LogMsg("Found Java");				 			
					$found_java=1;
 
			}            
		}
	}
	my $firefox="unknown";
	if ($found_mozilla) {
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Mozilla\"";		  
		@info=`$cmd`;  
		
		if ($?) {
			LogMsg("WARNING: Could not find Mozilla settings in the registry");  
			$warns++;    
 
		} else {   
			LogMsg("Found FireFox");		
			foreach my $i (@info) { 
				if ($i =~ /firefox [0-9]/i) {
					my @tmp=split(/\s+/,$i);
					$firefox=$tmp[2];					
					LogMsg("System Info: Mozilla Firefox Version: $firefox");									
				}
			}
		}
		
		#if ($firefox eq "unknown") {
		
			# A more intensive search for the firefox version
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\" /s";		  
			@info=`$cmd`;  	
			my $target_found=0;
			foreach my $i (@info) { 
				if ($i =~ /displayname.*firefox/i) {
					$target_found=1;
				}
				if ($target_found) {
					if ($i =~ /DisplayVersion/i) {						
						my @tmp=split(/\s+/,$i);
						$firefox=$tmp[-1];					
						LogMsg("System Info: Mozilla Firefox Version: $firefox");							
					}
					if ($i =~ /DisplayName/i) {
						$target_found=0;
					}					
				}
			}			
		#}
		# In case it is installed in user space
		
		#if ($firefox eq "unknown") {			
			my $uid = get_user("VILLAGES");			
			my @tmp=split(/\\/,$uid);
			my $user=$tmp[1];
			if ($user) {
				$cmd="reg query \"HKEY_USERS\\$user\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\"";	
				@info=`$cmd`;  				
				$target_found=0;
				foreach my $i (@info) { 
					if ($i =~ /Uninstall/i) {
						$target_found=1;						
					} 
				}	
			} else {
				LogMsg("Was not able to determine the Villages user in the registry.");
			}
			if ($target_found) {
				# A more intensive search for the firefox version
				$cmd="reg query \"HKEY_USERS\\$user\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\" /s";					
				@info=`$cmd`;  					
				$target_found=0;
				foreach my $i (@info) { 
					if ($i =~ /displayname.*firefox/i) {
						$target_found=1;	
						LogMsg(indent(1)."FireFox found in user space");
					}
					if ($target_found) {
						if ($i =~ /DisplayVersion/i) {							
							my @tmp=split(/\s+/,$i);
							$firefox=$tmp[-1];											
							LogMsg("System Info: Mozilla Firefox Version: $firefox");							
						}
						if ($i =~ /DisplayName/i) {
							$target_found=0;
						}
					}
				}	
			}  
			
		#}	
		if ($firefox eq "unknown") {		
			LogMsg(indent(1)."Unable to determine FireFox Version - Must not be installed");
			#$warns++;
		}
	
	}
 	
	#if ($verbose) {
		LogMsg(indent(1)."Determining FireFox preferences...");
	#}		
	# Check Firefox preferences
	my $firefox_dir="c:/Documents and Settings/Villages/Application Data/Mozilla/Firefox/Profiles";
	my $pref_dir;
	my $pref_file; 
	if (-d $firefox_dir) {
		opendir(PREFS,"$firefox_dir");
		my @dir_list=readdir(PREFS);
		close(PREFS);
		# Find the preferences folder
		foreach my $d (@dir_list) {
			if ($d =~ /default$/i) {
				$pref_dir="${firefox_dir}/${d}";
			}
		}		

		if (-d $pref_dir) {
			# Find the settings
			$pref_file="${pref_dir}/prefs.js";
			my $proxy_configured=0;
			my $proxy_set=0;
			if (-f $pref_file) {
				open(PREFSJS,"$pref_file");
				my @pref_info=(<PREFSJS>);
				close PREFSJS;
				foreach my $p (@pref_info) {
					chomp($p);
					if ($p =~ /network.proxy.type/) {
						if ($p =~ /1/) {
							$proxy_set=1;
						}
					}
					if ($p=~ /network.proxy.http/) {
						if ($p =~ /192.168/) {
							$proxy_configured++;
						}				
					}
					if ($p=~ /network.proxy.http_por/) {
						if ($p =~ /8118/) {
							$proxy_configured++;
						}				
					}				
				}
			}		
			#if ($privoxy_running) {
				if ($proxy_configured) {
					LogMsg("Privoxy is configured on Firefox");
					unless ($proxy_set) {
						#LogMsg("ISSUE: Privoxy is not set in FireFox");
						LogMsg("Privoxy is not set in FireFox"); 
						
					} else {
						LogMsg("Privoxy is set on Firefox.");
						 
						$warns+=disable_privoxy_firefox("$pref_dir",$firefox);
					}
				} else {				
					#LogMsg("ISSUE: Privoxy is not configured in FireFox");
					LogMsg("Privoxy is not configured in FireFox");
					#$warns++;
				}
			#}
		}
	}
 
	#if ($verbose) {
		LogMsg(indent(1)."Checking JavaSoft...");
	#}		 
	if ($found_java) {     
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft\" /s";		  
		@info=`$cmd`;  
		my $plugin="unknown";
		if ($?) {
			LogMsg("WARNING: Could not find Java settings in the registry");  
			$warns++;     
		} else {   		
			foreach my $i (@info) {                 
				if ($i =~ /UseJava.*IExplorer/i) {                
                    if ($i =~ /0x1/) {                        
                        LogMsg("WARNING: Java is enabled in Internet Explorer");  
                        $warns++;   				
                    }
				}
			}
		}
	
	} 
 
	if (0) {
		# Check when the AntiVirus is scheduled to update  --- THIS IS OBSOLETE

		
		$cmd="reg query \"HKLM\\Software\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\PatternManager\\Schedule\"";	
		@info=`$cmd`;  	
		my $time_of_day;
		#my $day_of_week;
		my $adjust_date=0;
		unless ($?) {
			LogMsg("Checking AntiVirus update schedule...");
			foreach my $i (@info) {
				chomp($i);  
				if ($i =~ / MinOfDay/) {
					my @tmp=split(/x/,$i);
					my $time_of_day=$tmp[-1];				
					$time_of_day = hex($time_of_day);
					LogMsg("System Info: VirusUpdateTime: $time_of_day");
					if (($time_of_day > 480) && ($time_of_day <  1260)) {
						LogMsg("Virus Update is set to $time_of_day");					 
						$adjust_date=1;
					}				
				}
				if ($i =~ / DayOfWeek/) {
					my @tmp=split(/x/,$i);
					my $VirusUpdateDay=$tmp[-1];				
					LogMsg("System Info: VirusUpdateDay: $VirusUpdateDay");					
				}			
				#print "Registry: ($i)\n";			
				if ($verbose) {
					print "Registry: ($i)\n";
				}			 
			}   
		} 		
		if ($adjust_date) {
			set_virus_update();
			my $date_set=1;
			# Now recheck the date
			$cmd="reg query \"HKLM\\Software\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\PatternManager\\Schedule\"";	
			@info=`$cmd`;  	
			my $time_of_day;
			#my $day_of_week;
			my $adjust_date=0;
			unless ($?) {
				LogMsg("Checking AntiVirus update schedule...");
				foreach my $i (@info) {
					chomp($i);  
					if ($i =~ / MinOfDay/) {
						my @tmp=split(/x/,$i);
						my $time_of_day=$tmp[-1];				
						$time_of_day = hex($time_of_day);
						LogMsg("System Info: VirusUpdateTime: $time_of_day");
						if (($time_of_day > 480) && ($time_of_day <  1260)) {
							LogMsg("Warning: Virus Updated should be set earlier in the morning");
							$warns++;		
							$date_set=0;
						}				
					}
					if ($i =~ / DayOfWeek/) {
						my @tmp=split(/x/,$i);
						my $VirusUpdateDay=$tmp[-1];				
						LogMsg("System Info: VirusUpdateDay: $VirusUpdateDay");					
					}							
					if ($verbose) {
						print "Registry: ($i)\n";
					}			 
				}   
			}
			if ($date_set) {
				LogMsg("Successfully set the Virus Update time");
			} else {
				LogMsg("Failed to set the Virus Update time");
			}
			
		}
	}
 
	###
    # Check Automatic Updates Setting
	# Are windows updates enabled?
	my $AutomaticUpdates=0;
 
	foreach my $e (@expected_service_list) {
		if ($e eq "AutomaticUpdates") {
			$AutomaticUpdates=1;
		}
	}	
 
	if ($AutomaticUpdates) {
		if (check_windows_updates()) {
			LogMsg(indent(1)."Windows Updates are enabled");
		} else {
			# Enable Windows Updates
			enable_windows_updates(); 
			# Check again...
			if (check_windows_updates()) {
				LogMsg(indent(1)."Windows Updates are enabled");
			} else {
				LogMsg(indent(1)."WARNING: Windows Updates are NOT enabled");
				$warns++;
			}			
		}
	} else {
		if (check_windows_updates()) {
			# Disable Windows Updates
			disable_windows_updates();
			# Check Again
			if (check_windows_updates()) {
				LogMsg(indent(1)."WARNING: Windows Updates are enabled");
				$warns++;
			}
		} else {
			LogMsg(indent(1)." Windows Updates are not enabled");			
		}	
	} 
	# Check for Chrome and the version
	# We will look in the registery and determine what user installed Chrome as well.

	foreach my $user ("Administrator","Villages") {		
		my $chrome_version=0;
		my $chrome_found=0;	
		my $found_setting=0;
		my $google_link='';
		my $chrome_link='';
		my $uid = get_user("$user");		
		my @tmp=split(/\\/,$uid);
		$uid=$tmp[1];	

		if ($verbose) {
			LogMsg("User: $user - UID: $uid");
		}
		if ($uid) {	
			my $google_software="\"HKEY_USERS\\$uid\\Software";	

			$cmd="reg query $google_software";					
			my $location = '';
			@info=`$cmd`; 				
			if ($?) {		
				#LogMsg("WARNING: Could not find Google Software in the registry at $google_software for $user");  
				#$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 
				if ($i =~ /google/i) {					
					$google_link=$i;
				}
			}			
		}
		if ($google_link) {
			$cmd="reg query $google_link";
			@info=`$cmd`; 			
			if ($?) {		
				#LogMsg("WARNING: Could not find Google Software in the registry at $google_link for $user");  
				#$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 
				if ($i =~ /chrome/i) {					
					$chrome_link=$i;
				}
			}				
		}
		if ($chrome_link) {
			
			$cmd="reg query $chrome_link\\BLBeacon";
			@info=`$cmd`; 			
			if ($?) {		
				#LogMsg("WARNING: Could not find Google Software in the registry at $chrome_link for $user");  
				#$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 
				
				if ($i =~ /version/i) {
					unless ($i =~ /reg.exe/i) {
						my @tmp=split(/\s+/,$i);
						$chrome_version=$tmp[-1];
						LogMsg("System Info: Chrome $user Version: $chrome_version");						
					}				
				}
			}				
		}		
	}
	###
	# Find FireFox Version
	foreach my $user ("Administrator","Villages") {		
		my $firefox_version=0;
		my $firefox_found=0;	
		my $found_setting=0;
		my $mozilla_link='';
		my $firefox_link='';
		my $uid = get_user("$user");		
		my @tmp=split(/\\/,$uid);
		$uid=$tmp[1];	

		if ($verbose) {
			LogMsg("User: $user - UID: $uid");
		}
		if ($uid) {	
			my $mozilla_software="\"HKEY_USERS\\$uid\\Software";	

			$cmd="reg query $mozilla_software";					
			my $location = '';
			@info=`$cmd`; 				
			if ($?) {		
				LogMsg("WARNING: Could not find Mozilla Software in the registry at $mozilla_software for $user FF1");  
				$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 
				if ($i =~ /mozilla/i) {		
					unless ($i =~ /plugins/i) {						
						$mozilla_link=$i;
					}
				}
			}			
		}
		if ($mozilla_link) {
			$cmd="reg query $mozilla_link";
			@info=`$cmd`; 			
			if ($?) {		
				LogMsg("WARNING: Could not find Mozilla Software in the registry at $mozilla_link for $user FF2");  
				$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 
				
				if ($i =~ /firefox$/i) {					
				
					$firefox_link=$i;
				}
			}				
		}
		if ($firefox_link) {			
			$cmd="reg query \"$firefox_link\" /s";			
			@info=`$cmd`; 			
			if ($?) {		
				LogMsg("WARNING: Could not find Mozilla Software in the registry at $firefox_link for $user FF3");  
				$warns++;    
			}			
			foreach my $i (@info) {
				chomp($i); 				
				if ($i =~ /version/i) {
					unless ($i =~ /reg.exe/i) {
						my @tmp=split(/REG_SZ/,$i);
						$firefox_version=$tmp[1];
						LogMsg("System Info: FireFox $user Version: $firefox_version");						
					}				
				}
			}				
		}		
	}	
	###	
	# 
	LogMsg("There have been $warns issues found on the daily checking of the registry");
	if ($warns) {
		$warning_count++;	
	}
}	

sub check_windows_updates {
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update\"";    
	my @info=`$cmd`;  
	my $AUOptions;
	my $AUOptions_expected="0x4";
 
	my $return=0;
	if ($?) {
		LogMsg("WARNING: Could not find WindowsUpdate Settings in the registry");  
		         
	} else {    
		foreach my $i (@info) {
			chomp($i);
			if ($i =~ /AUOptions/) {                
				my @tmp=split(/\s+/,$i);
				$AUOptions=$tmp[-1];                  
				if ($AUOptions eq "$AUOptions_expected") {
					$return=1;					
				}
			}             
		}    
	}
	return $return;
}

sub enable_windows_updates {
	LogMsg("Enabling Windows Updates...");
	my $cmd="reg add \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update\" /v AUOptions /t REG_DWORD /d 4 /f";  
	system("$cmd");   
}

sub disable_windows_updates {
	LogMsg("Disabling Windows Updates...");
	my $cmd="reg add \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update\" /v AUOptions /t REG_DWORD /d 1 /f";  
	system("$cmd"); 

}

sub set_virus_update {
	# Set the time for the virus definitions update in the registry
	
	# Delete the old setting
	LogMsg("Removing the original MinOfDay setting...");
	my $cmd="reg delete \"HKLM\\Software\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\PatternManager\\Schedule\" /v MinOfDay /f";	
	system("$cmd");
	# Add the new setting
	LogMsg("Adding the new MinOfDay setting...");
	$cmd="reg add \"HKLM\\Software\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\PatternManager\\Schedule\" /v MinOfDay /t REG_DWORD /d 0x000000d2 /f";	
	system("$cmd");	
 
 
}


sub find_user {
	my $u=shift;
	my $user=shift;
	my $cmd="reg query \"$u\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\"";	
	my @info=`$cmd`;  
	my $return_value;	
 
	unless ($?) {
		foreach my $i (@info) {
			chomp($i);  
			 
			if (($i =~ /Logon User Name/) && ($i =~ /$user/i)) {		 
				$return_value=$u;
				return $return_value;
			} 
 
		}   
 
	}
	return $return_value;
}
 
sub identify_user {
	my $u=shift;
	my $cmd="reg query \"HKEY_USERS\\$u\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\"";	
	my @info=`$cmd`;  
	my $return_value="unknown";	
 
	unless ($?) {
		foreach my $i (@info) {
			chomp($i);  
			if ($i =~ /Desktop/i) {
		 
				my @tmp=split(/\\/,$i);
				for (my $x=0; $x<=$#tmp; $x++) {
					my $value=$tmp[$x];
					my $next_value=$tmp[($x + 1)];
					my $next_next_value=$tmp[($x + 2)];
 
					if (($value eq "Users") && ($next_next_value eq "Desktop")) {
						$return_value=$next_value;
					 
					}
				} 				
			} 
		}    
	}	
	return $return_value;
} 
 
sub check_quickscan {
	my $u=shift;
	unless (length($u) > 20) {
		return;
	}
 
	my $cmd="reg query \"$u\\Software\"";	
	my @info=`$cmd`;  	
	my $Intel=0;	
	unless ($?) {
		foreach my $i (@info) {
			chomp($i);  			
			if ($verbose) {
				print "Registry: ($i)\n";
			}
			if ($i =~ /Intel/) {	
				$Intel=1;
			} 
		}   
	} 		
	unless ($Intel) {
		return $Intel;
	}
 
	$cmd="reg query \"$u\\Software\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\Custom Tasks\\TaskPadStartUp\"";	
	@info=`$cmd`;  	
	my $target_found=0;	
	unless ($?) {
		foreach my $i (@info) {
			chomp($i);  			
			if ($verbose) {
				print "Registry: ($i)\n";
			}
			my @tmp=split(/\\/,$i);
			if (defined($tmp[-2])) {
				if ($tmp[-2] =~ /TaskPadStartUp/) {
					$target_found=1;
				}
			}
			
			if ($i =~ /Auto-Generated QuickScan/) {	
				$target_found=1;
			} 
		}   
	} 	
	return $target_found;
}

sub clear_quickscan {
	unless ($OS eq "WinXP") {
		LogMsg("The clear_quickscan function does not support $OS");
		return;
	}
	my $u=shift;	
	my $reg_fix="c:/temp/RemoveStartScanVillages.reg";
	open(FIX,">$reg_fix");
	print FIX "Windows Registry Editor Version 5.00\n";
	print FIX "\n";
	print FIX "[-$u\\Software\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\Custom Tasks\\TaskPadStartup\\Auto-Generated QuickScan]\n";
	close FIX;
	if (-f $reg_fix) {
		LogMsg("Created $reg_fix");
		# Now fix it
		my $fixcmd = "regedit.exe /s $reg_fix";
		LogMsg(indent(2)."Running $fixcmd");
		my @results=`$fixcmd`;
		foreach my $r (@results) {
			LogMsg(indent(1)."$r");
		}		
	} else {
		LogMsg("WARNING: Failed to create $reg_fix");
	}
	unlink $reg_fix;
}

sub check_pcAnywhere {  
	unless ($OS eq "WinXP") {
		LogMsg("check_pcAnywhere is not required for $OS");
		return;
	}
    if ($new_register_test) {
        print "Not checking pcAnywhere\n";
        return;
    }
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=30;
	$freq=0;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="pcAnywhere";
	my $warns=0;
	unless ($force) {
		if ($monitor_record_hash{$check}) {
			my $last_pass=$monitor_record_hash{$check};
			my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
			unless ((($timeseconds - $last_pass)/86400) > $freq) {
				# We do not have to run this test again 
				LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
				return;
			}
		}		
	}
	LogMsg("Checking pcAnywhere");	
	my $no_fix=shift;
 
    # Check logging - We want logging on but not port scanning
    my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\pcAnywhere\\CurrentVersion\\Logging\\NT Events\" ";    
    my @info=`$cmd`;  
    my $logging=1;
    foreach my $i (@info) { 
        chomp($i);
        if ($i =~ /UseLogging/) {
            # We prefer to have NT Logging on
            my @tmp=split(/\s+/,$i);
            if ($tmp[-1] eq "0x0") {
                LogMsg(indent(1)."WARNING: pcAnywhere NT Logging is turned off.");                  
                $warns++;				
                $logging=0;
            }               
        } 
        if ($logging) {
            # If NT Logging is on, check that we are not logging port scanning
            if ($i =~ /ConnPortScanned/) {
                my @tmp=split(/\s+/,$i);
                if ($tmp[-1] eq "0x1") {
                    LogMsg(indent(1)."WARNING: pcAnywhere ConnPortScanned is being logged.");                       
                    $warns++;					
                }               
            }
        }
         
    }
    # Check some registry settings
    LogMsg("Checking pcAnywhere settings.");

    $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\pcAnywhere\\CurrentVersion\\Remote Control\" /s";    
    @info=`$cmd`;
	my $cleanup_needed=0;

    foreach my $i (@info) {        
        if ($i =~ /TCPIPRemoteNames/) {   
			$cleanup_needed++;				
        }
    }
	if ($cleanup_needed) {
		LogMsg("Need to cleanup TCP IP Remote Names for pcAnywhere");
		my $tool="//Edna/temp/install/pcAnywhere/removeIPpcAnywhere.bat";
		unless (-f $tool) {
			LogMsg("WARNING: Failed to locate $tool");
			$warning_count++;
		} else {
			# Run the tool which will remove the TCPIPRemoteNames entries
			`$tool`;	
			# Check the registry again			
			@info=`$cmd`;
			foreach my $i (@info) {        
				if ($i =~ /TCPIPRemoteNames/) {
					LogMsg(indent(1)."WARNING: A TCPIPRemoteName is configured in pcAnywhere");	
					$warns++;
				}
			}
		}		
	} else {
		LogMsg(indent(1)."No TCPIPRemoteNames configured");
	}
	# pcAnywhere Settings
	 
	unless ($hostname) {
		# If for some reason we don't have the hostname
		chomp($hostname = `hostname`); 
	}
	unless ($storenum) {
		# If for some reason we don't have the storenum
		if (-f $system_info_file) {
			# Read in the site info
			LogMsg("Reading $system_info_file...");
			open(REC,"$system_info_file");
			my @site_info=(<REC>);
			close REC;
			my $regnum=lc($hostname);
			foreach my $r (@site_info) {
				chomp($r);
				my @tmp=split(/\s+/,$r);
				if ($tmp[0] =~ /storenum/) {
					$storenum=$tmp[1];
					LogMsg("Running on store $storenum");
				}
				if ($tmp[0] =~ /storeStatus/) {
					$storeStatus=$tmp[1];
					LogMsg("Running on a $storeStatus store");
				}	
				if ($tmp[0] =~ /privoxy/) {
					$privoxy_running=$tmp[1];
				}
				if ($r =~ /customer_number.*$regnum/)  {
					my @ttmp=split(/:/,$r);
					$customer_number=$ttmp[-1];				
				}
				if ($tmp[0] =~ /timezone/) {
					$timezone=$tmp[1];
				}				
			}
		} else {
			LogMsg("Did not find $system_info_file");
		}				 
	}	
 
	if ($hostname) { 
		if ($storenum) {
			unless (check_pcAnywhere_host_name()) {		
				LogMsg("WARNING: pcAnywhere host name is not set correctly");
				$warns++;		
			}	
		} else {
			LogMsg("WARNING: Failed to determine storenum");
			$warns++;	
		}
	} else {
		LogMsg("WARNING: Failed to determine hostname");
		$warns++;	
	}	
	update_action_log($check,$warns);
	if ($warns) {
		LogMsg("There have been $warns issues found checking pcAnywhere");
		if ($warns) {
			$warning_count++;
		}
	} else {		
		$monitor_record_hash{$check}=$timeseconds;	
	}
    LogMsg("Finished checking pcAnywhere settings.");    
}

sub check_for_update {
	if ($debug_mode) {
		return;
	}
	LogMsg("Check for update...");
	# Check to see if there are files we need to copy from the server.  These will be scripts or registry updates in most cases.
	my $warns=0;
	my $update_count=0;
    my $updating_self=0;
    
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=1;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="update";
 
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			return;
		}
	}		
	# If this register has the following directories, then we can update all of the scripts.  
	# Otherwise, just update the Perl Modules.
	my $update_all = 1;	
	my @folder_list=("c:/program files/apache group/apache/cgi-bin","c:/mlink/akron","c:/program files/apache group/apache/htdocs/pdf");
	foreach my $folder (@folder_list) {
		unless (-d $folder) {
			$update_all = 0;
			LogMsg(indent(1)."Did not locate $folder - cannot update all scripts");
		}
	}
	LogMsg("Checking for updated files...");
	# Check for updated perl modules 
	
	LogMsg(indent(1)."Day of week: $day_of_week");
	LogMsg(indent(1)."Store Num: $storenum");
	LogMsg(indent(1)."Update All: $update_all");
	LogMsg(indent(1)."Precheck Only: $precheck_only");
	if ($day_of_week == 1) {
		if ($storenum > 1000) {
			if ($update_all) {
				LogMsg(indent(2)."Expect to update all today...");
			} else {
				LogMsg(indent(2)."Update all not selected");
			}
		} else {
			LogMsg(indent(2)."Not updating this store yet");
		}
	} else {
		LogMsg(indent(2)."Not updating today");
		$update_all = 0;
	}	
	if ($precheck_only) {
		if ($update_all) {
			LogMsg(indent(2)."Expect to update all today...");
		}
	}
	
	if (($update_all) || (($precheck_only) && ($update_all)))  {		 
		# Once a week update all of the scripts
		LogMsg(indent(1)."Checking All Script Updates");
		$warns+=check_script_updates();
	} else {
		# Otherwise, just make certain the perl modules are current
		LogMsg(indent(1)."Checking for Perl Module Updates");
		$warns+=check_perl_updates();
	}	
	LogMsg(indent(1)."Getting updates of register utilities...");
	my $source="//Edna/temp/install/register_utilities";
	my $target="/temp/scripts";
    my $temp_target_file="${target}/temp_monitor.pl";    
    my $monitor_target_file; 
    my $basename=basename($0);
    my $file_ok=0;
	# Make certain that the target exists
	if (-d $target) {
		LogMsg("Found $target");
	} else {
		mkdir ($target);
		unless (-d $target) {
			LogMsg("ERROR: Failed to create $target");
			$warning_count++;
			return;
		}
	}
	if (-d $source) {
		# We get a listing of the files available
		opendir(SOURCE,"$source");
		my @source_dir=readdir(SOURCE);
		close(SOURCE);
		foreach my $file (@source_dir) {
			next if ($file eq ".");
			next if ($file eq "..");
			next unless (
				($file =~ /.pl$/) || 
				($file =~ /.bat$/) || 
				($file =~ /.reg$/) || 
				($file =~ /.xml$/) || 
				($file =~ /.docx$/) || 
				($file =~ /.job$/) || 
				($file =~ /.ini$/) || 
				($file =~ /.txt$/) || 
				($file =~ /.exe$/) || 
				($file =~ /.ini$/) || 
				($file =~ /.rdp$/)
				);			
			my $source_file="${source}/${file}";
			my $target_file="${target}/${file}";
        
			if (-f "$source_file") {
				if ($verbose) {
					LogMsg("Found register file on Edna");
				}
				my @file_info=stat($source_file);
				my $file_date=$file_info[9];			
				if (-f "$target_file") {
					if ($verbose) {
						LogMsg("Found local file");
					}
					my @local_file_info=stat($target_file);
					my $local_file_date=$local_file_info[9];					
					if ($file_date > $local_file_date) {						 
						LogMsg("There is a newer version of $source_file available");		 
                        # If the file is the monitor.pl file itself, copy it to a temporary file						
                        if ($file eq "$basename") {               
							LogMsg("Found script to update self");
    						if (copy($source_file,$temp_target_file)) {
    							$update_count++;
                                $updating_self++;
                                $monitor_target_file=$target_file;                          
								LogMsg("Wrote to $temp_target_file");    						 
    						} else {
    							LogMsg("WARNING: Error attempting to create $temp_target_file");
    							$warns++;
    						}                        
                        } else {
    						if (copy($source_file,$target_file)) {
    							$update_count++;
    							if ($verbose) {
    								LogMsg("Updated $target_file");
    							}
    						} else {
    							LogMsg("WARNING: Error attempting to update $target_file");
    							$warns++;
    						}
                        }
					} else {
						if ($verbose) {
							LogMsg("$file is not newer");
						}
					}
				} else {	
					LogMsg("Failed to locate local file $target_file");
					if (copy($source_file,$target_file)) {
						$update_count++;
						if ($verbose) {
							LogMsg("Copied $target_file to $target_file");
						}
					} else {
						LogMsg("WARNING: Error attempting to copy $target_file");
						$warns++;
					}						
				}
			} else {
				LogMsg("WARNING: Failed to locate file on Edna");
				$warns++;	
			}			
		}
	} else {
		LogMsg("NOTICE: Failed to locate $source");
		#$warns++;
	}
    if ($updating_self) {
        # Make certain that the new perl script will not fail miserably
        
        if (-f $temp_target_file) {
			LogMsg("Checking $temp_target_file...");
            my $cmd="perl -w -c $temp_target_file 2>&1";            
            my @info=`$cmd`;          
            foreach my $i (@info) {
                if ($i =~ /syntax OK/i) {                    
                    $file_ok=1;
                }                
            }             
        }    
        if ($file_ok) {
            # The new monitor script looks like it will run so replace the original
            if (copy($temp_target_file,$monitor_target_file)) { 
                LogMsg("Updating $monitor_target_file");
                if ($verbose) {
                    LogMsg("Wrote to $monitor_target_file");
                }
            } else {
                LogMsg("WARNING: Error attempting to create $monitor_target_file");
                $warns++;
            }            
        } else {
            LogMsg("WARNING: New monitor script has syntax issues");
            $warns++;
        }
    }
	
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking for updates");
		$warning_count++;
	} else {
		LogMsg("Finished checking for updates: $update_count updates");

		if ($update_count) {
			# If we got new updates, then it may well be that we need to run a check that was modified.  We don't know which 
			# check may have been modified so we clear the record for all of them.  This way, it will run the next time the
			# script is executed.
			reset_record();		
		}
	}
    if ($file_ok) {    
        # Call the new script here unless this script was called with the update option
		# If the script was called with the update option, the intention was only to update it and not run the updated script.		
		unless ($check_for_update) {
			# Call the new regmonitor with the original arguments
			my $cmd = "$0 @arguments";
			LogMsg("Calling $0 here...");
			system($cmd);
			# Exit the initial version of the script.
			exit;
		}
    } else {
        # This means that there is either no new monitor script or it failed the syntax check so just run this current version
        $checks_enabled=1;        
    }
}

sub check_perl_updates {
	LogMsg("Check for Perl update...");
	my $warns=0;
	my $target_dir="C:/perl/site/lib/TTV";
	my $pm_source="//edna/temp/install/scripts/pm";
	my $temp_dir="//edna/temp";
 	if (-d $temp_dir) {
		my %pm_hash=();
		if (-d $pm_source) {
			if (-d $target_dir) {
				# Read the source dir
				opendir(PM,$pm_source);
				my @dir_listing=readdir PM;
				close PM;
				foreach my $file (@dir_listing) {
					next if ($file eq ".");
					next if ($file eq "..");				
					my $full_path="${pm_source}/$file";
					my $cmd="md5 $full_path";
					my $sum=`$cmd`;
					my @tmp=split(/\s+/,$sum);
					$pm_hash{$file}=$tmp[0];
				}
				# Read the target dir
				opendir(PM,$target_dir);
				my @target_dir_listing=readdir PM;
				close PM;			
				foreach my $source_file (keys(%pm_hash)) {
					my $file_found=0;			
					foreach my $file (@target_dir_listing) {
						next if ($file eq ".");
						next if ($file eq "..");			
						my $full_path="${target_dir}/$file";
						my $cmd="md5 $full_path";
						my $sum=`$cmd`;
						my @tmp=split(/\s+/,$sum);
						my $current_sum=$tmp[0];
						if ($source_file eq $file) {
							# The file exists so see if the file is the same
							unless($current_sum eq $pm_hash{$source_file}) {
								LogMsg("$file needs to be updated in $target_dir");
							} else {
								if ($verbose) {
									LogMsg("$file is already current in $target_dir");
								}
								$file_found=1;
							}
							
						}  				
					}	
					unless ($file_found) {
						LogMsg("Need to copy $source_file to $target_dir");
						my $full_path="${pm_source}/$source_file";
						my $full_target="${target_dir}/$source_file";
						unless ($debug_mode) {
							copy($full_path,$full_target);			
						}
						if ($verbose) {
							LogMsg("Copy $full_path to $full_target");
						}
					}
				}
			} else {
				LogMsg("WARNING: Could not locate $target_dir");
				$warns++;	
			}
		} else {		
			LogMsg("WARNING: Found $temp_dir but could not locate $pm_source");
			$warns++;			
		}
	} else {
		LogMsg("NOTICE: Could not locate $temp_dir");
	}
	return $warns;
}

sub check_script_updates {
	unless ($force) {
		if ($day_of_week == 1) {
			if ($current_hour < 6) {
				LogMsg("Check for script update...");	 
			} else {
				LogMsg("Not checking for script updates because it is too late in the day");
				return 0;
			}	
		}
	}
	my $warns=0;
	my $manifest = "//edna/temp/store_manifest.txt";
	my $update_count = 0;
	my %source_sum_hash = ();
	my %manifest_sum_hash = ();
	my %target_hash=();
	LogMsg("Checking for script updates...");
	# Reading the manifest on the Edna server gives us a list of known files 
	# and known locations for those files.  It also gives a checksum for the
	# current version of that file.  Here we read through that manifest and 
	# build a hash that contains the files and their locations and a second
	# has that contains the files and their current checksum.
	if (-f $manifest) {
		if ($verbose) {
			print "Reading $manifest...\n";
		}
		open(FILE,"$manifest");
		my @manifest_info=(<FILE>);
		close FILE;
		foreach my $m (@manifest_info) {
			my @tmp=split(/\*/,$m);			
			@tmp=split(/,/,$tmp[1]);
			my $file=lc($tmp[0]);
			my $target_dir=$tmp[1];			
 	
			$target_hash{$file}=$target_dir;
			# Does this file currently exist on this register?  If so, get the checksum
			my $full_path="${target_dir}/$file";
			if (-f $full_path) {
 
				my $cmd="md5 \"$full_path\"";
				my $sum=`$cmd`;
				@tmp=split(/\s+/,$sum);
				$manifest_sum_hash{$file} = $tmp[0];			
			}
		}
	} else {
		LogMsg("WARNING: Failed to locate $manifest");		
		return 1;
	}
 
	my %excluded_files= (
		'regmonitor.pl' => '1'
	);
	my $target_dir=" ";
	my $source="//edna/temp/install/scripts";
	my %source_hash = ();
	# The source folder is the intermediate or staging folder on the edna server.  We read
	# that folder and get a list of the files there and their checksums.
	my $source_ref=read_source("$source");
	if ($source_ref) {
		%source_hash=%$source_ref;
	}
	if ($verbose) {
		print "Iterating through source hash...\n";
	}
	# Now we iterate through that source_hash (the list of files and their current location on Edna)
	# We use that location to go find the find the file on this register and then to get the
	# checksum of that file.  If they do not match, we copy the file from that staging folder on Edna
	# to the Register.
	foreach my $s (keys(%source_hash)) {
		my $file = lc($s);	
		my $source_dir = $source_hash{$s};
		my $full_path="${source_dir}/$s";
		my $cmd="md5 \"$full_path\"";
		my $sum=`$cmd`;
		my @tmp=split(/\s+/,$sum);
		my $source_sum=$tmp[0];
		my $needs_update=0;
		next if ($excluded_files{$s});
		if ($manifest_sum_hash{$file}) {
			my $current_sum = $manifest_sum_hash{$file};
			unless ($current_sum eq $source_sum) {
				#LogMsg("$file is out of date and needs to be updated");
				$needs_update=1;
				if ($verbose) {
					print "Sum for $file is $source_sum\n";
					print "Sum for current version of $file is $current_sum\n";
				}
			}
 
		}
		if ($needs_update) {
			if ($verbose) {
				print "$file needs to be updated\n";
			}
			my $target=0;
			if (defined($target_hash{$file})) {
				$target = $target_hash{$file};
			}
 
			if ($target) {	
				if ($verbose) {
					print "Target is $target\n";
				}			
				# This target should be a directory
				unless (-d $target) {
					if ($verbose) {
						print "Need to make folder $target\n";
					}
					mkdir $target;
				} 
				if ($verbose) {
					print "Copy from $full_path to $target...\n";
				}
				copy($full_path,$target);
				# Check to see that the file is not there
				my $target_file = "${target}/${file}";
				if (-f $target_file) {
					$update_count++;
					#if ($verbose) {
						LogMsg(indent(2)."Copied $full_path to $target");
						#print "Copied $full_path to $target\n";
					#}
				} else {					
					LogMsg("WARNING: Failed to successfully copy $file to $target_file");		
					$warns++;
				}
				
			} else {
				if ($verbose) {
					LogMsg("NOTE: Failed to determine target directory for $file");
					# Log a notice but it may be a garbage file so no error.
				}				 
			}	
		}		 		
	}
	
 
	LogMsg("Updated $update_count files");
	return $warns;
}

sub read_source {
	my $source=shift;	
	my $level=shift;
	 
	unless ($level) {
		$level=1;
	} else {
		$level++;
	}
	
	if ($level > 5) {
		return 0;
	}
	my %source_hash=();	# Hash where the index is the file name and the value is the path where it is found
	opendir(SOURCE,$source);
	my @dir_listing=readdir SOURCE;
	close SOURCE;	
	foreach my $d (@dir_listing) {
		next if ($d eq ".");
		next if ($d eq "..");	
		 
		my $full_path = "${source}/${d}";
		if (-d $full_path) {
			 	
			# Get the files in this directory
			my $ref=read_source("$full_path",$level);
			if ($ref) {
				my %tmp_hash=%$ref;
				# Add the tmp_hash to the source_hash
				@source_hash{keys %tmp_hash} = values %tmp_hash;
				#foreach my $f (keys(%tmp_hash)) {
				#	$source_hash{$f} = $tmp_hash{$f};
				#}
			}
		}
		if (-f $full_path) {
			 		
			$source_hash{$d} = $source;
		}
	}
	my $key_count=keys(%source_hash);
 
	return \%source_hash;
}

sub check_scheduled_tasks {
	if ($OS eq "Win7") {
		check_scheduled_tasks_win7();
		return;
	}
	unless ($OS eq "WinXP") {
		LogMsg("Check Scheduled Tasks function is not supported by $OS");
		return;
	}
	LogMsg("Check scheduled tasks...");
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=30;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="tasks";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless (((($timeseconds - $last_pass)/86400) > $freq) || ($force)) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			return;
		}
	}	
	# I am revising the scheduled task that calls this script.  Here we check that the new task is in place and then
	# remove the old one.
	
	unless(check_new_task("$new_task")) {
		set_task();
		unless(check_new_task("$new_task")) {
			LogMsg("WARNING: Failed to create $new_task");
			$warning_count++;	
			return;
		}		
	} else {
		# The new task is already in place
		update_action_log($check,$warns);
		$monitor_record_hash{$check}=$timeseconds;	
	}
	if (check_old_task("$old_task")) {
		delete_task("$old_task");
		if (check_old_task("$old_task")) {
			LogMsg("WARNING: Failed to delete $old_task");
			$warning_count++;
		}  
	}
	check_posmonitor_task();	# Check that the posmonitor.pl task is present
	
	if (1) {	# Remove unwanted tasks
		###
		# Check scheduled task
		###
		LogMsg(indent(1)."Checking for Scheduled Tasks to Remove...");
		my @delete_list = (
			"Microsoft Windows XP End of Service Notification"
		);
		my $cmd = "schtasks /query /fo list /v";
		my @cmd_info = `$cmd`;
 
		my @action_list=();
		my $found_task_to_delete = 0;	
		foreach my $c (@cmd_info) {
			chomp($c);		
			if ($c =~ /taskname:/i) {
				foreach my $d (@delete_list) {
					if ($c =~ /$d/) {						
						my @tmp=split(/:/,$c);
						my $task=$tmp[1];
						while ($task =~ /^\s/) {						
							$task=~ s/^ //g;						
						}						
						push(@action_list,$task);
					}
				}
 
			} 
		}
		foreach my $a (@action_list) {
			my $cmd="schtasks /delete /tn \"$a\" /F";
			LogMsg(indent(1)."Removing task $a");			
			my @result=`$cmd`;
			foreach my $r (@result) {
				LogMsg(indent(2)."$r");
			}			
		} 
	}	
}

sub check_new_task_n {
	LogMsg("Checking new scheduled tasks...");
	# This monitoring task was originally a weekly task.  This function creates a weekly task.  (WARNING This function is not as 
	# generic as it may first appear since the directories and sub directories are not handled very well.)
	my $requested_task=shift;
	
	# Check the current tasks
	my $warns=0;
	my @requested_task_info=split(/\//,$requested_task);	
	
	my $expected_script_drive="$requested_task_info[0]";
	my $expected_script_dir="$requested_task_info[1]";
	my $expected_script_folder="$requested_task_info[2]";
	my $expected_script="$requested_task_info[3]";
	my $found_expected=0;
	my $cmd="SCHTASKS /query /fo csv /v";
	my @task_info=`$cmd`;
	foreach my $t (@task_info) {
		my @tmp=split(/\,/,$t);
		next unless ($#tmp > 0);		
		my $task_name='';
		$task_name=$tmp[1];
		$task_name=~ s/"//g;
		my $script='';
		$script=$tmp[11];
		$script=~ s/"//g;
		$script=~ s/ //g;
		$script=lc($script);
		if ($task_name eq "monitor") {
			LogMsg(indent(1)."Found monitor task ");
			my @script_info=split(/\//,$script);

			if ($script_info[0] eq "$expected_script_drive") {
				if (defined($expected_script_dir) && ($script_info[1] eq "$expected_script_dir")) {
					if ($script_info[2] eq "$expected_script_folder") {				
						if ($script_info[3] eq "$expected_script") {
							LogMsg(indent(1)."monitor task is configured to run $expected_script");
							$found_expected=1;
						} else {							
							LogMsg(indent(1)."WARNING: monitor task is NOT configured to run $expected_script");
							$warns++;						
						}
					} else {
						LogMsg(indent(1)."WARNING: monitor task is NOT configured to run $expected_script in $expected_script_folder");
						$warns++;
					}
				} else {
					LogMsg(indent(1)."WARNING: monitor task is NOT configured to run $expected_script in $expected_script_dir");
					$warns++;										
				}
			} else {				
				LogMsg(indent(1)."WARNING: monitor task is NOT configured to run $expected_script on $expected_script_drive");
				$warns++;													
			}
 	
			if ($script eq $expected_script) {
				LogMsg(indent(1)."Found expected script: $script");
			}
		}
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking tasks");
		
	}
	return $found_expected;
	LogMsg("Finished checking scheduled tasks");
}


sub check_new_task {
	LogMsg("Checking new scheduled tasks...");
	# This monitoring task was originally a weekly task.  This function creates a weekly task.  (WARNING This function is not as 
	# generic as it may first appear since the directories and sub directories are not handled very well.)
	my $requested_task=shift;
	
	# Check the current tasks
	my $warns=0;
	my @requested_task_info=split(/\//,$requested_task);	
	
	my $expected_script_drive="$requested_task_info[0]";
	my $expected_script_dir="$requested_task_info[1]";
	my $expected_script_folder="$requested_task_info[2]";
	my $expected_script="$requested_task_info[3]";
	my $found_expected=0;
	my $cmd="SCHTASKS /query /fo list /v";
	my @task_info=`$cmd`;
    my $trigger_found=0;
	foreach my $line (@task_info) {
        chomp($line);
		if ($verbose) {
			print "$line\n";
		}
        if ($line =~ /^TaskName/) {
			if ($verbose) {
				print ">>>>>>> $line\n";
			}
            if ($line =~ /monitor$/) {
                $trigger_found=1;
				if ($verbose) {
					LogMsg("Found monitor task");
				}
            } else {
                $trigger_found=0;
            }
        }
        if ($trigger_found) {            
            if ($line =~ /^Task To Run/) {
                if ($line =~ /regmonitor.pl/) {
                     $found_expected=1;  
					LogMsg(indent(1)."Found TaskName: monitor Task: regmonitor.pl");
                }  
            }        
        }        

	}
 
	return $found_expected;
	LogMsg("Finished checking scheduled tasks");
}


sub set_task {
	# This function will create a scheduled task to run the regmonitor.pl script
	LogMsg("Setting monitor task...");
	my $cmd="SCHTASKS /Create /SC daily /TN monitor /ST 03:00:00 /TR c:/temp/scripts/regmonitor.pl /RU administrator /RP ****";
	my @cmd_info=`$cmd`;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
	}	
}

sub check_posmonitor_task {
	# This function will create a scheduled task to run the regmonitor.pl script
	# First check to see if it is already present
	LogMsg("Checking posmonitor scheduled tasks...");
 
	# Check the current tasks
	my $option=shift;
	my $warns=0;
	my $schtasks_bat="C:/Documents and Settings/Villages/Start Menu/Programs/Startup/posmonitor.bat";
	my $posmonitor_job="c:/temp/scripts/posmonitor.job";
	my $posmonitor_task="c:/windows/tasks/posmonitor.job";
	
	my $found_expected=0;
	my $cmd="SCHTASKS /query /fo list /v";
	my @task_info=`$cmd`;
    my $trigger_found=0;
	foreach my $line (@task_info) {
        chomp($line);
		if ($verbose) {
			print "$line\n";
		}
        if ($line =~ /^TaskName/) {
			if ($verbose) {
				print ">>>>>>> $line\n";
			}
            if ($line =~ /posmonitor$/) {
                $trigger_found=1;
				if ($verbose) {
					LogMsg("Found monitor task");
				}
            } else {
                $trigger_found=0;
            }
        }
        if ($trigger_found) {            
            if ($line =~ /^Task To Run/) {
				if ($verbose) {
					print "Task to Run: ($line)\n";
				}
                if ($line =~ /posmonitor.pl/) {
                     $found_expected=1;  
					LogMsg(indent(1)."Found TaskName: monitor Task: posmonitor.pl");
                }  
            }  
			if ($line =~ /^Status/) {
				if ($line =~ /could not start/i) {
					LogMsg(indent(1)."WARNING: posmonitor.pl could not start");
					$warns++;
				}				
			}
        }        
	}
 
	if ($found_expected) {
		LogMsg("System Info: posmonitor: Yes");
	} else {
		if ($option) {
			LogMsg("System Info: posmonitor: No");	 	
			LogMsg("WARNING: No POS Monitor found");
			$warns++;
		} else {
			if (-f $posmonitor_job){
				if (-f $posmonitor_task) {
					LogMsg("WARNING: Found $posmonitor_task but did not expect to");
					$warns++;
				} else {
					copy($posmonitor_job,$posmonitor_task);
					if (-f $posmonitor_task) {
						LogMsg(indent(1)."Copied in $posmonitor_task");
						# Set the permission
						my $cmd="cacls $posmonitor_task";
						my @info=`$cmd`;
						my $found_villages=0;
						foreach my $i (@info) {
							chomp($i);						
							if ($i =~ /$hostname/i) {
								if ($i =~ /Villages.F/i) {
									$found_villages=1;
								}
							}												
						}
						unless ($found_villages) {
							LogMsg(indent(1)."Setting permissions for $posmonitor_task");
							$cmd="cacls $posmonitor_task /T /E /G Villages:F";
							system($cmd);
							# Check permissions again;
							$cmd="cacls $posmonitor_task";
							@info=`$cmd`;
							$found_villages=0;
							foreach my $i (@info) {
								chomp($i);							
								if ($i =~ /$hostname/i) {							
									if ($i =~ /Villages.F/i) {
										$found_villages=1;
									}
								}												
							}
							if ($found_villages) {
								LogMsg(indent(1)."Successfully set permissions for $posmonitor_task");
							} else {
								LogMsg(indent(1)."WARNING: Failed to set permissions for $posmonitor_task");
								$warns++;
							}
						}
					} else {
						LogMsg(indent(1)."WARNING: Failed to copy in $posmonitor_task!");
						$warns++;
					}
				}
			} else {
				LogMsg(indent(1)."WARNING: Failed to locate $posmonitor_job");
				$warns++;
			}		
		}
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking scheduled task for posmonitor.");
		$warning_count++;
	}
	LogMsg("Finished checking scheduled tasks for posmonitor");	
}

sub check_old_task {
	LogMsg("Checking old scheduled tasks...");
	# This monitoring task was originally a weekly task.  This function creates a weekly task
	my $requested_task=shift;
	
	# Check the current tasks
	my $warns=0;
	my @requested_task_info=split(/\//,$requested_task);	
	 
	my $expected_script_drive="$requested_task_info[0]";
	my $expected_script="$requested_task_info[1]";
	my $found_expected=0;
	my $cmd="SCHTASKS /query /fo list /v";
	my @task_info=`$cmd`;
	my %setting_hash = (
		'TaskName:' => '',
		'Task To Run:' => ''
	);
    my $trigger_found=0;
	foreach my $line (@task_info) {
        chomp($line);
        if ($line =~ /^TaskName/) {
            if ($line =~ /everyweek$/) {
                $trigger_found=1;
            } else {
                $trigger_found=0;
            }
        }
        if ($trigger_found) {            
            if ($line =~ /^Task To Run/) {
                if ($line =~ /rmonitor.bat/) {
                     $found_expected=1;                     
                }  
            }        
        }        

	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking tasks");
		
	}
	return $found_expected;
	LogMsg("Finished checking scheduled tasks");
}

sub delete_task {
	# This function removes the original task
	LogMsg("Deleting everyweek task...");
	my $cmd="SCHTASKS /Delete /TN everyweek /f";
	my @cmd_info=`$cmd`;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
	}		
	
}

sub schedule_update_win7 {
	# Check to see if an update is scheduled
	my @at_info=`at`;

	# Schedule the update here
	my $update_tool="C:/Program Files/Symantec/Symantec Endpoint Protection/SepLiveUpdate.exe";	
	my $tool_exe="SepLiveUpdate";
 
 
	foreach my $a (@at_info) {
		chomp($a);
		if ($a =~ /$tool_exe/) {
			LogMsg("Live Update is already scheduled");
			LogMsg("$a");
			return;
		}
	} 
	if (-f $update_tool) {
		LogMsg(indent(1)."Scheduling virus definition update for 6:00...");
		my $cmd="at 6:00 \"$update_tool\" -s";	
		system($cmd);		 
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $update_tool");
		return 1;
	}   
}
 				
sub schedule_update {
	# Check to see if an update is scheduled
	my @at_info=`at`;

	# Schedule the update here
	my $update_tool="c:/program files/symantec antivirus/vpdn_lu.exe";
	$update_tool="C:/Program Files/Symantec/LiveUpdate/Luall.exe";
	my $tool_exe="Luall";
	# SEP12 uses SEPLiveUpdate.exe
	my $base_dir="c:/program files/symantec antivirus";
	if (-d $base_dir) {
		LogMsg("Found $base_dir");
		# Need to determine if there is a directory in here for SEP12
 
		opendir(SOURCE,$base_dir);
		my @dir_listing=readdir SOURCE;
		close SOURCE;	
		foreach my $d (@dir_listing) {
			my $dir2 = "${base_dir}/${d}";
			if (-d $dir2) {
				if ($d =~ /^\d/) {
					my $bin_dir="${dir2}/bin";
					if (-d $bin_dir) {
						my $tool="${bin_dir}/SepLiveUpdate.exe";
						if (-f $tool) {
							$update_tool = $tool;
							$tool_exe="SepLiveUpdate";
							LogMsg("Update Tool: ($tool_exe)");
							
						}
					}
				}
			}
		}
	} else {
		LogMsg("WARNING: Failed to locate $base_dir");
	}
	foreach my $a (@at_info) {
		chomp($a);
		if ($a =~ /$tool_exe/) {
			LogMsg("Live Update is already scheduled");
			LogMsg("$a");
			return;
		}
	} 
	if (-f $update_tool) {
		LogMsg(indent(1)."Scheduling virus definition update for 6:00...");
		my $cmd="at 6:00 \"$update_tool\" -s";	
		system($cmd);		 
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $update_tool");
		$warning_count++;
	}   
}

sub schedule_reboot {
	# Schedule a reboot
	# Schedule for 2 minutes from now.
	my @ltm = localtime();
	my $current_hour=$ltm[2];
	my $current_minute=$ltm[1];
	my $schedule_minute = sprintf("%02d",($current_minute + 2));
	my $schedule_time="$current_hour".":"."$schedule_minute";
	
	my $request="shutdown -r";
	my $cmd;
	if ($current_hour < 7) {
		$cmd="at $schedule_time \"$request\" /s";
	} else {
		$cmd="at 4:00 \"$request\" /s";	
	}
 
	LogMsg(indent(1)."NOTICE: Scheduling reboot ($cmd)...");	
	system($cmd);		
   
}
 
sub check_antivirus_win7 {
	LogMsg("Determining version of Symantec Antivirus...");
	my $antivirus_version=check_antivirus_version(); 
	my $expected_version = 12;
    my $version;	
	my $warns=0;
	if ($antivirus_version =~ /^$expected_version/) {
		LogMsg("Found version: $antivirus_version");
		$version=$expected_version;
	} else {
		LogMsg("WARNING: Found antivirus version $antivirus_version but expected $expected_version");
		$warns++;
		$warning_count+=$warns;
		return;
	}
	 
	$warns+=check_antivirus_definitions_win7("$version");
	
	$warns+=check_antivirus_scan_win7("$version");	
	$warns+=check_antivirus_scan_results();	
	
	my $antivirus_log_dir="C:/Users/POS Support/AppData/Local/Symantec/Symantec Endpoint Protection/Logs";	
	$warns+=check_antivirus_log("$antivirus_log_dir");
	
	if ($warns) {
		LogMsg("WARNING: Found $warns checking antivirus");
		$warning_count++;
	}
	
 }
 
sub check_antivirus_definitions_win7 { 
    LogMsg("Checking antivirus - Symantec Endpoint Protection");
	my $warns=0;
	my $version=shift;;	
	# Check that the installation is as unmanaged
	my $sep_found=0;
	my $smc_found=0;
	my $smc_managed=0;
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\"  ";    
	my @info=`$cmd`;
	unless ($?) {
		foreach my $i (@info) {        
			if ($i =~ /Symantec Endpoint Protection/) {						
				$sep_found = 1;			
			}
		} 
	}                        
		
	if ($sep_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\"  ";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {        
				if ($i =~ /SMC/) {						
					$smc_found = 1;					
				}
			}		

		}                        
	}
	if ($smc_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\SMC\" /s";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {
				if ($i =~ /CommunicationStatus/) {	
					$smc_managed=1;				 				
				}						 
			}		

		}                        
	}	
	if ($smc_managed) {
		LogMsg(indent(1)."WARNING: Symantec Endpoint Protection is installed as managed");
		$warns++;
	}	
 
	# Determine directory
 
	my $antivirus_dir="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection";
	my $version_dir="";
	opendir(UPLOAD,"$antivirus_dir");
	my @dir_list=readdir(UPLOAD);		
	close UPLOAD;		
	foreach my $d (@dir_list) {
		if ($d =~ /$version/) {
			$version_dir=$d; 
		}
	}
	# Check that the antivirus definitions are up to date
	LogMsg("Checking Symantec antivirus definitions");
	
    my %def_hash=(
        'VirusDefs' => 'Virus',
        'BASHDefs' => 'Proactive Threat',
        'IPSDefs' => 'Network Threat',
    );
    my %def_limit_hash=(
        'VirusDefs' => '22',
        'BASHDefs' => '60',
        'IPSDefs' => '22',
    );	
    for my $def_dir (keys (%def_hash)) {
	 
        my $def_label=$def_hash{$def_dir};
	 
        # Check the date of the antivirus definitions					   
        my $antivirus="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/$version_dir/Data/Definitions/$def_dir/definfo.dat";
 
        if (-f $antivirus) {
            my $virus_updated=0;			
            my @ltm = localtime();		
            my $month=$ltm[4]+1;
            my $year=$ltm[5]+1900;	
            my $last_month=($month - 1);
            my $last_month_year=$year;
            if ($month == 1) {
                $last_month=12;
                $last_month_year=($year - 1);
            }
            # Look for the log files for this month
            open(DEF,"$antivirus");
            my @file_info=(<DEF>);
            close DEF;
            if ($verbose) {
                LogMsg(indent(1)."Read contents of $antivirus");
            }
            my $deftime;

            foreach my $line (@file_info) {	
                if ($verbose) {
                    LogMsg(indent(1)."Scanning line $line...");
                }			
     
                if ($line =~ /CurDefs/) {
                    if ($verbose) {
                        LogMsg("$line");
                    }
                    # What is the date of the current defs
                    my @tmp=split(/=/,$line);
                    my $date=$tmp[1]; 
                    my $defyear=substr($date,0,4);
                    my $defmonth=substr($date,4,2);
                    my $defday=substr($date,6,2); 
                    if ($verbose) {
                        LogMsg("Determined date to be year: $defyear month: $defmonth day: $defday");
                    }
                    my $timemonth=($defmonth - 1);								
                    $deftime = timelocal(0,0,0,$defday,$timemonth,$defyear);	
                    # Calculate how old the definitions are
                    my $timeseconds=time();
                    my $timediff=($timeseconds - $deftime);	# This is how many seconds old the definitions are.
                    # The definitions should not be more than two weeks old if they are set to update daily.
                    # If they are more than two weeks old and they are not set to update daily, set daily updates.
                    # If they are set to weekly and they never get set to daily, then the warning will go out after three weeks.
                    my $limit=(86400 * $def_limit_hash{$def_dir});
 
                    my $notice_limit=(86400 * 15);
                    if (($timediff > $notice_limit) && ($timediff < $limit)) {
                        LogMsg(indent(1)."NOTICE: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");						
                        schedule_update_win7();                    
                    } elsif  ($timediff > $limit) {           
                        LogMsg(indent(1)."WARNING: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");
                        $warns++;                        
						schedule_update_win7();                    
                    } else {					 
                        LogMsg(indent(1)."$def_label definitions are from ${defyear}-${defmonth}-${defday}");					 
                    }	                    
                }
            } 
            unless ($deftime) {
                LogMsg(indent(1)."WARNING: SAV Failed to determine date of $def_label definitions");
                $warns++;									
            }		
        } else {
            LogMsg(indent(1)."WARNING: Did not find $antivirus");
            $warns++;             
        }            
    }
	
	return $warns;
	
} 
	
	

sub check_antivirus_scan_win7 {
	# Look for the CustomScheduledScan
	my $version=shift;
	my $found_custom_scan=0;
	my $scan_id=0;
	my $enabled=0;
	my $dow=0;
	my $mod=0;
	my $warns=0;
	my @found_user_list;
	my %enabled_user_hash;
	my $cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\TaskPadScheduled\" "; 	
	LogMsg("Checking antivirus scan...");
	if ($version =~ /^12/) {
		my @id_list;
		my @custom_id_list;
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\" "; 
		if ($arch eq "32-bit") {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\" "; 
		}

		my @info=`$cmd`;		
		foreach my $i (@info) {
			chomp($i);			
			my @tmp=split(/\\/,$i);
			my $id=$tmp[-1];
			if ($id) {
				push(@id_list,$id);
			}			
			if ($i =~ /CustomScheduledScan/) {				
				$found_custom_scan=1;
			}
		}			
		foreach my $i (@id_list) {
			my $found_custom_scan_user=0;
			my $user="unknown";
			$user=identify_user($i);			
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\TaskPadScheduled\" "; 						
			if ($arch eq "32-bit") {
				$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\TaskPadScheduled\" "; 						
			}	
	
			@info=`$cmd`;			
			foreach my $in (@info) {
				chomp($in);				
				if ($in =~ /CustomScheduledScan/) { 			
					$found_custom_scan=1;		# Set this to show we found a custome scheduled scan 
					$found_custom_scan_user=1;	# Set this to show we found one for this particular user					
				}
			}	
			if ($found_custom_scan_user) {
				LogMsg(indent(1)."Found CustomScheduledScan for $i - user $user");	
				push(@found_user_list,$user);
				# Find the scan id
				$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\" "; 						
				if ($arch eq "32-bit") {
					$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\" "; 						
				}					
				my $id=$i;
				@info=`$cmd`;			
				foreach my $in (@info) {
					chomp($in);				
					if ($in =~ /Default/) {
						
						my @tmp = split(/\s+/,$in);				
						$scan_id = $tmp[-1];						
						#LogMsg("Scan id for $id is $scan_id");
					}
				}	
 
			}
			if ($scan_id) {
				$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\$scan_id\\Schedule\" "; 						
				if ($arch eq "32-bit") {
					$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\$scan_id\\Schedule\" "; 						
				}	
				
				@info=`$cmd`;			
				foreach my $i (@info) {
					chomp($i);				
					
					
					if ($i =~ /Enabled/)  {											
						unless ($i =~ /Missed/) {
							my @tmp = split(/\s+/,$i);				
							$enabled = $tmp[-1];									
						}
					}
					if ($i =~ /DayOfWeek/) {											
						my @tmp = split(/\s+/,$i);				
						$dow = $tmp[-1];								
					}	
					if ($i =~ /MinOfDay/) {											
						my @tmp = split(/\s+/,$i);				
						$mod = $tmp[-1];						
					}		
				}
				# Evaluate what we found
				unless ($enabled eq "0x1") {
					LogMsg("WARNING: CustomScheduledScan is not enabled for user $user");
					$warns++;
				} else {
					$enabled_user_hash{"$user"}=1;					
				}
				unless ($dow eq "0x0") {
					LogMsg("WARNING: CustomScheduledScan is not set for Sunday for user $user");
					$warns++;
				} else {
					LogMsg(indent(1)."CustomScheduledScan is set for Sunday for user $user");
				}			

				#
 
				my $dec_mod = $mod;
				$dec_mod =~ s/0x//;
				$dec_mod = (hex($dec_mod)/60);
				if (($dec_mod eq "12") || ($dec_mod eq "0")) {
					LogMsg(indent(1)."CustomScheduledScan is set for $dec_mod");
				} else {
					LogMsg(indent(1)."NOTICE: CustomScheduledScan is set for $dec_mod");
				}
			
				if ($verbose) {
					if ($enabled) {
						LogMsg(indent(1)."Scan is enabled");
					}
					if ($dow eq "0x0") {
						unless ($mod eq "0x0") {
							my $dec_mod = $mod;					
							$dec_mod =~ s/0x//;
							$dec_mod = (hex($dec_mod)/60);
							LogMsg(indent(1)."Scan runs on Sunday at $dec_mod");					
						} else {
							LogMsg(indent(1)."Scan runs on Sunday ");
						}
					}
				}												
			}
		}
		# We want to find a custom scan for POS Support but not for villages.
		if ($found_custom_scan) {
			foreach my $user (@found_user_list) {
				if ($user =~ /villages/i) {
					if ($enabled_user_hash{"Villages"}) {
						# This cannot be programmatically removed so we need to log a warning
						LogMsg(indent(1)."WARNING: Found CustomScheduledScan for $user");	 			
						$warns++;	
					}
				} elsif ($user =~ /POS Support/i) {
					LogMsg(indent(1)."Found CustomScheduledScan for $user");	 			
				}				
			}						
		} else {
			LogMsg("WARNING: Failed to find CustomScheduledScan");
			$warns++;							 
		}
	}

	return $warns;
	
}
 
 

sub check_antivirus { 
	if ($OS eq "Win7") {
		#LogMsg("check_antivirus is currently not supported for $OS");
		check_antivirus_win7();
		return;
	}
    my $warns=0;
	my $expected_version = 11;
	$expected_version = 12;
	LogMsg("Determining version of Symantec Antivirus...");
    my $antivirus_version=check_antivirus_version(); 
	my $install_enabled=0;
	my $success_flag="c:/temp/sep12_installed.txt";
	

	unless ($antivirus_version =~ /^$expected_version/) {
		#if (($storenum =~ /^$day_of_week/) || ($storenum > 7000)){
		if ($hostname =~ /Reg/i) {
			LogMsg("Hostname: $hostname");
			#if ((($storenum =~ /^4817/) ||  ($storenum =~ /^4756/)) && ($day_of_week > 0) && ($day_of_week < 6) ) {
			#if (($storenum =~ /^$day_of_week/) && ($day_of_week > 0) && ($day_of_week < 6) ) {
			if (($day_of_week > 0) && ($day_of_week < 6) ) {
				if ($current_hour < 7) {
					$install_enabled=1;	 
				} else {
					LogMsg("Install would be enabled but it is too late in the day");
				}
			}	
		} else {
			LogMsg("Hostname: $hostname");
		}
	 
		if ($force) {
			print "Enabling SEP upgrade...\n";
			$install_enabled=1;
		}
		####
		#LogMsg("NOTICE: SAV Expected version $expected_version but found $antivirus_version ");
		LogMsg("WARNING: SAV Expected version $expected_version but found $antivirus_version ");
		$warns++;		
		my $sourcefile="//Edna/temp/install/SEP11/SEP11.zip";	
		$sourcefile="//Edna/temp/install/SEP12/SEP12.zip";	
		if (-f $sourcefile) {
			if ($install_enabled) {
				backup_S32(); 
				LogMsg("Launching SEP upgrade...");
                sep_install($expected_version);

				if ( -f $success_flag)  {
                    LogMsg("SEP Install finished successfully");
					 
				} else {						
					LogMsg("WARNING: SEP Install was not successful");
					$warns++;
				}
				if (check_dlls()) {
					LogMsg("WARNING: Possible issues with DLLs");
					$warns++;				
				} else {
					LogMsg("DLLs look good");
				}	
			} else {
				LogMsg("NOTICE: $sourcefile is present but install is not enabled for today.");
			}					
		} else {
			LogMsg("NOTICE: Antivirus version $antivirus_version is installed but version $expected_version is expected. ($sourcefile not found)");
			#$warns++;			
		}			
	}
    if (($antivirus_version =~ /^11/) || ($antivirus_version =~ /^12/)){
		LogMsg("Checking Antivirus $antivirus_version");
        check_antivirus_sep($antivirus_version);
 
	} else {
		LogMsg("WARNING: SAV Antivirus version $antivirus_version is not supported");
        $warns++;
	}
    unless ($antivirus_version) {
        LogMsg("WARNING: SAV Failed to determine version of antivirus application");
        $warns++;
    }   
	LogMsg("Checking Antivirus Scan...");
	if ($antivirus_version =~ /^11/) {
		check_antivirus_scan();
	} elsif ($antivirus_version =~ /^12/) {
		# It seems that this scan did not survive the upgrade to SEP12.
		# Rather than install it "manually" on every register, I have created
		# schedule_do_scan to run a scan as directed by this script. - kdg
		#check_CustomScheduledScan_12();
	} else {
		LogMsg("WARNING: Undetermined antivirus version ($antivirus_version)");
		$warns++;
	}
	LogMsg("Checking Antivirus AutoUpdate");
	check_antivirus_autoupdates();
	if ($day_of_week == 0) {	
		$warns+=check_specific_files();		
	}
	$warns+=check_antivirus_scan_results();	
 
	if ($warns) {
		$warning_count++;
	}    
}
 
sub check_symantec_dll {
	my $dll_1 = "/program files/symantec/S32EVNT1.DLL";
	my $dll_1_age=0;
	my $dll_2 = "/windows/system32/S32EVNT1.DLL";
	my $dll_2_age=0;
	if (-f $dll_1) {
		my @file_info = stat $dll_1;		
		$dll_1_age = $file_info[9];
	} else {
		return "Cannot locate $dll_1";
	}
	if (-f $dll_2) {
		my @file_info = stat $dll_2;		
		$dll_2_age = $file_info[9];
	} else {
		return "Cannot locate $dll_2";
	}
	my $diff=abs($dll_1_age - $dll_2_age);
	return $diff;
} 
 
sub sync_symantec_dll {
	if ($verbose) {
		LogMsg("Sync Symantec DLL");
	}
	my $check=shift;
	unless (defined($check)) {
		$check=0;
	}
	my $warns=0;
	my $backup_version='';	 
	my $source_version='';		
	my $backup_ver='';
	my $backup_dll = "//Edna/temp/install/reg/S32EVNT1_v8.DLL";
	my $source_dll = "//Edna/temp/install/reg/S32EVNT1.DLL";	
	my $system_dll = "c:/windows/system32/S32EVNT1.DLL";	
	my $symantec_dll = "c:/Program Files/Symantec/S32EVNT1.DLL";		
	my $fix_dll_8=0;
	my $fix_dll_9=0;
	
	# Check the pcAnywhere DLLs
	my ($dll_version, $dll_symantec_version)=get_S32_version();
	my ($sys_version, $sys_symantec_version)=get_SYMEVENT_version(); 
	my ($dll_ver, $dll_sym_ver)=get_S32_version("base");
	my ($sys_ver, $sys_sym_ver)=get_SYMEVENT_version("base"); 	
 

	if ($verbose) {
		print "DLL: ($dll_version) Symantec: ($dll_symantec_version)\n";
		print "SYS: ($sys_version) Symantec: ($sys_symantec_version)\n";
	}	
	# The DLL and the SYS should be basically the same version
 
	if ($sys_ver eq $sys_sym_ver) {
		if ($verbose) {
			LogMsg("SYMEVENT.SYS is $sys_symantec_version");
		}
	} else {
		LogMsg("ERROR SYMEVENT.SYS has a version mismatch: $sys_version & $sys_symantec_version");
		#$warns++;
	}
	if ($dll_ver eq $dll_sym_ver) {
		if ($verbose) {
			LogMsg("S32EVNT1.DLL is $dll_symantec_version");
		}
	} else {
		LogMsg("ERROR S32EVNT1.DLL has a version mismatch: $dll_version & $dll_symantec_version");
		#$warns++;
	}	
	if ($dll_ver eq $sys_ver) {
		LogMsg("S32EVNT1.DLL is $dll_ver and SYMEVENT.SYS is $sys_ver");
	} else {
		LogMsg("ERROR S32EVNT1.DLL is $dll_ver but SYMEVENT.SYS is $sys_ver"); 
		#$warns++;
		if ($sys_ver =~ /^12.8/) {
			# DLL should be version 12.8 as well
			$fix_dll_8=1;
			if ($check) {
				return 0;
			}			
		}
		if ($sys_ver =~ /^12.9/) {
			# DLL should be version 12.9 as well
			$fix_dll_9=1;
			if ($check) {
				return 0;
			}
		}		
		
	}	
	
 
	if ($fix_dll_8) {
		LogMsg("Need to correct the DLL to 12.8");
		if (-f $backup_dll) {
			LogMsg("Found $backup_dll");
			# Need to copy the backup dll into the system32 folder
			# First need to stop pcAnywhere
			my $service="awhost32";
			if (stop_service($service)) {
				LogMsg(indent(1)."Stopped $service");
			} else {
				LogMsg("WARNING: Failed to stop $service");
				return 0;
			}		
	 
			copy($backup_dll,$system_dll);
			copy($backup_dll,$symantec_dll);
		 
 
			# Start pcAnywhere
			 
			if (start_service($service)) {
				LogMsg(indent(1)."Started $service");
			} else {
				LogMsg("WARNING: Failed to start $service");			 
				$warns++;
			}	
			
		} else {
			LogMsg("WARNING: Failed to locate $backup_dll");
			$warns++;
		}
	} elsif ($fix_dll_9) {
		LogMsg("Need to correct the DLL to 12.9");
		if (-f $source_dll) {
			LogMsg("Found $source_dll");
			# Need to copy the source dll into the system32 folder
			# First need to stop pcAnywhere
			my $service="awhost32";
			if (stop_service($service)) {
				LogMsg(indent(1)."Stopped $service");
			} else {
				LogMsg("WARNING: Failed to stop $service");
				return 0;
			}		
	 
			copy($source_dll,$system_dll);
			copy($source_dll,$symantec_dll);
 
			# Start pcAnywhere
			 
			if (start_service($service)) {
				LogMsg(indent(1)."Started $service");
			} else {
				LogMsg("WARNING: Failed to start $service");			 
				$warns++;
			}	
			
		} else {
			LogMsg("WARNING: Failed to locate $source_dll");
			$warns++;
		}	
	
	} else {
		LogMsg("DLL versions are fine.");
	}
	if ($warns) {
		LogMsg("WARNING: There were $warns issues checking DLL's");
		$warning_count++;
	}
 
	###
	return 1;
	###

	my $dll_1 = "/program files/symantec/S32EVNT1.DLL";
	my $dll_1_age=0;
	my $dll_2 = "/windows/system32/S32EVNT1.DLL";
	my $dll_2_age=0;
	if (-f $dll_1) {
		my @file_info = stat $dll_1;		
		$dll_1_age = $file_info[9];
	} else {
		return "Cannot locate $dll_1";
	}
	if (-f $dll_2) {
		my @file_info = stat $dll_2;		
		$dll_2_age = $file_info[9];
	} else {
		return "Cannot locate $dll_2";
	}
	
	my $diff=abs($dll_1_age - $dll_2_age);
	if ($diff) {
		unless(copy($dll_1, $dll_2)) {
			return "Cannot copy $dll_1 to $dll_2";
		}
	}
	return 0;
}  


sub check_antivirus_sep {
	if ($OS eq "Win7") {
		LogMsg("Check antivirus SEP is not supported by $OS");
		return;
	}
    my $version=shift;
	my $warns=0;
    LogMsg("Checking antivirus - Symantec Endpoint Protection $version");
	# Check the DLL is in sync	
	unless (sync_symantec_dll("check")) {
		LogMsg("Need to sync symantec DLL's"); 
		sync_symantec_dll(); 

		unless (sync_symantec_dll("check")) {
			LogMsg("WARNING: Symantec DLL's may still have a problem.");
			$warns++;			
		}

	} else {
		if ($verbose) {
			LogMsg(indent(1)."Symantec DLL is in sync");
		}
	}		
	if ($OS eq "WinXP") {
		unless (check_awhost32_excluded()) {
			if (1) {
				LogMsg("WARNING: pcAnywhere is not excluded from Symantec Endpoint Protection");
				$warns++;		
			}
			if (1) {
				my $fixfile_source="//Edna/temp/install/reg/awhost32_exclusion.reg";
				my $fixfile="c:/temp/scripts/awhost32_exclusion.reg";
				copy($fixfile_source,$fixfile);
				
				if (-f $fixfile) {			 
					my $cmd="regedit.exe -s $fixfile";
					LogMsg("Running $cmd");
					`$cmd`;
					if (check_awhost32_excluded()) {
						LogMsg("pcAnywhere was successfully excluded from SEP");
					} else {
						LogMsg("WARNING: Failed to exclude pcAnywhere");
						LogMsg("WARNING: pcAnywhere is not excluded from Symantec Endpoint Protection");
						$warns++;				
					}			
				} else {
					LogMsg("WARNING: Unable to locate $fixfile");
					$warns++;
				}	
			}
		}	
	}

		
	# Check that the installation is as unmanaged
	my $sep_found=0;
	my $smc_found=0;
	my $smc_managed=0;
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\"  ";    
	my @info=`$cmd`;
	unless ($?) {
		foreach my $i (@info) {        
			if ($i =~ /Symantec Endpoint Protection/) {						
				$sep_found = 1;			
			}
		}	
	}                        

	if ($sep_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\"  ";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {        
				if ($i =~ /SMC/) {						
					$smc_found = 1;					
				}
			}
		}                        
		
	}
	if ($smc_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\SMC\" /s";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {
				if ($i =~ /CommunicationStatus/) {	
					$smc_managed=1;				 				
				}						 
			}
		}                        
		
	}	
	if ($smc_managed) {
		LogMsg(indent(1)."WARNING: Symantec Endpoint Protection is installed as managed");
		$warns++;
	}
	LogMsg("Checking Symantec antivirus definitions");
	# Check that the antivirus definitions are up to date
	# The def_hash comes from SEP12 that we tried for a while.  It is not appropriate for SEP11 but I have not completely
	# removed in in case we eventually do go to SEP12 - kdg

    my %def_hash=(
        'VirusDefs' => 'Virus',
        'BASHDefs' => 'Proactive Threat',
        'IPSDefs' => 'Network Threat',
    );
	my %def_limit_hash=(
        'VirusDefs' => '22',
        'BASHDefs' => '60',	
	);	
	# We are typically only installing the antivirus 
    %def_hash=(
        'VirusDefs' => 'Virus', 	 
    );

	my $def_path="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/CurrentVersion/Data/Definitions";	
	if ($version =~ /^12/) {
		$def_hash{'BASHDefs'} = 'Proactive Threat',
	}
 
	my $antivirus_def = 0;
	my $proactive_threat_def=0;	
	foreach my $key (sort keys(%def_hash)) {
		my $def_limit = "$def_limit_hash{$key}";
		my $def_label = "$def_hash{$key}";
		my $definfo = "${def_path}/$key/definfo.dat"; 
		if (-f $definfo) {		 
			my $virus_updated=0;			
			my @ltm = localtime();		
			my $month=$ltm[4]+1;
			my $year=$ltm[5]+1900;	
			my $last_month=($month - 1);
			my $last_month_year=$year;
			if ($month == 1) {
				$last_month=12;
				$last_month_year=($year - 1);
			}
			# Look for the log files for this month
			open(DEF,"$definfo");
			my @file_info=(<DEF>);
			close DEF;
			if ($verbose) {
				LogMsg(indent(1)."Read contents of $definfo");
			}
			my $deftime;

			foreach my $line (@file_info) {	
				if ($verbose) {
					LogMsg(indent(1)."Scanning line $line...");
				}			
	 
				if ($line =~ /CurDefs/) {
					if ($verbose) {
						LogMsg("$line");
					}
					if ($def_label =~ /Virus/i) {
						$antivirus_def=1;
					}
					if ($def_label =~ /Proactive Threat/i) {
						$proactive_threat_def=1;
					}								
					# What is the date of the current defs
					my @tmp=split(/=/,$line);
					my $date=$tmp[1]; 
					my $defyear=substr($date,0,4);
					my $defmonth=substr($date,4,2);
					my $defday=substr($date,6,2); 
					if ($verbose) {
						LogMsg("Determined date to be year: $defyear month: $defmonth day: $defday");
					}
					my $timemonth=($defmonth - 1);								
					$deftime = timelocal(0,0,0,$defday,$timemonth,$defyear);	
			
					# Calculate how old the definitions are
					my $timeseconds=time();
					my $timediff=($timeseconds - $deftime);	# This is how many seconds old the definitions are.
					# The definitions should not be more than two weeks old if they are set to update daily.
					# If they are more than two weeks old and they are not set to update daily, set daily updates.
					# If they are set to weekly and they never get set to daily, then the warning will go out after three weeks.
					my $limit=(86400 * $def_limit);
					my $notice_limit=(86400 * 15);
					if (($timediff > $notice_limit) && ($timediff < $limit)) {
						LogMsg(indent(1)."NOTICE: SAV $def_label def are from ${defyear}-${defmonth}-${defday} CP12");						
						schedule_update();                    
					} elsif  ($timediff > $limit) {           
						LogMsg(indent(1)."WARNING: SAV $def_label def are from ${defyear}-${defmonth}-${defday} CP12");
						$warns++;    
						schedule_update(); 						
					} else {					 
						LogMsg(indent(1)."$def_label definitions are from ${defyear}-${defmonth}-${defday} CP12");					 
					}	                    
				}
			} 
			unless ($deftime) {
				LogMsg(indent(1)."WARNING: SAV Failed to determine date of $def_label definitions CP12");
				$warns++;									
			}		
		} else {
			LogMsg(indent(1)."WARNING: SAV Failed to find $definfo CP12");
			$warns++;             
		}  	
	}
	
	unless ($proactive_threat_def) {
		LogMsg(indent(1)."WARNING: SAV Proactive Threat Definitions date was not determined.");
		$warns++;
	}

	unless ($antivirus_def) {
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{1CD85198-26C6-4bac-8C72-5D34B025DE35}\"";
		@info=`$cmd`;  

		foreach my $i (@info) {        
			chomp($i);
			 
			if ($i =~ /CurrentSequenceNum#/i) {
			 
				my @tmp=split(/\s+/,$i);
				my $ptd=$tmp[-1];
				my $l=length($ptd);
				my $y; 
				my $m; 
				my $d; 
				my $r; 
				if ($l == 9) {
					$y = substr($ptd,0,2);
					$m = substr($ptd,2,2);
					$d = substr($ptd,4,2);
					$r = substr($ptd,6,3);			
				} elsif ($l == 8) {
					$y = substr($ptd,0,1);
					$m = substr($ptd,1,2);
					$d = substr($ptd,3,2);
					$r = substr($ptd,5,3);				
				}  
				if (($y) && ($m) && ($d)) {
					$y = sprintf("%02d",$y);
					$y = "20".$y;			
					$antivirus_def = "$y"."-"."$m"."-"."$d"; 
					my $timemonth=($m - 1);				 
					my $deftime = timelocal(0,0,0,$d,$timemonth,$y);
					my $timeseconds=time();
					my $timediff=($timeseconds - $deftime);	
					my $limit=(86400 * 22);
					my $notice_limit=(86400 * 15);
					if (($timediff > $notice_limit) && ($timediff < $limit)) {
						LogMsg(indent(1)."NOTICE: SAV AntiVirus Definitions are from $antivirus_def ");										
					} elsif  ($timediff > $limit) {           
						LogMsg(indent(1)."WARNING: SAV AntiVirus definitions are from $antivirus_def ");
						$warns++;                        
					} else {					 
						LogMsg(indent(1)."Proactive Threat definitions are from $antivirus_def");					 
					}		
				}
			}
		}
	}
	unless ($antivirus_def) {
		LogMsg(indent(1)."WARNING: SAV AntiVirus Definitions date was not determined.");
		$warns++;
	}		
	###
	# Check the logs for time of the update
	###
	
	my $antivirus_dir="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection";	
	if (-d $antivirus_dir) {
		#LogMsg("Located $antivirus_dir");
	}
	
	my $antivirus_log_dir="${antivirus_dir}/Logs";
    my $antivirus_log_dir_alt="${antivirus_dir}/CurrentVersion/Data/Logs/AV";
    if (-d $antivirus_log_dir_alt) {
		#LogMsg("Located $antivirus_log_dir_alt");
        $antivirus_log_dir=$antivirus_log_dir_alt;
    }	
	if (-d $antivirus_log_dir) {
		#LogMsg("Located $antivirus_log_dir");
		$warns+=check_antivirus_log($antivirus_log_dir);
	} else {
	 
		LogMsg(indent(1)."WARNING: Failed to locate Antivirus Log Directory");
		$warns++;		
	}
 
		
	if ($warns) {
        $warning_count++;
    }
}


sub check_antivirus_sep_old {
	if ($OS eq "Win7") {
		LogMsg("Check antivirus SEP is not supported by $OS");
		return;
	}
    my $version=shift;
	my $warns=0;
    LogMsg("Checking antivirus - Symantec Endpoint Protection $version");
	# Check the DLL is in sync	
	unless (sync_symantec_dll("check")) {
		LogMsg("Need to sync symantec DLL's"); 
		sync_symantec_dll(); 

		unless (sync_symantec_dll("check")) {
			LogMsg("WARNING: Symantec DLL's may still have a problem.");
			$warns++;			
		}

	} else {
		LogMsg(indent(1)."Symantec DLL is in sync");
	}		
	if ($OS eq "WinXP") {
		unless (check_awhost32_excluded()) {
			if (1) {
				LogMsg("WARNING: pcAnywhere is not excluded from Symantec Endpoint Protection");
				$warns++;		
			}
			if (1) {
				my $fixfile_source="//Edna/temp/install/reg/awhost32_exclusion.reg";
				my $fixfile="c:/temp/scripts/awhost32_exclusion.reg";
				copy($fixfile_source,$fixfile);
				
				if (-f $fixfile) {			 
					my $cmd="regedit.exe -s $fixfile";
					LogMsg("Running $cmd");
					`$cmd`;
					if (check_awhost32_excluded()) {
						LogMsg("pcAnywhere was successfully excluded from SEP");
					} else {
						LogMsg("WARNING: Failed to exclude pcAnywhere");
						LogMsg("WARNING: pcAnywhere is not excluded from Symantec Endpoint Protection");
						$warns++;				
					}			
				} else {
					LogMsg("WARNING: Unable to locate $fixfile");
					$warns++;
				}	
			}
		}	
	}

		
	# Check that the installation is as unmanaged
	my $sep_found=0;
	my $smc_found=0;
	my $smc_managed=0;
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\"  ";    
	my @info=`$cmd`;
	unless ($?) {
		foreach my $i (@info) {        
			if ($i =~ /Symantec Endpoint Protection/) {						
				$sep_found = 1;			
			}
		}	
	}                        

	if ($sep_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\"  ";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {        
				if ($i =~ /SMC/) {						
					$smc_found = 1;					
				}
			}
		}                        
		
	}
	if ($smc_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\SMC\" /s";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {
				if ($i =~ /CommunicationStatus/) {	
					$smc_managed=1;				 				
				}						 
			}
		}                        
		
	}	
	if ($smc_managed) {
		LogMsg(indent(1)."WARNING: Symantec Endpoint Protection is installed as managed");
		$warns++;
	}
	LogMsg("Checking Symantec antivirus definitions");
	# Check that the antivirus definitions are up to date
	# The def_hash comes from SEP12 that we tried for a while.  It is not appropriate for SEP11 but I have not completely
	# removed in in case we eventually do go to SEP12 - kdg

    my %def_hash=(
        'VirusDefs' => 'Virus',
        'BASHDefs' => 'Proactive Threat',
        'IPSDefs' => 'Network Threat',
    );
	# We are typically only installing the antivirus 
    %def_hash=(
        'VirusDefs' => 'Virus', 	 
    );	
	if ($version =~ /^12/) {
		$def_hash{'BASHDefs'} = 'Proactive Threat',
	}
 
	my $antivirus_def = 0;
	my $proactive_threat_def=0;	
    for my $def_dir (keys (%def_hash)) {
        my $def_label=$def_hash{$def_dir};
        # Check the date of the antivirus definitions
        # Finding the best way to check the definition dates is a bit tricky.  It seems to depend on what OS you have.
		# The following should work for Windows XP.  If this does not work, there is a registry key we look at later
		# and this seems to work for Windows 7 64bit at least - kdg
		
        my $antivirus="C:/Program Files/Common Files/Symantec Shared/$def_dir/definfo.dat";		
		my $antivirus_alt1="C:/Program Files/Common Files/Symantec Shared/VIRUSD~1/definfo.dat";
		my $antivirus_alt2="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/CurrentVersion/Data/Definitions/$def_dir/definfo.dat";
		unless (-f $antivirus) {
			if ($verbose) {
				print "Cannot locate $antivirus\n";
			}
			if (-f $antivirus_alt1) {	
				if ($verbose) {
					print "Found $antivirus_alt1\n";			
				}
				$antivirus=$antivirus_alt1;
			} elsif (-f $antivirus_alt2) {
				if ($verbose) {
					print "Cannot locate $antivirus_alt1\n";
					print "Found $antivirus_alt2\n";
				}
				$antivirus=$antivirus_alt2;
			} else {
				if ($verbose) {
					print "Cannot locate $antivirus_alt2\n";
				}
			}
		}
        if (-f $antivirus) {
            my $virus_updated=0;			
            my @ltm = localtime();		
            my $month=$ltm[4]+1;
            my $year=$ltm[5]+1900;	
            my $last_month=($month - 1);
            my $last_month_year=$year;
            if ($month == 1) {
                $last_month=12;
                $last_month_year=($year - 1);
            }
            # Look for the log files for this month
            open(DEF,"$antivirus");
            my @file_info=(<DEF>);
            close DEF;
            if ($verbose) {
                LogMsg(indent(1)."Read contents of $antivirus");
            }
            my $deftime;

            foreach my $line (@file_info) {	
                if ($verbose) {
                    LogMsg(indent(1)."Scanning line $line...");
                }			
     
                if ($line =~ /CurDefs/) {
                    if ($verbose) {
                        LogMsg("$line");
                    }
                    # What is the date of the current defs
                    my @tmp=split(/=/,$line);
                    my $date=$tmp[1]; 
                    my $defyear=substr($date,0,4);
                    my $defmonth=substr($date,4,2);
                    my $defday=substr($date,6,2); 
                    if ($verbose) {
                        LogMsg("Determined date to be year: $defyear month: $defmonth day: $defday");
                    }
                    my $timemonth=($defmonth - 1);								
                    $deftime = timelocal(0,0,0,$defday,$timemonth,$defyear);	
					if ($def_label =~ /Virus/i) {
						$antivirus_def=1;
					}
					if ($def_label =~ /Proactive Threat/i) {
						$proactive_threat_def=1;
					}					
                    # Calculate how old the definitions are
                    my $timeseconds=time();
                    my $timediff=($timeseconds - $deftime);	# This is how many seconds old the definitions are.
                    # The definitions should not be more than two weeks old if they are set to update daily.
                    # If they are more than two weeks old and they are not set to update daily, set daily updates.
                    # If they are set to weekly and they never get set to daily, then the warning will go out after three weeks.
                    my $limit=(86400 * 22);
                    my $notice_limit=(86400 * 15);
                    if (($timediff > $notice_limit) && ($timediff < $limit)) {
                        LogMsg(indent(1)."NOTICE: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");						
                        schedule_update();                    
                    } elsif  ($timediff > $limit) {           
                        LogMsg(indent(1)."WARNING: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");
                        $warns++;      
						schedule_update();     						
                    } else {					 
                        LogMsg(indent(1)."$def_label definitions are from ${defyear}-${defmonth}-${defday}");					 
                    }	                    
                }
            } 
            unless ($deftime) {
                LogMsg(indent(1)."WARNING: SAV Failed to determine date of $def_label definitions");
                $warns++;									
            }		
        } else {
            LogMsg(indent(1)."WARNING: SAV Failed to find $antivirus");
            $warns++;             
        }            
    }
	unless ($proactive_threat_def) {
		if ($verbose) {
			print "Looking in registry for proactive threat definition date...\n";
		}
		###
		#	Proactive Threat Definitions
		###
	 
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\AV\\Storages\\SymHeurProcessProtection\\RealTimeScan\"";		  
		 
		@info=`$cmd`;  
		unless ($#info > 1) {
			LogMsg("Did not find proactive threat info in registry");
		}
		foreach my $i (@info) {        
			chomp($i);
			if ($i =~ /ContentDate/i) {
				if ($verbose) {
					print "Found in Registry: ($i)\n";
				}
				my @tmp=split(/\s+/,$i);
				my $ptd=$tmp[-1];
				my $l=length($ptd);
				my $y; 
				my $m; 
				my $d; 
				my $r; 
				if ($l == 9) {
					$y = substr($ptd,0,2);
					$m = substr($ptd,2,2);
					$d = substr($ptd,4,2);
					$r = substr($ptd,6,3);			
				} elsif ($l == 8) {
					$y = substr($ptd,0,1);
					$m = substr($ptd,1,2);
					$d = substr($ptd,3,2);
					$r = substr($ptd,5,3);				
				}  
				if (($y) && ($m) && ($d)) {
					$y = sprintf("%02d",$y);
					$y = "20".$y;			
					$proactive_threat_def = "$y"."-"."$m"."-"."$d"; 
					my $timemonth=($m - 1);				 
					if ($verbose) {
						LogMsg(indent(1)."$i");
						LogMsg(indent(1)."$proactive_threat_def");
						LogMsg(indent(1)."Year: $y Month: $timemonth Day: $d");
					}
					my $deftime = timelocal(0,0,0,$d,$timemonth,$y);
					my $timeseconds=time();
					my $timediff=($timeseconds - $deftime);	
					my $limit=(86400 * 22);
					my $notice_limit=(86400 * 15);
					if (($timediff > $notice_limit) && ($timediff < $limit)) {
						LogMsg(indent(1)."NOTICE: SAV Proactive Threat Definitions are from $proactive_threat_def ");										
					} elsif  ($timediff > $limit) {           
						LogMsg(indent(1)."WARNING: SAV Proactive Threat definitions are from $proactive_threat_def ");
						$warns++;   
						schedule_update();  						
					} else {					 
						LogMsg(indent(1)."Proactive Threat definitions are from $proactive_threat_def");					 
					}		
				}
			}
		}
	}
	unless ($proactive_threat_def) {
		LogMsg(indent(1)."WARNING: SAV Proactive Threat Definitions date was not determined.");
		$warns++;
	}

	unless ($antivirus_def) {
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{1CD85198-26C6-4bac-8C72-5D34B025DE35}\"";
		@info=`$cmd`;  

		foreach my $i (@info) {        
			chomp($i);
			 
			if ($i =~ /CurrentSequenceNum#/i) {
			 
				my @tmp=split(/\s+/,$i);
				my $ptd=$tmp[-1];
				my $l=length($ptd);
				my $y; 
				my $m; 
				my $d; 
				my $r; 
				if ($l == 9) {
					$y = substr($ptd,0,2);
					$m = substr($ptd,2,2);
					$d = substr($ptd,4,2);
					$r = substr($ptd,6,3);			
				} elsif ($l == 8) {
					$y = substr($ptd,0,1);
					$m = substr($ptd,1,2);
					$d = substr($ptd,3,2);
					$r = substr($ptd,5,3);				
				}  
				if (($y) && ($m) && ($d)) {
					$y = sprintf("%02d",$y);
					$y = "20".$y;			
					$antivirus_def = "$y"."-"."$m"."-"."$d"; 
					my $timemonth=($m - 1);				 
					my $deftime = timelocal(0,0,0,$d,$timemonth,$y);
					my $timeseconds=time();
					my $timediff=($timeseconds - $deftime);	
					my $limit=(86400 * 22);
					my $notice_limit=(86400 * 15);
					if (($timediff > $notice_limit) && ($timediff < $limit)) {
						LogMsg(indent(1)."NOTICE: SAV AntiVirus Definitions are from $antivirus_def ");										
					} elsif  ($timediff > $limit) {           
						LogMsg(indent(1)."WARNING: SAV AntiVirus definitions are from $antivirus_def ");
						$warns++;                        
					} else {					 
						LogMsg(indent(1)."Proactive Threat definitions are from $antivirus_def");					 
					}		
				}
			}
		}
	}
	unless ($antivirus_def) {
		LogMsg(indent(1)."WARNING: SAV AntiVirus Definitions date was not determined.");
		$warns++;
	}		
	###
	# Check the logs for time of the update
	###
	
	my $antivirus_dir="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection";	
	if (-d $antivirus_dir) {
		#LogMsg("Located $antivirus_dir");
	}
	
	my $antivirus_log_dir="${antivirus_dir}/Logs";
    my $antivirus_log_dir_alt="${antivirus_dir}/CurrentVersion/Data/Logs/AV";
    if (-d $antivirus_log_dir_alt) {
		#LogMsg("Located $antivirus_log_dir_alt");
        $antivirus_log_dir=$antivirus_log_dir_alt;
    }	
	if (-d $antivirus_log_dir) {
		#LogMsg("Located $antivirus_log_dir");
		$warns+=check_antivirus_log($antivirus_log_dir);
	} else {
	 
		LogMsg(indent(1)."WARNING: Failed to locate Antivirus Log Directory");
		$warns++;		
	}

 
		
	if ($warns) {
        $warning_count++;
    }
}

sub fix_srtpv {
	# If the Symantec SRTPV.DAT file is old, it means that autoprotect is probably not working.
	# The solution:  1) Stop Antivirus 2) Delete/Rename the file 3) Restart Antivirus
	my $service="Symantec Antivirus";
	my $dat_file="C:/Program Files/Symantec AntiVirus/SRTPV.DAT";
	my $index=1;
	my $backup_file="C:/Program Files/Symantec AntiVirus/SRTPV_${index}.DAT";	
	my $warns=0;
	if (-f $dat_file) {	
		# Step 1
		if (stop_service($service)) {
			LogMsg(indent(1)."$service has been stopped");
			while (-f $backup_file) {
				$index++;
				$backup_file="C:/Program Files/Symantec AntiVirus/SRTPV_${index}.DAT";
			}
			# Step 2
			rename($dat_file,$backup_file);
			if (-f $dat_file) {
				LogMsg("WARNING: Failed to rename $dat_file");				
				return 0;
			}
			if (-f $backup_file) {
				LogMsg("Successfully renamed SRVT.DAT");
			}			
			# Step 3
			if (start_service($service)) {
				LogMsg("Successfuly restarted $service");
			}
		} else {
			LogMsg(indent(1)."WARNING: Failed to stop $service");
			if (start_service($service)) {
				LogMsg("Successfuly restarted $service");
			} else {		
				LogMsg(indent(1)."WARNING: Failed to re-starat $service");				
			}
			return 0;
		}
	} else {
		LogMsg("WARNING: Cannot locate $dat_file");		
		return 0;
	}
	return 1;	
}

sub read_most_recent {
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="recent";
	my $warns=0;
	my $timeseconds=time();	
	
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};	
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			return;
		}
	}	
	LogMsg("Checking the most recent antivirus log file");
	# For processing the most recent antivirus log file
	my $antivirus_log=shift;
	my $most_recent=shift;	# Date of the most recent file
	my $most_recent_file=shift; # Most recent file
	my $antivirus="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec AntiVirus Corporate Edition/7.5/Logs";
 
	my @ltm=localtime($most_recent); 
	my $file_date=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);  

	if ($most_recent_file) {
		open(INPUT,"${antivirus_log}/${most_recent_file}");
		my @file_info=(<INPUT>);
		close INPUT;	
		if ($verbose) {
			LogMsg("Reading antivirus log file $most_recent_file");
			foreach my $line (@file_info) {
				print "$line\n";
			}
		}
		# Read the last line in the file.  I think that the last line is the most recent activity
		my $first_line=$file_info[-1];
		if ($first_line =~ /Symantec AntiVirus has determined that the virus definitions are missing on this computer/) {
			LogMsg(indent(1)."WARNING: SAV Symantec Antivirus definitions are missing on $file_date");
			$warns++;				
		} elsif ($first_line =~ /Symantec AntiVirus has detected a problem with the virus definitions on this computer/) {
			LogMsg(indent(1)."NOTICE: SAV Symantec Antivirus has detected a problem with the virus definitions on $file_date");
			#$warns++;				
		} elsif ($first_line =~ /Download of virus definition file from LiveUpdate server failed./) {
			LogMsg(indent(1)."NOTICE: SAV Symantec Antivirus has detected a problem downloading the virus definitions on $file_date");
			#$warns++;
		}                  
	}	
	if ($warns) {
		LogMsg("WARNING: SAV There were $warns issues checking antivirus log file");
		$warning_count++;
	}	
	update_action_log($check,$warns);
	$monitor_record_hash{$check}=$timeseconds;	
	
}

sub fix_registry {
	# This function is to execute a registry fix.  Send this function a path to a .reg file and this function will run it with regedit.exe
	if ($debug_mode) {
		# No fixes made in debug mode
		return 1;
	}
	unless ($OS eq "WinXP") {
		LogMsg("Fix_Registry does not support $OS");		
		return;
	}	
	my $file=shift;
    # Change forward slashes to back slashes
    $file=~ s/\//\\/g;
	my $cmd="regedit.exe -s $file";
    if ($verbose) {
        LogMsg("Running: $cmd");        
    }
	LogMsg("Fix Registry: running ($file) with regedit");
    `$cmd`;
	if ($?) {
		# Failed
		return 0;
	} else {  	
		# Succeeded
		return 1;
	}
}

sub fix_registry_bat {
	# This function is to execute a registry fix.  Send this function a path to a bat file which has the appropriate reg commands.
	if ($debug_mode) {
		# No fixes made in debug mode
		return 1;
	}	
	my $file=shift;
	if (-f $file) {
		my $cmd=`$file`;
			
		if ($?) {
			# Failed
			return 0;
		} else {  	
			# Succeeded
			return 1;
		}
	} else {
		LogMsg("ERROR: $file not found");
		return 0;
	}
}


sub save_log {
	if ($debug_mode) {
		return;
	}
	# Save this log on the Edna server
	my $warns=0;
	my $target_dir="//Edna/temp/install/register_utilities/";
	my $target_file="${target_dir}/${hostname}.log";
	my $local_target_file="c:/temp/scripts/last_monitor.log";
	my $local_target_file2="c:/temp/scripts/last2_monitor.log";
	my $local_target_file3="c:/temp/scripts/last3_monitor.log";
	my @tmp=split(/\./,$0);
	
	my $logfile=$tmp[0];
	$logfile="${logfile}.log";			# The name of the log file for this script (with the path).
	my $basename=basename($logfile);	# The name of the logfile without the path.
	if (-f $local_target_file3) {
		LogMsg("Removing existing $local_target_file3...");
		unlink $local_target_file3;
		if (-f $local_target_file3) {
			LogMsg("Try again to unlink $local_target_file3...");
			sleep 5;
			unlink $local_target_file3;
			if (-f $local_target_file3) {
				LogMsg("Unlink was not successful");
			} else {
				LogMsg("Unlink was successful");
			}
		} else {
			LogMsg("Successful");
		}		
	}
	if (-f $local_target_file2) {
		LogMsg("Moving existing $local_target_file2 to $local_target_file3...");
		if (move($local_target_file2,$local_target_file3)) {	
			if (-f "$local_target_file3") {
				LogMsg("Successfully moved.");			
			} else {
				LogMsg("Did not find $local_target_file3");
				LogMsg("WARNING: Failed to move $local_target_file2 to $local_target_file3");
				$warns++;
			}
		} else {		
			LogMsg("WARNING: Move of $local_target_file2 appears to have failed.");
			$warns++;
		}		
	}	
	if (-f $local_target_file) {
		LogMsg("Moving existing $local_target_file to $local_target_file2...");
		if (move($local_target_file,$local_target_file2)) {	
			if (-f "$local_target_file2") {
				LogMsg("Successfully moved.");			
			} else {
				LogMsg("Did not find $local_target_file2");
				LogMsg("WARNING: Failed to move $local_target_file to $local_target_file2");
				$warns++;
			}
		} else {		
			LogMsg("WARNING: Move of $local_target_file appears to have failed.");
			$warns++;
		}		
=pod	
		LogMsg("Removing existing $local_target_file...");
		unlink $local_target_file;
		if (-f $local_target_file) {
			LogMsg("Try again to unlink $local_target_file...");
			sleep 5;
			unlink $local_target_file;
			if (-f $local_target_file) {
				LogMsg("Unlink was not successful");
			} else {
				LogMsg("Unlink was successful");
			}
		} else {
			LogMsg("Successful");
		}		
=cut		
	} else {
		LogMsg("No $local_target_file found");
	}
	LogMsg("Create $local_target_file...");	
	if (copy($logfile,$local_target_file)) {	
		if (-f "$local_target_file") {
			LogMsg("Successfully saved a copy locally.");			
		} else {
			LogMsg("Did not find $local_target_file");
			LogMsg("WARNING: Failed to create a local copy");
			$warns++;
		}
	} else {		
		LogMsg("WARNING: Failed to create a local copy");
		$warns++;
	}	
	unless ($no_edna) {
		my $attempt=5;
		while ($attempt) {
			$attempt--;
			if (-d $target_dir) {
				LogMsg(indent(1)."Contacted Edna.");
				$attempt=0;
			} else {
				sleep 5;
			}		
		}
		if (-d "$target_dir") {	
			LogMsg(indent(1)."Found $target_dir");
			if (-f $logfile) {			
				if (copy($logfile,$target_file)) {
					if (-f "$target_file") {
						LogMsg("Successfully copied log to Edna");
						unlink $logfile;
					} else {
						LogMsg("Did not find $target_file");
						LogMsg("NOTICE: Failed to copy log file to Edna");
						$warns++;
					}
				} else {		
					LogMsg("NOTICE: Failed to copy log file to Edna");
					$warns++;
				}
			}
		} else {
			LogMsg("NOTICE: Failed to locate Edna");
			#$warns++;
		}
	}

	
	if ($warns) {
		LogMsg("NOTICE: Found $warns issues saving log file");
		#$warning_count++;
	}	
}

sub save_record {
	if ($debug_mode) {
		return;
	}
	LogMsg("Saving monitor record...");
	# Save the results to the monitor record
	my $records=keys(%monitor_record_hash);
	unless ($records) {
		LogMsg("No records to update.");
		return;
	}
	if (open(REC,">$monitor_record")) {
		LogMsg("Updating $monitor_record");
		foreach my $key (sort keys(%monitor_record_hash)) {
			next unless ($key);
			if ($verbose) {
				print "Saving $key $monitor_record_hash{$key} in monitor record...\n";
			}
			print REC "$key $monitor_record_hash{$key}\n";
		}
	
	} else {
		LogMsg("WARNING: Failed to open $monitor_record");
		$warning_count++;
	}
	close REC;	
}

sub reset_record {
	# Clear the monitor record hash variable so that the record will effectivly get cleared
	LogMsg("Clearing monitor_record_hash variable");
	%monitor_record_hash=();

}

sub check_antivirus_scan_results {
	LogMsg("Checking Antivirus Scan Results");
	# We used to check the scan results in check_eventlog but the new SEP12 complains so much about using Perl
	# that the event log overflows and we sometimes lose the scan data.
	# Here we check the SEP logs directly
 
	my $warns=0;
	my $scan_found=0;
	my $scan_starts_found=0;
	my $antivirus_log_dir="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/CurrentVersion/Data/Logs/AV";
	unless (-d $antivirus_log_dir) {
		LogMsg("WARNING: Failed to locate $antivirus_log_dir");
		return 1;
	}
	# Look for the log files for the past 7 days
	my @past_week_list=();
	my @past_month_list=();	
	my $last_scan="none";
	opendir(LOGS,"$antivirus_log_dir");
	my @dir_list=readdir(LOGS);
	close LOGS;	
	
 
	my @reboot_phrases=(
		"Auto-Protect Error",
		"Definition Failure",
		"SONAR has generated an error"
	);	
 
	foreach my $file (@dir_list) {
		next if ($file eq ".");
		next if ($file eq "..");
		my $target="${antivirus_log_dir}/${file}";
		my @file_info=stat($target);
 
		my $current_timeseconds=time();				
		my $file_age = (($current_timeseconds - $file_info[9]) / 86400);         
		$file_age = sprintf("%.2f", $file_age);            
		if ($file_age < 7) {			
			push(@past_week_list,$target);
		} elsif ($file_age < 31) {			
			push(@past_month_list,$target);
		}		
	}
	foreach my $file (@past_week_list) {
		# Get the date from the file name
		my $basename=basename($file);
		my $month=substr($basename,0,2);
		my $day=substr($basename,2,2);
		my $year=substr($basename,4,4);
		 
		my $date = "$year"."/"."$month"."/"."$day";
	 
		open(LOG,"$file");
		my @log_data=(<LOG>);
		close LOG;
		foreach my $l (@log_data) {
			chomp($l);
			if ($l =~ /Scan started/i) {
				LogMsg("Scan started on $date");
				$scan_starts_found++;
			}
			if ($l =~ /Scan Complete/i) {
				my @tmp=split(/\,/,$l);
				foreach my $t (@tmp) {
					if ($t =~ /Scan Complete/i) {
						LogMsg("Antivirus Scan Complete $date");
						$scan_found++;
						$t=~ s/\"//g;
						my @tmp2=split(/\s+/,$t);
						for (my $x=0; $x<=$#tmp2; $x++) { 
							my $data=$tmp2[$x];
							my $data_next=$tmp2[($x+1)];
							if ($data =~ /Risks/i) {
								my $risks=$data_next;
								if ($risks) {
									LogMsg("WARNING: Risks: $risks");
									$warns++;
								} else {
									LogMsg("Risks:   $risks");
								}																
							}
							if ($data =~ /Scanned/i) {
								my $scanned=$data_next;
								LogMsg("Scanned: $scanned"); 														
							}							
							if ($data =~ /Omitted/i) {
								my $omitted=$data_next;
								LogMsg("Omitted: $omitted"); 														
							}
							if ($data =~ /Skipped/i) {
								my $skipped=$data_next;
								LogMsg("Skipped: $skipped"); 														
							}								
						}
					}
				}				
			}
		}
	}
 
	unless ($scan_found) {	
		if ($scan_starts_found) {
			LogMsg("WARNING: $scan_starts_found Antivirus Scan Starts Found in the past 7 days but none finished.");
		} else {
			LogMsg("WARNING: No Antivirus Scan Found in the past 7 days");
		}
		$warns++;
		$warns+=schedule_do_scan();	
		# find when the last scan was
		LFILE: foreach my $file (@past_month_list) {			 
			# Get the date from the file name
			my $basename=basename($file);
			my $month=substr($basename,0,2);
			my $day=substr($basename,2,2);
			my $year=substr($basename,4,4);
			 
			my $date = "$year"."/"."$month"."/"."$day";
		 
			open(LOG,"$file");
			my @log_data=(<LOG>);
			close LOG;
			foreach my $l (@log_data) {
				chomp($l);
				if ($l =~ /Scan Complete/i) {
					my @tmp=split(/\,/,$l);
					foreach my $t (@tmp) {
						if ($t =~ /Scan Complete/i) {
							LogMsg("WARNING: Last Antivirus Scan Complete $date");
							last LFILE;
						}
					}
				}
			}
		}
	}
	return $warns;
}

sub check_eventlog {
    if ($new_register_test) {
        print "Not checking event log\n";
        return;
    }
	if ($OS eq "Win7") {
		unless ($day_of_week == 0) {
			# 2016-01-06 Not checking due to resource constraints
			return;	
		}
	}
	
	my $warns=0;
	my $need_perflib_fix=0;
	my $counter_file="c:/temp/scripts/counters.txt";	

	LogMsg("Checking eventlog");	
	if (0) {	# This next section replaced by check_antivirus_scan_results above - kdg
			
		# Check that there was a scan within the past week
	 
		my $cmd= "psloglist -d 7 -s -o \"Symantec AntiVirus\" -x application" ;
		my @results=`$cmd`;
		  
		my $scan_found=0;
		my $sav_found=0;
		
		LogMsg(indent(1)."First Check:  Symantec AntiVirus");
		foreach my $r (@results) {	
		 
			next unless ($r);
			$sav_found++;
			my @tmp=split(/\,/,$r);
			my $date;
			my $result;
			if ($#tmp > 7) {
				$date=$tmp[5];
				$result=$tmp[8];
				 
				# Evaluation the result
				if ($result =~ /Symantec AntiVirus services/i) {
					
					# This looks like the startup message
					unless ($result =~ /successful/i) {
						LogMsg(indent(1)."WARNING: ($result) on $date");
						$warns++;
					}
				}
				if ($result =~ /Scan Complete/i) {
					LogMsg(indent(1)."Found Antivirus Scan Complete");
					# This looks like the scan results
					$scan_found++;				
					my @result_tmp=split(/\s+/,$result);
					my $risks=$result_tmp[4];
					my $scanned=$result_tmp[6];				
					if ($risks) {					
						LogMsg(indent(1)."WARNING: $risks risks found on $date");
						$warns++;
					} else {
						LogMsg(indent(1)."$risks risks found on $date");
					}
					if ($scanned) {
						LogMsg(indent(1)."$scanned files scanned on $date");
					} else {
						LogMsg(indent(1)."Notice: $scanned files scanned");
						#$warning_count++;
					}
				}	
				if ($result =~ /Security Risk Found/i) {			
					my @result_tmp=split(/:/,$result);
					my $trigger=0;
					foreach my $r (@result_tmp) {
						if ($r =~ /quarantine/i) {
							LogMsg(indent(1)."NOTICE: $r");
						}
						if ($trigger) {
							LogMsg(indent(1)."NOTICE: $r");
						}
						if ($r =~ /Action Description/i) {
							$trigger=1;
						} else {
							$trigger=0;
						}					
					}
				}
			}		
		}
		if ($sav_found) {
			LogMsg("Found $sav_found Symantec Antivirus Entries");
			unless ($scan_found) {
				LogMsg("WARNING: No scan found in the past 7 days");
				$warns++;
				$warns+=schedule_do_scan();
			}
		} else {
			LogMsg("WARNING: No Symantec Antivirus activity logged");
			$warns++;
		}
	}
	# Determine the last reboot
	my @reboot_log_data=`psloglist -i 6005 -n 1`;
	# Variables for possible reboots due to automatic updates:
	my $last_reboot_day='';
	# This will be the hour of the reboot.
	my $last_reboot_time='';
	
	foreach my $line (@reboot_log_data) {
	   if ($line =~ /Time:/) {	   
			$_=$line;
			my @tmp=split;
			if ($tmp[3] eq "AM") {
				# This may be due to automatic reboots for windows updates.
				$last_reboot_day=$tmp[1];
				$last_reboot_time=$tmp[2];
				@tmp=split(/:/,$last_reboot_time);
				$last_reboot_time=$tmp[0];
			}
	   }
	}	
	my $cmd_application="psloglist -d $day_request -s application";
	my $cmd_system="psloglist -d $day_request -s system";
    my @phrases=(
        "unable to allocate",
        "bad image",
        "application error",
        "the data is invalid",
        "pool was empty",
        "an error was detected on device",
        "service terminated unexpectedly",
		"ERROR",

    );    
	my @exclude_phrases=(
		"Failed auto update retrieval of third-party root list sequence",
		"Failed extract of third-party root list from auto update cab",
		"Could not scan .* files inside",
		"The device does not recognize the command",
		"The parameter is incorrect",
		"LiveUpdate returned a non-critical error",
		"Failed extract of third-party root list from auto update cab",
		"A required certificate is not within its validity period",
		"Failed auto update retrieval of third-party root list sequence number",
		"This network connection does not exist.",
		"crypt32.*403.*HTTP Response Status",
		"crypt32.*http.*authrootseq",
		"DCOM was unable to communicate with the computer R",
		"The browser was unable to retrieve a list of servers from the browser master",
		"Customer Experience Improvement Program",
		"Windows Installer installed an update",
		"Windows Installer reconfigured the product",
		"PnPRequestAdditionalSoftware",
		"The Windows Error Reporting Service service entered the running state",
		"The Windows Error Reporting Service service entered the stopped state",
		"Fault bucket type 0",
		"P4: generaltel.dll  P5: 10.0.10037.0",
		"generaltel.dll version: 10.0.10037.0",
		"generaltel.dll.*10.0.10037.0",
		"The Parallel port driver service failed to start due to the following error:",		
		"DCOM",
		"ProgramData.Microsoft.Windows.WER.ReportQueue.NonCritical",
		"WindowsUpdateFailure3  Response: Not available",
		
	);
	
	my @time_sensitive_phrases=(
		"NtpClient was unable to set a manual peer",
		"WindowsUpdateFailure",		
	);
	
	# If errors are caused by the system rebooting
	my @reboot_sensitive_phrases=(
		"bkoff"
	);
 
	my @known_phrases=(
		"pcAnywhere.*Host Connection Failure - User Login Failed",
		"posw.exe 9.0.0.0 xrules.dll",
		"extraction errors encountered by the Decomposer Engines",
		"Hanging application iexplore.exe",
		
	);
	my @reboot_phrases=(
		"Auto-Protect Error",
		"Definition Failure",
		"SONAR has generated an error"
	);	
 
	my @win7_exclude_phrases=(
		"Windows Search Service",
		"Windows: Error.*occurred while opening logfile C:.ProgramData.Microsoft.Search.Data.Applications.Windows",	
		"An attempt to configure the input mode of a multitouch device failed"
	);	
	if ($OS eq "Win7") {
		push(@exclude_phrases,@win7_exclude_phrases);
	}
	
	# Some phrases need to be examined more closely.  For example, there are some tamper alert situations
	# that need to be excluded.  Since Symantec Antivirus v10 does not accomodate exclusions, I am going 
	# to need to do it here.
	# If both the key and the value are found in the event log line, then the event may be ignored.
	my %evaluate_phrases_hash=(
		"SYMANTEC TAMPER PROTECTION ALERT" => 'Actor Process:  C:.WINDOWS.system32.services.exe .PID ',
	);
    # Check the systemlog
    my @log_data;

    my %warning_hash=();			# This hash will record events that will be recorded as warnings
    my %event_time=();
	my %notice_hash=();				# This has will record events that will be recorded as notices
	LogMsg(indent(1)."Second Check:  Application & System logs");     
	foreach my $c ("$cmd_application","$cmd_system") {
		if ($c =~ /application/) {
			LogMsg(indent(1)."Checking application log...");
		}
		if ($c =~ /system/) {
			LogMsg(indent(1)."Checking system log...");
		}		
		@log_data=`$c`;
		foreach my $l (@log_data) { 
			chomp($l);
			if ($l =~ /Error reading/) {
				LogMsg(indent(1)."WARNING: $l");				
				$warns++;
				if ($l =~ /System event log/) {
					# Clear the System Event Log
					my $tool = "C:/temp/scripts/mrt.exe";
					if (-f $tool) {
						my $cmd="$tool -clear system";
						my @results=`$cmd`;
						LogMsg("Clearing system event log: ($cmd)");
						foreach my $r (@results) {
							LogMsg(indent(1)."$r");
						}
					} else {
						LogMsg("WARNING: Failed to locate $tool");
						$warns++;
					}
				}
				last;
			}
			foreach my $p (@reboot_phrases) {
				if ($l =~ /$p/i) { 	
					LogMsg("WARNING: Setting reboot request for ($p)");
					LogMsg("Setting flag to reboot");
					$need_to_reboot=1;	
				}
			}
			foreach my $p (@phrases) {
				if ($l =~ /$p/i) { 
					my $exclude=0;
					my $notice=0;
					
					foreach my $e (@exclude_phrases) {
						if ($l =~ /$e/i) {
							$exclude=1;
						}
					}
					next if ($exclude);
					foreach my $e (keys (%evaluate_phrases_hash)) {
						if ($l =~ /$e/i) {					
							if ($l =~ /$evaluate_phrases_hash{$e}/) {							
								$exclude=1;
							}  
						}
					} 
					next if ($exclude);
					my @tmp=split(/,/,$l);
					my $source=$tmp[2];
					my $event_record;
					my $event_detail;
					my $event_time=$tmp[5];	
					# Get the day and the hour & minute
					my $event_day;
					my $event_hour;
					my $event_minute;
					my @time_tmp=split(/\s+/,$event_time);
					$event_day=$time_tmp[0];
					$event_hour=$time_tmp[1];
					@time_tmp=split(/:/,$event_hour);
					$event_hour=$time_tmp[0];
					$event_minute=$time_tmp[1];
					
					foreach my $e (@time_sensitive_phrases) {						
						if ($l =~ /$e/i) {							
							# If the event requires Internet access it will fail from 8:00am to 10:00pm
							my @tmp1=split(/\s+/,$event_time);
							my $time=$tmp1[1];
							my $AMPM=$tmp1[2];
							my @tmp2=split(/:/,$time);
							my $hour=$tmp2[0];							
							if (($hour > 7) && ($AMPM eq "AM")) {								
								$exclude=1;								
							}
							if (($hour < 9) && ($AMPM eq "PM")) {								
								$exclude=1;
							}														
						}
					}	
 
					# If the event is an appcrash and appears to be due to a reboot at 3:00AM (Windows Updates)
					# it can be excluded	
					if (($event_day eq $last_reboot_day) && ($event_hour == $last_reboot_time) && ($event_hour == 3) && ($l =~ /APPCRASH/)) {								
						$exclude=1;
					} 													 											
					next if ($exclude);
					foreach my $e (@known_phrases) {	 			
						if ($l =~ /$e/i) {				 
							$notice=1;
						}
					}				

					# Concatinate detail from index 8 to the end 					
					for (my $x=8; $x<=$#tmp; $x++) {
						chomp(my $part = $tmp[$x]);
					 
						while ($part =~ /^ /) {
							$part =~ s/^ //;
						}
						while ($part =~ / $/) {
							$part =~ s/ $//;
						}				 							
						$event_detail .= "$part ";					
					}

					$event_record="$source".": "."$event_detail";
					if ($event_record =~ /Application Error: pslist.exe/) {
						$need_perflib_fix=1;
					}					
					if ($verbose) {					
						LogMsg("$event_record on $tmp[5]");
					} 		
					if ($notice) {
						$notice_hash{$event_record}++;
						$event_time{$event_record}=$tmp[5];
					} else {	 
						$warning_hash{$event_record}++;
						$event_time{$event_record}=$tmp[5];
						 
					}
				}                        
			}
		}
	}
 
    my $event_time;
	unless ($debug_mode) {
		foreach my $key (sort keys(%warning_hash)) {
			$warns++;
			LogMsg(indent(1)."WARNING: Found $warning_hash{$key} instances of: $key - $event_time{$key}");
			$event_time=$event_time{$key};
		}
		foreach my $key (sort keys(%notice_hash)) {        
			LogMsg(indent(1)."NOTICE: Found $notice_hash{$key} instances of: $key - $event_time{$key}");
			$event_time=$event_time{$key};
		}	
	}
	if ($need_perflib_fix) {
		LogMsg("Need to fix perflib");
		if (-f $counter_file) {
			LogMsg("Refreshing counters using lodctr");
			my $cmd="lodctr /r:$counter_file";							 
			system($cmd);							 
		} else {
			LogMsg("WARNING: Failed to locate $counter_file");
			$warning_count++;
			return 0;
		}			
	}
    if ($warns) {
        LogMsg("WARNING: There were $warns issues seen in the system event log in the past day."); 
        $warning_count++;
    }   	
}


sub check_minidump {
 
	my $dump_dir="c:/WINDOWS/Minidump";
	my $temp_dump_dir="c:/temp/Minidump";	# This used to backup the minidumps

	my @dump_list=();
	my @temp_dump_list=();	
	my %temp_dump_hash=();
	
	my $current_time = time();
	my $warns=0;
	my $recent_dump_count=0; 
	my $year_dump_count=0;
	LogMsg("Checking for Windows dump");
	unless (-d $temp_dump_dir) {
		mkdir $temp_dump_dir;
	} else {
		opendir(DUMP,"$temp_dump_dir");
		@temp_dump_list=readdir DUMP;
		closedir DUMP;	
		foreach my $t (@temp_dump_list) {
			$temp_dump_hash{$t}=1;
		}
	}	
	# We copy everything out of the windows minidump folder since cCleaner may purge minidumps.
	# We save a copy in temp/minidump and check that folder for the dates of the dumps.
	if (-e $dump_dir) {
		opendir(DUMP,"$dump_dir");
		@dump_list=readdir DUMP;
		closedir DUMP;		
		chdir "$dump_dir";
		foreach my $file (@dump_list) {
			if ($file =~ /dmp$/) {
                unless ($temp_dump_hash{$file}) {
					# Save a copy of this dump file
					LogMsg(indent(1)."Saving a copy of $file to $temp_dump_dir");					
					copy("${dump_dir}/${file}","${temp_dump_dir}/${file}");
				}
 			
			}
		}
	}
	if (-e $temp_dump_dir) {
		opendir(DUMP,"$temp_dump_dir");
		@dump_list=readdir DUMP;
		closedir DUMP;		
		chdir "$temp_dump_dir";
		foreach my $file (@dump_list) {
			if ($file =~ /dmp$/) {
 
				# This appears to be a Windows dump file
				my @file_info = stat $file;
				# Find out how many days old this file is.  (One day = 86400 seconds).
				my $file_age = (($current_time - $file_info[9]) / 86400);
				$file_age = sprintf("%.2f", $file_age);
				if ($file_age < 1) {
					$warns++;
				}
				if ($file_age < 90) {
					$recent_dump_count++;
				}	
				if ($file_age < 365) {
					$year_dump_count++;
				}	
				LogMsg(indent(1)."Found $file $file_age days old");				
			}
		}
	}	
	my $dump_count=($#dump_list + 1);
	LogMsg("System Info: DumpCount: $dump_count");		
	if ($warns) {
		LogMsg(indent(1)."WARNING: $warns Windows dump occurred in the past 24 hours. Memory: $total_memory MB"); 		
		$need_to_defrag=1;
        $warning_count++;
		if ($recent_dump_count > 1) {
			LogMsg(indent(1)."WARNING: $recent_dump_count Windows dumps in the past 90 days."); 
		}
		if ($year_dump_count > $recent_dump_count) {
			LogMsg(indent(1)."WARNING: $year_dump_count Windows dumps in the past year."); 
		}	
		check_bluescreenview();
		unless ($check_disk_ran) {
			check_disk("1");
		} else {
			LogMsg("Check disk has already run");
		}
	} else {
		LogMsg(indent(1)."No windows dumps found during the past 24 hours.");
	}
}

sub check_bluescreenview {
	my $file="C:/Program Files/NirSoft/BlueScreenView/BlueScreenView.exe";
	if (-f $file) {
		LogMsg("BluescreenView is installed");
	} else {
		#LogMsg("NOTICE: Bluescreenview is not installed");
		LogMsg("Bluescreenview is not installed");
	}
}

sub check_reboots {
    if ($new_register_test) {
        print "Not checking for reboots\n";
        return;
    }
    # Check for reboots
    my $warns=0;
	my @log_data=`psloglist -i 6005 -d 1`;
	my $event_time;
	my $count=0;
	my @events;
	my $found_reboot = 0;
    foreach my $l (@log_data) {		
        chomp($l);        
        if ($l =~ /Time:/) {
			$count++;			
            $_=$l;
            my @tmp=split;   
			$event_time="$tmp[0] $tmp[1] $tmp[2] $tmp[3]";			
			unless (($tmp[2] =~ /^3/) && ($tmp[3] =~ /AM/)) {
				# Unless the reboot is at the 3 in the morning (Windows Updates), the reboot is unexpected.				
				$warns++;
				push(@events,$event_time);
			}
            LogMsg(indent(1)."Found system reboot at $tmp[0] $tmp[1] $tmp[2] $tmp[3]");   
			$found_reboot++;			
           
        }                        
    }
	unless ($found_reboot) {
		my $cmd = "net statistics workstation";
		my @cmd_info = `$cmd`;
		foreach my $c (@cmd_info) {
			chomp($c);
			if ($c =~ /Statistics since/) {
				my @tmp=split(/\s+/,$c);
				LogMsg("Last reboot: $tmp[2] $tmp[3] $tmp[4]");
				$found_reboot++;	
			}
		}
	}
	unless ($found_reboot) {
		LogMsg("Did not determine system reboot time");
	}
    if ($warns) {
		# Only create a warning if there were more than one unexpected reboot
		if ($warns > 1) {
			LogMsg(indent(1)."WARNING: There were $warns unexpected reboots ");        
			foreach my $e (@events) {
				LogMsg(indent(2)."WARNING: Reboot at $e");
			}
			$warning_count++;
		} else {
			LogMsg(indent(1)."Notice: There were $warns unexpected reboot ");  
			foreach my $e (@events) {
				LogMsg(indent(2)."NOTICE: Reboot at $e");
			}			
		}
    }      
}

sub gather_sysinfo {

	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=30;
 
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="sysinfo";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}
	
	my $update_log="c:/WINDOWS/WindowsUpdate.log";
	
	# Check the update log
    if (-e $update_log) {
		if ($verbose) {
			print "Reading $update_log\n";
		}
        open(INPUT, "$update_log") ;
        my @loginfo=(<INPUT>);
        close INPUT;
		my %systemInfo=();
        foreach my $line (@loginfo) {
            # Find the computer brand
            my @tmp=();
            if ($line =~ /Computer Brand/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
				$value=~s/^\s+//g;
                $systemInfo{Brand}=$value;
            }
            # Find the computer model
            if ($line =~ /Computer Model/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
				$value=~s/^\s+//g;
                $systemInfo{Model}=$value;
            }
            # Find the BIOS Rev
            if ($line =~ /Bios Revision/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
				$value=~s/^\s+//g;
                $systemInfo{BIOSRev}=$value;
            }            
            # Find the BIOS Name
            if ($line =~ /Bios Name/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
				$value=~s/^\s+//g;
                $systemInfo{BIOSName}=$value;
            } 
            # Find the BIOS Release Date
            if ($line =~ /Bios Release Date/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
				$value=~s/^\s+//g;
                $systemInfo{BIOSReleaseDate}=$value;
            }                         
        }

		if (-f $local_system_info) {
			if ($verbose) {
				print "Found $local_system_info\n";
			}		
			if (keys(%systemInfo) > 3) {
				# If we collected data, clear the previous data
				if ($verbose) {
					print "Clearing $local_system_info\n";
				}						
				unlink $local_system_info;
			}
		}
		foreach my $key(keys(%systemInfo)) {
			LogMsg("System Info: $key: $systemInfo{$key}");
			 
			open(LSI,">>$local_system_info");
			print LSI "$key"."="."$systemInfo{$key}\n"; 				
			if ($verbose) {
				print "$key"."="."$systemInfo{$key}\n"; 
			}		
		 			
		}
	
	} else {
		LogMsg("$update_log not found");
	}
	# Get the system serial number
	my $cmd="wmic bios get serialnumber";
	my @info=`$cmd`;
	my $serialnumber=0;
 
	foreach my $i (@info) {
		chomp($i); 
		next if ($i =~ /SerialNumber/i);
		next unless (($i =~ /\d/) || ($i =~ /\w/));
		$serialnumber=$i;				
	}
	
	if ($serialnumber) {
	
		LogMsg("System Info: SerialNumber: $serialnumber"); 
	}
	# Collect the Mac Address
	my @ipconfig_info=`ipconfig /all`;
	my $MAC=0;
	foreach my $i (@ipconfig_info) {
		if ($i =~ /physical address/i) {
			my @tmp=split(/\s+/,$i);
			$MAC=$tmp[-1];
			LogMsg("System Info: MAC: $MAC");			 
		}
	}		
	# Collect Network Card Info
 
	$cmd="wmic nic list /format:list";
	@info=`$cmd`;
	my $nic_driver='';
 
	my $found_target1=0;
	my $found_target2=0;
	my $manufacturer;
	foreach my $i (@info) {
		#chop($i);
		if ($i =~ /AdapterType=Ethernet 802.3/) {
			$found_target1=1;						
		}
		if ($i =~ /AdapterType/i) {
			unless ($i =~ /AdapterType=Ethernet 802.3/) {
				$found_target1=0;	
				$found_target2=0;									
			}
		}
		if ($found_target1) {
			if ($i =~ /Manufacturer/i) {				
				my @tmp=split(/=/,$i);
				$manufacturer=($tmp[1]);				
				if ($manufacturer) {
					if ($manufacturer =~ /microsoft/i) {
						$found_target2=0;		
					} elsif  ($manufacturer =~ /Symantec/i) {
						$found_target2=0;							
					} else {
						$found_target2=1;						 
					}					
				} else {
					$found_target2=0;					 
				}
			}
		}
		if ($found_target2) {
			if ($i =~ /^ProductName/) {		 
				my @tmp=split(/=/,$i);
				$nic_driver=($tmp[1]);				 				
			}
			if ($i =~ /ProductName=Packet Scheduler Miniport/) {
				$nic_driver='';			 
			}		
		}  		
	}
	if ($nic_driver) {
		chomp($nic_driver);
		chop($nic_driver);		
		LogMsg("System Info: NIC: $nic_driver"); 
	}	
	###	
	update_action_log($check,$warns);
	$monitor_record_hash{$check}=$timeseconds;	
}
 
sub check_tasks {
	my $warns=0;
	# First, terminate any tasks on the kill list
    foreach my $p (@kill_task_list) {
       
        my $pid=checkPID("$p");
        if ($pid) {
            LogMsg("Killing $p");
            LogMsg("PID is $pid");
            `pskill $accepteula $pid`;
            sleep 1;
            $pid=checkPID("$p");
            if ($pid) {
                LogMsg("Failed to stop $p");
            } else {
               LogMsg("Stopped $p");
            }
			if ($p =~ /gwx/i) {
				if ($OS eq "Win7") {
					# Special handling for GWX
					$warns+=check_gwx();
				}
			}			
        }
    }  
	if ($OS eq "Win7") {
		foreach my $key (keys(%valid_win7_task_hash)) {		
			$valid_task_hash{$key}=1;
		}
	}
	if ($storenum == 1245) {
		$valid_task_hash{'escsvc.exe'}=1;		
		$valid_task_hash{'e_tatikde.exe'}=1;		
	}
	if ($storenum == 1926) {			
		$valid_task_hash{'e_tatikde.exe'}=1;		
	}	
	if ($storenum == 2422) {			
		$valid_task_hash{'hpqgpc01.exe'}=1;		
		$valid_task_hash{'hpqbam08.exe'}=1;	
	}	
	if ($storenum == 3261) {			
		$valid_task_hash{'presentationfontcache.exe'}=1;		
		$valid_task_hash{'hplaserjetservice.exe'}=1;	
	}		
	if ($storenum == 1472) {			
		$valid_task_hash{'hpwuschd2.exe'}=1;			 	
	}			
 
	if ($register_server) {			
		$valid_task_hash{'apache.exe'}=1;		
		$valid_task_hash{'mlink.exe'}=1;	
		$valid_task_hash{'transnet.exe'}=1;	
		$valid_task_hash{'dbsrv12.exe'}=1;	
		$valid_task_hash{'xps.exe'}=1;	
	}			
	# Check that all running tasks are expected
	LogMsg("Checking running tasks");

	my $cmd="tasklist";
	my @task_list=`$cmd`;
	my $start_now=0;
	my $tvn_count=0;
	my $eft_count=0;	
	foreach my $t (@task_list) { 
		my @tmp=split(/\s+/,$t);
		my $task=lc($tmp[0]);			
		if ($task eq "tvnserver.exe") {
			$tvn_count++;			
		}
		if ($task eq "eftdevicedriver.exe") {
			$eft_count++;			
		}		
		next unless $task;
		if ($start_now) {
			next if ($task =~ /windows-kb/);	# Excuse Windows updates
			unless ($valid_task_hash{$task}) {				
				LogMsg("WARNING: $task is an unexpected task running");
				$warns++;
				if ($uninstall_hash{$task}) {					
					uninstall_apps();
				}
			}		
		}
		if ($task eq "system") {
			$start_now=1;
		}
	}
	if ($tvn_count > 1) {
		LogMsg(indent(1)."Found $tvn_count instances of tvnserver.");
		if ($current_hour < 6) {
			# Reset the TightVNC server - Sometimes we see more instances that we expect
			# so I am trying this - kdg
			LogMsg("NOTICE: Restarting the tvnserver because there are $tvn_count instances showing");
			restart_service("tvnserver");	# The tightvnc service is stopped and started
		}
	}	
	if ($eft_count > 1) {
		LogMsg(indent(1)."Found $eft_count instances of EFTDeviceDriver.");
		if ($current_hour < 6) {
			# Kill the POS and let posmonitor.pl restart it			
			LogMsg("WARNING: Found $eft_count instance of the EFTDeviceDriver.  Restarting POS...");
			exit_pos();
			
		}
	}		
 
	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking running tasks");
		$warning_count++;
	} else {
		LogMsg("No issues found checking running tasks.");
	}
}

sub check_running_services {
	# Check that all running services are expected
	LogMsg("Checking running services");

	if ($OS eq "Win7") {
		%valid_services_hash = %valid_win7_services_hash;
		@expected_service_list = @expected_services_win7_list;
		%start_service_hash = %start_service_win7_hash;
		%stop_service_hash = %stop_win7_service_hash;
	}

    	
	my $warns=0;	
	my $notes=0;

	my %enable_service_hash = (
		'wuauserv' => 'wuauserv'
	);	  
	if ($OS eq "WinXP") {
	
		$start_service_hash{'pcAnywhereHostService'} = 'awhost32';
	}
	
    # Verify services are enabled
	foreach my $s (sort keys(%enable_service_hash)) {
		if (check_enabled_service($s)) {	
			LogMsg(indent(1)."Service $s is enabled");
		} else {
			LogMsg(indent(1)."Service $s is not enabled");
			# Enable certain services			
			LogMsg(indent(1)."Enabling Service $s ...");
			my $rc=set_start($enable_service_hash{$s},"auto");  
			unless ($rc) {		
				LogMsg(indent(1)."WARNING: Failed to enable service $s - crs1");				 
				$warns++;		
			}	
		}
	}       	
	my $cmd="net start";
	my @service_list=`$cmd`;
 
	foreach my $s (@service_list) {
		chomp($s);
		
		$s=~ s/ //g;
		next if ($s =~ /Thecommandcompletedsuccessfully/);
		next unless $s;
		if ($verbose) {
			LogMsg("Service running: $s");
		}
		unless ($valid_services_hash{$s}) {
			
			$notes++;		
			if ($stop_service_hash{$s}) {
				LogMsg("Attempting to stop $s...");
				if (stop_service($stop_service_hash{$s})) {
					LogMsg("Success.");
					# If we were able to stop the service, we can decrement the warns
					$notes--;
				} else {
					LogMsg("NOTICE: $s is an unexpected service running");
					LogMsg("Failed to stop $s");
				}
			} else {
				LogMsg("NOTICE: $s is an unexpected service running");
				LogMsg("No provision to stop $s.");
			}
			if ($manual_service_hash{$s}) {	
				LogMsg("Setting $s to start only on demand");
				set_start($manual_service_hash{$s},"demand");
			}			
		}			
	}
	# Check that expected services are running
	foreach my $expected (@expected_service_list) {
		my $found=0;
		foreach my $s (@service_list) {
			chomp($s);
			$s=~ s/ //g;
			next unless $s;
			if ($expected eq $s) {
				$found=1;
			}
		}
		if ($found) {
			if ($verbose) {
				LogMsg(indent(1)."Found $expected running");
			}
		} else {
			if ($start_service_hash{$expected}) { 			
				unless (start_service($start_service_hash{$expected})) {
					LogMsg("WARNING: Failed to start $expected - crs2");
					$warns++;
					LogMsg("WARNING: $expected is not running - crs3");
					$warns++;					
				} else {
					LogMsg("NOTICE: Started $expected service - crs4");
					$notes++;
				}	
 				
			} else {
				LogMsg("WARNING: $expected is not running - crs5");
				$warns++;
			}
		}
	}
	# Make certain that tight vnc and ultra vnc are not both running
	my $found_tight=0;
	my $found_ultra=0;
	foreach my $s (@service_list) {
		chomp($s);
		$s=~ s/ //g;
		next unless $s;		
		if ($s =~ /TightVNCServer/i) {
			$found_tight=1;			
		}
		if ($s =~ /uvnc_service/i) {
			$found_ultra=1;			
		}	

	}	
	if ($found_ultra) {
		if ($found_tight) {
			# Stop the ultra vnc
			LogMsg("Found Tight VNC and Ultra VNC both running");
			my $service="uvnc_service";
			LogMsg("Stopping Ultra VNC");
			if (stop_service($service)) {
				LogMsg("Setting Ultra VNC for manual start");
				set_start($service,"demand");
			} else {
				LogMsg("WARNING: Found Tight VNC and Ultra VNC running - crs6");
				$warns++;
			}				
		}
	}	
 	if ($notes) {
		LogMsg("NOTICE: There were $notes notices checking running services");
	} 
	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking running services");
		$warning_count++;
	} else {
		LogMsg("No issues found checking running services.");
	}
}

sub check_poswin {	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="POSWIN";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ($reset_profilereport) {
			# If this value is set, we do not want to skip the check
			unless ((($timeseconds - $last_pass)/86400) > $freq) {
				# We do not have to run this test again 
				LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
				unless ($force) {return;}
			}
		}
	}
	LogMsg("Checking POSWIN");
	my $profileReport="c:/Program Files/SAP/Retail Systems/Point of Sale/rdata/ProfileReport.log";
	my $index=1;
	my $backup_file="c:/Program Files/SAP/Retail Systems/Point of Sale/rdata/ProfileReport_${index}.log";
	if (-f $profileReport) {
		my @file_info=stat($profileReport);
		my $size=$file_info[7];
		#if (($size > 15000000) || ($reset_profilereport)) {		
			while (-f $backup_file) {
				$index++;
				$backup_file="c:/Program Files/SAP/Retail Systems/Point of Sale/rdata/ProfileReport_${index}.log";
			}
			 
			rename($profileReport,$backup_file);
			if (-f $profileReport) {
				LogMsg("WARNING: Failed to rename $profileReport");	
				$warns++;				
			}
			if (-f $backup_file) {
				LogMsg("Successfully renamed profileReport.log");
			}	
			# Create a new profilereport.log
			open(NEWLOG,">$profileReport");
			close NEWLOG;
			#LogMsg(indent(1)."WARNING: Found profileReport.log to be $size bytes");
			#$warns++
		#} else {
		#	LogMsg(indent(1)."Found profileReport.log to be $size bytes");
		#}
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $profileReport");
		$warns++;
	}
 	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking POSWIN");
		$warning_count++
	} else {
		LogMsg("No issues found checking POSWIN.");
	}		
	update_action_log($check,$warns);
	$monitor_record_hash{$check}=$timeseconds;			
}

sub check_misc {
	# Put miscellaneous small checks here
	my $warns=0;
	if ($OS eq "WinXP") {
		# Check share
		my $cmd="net share";
		my @info=`$cmd`;
		my $found_c_share=0;
		my $found_simple_share=0;
		foreach my $i (@info) {
			chomp($i);
			if ($i =~ /^C/i) {				
				my @tmp=split(/\s+/,$i);
				if ($tmp[0] eq "C") {				
					$found_c_share=1;
				}
			}		 
		}
		unless ($found_c_share) {
			LogMsg(indent(1)."WARNING: Did not find the C drive shared");
			$warns++;
		}
		# Check Sharing
		$cmd = "reg query \"HKLM\\SYSTEM\\CurrentControlSet\\Control\\Lsa\"";
		@info=`$cmd`;
		foreach my $i (@info) {
			chomp($i);
			if ($i =~ /forceguest/i) {				
				my @tmp=split(/\s+/,$i);				
				if ($tmp[-1] eq "0x1") {
					$found_simple_share=1;
				} 
			}							
		}
		unless ($found_simple_share) {
			LogMsg(indent(1)."Did not find simple file sharing");
			 
		} else {
			LogMsg(indent(1)."Found simple file sharing as expected.");
		}	
		# Check that tail is supported
		my $tools_dir="c:/program files/tools";
		my $tools_dir_alt="c:/program files/pstools";
		unless (-d $tools_dir) {
			if (-d $tools_dir_alt) {
				$tools_dir=$tools_dir_alt;
			} else {
				LogMsg("WARNING: Failed to locate $tools_dir");
				$warns++;
			}
		}
		#my $tools_dir="c:/progra~1/tools";
		#my $source_dir="c:/temp/scripts";
		my $source_dir="//Edna/temp/install/register_utilities";				
		my $tail1="cygintl-1.dll";
		my $tail2="cygwin1.dll";
		my $tail3="tail.exe";
		if (-d $tools_dir) {
			foreach my $t ("$tail1","$tail2","$tail3") {
				unless (-f "${tools_dir}/${t}") {
					if (-f "${source_dir}/${t}") {
						LogMsg("Installing $t...");
						copy("${source_dir}/${t}","${tools_dir}/${t}");
						if ($verbose) {
							print "Copy ${source_dir}/${t} to ${tools_dir}/${t}\n";
						}
						unless (-f "${tools_dir}/${t}") {
							LogMsg("WARNING: Failed to install $t");
							$warns++;
						}					
					} else {
						LogMsg("WARNING: Failed to locate ${source_dir}/${t}");
						$warns++;
					}
				}
			}
		}
		# Check that logit.exe is in the utilities folder
		my $logit_destination_dir="C:/Program Files/SAP/Retail Systems/Utilities";
		my $logit_source_dir="C:/Util_95";
		my $logit_source_dir_alt="//Edna/temp/install/register_utilities";
		my $dest_file="${logit_destination_dir}/logit.exe";
		my $source_file="${logit_source_dir}/logit.exe";
		my $source_file_alt="${logit_source_dir_alt}/logit.exe";
		if (-f $source_file_alt) {
			$logit_source_dir=$logit_source_dir_alt;
			$source_file=$source_file_alt;
		}
		if (-d $logit_destination_dir) {
			unless (-f $dest_file) {
				# Need to copy the file if we can find the source
				if (-d $logit_source_dir) {
					if (-f $source_file) {
						copy($source_file,$dest_file);
						if (-f $dest_file) {
							LogMsg("Copy to $dest_file successful");
						} else {
							LogMsg("WARNING: Failed to copy $source_file to $dest_file");
							$warns++;						
						}
					} else {
						LogMsg("WARNING: Failed to locate $source_file");
						$warns++;						
					}
				} else {
					LogMsg("WARNING: Failed to locate $logit_source_dir");
					$warns++;					
				}
			} else {
				if ($verbose) {
					LogMsg("Located $dest_file");
				}
			}			
		} else {
			LogMsg("WARNING: Failed to locate $logit_destination_dir");
			$warns++;			
		}
	
		
		
		
	} elsif ($OS eq "Win7") {

 
		# Determine if network discovery and file and printer sharing is on or off
		if (1) {
			my $cmd="netsh advfirewall firewall show rule name=all";
			my @firewall_info=`$cmd`;
			my %net_discovery_hash; 
			my %file_sharing_hash;
			my %echo_ping_hash;
			my $rulename;
			my $rulesetting;
			
			foreach my $f (@firewall_info) {
				chomp($f);
				if ($f =~ /Profiles/) {
					my $profile;
					if ($rulename =~ /NetworkDiscovery/) {				
						my @tmp=split(/:/,$f);
						$profile=$tmp[1];
						$profile =~ s/ //g;	
						if ($profile =~ /Private/) {						
							$net_discovery_hash{$rulename}=$rulesetting;
						}
					}	
					if ($rulename =~ /FileandPrinterSharing/) {				
						my @tmp=split(/:/,$f);
						$profile=$tmp[1];
						$profile =~ s/ //g;	
						if ($profile =~ /Private/) {
							$file_sharing_hash{$rulename}=$rulesetting;
						}					 
					}	
					if ($rulename =~ /EnableEchoPingRequest/) {				
						my @tmp=split(/:/,$f);
						$profile=$tmp[1];
						$profile =~ s/ //g;							
						if ($profile =~ /Private/) {
							$echo_ping_hash{$rulename}=$rulesetting;
						}					 
					}						
				}
				if ($f =~ /Enabled/) {
					if ($rulename =~ /NetworkDiscovery/) {				
						my @tmp=split(/:/,$f);
						$rulesetting=$tmp[1];
						$rulesetting =~ s/ //g;	
						 
						#$net_discovery_hash{$rulename}=$rulesetting;
					}
					if ($rulename =~ /FileandPrinterSharing/) {				
						my @tmp=split(/:/,$f);
						$rulesetting=$tmp[1];
						$rulesetting =~ s/ //g;	
						#$file_sharing_hash{$rulename}=$rulesetting;
						 
					}
					if ($rulename =~ /EnableEchoPingRequest/) {				
						my @tmp=split(/:/,$f);
						$rulesetting=$tmp[1];
						$rulesetting =~ s/ //g;							
						#$file_sharing_hash{$rulename}=$rulesetting;

					}						
					
				}
				if ($f =~ /Rule Name/) {
					my @tmp=split(/:/,$f);
					$rulename=$tmp[1];
					$rulename =~ s/ //g;			
				}
			}
			my $yes_count=0;
			my $no_count=0;
			my $ambiguous_count=0;
			foreach $rulename (sort keys(%net_discovery_hash)) {
				if ($net_discovery_hash{$rulename} =~ /yes/i) {
					$yes_count++;
				} elsif ($net_discovery_hash{$rulename} =~ /no/i) {
					$no_count++;
				} else {
					$ambiguous_count++;
				}			
			}
			if ($yes_count) {
				unless ($no_count) {
					LogMsg("Network Discovery is Enabled");
				} else {
					LogMsg("Network Discovery - Enabled: $yes_count - Not Enabled: $no_count");
				}
			} elsif ($no_count) {
				LogMsg("WARNING: Network Discovery is NOT Enabled");
				$warns++;
			}
			$yes_count=0;
			$no_count=0;
			$ambiguous_count=0;
			foreach $rulename (sort keys(%file_sharing_hash)) {
				if ($file_sharing_hash{$rulename} =~ /yes/i) {
					$yes_count++;
				} elsif ($file_sharing_hash{$rulename} =~ /no/i) {
					$no_count++;
				} else {
					$ambiguous_count++;
				}			
			}
			if ($yes_count) {
				unless ($no_count) {
					LogMsg("File Sharing is Enabled");
				} else {
					LogMsg("File Sharing - Enabled: $yes_count - Not Enabled: $no_count");
				}
			} elsif ($no_count) {
				LogMsg("WARNING: File Sharing is NOT Enabled");
				$warns++;
			}		
			# Echo Ping
			$yes_count=0;
			$no_count=0;
			$ambiguous_count=0;
			foreach $rulename (sort keys(%echo_ping_hash)) {
				if ($echo_ping_hash{$rulename} =~ /yes/i) {
					$yes_count++;
				} elsif ($echo_ping_hash{$rulename} =~ /no/i) {
					$no_count++;
				} else {
					$ambiguous_count++;
				}			
			}
			if ($yes_count) {
				unless ($no_count) {
					LogMsg("Ping Echo is Enabled");
				} else {
					LogMsg("Ping Echo - Enabled: $yes_count - Not Enabled: $no_count");
				}
			} else   {
				LogMsg("WARNING: Ping Echo is NOT Enabled");
				$warns++;
				unless (configure_firewall_rule("echo_ping")) {
					LogMsg("WARNING: Failed to enable Ping Echo rule");
					$warns++;
				}
			}				
		}
	}
 
	if (-f $reboot_requested_flag) {
		LogMsg(indent(1)."NOTICE Reboot request flag found");
		schedule_reboot();
		unlink $reboot_requested_flag;
	}
	# This reboot request flag would be set by the Edna server
	my $reboot_requested_flag_alt="c:/temp/${hostname}_reboot_request.flg";		
	if (-f $reboot_requested_flag_alt) {
		LogMsg(indent(1)."NOTICE Alternate Reboot request flag found");
		schedule_reboot();
		unlink $reboot_requested_flag_alt;
	}	
	# Check if there is a link to TimeStar on the desktop for Villages
	my $link = "C:/Documents and Settings/Villages/Desktop/TimeStar.lnk";
	if ($storeStatus eq "Contract") {
		if (-f $link) {
			LogMsg(indent(1)."WARNING: TimeStar link on desktop is not needed");
			$warns++;
		}
	}
	if ($storeStatus eq "Company") {
		unless (-f $link) {
			# Check for anything that looks like TimeStar
			my $dir="C:/Documents and Settings/Villages/Desktop";
			my $linksource="//Edna/temp/install/shortcuts/TimeStar.lnk";
			opendir(DESKTOP,"$dir");
			my @file_list=readdir(DESKTOP);
			close DESKTOP;
			my $found_timestar=0;
			foreach my $f (@file_list) {
				next if ($f eq ".");
				next if ($f eq "..");			
				if ($f =~ /timestar/i) {
					$found_timestar=1;
				}
			}
			unless ($found_timestar) {
				# Try to install it
				if (-f $linksource) {
					copy($linksource,$link);
					unless (-f $link) {
						LogMsg(indent(1)."WARNING: TimeStar link on desktop was not found and failed to install it.");
						$warns++;						
					}
				} else {
					LogMsg(indent(1)."WARNING: TimeStar link on desktop was not found and failed to find source on Edna.");
					$warns++;					
				} 		
			}
		}
	}	
	
	$warns+=unpack_sap_pos();
	check_posmonitor_task("check");
	$warns+=check_timezone();
	$warns+=check_dns();	
	$warns+=check_dlls();
	if (($day_of_week == 6) || ($force)) {		
		$warns+=schedule_do_scan();
	}
	$warns+=sync_time();
	# Hopefully the sync_time() will take care of this
	#if ($correct_date_store_hash{$storenum}) {
	#	correct_date();
	#}
	
	check_scheduled_tasks();	
	$warns+=check_bkoff_ini();
	if ($warns) {
		LogMsg("WARNING: Found $warns doing miscellaneous checks.");
		$warning_count++;
	} else {
		LogMsg("No issues found doing miscellaneous checks.");
	}
}
 	
sub configure_firewall_rule {
	my $rule=shift;
	my $rc=0;
	if ($rule eq "echo_ping") {
		$rc=configure_echo_ping();
	} else {
		LogMsg("WARNING: $rule is not currently supported");		
	}
	return $rc;
}	

sub configure_echo_ping {
=pod
Rule Name:                            Enable Echo Ping Request
----------------------------------------------------------------------
Enabled:                              Yes
Direction:                            In
Profiles:                             Domain,Private,Public
Grouping:                             
LocalIP:                              Any
RemoteIP:                             Any
Protocol:                             ICMPv4
                                      Type    Code
                                      Any     Any 
Edge traversal:                       No
Action:                               Allow
=cut
	LogMsg("Configuring Echo Ping");
	my $rc=0;
	my $cmd="netsh advfirewall firewall add rule name=\"Enable Echo Ping Request\" dir=in 
	protocol=ICMPv4 profile=any enable=yes action=Allow RemoteIP=Any LocalIP=Any";
	my @firewall_info=`$cmd`;
 
	foreach my $f (@firewall_info) {
		chomp($f); 
		if ($f =~ /ok/i) {
			$rc=1;
			LogMsg(indent(1)."Echo Ping successfully configured");
		} else {
			LogMsg("WARNING: $f");
			$rc=0;
		}
		
	}
	return $rc;
}

sub cleanup {
	LogMsg("Cleanup");
	my $cleanup_count=0;	
	my @unwanted = (
		"C:/Documents and Settings/Villages/Favorites/Links/Free Hotmail.url",
		"C:/Documents and Settings/Villages/Favorites/Links/Web Slice Gallery.url",
		"C:/Documents and Settings/Villages/Favorites/Links/Suggested Sites.url",
		"C:/TESTKEY.KEY",
		"C:/Documents and Settings/Villages/Start Menu/Programs/Startup/startPOS.pl",
		
	);
 
	if ($storeStatus eq "Contract") {
		push(@unwanted,"C:/Documents and Settings/Villages/Desktop/Timestar.lnk");
	}
	# Check if there are any files in the polling folder	
	my $poll_dir="c:/polling";
	if (-d $poll_dir) {
		opendir(POLL,"$poll_dir");
		my @dir_list=readdir(POLL);
		close POLL;
		my $file_count=0;
		my $current_time = time();
		foreach my $d (@dir_list) {
			next if ($d eq ".");
			next if ($d eq "..");
			my $target="${poll_dir}/${d}";
			if (-f $target) {
				my @file_info = stat $target;
				# Find out how many days old this file is.  (One day = 86400 seconds).
				my $file_age = (($current_time - $file_info[9]) / 86400);
				$file_age = sprintf("%.2f", $file_age);
				LogMsg(indent(1)."$target is $file_age days old");
				if ($file_age > 10) {
					LogMsg(indent(2)."Purging $target");
					unlink $target;
				} else {
					$file_count++;
				}
			}
			
			
		}
		if ($file_count) {		
			LogMsg(indent(1)."WARNING: Found $file_count files in the $poll_dir");
			$warning_count++;
		}
	}
	# Check if there are dmp files in Symantec
	 
	my $dir="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/CurrentVersion/Data/Install/Logs";
	if (-d $dir) {
		opendir(DIR,"$dir");
		my @dir_list=readdir(DIR);
		close DIR;
		my $file_count=0;
		my $current_time = time();
		foreach my $d (@dir_list) {
			next if ($d eq ".");
			next if ($d eq "..");
			next unless ($d =~ /dmp$/);
			my $target="${dir}/${d}";
			if (-f $target) {
				my @file_info = stat $target;
				# Find out how many days old this file is.  (One day = 86400 seconds).
				my $file_age = (($current_time - $file_info[9]) / 86400);
				$file_age = sprintf("%.2f", $file_age);
				LogMsg(indent(1)."$d is $file_age days old");
				if ($file_age > 30) {
					LogMsg(indent(2)."Purging $d");
					unlink $target;
					$file_count++;
 
				}
			}
			
			
		}
		if ($file_count) {		
			LogMsg(indent(1)."Removed $file_count dmp files in the $dir");
			 
		}
	}	
	#
	foreach my $u (@unwanted) {
		if (-f $u) {			
			unlink $u;
			if (-f $u) {
				LogMsg("ERROR: Failed to remove $u");
				return 0;
			} else {
				LogMsg("Deleted $u");
				$cleanup_count++;
			}				
		}
	
	}
	LogMsg("Cleaned up $cleanup_count items.");
}	

sub uninstall_apps {
	# Uninstall commonly found applications that we do not want present
	my $dir="C:/Program Files/Google/Google Toolbar/Component";
	
	if (-d $dir) {
		my $tool;
		# Find the manager program
		opendir(COMP,"$dir");
		my @dir_list=readdir(COMP);
		close COMP;
		foreach my $d (@dir_list) {
			if ($d =~ /GoogleToolbarManager/i) {
				LogMsg("Found GoogleToolbarManager");
				$tool="\"${dir}\"/$d /uninstall /passive /quiet";
			}
		}
		# Uninstalling will prompt a new instance if Internet Explorer - we will kill it.
		# Get a list of ie sessions
		my @tasklist=`tasklist`;
		my @ie_list;
		foreach my $t (@tasklist) {
			chomp($t);			 
			if ($t =~ /iexplore/i) {
				my @tmp=split(/\s+/,$t);
				my $pid=$tmp[1];
				push(@ie_list,$pid);				 
			}
		}
		if ($tool) {
			LogMsg("Attempting to uninstall GoogleToolbarNotifier");
			system("$tool");
		}
		# Get the current list of ie sessions
		@tasklist=`tasklist`; 
		foreach my $t (@tasklist) {
			chomp($t);			 
			if ($t =~ /iexplore/i) {
				my @tmp=split(/\s+/,$t);
				my $pid=$tmp[1];
				my $found=0;
				foreach my $i (@ie_list) {				 
					if ($i == $pid) {						
						$found=1;
					}
				}
				unless ($found) {
					my $cmd="pskill $accepteula $pid";
					LogMsg("Killing $pid");
					system($cmd);
				}				 
			}
		}	
		# Uninstalling, leaves msiserver running - we will stop it
		my $cmd="sc stop msiserver";
		system($cmd);
		sleep 3;
		
	} else {
		if ($verbose) {
			LogMsg("Did not find Google Toolbar Manager.");
		}
	}

 		
}

sub stop_service {
    my $service=shift;       
    my $state="";
    my $attempt=0;
	my $cmd="sc qc \"$service\"";	
    my @service_info=`$cmd`;    
    foreach my $s (@service_info) {	
        if ($s =~ /The specified service does not exist/i) {		
            return 1;
        }        
    }	
    my $querycmd="sc query \"$service\"";
    my $stopcmd="sc stop \"$service\"";
    @service_info=`$querycmd`;   
    until (("$state" eq "STOPPED") || ($attempt > 10)) {
        $attempt++;
		LogMsg("Checking $service...");
        @service_info=`$querycmd`;         

        foreach my $s (@service_info) { 
            chomp($s);            
            if ($s =~ /STATE/) {                     
                my @tmp=split(/ /,$s);
                chomp($state=$tmp[$#tmp]);
                LogMsg("current state: $state");
            }
        }

        if ("$state" eq "RUNNING") {
            LogMsg("Stopping $service...");
            `$stopcmd`;
        }    
		unless ("$state" eq "STOPPED") {
			sleep 6;
		}
        
    }
    if ($state eq "STOPPED") {
        LogMsg("$service has been stopped");
    } else {
        LogMsg("Failed to stop $service");
        return 0;        
    }
	return 1;	
}

sub disable_service {
    my $service=shift;       
    my $state="";
    my $attempt=0;
	# First make certain the service is stopped
	if (stop_service($service)) {
		LogMsg("$service is not running.");		
	} else {		
		LogMsg("Failed to stop $service");
		return 0;  
	}	
    my $querycmd="sc qc \"$service\"";
    my $disablecmd="sc config \"$service\" start= disabled";
    my @service_info=`$querycmd`;  	
    until (("$state" eq "DISABLED") || ($attempt > 4)) {
        $attempt++;
        LogMsg("Checking $service...");
        @service_info=`$querycmd`;         

        foreach my $s (@service_info) { 
            chomp($s);            
            if ($s =~ /START_TYPE/) {           
                my @tmp=split(/ /,$s);
                chomp($state=$tmp[$#tmp]);
                LogMsg("current state: $state");
            }
        }

        if ("$state" ne "DISABLED") {
            LogMsg("Disabling $service...");
            `$disablecmd`;			
        }    
		unless ("$state" eq "DISABLED") {
			sleep 5;
		}        
    }
    if ($state eq "DISABLED") {
        LogMsg("$service has been disabled");
    } else {
        LogMsg("Failed to disable $service");
        return 0;        
    }
	return 1;
}

sub set_start {
    my $service=shift;
    my $setting=shift;
    LogMsg("Setting $service to start to $setting...");
    my $cmd="sc config \"$service\" start= $setting";     
 
    my $start_type;
    my @info=`$cmd`;
    my $result=0;
    foreach my $i (@info) {        
        if ($i =~ /SUCCESS/) {
            $result=1;             
        }  
    }        
    return $result;
}

sub query_service {
    my $service=shift;       
    my $state="";
    my $attempt=0;
    my $querycmd="sc query \"$service\"";
    my @service_info=`$querycmd`;   
    my $found=0;
	foreach my $s (@service_info) {       
        if ($s =~ /SERVICE_NAME/) {
            if ($s =~ /$service/) {
                $found=1;
            }
        }
		if ($s =~ /STATE/)  {    
			my @tmp=split(/\s+/,$s);            
			chomp($state=$tmp[-1]);            
			return $state;
		}
	}
    if ($found) {
        return $state;
    } else {
        return "not_found";
    }
}

sub check_windows_update_results {
	# Are windows updates enabled?
	my $AutomaticUpdates=0;
	foreach my $e (@expected_service_list) {
		if ($e eq "AutomaticUpdates") {
			$AutomaticUpdates=1;
		}
	}
	return unless ($AutomaticUpdates);	# Do not check unless automatic updates are enabled.
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="WINUP";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}

	LogMsg("Checking Windows Updates");
	my $winuplog="c:/Windows/WindowsUpdate.log";
	my $update_count=0;
 
	if (-f $winuplog) {		
		my @file_info=stat($winuplog);
		my $current_timeseconds=time();				
		my $file_age = (($current_timeseconds - $file_info[9]) / 86400);         
		$file_age = sprintf("%.2f", $file_age); 
		if ($file_age > 7) {		
			# Second check - It seems that checking this age is not always accurate.  So, next we open
			# the log file and read the last date.
			open(LOG,"$winuplog");
			my @winlog_info=(<LOG>);
			close LOG;
			my $last_line=$winlog_info[-1];
			my @tmp=split(/\s+/,$last_line);
			my $date=$tmp[0];
			@tmp=split(/-/,$date);
			my $uyear=$tmp[0];
			my $umonth=$tmp[1];
			my $uday=$tmp[2];
			unless (($uyear == $current_year) && ($umonth == $current_month) && ($uday == $current_day)) {
				my $mon=$umonth;
				$mon--;
				my $utime = timelocal(0,0,0,$uday,$mon,$uyear);				
				my $uage = (($current_timeseconds - $utime) / 86400);
				if ($uage > 7) {				 
					LogMsg(indent(1)."WARNING: Update log is $uage days old!");
					$warns++;				
				} else {				
					LogMsg(indent(1)."Update log is $uage days old");
				}
			} else {
				LogMsg(indent(1)."Update log was updated on $date");
			}			
		} else {		
			LogMsg(indent(1)."Update log is $file_age days old");
		}
		# Read the log and determine if windows updates are happening
		open(LOG,"$winuplog");
		my @win_update_info=(<LOG>);
		close LOG;
		foreach my $line (@win_update_info) {
			chomp($line);
			if ($line =~ /updates detected/) {
				if ($line =~ /$date_label/) {					
					$update_count++;
				}
				if ($line =~ /$previous_date_label/) {					
					$update_count++;
				}
				if ($line =~ /$previous_previous_date_label/) {					
					$update_count++;
				}				
			}
		}
		unless ($update_count) {		 
			LogMsg(indent(1)."WARNING: Failed to find an update count");
			$warns++;		
		}
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $winuplog");
		$warns++;
	}
	update_action_log($check,$warns);
 	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking Windows Updates");
		$warning_count++
	} else {
		LogMsg("No issues found checking Windows Updates.");
	}		
	$monitor_record_hash{$check}=$timeseconds;	
}

sub checkPID {
	my $taskname = shift;
	my @process_list = `tlist`;
	my $pid = 0;

	foreach my $line (@process_list) {
		if ($line =~ /$taskname/i) { 
			$pid=substr($line,0,4);
			$pid=~ s/ //g;
			#my @tmp=split(/\s+/,$line);
			#$pid=$tmp[0];
		}
	}
	return $pid;
}

sub alternateCheck {
	my $taskname = shift;
	my @process_list = `tasklist`;
	my $found = 0;

	foreach my $line (@process_list) {
		if ($line =~ /$taskname/i) { 
			$found=1;			
		}
	}
	return $found;
}

sub check_firewall {
    my $warns=0;
	if ($OS eq "Win7") { 
		$warns+=check_firewall_win7();
		return $warns;
	}
    my $key="";
    my $start_reading=0;
	my $fw_status=0;
	my $configuration=0;
    my %enabled_domain_items=(
        'NetBIOS'=>'0',
        'SMB over TCP'=>'0',     
    );
    my %enabled_items=(
        'javaw'=>0,
        
        'Xpress Server'=>'0',
        'pcAnywhere Host'=>'0',
 
        'Remote Assistance'=>'0',
        'Network Diagnostics'=>'0',
        'SQL Anywhere'=>'0',
        'Web'=>'0',
        'MLINK'=>'0',
        'Telnet'=>'0',
        'Transnet'=>'0',
 
        'Exception mode'=>'0',
        'response mode'=>'0',
        'Notification mode'=>'0',
        'File and Printer Sharing'=>'0',
        
    );

    
    LogMsg("Checking Firewall");
 
	
    my @firewall_info=`netsh firewall show config`;
    foreach my $f (@firewall_info) {
		
		if ($f =~ /configuration/) {
			$configuration=0;	# Reset this			
		}		
        if ($f =~ /Standard profile configuration/) {
            $start_reading=1;
			$configuration="Standard profile configuration";			
        }
        if ($f =~ /Local Area Connection firewall configuration/) { 
			$start_reading=0;		
			$configuration="Standard profile configuration";			
        }		
        if ($f =~ /Enable/) {
            foreach $key (keys(%enabled_domain_items)) {
                if ($f =~ /$key/) {
                    $enabled_domain_items{$key}=1;
                }
            }
        }        
        if ($start_reading) {            
            if ($f =~ /Operational mode/) {
				LogMsg(indent(1)."Configuration: $configuration");
				LogMsg(indent(1)."$f");
                if ($f =~ /Disable/) {					
					LogMsg(indent(1)."Firewall may be disabled");
					#$warns++;	
                } elsif ($f =~ /Enable/) {								
					if ($configuration) {
						LogMsg(indent(1)."Firewall is enabled ($configuration)");
						$fw_status=1;						
					}
				} else {
					LogMsg(indent(1)."Firewall status is unknown.");
				}
            }
            if ($f =~ /Enable/) {				
				if ($OS eq "WinXP") {
					foreach $key (keys(%enabled_items)) {
						if ($f =~ /$key/i) {
							$enabled_items{$key}=1;
							if ($verbose) {
								print "$f\n";
							}
						}
					}
				}
            }

        }        
    }

	if ($OS eq "WinXP") {
		# Check that all of the enabled items were found to be enabled
		foreach $key (keys(%enabled_items)) {      
			unless ($enabled_items{$key}) {
				LogMsg(indent(1)."WARNING: item $key may be disabled");
				$warns++;	        
			}
		}
		foreach $key (keys(%enabled_domain_items)) {
			unless ($enabled_domain_items{$key}) {
				LogMsg(indent(1)."WARNING: $key may be disabled");
				$warns++;	        
			}
		}   
		unless ($fw_status) {
			# If the firewall is disabled, here we attempt to enable it
			LogMsg(indent(1)."Enabling Firewall...");
			my $cmd="netsh firewall set opmode mode = ENABLE";
			my @results = `$cmd`;
			foreach my $r (@results) {
				chomp($r);
				if ($r =~ /ok/i) {
					LogMsg(indent(2)."Firewall Successfully Enabled");
					$fw_status=1;
				}				
			}
		}
		unless ($fw_status) {
			LogMsg(indent(1)."WARNING: Firewall is disabled");
			$warns++;		
		}
	}
 
	if ($warns) {	        
        LogMsg(indent(1)."WARNING: $warns Firewall issues found");
      
        $warning_count++;
	}  else {
       LogMsg(indent(1)."No Firewall issues found");
   
    }
    
}

sub check_firewall_win7 {
    my $warns=0; 
    my $key="";
    my $start_reading=0;
 
	my $fw_status_on=0;
	my $fw_status_off=0;
	my $profile=0;
	my $state=0;
 

    
    LogMsg("Checking Firewall");
 
	my $cmd="netsh advfirewall show allprofiles";
 
    my @firewall_info=`$cmd`;
    foreach my $f (@firewall_info) { 
		chomp($f);
		if ($f =~ /Profile Settings:/) {
			$profile=$f;			
		}		
		if ($f =~ /State/) {
			my @tmp=split(/\s+/,$f);
			$state=$tmp[-1];
			if ($state eq "ON") {
				LogMsg("$profile $state");
				$fw_status_on++;
			} else {
				LogMsg("WARNING: $profile $state");
				$fw_status_off++;
			}			
		}					
    }
	if ($fw_status_off) {
		# If the firewall is disabled, here we attempt to enable it
		LogMsg(indent(1)."Enabling Firewall...");
		my $cmd="netsh advfirewall set allprofiles state on";
		#my $cmd="NetSh Advfirewall set allrprofiles state on";
		my @results = `$cmd`;
		foreach my $r (@results) {
			chomp($r);				
			if ($r =~ /ok/i) {
				LogMsg(indent(2)."Firewall Successfully Enabled");
				$fw_status_on=1;
			}				
		}	
	}
	unless ($fw_status_on) {
		LogMsg(indent(1)."WARNING: Firewall is disabled");
		$warns++;		
	}	

 	
	if ($warns) {	        
        LogMsg(indent(1)."WARNING: $warns Firewall issues found");
      
        $warning_count++;
	}  else {
       LogMsg(indent(1)."No Firewall issues found");
   
    }
	return $warns;
    
}

sub check_printers {
	# This function really only checks what printers are configured and what their IP address is.
	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="printers";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}
	LogMsg("Checking printers...");	
	my @printers=();
	my $cmd="reg query \"HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers\" ";    
	my @info=`$cmd`;  
	foreach my $i (@info) {
		chomp($i);        
		
		next if ($i =~ /fax/i) ;
			
		next unless ($i =~ /print.printers/i);
		my @tmp=split(/inters\\/,$i);
		
		my $printer=$tmp[-1];
		next if ($printer =~ /HKEY_LOCAL_MACHINE/);		
		push(@printers,$printer);
	}
	# Now see the info on each printer
	foreach my $p (@printers) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Print\\Printers\\$p\" "; 
		@info=`$cmd`;  
		foreach my $i (@info) {
			chomp($i);	
			if (($i =~ /Port/) && ($i =~ /192.168/)) {
				
				my @tmp=split(/\s+/,$i);
				my $IP=$tmp[-1];
				@tmp=split(/_/,$IP);
				foreach my $t (@tmp) {
					if ($t =~ /192.168/) {
						$IP=$t;
					}
				}
				LogMsg("PRINTER: $p IP: $IP");				
			}
		}
	}
	update_action_log($check,$warns);
    if ($warns) {
        LogMsg(indent(1)."WARNING: $warns issues found checking printers");          
        $warning_count++;
    } else {
		$monitor_record_hash{$check}=$timeseconds;
	}		
}

sub check_permissions {
	unless ($OS eq "WinXP") {
		LogMsg("check_permissions not required for $OS");		
		return;
	}	
	# This function really only checks what permissions are configured.
	# This was created after installing SAP POS 2.3 where the permissions of the parm folder disappeared for user villages
	# on the Lancaster store soon after it was installed.
	my $dir="c:/Program Files/SAP";
	unless (-d $dir) {
		# This is only for SAP POS
		return;
	}
	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=7;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="permissions";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless (($force) || ($perms_only)) {return;}
		}
	}
	LogMsg("Checking permissions...");	
	#C:\Program Files\SAP\Retail Systems\Point of Sale\Parm
	my $found_expected=0;
	$dir="c:/Program Files/SAP/Retail Systems/Point of Sale/parm";
	my @dir_list=(
		"c:/Program Files/SAP/Retail Systems/Point of Sale",
		"c:/Program Files/SAP/Retail Systems/Point of Sale/parm",
		"c:/Program Files/SAP/Retail Systems/Point of Sale/TTVEFT",
	);
	foreach $dir (@dir_list) {
		LogMsg("Checking permissions for $dir...");
		$found_expected=0;
		if (-d $dir) {
			my $cmd="cacls \"$dir\"";
	 
			my @info=`$cmd`;
			foreach my $i (@info) {
				chomp($i);
				 
				if ($i =~ /Villages/i) {
					if ($i =~ /\(OI\)\(CI\)C/) {
						$found_expected=1;
					}
					if ($i =~ /Villages:F/) {
						$found_expected=1;
					}					
					if ($verbose) {
						print "$i\n";
					}			 
				}
				if ($dir =~ /EFT/) {
					if ($i =~ /Users:\(OI\)\(CI\)\(IO\)\(special access:\)/) {
						$found_expected=1;
					}						
				}
			}
			unless ($found_expected) {
				LogMsg("WARNING: Failed to find expected permissions for $dir");
				$warns++;
			}
		} else {
			LogMsg(indent(1)."WARNING: Failed to locate $dir");
			$warns++;
		}
	}
	update_action_log($check,$warns);
    if ($warns) {
        LogMsg(indent(1)."WARNING: $warns issues found checking permissions.");          
        $warning_count++;
    } else {
		$monitor_record_hash{$check}=$timeseconds;
	}		
}

sub check_antivirus_version {
	 
    my $antivirus_version=0;

	my $sep_found=0;
	my $smc_found=0;
	my $sep11_found=0;
	my $sep12_found=0;
    my $landesk_found=0;
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\"  ";    
	my @info=`$cmd`;
    my $continue=1;
	if ($?) {
		$continue=0;
		next;
	}                        
	foreach my $i (@info) {        
		if ($i =~ /Symantec Endpoint Protection/) {						
			$sep_found = 1;		
            
		}
	}	
	if ($sep_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\"  ";    
		@info=`$cmd`;
		if ($?) {
			$continue=0;
			next;
		}                        
		foreach my $i (@info) {        
			if ($i =~ /SMC/) {						
				$smc_found = 1;					
			}
		}		
	}
	if ($smc_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\SMC\" /s";    
		@info=`$cmd`;
		if ($?) {
			$continue=0;
			next;
		}                        
		foreach my $i (@info) {
			if ($i =~ /ProductVersion/) {	
                chomp($i);
                my @tmp=split(/\s+/,$i);
                $antivirus_version=$tmp[-1];            				
			}						 
		}		
	}	    
    unless ($smc_found) {
 
        $cmd="reg query \"HKLM\\SOFTWARE\\Intel\" /S ";    
        @info=`$cmd`;
                     
        foreach my $i (@info) {        
            if ($i =~ /LANDesk/) {						
                $landesk_found = 1;		                 
            }
        }	        
 
    }
	if ($landesk_found) {
		unless ($antivirus_version) {
			# Most likely this
			$antivirus_version = 10;
		}
	}
  
	return "$antivirus_version";
 
}

sub check_CustomScheduledScan_12 {
 
	LogMsg("Checking CustomScheduledScan 12");
	# Look for the CustomScheduledScan
	my $found_custom_scan=0;
	my $scan_id=0;
    my $no_scan_id_needed=0;

	my $warns=0;
    my @scheduler_list;
    my $custom_scheduled_task=0;
    my $scheduler_found=0;
    # The settings we wish to determine:
    my $CloseScan=0;
    my $CloseScan_correct=0;
    my $DisplayStatusDialog=0;
    my $DisplayStatusDialog_correct=0;
    my $DisplayIfThreat=0;   
    my $DisplayIfThreat_correct=0;
	my $enabled=0;
	my $dow=0;
	my $mod=0;    
	my $current_user_task=0;
	# The settings might be under Current User:
	my $cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\" /s ";     
    my @info=`$cmd`; 
	foreach my $i (@info) {
		chomp($i);	
		if ($i =~ /CustomScheduledScan/) {
			$current_user_task=1;
		}		
	}		
	$current_user_task=0;	
	if ($current_user_task) {
		# Look for the settings under HKEY_CURRENT_USER
		$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\"  ";     	 
		@info=`$cmd`;	 
		foreach my $i (@info) {
			chomp($i);			
            if ($i =~ /NO NAME/) {
                my @tmp=split(/\s+/,$i);
                $custom_scheduled_task=$tmp[-1];                				
            }	 			 	
		}					
	} else {	
		# Look under HKEY_LOCAL_MACHINE
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\" ";    
	 
		@info=`$cmd`;
	 
		foreach my $i (@info) {
			chomp($i);		         
			if ($i =~ /Scheduler/) {
				my @tmp=split(/Scheduler/,$i);
				if ($tmp[-1]) {
					unless ($tmp[-1] =~ /HKEY_LOCAL_MACHINE/) {
						push(@scheduler_list,$tmp[-1]);
					}
				}
			}		
		}			
		# Examine each scheduler to find the particular task that is the custom scheduled scan
		foreach my $scheduler (@scheduler_list) {
			my $scheduled=0;			
			#$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$scheduler\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\" ";                    
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$scheduler\\Custom Tasks\" ";                    
			@info=`$cmd`;  
			foreach my $i (@info) {
				chomp($i);					
				if ($i =~ /TaskPadScheduled/) {				
					$scheduled=1; 
				}		
			}
			if ($scheduled) {
				$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$scheduler\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\" ";                    
				@info=`$cmd`;  
				foreach my $i (@info) {
					chomp($i);						
					if ($i =~ /NO NAME/) {
						my @tmp=split(/\s+/,$i);
						$custom_scheduled_task=$tmp[-1];
						$scheduler_found=$scheduler;
					}		
				}				
			}
		}
	}	
    if ($custom_scheduled_task) {
        # Now we can get the info we are looking for
        if ($current_user_task) {
			$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\$custom_scheduled_task\"  ";     		
		} else {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$scheduler_found\\Custom Tasks\\$custom_scheduled_task\"";         
        }
		@info=`$cmd`;   
        foreach my $i (@info) {
            chomp($i);             
            if ($i =~ /CloseScan/) {											
                my @tmp = split(/x/,$i);				
                $CloseScan = $tmp[-1]; 
                if ($CloseScan == 0) {
                    $CloseScan_correct=1;
                    if ($verbose) {
                        print "CloseScan is set correctly\n";
                    }
                } else {
                    LogMsg(indent(2)."WARNING: CloseScan is set to $CloseScan");
                    $warns++;
                }                
               
            }	        
            if ($i =~ /DisplayStatusDialog/) {											
                if ($i =~ /DisplayStatusDialogIfThreatDetected/) {
                    my @tmp = split(/x/,$i);				
                    $DisplayIfThreat = $tmp[-1];   
                    if ($DisplayIfThreat == 0) {
                        $DisplayIfThreat_correct=1;
                        if ($verbose) {
                            print "DisplayIfThreat is set correctly\n";
                        }
                    } else {
                        LogMsg(indent(2)."WARNING: DisplayIfThreat is set to $DisplayIfThreat");
                        $warns++;
                    }
                    
                } else {
                    my @tmp = split(/x/,$i);				
                    $DisplayStatusDialog = $tmp[-1];  
                    if ($verbose) {
                        print "($i) ($DisplayStatusDialog)\n";
                    }
                    if ($DisplayStatusDialog == 0) {
                        $DisplayStatusDialog_correct=1;
                        if ($verbose) {
                            print "DisplayStatusDialog is set correctly\n";
                        }
                    } else {
                        LogMsg(indent(2)."WARNING: DisplayStatusDialog is set to $DisplayStatusDialog");
                        $warns++;
                    }                    
                }
            }            
        }
		
        # Get the schedule info
        if ($current_user_task) {
			$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\$custom_scheduled_task\\Schedule\"  ";		
        } else {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$scheduler_found\\Custom Tasks\\$custom_scheduled_task\\Schedule\"";         
        }
		@info=`$cmd`;       
        foreach my $i (@info) {
            chomp($i);
            if ($i =~ /Enabled/)  {											
                unless ($i =~ /Missed/) {
                    my @tmp = split(/\s+/,$i);				
                    $enabled = $tmp[-1];									
                }
            }
            if ($i =~ /DayOfWeek/) {											
                my @tmp = split(/\s+/,$i);				
                $dow = $tmp[-1];								
            }	
            if ($i =~ /MinOfDay/) {											
                my @tmp = split(/\s+/,$i);				
                $mod = $tmp[-1];						
            }	            
        }
        # Evaluate:
        if ($DisplayIfThreat) {
            LogMsg("WARNING: CustomScheduledScan is set to display if threat detected");
            $warns++;
        }
        if ($DisplayStatusDialog) {
            if ($CloseScan) {
               LogMsg("NOTICE: CustomScheduledScan is set to display dialog and then close");
            } else {        
                LogMsg("WARNING: CustomScheduledScan is set to display dialog");
                $warns++;
            }
        }   
        if ($enabled eq "0x1") {
            LogMsg(indent(1)."CustomScheduledScan is enabled");
        } else {
            LogMsg("WARNING: CustomScheduledScan is not enabled");
            $warns++;
        }
        if ($dow eq "0x0") {
            LogMsg(indent(1)."CustomScheduledScan is set for Sunday");
        } else {
            LogMsg("WARNING: CustomScheduledScan is not set for Sunday");
            $warns++;
        }   
        if ($CloseScan_correct) {
            LogMsg(indent(1)."CloseScan is correct");
        } else {
            LogMsg("WARNING: CloseScan is not set for $CloseScan");
            $warns++;            
        }
        if ($DisplayStatusDialog_correct) {
            LogMsg(indent(1)."DisplayStatusDialog is correct");
        } else {
            LogMsg("WARNING: DisplayStatusDialog is not set for $DisplayStatusDialog");
            $warns++;            
        }    
        if ($DisplayIfThreat_correct) {
            LogMsg(indent(1)."DisplayIfThreat is correct");
        } else {
            LogMsg("WARNING: DisplayIfThreat is not set for $DisplayIfThreat");
            $warns++;            
        }             
       
            
        
    } else {
        LogMsg(indent(1)."ERROR: Failed to locate CustomScheduledScan");
        $warns++;
    }
    if ($warns) {
        LogMsg(indent(1)."WARNING: Found $warns issues checking CustomScheduledScan");
        $warning_count++;        
    }
 
}

sub check_antivirus_scan {
	# Look for the CustomScheduledScan
	my $found_custom_scan=0;
	my $scan_id=0;
	my $enabled=0;
	my $dow=0;
	my $mod=0;
	my $warns=0;
	my $cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\TaskPadScheduled\" ";    
	my @info=`$cmd`;
	
	foreach my $i (@info) {
		chomp($i);
		 
		if ($i =~ /CustomScheduledScan/) {
			$found_custom_scan=1;
		}
	}	
	if ($found_custom_scan) {
		$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\" ";    
		@info=`$cmd`;		
		foreach my $i (@info) {
			chomp($i);			
			if ($i =~ /NO NAME/) {											
				my @tmp = split(/\s+/,$i);				
				$scan_id = $tmp[-1];				
			}
		}		
		LogMsg("Found CustomScheduledScan");
	} else {
		LogMsg("WARNING: Failed to find CustomScheduledScan");
		$warns++;		
	}
	if ($scan_id) {						
		$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\$scan_id\\Schedule\" ";    
		@info=`$cmd`;		
		foreach my $i (@info) {
			chomp($i);	
			
			if ($i =~ /Enabled/)  {											
				unless ($i =~ /Missed/) {
					my @tmp = split(/\s+/,$i);				
					$enabled = $tmp[-1];									
				}
			}
			if ($i =~ /DayOfWeek/) {											
				my @tmp = split(/\s+/,$i);				
				$dow = $tmp[-1];								
			}	
			if ($i =~ /MinOfDay/) {											
				my @tmp = split(/\s+/,$i);				
				$mod = $tmp[-1];						
			}				
		}	
		# Evaluate what we found
		unless ($enabled eq "0x1") {
			LogMsg("WARNING: CustomScheduledScan is not enabled");
			$warns++;
		}
		unless ($dow eq "0x0") {
			LogMsg("WARNING: CustomScheduledScan is not set for Sunday");
			$warns++;
		}
		unless ($mod eq "0x0") {
			my $dec_mod = $mod;
			$dec_mod =~ s/0x//;
			$dec_mod = (hex($dec_mod)/60);
			unless (($dec_mod eq "12") || ($dec_mod eq "1")) {
				LogMsg(indent(1)."WARNING: CustomScheduledScan is set for $dec_mod");
				$warns++;
			}
			 
			#LogMsg("WARNING: CustomScheduledScan is not set for 1:00 AM");
			#$warns++;
		}			
	} else {
		LogMsg("WARNING: Failed to find Scan ID");
		$warns++;		
	}
	if ($warns) {
		$warning_count++;
	}
	
}

sub check_antivirus_autoupdates {
	# Look for the AutoUpdate setting 
	my $enabled=0; 
	my $warns=0;
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\LiveUpdate\\Schedule\" ";    
	my @info=`$cmd`;
	
	foreach my $i (@info) {
		chomp($i);		 
		if ($i =~ /\sEnabled\s/)  {				
			my @tmp = split(/\s+/,$i);				
			$enabled = $tmp[-1];	
			@tmp=split(/x/,$enabled);
			$enabled = $tmp[1];	
		} 						 	
	}	
	if ($enabled) {
		LogMsg(indent(1)."WARNING: Symantec Endpoint Protection is set to update automatically");
		$warning_count++;
	}
	
}


sub check_antivirus_log {
	LogMsg("Checking Antivirus Log");
	my $antivirus_log=shift;
	my $warns=0;
	if (-d $antivirus_log) {
        my %event_hash=(
        '1' => 'GL_EVENT_IS_ALERT', 
        '2' => 'GL_EVENT_SCAN_STOP',
        '3' => 'GL_EVENT_SCAN_START',
        '4' => 'GL_EVENT_PATTERN_UPDATE',
        '5' => 'GL_EVENT_INFECTION',
        '6' => 'GL_EVENT_FILE_NOT_OPEN',
        '7' => 'GL_EVENT_LOAD_PATTERN',
        '10' => 'GL_EVENT_CHECKSUM',
        '11' => 'GL_EVENT_TRAP',
        '12' => 'GL_EVENT_CONFIG_CHANGE',
        '13' => 'GL_EVENT_SHUTDOWN',
        '14' => 'GL_EVENT_STARTUP',
        '16' => 'GL_EVENT_PATTERN_DOWNLOAD',
        '17' => 'GL_EVENT_TOO_MANY_VIRUSES',
        '18' => 'GL_EVENT_FWD_TO_QSERVER',
        '19' => 'GL_EVENT_SCANDLVR',
        '20' => 'GL_EVENT_BACKUP',
        '21' => 'GL_EVENT_SCAN_ABORT',
        '22' => 'GL_EVENT_RTS_LOAD_ERROR',
        '23' => 'GL_EVENT_RTS_LOAD',
        '24' => 'GL_EVENT_RTS_UNLOAD',
        '25' => 'GL_EVENT_REMOVE_CLIENT',
        '26' => 'GL_EVENT_SCAN_DELAYED',
        '27' => 'GL_EVENT_SCAN_RESTART',
        '28' => 'GL_EVENT_ADD_SAVROAMCLIENT_TOSERVER',
        '29' => 'GL_EVENT_REMOVE_SAVROAMCLIENT_FROMSERVER',
        '30' => 'GL_EVENT_LICENSE_WARNING',
        '31' => 'GL_EVENT_LICENSE_ERROR',
        '32' => 'GL_EVENT_LICENSE_GRACE',
        '33' => 'GL_EVENT_UNAUTHORIZED_COMM',
        '34' => 'GL_EVENT_LOG_FWD_THRD_ERR',
        '35' => 'GL_EVENT_LICENSE_INSTALLED',
        '36' => 'GL_EVENT_LICENSE_ALLOCATED',
        '37' => 'GL_EVENT_LICENSE_OK',
        '38' => 'GL_EVENT_LICENSE_DEALLOCATED',
        '39' => 'GL_EVENT_BAD_DEFS_ROLLBACK',
        '40' => 'GL_EVENT_BAD_DEFS_UNPROTECTED',
        '41' => 'GL_EVENT_SAV_PROVIDER_PARSING_ERROR',
        '42' => 'GL_EVENT_RTS_ERROR',
        '43' => 'GL_EVENT_COMPLIANCE_FAIL',
        '44' => 'GL_EVENT_COMPLIANCE_SUCCESS',
        '45' => 'GL_EVENT_SECURITY_SYMPROTECT_POLICYVIOLATION',
        '46' => 'GL_EVENT_ANOMALY_START',
        '47' => 'GL_EVENT_DETECTION_ACTION_TAKEN',
        '48' => 'GL_EVENT_REMEDIATION_ACTION_PENDING',
        '49' => 'GL_EVENT_REMEDIATION_ACTION_FAILED', 
        '50' => 'GL_EVENT_REMEDIATION_ACTION_SUCCESSFUL',
        '51' => 'GL_EVENT_ANOMALY_FINISH',
        '52' => 'GL_EVENT_COMMS_LOGIN_FAILED',
        '53' => 'GL_EVENT_COMMS_LOGIN_SUCCESS',
        '54' => 'GL_EVENT_COMMS_UNAUTHORIZED_COMM',
        '55' => 'GL_EVENT_CLIENT_INSTALL_AV',
        '56' => 'GL_EVENT_CLIENT_INSTALL_FW',
        '57' => 'GL_EVENT_CLIENT_UNINSTALL',
        '58' => 'GL_EVENT_CLIENT_UNINSTALL_ROLLBACK',
        '59' => 'GL_EVENT_COMMS_SERVER_GROUP_ROOT_CERT_ISSUE',
        '60' => 'GL_EVENT_COMMS_SERVER_CERT_ISSUE',
        '61' => 'GL_EVENT_COMMS_TRUSTED_ROOT_CHANGE',
        '62' => 'GL_EVENT_COMMS_SERVER_CERT_STARTUP_FAILED',
        '63' => 'GL_EVENT_CLIENT_CHECKIN',
        '64' => 'GL_EVENT_CLIENT_NO_CHECKIN',
        '65' => 'GL_EVENT_SCAN_SUSPENDED',
        '66' => 'GL_EVENT_SCAN_RESUMED',
        '67' => 'GL_EVENT_SCAN_DURATION_INSUFFICIENT',
        '68' => 'GL_EVENT_CLIENT_MOVE',
        '69' => 'GL_EVENT_SCAN_FAILED_ENHANCED',
        '70' => 'GL_EVENT_MAX_EVENT_NUMBER',
        '72' => 'GL_EVENT_INTERESTING_PROCESS_DETECTED_START',
        '73' => 'GL_EVENT_LOAD_ERROR_COH',
        '74' => 'GL_EVENT_LOAD_ERROR_SYKNAPPS',
        '75' => 'GL_EVENT_INTERESTING_PROCESS_DETECTED_FINISH',
        '76' => 'GL_EVENT_HPP_SCAN_NOT_SUPPORTED_FOR_OS',
        '77' => 'GL_EVENT_HEUR_THREAT_NOW_KNOWN',
        );
        my %category_hash=(
        '1' => 'GL_CAT_INFECTION',
        '2' => 'GL_CAT_SUMMARY',
        '3' => 'GL_CAT_PATTERN',
        '4' => 'GL_CAT_SECURITY',
        );
        my %category_count_hash;
		# Look for the log files for this month
		opendir(UPLOAD,"$antivirus_log");
		my @dir_list=readdir(UPLOAD);
		close UPLOAD;	
		# Determine the most recent log
		my $most_recent=0;
		my $most_recent_file;
		foreach my $file (@dir_list) {
			next if ($file eq ".");
			next if ($file eq "..");
			my $target="${antivirus_log}/${file}";
			my @file_info=stat($target);
			my $file_date=$file_info[9];			
			if ($file_date > $most_recent) {
				$most_recent=$file_date;
				$most_recent_file=$file;				
			}
            my $current_timeseconds=time();				
            my $file_age = (($current_timeseconds - $file_info[9]) / 86400);         
            $file_age = sprintf("%.2f", $file_age);            
            if ($file_age < 1) {
				LogMsg(indent(1)."Reading antivirus log $file...");				
                # See what else we can get from this file if it was generated in the last day
                open(INPUT,"${antivirus_log}/${file}");
                my @file_info=(<INPUT>);
                close INPUT;	
               
                foreach my $line (@file_info) {                
                    my @tmp=split(/,/,$line);                 
                    my $date_field=$tmp[0];
                    my @date_tmp=split(//,$date_field);
                    my $year_field=$date_tmp[0].$date_tmp[1];
                    my $month_field=$date_tmp[2].$date_tmp[3];
                    my $day_field=$date_tmp[4].$date_tmp[5];
                    my $hour_field=$date_tmp[6].$date_tmp[7];
                    my $minute_field=$date_tmp[8].$date_tmp[9];                
                    my $year=(1970 + hex($year_field));
                    my $month=(1 + hex($month_field));
                    my $day=hex($day_field);
                    my $hour=hex($hour_field);
                    my $minute=hex($minute_field);
                    my $date="${year}-${month}-${day}";					
                    # Get the event number
                    my $event_number=$tmp[1];
                    my $event=$event_hash{$event_number};                
                    my $cat_number=$tmp[2];
                    my $category=$category_hash{$cat_number};  
					my $suspect=$tmp[6];
                    $category_count_hash{$cat_number}++;
					if (($cat_number == 1) || ($cat_number == 4)) {
						if ($verbose) {
							print "Date: $date \nEvent: $event \nCategory: $category \n";
							print "$line\n";
						}							
						if ($cat_number == 1) {
							next if ($event_number == 46);
							next if ($event_number == 50);
							next if ($event_number == 51);
							my $msg="Date $date Event $event Category $category File $suspect";
							LogMsg(indent(1)."WARNING: $msg");
							LogMsg(indent(1)."ISSUE: $msg");
							$warns++;
						}
					}
					# Create warnings for certain events
					my @warning_event_list=("5","17","30","31","32","33","34","38","41","43","48","49","52","67","73","77");
					foreach my $w (@warning_event_list) {
						if ($event_number == $w) {
							LogMsg("WARNING: SAV Date $date Event $event Category $category ");						 
							$warns++;
						}
					}					
                }   
            }
			
		}		 
		read_most_recent($antivirus_log,$most_recent,$most_recent_file);
 		 		 
	} else {
		LogMsg("WARNING: Could not locate $antivirus_log");
		$warns++;
	}
	return $warns; 
}

sub check_awhost32_excluded {
	my $awhost32_excluded=0;
 	
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\AV\\Exclusions\\HeuristicScanning\\FileHash\" /s ";    
	my @info=`$cmd`;
	
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /ThreatName/i) {
			if ($i =~ /awhost32/i) {
				$awhost32_excluded++;
			}
		}
	}
	return $awhost32_excluded;
	 
}

sub check_pcAnywhere_host_name {
    if ($new_register_test) {
        print "Not checking pcAnywhere Host Name\n";
        return;
    }
	my $hostname_correct=0;
 	my $setting;	
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\pcAnywhere\\CurrentVersion\\System\" /s ";    
	my @info=`$cmd`;
	
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /Computer/i) {						
			my @tmp=split(/REG_SZ/,$i);			
			$setting=$tmp[-1];								
			LogMsg("pcAnywhere Host name: $setting");
		}
	}
	if ($setting =~ /$hostname/i) {
		LogMsg("pcAnywhere Host name contains $hostname");
		if ($setting =~ /$storenum/) {
			LogMsg("pcAnywhere Host name contains $storenum");
			unless (($setting =~ /\#/) || ($setting =~ /\&/)) {
				$hostname_correct=1;
			} 
		} else {
			LogMsg("WARNING: pcAnywhere Host name does not contain $storenum");
		}
	} else {
		LogMsg("WARNING: pcAnywhere Host name does not contain $hostname");
	}
	unless ($hostname_correct) {
		LogMsg(indent(1)."WARNING: Hostname for pcAnywhere is set to: $setting");
	}
	return $hostname_correct;

}
 
sub gethost {
    # Try to get the host name associated with the IP address given
    my $ip=shift;
    my @info=`NBTSTAT -A $ip`;
    my $host;
    foreach my $i (@info) {
        if ($i =~ /UNIQUE/) {
            # The host name should be on this line
            my @tmp=split(/</,$i);
            $host=$tmp[0];
            last;
        }
    }
    if ($host) {
        return $host;
    } else {
        # If we could not find the host, just return the IP address
        return $ip;
    }
}

sub check_log {
	my $log = "C:/Program Files/SAP/Retail Systems/Point of Sale/logs/posclient.log";
	my @watch_list=(
		"TLOG Data Error",
		"CreateDirectory",
		"mutex"
	);
	@watch_list=(
	 
		"CreateDirectory",
		"mutex"
	);	
	my $warns=0;
	my %error_hash=();
	if (-f $log) {
		LogMsg("Reading posclient log...");
		my $month=$month_name_hash{$previous_day_month};
		my $day=$previous_day_day;

		my $date_label="$month $day";
		open(LOG,"$log");
		my @log_info=(<LOG>);
		close LOG;		
		foreach my $l (@log_info) {
			chomp($l);			
			if ($l =~ /$date_label/i) {
				foreach my $w (@watch_list) {
					if ($l =~ /$w/i) {
						$error_hash{$w}++;						
					}
				}
			}
			
		}
		foreach my $key (sort keys(%error_hash)) {
			LogMsg(indent(1)."Found $error_hash{$key} $key issues");
			#LogMsg(indent(1)."WARNING: Found $error_hash{$key} $key issues");
			#$warns++;
		}
	}
	if ($warns) {
		LogMsg("Found $warns issues looking at posclient.log");
		#LogMsg("WARNING: Found $warns issues looking at posclient.log");
		#$warning_count++;
	}
}

sub update_action_log {
	my $check = shift;
	my $warns = shift;
	open(LOG,">>$action_log");
	if ($verbose) {
		print "Update Log:\n";
		print "$date_label - $check warns: $warns\n";
	}
	print LOG "$date_label - $check warns: $warns\n";
	close LOG;
}

sub check_enabled_service {
    my $service=shift;
    my $setting=shift;
    LogMsg("Checking status of $service");
    my $cmd="sc qc \"$service\"";     
 
    my $start_type;
    my @info=`$cmd`;
    my $result=0;
    foreach my $i (@info) {        
        if ($i =~ /START_TYPE/) {			
			if ($i =~ /AUTO_START/) {
				$result=1;             
			} 
			if (defined($setting)) {
				if ($i =~ /$setting/) {
					$result=1;             
				}  			
			}
        }  
    }        
    return $result;
}


sub unpack_sap_pos {
	if (-d "c:/sybtools_95") {
		# This is only for stores not yet upgraded to SAP POS 2.3
		LogMsg("Appears to have already been upgraded to SAP POS 2.3 - Not unpacking SAP POS");
		return 0;	
	}
	if (-d "C:/Program Files/SAP/Retail Systems/Point of Sale") {
		# Second check to see if the system has been upgraded to SAP POS
		return 0;
	}	
	my $md5="md5";
    # Check to see if the SAP_POS files have been unpacked.  Unpack them if need be.
    # Return 0 will be considered success and Return 1 will be considered an error
    my $initial_dir="//edna/temp/install/SAP_POS_2";
    my $final_dir="c:/temp/install/SAP_POS_2.3";
    my $customization_dir="${final_dir}/customization";
    my $unpacked_flag="c:/temp/unpack_complete.flg";
    my $unpack_failed_flag="c:/temp/unpack_failed.flg";
	my %checksum_hash;
	my @unpack_list;
	# Some basic checks before going any further
    if (-f $unpack_failed_flag) {
        #LogMsg("WARNING: Previous unpack of SAP POS install files has failed.  Not trying again.");
		LogMsg("NOTICE: Previous unpack of SAP POS install files has failed.  trying again.");
        #return 1;
    }    
    unless (-d $initial_dir) {
        LogMsg("WARNING: Failed to locate $initial_dir");        
        return 1;
    }
    unless (-d $final_dir) {
        LogMsg("Making $final_dir...");
        mkdir($final_dir);
        unless (-d $final_dir) {
            LogMsg("ERROR: Failed to create $final_dir");
            return 1;
        }
    }	
    unless (-d $customization_dir) {
        LogMsg("Making $customization_dir...");
        mkdir($customization_dir);
        unless (-d $customization_dir) {
            LogMsg("ERROR: Failed to create $customization_dir");
            return 1;
        }
    }    	
	# Get a list of the current install zips    
    opendir(SOURCE,"$initial_dir");
    my @dir_list=readdir SOURCE;
    closedir SOURCE;    
	my @source_list;
    my $zip_count=0;
    my $unzip_count=0;
 
    foreach my $d (@dir_list) {
        next if ($d eq ".");
        next if ($d eq "..");
        next unless ($d =~ /zip$/i);  
		LogMsg(indent(1)."Found $d in $initial_dir");
        $zip_count++;	
		push(@source_list,$d);
    }
    
    unless ($zip_count > 4) {
		# the zip count is the total count of the zip files located
        LogMsg("Not unzipping source files.  Expected at least 5 but found only $zip_count");
        return 0;
    }	
 	 
	if (-d $initial_dir) {
		opendir(INITIAL,"$initial_dir");
		my @dir_list=readdir INITIAL;
		closedir INITIAL;  		
		LogMsg(indent(1)."Getting Zip File Info...");		 		
		foreach my $f (@dir_list) {
			next if ($f eq ".");
			next if ($f eq "..");
			next unless ($f =~ /zip$/i);
			my $full_path_file="${initial_dir}/${f}";
			my @file_info=stat($full_path_file);
			my $file_date=$file_info[9];
			my $file_size=$file_info[7];
			my $file_data = "$file_date $file_size";
			# Using file size & date rather than a checksum to identify the file			 
			$checksum_hash{$f}=$file_data;			 
		}
	} else {
		LogMsg("WARNING: Failed to locate $initial_dir");
		return 1;
	}	
	copy_install_misc();
 
	my @unpacked_info;
    if (-f $unpacked_flag) {
        LogMsg("Found $unpacked_flag");
		my $warns=check_sap_pos_install();
		if ($warns) {
			LogMsg("Found $warns issues checking SAP POS Install files");
			#return $warns;
		}
		# Open the unpacked_flag file and check the sums to make certain that there are no new files to unpack
		open(PACK,"$unpacked_flag");
		@unpacked_info=(<PACK>);
		close PACK;	

    } else {
		LogMsg("No $unpacked_flag found");
	}
	# Create the list of files to unpack
	foreach my $s (@source_list) {
		chomp($s);
		my $found=0;
		foreach my $u (@unpacked_info) {
			chomp($u);
			my @tmp=split(/\s+/,$u);			
			my $sum="$tmp[0] $tmp[1]";
			my $name=$tmp[2];
			if ($s eq $name) {					
				if ($checksum_hash{$name} eq "$sum") {
					# If it is on the list of unpacked files and the sum is the same, consider it found
					LogMsg(indent(1)."$name is already unpacked");
					$found=1;
				}						
			}						
		}	  
		unless ($found) {					
			# Need to unpack this zip file.
			LogMsg(indent(1)."$s needs to be unpacked");
			push(@unpack_list,$s);				
		}
	}	
	my @success_list;
    foreach my $zip (@unpack_list) {
        my $source_zip="${initial_dir}/${zip}";        
        my $target_dir=$final_dir;
        if ($zip =~ /customization/i) {
            # If the zip has customization in the name, it goes in the customization folder
            $target_dir=$customization_dir;            
        }
        my $destination_zip="${target_dir}/${zip}";  
        if (-f $source_zip) {
            # Copy the zip file to the local install folder
            if (-f $destination_zip) {
                # Just in case it is corrupted.
                LogMsg(indent(1)."Removing existing copy of $destination_zip...");
                unlink $destination_zip;
            }
            LogMsg(indent(1)."Copying $source_zip to $destination_zip...");
            copy("$source_zip","$destination_zip");
            if (-f $destination_zip) {                
                unless (un7zip($target_dir,$zip)) {
                    LogMsg(indent(1)."WARNING: Failed to unzip $zip file");
                    set_flag($unpack_failed_flag);
                    return 1;					
                } else {
                    LogMsg(indent(1)."Successfully unzipped $zip file");
                    $unzip_count++;
                    LogMsg(indent(2)."Unpack count is $unzip_count");
					push(@success_list,$zip);
                }
            } else {
                LogMsg(indent(1)."WARNING: Failed to copy $source_zip to $destination_zip");
                set_flag($unpack_failed_flag);
                return 1;
            }
        } else {
            LogMsg(indent(1)."WARNING: Failed to locate $source_zip");
            set_flag($unpack_failed_flag);
            return 1;
        } 
    }
    if ($unzip_count) {
		if ($unpack_failed_flag) {
			unlink $unpack_failed_flag;
		}
		# Record the files unpacked so far.
        LogMsg(indent(1)."Setting $unpacked_flag");        
		open(FLAG,">$unpacked_flag");
		foreach my $name (sort keys(%checksum_hash)) {
			print FLAG "$checksum_hash{$name} $name\n";
		}
		close FLAG;
        return 0;
    }
}

sub copy_install_misc {
	LogMsg("Copying misc install files for SAP POS...");
	my $warns=0;
	# Copy the install and the manifest files
    my $source_file="//edna/temp/install/SAP_POS_2.3/pos_upgrade.pl";
    my $target_file="c:/temp/install/SAP_POS_2.3/pos_upgrade.pl";
    my $source_manifest_file="//edna/temp/install/SAP_POS_2.3/sap_pos_manifest.txt";
    my $target_manifest_file="c:/temp/install/SAP_POS_2.3/sap_pos_manifest.txt";	
 
	if (-f $source_file) { 
		LogMsg(indent(1)."Found $source_file");
		my @file_info=stat($source_file);
		my $file_date=$file_info[9];
		my $file_size=$file_info[7];
		my $source_file_data = "$file_date $file_size";	
		my $target_file_data = 0;
		if (-f $target_file) {
			@file_info=stat($target_file);
			$file_date=$file_info[9];
			$file_size=$file_info[7];
			$target_file_data = "$file_date $file_size";				
		}
		if ($target_file_data eq $source_file_data) {
			LogMsg("Target: $target_file_data matches Source: $source_file_data");
		} else {
			# Copy the file
			LogMsg("Copy $source_file to $target_file");
			copy($source_file,$target_file); 
		}
	} else {
		LogMsg("Error: Failed to locate $source_file");
		$warns++;
	}
	if (-f $source_manifest_file) { 
		LogMsg(indent(1)."Found $source_manifest_file");
		my @file_info=stat($source_manifest_file);
		my $file_date=$file_info[9];
		my $file_size=$file_info[7];
		my $source_file_data = "$file_date $file_size";	
		my $target_file_data = 0;
		if (-f $target_manifest_file) {
			@file_info=stat($target_manifest_file);
			$file_date=$file_info[9];
			$file_size=$file_info[7];
			$target_file_data = "$file_date $file_size";				
		}
		if ($target_file_data eq $source_file_data) {
			LogMsg("Target: $target_file_data matches Source: $source_file_data");
		} else {
			# Copy the file
			LogMsg("Copy $source_manifest_file to $target_manifest_file");
			copy($source_manifest_file,$target_manifest_file); 
		}
	} else {
		LogMsg("ERROR: Failed to locate $source_manifest_file");
		$warns++;
	}
	return $warns;
}

sub set_flag {
    my $flag=shift;
    open(FLAG,">$flag");
    close FLAG;
}

sub un7zip {
	my $dir=shift;
	my $file=shift;
	my $sourcefile="${dir}/${file}";
    my $tool="\"C:/Program Files/7-Zip/7z.exe\"";
    $tool="C:/Progra~1/7-Zip/7z.exe";
    unless (-f $tool) {
        LogMsg("ERROR: Failed to locate $tool");
        return 0;
    }
    my $result=0;
	if (-f $sourcefile) {
		LogMsg("Extracting $sourcefile...");
 
		# Need to unzip the source file
		chdir($dir);
        if (-f $file) {
            # Using 7-zip to extract the files and directories  (x maintains the directory structure)
            my $cmd="$tool x $file -y";
            my @results=`$cmd`;
            foreach my $r (@results) {
                chomp($r);
                LogMsg(indent(2)."$r");
                if ($r =~ /Everything is Ok/i) {
                    $result=1;
                }
            }
        } else {
            LogMsg(indent(1)."WARNING: Failed to locate $file");
            return 0;        
        }
		 
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $sourcefile");
		return 0;
	}
    #if ($result) {
        # remove the zip file whether the unzip was successful or not
        unlink $sourcefile;
    #} 
    return $result; 	
}


sub check_sap_pos_install {
	if (-d "c:/sybtools_95") {	
		# Check to see if the system has been upgraded to SAP POS
		return 0;
	}
	if (-d "C:/Program Files/SAP/Retail Systems/Point of Sale") {
		# Second check to see if the system has been upgraded to SAP POS
		return 0;
	}
	my $warns=0;
    my $final_dir="c:/temp/install/SAP_POS_2.3";	
    opendir(FINAL,"$final_dir");
    my @dir_list=readdir FINAL;
    closedir FINAL;   
    my $dir_count=0;
    my $file_count=0;
	my $dir_count_expected=8;
	my $file_count_expected=5;
    foreach my $d (@dir_list) {
        next if ($d eq ".");
        next if ($d eq "..");
        my $fullpath="${final_dir}/${d}";
        if (-d $fullpath) {
            LogMsg(indent(1)."Found $fullpath");
            $dir_count++;
        }
        if (-f $fullpath) {
            LogMsg(indent(1)."Found $fullpath");
            $file_count++;
        }       		
	}
	LogMsg("Dir Count: $dir_count");
	LogMsg("File Count: $file_count");
	unless ($dir_count == $dir_count_expected) {
		LogMsg("WARNING: SAP POS 2.3 shows  $dir_count directories and expected $dir_count_expected");
		$warns++;
	}
	unless ($file_count >= $file_count_expected) {
		LogMsg("WARNING: SAP POS 2.3 shows  $file_count files and expected $file_count_expected");
		$warns++;
	}	
	return $warns;
	
}


sub check_arf { 
    if ($new_register_test) {
        print "Not checking ARF file\n";
        return;
    }
	
	unless ($day_of_week == 0) {
		# Due to resource constraints, this is being checked only once a week.
		return;
	}
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
	my $freq=1;
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="ARF";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400)); 
		unless ((($timeseconds - $last_pass)/86400) > $freq) {			
			unless ($force) {
				# We do not have to run this test again 
				LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
				return;
			}
		} 
	}
	my $dir="C:/Program Files/SAP/Retail Systems/Point of Sale/sdata";
    my $file="C:/Program Files/SAP/Retail Systems/Point of Sale/sdata/state.arf";
	my $source_file="//Edna/temp/install/register_utilities/state.arf";
	#my $alt_file="C:/POSWIN/sdata/state.arf";	# This was a provision to support Triversity 9.5 but I decided
												# to just correct them after they were upgraded - kdg
	unless (-f $file) {
		LogMsg(indent(1)."WARNING: The $file file was not found.  Need to run Mgr Code 26 to set it."); 
		$warning_count++;	
		return;
	}
	# Unfortunately, this does not see to be reliable.  It is all currently disabled - 2014-05-14 - kdg
	 
		if (0) {
		unless (-f $file) {	
			if (-d $dir) {
				# If the file is not present, copy the one in from Edna
				if (-f $source_file) {
					LogMsg(indent(1)."Copying in from $source_file...");
					copy($source_file,$file);
					unless (-f $file) {
						LogMsg(indent(1)."WARNING: The $file file was not found."); 
						$warning_count++;				
					}
				} else {
					LogMsg(indent(1)."WARNING: The $file file was not found.");
					LogMsg(indent(1)."WARNING: The $source_file file was not found.");
					$warning_count++;
				}
			}
			return;
			#if (-f $alt_file) {
			#	$file=$alt_file;
			#}
		}
	}
    my $current_customer_number='';
	LogMsg("Checking STATE.ARF file...");
    if (-f $file) {             
        open (ARF,"$file");
        binmode ARF; 
        my $buff;
        my $ttl = 0;
        my $i = 0;
        my $size = 1;   
        my $offset=0;
         
        my $start=42;
        my $end=45;
     
        my $end_found=0;
        my $read=0;        
        while(my $len = read(ARF, $buff, $size)){  
        ###
            my $skip=0;
            $ttl += $len;
            $offset = ($i * $size);
            my $value=join(' ', map {unpack('H*', $_)} split //, $buff);                         
                if  ($start == $offset) {                     
                    #print "Offset is $offset - start is $start\n";                     
                    $read=1;
                }
                if  ($end == $offset) {
                    $end_found=1;
                    #print "Offset is $offset - end is $end\n";                   
                }  
                if ($offset == ($end + 1)) {            
                    $read=0;
                }
                if ($read) {
					if ($value) {
						# The value is in hex so convert it to decimal
						my $dec=hex($value);
						if ($dec) {
							# This decimal is the ascii value so get the character this represents
							my $number=chr("$dec");	
							if ($number =~ /\d/) {
								$current_customer_number.=$number;	
							}
							LogMsg("Offset: $offset - Number: $number - CCN: $current_customer_number");							
						}
					}
                }                
            ++$i;
            if ($end_found) {
                $read=0;            
                $end_found=0;
            }        
        ###
        }
        close ARF;
    } else {
        LogMsg("Error: Failed to locate $file");
		$warns++;
    }
    if ($current_customer_number) {		
		$current_customer_number=sprintf("%04d", $current_customer_number);		
        LogMsg("The current customer number is $current_customer_number.");

		if ($customer_number) {
			my $set_to = ($customer_number + 1);
			$customer_number=~ s/ //g;	# Trim out any spaces.
			if ($customer_number eq $current_customer_number) {
				LogMsg(indent(1)."The expected customer number is $customer_number");
			} else {
				# Determine if we can update the state.arf or not.  Assuming that if the hour is before 8
				# then it is safe to kill the POS, update the arf and restart the pos.  Otherwise, just log a warning.				
				
				# This is not reliable and has been disabled - 2014-07-23 - kdg
				if (0)  {	
					if (($current_hour < 8) || ($arf_only)) {
						# Update the arf to the correct number
						LogMsg("Hour is $current_hour - Updating ARF...");
						$warns+=update_arf($customer_number);
					} else {
						LogMsg("WARNING: The current customer number is $current_customer_number but expected $customer_number.  Run MC 26 and set autogen #3 to $set_to.");
						$warns++;
					}
				} else {
					my $diff = ($current_customer_number - $customer_number);
					# If the current setting is less than what it should be, the system will have problems.
					# If the current setting is significantly more than what it should be, we might as well correct it.
					if (($current_customer_number < $customer_number) || ($diff > 3)) {
						LogMsg("WARNING: The current customer number is $current_customer_number but expected $customer_number.  Run MC 26 and set autogen #3 to $set_to.");
						$warns++;				
					}
				}
			}
		} else {
			LogMsg("The expected number is unknown at this point.");
		}
    } else {
        LogMsg("Failed to get the current customer number");
    }
	update_action_log($check,$warns);	
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking Current Customer Number");
		$warning_count++;
    } else {
		$monitor_record_hash{$check}=$timeseconds;
	}			
}



sub update_arf {
    #return 1;   # Currently this is now working.  It does not seem to update the ARF file correctly. - kdg 2014-03-07
    # Here is where we update the state.arf file with the current last customer number
    my $current_customer_number=shift;
    my $warns=0;
    LogMsg("Update current customer number in state.arf to $current_customer_number");
	# Stop the POS
	my $pid=checkPID("posw.exe");
	if ($pid) {
		LogMsg("Killing posw.exe");
		LogMsg("PID is $pid");
		`pskill $accepteula $pid`;
		sleep 1;
		$pid=checkPID("posw.exe");
		if ($pid) {
			LogMsg("ERROR: Failed to stop posw.exe");
			return 1;
		} else {
		   LogMsg("Stopped posw.exe");
		}
	} else {
		LogMsg("Failed to determine PID for posw.exe - assume not running");
		#return 1;
	}	
    my $file="C:/Program Files/SAP/Retail Systems/Point of Sale/sdata/state.arf";
    my $orig="C:/Program Files/SAP/Retail Systems/Point of Sale/sdata/state.bak";
    my $new_file="C:/Program Files/SAP/Retail Systems/Point of Sale/sdata/state.new";    
	my $alt_file="C:/POSWIN/sdata/state.arf";
    my $alt_orig="C:/POSWIN/sdata/state.bak";
    my $alt_new="C:/POSWIN/sdata/state.new";    
	unless (-f $file) {
		if (-f $alt_file) {
			$file=$alt_file;
            $orig=$alt_orig;
            $new_file=$alt_new;
		}
	}
    unless (-f $file) { 
        LogMsg("ERROR: Failed to locate state.arf");       
        return 1;    
    }
    # We will create a copy of the original file and call it orig.  We then read this orig file and write it to the new file making the necessary changes along the way
    # After we have finished.  Overwrite the file with the new file.
    
    if (-f $orig) {
        # If this file already exists, we need to remove it so that we know what we are dealing with.
        unlink $orig;
        if (-f $orig) {
            LogMsg("ERROR: Failed to remove existing $orig");            
            return 1;
        }
    }
    unless (-f $orig) {
        LogMsg(indent(1)."Copy $file to $orig...");
        copy ($file,$orig);
    }
    unless (-f $orig) {
        LogMsg("ERROR: Failed to create $orig");        
        return 1;
    }
      
    open (INPUT,"$orig");
    binmode INPUT;  
    if (tell(INPUT) == 01) {
        LogMsg("ERROR: Failed to open $orig");
        return 1;
    }
    open (OUTPUT,">$new_file");    
    binmode OUTPUT;   
    if (tell(OUTPUT) == 01) {
        LogMsg("ERROR: Failed to open $new_file");
        return 1;
    }
    my $buff;
    my $ttl = 0;
    my $i = 0;
    my $size = 1;   
    my $offset=0;         
    my $start=42;
    my $end=45;     
    my $end_found=0;
    my $overwrite_enabled=0;  
    
    # Take the number and put it in the correct format
    my %input_hash;
    my @tmp=split(//,$current_customer_number);
    for (my $x=0; $x<=$#tmp; $x++) {
        my $digit=$tmp[$x];
		LogMsg("Current Customer Number: $current_customer_number - Split(@tmp) - digit: $digit");
        # Take this digit and convert it to the ascii value
        my $ascii=ord($digit);
        # Take the ascii and convert it to hex
        my $hex=sprintf("%x",$ascii);
		
        my $index=($start + $x);
		LogMsg("Index: $index - Digit: $digit - ASCII: $ascii - Hex: $hex");
        $input_hash{$index}=$hex;
    }
    #
    while(my $len = read(INPUT, $buff, $size)){
        my $skip=0;
        $ttl += $len;
        $offset = ($i * $size);
        my $value=join(' ', map {unpack('H*', $_)} split //, $buff);        
                 
        if  ($start == $offset) {                 
            LogMsg("Offset is $offset - start is $start");                 
            $overwrite_enabled=1;
        }
        if  ($end == $offset) {
            $end_found=1;
            LogMsg("Offset is $offset - end is $end");
           
        }  
        if ($offset == ($end + 1)) {            
            $overwrite_enabled=0;
        }
 

        if ($overwrite_enabled) {  
            LogMsg("Printing offset $offset $input_hash{$offset}");
            # Take that hex value out of the input_hash and pack it into the new buff and print it to the file
            my $new_buff = pack('H*', $input_hash{$offset});
            print OUTPUT $new_buff;
        } else {
            # If this is an offset that we are not changing/overwriting, just print what we have read in
            print OUTPUT $buff;
        }
 
 
        ++$i;
        if ($end_found) {
            $overwrite_enabled=0;            
            $end_found=0;
        }
    }
    close INPUT;
    close OUTPUT;    
    # Copy the new file over the original
    if (copy($new_file,$file)) {
        LogMsg("Copied $new_file to $file");
    } else {
        LogMsg("ERROR: Failed to copy $new_file to $file");
        return 1;
    }
	# Safest to just reboot the system
	LogMsg("Setting flag to reboot");
	$need_to_reboot=1;

    return 0;
}

sub reboot {
	LogMsg("rebooting here...");
    my $command="\"shutdown -r -f -t 5\"";                    
	system("$command");
}

sub exit_pos {
	LogMsg("Exiting POS application...");
	# Stop the POS
	my $instruction=shift;
	unless (defined($instruction)) {
		$instruction=0;
	}
	my $pid=checkPID("posw.exe");

	my $application="c:/Documents and Settings/Villages/Start Menu/Programs/Startup/Point of Sale.lnk";
	my $alt_app="c:/Documents and Settings/Villages/Start Menu/Programs/Startup/POS w EFT.lnk";
	if (-f $alt_app) {
		$application = $alt_app;
	}
	unless (-f $application) {
		LogMsg("ERROR: Failed to locate $application");
		return 1;
	}
	if ($pid) {
		LogMsg("Killing posw.exe");
		LogMsg("PID is $pid");
		`pskill $accepteula $pid`;
		sleep 1;
		$pid=checkPID("posw.exe");
		# Try again
		`pskill $accepteula $pid`;
		sleep 5;

		$pid=checkPID("posw.exe");
		if ($pid) {
			# Sleep again
			sleep 5;
			$pid=checkPID("posw.exe");			
		}
		if ($pid) {
			LogMsg("ERROR: Failed to stop posw.exe");
			return 1;
		} else {
			LogMsg("Stopped posw.exe");
			reset_eft_log();
		}
	} else {
		LogMsg("Failed to determine PID for posw.exe ");
		return 1;
	}	
	# Now make certain that the EFTDeviceDriver has also been stopped
	my $EFTpid=checkPID("EFTDeviceDriver.exe");	
	if ($EFTpid) {
		LogMsg("Killing EFTDeviceDriver.exe");
		LogMsg("PID is $EFTpid");
		`pskill $accepteula $EFTpid`;
		sleep 1;
		$EFTpid=checkPID("EFTDeviceDriver.exe");
		# Try again
		`pskill $accepteula $EFTpid`;
		sleep 5;

		$EFTpid=checkPID("EFTDeviceDriver.exe");
		if ($EFTpid) {
			# Sleep again
			sleep 5;
			$EFTpid=checkPID("EFTDeviceDriver.exe");			
		}
		if ($EFTpid) {
			LogMsg("ERROR: Failed to stop EFTDeviceDriver.exe");
			return 1;
		} else {
			LogMsg("Stopped EFTDeviceDriver.exe");
			reset_eft_log();
		}
	} else {
		LogMsg("Did not determine a PID for EFTDeviceDriver.exe ");	 
	}		
	if ($instruction eq "restart") {
		open(FILE,">$pos_instruction_file");
		print FILE "RESTART POS\n";
		close FILE;
	}
	LogMsg("Finished Exiting POS application...");
	return 0;
}

 

sub run_benchmarks {
	if ($new_register_test) {
        print "The Benchmarks will not be run\n";
        return;
    }
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
 
	my $freq=30;
 
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="benchmark";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}
	if (($current_hour < 8) || ($force)) {	
		LogMsg("Running Benchmarks...");
		run_cpu_benchmark();
		run_hd_benchmark();
		update_action_log($check,$warns);	
		if ($warns) {
			LogMsg(indent(1)."WARNING: $warns benchmark issues found");          
			$warning_count++;
		} else {
			$monitor_record_hash{$check}=$timeseconds;
		}		
	}
}

sub run_cpu_benchmark {
	my $counter=0;
	my $limit = 50000;
	my $starttimeseconds=time();
	
	while ($counter < $limit)	{
		$counter++;
		for (my $x=0; $x<=1000; $x++) {
			my $tmp=sin($x/($x+2));
		}		
	}
	my $endtimeseconds=time();
	my $diff=($endtimeseconds - $starttimeseconds);
	LogMsg("System Info: CPU_Benchmark: $diff");
 
}

sub run_hd_benchmark {
	my $counter=0;
	my $limit = 1000;
	my $starttimeseconds=time();
	my $file="c:/temp/hd_benchmark.txt";
	if (-f $file) {
		unlink $file;
	}
	my $cmd="echo \"1234567890\" >> $file";
	
	while ($counter < $limit)	{
		`$cmd`;
		$counter++;
	}
	if (-f $file) {
		unlink $file;
	}	
	my $endtimeseconds=time();
	my $diff=($endtimeseconds - $starttimeseconds);	 
	LogMsg("System Info: HD_Benchmark: $diff");	
}

sub cpu_benchmark {
    # Very crude and simple CPU benchmarking tool
    use Benchmark;

    # run code 100000 times and display result 
    timethis(100000, '
          for ($x=0; $x<=200; $x++)
          {
                sin($x/($x+2));
          }
    ');
	
}   

sub check_user {
	# This script should only be run by Administrators
	my @set_info=`set`;
	my $user;
	foreach my $s (@set_info) {
		if ($s =~ /^USERNAME/) {
			chomp($s);			 
			my @tmp=split(/=/,$s);
			$user=$tmp[1];
		}
	}
 
	unless (($user =~ /Administrator/i) || ($user =~ /POS Support/) || ($as_admin)) {
		LogMsg("Sorry, this script should only be run by the Administrator");
		exit;
	} 	
}

sub check_web_access {
	my $warns=0;

	if ($revised_network) {
		LogMsg("Checking web access...");
		LogMsg("Hour: $current_hour");		
	
		# A revised network should be able to access Symantec 24 hrs a day.  These
		# stores are not on a time clock.
		LogMsg("This store is on the revised network scheme which blocks web access.");
		# Check that it can still get antivirus updates
		LogMsg("Checking connectivity to Symantec");
		system("ping liveupdate.symantecliveupdate.com");
		if ($?) {
			system("ping liveupdate.symantecliveupdate.com");
			if ($?) {
				LogMsg(indent(1)."Cannot ping Symantec");			 
				$warns++;
			} else {
				LogMsg("Connectivity to Symantec verified");
			 
			}
		} else {
			LogMsg("Connectivity to Symantec verified");			 
		}
 
		my $found_url=0;
		use LWP::Simple;
		my $URL="http://windowsupdate.microsoft.com";
		LogMsg(indent(1)."Checking access to $URL");
		my $content=get($URL);
		unless ($content) {
			LogMsg(indent(2)."No content - try again...");
			# If we got nothing try again
			sleep 1;
			$content=get($URL);
			unless ($content) {
				# If we got nothing try again
				LogMsg(indent(2)."No content - try one more time...");
				sleep 5;
				$content=get($URL);
			}				
		}

		if ($content) {	
			my @content_info=split(/HREF/i,$content);
			foreach my $c (@content_info) { 		 
				if ($c =~ /WindowsUpdate/) {
					$found_url=1;
					LogMsg(indent(1)."Found Windows Update");
				}
			}
		}
		if ($found_url) {
			LogMsg(indent(1)."Verified Web Access.");
		} else {
			LogMsg("WARNING: Failed web access to Windows Updates CWA1");
			$warns++;
		}
		
	}
	# Freshen the access to gift registry manager
	# The cookie keeps the login alive for 24 hours after last activity.  
	# I am hoping that this access counts as "activity" and keeps the login alive. -kdg
	use LWP::Simple;
	my $URL="http://www.tenthousandvillages.com/grmanage";					
	my $content=get($URL);
	LogMsg("Connecting to grmanage...");
	if ($content) {
		LogMsg(indent(1)."Connected");
	} else {
		LogMsg(indent(1)."Trying again...");
		# If we got nothing try once more
		sleep 1;
		$content=get($URL);
	}	
	
	my $freq=30;
 
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="webaccess";

	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}

	# The SonicWall cuts off web access from 8:00am to 10:00pm but the register should be able to get out otherwise.

	unless ($revised_network) { 
		LogMsg("Checking web access...");
		LogMsg("Hour: $current_hour");		
		if (($current_hour < 8) || ($force)) {
			my $found_google=0;
			use LWP::Simple;
			$URL="http://google.com";
			LogMsg(indent(1)."Checking access to $URL");
			my $content=get($URL);
			unless ($content) {
				LogMsg(indent(2)."No content - try again...");
				# If we got nothing try again
				sleep 1;
				$content=get($URL);
				unless ($content) {
					# If we got nothing try again
					LogMsg(indent(2)."No content - try one more time...");
					sleep 5;
					$content=get($URL);
				}				
			}

			if ($content) {	
				my @content_info=split(/HREF/i,$content);
				foreach my $c (@content_info) { 				
					if ($c =~ /Google Search/) {
						$found_google=1;
						
					}
				}
			}
			if ($found_google) {
				LogMsg(indent(1)."Found Google Search");
				LogMsg("Verified Web Access.");
			} else {
				LogMsg("WARNING: Failed web access check");
				$warns++;
			}
			# All registers should have access to windows updates
			my $found_url=0;
			use LWP::Simple;
			$URL="http://windowsupdate.microsoft.com";
			LogMsg(indent(1)."Checking access to $URL");
			$content=get($URL);
			unless ($content) {
				LogMsg(indent(2)."No content - try again...");
				# If we got nothing try again
				sleep 1;
				$content=get($URL);
				unless ($content) {
					# If we got nothing try again
					LogMsg(indent(2)."No content - try one more time...");
					sleep 5;
					$content=get($URL);
				}				
			}

			if ($content) {	
				my @content_info=split(/HREF/i,$content);
				foreach my $c (@content_info) { 		 
					if ($c =~ /WindowsUpdate/) {
						$found_url=1;
						LogMsg(indent(1)."Found Windows Update");
					}
				}
			}
			if ($found_url) {
				LogMsg(indent(1)."Verified Web Access.");
			} else {
				LogMsg("WARNING: Failed web access to Windows Updates CWA2");
				$warns++;
			}			
		} else {
			LogMsg("No web access expected after 8:00am");
		}
	}
 
 
	
    if ($warns) {
        LogMsg(indent(1)."WARNING: $warns web access issues found");          
        $warning_count++;
    } else {
		$monitor_record_hash{$check}=$timeseconds;
	}		
}

sub check_timezone {
	# Get the TimeZone
	my $cmd = "wmic timezone get description";
	my $warns=0;
	my @tzinfo=`$cmd`;
	foreach my $t (@tzinfo) {
		chomp($t);
		if ($t =~ /GMT/) {
			my $found_timezone=$t;
			if ($timezone) {
				if ($t =~ /$timezone/i) {
					LogMsg(indent(1)."Timezone is $found_timezone and expected $timezone.");
				} else {
					LogMsg(indent(1)."WARNING: Timezone is $found_timezone but expected $timezone.");
					$warns++;
				}
			} else {
				LogMsg(indent(1)."Failed to determine expected timezone.  Found $found_timezone");
			}
		}
	}
	return $warns;
}

sub sync_time {
	# Not certain this is the way to do it with Win7
	if ($OS eq "Win7") {
		return 0;
	}
	# Not so much check time but set time
	# Note:  This is a best effort.  There is no checking
	# whether it was successful or not.
	
	LogMsg("Sync time with time server...");
	my $cmd = "w32tm /resync";
	my @response=`$cmd`;
	foreach my $r (@response) {
		LogMsg("$r");
	}
	return 0
}

sub check_dns {
	# First check that this register has connectivity
	# If we can ping google, then the DNS configuration must be correct.
	LogMsg("Checking DNS");
	system("ping google.com");
	if ($?) {
		system("ping google.com");
		if ($?) {
			LogMsg(indent(1)."Cannot ping Google");			 
		} else {
			LogMsg("Connectivity to Internet verified");
			return 0;
		}
	} else {
		LogMsg("Connectivity to Internet verified");
		return 0;
	}	
	system("ping edna");
	if ($?) {
		system("ping edna");
		if ($?) {
			LogMsg(indent(1)."Cannot ping Edna server ");
			return;
		}
	}	
	# Can this register contact its dns server?
	my $cmd="ipconfig /all";
	my @info=`$cmd`;
	my $dns1;
	my $dns2;
	my $alt_dns="8.8.8.8";
	my $ethernet=0;
	my $found_dns_servers=0;
	foreach (my $x=0; $x<=$#info; $x++)  {
		my $line=$info[$x];
		my $line1=$info[($x +1)];
		chomp($line);
		chop($line);
		if (defined($line1)) {
			chomp($line1);
			chop($line1);		
		}
		if ($line =~ /DNS Servers/i) {			
			$found_dns_servers=1;
			my @tmp=split(/:/,$line);
			$dns1 = $tmp[-1];
			$dns1 =~ s/^ //g;
			 
			# The second DNS server should be on the next line
			$dns2=$line1;
			while ($dns2 =~ /^ /) {
				$dns2 =~ s/^ //g;
			}
			
			LogMsg("First DNS Server: $dns1");
			LogMsg("Second DNS Server: $dns2");
		}
		if ($line =~ /Ethernet adapter/) {
			
			my @tmp=split(/:/,$line);
			$ethernet=$tmp[0];
			@tmp=split(/\s+/,$ethernet);
			$ethernet='';
			my $sep='';
			for (my $x=2; $x<=$#tmp; $x++) {
				$ethernet.="$sep";
				$ethernet.="$tmp[$x]";
				$sep=' ';
			}
			
		}
		if ($found_dns_servers) {
			if ($line =~ /$alt_dns/) {
				LogMsg(indent(1)."WARNING: Alternate DNS server $alt_dns is already configured");
				LogMsg(indent(1)."WARNING: Cannot connect to Internet");
				return 1;
			}
		}
		
	}
	if ($ethernet) {
		my $ping_dns=0;
		if ($dns1) {
			LogMsg("Checking connectivity to $dns1");
			system("ping $dns1");
			if ($?) {
				system("ping $dns1");
				if ($?) {
					LogMsg(indent(1)."Cannot ping dns server $dns1");
				}
			} else {
				LogMsg(indent(1)."Successful ping to dns server $dns1");
				#return 0;
				$ping_dns=1;
			}
		}
		if ($dns2) {
			LogMsg("Checking connectivity to $dns2");
			system("ping $dns2");
			if ($?) {
				system("ping $dns2");
				if ($?) {
					LogMsg(indent(1)."Cannot ping dns server $dns2");
					$ping_dns=0;
				}
			} else {
				LogMsg(indent(1)."Successful ping to dns server $dns2");
				#return 0;			
			}
		}
		# If we can ping each DNS server, see if we can ping Google again
		if ($ping_dns) {
			LogMsg("Checking connectivity to Google again...");
			system("ping google.com");
			if ($?) {
				system("ping google.com");
				if ($?) {
					LogMsg(indent(1)."Cannot ping Google");			 
				} else {
					LogMsg("Connectivity to Internet verified");
					return 0;
				}
			} else {
				LogMsg("Connectivity to Internet verified");
				return 0;
			}		
		}
		# If we reached this point, we can connect to Edna but neither dns server. 
		# If the system is not using the alternate dns server yet, see if it can be contacted.
		# If it can, use it.
		unless (($alt_dns eq "$dns1") || ($alt_dns eq "$dns2")) {
			system("ping $alt_dns");
			if ($?) {
				system("ping $alt_dns");
				if ($?) {
					LogMsg(indent(1)."Cannot ping dns server $alt_dns");
				}
			} else {
				LogMsg(indent(1)."Successful ping to dns server $alt_dns");
				$cmd="netsh interface ip add dns \"$ethernet\" $alt_dns";
				LogMsg(indent(1)."Running: ($cmd)");
				my @results=`$cmd`;		
				if ($results[0] =~ /Ok/) {
					LogMsg(indent(1)."Result: Success");
					LogMsg(indent(1)."WARNING: Added $alt_dns as an alternate DNS server");
				} else {
					LogMsg(indent(1)."WARNING: Cannot contact any DNS servers & failed to set alternate");
					LogMsg(indent(1)."WARNING: Result: ($results[0])");
					return 1;
				}
				LogMsg("Finished setting DNS");				
			}
		}
	} else {
		LogMsg("WARNING: Failed to determine ethernet interface in check_dns function.");
		return 1;
	}
}

sub check_specific_files {
	LogMsg("Checking for specific unwanted files.");
	my @specific_file_list=("FrameworkServiceLog.exe");
	chdir("/");
	my $warns=0;
	foreach my $file (@specific_file_list) {
		if ($verbose) {
			LogMsg(indent(1)."Looking for $file");
		}
		my $cmd="dir /s /b $file";
		my @results=`$cmd`;
		foreach my $r (@results) {
			chomp($r);
			$warns++;
			LogMsg(indent(2)."WARNING: $r");			
		}
	}
	return $warns;
}

sub check_shadowstorage {	
	unless ($OS eq "Win7") {
		return;
	} 
	LogMsg("Checking ShadowStorage");
	# If the system has not had backups configured, shadow storage will not show anything
	my $cmd = "Vssadmin List ShadowStorage";
	my @result=`$cmd`;
	
	foreach my $r (@result) {
		chomp($r);
		if ($r =~ /No items found/i) {
			LogMsg("WARNING: System Backups are not configured");
			$warning_count++;	
		}
	}	
}

sub disable_privoxy {
	LogMsg("Disable Privoxy...");
 	my $cmd="reg query \"HKEY_USERS\"";
    my @info=`$cmd`;  	
	my @users;
	my $villages_user='';
	my $warns=0;
    if ($?) {
        LogMsg("Could not find HKEY_USERS in the registry ");  
       
    } else { 		
        foreach my $i (@info) {
			# Get a list of users
            chomp($i); 
			unless ($i =~ /_Classes/) {
				if ($i =~ /S-1/) {
					#print "Registry: ($i)\n";
					push(@users,$i);
				}
			}
			if ($verbose) {
				print "Registry: ($i)\n";
			} 
		}
    } 
 
	foreach my $u (@users) {		
		my $tmp=find_user($u, "Villages");
		if ($tmp) {
			$villages_user=$tmp;			 
		} 			
	}
	if ($verbose) {
		LogMsg(indent(1)."Determining Villages user...");
	}
	# Determine the Villages user in the registry
	$cmd="reg query \"HKEY_USERS\"";
    @info=`$cmd`;  		 		
	# Check if IE has a proxy server configured.  This would be our privoxy most likely
 
	if ($villages_user) {
		LogMsg(indent(1)."Villages User: $villages_user");
		$cmd="reg query \"$villages_user\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\" /v ProxyEnable";		
		@info=`$cmd`;  	
		my $privoxy_found=0;
		my $proxy_enable=0;
		my $agent="unknown";
		LogMsg("Checking for proxy server configured...");
		foreach my $i (@info) {
			chomp($i);  			 
			if ($i =~ /ProxyEnable/) {				
				my @tmp=split(/\s+/,$i);
				LogMsg("Found ProxyEnable: $tmp[-1]");
				if ($tmp[-1] eq "0x1") {
					$proxy_enable=1;			
				}				
			} 			
		}
		if ($proxy_enable) {	
			LogMsg("Disabling Privoxy...");
            $cmd="reg add \"$villages_user\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\" /v ProxyEnable /t REG_DWORD /d 0 /f";				
		    system("$cmd");	       				 
		}  
		
	} else {
		LogMsg("WARNING: Never determined villages user");
		$warns++;
	}
}

sub disable_privoxy_firefox {
	my $pref_dir=shift;
	my $firefox=shift;
	my $pref_file="prefs.js";
	my $user_file="user.js";
	my $flag="c:/temp/scripts/firefox_privoxy_disabled.flg";
	my $return=0;
	LogMsg("Disabling Privoxy for FireFox");
	if (-d $pref_dir) {
		if (-f $flag) {
			unless ($firefox eq "unknown") {
				# It is not at all clear that FireFox is still installed
				# so don't give a warning if this flag is still here.
				LogMsg("WARNING: Found $flag already set");
			}
			$return=1;
		}
		if (-f ("${pref_dir}/${user_file}")) {
			LogMsg("Found $user_file");			
		} else {
			LogMsg("Creating $user_file");
			open(FILE,">${pref_dir}/${user_file}");
			print FILE 'user_pref("network.proxy.type", 0);';
			close FILE;
			# Now terminate FireFox		 
			my $p = "firefox.exe";
			my $pid=checkPID("$p");
			if ($pid) {
				LogMsg("Killing $p");
				LogMsg("PID is $pid");
				`pskill $accepteula $pid`;
				sleep 1;
				$pid=checkPID("$p");
				if ($pid) {
					LogMsg("WARNING: Failed to stop $p");
					return 1;
				} else {
				   LogMsg("Stopped $p");
				}
			}			
		}
	} else {
		LogMsg("WARNING: Failed to locate $pref_dir");
		return 1;
	}
	open(FILE,">$flag");
	close FILE;
	return $return;
}

sub terminate_browsers {
	return;
	# This is not working because it causes a warning to come up after the restart about it crashing.
	# After changing the proxy settings, we want to force users to restart their browsers.
	my $chrome="chrome.exe";
	my $ie="iexplore.exe";
	my $firefox="firefox.exe";
	my @task_list_info=`tlist`;
	my @task_list;
	foreach my $t (@task_list_info) {
		while ($t =~ /^ /) {
			$t =~ s/^ //;
		}
		my @tmp=split(/\s+/,$t);
		my $pid=$tmp[0];
		my $task=lc($tmp[1]);
		if (($task eq $chrome) || ($task eq $ie) || ($task eq $firefox)) {
			LogMsg("Adding $task PID $pid to list...");
			push(@task_list,$pid);
		}
 
	}
	foreach my $t (@task_list) {
		LogMsg("Killing PID $t");
		my $cmd="pskill $accepteula $t";
		`$cmd`;
		
	}
	
}

sub check_correct_S32 {
	
	my $directive=shift;
	my $dll = "c:/windows/system32/S32EVNT1.DLL";
    my $dll_II = "c:/Program Files/Symantec/S32EVNT1.DLL";
	my $patch_dll = "c:/temp/install/reg/S32EVNT1.DLL";
		 
	my ($version, $symantec_version)=get_S32_version();
	unless (defined($directive)) {
		$directive="none";
	}
	if (($version =~ /^12.8/) || ($symantec_version =~ /^12.8/)) {
		# Report here:
		LogMsg(indent(1)."Windows S32EVNT1.DLL is version $version");
		LogMsg(indent(1)."Symantec S32EVNT1.DLL is version $symantec_version");
		if ($directive eq "correct") {
			# Correct here if requested:
			LogMsg(indent(1)."Updating...");		
			if ($version =~ /^12.8/){
				# The dll under windows system32 cannot be replaced while pcAnywhere is
				# running and so the directive must be specified for that copy to occur.
				if (copy($patch_dll,$dll)) {
					LogMsg(indent(1)."Copied over the replacement dll");
				} else {
					LogMsg(indent(1)."WARNING: Failed to copy over a replacement");
				}
			}		
			if ($symantec_version =~ /^12.8/) {
 		
				if (copy($patch_dll,$dll_II)) {
					LogMsg(indent(1)."Copied over the replacement dll to $dll_II");
				} else {
					LogMsg(indent(1)."WARNING: Failed to copy over a replacement");
				}  		 
			}
			($version, $symantec_version)=get_S32_version();
			if ($version =~ /^12.8/) {
				LogMsg(indent(1)."WARNING: S32EVNT1.DLL is version $version");
				return 0;
			}
			if ($symantec_version =~ /^12.8/) {
				LogMsg(indent(1)."WARNING: Symantec S32EVNT1.DLL is version $symantec_version");
				return 0;
			}	
		}			
	} elsif (($version =~ /^12.9/) && ($symantec_version =~ /^12.9/)) {
		LogMsg(indent(1)."Windows S32EVNT1.DLL is version $version");
		LogMsg(indent(1)."Symantec S32EVNT1.DLL is version $symantec_version");
		return 1
	} else {
		LogMsg(indent(1)."WARNING: Windows S32EVNT1.DLL is version $version");
		LogMsg(indent(1)."WARNING: Symantec S32EVNT1.DLL is version $symantec_version");
		return 0;			
	}
	# Return a 1 if all is well.
	return 1
}

sub get_S32_version {
	my $base=shift;
	unless (defined($base)) {
		$base=0;
	}
	# Check a DLL
 
	my $cmd="wmic datafile where name=\"c:\\\\windows\\\\system32\\\\S32EVNT1.DLL\" list /format:list";
	#LogMsg("($cmd)");
	my @datafile_info=`$cmd`;
	my $version;
	my $symantec_version;
	foreach my $d (@datafile_info) {
		chop($d);
		chop($d);
		if ($d =~ /^version/i) {
			my @tmp=split(/=/,$d);
			$version=$tmp[1];		 			
		}
	}
	$cmd="wmic datafile where name=\"c:\\\\program files\\\\symantec\\\\S32EVNT1.DLL\" list /format:list";
	#LogMsg("($cmd)");
	@datafile_info=`$cmd`;	
	foreach my $d (@datafile_info) {
		chop($d);
		chop($d);
		if ($d =~ /^version/i) {
			my @tmp=split(/=/,$d);
			$symantec_version=$tmp[1];		 			
		}
	}		
	if ($base) {	
		my @tmp=split(/\./,$version);
		$version="$tmp[0]"."."."$tmp[1]";
		@tmp=split(/\./,$symantec_version);		
		$symantec_version="$tmp[0]"."."."$tmp[1]"; 	
	}
	if ($verbose) {
		LogMsg(indent(2)."Windows System32: ($version) symantec ($symantec_version)");
	}
	return ($version,$symantec_version);
}
sub get_SYMEVENT_version {
	my $base=shift;
	unless (defined($base)) {
		$base=0;
	}
	# Check a SYS
	my $cmd="wmic datafile where name=\"c:\\\\windows\\\\system32\\\\drivers\\\\SYMEVENT.SYS\" list /format:list";
	#LogMsg("($cmd)");
	my @datafile_info=`$cmd`;
	my $version;
	my $symantec_version;
	foreach my $d (@datafile_info) {
		chop($d);
		chop($d);
		if ($d =~ /^version/i) {
			my @tmp=split(/=/,$d);
			$version=$tmp[1];		 			
		}
	}
	$cmd="wmic datafile where name=\"c:\\\\program files\\\\symantec\\\\SYMEVENT.SYS\" list /format:list";
	#LogMsg("($cmd)");
	@datafile_info=`$cmd`;	
	foreach my $d (@datafile_info) {
		chop($d);
		chop($d);
		if ($d =~ /^version/i) {
			my @tmp=split(/=/,$d);
			$symantec_version=$tmp[1];		 			
		}
	}	
	if ($base) { 
		my @tmp=split(/\./,$version);
		$version="$tmp[0]"."."."$tmp[1]";
		@tmp=split(/\./,$symantec_version);	 
		$symantec_version="$tmp[0]"."."."$tmp[1]"; 	
	}	
 
	if ($verbose) {
		LogMsg(indent(2)."Windows System32: ($version) symantec ($symantec_version)");
	}	
	return ($version,$symantec_version);
}

sub backup_S32 {
    my $dll = "c:/Program Files/Symantec/S32EVNT1.DLL";
	my $backup_dll = "c:/temp/install/reg/S32EVNT1_orig.DLL";
	my $install_dir = "c:/temp/install";	
	my $reg_dir = "c:/temp/install/reg";
	unless (-d $install_dir) {
		mkdir $install_dir;
	}	
	unless (-d $reg_dir) {
		mkdir $reg_dir;
	}
	if (-f $backup_dll) {
		LogMsg("S32EVNT1.DLL is already backed up");
		return 1;
	} else {
		if (-f $dll) {
			LogMsg("Found $dll");
			if (copy($dll,$backup_dll)) {
				LogMsg(indent(1)."Backed up $dll to $backup_dll");
			} else {
				LogMsg(indent(1)."WARNING: Failed to backup $dll");
			}
		}
	}
	if (-f $backup_dll) {
		LogMsg("Backup of S32EVNT1.DLL successful");
	} else {
		LogMsg("Backup of S32EVNT1.DLL unsuccessful");
		return 0;
	}
	return 1;
}

sub check_dlls {
	LogMsg("Checking basic necessary DLL's...");
	# Check that some basic necessary DLL's can be found
	my %dll_hash=(
		'ATL71.DLL' => 'c:/windows/system32/ATL71.DLL'
	);
	my %dll_source_hash=(
		'ATL71.DLL' => '//Edna/temp/install/reg/ATL71.DLL'	
	);
	my $warns=0;
	foreach my $dll (sort keys(%dll_hash)) {
		if (-f $dll_hash{$dll}) {
			LogMsg(indent(1)."Located $dll");
		} else {
			LogMsg("$dll was not found");
			if ($dll_source_hash{$dll}) {
				LogMsg("Getting $dll from source safe...");
				if (-f $dll_source_hash{$dll}) {
					copy($dll_source_hash{$dll},$dll_hash{$dll}) ;
					if (-f $dll_hash{$dll}) {
						LogMsg(indent(1)."Copy of $dll successful");
					} else {
						LogMsg(indent(1)."WARNING: Failed to locate $dll!");					
						LogMsg(indent(1)."WARNING: Failed to copy $dll from source safe.");
						$warns++;						
					}
				}
			
			} else {
				LogMsg(indent(1)."WARNING: Failed to locate $dll!");
				$warns++;
			}
		}
	}
	return $warns;
}

sub check_dotnet {
	# Registers need to have Microsoft .Net installed to support the VeriFone EFT code.
	my $cmd="reg query \"HKLM\\SOFTWARE\\Microsoft\\.NetFramework\" ";    
	my @info=`$cmd`;	
	my $dotNet_found=0;
	my $warns=0;
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /NetFramework.v4/) {			
			$dotNet_found=1;
		}
	}	
	if ($dotNet_found) {
		LogMsg("Found NetFramework V4 installed");
	} else {
		LogMsg("Did not find NetFramework V4 installed");
		my $install_enabled=0;
		if ($force) {
			LogMsg("Force enabling installation for $hostname");
			$install_enabled=1;	
		} else {
			#if ($hostname =~ /Reg1/i) {
				LogMsg("Hostname: $hostname Store: $storenum Hour: $current_hour Day: $day_of_week");
				#if ((($storenum =~ /^8/) ||  ($storenum =~ /^11/)) && ($day_of_week > 0) && ($day_of_week < 6) ) {			 
				#if ((($storenum =~ /^1/) ||  ($storenum =~ /^11/)) && ($day_of_week > 0) && ($day_of_week < 6) ) {			 
				if (($day_of_week > 0) && ($day_of_week < 6) ) {			 
					if ($current_hour < 7) {
						$install_enabled=1;	 
					} else {
						LogMsg("Install would be enabled but it is too late in the day");
					}
				}	
			#} else {
				LogMsg("Hostname: $hostname");
			#}	
		}
		if ($install_enabled) {
			unless (net_install()) {
				LogMsg("WARNING: Failed to install .Net");
				$warns++;
			}
		} else {
			LogMsg("NOTICE: dotNet is not installed but install is not enabled at this time.");
		}
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking dot net");
		$warning_count++;
	}	 
}

sub net_install {
	unless ($OS eq "WinXP") {
		LogMsg("The net_install function does not support $OS");
		return;
	}
	if (($no_action) || ($debug_mode)) {
		LogMsg("Skipping net install as requested.");
		return 1;
	}
	 
	LogMsg(".Net Install");
    my $installer="c:/temp/scripts/net_install.pl";
 
	my $failed_flag="c:/temp/net_install_failed.txt";
	my $success_flag="c:/temp/net_installed.txt";		

	if (-f $failed_flag) {
		LogMsg("WARNING: Detected the install has previously failed");
		return 0;
	}
	if (-f $success_flag) {
		LogMsg("WARNING: Detected the install has previously been completed.");
		return 0;
	}	 		 
    if (-f $installer) {
		if ($verbose) {
			LogMsg(indent(1)."Found $installer");
		} 
        my $cmd="$installer";            
        LogMsg("Running $installer...");
		if (-f $installer) {
			if ($verbose) {
				LogMsg(indent(1)."Found $installer");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $installer");
			return 0;
		}
        `$cmd`;

        # Check that it was indeed installed  
		if (-f $failed_flag) {
			LogMsg("WARNING: Detected the install has failed");
			return 0;
		}
		if (-f $success_flag) {
			LogMsg("Detected the install has been completed.");	
 			
		}				  
    } else {
        LogMsg("Failed to locate $installer");
        return 0;
    }		
    return 1;
}

sub check_date {
	# Get time
	my @ltm = localtime();
 
	my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);
	LogMsg("Date: $date_label");
	my $current_year=$ltm[5]+1900;	
	if ($current_year < 2014) {
		LogMsg("ERROR: Suspect that the battery on this unit is dead.  Please set the date!");
		return 0;
	}
	return 1;
}

sub correct_date {
	# We could just try to sync with a time server but if the date and time are off too far, that will fail.
	# This function gets the time and date from the Internet and failing that, from Edna
    # If the date is lost due to a dead battery, can we get it from the Internet?
	# First check if we can connect to it
 
 
    use LWP::Simple;
	chomp(my $current_system_date = `date /T`);
	my @tmp=split(/\s+/,$current_system_date);
	$current_system_date = $tmp[1];
	chomp(my $current_system_time = `time /T`);
	@tmp=split(/\s+/,$current_system_time);
	$current_system_time = $tmp[0];	
	my $current_system_am_pm = $tmp[1];
    my $current_date;
    my $current_time;
    my $current_am_pm;	
	my $need_to_correct=0;	
 
	# Try getting the date from the Edna server
	my $URL="http://Edna/cgi-bin/i_toolshed.pl?report=GETDATE";		
	my $content=get($URL); 		
		unless ($content) {		 
			# If we got nothing try once more
			sleep 1;
			$content=get($URL);
			###
		}
		if ($content) {
			# If we got content, parse it for the info we want
			my @content_info=split(/\<br\>/,$content);
			my $target_found=0;
			my $year_value;
			my $month_value;
			my $day_value;
			my $hour_value;
			my $minute_value;
			my $am_pm_value;
			foreach my $c (@content_info) { 
	 
				my @tmp=split(/body/,$c);           
				foreach my $t (@tmp) {
					if ($t =~ /year.*month.*day.*hour.*minute/) {
						my @tmp2 = split(/>/,$t);
						foreach my $t2 (@tmp2) {
							if ($t2 =~ /year /) {							
								my @tmp3 = split(/\</,$t2);
								my @tmp4 = split(/\s+/,$tmp3[0]);
								$year_value = $tmp4[1];							
							}
							if ($t2 =~ /month /) {						
								my @tmp3 = split(/\</,$t2);
								my @tmp4 = split(/\s+/,$tmp3[0]);								
								$month_value = sprintf("%02d",$tmp4[1]);															
							}
							if ($t2 =~ /day /) {						
								my @tmp3 = split(/\</,$t2);
								my @tmp4 = split(/\s+/,$tmp3[0]);
								$day_value = sprintf("%02d",$tmp4[1]);							
							}	
							if ($t2 =~ /hour /) {						
								my @tmp3 = split(/\</,$t2);
								my @tmp4 = split(/\s+/,$tmp3[0]);
								$hour_value = $tmp4[1];
								if ($hour_value > 12) {
									$hour_value -= 12;
									$am_pm_value = "PM"
								} else {
									$am_pm_value = "AM"
								}							
							}		
							if ($t2 =~ /minute /) {						
								my @tmp3 = split(/\</,$t2);
								my @tmp4 = split(/\s+/,$tmp3[0]);
								$minute_value = sprintf("%02d",$tmp4[1]);							
							}														 
						}
						$hour_value=sprintf("%02d",$hour_value);
						$current_date = "$month_value"."/"."$day_value"."/"."$year_value";
						$current_time = "$hour_value".":"."$minute_value";
						$current_am_pm = $am_pm_value;
					} 				
				}                     
			} 			    		
		}			
 
  
    if ($current_date) {
		# Check the current date found with the current system date
 
 	
		if ($current_system_date ne $current_date) {
			LogMsg("WARNING: System Date: $current_system_date - Expected: $current_date");
			$need_to_correct=1;
		}
 
		if ($current_system_time ne $current_time) {
			LogMsg("WARNING: System Time: $current_system_time - Expected: $current_time");
			$need_to_correct=1;
		}			
		if ($current_system_am_pm ne $current_am_pm) {
			LogMsg("WARNING: System AM/PM: $current_system_am_pm - Expected: $current_am_pm");
			$need_to_correct=1;
		}				
 		
		if ($need_to_correct) {			
			# Correct the date
			LogMsg("Setting date to $current_date");
			my $cmd = "date $current_date";
			`$cmd`;
			# Correct the time
			LogMsg("Setting time to $current_time $current_am_pm");
			$cmd = "time $current_time $current_am_pm";
			`$cmd`;             
		} else {
			LogMsg("Date and Time look correct");
		}
    } else {
        LogMsg("ERROR: Failed to determine today's date");
    }
 
}

sub schedule_do_scan {
	# The CustomScheduledScan got lost with the upgraded from SEP11 to SEP12.  So, we will launch a scan 
	# with this function.  It should run on Saturday and schedule it for Sunday morning at 2:00am - kdg
	my $warns=0;
	my $tool=0;
	my $dir="C:/Program Files/Symantec/Symantec Endpoint Protection";
	my $dir_alt="C:/Program Files/Symantec Antivirus";		
		
	unless (-d $dir) {	
		unless (-d $dir_alt) {
			LogMsg("WARNING: Failed to locate $dir");		
			return 1
		} else {
			$dir=$dir_alt;
		}
	}
	opendir(DIR,"$dir");
	my @dir_list=readdir(DIR);
	close DIR;	
	foreach my $d (@dir_list) {		
		next if ($d eq ".");
		next if ($d eq "..");		
		if (-d $d) {			
			my $candidate="${dir}/${d}/Bin/DoScan.exe";
			
			if (-f $candidate) {
				$tool=$candidate;
				last;
			}
		} elsif ($d eq "DoScan.exe") {
			my $candidate="${dir}/${d}";
			if (-f $candidate) {
				$tool=$candidate;
				last;
			}
		}
	}
 
	if (-f $tool) {
		LogMsg("Scheduling Scan of Drive C");
		my $cmd = "at 2:00 \"$tool\" /ScanDrive \"C\"";						
		system($cmd);
	} else {
		LogMsg("WARNING: Failed to locate DoScan.exe");
		$warns++;
	}
	return $warns;
}

sub check_eft {
	LogMsg("Checking EFT");
	my $warns=0;
	my $install_enabled=0;
 
	$warns+=check_eft_log();
	
	my $IP=gather_eft_info();
	$warns+=check_eft_ini();
	if ($IP) {	
		if (ping_test("$IP")) {
			LogMsg("Ping successful to VeriFone at $IP");			
			# If there is a successful ping, we can check the reboot utility
			#$warns+=check_MxReboot_install();	
		} else {
			LogMsg("WARNING: NOT able to ping VeriFone at $IP");
			$warns++;
		}
	} elsif ($found_eft_status eq "ACTIVE") {
		LogMsg("WARNING: NOT able to determine VeriFone $IP");
		$warns++;	
	}
	LogMsg("EFT status is $found_eft_status");
	if ($found_eft_status eq "ACTIVE") {		
		$warns+=check_eft_driver();
		$warns+=check_pos_link();
		$warns+=check_local_ini();
	}
	
	if ($force) {
		LogMsg("Force enabling check_eft for $hostname");
		$install_enabled=1;	
	} else { 
		LogMsg("check_eft not forced for $hostname");
		if ($hostname =~ /Reg/i) {
			LogMsg("Hostname: $hostname Store: $storenum Hour: $current_hour Day: $day_of_week");
			if (($day_of_week > 1) && ($day_of_week < 5)) {
				# Yet another layer of control to roll the driver out progressively
				# Certain registers will ONLY be updated on certain days of the week.
				# This is to prevent all of the registers in any given store from being updated on the same day.
				if ((($hostname =~ /reg1/i) && ($day_of_week == 2)) ||
					(($hostname =~ /reg2/i) && ($day_of_week == 3)) ||
					(($hostname =~ /reg[3-6]/i) && ($day_of_week == 4))) {
					
					LogMsg("Install is enabled for day $day_of_week");
					if ($current_hour < $cuttoff_hour) {
						# If Edna is telling us that EFT should be enabled but we have found that it is not, install it.
						# This is useful in the event that a register is replaced and the new one does not have the
						# EFT driver installed yet. 
						LogMsg("Install is enabled for hour $current_hour");
						LogMsg("Found EFT STATUS: ($found_eft_status)");
						if ($found_eft_status ne "ACTIVE") {                    
							if ($EFT_status eq "ACTIVE") {
								LogMsg("Enabling EFT install as Edna indicates it should be installed but it appears that it is not.");
								$install_enabled=1;	 
								$enable_eft_ini_bat_install=1;
							}
						} else {
							# If eft is known to be active, enable install.
							# This is necessary so that it can update itself if necessary.						
							LogMsg("EFT was found to be ACTIVE");
							$install_enabled=1;		# This will allow the driver to update  
						}
					} else {
						LogMsg("Install would be enabled but it is too late in the day");
					}
				} else {
					LogMsg("Install is not enabled for host $hostname on day $day_of_week");
				}
			} else {
				LogMsg("Install is not enabled for day $day_of_week");
			}
 	
		} else {
			LogMsg("Hostname: $hostname");
		}	
	}
	unless ($install_enabled) {
		LogMsg("Not downloading EFT code yet...returning");

		return;
	}
	LogMsg("Calling eft_code_download...");
	$warns+=eft_code_download();
	if ($force) { 
		LogMsg("Forcing eft_code_update...");
		unless(eft_code_update("force")) {
			$warns++;
		}	
	} else {
		LogMsg("Calling eft_code_update...");
		unless(eft_code_update()) {
			$warns++;
		}
	}
	# Show the version here	
	LogMsg("Gather eft info...");
	gather_eft_info();

	if ($enable_eft_ini_bat_install) {
		LogMsg("Calling eft_ini_bat_install...");
		$warns+=eft_ini_bat_install();	
	} else {
		LogMsg("Installing EFT ini & bat file is not currently enabled");
		LogMsg("Run with both the -eft and the -enable options to install EFT fully.");	
	}
	if ($update_eft_ini) {
		LogMsg("Calling update_eft_ini...");
		$warns+=update_eft_ini();
	} else {
		LogMsg("Updating of the eft ini is not currently enabled because no EFT code was updated.");
	}

	if ($warns) {
		LogMsg("WARNING: There were $warns issues checking eft code");
		$warning_count++;
	}
}

sub check_eft_status {
	my $warns=0;
	my $local_ini="C:/Program Files/SAP/Retail Systems/Point of Sale/Parm/local.ini";
	my $eft_ini="C:/Program Files/SAP/Retail Systems/Point of Sale/TTVEFT/TTVGateway.ini";
	my $active_line="EUROEFTDLLNAME";	
	my $eft_active=0;
	if (-f $eft_ini) {
		# This indicates that the EFT driver is at least present.  Check the local_ini to verify if it is in effect
		if (-f $local_ini) {
			open(INI,"$local_ini");
			my @ini_info=(<INI>);
			close INI;
			foreach my $i (@ini_info) {
				chomp($i);
				if ($i =~ /^$active_line/) {					
					$eft_active=1;
				}				
			}
		} else {
			LogMsg("WARNING: Failed to locate local.ini");
			$warns++;
		}
	} else {
		LogMsg("EFT Driver does not appear to be installed.");
	}
	return $eft_active;
}

sub check_eft_driver {
	LogMsg("Checking EFT driver...");
	# If the eft is enabled, then the eftdevice driver should be running
	my $warns=0;
	my $pos_proc="posw.exe";
	my $eft_proc="EFTDeviceDriver.exe";
	
	if (checkPID($pos_proc)) {
		LogMsg(indent(1)."process $pos_proc is running");
		if (checkPID($eft_proc)) {
			LogMsg(indent(1)."process $eft_proc is running");
		} elsif (alternateCheck($eft_proc)) {
			LogMsg(indent(1)."process $eft_proc is found (via alt check) to be running ");			
		} else {
			LogMsg(indent(1)."WARNING: process $eft_proc is NOT running");
			LogMsg(indent(1)."Need to kill the POS");
			$restart_pos = 1;
			$warns++;
		}		
	} else {
		if (checkPID($eft_proc)) {
			LogMsg(indent(1)."WARNING process $eft_proc is running but $pos_proc is not");
			$restart_pos = 1;
			$warns++;
		} else {
			LogMsg(indent(1)."WARNING: process $eft_proc is NOT running and $pos_proc is not");
			LogMsg(indent(1)."Need to restart the POS");
			$restart_pos = 1;
			$warns++;
		}			
	}	
	return $warns;
	
}

sub check_pos_link {
	LogMsg("Checking POS link...");
	# Check that the old Point of Sale link is gone if EFT is active
	my $dir="C:/Documents and Settings/Villages/Desktop";
	my $dir_alt="C:/Users/Villages/Desktop";
	if (-d $dir_alt) {
		$dir=$dir_alt;
	}
	#my $dir2="//Edna/temp/install/shortcuts/TimeStar.lnk";
	#my $dir2="//Edna/temp/install/shortcuts/TimeStar.lnk";
	opendir(DESKTOP,"$dir");
	my @file_list=readdir(DESKTOP);
	close DESKTOP;
	my $warns=0;
	my $found_old_pos_link=0;
	my $found_new_pos_link=0;
	
	foreach my $f (@file_list) {
		next if ($f eq ".");
		next if ($f eq "..");		
		if ($f =~ /point of sale/i) {
			$found_old_pos_link=1;
		}
		if ($f =~ /POS w EFT/) {
			$found_new_pos_link=1;
		}		
	}
	if ($found_new_pos_link) {
		LogMsg("Found new POS w EFT link");
		if ($found_old_pos_link) {
			LogMsg("ERROR: Found old Point of Sale link on the desktop");
			$warns++;
		}
	}
	# Check the Public Desktop
	$dir = "C:/Users/Public/Desktop";
	opendir(DESKTOP,"$dir");
	@file_list=readdir(DESKTOP);
	close DESKTOP;
	 
	$found_old_pos_link=0;
 
	foreach my $f (@file_list) {
		next if ($f eq ".");
		next if ($f eq "..");			
		if ($f =~ /point of sale/i) {		
			$found_old_pos_link=$f;
		}
		if ($f =~ /POS w EFT/) {
			$found_new_pos_link=1;
		}		
	}
	if ($found_new_pos_link) {	
		if ($found_old_pos_link) {
			# Remove the old link
			my $file="${dir}/${found_old_pos_link}";
			if (-f $file) {
				LogMsg("Unlinking $file...");
				unlink $file;				
			}
			if (-f $file) {
				LogMsg("ERROR: Found old Point of Sale link on the Public desktop and failed to remove it.");
				$warns++;
			}
		}	
	}
	return $warns;
}

sub check_local_ini {
	my $local_ini_file="local.ini";
	my $server_local_ini_path="C:/Program Files/SAP/Retail Systems/Xpress Server/Parm";
	my $reg_local_ini_path="C:/Program Files/SAP/Retail Systems/Point of Sale/parm";
	my $s_local_ini="${server_local_ini_path}/${local_ini_file}";
	my $r_local_ini="${reg_local_ini_path}/${local_ini_file}";

	my $warns=0;
	
	LogMsg("Checking local ini...");
	# If the local ini file in the server folder is not present, there is no need to continue
	if (-f $s_local_ini) {
		my $server_md5=get_file_sum($s_local_ini);
		my $reg_md5=get_file_sum($r_local_ini);	
		if ($server_md5) {
			if ($server_md5 eq $reg_md5) {
				if ($verbose) {
					print "Sum for server ini: $server_md5\n";
					print "Sum for register ini: $reg_md5\n";
				}
			} else {
				if ($verbose) {
					print "Sum for server ini: $server_md5\n";
					print "Sum for register ini: $reg_md5\n";
				}		
				LogMsg("Need to Copy $local_ini_file from server to reg folder.");			
				if (copy($r_local_ini, $s_local_ini)) {
					LogMsg(indent(1)."Done");
				} else {
					LogMsg(indent(1)."WARNING: Copy appears to have failed!");
					$warns++;
				}
				LogMsg("Checking sums again.");
				sleep 1;
				$server_md5=get_file_sum($s_local_ini);
				$reg_md5=get_file_sum($r_local_ini);	
				if ($verbose) {
					print "Sum for server ini: $server_md5\n";
					print "Sum for register ini: $reg_md5\n";
				}					
				if ($server_md5 eq $reg_md5) {	
					if ($verbose) {
						print "Sum for server ini: $server_md5\n";
						print "Sum for register ini: $reg_md5\n";
					}
				} else {
					LogMsg("WARNING: Failed to copy the local.ini file to $server_local_ini_path");
					$warns++;
				}
			}
		} else {
			LogMsg("WARNING: Failed to determine sum for server local.ini file");
			$warns++;
		}
	} else {
		if (-d $server_local_ini_path) {
			LogMsg("Found $server_local_ini_path but no local.ini file");
		} else {
			LogMsg("$server_local_ini_path was not found.");
		}
	}
	return $warns;
}

sub get_file_sum {
	my $file=shift;
	my $sum=0;
	my @md5_info=`md5 "$file"`;	
	foreach my $m (@md5_info) {
		chomp($m);		
		my @tmp=split(/\s+/,$m);
		$sum=$tmp[0]; 
	}
	return $sum;
}

 

sub ping_test {
	my $target=shift;
	system("ping $target");
	if ($?) {
		return 0;
	} else {
		return 1;
	}
}


sub gather_eft_info {
	# This function determines if EFTDeviceDriver is running and gathers some info

	my $IP=0;
	my $eft_code_enabled=0;
	if (-f $eft_local_ini) {
		open(INI,"$eft_local_ini");
		my @ini_info=(<INI>);
		close INI;
		foreach my $i (@ini_info) {
			chomp($i);
			if ($i =~ /^EUROEFTDLLNAME/) {
				$eft_code_enabled=1;
				
			}			
		}
	}	
 
	if ($eft_code_enabled) {
		LogMsg("System Info: EFTDeviceDriver: ACTIVE");
		$found_eft_status="ACTIVE";
		if (-f $eft_ini) {
			open(INI,"$eft_ini");
			my @file_info=(<INI>);
			close INI;
			foreach my $f (@file_info) {
				chomp($f);
				if ($f =~ /TerminalAddr/) {
					my @tmp=split(/=/,$f);
					$IP=$tmp[1];
					LogMsg("System Info: IP-VeriFone: $IP");				
				}
			}
		} else {
			LogMsg("WARNING: Failed to locate $eft_ini");			
		}
		if (-f $eft_version_ini) {
			open(INI,"$eft_version_ini");
			my @file_info=(<INI>);
			close INI;
			foreach my $f (@file_info) {
				chomp($f);
				
				if ($f){
					my @tmp=split(/-/,$f);
					my $key=$tmp[0];
					my $value=$tmp[1];
					LogMsg("System Info: $key: $value");
					$eft_version=$value;
		
				}
			}			
		} else {
			LogMsg("WARNING: Failed to locate $eft_version_ini");	 
		}
		if (-f $lib_version_ini) {
			open(INI,"$lib_version_ini");
			my @file_info=(<INI>);
			close INI;
			foreach my $f (@file_info) {
				chomp($f);
				
				if ($f){
					my @tmp=split(/-/,$f);
					my $key=$tmp[0];
					my $value=$tmp[1];
					LogMsg("System Info: $key: $value");	
					$lib_version=$value;					
				}
			}
		} else {
			LogMsg("WARNING: Failed to locate $lib_version_ini");				
		}		
	} else {
		LogMsg("System Info: EFTDeviceDriver: INACTIVE");
		LogMsg("System Info: IP-VeriFone: NA");	
	}
		
	return $IP;	# Return the IP so that we can check connectivity.
}

sub check_eft_log {
	LogMsg("Checking EFT LOG #1");
	# There are actually two eft.log files
	#################################
	# Check the first one for size
	#################################
	my $eft_log = "C:/Program Files/SAP/Retail Systems/Point of Sale/eft.log";
 
	my $warns=0;
	if (-f $eft_log) {
		LogMsg("Found eft log");
		 
		my @file_info=stat($eft_log);
		my $filesize=sprintf("%.2f",($file_info[7] / 1024));
		if ($filesize > 500) {
			LogMsg("WARNING: EFT LOG is $filesize KB");
			$warns++;
		} else {
			LogMsg("EFT LOG is $filesize KB");
		}		
	 
	}
	LogMsg("Checking EFT LOG #2");
	#################################
	# Check the second one for errors 
	#################################
	$eft_log="C:/Program Files/SAP/Retail Systems/Point of Sale/logs/EFT.LOG";
	my $eft_log1="C:/Program Files/SAP/Retail Systems/Point of Sale/logs/EFT.LOG.1";
	 
	my %error_string_hash=(
		'EFT is offline: TIMED OUT' => 'EFT TIMED OUT',		
		'Returned PayWithKnownCardTypeV2.*tenderType=CREDITCARD' => 'Returned tenderType=CREDITCARD',		
		'Returned PayWithKnownCardTypeV2.*tenderType=UNKNOWN' => 'Returned tenderType=UNKNOWN',
		'Returned PayWithKnownCardTypeV2.*tenderType=UNKNOWNMAN' => 'Returned tenderType=UNKNOWNMAN',				
		'Returned PayWithKnownCardTypeV2.*tenderType=^U.*respCode=3' => 'Returned respCode=3',
	);
	
	my %current_error_count_hash=();
	my %yesterday_error_count_hash=();
	my $my_current_month=sprintf("%02d",$current_month);	
	my $current_month_name="$month_name_hash{$my_current_month}";	
	my $current_date_string="$current_month_name $current_day";
	my $previous_day_month_name="$month_name_hash{$previous_day_month}";
	my $previous_date_string="$previous_day_month_name $previous_day_day";	
	foreach my $log ("$eft_log1","$eft_log") {
		
		if (-f $log) {
			LogMsg(indent(1)."Checking $log...");
			open(LOG,"$log");
			my @log_info=(<LOG>);
			close LOG;
			my $txn=0;
			my $amount='amount=unknown';
			my $dateTime=0;
			my $date_string;
			foreach my $line (@log_info) {
				# Find yesterday and todays dates only

                chomp($line);
				if (($line =~ /^$previous_date_string/) || ($line =~ /^$current_date_string/)) {				
				
					if ($line =~ /^$previous_date_string/) {
						$date_string=$previous_date_string;
					} elsif ($line =~ /^$current_date_string/) {
						$date_string=$current_date_string;
					}
					
                    if ($line =~ /Calling PayWithKnownCardTypeV2.*txn/) {
                        my @tmp=split(/\,/,$line);
                        $txn=$tmp[-1];                        
                        $txn =~ s/\D//g;           
                        $dateTime=substr($line,0,15);
                    }
                    if ($line =~ /PayWithKnownCardTypeV2 - begin/) {
                        my @tmp=split(/\s+/,$line);				 
						foreach my $t (@tmp) {
							if ($t =~ /amount/) {
								$amount=$t;
							}						 
						}                       						                     
                    }					
					foreach my $e (sort keys(%error_string_hash)) {
						if ($line =~ /$e/) {
							my $label=$error_string_hash{$e};					
							$yesterday_error_count_hash{$label}++;
							LogMsg("VERIFONE: $date_string Txn: $txn $amount $label");
						}
					}
				}		
=pod				
				if ($line =~ /^$current_date_string/) {
                    if ($line =~ /Calling PayWithKnownCardTypeV2.*txn/) {
                        my @tmp=split(/\,/,$line);
                        $txn=$tmp[-1];                        
                        $txn =~ s/\D//g;                        
                        $dateTime=substr($line,0,15);
                    } 
					foreach my $e (sort keys(%error_string_hash)) {
						if ($line =~ /$e/) {						
							my $label=$error_string_hash{$e};							 				
							$current_error_count_hash{$label}++;
							LogMsg("VERIFONE: $current_date_string Txn: $txn $label");
						}
					}								
				}	
=cut				
			}
			# Archive the EFT_LOG to Edna.  They will get processed by the monitor.pl on Edna and get polled back to Akron
			my $target_dir="//Edna/temp/install/register_utilities/";
			my $filename=basename($log);			
			my $target_file="${target_dir}/${hostname}_${filename}";
			if (-d "$target_dir") {	
				LogMsg(indent(1)."Found $target_dir");
				if (-f $log) {	 
					if (copy($log,$target_file)) {
						if (-f "$target_file") {
							LogMsg("Successfully copied eft.log to Edna");							 
						} else {
							LogMsg("Did not find $target_file");
							LogMsg("WARNING: Failed to copy eft.log file to Edna");
							$warns++;
						}
					} else {		
						LogMsg("WARNING: Failed to copy eft.log file to Edna");
						$warns++;
					}
				}
			} else {
				LogMsg("WARNING: Failed to locate Edna");
				$warns++;
			}
		} else {
			if ($log eq $eft_log) {
				# It is okay if we don't see eft_log1 but we should find eft_log
				LogMsg("WARNING: Failed to locate EFT.log");
				$warns++;
			}
		}
	}
 
	
	# Check the results
	foreach my $key (sort keys(%current_error_count_hash)) {
		my $count=$current_error_count_hash{$key};
		LogMsg("VERIFONE: $count instances of $key for $current_date_string");
		LogMsg(indent(1)."System Stats: $key: $count");
		#$warns++;
	}
	foreach my $key (sort keys(%yesterday_error_count_hash)) {
		my $count=$yesterday_error_count_hash{$key};
		LogMsg("VERIFONE: $count instances of $key for $previous_date_string");
		LogMsg(indent(1)."System Stats: $key: $count");
		
		#$warns++;
	}	
	return $warns;	
 
}

sub check_eft_ini {

	my $warns=0;
	my $eft_code_enabled=0;
	if (-f $eft_local_ini) {
		open(INI,"$eft_local_ini");
		my @ini_info=(<INI>);
		close INI;
		foreach my $i (@ini_info) {
			chomp($i);
			if ($i =~ /^EUROEFTDLLNAME/) {
				$eft_code_enabled=1;				
			}			
		}
	}	
	unless ($eft_code_enabled) {
		LogMsg(indent(1)."Not checking TTVGateway.ini since full EFT checking is not currently enabled");
		return 0;
	}
	if ($eft_IP_exception_store_hash{$storenum}) {
		LogMsg(indent(1)."Not checking TTVGateway.ini since store $storenum is excepted.");
		return 0;
	}
 
	if (-f $eft_ini) {
		# Check the configuration of the eft ini file
		if ($myip) {		
			# Determine the lane number
			my $lane=$hostname;		
			my $txnNumber=0;
			my @ini_info=();
			my $found_TxnNumber=0;
			if ($storenum) {
				open(INI,"$eft_ini");
				@ini_info=(<INI>);
				close INI; 
				for (my $x=0; $x<=$#ini_info; $x++) {
					chomp(my $line=$ini_info[$x]);
					if ($line =~ /TxnNumber/i) {
						$found_TxnNumber=1;
					} 
				}
			} else {
				LogMsg("ERROR: Failed to determine storenum and so could not check TTVGateway.ini");
				$warns++;		
				return $warns;
			}			
	
			$lane=~ s/\D//g;	
			$txnNumber = "$lane" . "003";	# Construct a txnnumber based on the lane number			
			$lane=sprintf("%02d",$lane);
			
			# Determine the VeriFone's IP address			 
			my @tmp=split(/\./,$myip);
			my $third_part=$tmp[2];
			my $fourth_part=$tmp[3];
			# Increment by 3 to get the VeriFone's IP
			# or by 2 for some stores.
			if ($eft_plus2_store_hash{$storenum}) {
				$fourth_part=($fourth_part + 2);
			} elsif ($revised_network) {
				$fourth_part=($fourth_part + 5);
			} else {
				$fourth_part=($fourth_part + 3);
			}
			
			my $VeriFone_IP="192.168"."."."$third_part"."."."$fourth_part";
			my @expected_ini=();
			push(@expected_ini,"Verifone Parameters");
			push(@expected_ini,"TerminalAddr=$VeriFone_IP");
			push(@expected_ini,"StoreNumber=$storenum");
			push(@expected_ini,"Lane=$lane");	
			LogMsg("EFT Version: $eft_version");

			if ((!$eft_version =~ /^V1/) || ($found_TxnNumber)) {
				# This feature only came with V2 of the code so do not
				# expect it for V1 of the code but it is acceptable if it is present.
				push(@expected_ini,"TxnNumber=$txnNumber");		 
			}			
			push(@expected_ini,"DebugFlag=FALSE");
		
			for (my $x=0; $x<=$#ini_info; $x++) {
				chomp(my $line=$ini_info[$x]);
				my $expected="nothing";
				if (defined($expected_ini[$x])) {
					$expected=$expected_ini[$x];
				}
				
				if ($line eq "$expected") {
					if ($verbose) {
						print "$line is correct\n";
					}
				} else {
					LogMsg("ERROR: Line $x in $eft_ini is ($line) but expected ($expected) - cei1");
					$warns++;							
				}
			}
		} else {
			LogMsg("ERROR: Failed to determine the IP address and so could not check TTVGateway.ini");
			$warns++;			
		}		
	}
	return $warns;
}


sub reset_eft_log {
	my $eft_log = "C:/Program Files/SAP/Retail Systems/Point of Sale/eft.log";
	my $pos_dir = "C:/Program Files/SAP/Retail Systems/Point of Sale";
	my $warns=0;
	if (-f $eft_log) {
		LogMsg("Found eft log");		
		my $index;
		
		if (-d $pos_dir) {
			my %eft_index_hash;
			opendir(SOURCE,"$pos_dir");
			my @dir_list=readdir SOURCE;
			closedir SOURCE; 
			foreach my $d (@dir_list) {
				next if ($d eq ".");
				next if ($d eq "..");
				if ($d =~ /eft.log.\d/) {
					my $file="${pos_dir}/$d";
					my @tmp=split(/\./,$d);
					my $counter=$tmp[-1];
					$eft_index_hash{$counter}=$d;
				}					
			}
			foreach my $count (reverse sort keys(%eft_index_hash)) {
				my $file=$eft_index_hash{$count};
				my $filepath="${pos_dir}/$file";
				if (-f $filepath) {
					# Move this log
					$index=$count;
					$index++;
					my $newfile="eft.log"."."."$index";
					my $newfilepath="${pos_dir}/$newfile";					
					if (move($filepath,$newfilepath)) {						
						if (-f $filepath) {
							LogMsg(indent(1)."WARNING: Unexpectedly still find $filepath");
							$warns++;							
						} else {
							LogMsg(indent(1)."Moved $file to $newfile");
						}
					} else {
						LogMsg(indent(1)."WARNING: Failed to move $file to $newfile");
						$warns++;						
					}
				}
			}
			# Move eft.log to eft.log.1
			my $file="eft.log";
			my $filepath="${pos_dir}/$file";				
			my $newfile="eft.log"."."."1";
			my $newfilepath="${pos_dir}/$newfile";				
			if (-f $filepath) {
				if (-f $newfilepath) {
					LogMsg(indent(1)."WARNING: Unexpectedly found $newfilepath");
					$warns++;
				} else {
					if (move($filepath,$newfilepath)) {
						LogMsg(indent(1)."Moved $file to $newfile");
					} else {
						LogMsg(indent(1)."WARNING: Failed to move $file to $newfile");
						$warns++;
					}						
				}
			} else {
				LogMsg(indent(1)."WARNING: Unexpectedly did NOT find $filepath");
				$warns++;
			}				
		}
 
	}
	return $warns;
}

sub eft_code_download {
	LogMsg("### EFT CODE Download ###");
	# This function will copy the EFT code from Edna and put it in the EFT_CODE directory
	# in the Point of Sale Folder. This is an intermediate folder from which it will be 
	# later installed. - kdg
	if ($no_edna) {
		LogMsg("Not checking for code on Edna.");
		return 0;
	}
	
	foreach my $store_reg (@eft_beta_store_reg_list) {
		my @tmp=split(/-/,$store_reg);
		my $beta_store=$tmp[0];
		my $beta_reg=$tmp[1];		
		if ($storenum == $beta_store) {
			LogMsg(indent(2)."Beta code enable check 1 - store: $beta_store");
			if ($hostname =~ /$beta_reg/i) {
				LogMsg(indent(2)."Beta code enable check 2 - hostname: $beta_reg");
				$install_beta=1;
				LogMsg(indent(2)."Enabling install beta: $install_beta");
			}	
			if ($install_beta) {
				LogMsg(indent(2)."Beta code enable check 3 - install_beta: $install_beta - (yes)");
			} else {
				LogMsg(indent(2)."Beta code enable check 3 - install_beta: $install_beta - (no)");
			}				
		}
	
	
	}
	
	if ((($storenum == 5263) && ($hostname =~ /Reg2/)) || ($install_beta)) {
		# Reg2 in Kitchen Kettle is our beta code test station
		LogMsg(indent(1)."Checking for Beta code...");
		if (-d $eft_code_beta_source_dir) {
			LogMsg(indent(2)."Found Beta code!");
			$eft_code_source_dir = $eft_code_beta_source_dir;
		} else {
			LogMsg(indent(2)."Failed to find Beta Code!");
			$install_beta=0;
		}
	} 
	if ($install_beta) {
		LogMsg(indent(1)."Installation of Beta Driver is enabled");
	} else {
		LogMsg(indent(1)."Installation of Beta Driver is NOT enabled");
	}
	my %source_md5_hash=();
	my %code_md5_hash=();
	my $start_dir=cwd();
	my @new_file_list=();
 
	if (-d $eft_code_source_dir) {
		# Get a listing of the code in the source directory
		chdir($eft_code_source_dir);
		opendir(SOURCE,"$eft_code_source_dir");
		my @dir_list=readdir SOURCE;
		closedir SOURCE; 	
		foreach my $file (@dir_list) {
			next if ($file eq ".");
			next if ($file eq "..");
			my $file_path="${eft_code_source_dir}/${file}";
			if (-f $file_path) {
				if ($verbose) {
					LogMsg(indent(1)."Getting checksum for $file_path...");
				}
				my @md5_info=`md5 $file_path`;
				foreach my $m (@md5_info) {
					chomp($m);
					my @tmp=split(/\s+/,$m);
					my $md5sum=$tmp[0];
					$source_md5_hash{$file}=$md5sum;				
				}
			} 
		}
		# Do we have this code yet?
		if (-d $eft_code_dir) {
			opendir(CODE,"$eft_code_dir");
			my @dir_list=readdir CODE;
			closedir CODE; 
			###
			foreach my $file (@dir_list) {
				next if ($file eq ".");
				next if ($file eq "..");
				my $file_path="${eft_code_dir}/${file}";
				if (-f $file_path) {
					if ($verbose) {
						LogMsg(indent(1)."Getting checksum for $file_path...");
					}
					my @md5_info=`md5 "$file_path"`;
					foreach my $m (@md5_info) {
						chomp($m);
						my @tmp=split(/\s+/,$m);
						my $md5sum=$tmp[0];
						$code_md5_hash{$file}=$md5sum;				
					}
				} 
			}			
			###
			
		} else {
			mkdir($eft_code_dir);
		}
		unless (-d $eft_code_dir) {
			LogMsg("WARNING: Failed to locate $eft_code_dir");
			return 1;			
		}
		# Check that the code we have is current
		foreach my $file (sort keys(%source_md5_hash)) {
			my $need_to_copy = 0;
			unless (defined($code_md5_hash{$file})) {
				$need_to_copy=1;
			} else {
				# The code is there but is it the same code?
				unless ($source_md5_hash{$file} eq "$code_md5_hash{$file}") {
					$need_to_copy=1;
				}
			}
			if ($need_to_copy) {
				LogMsg("Copying $file to $eft_code_dir...");
				my $source_file_path="${eft_code_source_dir}/${file}";
				my $destination_file_path="${eft_code_dir}/${file}";
				copy($source_file_path,$destination_file_path);
				if (-f $destination_file_path) {
					LogMsg(indent(1)."Success");
					push(@new_file_list,$file);
				} else {
					LogMsg("ERROR: Failed to copy $file from $eft_code_source_dir to $eft_code_dir");
					return 1;
				}			
			} else {
				LogMsg("$file is current");
			}
		}
		foreach my $file (@new_file_list) {
			LogMsg("Installing $file");
			if (eft_code_install_process($file)) {
				LogMsg(indent(1)."Success");
			} else {
				LogMsg("WARNING: Failed to process installation of $file");
				return 1;
			}
		}
	} else {
		LogMsg("WARNING: Failed to locate $eft_code_source_dir");
		return 1;
	}
	return 0;
}

sub eft_code_install_process {
	LogMsg("EFT CODE install process");
	# If there is new EFT code downloaded, this will unzip the downloaded code 
	# This function places the EFT code in the final destination folders.  It does
	# not, however, install the .bat file or the local.ini file that actually make
	# the EFT code run. - kdg
	my $file=shift;
	my $zipfile='';
	
	my $ttveft_dir="";
	my $ttvlib_dir="";

	my $file_path="${eft_code_dir}/${file}";
	my $tool="c:/Progra~1/7-zip/7z.exe";
	my $new_code_found=0;
	if ($file =~ /zip$/i) {
		$zipfile=$file;
	} else {
		LogMsg("$file does not appear to be a zip file");
		return 0;
	}
	if (-f $tool) {
		if ($verbose) {
			LogMsg("Found $tool");
		}
	} else {
		LogMsg("Failed to locate $tool");
		return 0;
			
	}		
	if (-f $file_path) {
		# Process this file
		# First it needs to be unzipped
		chdir ($eft_code_dir);
		my $current_dir=cwd;
		if ($verbose) {
			LogMsg("Currently we are here ($current_dir)");
		}
		my $cmd="$tool x $zipfile -y ";	
		LogMsg("Unzipping: ($cmd)");
		`$cmd`;	

		# Create the version.ini files
		opendir(CODE,"$eft_code_dir");
		my @dir_list=readdir CODE;
		closedir CODE; 	
		foreach my $d (@dir_list) {
			next if ($d eq ".");
			next if ($d eq "..");
			 
			my $dir_path="${eft_code_dir}/$d";
			if (-d $dir_path) {
				if ($verbose) {
					LogMsg("Creating version.ini for $d");
				}
				if (($d =~ /TTVEFT/i) || ($d =~ /TTVLib/i)) {
					my $version_ini="${dir_path}/version.ini";
					if ($verbose) {
						LogMsg("$version_ini");
					}
					open(VERSION,">$version_ini");
					print VERSION "$d\n";
					close VERSION;
				}
			}
		}	 
	} else {
		LogMsg("WARNING: Failed to locate $file");
		# In this case, returning 0 indicates failure
		return 0;
	}
	# This following code will never be run currently since nothing updates $new_code_found
	# It returns to the previous function (eft_code_download) which in turn returns to the
	# function that called it (check_eft) and then goes on to eft_code_update from there.
	if ($new_code_found) {
		if (eft_code_update("force")) {
		} else {
			LogMsg("There were issues updating the EFT code...trying again.");
			if (eft_code_update("force")) {
				LogMsg("EFT Code updated");
			} else {			
				LogMsg("WARNING: There were issues updating the EFT code");
				return 0;
			}
		}
	}	
	return 1;
}


sub eft_code_update {
	LogMsg("### EFT CODE Update ###");
	# This function compares the EFT code on the register to the EFT code "installed" on the register and updates it accordingly
	# See if the destination folders exist. 
	my $force=shift;
	if ($force) {
		LogMsg(indent(1)."EFT CODE UPDATE called with force option");
	}
		
	my $ttveft_dir="";
	my $ttvlib_dir="";
	my $error_count=0;	 

	if (-d $eft_code_dir) {
		LogMsg("Reading $eft_code_dir");
    	opendir(CODE,"$eft_code_dir");
    	my @dir_list=readdir(CODE);
    	close CODE;   
		foreach my $d (@dir_list) {
			next if ($d eq ".");
			next if ($d eq "..");
			LogMsg("--> $d");
			if ($d =~ /TTVEFT/) {
				my $tmp_dir="${eft_code_dir}/$d";
				if ($ttveft_dir) {
					my $newer=find_newer("$tmp_dir","$ttveft_dir");
					if ("$newer" eq "$tmp_dir") {
						LogMsg("$d is newer");
						$ttveft_dir=$tmp_dir;
					}
				} else {
					$ttveft_dir=$tmp_dir;
				}				
			}
			if ($d =~ /TTVLib/) {				 
				my $tmp_dir="${eft_code_dir}/$d";
				if ($ttvlib_dir) {
					my $newer=find_newer("$tmp_dir","$ttvlib_dir");
					if ("$newer" eq "$tmp_dir") {
						LogMsg("$d is newer");
						$ttvlib_dir=$tmp_dir;
					}
				} else {
					$ttvlib_dir=$tmp_dir;
				}					
			}	
		}	
	} else {
		LogMsg("Returning...");
		return 0;
	}		
 
	for my $dir ("$eft_destination_dir","$eftlib_destination_dir") {
		if (-d $dir) {
			if ($verbose) {
				LogMsg("$dir exists");
			}
		} else {
			mkdir ($dir);
			if (-d $dir) {
				LogMsg("$dir created");
			} else {
				LogMsg("ERROR: Failed to create $dir - Returning...");
				return 0;
			}					
		}
	}
	unless ($force) {
		LogMsg("Checking versions...");
		# Unless the force parameter was passed, just check the version.ini file
		my $eft_version_ini_alt="${ttveft_dir}/version.ini";
		my $lib_version_ini_alt="${ttvlib_dir}/version.ini";
		my $eft_installed_version="${eft_destination_dir}/version.ini";
		my $lib_installed_version="${eftlib_destination_dir}/version.ini";	
		my $match=1;
		if ((-f $eft_version_ini_alt) && (-f $eft_installed_version)) {
 
			open(FILE,"$eft_version_ini_alt");
			my @tmp=(<FILE>);
			close FILE;
			my $eft_ver='';
			my $eft_installed_ver='';
			foreach my $t (@tmp) {
				chomp($t);
				$eft_ver=$t;
			}

			$eft_installed_ver=get_eft_installed_version("EFT");	
			if ("$eft_ver" eq "$eft_installed_ver") {
				LogMsg("The TTVEFT code is current: $eft_ver");
			} else {
				$match=0;
			}			
		} else {
			$match=0;
		}
		if ((-f $lib_version_ini_alt) && (-f $lib_installed_version)) {
			open(FILE,"$lib_version_ini_alt");
			my @tmp=(<FILE>);
			close FILE;
			my $lib_ver='';
			my $lib_installed_ver='';
			foreach my $t (@tmp) {
				chomp($t);
				$lib_ver=$t;
			}
		
			$lib_installed_ver=get_eft_installed_version("LIB");				
			if ("$lib_ver" eq "$lib_installed_ver") {
				LogMsg("The TTVLib code is current: $lib_installed_ver");
			} else {
				$match=0;
			}			
		} else {
			$match=0;
		}		
		if ($match) {
			# The code appears to be the same version	
			LogMsg("Code appears to be the same version - returning");
			return 1;
		}
	}	
	if ($force) {
		LogMsg("Forcing the installation to $eft_destination_dir");
	} else {
		if ($current_hour < $cuttoff_hour) { 
		LogMsg("Continuing the installation to $eft_destination_dir");
		} else {
			LogMsg("Install canceled because it is too late in the day - returning");
			return 1;
		}
	} 
	# Copy in the contents of directories
	# Is the EFT currently running?
	my $EFT="EFTDeviceDriver.exe";
	my @task_info=`tasklist`;
	my $eft_running=0;
	foreach my $t (@task_info) {
		chomp($t);
		if ($t =~ /$EFT/) {
			$eft_running=1;
		}
	}
	if ($eft_running) {
		# First check that the POS is not running:
	 
		LogMsg("Checking if the POS is running...");
		if (exit_pos("restart")) {
			LogMsg("WARNING: Cannot update EFT Code with POS still running");
			LogMsg("WARNING: Failed to terminate POS - returning");
			return 1;
		}
	} else {
		LogMsg("EFT is not currently running...safe to install");
	}
	#######################
	#	Installing EFT CODE
	#######################
	my $copy_count=0;
	my $copy_error_count=0;
	if (1) {
		if (-d $ttveft_dir) {	
			LogMsg("Copying from $ttveft_dir to $eft_destination_dir");
			# This is the ttveft code that should be installed
			opendir(CODE,"$ttveft_dir");
			my @dir_list=readdir(CODE);
			close CODE;   
			foreach my $f (@dir_list) {
				next if ($f eq ".");
				next if ($f eq "..");
				my $source_file="${ttveft_dir}/$f";
				my $destination_file="${eft_destination_dir}/$f";
				if ($f eq "TTVGateway.ini") {
					if (-f $destination_file) {
						# Save a copy of the original .ini file
						my $master=$f."master";
						my $destination_file_copy="${eft_destination_dir}/$master";
						if (-f $destination_file_copy) {
							unlink $destination_file_copy;
						}
						copy($source_file,$destination_file_copy);
					}
					# Don't overwrite this if it exists
					next if (-f $destination_file);
				}
				if ($verbose) {
					print "Copying $f to $eft_destination_dir\n";
				}	
			
				my $rc=safe_copy($source_file,$destination_file);
				if ($rc) {
					if ($rc == 1) {
						$copy_count++;
					} else {
						if ($verbose) {
							LogMsg(indent(1)."No need to copy");
						}						
					}
				} else {
					LogMsg("ERROR: Failed to copy $f to $eft_destination_dir");
					$copy_error_count++;					
				}			
			}		
		} else {
			LogMsg("ERROR: Failed to locate $ttveft_dir - returning");
			return 0;
		}	
		if ($copy_count) {
			LogMsg("Copied $copy_count files to $eft_destination_dir");
		
			# Code was updated - need to run through the ini_bat install
			LogMsg(indent(1)."Enabling eft_ini_bat installation");			
			$update_eft_ini=1;
		}
		if ($copy_error_count) {
			LogMsg("WARNING: There were $copy_error_count issues copying files to $eft_destination_dir");
		}
	}
	$error_count=$copy_error_count;
	$copy_count=0;
	$copy_error_count=0;
	if (-d $ttvlib_dir) {
		opendir(CODE,"$ttvlib_dir");
		my @dir_list=readdir(CODE);
		close CODE; 
		foreach my $f (@dir_list) {
			next if ($f eq ".");
			next if ($f eq "..");
			my $source_file="${ttvlib_dir}/$f";
			my $destination_file="${eftlib_destination_dir}/$f"; 
			if ($verbose) {
				LogMsg("Checking $f");				
			}		 			 
			my $rc=safe_copy($source_file,$destination_file);
			if ($rc) {
				if ($rc == 1) {
					$copy_count++;
					if ($verbose) {
						LogMsg(indent(1)."Copied $f to $eftlib_destination_dir");
					}
				} else {
					if ($verbose) {
						LogMsg(indent(1)."No need to copy");
					}
				}
			} else {
				LogMsg("ERROR: Failed to copy $f to $eftlib_destination_dir");
				$copy_error_count++;					
			}				 
		}	
		$error_count=$copy_error_count;		
	} else {
		LogMsg("ERROR: Failed to locate $ttvlib_dir - returning");
		return 0;
	}	
	if ($copy_count) {
		LogMsg("Copied $copy_count files to $eftlib_destination_dir");
	}	
	if ($copy_error_count) {
		LogMsg("WARNING: There were $copy_error_count issues copying files to $eft_destination_dir");		
	}		
	if ($error_count) {	
		return 0;
	}
	return 1;
}

sub get_eft_installed_version {
	my $request=shift;
	my $eft_installed_version="${eft_destination_dir}/version.ini";
	my $lib_installed_version="${eftlib_destination_dir}/version.ini";	
	my $file;
	my $return=0;
	if ($request eq "EFT") {
		$file=$eft_installed_version;
	} elsif ($request eq "LIB") {
		$file=$lib_installed_version;
	} else {
		LogMsg("WARNING: Failed to determine request ($request)");
		$warning_count++;
		return 0;
	}
	open(FILE,"$file");
	my @tmp=(<FILE>);
	close FILE;
	
	foreach my $t (@tmp) {
		chomp($t);
		$return=$t;
	}		
	return $return;

}

sub find_newer {
	my $var1=shift;
	my $var2=shift;
	if ("$var1" eq "$var2") {
		return 0;
	}	
	# Determine if this is newer code or older
	my @tmp=split(/-V/,$var1);
	my $version1 = $tmp[-1];
	@tmp=split(/-V/,$var2);
	my $version2 = $tmp[-1];		
	my @tmp1=split(/\./,$version1);
	my @tmp2=split(/\./,$version2);
	my $ver1="$tmp1[0]"."$tmp1[1]"."$tmp1[2]"."$tmp1[3]";
	my $ver2="$tmp2[0]"."$tmp2[1]"."$tmp2[2]"."$tmp2[3]";
 
	LogMsg("Comparing (@tmp1) ($ver1) with (@tmp2) ($ver2)");
	if ($ver1 > $ver2) {
		return $var1;
	}
	if ($ver2 > $ver1) {
		return $var2;
	}	
	return 0;
	if ((($tmp1[0] > $tmp2[0]) || ($tmp1[0] == $tmp2[0])) &&
		(($tmp1[1] > $tmp2[1]) || ($tmp1[1] == $tmp2[1])) &&
		(($tmp1[2] > $tmp2[2]) || ($tmp1[2] == $tmp2[2])) &&
		(($tmp1[3] > $tmp2[3]) || ($tmp1[3] == $tmp2[3]))) {
		LogMsg("Returning $var1");
		return $var1;
	} else {
		LogMsg("Returning $var2");
		return $var2;
	}
	
}

sub safe_copy {
	my $source=shift;
	my $destination=shift;
	# Return 0 if copy failed
	# Return 1 if copy successful
	# Return 2 if no need to copy
	my $need_to_copy=0;
	if (-f $source) {
		if (-f $destination) {
			my @destination_info=`md5 "$destination"`;
			my @source_info=`md5 "$source"`; 
			my $destination_sum;
			my $source_sum;
			foreach my $m (@destination_info) {
				chomp($m);
				my @tmp=split(/\s+/,$m);
				$destination_sum=$tmp[0];						
			}		
			foreach my $m (@source_info) {
				chomp($m);
				my @tmp=split(/\s+/,$m);
				$source_sum=$tmp[0];						
			}	
			unless ($source_sum eq $destination_sum) {
				$need_to_copy=1;
			}			
		} else {
			$need_to_copy=1;
		}		
	} else {
		return 0;
	}
	if ($need_to_copy) {
		if (copy("$source","$destination")) {
			# Successful copy
			return 1;
		} else {
			# Had a problem copying		
			sleep 2;
			if (-f $destination) {
				if ($verbose) {
					print "Deleting $destination...\n";
				}					
				unlink $destination;
			}
			if (-f $destination) {
				# Had a problem deleting
				return 0;
			}
			if (copy("$source","$destination")) { 
				return 1;
			} else {	
				return 0;
			}			
		}	
	}
	# No need to copy
	return 2;
 	
}

sub check_MxReboot_install {
	return 0; # disabled 2016-02-19 - kdg	
}

sub update_eft_ini {
	# This does not install the ini but will update it if necessary

	# This function should check that the TxnNumber is correct.
	# If we need to do other things in the future, we will add them later.
	# For now, just keep it simple.
	LogMsg("Update EFT ini file...");
 	
	my $warns=0;		
	
	# Get the ini fields
	my @current_data;
	my @new_data;
	my $need_to_write_new=0;
	if (-f $eft_ini) {
		LogMsg("Reading $eft_ini...");
		open(CURRENT,"$eft_ini");
		@current_data=(<CURRENT>);
		close CURRENT;
		my $insert=0;
		for (my $x=0; $x<=$#current_data; $x++) {
			my $current=$current_data[$x];
			my $next=$current_data[($x + 1)];
			chomp($current);
			my @tmp=split(/=/,$current);
			my $field=$tmp[0];
			my $value=$tmp[1];
			# Here we check that the TxnNumber follows the Lane
			if ($field =~ /Lane/) {
				unless ($next =~ /^TxnNumber/) {
					# Need to insert the TxnNumber
				
					my $lane = $value;
					$lane=~ s/\D//g;
					while ($lane =~ /^0/) {
						$lane =~ s/^0//;
					}
					my $txnNumber = "$lane" . "003";	# Construct a txnnumber based on the lane number
					$insert="TxnNumber=$txnNumber";
					LogMsg(indent(1)."Need to add: ($insert)");
					$need_to_write_new=1;					
				}
			}
			
			push(@new_data,$current);
			if ($insert) {
				push(@new_data,$insert);
				$insert=0;
			}
		}	
	} else {
		LogMsg("WARNING: Failed to locate $eft_ini");
		$warns++;
	}
	if ($need_to_write_new) {
		LogMsg(indent(1)."Writing new data to $eft_ini");
		open(INI,">$eft_ini");
		foreach my $n (@new_data) {
			print INI "$n\n";
		}
		close INI;
	} else {
		LogMsg(indent(1)."No need to write new data");
	}
	LogMsg("Finished updating EFT ini file");
	return $warns;	
}

sub eft_ini_bat_install {

	unless ($enable_eft_ini_bat_install)  {
		LogMsg("Installing EFT ini & bat file is not currently enabled");
		LogMsg("Run with both the -eft and the -enable options to install EFT fully.");
		return 0;
	}	
	
	# This function will install and update the ini and bat files needed to operate the VeriFone terminal.
	# This is the final stage of the EFT code installation.  Once this is executed, then the terminal will
	# run the EFT Device Driver.  Everything up to this point can be run without the terminal actually
	# running the EFT code.  Changing the local.ini and launching the POS via the aPOSRun.bat file is what
	# brings the EFT code into play. - kdg
	LogMsg("### EFT INI BAT install ###");
	my $warns=0;
	my $zipfile="EFT_INI_BAT.zip";
	my $install_dir = "C:/temp/install";
	my $eft_ini_bat_dir = "${install_dir}/${zipfile}";
	my $eft_ini_bat = "${eft_ini_bat_dir}/${zipfile}";
	my $eft_ini_bat_source = "//Edna/temp/install/EFT_INI_BAT/${zipfile}";
	my $tool="c:/Progra~1/7-zip/7z.exe";	
	my $need_to_copy=0;	
	my @old_app_list=(
		"c:/Documents and Settings/Villages/Start Menu/Programs/Startup/Point of Sale.lnk",		
		"C:/Documents and Settings/Villages/Desktop/Point of Sale.lnk"
	);
	
	my %file_hash=(		 
		'C:/Program Files/SAP/Retail Systems/Point of Sale/parm' => 'local.ini',
		'C:/Documents and Settings/Villages/Desktop' => 'POS w EFT.lnk',
		'c:/Documents and Settings/Villages/Start Menu/Programs/Startup' => 'POS w EFT.lnk', 
		'C:/Program Files/SAP/Retail Systems/Point of Sale' => 'aPOSRun.bat',
	);
 
	if (-f $tool) {
		if ($verbose) {
			LogMsg("Found $tool");
		}
	} else {
		LogMsg("ERROR: Failed to locate $tool");
		return 1;
			
	}			
	unless (-d $install_dir) {
		mkdir($install_dir);
	}
	unless (-d $install_dir) {
		LogMsg("ERROR: Failed to find $install_dir");
		return 1;
	}
	unless (-d $eft_ini_bat_dir) {
		mkdir($eft_ini_bat_dir);
	}
	unless (-d $eft_ini_bat_dir) {
		LogMsg("ERROR: Failed to find $eft_ini_bat_dir");
		return 1;
	}	
	# If Edna thinks EFT has been installed but the register does not,
	# we need to copy.
	if ($EFT_status eq "ACTIVE") {
		if ($found_eft_status ne "ACTIVE") {
			LogMsg("Edna says EFT is Active but found it not active.");
			LogMsg("Need to install...");
			$need_to_copy=1;
		}		
	}
	if (-f $eft_ini_bat_source) {	
		if (-f $eft_ini_bat) {
			my @source_info=`md5 "$eft_ini_bat_source"`; 		
			my @destination_info=`md5 "$eft_ini_bat"`; 
			my $destination_sum;
			my $source_sum;
			foreach my $m (@destination_info) {
				chomp($m);
				my @tmp=split(/\s+/,$m);
				$destination_sum=$tmp[0];						
			}		 
			foreach my $m (@source_info) {
				chomp($m);
				my @tmp=split(/\s+/,$m);
				$source_sum=$tmp[0];						
			}	
			if ($source_sum eq $destination_sum) {
				if ($verbose) {
					LogMsg("$eft_ini_bat_source is the same as $eft_ini_bat");
				}
			} else {
				$need_to_copy=1;
			}	
		} else {
			$need_to_copy=1;
		}
	} else {
		LogMsg("$eft_ini_bat_source was not found");
	}
	if ($need_to_copy) {
		if (copy("$eft_ini_bat_source","$eft_ini_bat")) {
			# Successful copy
			LogMsg("Successful copy");
		} else {
			# Had a problem copying		
			LogMsg("ERROR Failed to copy $eft_ini_bat_source");
			return 1; 			
		}	
		# Unzip the files
		chdir ($eft_ini_bat_dir);
		my $current_dir=cwd;
		if ($verbose) {
			LogMsg("Currently we are here ($current_dir)");
		}
		my $cmd="$tool x $zipfile -y ";	
		LogMsg("Unzipping: ($cmd)");
		`$cmd`;	

		# Now install each file as needed
		foreach my $destination (sort keys(%file_hash)) {
			my $need_to_install=0;
			my $file=$file_hash{$destination};
			my $source="${eft_ini_bat_dir}/$file";
			if (-f $destination) {
				my @source_info=`md5 "$source"`; 		
				my @destination_info=`md5 "$destination"`; 
				my $destination_sum;
				my $source_sum;
				foreach my $m (@destination_info) {
					chomp($m);
					my @tmp=split(/\s+/,$m);
					$destination_sum=$tmp[0];						
				}		 
				foreach my $m (@source_info) {
					chomp($m);
					my @tmp=split(/\s+/,$m);
					$source_sum=$tmp[0];						
				}	
				unless ($source_sum eq $destination_sum) {
					$need_to_install=1;
				}	
			} else {
				$need_to_install=1;
			}	
			if ($need_to_install) {
				LogMsg("Installing $file...");
				if (copy ($source,$destination)) {
					LogMsg(indent(1)."Copy successful");
				} else {
					LogMsg(indent(1)."ERROR: Failed to copy $file");
					return 1;
				}
			} else {
				LogMsg("Do not need to install $file");
			}			
		}
		# Need to remove the old POS link if it is still there
		foreach my $old_application (@old_app_list) {
			if (-f $old_application) {
				if (move($old_application,$eft_ini_bat_dir)) {
					LogMsg("Moved $old_application to $eft_ini_bat_dir");
				} else {
					LogMsg("ERROR: Failed to move $old_application to $eft_ini_bat_dir");
					$warns++;
				}
			}
		}
		# Exit the current POS
		exit_pos();
	} else {
		LogMsg("No need to copy");
	}
 
 	if ($eft_IP_exception_store_hash{$storenum}) {
		LogMsg(indent(1)."Not configuring TTVGateway.ini since store $storenum is excepted.");	 
	} elsif ($myip) {		
		configure_VeriFone_ini();	
	} else {
		LogMsg("ERROR: Failed to determine the IP address and so could not edit TTVGateway.ini");
		$warns++;			
	}
	
		
	return $warns;	
}

sub check_VeriFone_ini {
	return 0; # disabled 2016-02-19 - kdg	
}

sub configure_VeriFone_ini {

	my $eft_dir = $eft_destination_dir;
	my $warns=0;
	if ($myip) {
		# Determine the lane number
		my $lane=$hostname;		
		my $txnNumber=0;
		unless ($lane =~ /^reg/i) {			
			LogMsg(indent(1)."WARNING: Expected hostname to be Reg and found $lane");
			$warns++;
			return $warns;
		}
		if ($lane =~ /^regx/i) {
			LogMsg(indent(1)."WARNING: Expected hostname to be Reg followed by a number but found $lane");
			$warns++;
			return $warns;		
		}		
		$lane=~ s/\D//g;		
		$txnNumber = "$lane" . "003";	# Construct a txnnumber based on the lane number
		$lane=sprintf("%02d",$lane);		
		# Determine the VeriFone's IP address
		 
		my @tmp=split(/\./,$myip);
		my $third_part=$tmp[2];
		my $fourth_part=$tmp[3];
		# Increment by 3 to get the VeriFone's IP
		# or by 2 for some stores.
		if ($eft_plus2_store_hash{$storenum}) {
			$fourth_part=($fourth_part + 2);
		} elsif ($revised_network) {
			$fourth_part=($fourth_part + 5);
		} else {
			$fourth_part=($fourth_part + 3);
		}
		my $VeriFone_IP="192.168"."."."$third_part"."."."$fourth_part";
		if ($storenum) {
			# Configure the TTVGateway.ini

			if (-d $eft_dir) {
				if (-f $eft_ini) {
					LogMsg("Overwriting $eft_ini...");
				}
				open(INI,">$eft_ini");
				print INI "Verifone Parameters\n";
				print INI "TerminalAddr=$VeriFone_IP\n";
				print INI "StoreNumber=$storenum\n";
				print INI "Lane=$lane\n";
				print INI "TxnNumber=$txnNumber\n";
				print INI "DebugFlag=FALSE\n";
				close INI;	
				print "Verifone Parameters\n";
				print "TerminalAddr=$VeriFone_IP\n";
				print "StoreNumber=$storenum\n";
				print "Lane=$lane\n";
				print "TxnNumber=$txnNumber\n";
				print "DebugFlag=FALSE\n";				
			} else {
				LogMsg("ERROR: Failed to locate $eft_dir");
				$warns++;	
			}
		} else {
			LogMsg("ERROR: Failed to determine storenum and so could not edit TTVGateway.ini");
			$warns++;		
		}
	}
	return $warns;
}
 
sub update_flag_log {
	
	#LogMsg("Update flag log");
	# Keep a log file of when restart flags were set
	my $label=shift;
	my $warns=0;
	my $flag_log="c:/temp/scripts/flag_log.txt";
	my $counter=0;
	my $week_counter=0;
	my $week_seconds=604800;	# Number of Seconds in a week
	my $month_counter=0;
	my $month_seconds=2592000;	# Number of seconds in 30 days
	if ($label) {
		# First read the log and see how many times this entry has been sent
		if (-f $flag_log) {
			#LogMsg("Opening $flag_log...");
			open(LOG,"$flag_log");
			my @flag_info=(<LOG>);
			close LOG;
			foreach my $f (@flag_info) {
				chomp($f);
				if ($f =~ /$label/) {
					$counter++;
					my @tmp=split(/\s+/,$f);
					my $seconds=$tmp[0];
					# How long ago was this label entered?	 
					my $age=($timeseconds - $seconds);					
					if ($age < $week_seconds) {
						$week_counter++;
					}
					if ($age < $month_seconds) {
						$month_counter++;
					}					
				}
			}
		}
		# Now enter this instance
		open(LOG,">>$flag_log");
		print LOG "$timeseconds $label\n";
		close LOG;
	}
	if ($week_counter > 2) {
		LogMsg(indent(1)."WARNING: This flag has been set $week_counter times in the past week");
		$warns++;
	}
	if ($month_counter > 4) {
		LogMsg(indent(1)."WARNING: This flag has been set $month_counter times in the past 30 days");
		$warns++;
	}	
	LogMsg(indent(1)."This flag has been set $counter times in recorded history.");
	return $warns;
} 

sub get_register_info {
	my $request=shift;
	# Get the requested information about the register 
	my $answer='';
	if (-f $local_system_info) {
		open(LSI,"$local_system_info");
		my @lsi_tmp=(<LSI>);
		close LSI;
		foreach my $l (@lsi_tmp) {
			if ($l =~ /$request/) {
				my @ltmp=split(/=/,$l);
				chomp($answer=$ltmp[1]);
			}
		}
	}	
	return $answer;
}

sub check_register_server {
	# Is this register working as a register and a server?
	# this is important to know so that we do not disable
	# certain services
	my $am_server=0;
	my $service_count=0;
 
	my %service_hash=(
		'dbsrv12.exe'=>'ASANYs_SQL_ENG',
		'xps.exe'=>'Xpress Server',
		'Apache.exe'=>'Apache',
	);
	foreach my $ser (sort keys(%service_hash)) {
		my $service=$service_hash{$ser};
		if (checkPID($ser)) {
			LogMsg(indent(1)."Service process $ser is running");
		}		
		if (check_enabled_service($service, "DEMAND_START")) {	
			LogMsg(indent(1)."Service $service is enabled");
			$service_count++;			
		} else {
			if ($verbose) {
				LogMsg(indent(2)."Service $service is NOT enabled");
			}
		}		
	}
		
	if ($service_count > 2) {
		# Chances are, it is a server
		$am_server=1;
	}
	
	if ($am_server) {
		if (ping_test("Edna")) {
			LogMsg("WARNING: Register is configured as a server but a ping successful to Edna");
			$warning_count++;
		} else {
			LogMsg("This system is running as a register/server");
			LogMsg("NOTICE: NOT able to ping Edna");		
		}
	}
	return $am_server;
}

sub check_bkoff_ini {
		
	LogMsg("Checking bkoff.ini");
	
	my $file="C:/Program Files/SAP/Retail Systems/Store Manager/data/bkoff.ini";
	my $source_reg="C:/temp/scripts/bkoff_reg.ini";
	my $source_srv="C:/temp/scripts/bkoff_srv.ini";
	my @file_info=();
	my $line="";
	my $warns=0;
	my $bkoff_server='unknown';
	my $expected_server="edna";
	my $expected_server_alt="undefined";
	if (-e $file) {
		open(BKOFF,"$file");
		@file_info=(<BKOFF>);
		close BKOFF;
		foreach $line (@file_info) {
			if ($line =~ /^server=/) {
				my @tmp=split(/=/,$line);
				$bkoff_server=lc($tmp[1]);
				$bkoff_server =~ s/\s//g;
			} 			
		}		
	} else {
		LogMsg("WARNING: Failed to locate $file");
		$warns++;		
	}
	if ($bkoff_server eq "unknown") {
		LogMsg("WARNING: Failed to determine server definition in bkoff.ini");
		$warns++;
	} else {
		if ($register_server) {
			$expected_server='127.0.0.1';
			$expected_server_alt="$hostname";
		}
		if ($bkoff_server eq $expected_server) {
			LogMsg("bkoff server correct: $bkoff_server");
		} elsif ($register_server) {
			if ($expected_server_alt eq $expected_server) {
				LogMsg("bkoff server accepted: $expected_server_alt");
			} else {
				LogMsg("WARNING: bkoff server appears to be incorrect: ($bkoff_server) expected: ($expected_server)");
				$warns++;
			}
		} else {
			if (-f $source_reg) {
				LogMsg("Replacing bkoff.ini file...");
				copy($source_reg,$file);
			} else {
				LogMsg("WARNING: Failed to locate $source_reg");
				$warns++;					
			}
			LogMsg("WARNING: bkoff server appears to be incorrect: ($bkoff_server) expected: ($expected_server)");
			$warns++;		
		}
	}
	if ($warns) {
		LogMsg(indent(1)."WARNING: $warns issues checking bkoff.ini");		  		
	} else {
		LogMsg(indent(1)."No issues found with bkoff.ini configuration.");     
	}
	return $warns;
}

sub getStoreNum_from_Registry {
    # Get the store number from what is configured in the Xpress Server
    my $cmd="reg query \"HKLM\\SYSTEM\\CurrentControlSet\\Services\\Xpress Server\" /v XPS_STORENUM 2> /temp/junk";    
    my @info=`$cmd`;
    my $return="unknown";
    unless ($?) {
        foreach my $i (@info) {              
            if ($i =~ /XPS_STORENUM/) {
                chomp($i);
                my @tmp=split(/\s+/,$i);
                $return=$tmp[-1];                     
            }
        }        
    }    
    return $return;
}


sub check_scheduled_tasks_win7 {
	if  ($OS eq "XP") {
		# This function does not work on this system
		return;			
	}	
 
	LogMsg("Checking Scheduled Tasks win7...");
	# Sometimes "AT" tasks get added but never get completed. This checks that task
	# This function also disables the task associated with updating to Windows 10.
	my $schtaskcmd="schtasks /query /fo list /v";
	my @schtask_info=`$schtaskcmd`;
	my $found_target=0;
	my $found_target2=0;
	my $found_at_task=0;
	my $found_gwx_task=0;
	my $task;
	my $status;
	my $warns=0;
	my @at_list;
	foreach my $s (@schtask_info) {
		chomp($s);		
		if ($found_target) {
			if ($s =~ /Next Run/) {				
				my @tmp=split(/\s+/,$s);
				my $date=$tmp[3];	
				if ($OS eq "XP") {
					$date=$tmp[4];
				}						
				LogMsg(indent(1)."Task: $task Next Run: $date");
				if ($date eq "N/A") {
					LogMsg(indent(2)."Will purge $task");
					push(@at_list,$task);
				}
			}
			if ($s =~ /Status/i) {		
				
				my @tmp=split(/\s+/,$s);
				shift @tmp;				
				my $status = join(' ',@tmp);
				unless ($status) {
					$status = "unknown";
					if ($OS eq "XP") {
						# In XP, no status seems to be ok.
						$status = "Ready";
					}
				}				
				unless ($status =~ /Ready/i) {
					LogMsg(indent(1)."WARNING: Status for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}				
			}	
			if ($s =~ /Last Run/) {					
				#LogMsg(indent(1)."$s");				
				my @tmp=split(/\s+/,$s);								
				my $date=$tmp[3];
				if ($OS eq "XP") {
					$date=$tmp[4];
				}	
				if ($date eq "N/A") {
					LogMsg(indent(1)."WARNING: $task has never run");
					$warns++;				
				} else {
					@tmp=split(/\//,$date);												
					my $day=$tmp[1];
					my $month=$tmp[0];
					my $local_time_month=$month;
					$local_time_month--;
					my $year=$tmp[2];	
					if ($verbose) {
						print "Date: ($date) day ($day) month ($month) year ($year)\n";
					}
					my $lastrun = timelocal(0,0,0,$day,$local_time_month,$year);
					my $current_timeseconds=time();						
					my $last_run_age = (($current_timeseconds - $lastrun) / 86400);
					if ($last_run_age > 7) {
						LogMsg(indent(1)."WARNING: $task has not run since $year-$month-$day");
						$warns++;
						push(@at_list,$task);
					} else {
						LogMsg(indent(1)."Task: $task Last Run: $date");
					}	
				}
			}			
			if ($s =~ /Scheduled Task State: /i) {				
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];				
				unless ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}					
			}						
		}
		if ($found_target2) {	
			if ($s =~ /Scheduled Task State: /i) {								
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];						
				if ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$warns++;
					$found_gwx_task=1;					
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");					
				}					
			}						
		}		
		if (($s =~ /At\d/i) && ($s =~ /TaskName:/)) {
			$found_target=1;						
			my @tmp=split(/\\/,$s);
			$task=$tmp[1];
			if ($OS eq "XP"){
				@tmp=split(/:/,$s);				
				$task=$tmp[1];				
				$task =~ s/\s//g;				
			}			
					
			LogMsg("Found task: $task");			
			$found_at_task=1;
		
		}
		if (($s =~ /refreshgwxconfigandcontent$/) && ($s =~ /TaskName:/)) {
			$found_target2=1;						
			my @tmp=split(/\\/,$s);
			$task=$tmp[-1];						
			LogMsg("Found task: $task");								
		}		

		if ($s =~ /Hostname/i) {
			$found_target=0;
			$found_target2=0;			
		}
	}
	if ($found_at_task) {
		LogMsg("Found AT tasks");
		#LogMsg("ISSUE: Found AT tasks");
		#$warns++;
		foreach my $a (@at_list) {
			LogMsg("Deleting task $a");
			my $cmd="schtasks /delete /tn $a /f";
			system($cmd);			
		}
	}
	if ($found_gwx_task) {
		LogMsg("Found GWX tasks"); 
		LogMsg("Disabling task ");
		my $cmd="schtasks /change /disable /tn \\Microsoft\\Windows\\Setup\\gwx\\refreshgwxconfigandcontent";
		 
		system($cmd);			

	}	
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking scheduled tasks");
		$warning_count++;
	} else {
		LogMsg("Found no issues checking scheduled tasks");
	}
}

sub get_temp {
	my $cmd="wmic /namespace:\\\\root\\wmi PATH MSAcpi_ThermalZoneTemperature get CurrentTemperature";
	my @temp_info=`$cmd`;
	my $temp=0;
	my $temp_critical=0;
	my $found_key=0;
	foreach my $t (@temp_info) {
		chomp($t);	
		$t =~ s/\s+//g;
		next unless ($t);
 
		if ($t =~ /\d/){
			# Assign the first number to temp and the second to temp critical
			if ($temp) {
				$temp_critical=(($t / 10) - 273.15);
			} else {
				$temp=(($t / 10) - 273.15);
			}
			
		}		
	}
	if ($temp) {
		LogMsg("Temperature: $temp C");
	}
	if ($temp_critical) {
		LogMsg("Critical Temperature: $temp_critical C");
	}	
}

sub check_gwx {
	my $warns=0;
	# Check installed updates
	LogMsg(indent(1)."Checking installed updates for gwx...");
	my $cmd="wmic qfe get";
	my @updates_info=`$cmd`;
	my $found_KB3035583=0;
	foreach my $u (@updates_info) {
		chomp($u);
		if ($u =~ /KB3035583/) {
			$found_KB3035583=1;
		}
	}
	if ($found_KB3035583) {					
		if (gwx_uninstall()) {
			LogMsg("WARNING: Found KB3035583 (GWX) to be installed");
			$warns++;
		}
	}
	return $warns;
}

sub gwx_uninstall {
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Microsoft\\Windows\\Gwx\"";				
		
	my @info=`$cmd`;  	
	 
	my $DisableGwx=0;
	my $warns=0;
 
	LogMsg("Checking if DisableGwx configured...");
	foreach my $i (@info) {
		chomp($i);  			 
		if ($i =~ /DisableGwx/) {				
			my @tmp=split(/\s+/,$i);
			LogMsg("Found DisableGwx: $tmp[-1]");
			if ($tmp[-1] eq "0x1") {
				$DisableGwx=1;			
			}				
		} 			
	}	
	unless ($DisableGwx) {
		LogMsg("WARNING: Disabling GWX notification");
		$cmd="reg add \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Microsoft\\Windows\\Gwx\" /v DisableGwx /t REG_DWORD /d 1 /f";				
		system("$cmd");
		$warns++;
	}
	return $warns;
}

sub get_arch {
	my $cmd="wmic os get OSArchitecture";
	my @info=`$cmd`;
	my $arch=0;
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /64-bit/) {
			$arch="64-bit";
		}
		if ($i =~ /32-bit/) {
			$arch="32-bit";
		}				
	}
	return $arch;
}
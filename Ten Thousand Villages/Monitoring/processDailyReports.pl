#!/usr/bin/perl -w

## processDailyReports.pl
## Process the backup reports returned by the polling process.
## 2008-02-27 - Added error_count inside checkfilesize - kdg
## 2008-02-28 - Revised processing backup_report.bck
## 2008-03-10 - Removing changing name of file when copying to processed dir - kdg
## 2008-03-12 - Formatting and correct some error counting - kdg
## 2008-03-14 - Changed name of script - kdg
## 2008-03-17 - Changed from Utilities to MlinkUtilities - kdg
## 2008-03-19 - Changes to display error when report not found - kdg
## 2008-03-19 - Cleanup unused variables - kdg
## 2008-03-26 - Added brief option -kdg
## 2008-03-27 - Formating revisions - kdg
## 2008-04-04 - Moving away from excluding store numbers toward expecting store numbers - kdg
## 2008-04-17 - Fixes to error counting when verifying mlink backup - kdg
## 2008-04-22 - Also get ".rpt" reports -kdg
## 2008-04-25 - Added Evanston (2035), Pasadena (3667), Souderton (7044), Orlando (3718), Salem (2763) - kdg
## 2008-04-29 - Adjustments to include monitor reports.  Changed name from processBackupReport - kdg
## 2008-05-13 - Using polled_store_list - kdg
## 2008-05-14 - Updated exclusions - kdg
## 2008-05-29 - Added view_current() - kdg
## 2008-06-02 - Checking date of the report - kdg
## 2008-06-04 - Adding error number to report. - kdg
## 2008-07-02 - Revised significantly to create daily_report in the morning and to only check mlink in the afternoon. - kdg
## 2008-07-08 - 1.3.1 - Print out ip of store if you cannot connect - kdg
## 2008-07-10 - 1.3.2 - Added monitor and backup only arguments.  Added store count to subject line - kdg
## 2008-07-17 - 1.3.3 - Found an errant debug setting which was preventing email from being sent.  Removed it - kdg
## 2008-07-30 - 1.4.0 - Adding store number and contract/company to report (incorporating storelist.txt).  Added "use strict". Removed brief.  Updated help - kdg
## 2008-07-30 - 1.4.1 - Don't show cs_error_count for mlink backup report - kdg
## 2008-08-01 - 1.4.2 - Added nomail option.  Added company_store_ips to assist in determining if we cannot connect to a company store - kdg
## 2008-08-04 - 1.4.3 - Replace print statements with LogMsg except for debug mode.  Trimmed blank spaces from company store ip list. -kdg
## 2008-08-06 - 1.4.4 - Set debug in smtp to 0. -kdg
## 2008-09-05 - 1.4.5 - Using storelist for IP source.  Added process_po_report- kdg
## 2008-09-08 - 1.4.6 - Cleanup process_po.  Account for if the open pos file is not found.  Added --day argument - kdg
## 2008-09-09 - 1.4.7 - Added Jerry (jpl) to the mail list for the PO report - kdg
## 2008-09-09 - 1.4.8 - Added query_rd - kdg
## 2008-09-12 - 1.4.9 - Added --storeid option.  Account for receiving documents of 0. - kdg
## 2008-09-16 - 1.4.10 - Minor improvement in reporting if no open_pos file is found - kdg
## 2008-09-19 - 1.4.11 - Added Windows Update count to report - kdg
## 2008-09-23 - 1.4.12 - Updated date_label format to match monitor.pl -kdg
## 2008-09-24 - 1.4.13 - Adding stornum to error when report date is not as expected - kdg
## 2008-09-29 - 1.4.14 - Added jpl as Mailcc unless test mode - kdg
## 2008-09-30 - 1.4.15 - Disabled PO's from --current.  They will be run separately. - kdg
## 2008-10-09 - 1.4.16 - PO's don't generate an error if there is no po file.  Send email only if there are errors. - kdg
## 2008-10-14 - 1.4.17 - Corrected configration so that po_only option sends email only if there is an error. - kdg
## 2008-10-21 - 1.4.18 - Adjusted so mlink report is only mailed if there were errors. - kdg
## 2008-10-22 - 1.4.19 - Added a blank line between stores in email - kdg
## 2008-10-28 - 1.5.0 - Added ability to include or exluce stores in current_view.  Updaed usage - kdg
## 2008-11-13 - 1.5.1 - Adding known_issues recording - kdg
## 2008-11-19 - 1.5.2 - Added issue count at the end of monitor report - kdg
## 2008-11-20 - 1.5.3 - Changed to using storeissues.txt rather than knownissues.txt.  Some adjustments to update_known_issues  - kdg
## 2008-11-21 - 1.5.4 - More work on the update_known_issues function - kdg
## 2008-11-28 - 1.5.5 - Having problems getting update_known_issues to run from cron it appears - kdg
## 2008-12-11 - 1.6.0 - Revised to write issues to database - kdg
## 2008-12-30 - 1.6.1 - Added listing of all open issues at the end of the monitor report - kdg
## 2008-12-31 - 1.6.2 - Added a counter to issue report.  Added a check that the db query was successful - kdg
## 2009-01-27 - 1.6.3 - Corrections to update_known_issues_db - kdg
## 2009-02-09 - 1.7.0 - Added @misc_errors and checking build version - kdg
## 2009-03-03 - 1.7.1 - Corrections to account for adding IP info to monitor report - kdg
## 2009-03-27 - 1.7.2 - Revision for $msg when monitor passes - kdg
## 2009-04-06 - 1.7.3 - Added check for connectivity to store_issues database - kdg
## 2009-04-07 - 1.7.4 - Added $wired rather than calling "wired" - kdg
## 2009-04-16 - 1.7.5 - Small revision to look for ^Date: rather than Date: - kdg
## 2009-04-16 - 1.7.6 - Changed report_misc_errors to report_misc_info & added SysInfo - kdg
## 2009-04-17 - v1.7.7 - Added @Tenders - kdg
## 2009-04-21 - v1.7.8 - Revised system info reporting - kdg
## 2009-04-21 - v1.7.9 - Revised to check tenders against storelist - kdg
## 2009-04-22 - v1.7.10 - Small correction to checking tenders - kdg
## 2009-04-24 - v1.7.11 - Only push the SystemInfo string for monitor - kdg
## 2009-05-06 - v1.8.0 - Added check_ttvsftp1 - kdg
## 2009-05-07 - v1.8.1 - Created alternate check_ttvsftp1 to support different systems - kdg
## 2009-05-12 - v1.8.3 - Added store name to checking ttvsftp1 - kdg
## 2009-05-13 - v1.8.4 - Using storelist to double-check list of company stores - kdg
## 2009-05-18 - v1.8.5 - Added a retry if the script does not get content the first time with get($url) - kdg
## 2009-05-20 - v1.8.6 - Added more logging - kdg
## 2009-05-26 - v1.8.7 - Getting backup file name and size from a different part of the report - kdg
## 2009-06-10 - v1.8.8 - Added collection of Traffic Counter Info and moved misc reporting to just Friday - kdg
## 2009-06-16 - v1.8.9 - Corrected formating when reporting new issues - kdg
## 2009-06-17 - v1.8.10 - Report misc info every day
## 2009-06-29 - v1.8.11 - Revised to use exchange - kdg
## 2009-07-10 - v1.8.12 - Added check_ttvsftp1 as the SFTP methods are temporarily disabled - kdg
## 2009-07-17 - v1.8.13 - Re-enabled checking ttvsftp1 with sftp - kdg
## 2009-07-30 - v1.8.14 - Added ping test before trying to contact ttvsftp1 - kdg
## 2009-07-31 - v1.9.0 - Added creating "hard copy" today reports - kdg
## 2009-09-03 - v1.9.1 - Added backup method to the report - kdg
## 2009-09-18 - v1.9.2 - Corrections to finding date of report - kdg
## 2009-10-27 - v1.9.3 - Adding noInternet count.  Added @sftp_stores - kdg
## 2009-11-03 - v1.9.4 - Corrected a pattern matching issue in checking ttvsftp1.  Collect Traffic Sensor Version info - kdg
## 2009-12-07 - v1.9.5 - Using $mysqld rather than $wired.  Pointing to ttv-lamp1 now - kdg
## 2009-12-08 - v1.9.6 - Added UPS Data connection - kdg
## 2009-12-18 - v1.9.7 - process_po_report now updates the storeIssues database table - kdg
## 2009-12-21 - v1.9.8 - Added date_label to rpt.txt reports - kdg
## 2009-12-28 - v1.9.9 - Updated $sumInfo - kdg
## 2010-01-05 - v1.9.10 - Added $verbose - kdg
## 2010-01-15 - v1.9.11 - Added Report Date - kdg
## 2010-02-02 - v1.9.12 - Sent email  to me at home on weekends - kdg
## 2010-02-23 - v1.9.13 - Added status_only - kdg
## 2010-03-10 - v1.9.14 - Added prepend_summary - kdg
## 2010-03-11 - v1.9.15 - Increment and utilize count in prepend_summary - kdg
## 2010-06-18 - v1.10.0 - Added update_storeinfo_db - kdg
## 2010-07-12 - v1.10.1 - Corrected date_label for checking ttvsftp1 - kdg
## 2010-07-13 - v1.10.2 - More work on making date_label consistent - kdg
## 2010-09-02 - v1.10.3 - Removed single quote from issue when adding to database - kdg
## 2010-10-29 - v1.10.4 - Added support for 99 in view_current function - kdg
## 2010-11-01 - v1.10.5 - Skip store 99 for process_po_report - kdg
## 2010-11-10 - v1.10.6 - Updated to collect bank account - kdg
## 2010-11-15 - v1.10.7 - Removing mailcc2 & some work to better determine storenum - kdg
## 2010-11-16 - v1.10.8 - Corrected a bug associated with collecting back account - kdg
## 2010-11-30 - v1.11.0 - Put sftp to ttvsftp1 inside eval so it would not bomb out the script if it failed - kdg
## 2010-12-06 - v1.11.1 - Don't try ftp connection to ttvsftp1 if sftp fails.  Updated password - kdg
## 2011-01-30 - v1.11.2 - Added collection of serial number for Edna servers - kdg
## 2011-02-03 - v1.11.3 - Revised update_storeinfo_db function to account for connectivity stats kept in a new table - kdg
## 2011-02-11 - v1.12.0 - Revisions to check updates on ttv_sftp1 - kdg
## 2011-02-14 - v1.12.1 - Included NOTICE in view_current reports - kdg
## 2011-02-26 - v1.12.2 - Corrected ttv_sftp1 for mlink - kdg
## 2011-03-03 - v1.12.3 - Adjustments to accomodate store backups now occuring at EOD. - kdg
## 2011-03-18 - v1.13.0 - Revised to permit computer info and system info gathering to be more flexible - kdg
## 2011-03-21 - v1.13.1 - Added storenum to IP_hash for more accurate determination of store in view_current - kdg
## 2011-03-22 - v1.13.2 - More cleanup related to changes made to IP_hash - kdg
## 2011-03-23 - v1.13.3 - More minor cleanup - kdg
## 2011-03-26 - v1.13.4 - Corrections to provide connection to ttv-sftp1 from ttv-mlink - kdg
## 2011-03-27 - v1.13.5 - More cleanup for checking ttv-sftp1 - kdg
## 2011-03-28 - v1.13.6 - Disabled Report Date - kdg
## 2011-04-04 - v1.13.7 - Updates to add the collection of throughput and system stats into the database - kdg
## 2011-04-05 - v1.13.8 - Some work in update_storeinfo_db.  Added storeStats table - kdg
## 2011-05-07 - v1.13.9 - Fixed finding no internet count - kdg
## 2011-04-11 - v1.14.0 - Fixed getting backup file name.  Trimmed out lots of obsolete code - kdg
## 2011-06-01 - v1.14.1 - Work on update_storeinfo_db function to start using storeStats to store connection info - kdg
## 2011-07-25 - v1.14.2 - More work on collecting store stats.  Report can now list System Stats and this tool can record them - kdg
## 2011-08-17 - v1.14.3 - Added Al Rehman to the mailcc - kdg
## 2011-11-18 - v1.14.4 - Added @noted to collect basic sku fixes (BSF) - kdg
## 2011-12-28 - v1.14.5 - Added Al and Jerry to status reports - kdg
## 2012-04-10 - v1.14.6 - Re-enabled collecting of throughput stats which had been mistakenly disabled. - kdg
## 2012-07-02 - v1.14.7 - Added checking report date of monitor report - kdg
## 2012-07-17 - v1.15.0 - Added special_only and the misc report - kdg
## 2012-07-18 - v1.15.1 - An adjustment to misc report to not show store entry if there is no misc info for that store - kdg
## 2012-07-23 - v1.15.2 - Another adjustment to determining if there is anything on the misc report - kdg
## 2012-07-24 - v1.15.3 - Moved NOTICE: and BSF: to misc report.  Adjusted report date errors. - kdg
## 2012-08-01 - v1.15.4 - Put errors_label back into the subject line in send_email function - kdg
## 2012-08-07 - v1.15.5 - Added Mailcc for current_report - kdg
## 2012-08-26 - v1.15.6 - Added emailing of alerts - process_alerts - kdg
## 2012-08-28 - v1.15.7 - Some refinements to process_alerts - kdg
## 2012-09-10 - v1.15.8 - Enabled sending email to RSM and store manager - kdg
## 2012-09-13 - v1.15.9 - Modified send_email_note to send admin an email only if there is a NOTE - kdg
## 2012-09-17 - v1.15.10 - Added Jennifer Hoke Bentivogli - kdg
## 2012-09-19 - v1.15.11 - Checking that I can connect to Navision in process_alerts - kdg
## 2012-10-01 - v1.15.12 - Getting IP address for stores from rtblstores table instead of storelist.txt - kdg
## 2012-10-10 - v1.15.13 - Added ping to getting content from stores - kdg
## 2012-10-12 - v1.15.14 - Send Al the morning report - kdg
## 2012-10-26 - v1.15.15 - Added separate UPS report - kdg
## 2012-10-30 - v1.15.16 - Added handling of UPS notices - kdg
## 2012-11-12 - v1.15.17 - Limit desc to 30 characters before checking codes - kdg
## 2012-11-16 - v1.15.18 - I revised the codes table to auto-increment the codeId field and updated this script accordingly - kdg
## 2012-12-17 - v1.16.0 - Do not send UPS messages in monitor report.  Do not email misc and ups reports for status updates - kdg
## 2013-01-07 - v1.16.1 - Email status messages only if there are errors - kdg
## 2013-01-11 - v1.16.2 - Clear email_alert_messages before starting a new store - kdg
## 2013-01-13 - v1.16.3 - Clear alert_results before starting a new store - kdg
## 2013-02-13 - v1.16.4 - Revisions to include Kevin on email alerts - kdg
## 2013-03-05 - v1.16.5 - Filter rtblStore by StoreCode - kdg
## 2013-04-01 - v1.16.6 - Correction to alert_count - kdg
## 2013-04-02 - v1.16.7 - Modified parsing in view_current to handle both <br> and <br /> - kdg
## 2013-05-24 - v1.17.0 - Created separate emails for workstations and registers - kdg
## 2013-06-04 - v1.18.0 - Revision to stay connected to the database rather than to be connecting and disconnecting repeatedly - kdg
## 2013-06-14 - v1.18.1 - Showing if iDrive backup has failed - kdg
## 2013-08-16 - v1.18.2 - Added source_safe variable and pointed to villages.com - kdg
## 2013-08-28 - v1.18.3 - Added $expected_version_sap variable to support SAP POS 2.3 - kdg
## 2013-08-29 - v1.18.4 - Modified query_po to better match how PO's get truncated and renamed - kdg
## 2013-09-20 - v1.18.5 - Confirmed Karen Dooley's email address - kdg
## 2013-10-17 - v1.18.6 - Added Jon Barrick - kdg
## 2013-11-18 - v1.19.0 - Added $store_info_file hoping to be more robust if **** is down - kdg
## 2013-11-27 - v1.20.0 - Added email for MixMatch discounts - kdg
## 2013-11-29 - v1.20.1 - Correction to MixMatch to process by default - kdg
## 2013-12-01 - v1.20.2 - Corrections to handling Mailcc in send_email - kdg
## 2013-12-13 - v1.20.3 - Prepend email_messages_reg with total counts of warnings found - kdg
## 2013-12-16 - v1.20.4 - Corrections to collecting total counts.  Also, added alt_IP_hash - kdg
## 2013-12-26 - v1.20.5 - Refined parsing of Windows Dump count - kdg
## 2014-01-10 - v1.20.6 - Collecting counts of Windows dumps on Edna - kdg
## 2014-03-18 - v1.21.0 - Added edna_issue_watch_list - kdg
## 2014-03-26 - v1.21.1 - Updated sap expected build - kdg
## 2014-04-13 - v1.21.2 - Updated edna_issue_watch_list - kdg
## 2014-04-24 - v1.21.3 - Updated edna_issue_watch_list - kdg
## 2014-05-27 - v1.21.4 - Comparing date before recording throughput - kdg
## 2014-06-19 - v1.21.5 - Revised Important Issues - kdg
## 2014-08-19 - v1.21.6 - Send out a monitor email even if there are no errors - kdg
## 2014-09-22 - v1.22.0 - Added manager_email2 variable - kdg
## 2014-10-03 - v1.22.1 - Added rsm_covering_store_email_hash to map RSM by storeid - kdg
## 2014-10-06 - v1.22.2 - Revisions to rsm_covering_store_email_hash - kdg
## 2014-11-17 - v1.22.3 - Cleared the %rsm_covering_email_hash and %rsm_covering_store_email_hash variables. - kdg
## 2014-11-24 - v1.23.4 - No longer submitting po issues to the issues database - kdg
## 2015-01-09 - v1.23.5 - Updated with new IP address for The Wire - kdg
## 2015-03-03 - v1.23.6 - Added $stat2 for system stats - kdg
## 2015-03-09 - v1.23.7 - Added note to ednaIssues_hash if Report date is not expected date. - kdg
## 2015-03-12 - v1.23.8 - Added note to ednaIssues_hash for "No Review found". - kdg
## 2015-03-12 - v1.23.9 - Added $value2 to facilitate collecting IP addresses for Printers.  Updaed update_storeinfo_db accordingly - kdg
## 2015-03-25 - v1.23.10 - Adjusted adding value to email_messages_ws on line 1686 to not show error - kdg
## 2015-05-05 - v1.23.11 - Corrected access to the Wire database - kdg
## 2015-06-02 - v1.23.12 - A bit of cleanup and reduced logging - kdg
## 2015-06-11 - v1.23.13 - A bit more cleanup and reduced logging.  Cleaned up the subject line. - kdg
## 2015-07-04 - v1.23.14 - Added Store state (open or closed) to review line - kdg
## 2015-07-09 - v1.23.15 - Adjustment to msg to avoid duplicate OPEN or CLOSE messages - kdg
## 2015-07-13 - v1.24.0 - Added support for printer report - kdg
## 2015-07-20 - v1.24.1 - Revised formating of the printer report a bit - kdg
## 2015-08-03 - v1.24.2 - Revised eDSR reporting - kdg
## 2015-08-04 - v1.24.3 - Revised eDSR reporting - kdg
## 2015-08-05 - v1.24.4. - Add could not contact to eDSR only if it is a company store - kdg
## 2015-08-17 - v1.24.5 - Revised eDSR reporting to make it easier for Gloria to read - kdg
## 2015-10-20 - v1.24.6 - Revision to Record the number of days up after adding network and card processing info to result line - kdg
## 2015-11-06 - v1.24.7 - Added Kate McMahon to rsm_email_hash - kdg
## 2015-12-16 - v1.24.8 - Disabled serveral calls to send_email as requested by Leonard and Jon. - kdg
## 2016-01-07 - v1.25.0 - Added VeriFone - kdg
## 2016-01-12 - v1.25.1 - Worked on formatting email_verifone_messages - kdg
## 2016-01-13 - v1.25.2 - Show stores without VeriFone warnings on the VeriFone report as well - kdg
## 2016-01-20 - v1.25.3 - Added Mailcc4 - kdg
## 2016-01-21 - v1.25.4 - Replaced Blaine with Robin Steiff - kdg
## 2016-02-03 - v1.25.5 - Replaced getting info from store list.  Replaced get_store_id with get_store_info_hash. - kdg
## 2016-02-08 - v1.26.0 - Added %level1Issues_hash and retired ednaIssues_hash - kdg
## 2016-02-10 - v1.26.1 - Added get_alt_ip.  Added another use of monitor_email_only - kdg
## 2016-02-12 - v1.26.2 - Updated the label for no review found - kdg
## 2016-02-25 - v1.26.3 - Confirming Jim Gittings email address - kdg

use constant SCRIPT_VERSION => "1.26.3";

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use File::Copy;
use Net::SMTP;
use TTV::MlinkUtilities qw(mkpath trim openODBCDriver LogMsg indent dequote execQuery_arrayref recordCount);

use strict;
use Getopt::Long;
use DBI;
use Cwd;

# Variables to connect to the mysql db on ttv-lamp1


my $mysqld="192.168.21.42";
my $dbh="";
unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) {
	LogMsg("Error.  Cannot connect to mysql on host $mysqld");
	exit;
}
 
# Variables to connect to dbStoreData
my $dsn = "driver={SQL Server};Server=****;database=dbStoreData;uid=****;pwd=****;";
my $mssql_dbh = openODBCDriver($dsn, '****', '****');
 

my %store_id_hash=();

my $po_date_label="";   # This variable allows use to use the --day argument to specify the day for checking po's
my @ltm = localtime();   
# The date variables are in three different formats
# YYYYMMDD   
my $date_label = sprintf("%04d%02d%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);  
# YYYY-MM-DD
my $date_label2 = sprintf("%04d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);  
# MM-DD-YYYY
my $date_label3 = sprintf("%02d-%02d-%04d", $ltm[4]+1, $ltm[3], $ltm[5]+1900);

# Get yesterday's date
my $timeseconds=time();
my @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
my $previous_date_label = sprintf("%04d%02d%02d", $previous_day_ltm[5]+1900, $previous_day_ltm[4]+1, $previous_day_ltm[3]);  
my $previous_date_label3 = sprintf("%02d-%02d-%04d", $previous_day_ltm[4]+1, $previous_day_ltm[3], $previous_day_ltm[5]+1900);
## Email settings

my $Mailserver = 'exchange.tenthousandvillages.com';
my $Mailfrom = "Backup_check";
my $Mailto = 'kdg@tenthousandvillages.com';

my $Mailcc = '';
my $Mailcc2 = '';
my $Mailcc3 = '';
my $Mailcc4 = '';

# Hash defining supported store info - These entries get added to the database
# whenever they are collected.
my %storeinfo_hash=(
    "No Internet Count" => '1',      
);
# These entries only get added if they have changed since the last entry
my %storeinfo_hash_on_update=(    
    "System Brand" => '2',
    "System Model" => '3',
    "BIOS Name" => '4',
    "BIOS Revision" => '5',
    "BIOS Release Date" => '6',
    "Tenders" => '7',
    "Traffic Sensor Version" => '8',
    "UPS has a data connection" => '9',    
    "Traffic Counters" => '10',
    "Traffic Counter Adjustments" => '11',
    "Traffic Sensor Version" => '12',
    "Bank Account" => '13',  
    "Serial Number" => '14'      
);

my @store_list = ();
my @mlink_list = ();
my $mlink = 0;
my $daily_report="//Ttv-mlink/mlink/Backup/todaysBackup.txt";
my $source_safe="//villages.com/docs/Departments/IT/scripts/Store Scripts";
my $storelist_file="//ouagadougou/Departments/IT/scripts/Store Scripts/storelist.txt";
$storelist_file="${source_safe}/storelist.txt";
 
my $base_dir="//ttv-mlink/mlink/ToStore/newstore/last_version";
my $today_backup_report="${base_dir}/backup_rpt.txt";
my $today_monitor_report="${base_dir}/monitor_rpt.txt";
my $today_po_report="${base_dir}/po_rpt.txt";
my $today_mlink_report="${base_dir}/mlink_rpt.txt";
my $store_info_file="${base_dir}/store_info.txt";
my @store_info=();
my $s="";
my %store_info_hash=();
my @email_messages=();
my @email_messages_ups=();
my @email_messages_ws=();
my @email_messages_reg=();
my @email_messages_add=();
my @email_misc_messages=();
my @email_alert_messages=();
my @email_messages_mixmatch=();
my @email_printer_messages=();
my @email_eDSR_messages=();
my @email_verifone_messages=();
my @edna_issue_watch_list=(
	"Failed to find xps process running",
	"Store is closed but",
	"Store is open for",
	"Raid logical device status is Degraded",
	"instances of Orphaned page",
	"Possible problems with End Of Day process",
	"No Review found",
	"Could not contact",
	"pool was empty"
 
);
my %email_alert_hash=();
my $alert=0;
my $check_dbh;
my $email="";
my @stores=();
my @company_store_ips=();
my @issues_found=();
my @known_issues=();
my %filesize_hash=();
my %filename_hash=();
my %IP_hash=();
my $error_count = 0;
my $cs_error_count = 0;
my $warn_count = 0;
my $fail_count = 0;
my $debug_mode = 0;
my $store_count=0;
my $contract_store_count=0;
my $company_store_count=0;
my $mlink_count=0;
my $daily_issues=0;
my $weekly_issues=0;
my $monthly_issues=0;
my $file_size_issues=0;
my $updateknownissues=1;
my %table=();
my %table2=();
my %table3=();
my %table4=();
my %table5=();

my $mlink_dir="//Ttv-mlink/mlink/Backup";
my $polled_dir="//Ttv-mlink/mlink/FromStore";

my $processDir = "processed";
my $mlink_only=0;
my $nomail=0;
my $monitor_only=0;		# Process the monitor reports only
my $mixmatch_only=0;	# Process the mixmatch reports only
my $eDSR_only=0;		# Process the eDSR reports only
my $verifone_only=0;	# Process the VeriFone reports only
my $backup_only=0;		# Process the backup reports only
my $special_only=0;		# The miscellaneous report is actually a part of the monitor report
my $po_only=0;          # Process the po reports only
my $ttvsftp1_only=0;      # Only check ttvsftp1 for store backups
my $help=0;
my $current_report=0;
my $day=0;
my $dayname="";
my $daynum;
my $test_mode=0;
my $verbose=0;
my $status_only=0;
my $monitor_email_only=0;	# Show monitor email only
my @storeid_req=();
my @exclude_id=();
my $enable_process_po=1;    # Used to enable the process_po function
## If the backup file is smaller than minfilesize, a warning will be mailed to IT.
my $minfilesize = 600000;
#my $IPsource="//Drone1/c/BB/BBNTD/2.2j/etc/bb-hosts.cfg";
my $IPsource="${source_safe}/storelist.txt";
my $known_issues="${source_safe}/storeissues.txt";
#$minfilesize = 1200000;
## The ratio of file sizes between the largest and the smallest backup
#	file should be no larger than max_filesize_ratio.
my $max_filesize_ratio = 1.5;
my $expected_version="9.5.0.0Build390";
my $expected_version_sap="10.3.0Build740";
my @misc_errors=();
my @SysInfo=();
my @Tenders=();
my @UPS=();
my @TCounters=();
my @TCounterAdj=();
my @TCounterType=();
my @noInternet=();
my @sftp_stores=();
my $sumInfo="${source_safe}/sumInfo.txt";
$sumInfo="//ttv-mlink/mlink/ToStore/newstore/last_version/sumInfo.txt";
my $e_man_sum="";   # Checksum of exceptions_manifest
my $s_man_sum="";   # Checksum of store_manifest
my $hostname=`hostname`;
my $day_of_week;
my $need2checkttvsftp1=0;
my %noInternet_hash;
my %ednaIssues_hash;
my %level1Issues_hash;
my %level2Issues_hash;
my %upDay_hash;
my %throughput_hash;
my %reg_warn_hash=();
my %edna_warn_hash=();
my %alt_IP_hash=();

##########
## MAIN
##########
GetOptions(
	"debug"=>\$debug_mode,
	"nomail"=>\$nomail,
	"monitor"=>\$monitor_only,
	"mixmatch"=>\$mixmatch_only,
	"eDSR"=>\$eDSR_only,	
	"verifone"=>\$verifone_only,
	"backup"=>\$backup_only,
	"help"=>\$help,
	"current"=>\$current_report,
    "po"=>\$po_only,
    "day=i"=>\$day,
    "test"=>\$test_mode,
    "storeid=s"=>\@storeid_req,
    "exclude=s"=>\@exclude_id,
    "ttvsftp"=>\$ttvsftp1_only,
    "verbose"=>\$verbose,
    "status"=>\$status_only,
	"special"=>\$special_only,
	"alert"=>\$alert,
	"dbh"=>\$check_dbh,
	"emailm"=>\$monitor_email_only
);
my @storeid_list=split(/,/,join(',',@storeid_req));
my @exclude_list=split(/,/,join(',',@exclude_id));

if ($current_report) {
	$Mailcc='jpl@tenthousandvillages.com';
	$Mailcc2='al.rehman@tenthousandvillages.com';	
	$Mailcc3='jon.barrick@tenthousandvillages.com';	
	$Mailcc4='Leonard.Wolf@tenthousandvillages.com';	
}
 
if ($status_only) {
    $Mailcc='jpl@tenthousandvillages.com';
	$Mailcc2='al.rehman@tenthousandvillages.com';
	$Mailcc3='jon.barrick@tenthousandvillages.com';	
	$Mailcc4=''
}
if ($test_mode) {
    $Mailcc='';
	$Mailcc2='';
	$Mailcc3='';
	$Mailcc4=''
}
if ($help) {
	usage();
}
if ($mixmatch_only) {
	$monitor_only=1;
}
if (($eDSR_only) || ($verifone_only)) {
	$monitor_only=1;
}

if ($check_dbh) {
	check_dbh();
	exit;
}

LogMsg("\n\n");
LogMsg("#####################################");
LogMsg("$0 version ".SCRIPT_VERSION);

getdate();
if ($verbose) {
	LogMsg("Today is a $dayname backup.");
}
LogMsg("#####################################\n\n");
 
get_store_info_hash(); 
 

if ($ttvsftp1_only) {  
    check_ttvsftp1();    
    exit;
}

if ($po_only) {
    if ($enable_process_po) {
		 
        process_po_report();
        create_todays_report(\@email_messages, $error_count, "PurchaseOrders");
        if (($debug_mode)||($nomail)) {
            LogMsg("Check PO report error count: $error_count ");
            foreach $email (@email_messages) {
                print "$email";
            }
        } elsif ($error_count) {           
            send_email(\@email_messages, $error_count, "PurchaseOrders");
        }
    } else {
        LogMsg("Process PO not valid on Monday");
    }
        
    exit;
}

# If the current reports are called for
if ($current_report) {
	# This report is run in the morning to check the most current backup at all stores, company and contract.
 
    get_IP_hash();
 
    # Check Backup Reports
	if (($monitor_only) || ($special_only)) {
		if ($verbose) {
			print "Monitor only: ($monitor_only)\n";
			print "Special only: ($special_only)\n";
			LogMsg("Skipping the backup report...");
		}
		
	} else {
        LogMsg("Getting the backup report");
		view_current("backup_report");
        #ttvsftp1();
        check_ttvsftp1();
        report_misc_backup_info();
        create_todays_report(\@email_messages, $error_count, "Backup");
		if (($debug_mode)||($nomail)) {
			LogMsg("Backup error count: $error_count ($cs_error_count Company Stores) - $store_count stores checked");
			foreach $email (@email_messages) {
				print "$email";
			}
		} else {
			send_email(\@email_messages, $error_count, "Backup");
		}
	}
	
	# Check Monitor Reports
	@email_messages=();
	@email_messages_ups=();
	@email_messages_ws=();
	@email_messages_reg=();
	@email_messages_mixmatch=();
	@email_printer_messages=();
	@email_eDSR_messages=();
	@email_verifone_messages=();
	
	$error_count=0;
	$cs_error_count=0;
	if (($backup_only) || ($special_only)) {
		
		if ($verbose) {
			LogMsg("Skipping the monitor report...");
			print "Backup only: ($backup_only)\n";
			print "Special only: ($special_only)\n";
		}		
	} else {
        LogMsg("Getting the monitor report");
        if (1) {
            manifest_info();      
            view_current("monitor_report");       
            # Update the known issues file
            #update_known_issues();
            update_known_issues_db();
            report_misc_info();
            prepend_summary();
            create_todays_report(\@email_messages, $error_count, "Monitor");
			
            if (($debug_mode)||($nomail)) {                
                LogMsg("Monitor error count: $error_count - $store_count stores checked");
			 
                foreach $email (@email_messages) {
                    print "$email";
                }
				unless ($monitor_email_only) {
					if ($mixmatch_only) {
						if ($#email_messages_mixmatch > -1 ) {
							my $message_count=($#email_messages_mixmatch + 1);
							LogMsg("MixMatch count: $message_count - $store_count stores checked");				
							foreach $email (@email_messages_mixmatch) {
								print "$email";
							}		
						}	
					} elsif ($eDSR_only) {					
						if ($#email_eDSR_messages > -1 ) {
							my $message_count=($#email_eDSR_messages + 1);
							LogMsg("eDSR count: $message_count - $company_store_count stores checked");				
							foreach $email (@email_eDSR_messages) {
								print "$email";
							}		
						}	
					} elsif ($verifone_only) {					
						if ($#email_verifone_messages > -1 ) {
							my $message_count=($#email_verifone_messages + 1);
							LogMsg("eDSR count: $message_count - $store_count stores checked");				
							foreach $email (@email_verifone_messages) {
								print "$email";
							}		
						}						
					
					} else {
						if ($#email_misc_messages > -1 ) {
							my $message_count=($#email_misc_messages + 1);
							LogMsg("Miscellaneous count: $message_count - $store_count stores checked");				
							foreach $email (@email_misc_messages) {
								print "$email";
							}		
						}
						if ($#email_messages_mixmatch > -1 ) {
							my $message_count=($#email_messages_mixmatch + 1);
							LogMsg("MixMatch count: $message_count - $store_count stores checked");				
							foreach $email (@email_messages_mixmatch) {
								print "$email";
							}		
						}					
					
						if ($#email_messages_ups > -1 ) {
							my $message_count=($#email_messages_ups + 1);
							LogMsg("UPS count: $message_count - $store_count stores checked");				
							foreach $email (@email_messages_ups) {
								print "$email";
							}		
						}	
						if ($#email_messages_ws > -1 ) {
							my $message_count=($#email_messages_ws + 1);
							LogMsg("WS count: $message_count - $store_count stores checked");				
							foreach $email (@email_messages_ws) {
								print "$email";
							}		
						}	
						if ($#email_messages_reg > -1 ) {
							my $message_count=($#email_messages_reg + 1);
							LogMsg("REG count: $message_count - $store_count stores checked");				
							foreach $email (@email_messages_reg) {
								print "$email";
							}		
						}		
					}
				}
            } else {  	
				unless ($mixmatch_only) {
					# Send the Monitor email out
					send_email(\@email_messages, $error_count, "Monitor"); 
 	
				}
				# If this is the status report - do not send the misc or ups reports
						
				unless (($status_only) || ($monitor_email_only)) {  
					if ($mixmatch_only) {
						if ($#email_messages_mixmatch > -1 ) {
							my $message_count=($#email_messages_mixmatch + 1);				
							send_email(\@email_messages_mixmatch, $message_count, "MixMatchMessages");  				
						}	
					} elsif ($eDSR_only) {					
						if ($#email_eDSR_messages > -1 ) {						
							send_email(\@email_eDSR_messages, "NA", "eDSRMessages");  				
						}		
					} elsif ($verifone_only) {					
						if ($#email_verifone_messages > -1 ) {						
							send_email(\@email_verifone_messages, "NA", "verifoneMessages");  				
						}								
					} else {
						if ($#email_misc_messages > -1 ) {
							my $message_count=($#email_misc_messages + 1);	
							# Disabled 2015-12-16 - kdg							
							#send_email(\@email_misc_messages, $message_count, "MiscMessages");  				
						}
						if ($#email_messages_mixmatch > -1 ) {
							my $message_count=($#email_messages_mixmatch + 1);				
							send_email(\@email_messages_mixmatch, $message_count, "MixMatchMessages");  				
						}							
						if ($#email_messages_ups > -1 ) {
							#my $message_count=($#email_messages_ups + 1);
							# Disabled 2015-12-16 - kdg							
							#send_email(\@email_messages_ups, "NA", "UPSMessages");  				
						}	
						if ($#email_messages_ws > -1 ) {									
							send_email(\@email_messages_ws, "NA", "WSMessages");  				
						}		
						if ($#email_messages_reg > -1 ) {						
							send_email(\@email_messages_reg, "NA", "REGMessages");  				
						}	
						if ($#email_printer_messages > -1 ) {						
							# Disabled 2015-12-16 - kdg
							#send_email(\@email_printer_messages, "NA", "PrinterMessages");  				
						}
						if ($#email_eDSR_messages > -1 ) {
							# Disabled 2015-12-16 - kdg
							#send_email(\@email_eDSR_messages, "NA", "eDSRMessages");  				
						}		
						if ($#email_verifone_messages > -1 ) {							 
							send_email(\@email_verifone_messages, "NA", "verifoneMessages");  				
						}								

						
					}
				}
            }
			# If the alert argument is sent, process alert emails			
			if ($alert) { 
				LogMsg("Alerts are enabled");			
				foreach my $storenum (sort keys(%email_alert_hash)) {
					#if ($verbose) {
						LogMsg("Processing Alerts for $storenum - al1");
					#}
					my $ref=$email_alert_hash{$storenum};
					my @messages=@$ref;					
					process_alerts($storenum,\@messages);
				}
			} else {
				LogMsg("Alerts are not enabled");
			}
        }
	}
	
	if ($special_only) {
		LogMsg("Getting the special report");
 
		# The special report or miscellaneous report is a subset of the monitor report
		manifest_info();  
		view_current("monitor_report");	
		# The only time this runs is in the morning when the full report runs
		# Send the misc messages	
		if ($#email_misc_messages > -1 ) {
			my $message_count=($#email_misc_messages + 1);
			if (($debug_mode)||($nomail)) {                
				LogMsg("Miscellaneous count: $message_count - $store_count stores checked");				
				foreach $email (@email_misc_messages) {
					print "$email";
				}
			} else {  				
				send_email(\@email_misc_messages, $message_count, "MiscMessages");          				
			}						
		}
	}	
    if (0) {    # Currently disabled
        # Check PO's
        @email_messages=();
    	$error_count=0;
    	$cs_error_count=0;    
    	if (($backup_only) || ($monitor_only)) {
			if ($verbose) {
				LogMsg("Skipping the Purchase Order report...");    
			}
        } elsif ($enable_process_po) {
            process_po_report();
            create_todays_report(\@email_messages, $error_count, "PurchaseOrders");
            if (($debug_mode)||($nomail)) {
                LogMsg("Check PO report error count: $error_count ");
                foreach $email (@email_messages) {
                    print "$email";
                }
            } elsif ($error_count) {
                # Only send an email if there were errors
                send_email(\@email_messages, $error_count, "PurchaseOrders");
            }           
        }
    }


} else {
	# Check the company store backups retrieved via mlink
	# This runs in the afternoon after mlink has had the time to retrieve the backup.
 
	if (0) {	# Disabled 2013-08-16 - I think this section is obsolete - kdg
		push(@email_messages, " - Checking mlink backups -\n");
		unless (opendir MLINK_DIR, $mlink_dir) {
			LogMsg("Could not open the $mlink_dir!");
			LogMsg("End of backup check");
			exit;
		}
		# Getting a list of the stores we have
		@store_list = grep /^\d{4}$/, readdir MLINK_DIR;    
		closedir MLINK_DIR;

		# Checking what stores we expect to have.
		foreach my $key (sort keys(%store_info_hash)) {        
			my $found=0;
			if ($store_info_hash{$key}->[1] eq "Company") {
				foreach my $s (@store_list) {
					if ($s == $key) {
						$found=1;
					}
				}
				unless ($found) {
					# We found a store we expected to be on the list but was not so add it
					push(@store_list,$key);
				}
			}
		}

		read_todays_backup();

		create_todays_report(\@email_messages, $error_count, "MlinkBackups");    
		push(@email_messages, " - Finished checking mlink backups - $store_count stores checked $error_count errors\n");	
		if (($debug_mode)||($nomail)) {    
			foreach $email (@email_messages) {        
				print "$email";
			}	
		} elsif ($error_count) {          
			# Only send an email if there were errors
			send_email(\@email_messages, $error_count, "MlinkBackups");
		}                     
		exit;
	}

}
update_codes();
update_store_info_file();
$dbh->disconnect;
exit;
######################
##  Functions
######################
sub check_ttvsftp1 {
    # Different systems have different sftp modules and so I have two different routines 
    # depending upon the system.  This function calls the appropriate function to check the
    # ttvsftp1 ftp site for offsite backups.    
    # Check that we can ping the ttvsftp1 site.
    my $host = '192.168.21.44';
    LogMsg("Check connectivity to ttvsftp1...");
    if (ping_site($host)) {
        LogMsg(indent(1)."Successful ping to ttvsftp1");
    } else {
        LogMsg(indent(1)."First ping to ttvsftp1 was NOT successful");
        # Try one more time before giving up
        sleep 2;
        if (ping_site($host)) {        
            LogMsg(indent(1)."Successful ping to ttvsftp1 on second attempt");
        } else {
            LogMsg(indent(1)."Ping to ttvsftp1 was NOT successful");
            push(@email_messages, "Could not contact ttvsftp1 to verify sftp backups to that site\n");
            return;
        }        
    }   
    unless (check_ttvsftp1_IT()) {
        LogMsg("Failed connection to ttvsftp1 with sftp.  ");
    }      
}   

sub check_ttvsftp1_IT {
    LogMsg("Checking ttvsftp1...");    
    push(@email_messages, "\n\n-----------------------------\nChecking ttvsftp1...(IT)\n");     
    use Net::SFTP::Foreign;
    my $remoteDir="store.backup";
    my $host = '192.168.21.44';
    my $uid = 'manager';
    my $pwd = 'ttvadmin'; 
    my $debug = 0;    
    my @store_list=();
    my $d;    
    my $fileDiffLimit=100;  # Limit to the acceptable difference in file sizes.
    
    my $plink_verbose = $debug ? '-v' : '';
    my $sftp;
  
    $sftp = Net::SFTP::Foreign->new((
              open2_cmd => "plink $host -l $uid -pw $pwd $plink_verbose -ssh -2 -s sftp",
              )
           );

    if ($sftp->{pid}) {
        LogMsg(indent(1). "SFTP process spawned as pid: ". $sftp->{pid});
    } else {
        LogMsg(indent(1). "Failed to log in to ttvsftp1");
        push(@email_messages, "Failed to log in to ttvsftp1"); 
        return 0;
    }
    
    LogMsg(indent(1). "Getting file listing from $remoteDir"); 
    #my @dir = $sftp->ls("$remoteDir");
    my @dir;
    eval {@dir= $sftp->ls(".")};
    if ($@) {
        LogMsg("Error: Could not get a directory listing");
        return;
    }
    
    foreach $d (@dir) {  
        if ($verbose) {
            print "$d\n";
        }
         
        my @tmp=@$d;
        foreach my $t (@tmp) {      
            my $file=$$t{longname};
            my @line=split(/\s+/,$file);
            my $filename=$line[8]; 
            next if ($filename eq ".");
            next if ($filename eq "..");
            if ($verbose) {
                print "$filename\n";
            }
    
            push(@store_list,$filename);              
        }  
    }      
    foreach my $s (@store_list) {  
    
        next if ($s =~ /^\./);                
        # Only check stores that regularly backup to sftp site.        
        my $include=0;
        if ($ttvsftp1_only) {
            $include=1;
        }
        foreach my $sl (@sftp_stores) {                    
            if ($s eq $sl) {
                $include=1;    
               
            }
        }        
        next unless ($include);    
        if (defined($filename_hash{$s})) {            
            my @dir = $sftp->ls("$s");
            my $found=0;
            foreach $d (@dir) {               
                my @tmp=@$d;           
                foreach my $t (@tmp) {      
                    my $file=$$t{longname};                
                    my @line=split(/\s+/,$file);
                    my $filename=$line[8]; 
                    my $size=$line[4];
                    next if ($filename =~ /^\./);                  
                    if ($filename =~ /$filename_hash{$s}/) {                    
                        #LogMsg(indent(1)."$s Found $filename size:($size)");
                        
                        unless(defined($filesize_hash{$s})) {
                            LogMsg(indent(1)."NOTE: Expected file size for store $s is unknown");   
                            push(@email_messages, "\n$s - NOTE: Expected file size for store $s is unknown"); 	                        
                        } else {
                            my $diff = $filesize_hash{$s} - $size;
                            $diff=abs($diff);                        
                            if ($diff < $fileDiffLimit) {
                                LogMsg(indent(1)."PASSED: Found $filename size:($size)"); 
                                push(@email_messages, "\n$s  - PASSED: Found $filename size:($size)"); 
                            } else {
                                $error_count++;
                                LogMsg(indent(1)."WARNING: File size expected to be $filesize_hash{$s}");
                                push(@email_messages, "\n\t-->ERROR #$error_count: File size expected to be $filesize_hash{$s}\n");                             
                            }
                        }
                        $found=1;
                    }                
                }  
            }   
            unless ($found) {
               $error_count++;
                LogMsg(indent(1)."WARNING: Found no backup for store $s  ");
                push(@email_messages, "\n$s - -->ERROR #$error_count: Found no backup for store $s");                      
            }                    
        } else {
            $error_count++;
            LogMsg(indent(1)."WARNING: File name unknown for $s");
            push(@email_messages, "\n$s-->ERROR #$error_count: File name unknown for $s\n");           
        }
    }
    LogMsg(indent(1). "Stopping external SFTP process");

    my $cmd = "exit\n";
    my $bw = syswrite($sftp->{ssh_out}, $cmd, length($cmd));  
    return 1;
}


sub manifest_info {
    my @tmp=();        
    my @info=();
	if ($verbose) {
		LogMsg("Getting sum info");
	}
    # Get the checksums of the manifest files
    if (-e "$sumInfo") {
        open(INPUT,"$sumInfo");
        @info=(<INPUT>);
        close INPUT;
        foreach my $i (@info) {
            if ($i =~ /store_manifest/i) {
                @tmp=split(/\s+/,$i);
                $s_man_sum=$tmp[0];
            }
            if ($i =~ /exceptions_manifest/i) {
                @tmp=split(/\s+/,$i);
                $e_man_sum=$tmp[0];
            }            
        }     
    }  else {
        LogMsg("ERROR: Failed to find $sumInfo"); 
    }

}
=pod
sub get_IP_hash_orig {
	my @line=();
	my $storenum="";
	my $storename="";  
	my $IP="";	    
	
	if (0) {
		# This is the old way of getting the IP address - 2012-10-01 - kdg
		if (-e "$IPsource") {
			LogMsg("Found ipsource");
			open(IPSOURCE,"$IPsource");
			my @ipinfo=(<IPSOURCE>);
			close IPSOURCE;
			foreach (@ipinfo) {
				@line=split();
				$storenum = $line[0];
				next if ($storenum =~ /^#/); # Skip lines which are comments.
				
				# Check to see if the store is excluded;
				my $exclude=0;
				foreach my $ex (@exclude_list) {
					if ($storenum == $ex) {
						$updateknownissues=0;
						LogMsg("Excluding $storenum");
						$exclude=1;                
					}
				}
				next if ($exclude);
				
				# If specific stores were requested:
				my $include=1;
				if ($#storeid_list > -1) {
					$include=0;
					$updateknownissues=0;
					foreach my $si (@storeid_list) {
						if ($storenum == $si) {
							LogMsg("Including $storenum");
							$include=1;
						}
					}
				} 
				next unless ($include);
				
				$storename = $line[2];
				$IP = $line[6];
				$IP_hash{$storename}=["$IP","$storenum"];            
			}
		} else {
			LogMsg(indent(1)."Error!  Could not access $IPsource");
			LogMsg(indent(1)."exiting");
			return;
		}
	}
	# Get the IP address for the stores
 
	my $query = "
		select 
			StoreId, 
			StoreName, 
			StoreCode,
			ElderIP
		from 
			rtblstores 
		where 
			StartDate < getdate()
		and
			EndDate is  NULL
		and
			StoreId > 100	
		and
			StoreCode in ('V1','V2')
		";
 
 
	#my $dbh = openODBCDriver($dsn, 'rpt', 'rpt'); 
	my $sth = $mssql_dbh->prepare($query);
	$sth->execute();
 
	my $found_po=0;

    while (my @tmp=$sth->fetchrow_array()) {
		my $storenum = $tmp[0];
		my $storename = $tmp[1];
		my $IP = $tmp[3];
		# Check to see if the store is excluded;
		my $exclude=0;
		foreach my $ex (@exclude_list) {
			if ($storenum == $ex) {
				$updateknownissues=0;
				LogMsg("Excluding $storenum");
				$exclude=1;                
			}
		}
		next if ($exclude);
		
		# If specific stores were requested:
		my $include=1;
		if ($#storeid_list > -1) {
			$include=0;
			$updateknownissues=0;
			foreach my $si (@storeid_list) {
				if ($storenum == $si) {
					LogMsg("Including $storenum");
					$include=1;
				}
			}
		} 
		next unless ($include);
		
		$IP_hash{$storename}=["$IP","$storenum"];  		
	}
	#$dbh->disconnect;
}
=cut


sub get_IP_hash {
	my @line=();
	my $storenum="";
	my $storename="";  
	my $IP="";	    
	
	if (0) {
		# This is the old way of getting the IP address - 2012-10-01 - kdg
		if (-e "$IPsource") {
			LogMsg("Found ipsource");
			open(IPSOURCE,"$IPsource");
			my @ipinfo=(<IPSOURCE>);
			close IPSOURCE;
			foreach (@ipinfo) {
				@line=split();
				$storenum = $line[0];
				next if ($storenum =~ /^#/); # Skip lines which are comments.
				
				# Check to see if the store is excluded;
				my $exclude=0;
				foreach my $ex (@exclude_list) {
					if ($storenum == $ex) {
						$updateknownissues=0;
						LogMsg("Excluding $storenum");
						$exclude=1;                
					}
				}
				next if ($exclude);
				
				# If specific stores were requested:
				my $include=1;
				if ($#storeid_list > -1) {
					$include=0;
					$updateknownissues=0;
					foreach my $si (@storeid_list) {
						if ($storenum == $si) {
							LogMsg("Including $storenum");
							$include=1;
						}
					}
				} 
				next unless ($include);
				
				$storename = $line[2];
				$IP = $line[6];
				$IP_hash{$storename}=["$IP","$storenum"];            
			}
		} else {
			LogMsg(indent(1)."Error!  Could not access $IPsource");
			LogMsg(indent(1)."exiting");
			return;
		}
	}
	if ($mssql_dbh) {
		# Get the IP address for the stores
	 
		my $query = "
			select 
				StoreId, 
				StoreName, 
				StoreCode,
				ElderIP
			from 
				rtblstores 
			where 
				StartDate < getdate()
			and
				EndDate is  NULL
			and
				StoreId > 100	
			and
				StoreCode in ('V1','V2')
			";
	 
	 
		#my $dbh = openODBCDriver($dsn, 'rpt', 'rpt'); 
		my $sth = $mssql_dbh->prepare($query);
		$sth->execute();
	 
		my $found_po=0;

		while (my @tmp=$sth->fetchrow_array()) {
			my $storenum = $tmp[0];
			my $storename = $tmp[1];
			my $IP = $tmp[3];
			# Check to see if the store is excluded;
			my $exclude=0;
			foreach my $ex (@exclude_list) {
				if ($storenum == $ex) {
					$updateknownissues=0;
					LogMsg("Excluding $storenum");
					$exclude=1;                
				}
			}
			next if ($exclude);
			
			# If specific stores were requested:
			my $include=1;
			if ($#storeid_list > -1) {
				$include=0;
				$updateknownissues=0;
				foreach my $si (@storeid_list) {
					if ($storenum == $si) {
						LogMsg("Including $storenum");
						$include=1;
					}
				}
			} 
			next unless ($include);
			
			$IP_hash{$storename}=["$IP","$storenum"];  		
		}
	} elsif (-f $store_info_file) {
		open(INFO,"$store_info_file");
		my @store_info_list=(<INFO>);
		close INFO;
		foreach my $s (@store_info_list) {
			chomp($s);
			next if ($s =~ /^#/);
			my @tmp=split(/,/,$s);
			my $label = $tmp[0];
			my $storename = $tmp[1];
			my $storenum = $tmp[2];
			my $IP = $tmp[3];			
			if ($label eq "IP") {
				$IP_hash{$storename}=["$IP","$storenum"];  	
			}
		}
	}
	#$dbh->disconnect;
}

sub get_alt_ip {
	my $myip=shift;	
	my @tmp=split(/\./,$myip);
	my $third_part=$tmp[2];
	my $fourth_part=$tmp[3];
	# Increment by 1 to get the register's IP
	$fourth_part=($fourth_part + 1); 	
	my $reg_IP="192.168"."."."$third_part"."."."$fourth_part";
	return $reg_IP;
}

sub view_current {
	# This method uses http to view the latest backup report currently on the edna/reg1 system.	
	use strict;
	use LWP::Simple;
	my $report=shift;	
	LogMsg("Viewing current $report");
	my @tmp=();
	my @results=();
	my @misc_results=();
	my @alert_results=();
	my @noted=();
	my @ups_results=();
	my @ws_results=();
	my @reg_results=();
	my @printer_results=();
	my @eDSR_results=();
	my @verifone_results=();
	

	my @mixmatch_results = ();
	if ($report =~ /backup/i) {
		open (REPORT,">$daily_report");
	}
	my $report_date;
	$store_count=0;
	$company_store_count=0;
 
	KEY: foreach my $key (sort keys(%IP_hash)) {
        if ($verbose) {
            print "Getting info for $key...\n";
        }
		
        # Is this a company store?
        my $storenum=$IP_hash{$key}->[1];  

        
		my $label="";       
		 
        if ($store_info_hash{$storenum}->[1] =~ /company/i) {
            $label="--> Company Store <--";  
			$company_store_count++;			
        } else {
            $label="contract";           
        }   
      
        # Need the storename
		 
        my $storename=$store_info_hash{$storenum}->[0];
        $storename=lc($storename);
        $storename=~s/\.//g;
        $storename=~s/ //g;     
        
		my $content="";
        my $found_version="";
        my $IP=$IP_hash{$key}->[0]; 
		if ($verbose) {
			LogMsg("Processing report from $key...");
		}
		my $URL="http://${IP}/cgi-bin/i_toolshed.pl?tool=RPT&reportreq=$report";
        #my $URL="http://$IP_hash{$key}/cgi-bin/i_toolshed.pl?tool=RPT&reportreq=$report";
 
		$content = get($URL);
		unless ($content) {
			# See if Reg1 can be contacted
			
			my $alt_IP=get_alt_ip($IP);
			unless (ping_site($alt_IP))	{
				# If we cannot reach the first register, try the second
				$alt_IP=get_alt_ip($alt_IP);
			}
			
			LogMsg("Could not get content for $key...");
			LogMsg("Check connectivity to $key...");		  
			if (ping_site($IP)) {
				LogMsg(indent(1)."Successful ping to $key at $IP");	
				push(@email_messages, "\nSuccessful ping to $key at $IP\n");					
				$content = get($URL);  	
				unless ($content) {
					sleep 5;
					LogMsg("Could not get content for $key...trying again");
					$content = get($URL);    
				}	
			} elsif (($alt_IP) && ("$IP" ne "$alt_IP")) {
				LogMsg("Check connectivity to $key at alternative IP: $alt_IP...");	
				if (ping_site($alt_IP)) {
					$URL="http://${alt_IP}/cgi-bin/i_toolshed.pl?tool=RPT&reportreq=$report";
					LogMsg("Check connectivity to $key at $alt_IP...");	
					LogMsg(indent(1)."Successful ping to $key at $alt_IP");	
					push(@email_messages, "\nSuccessful ping to $key at $alt_IP\n");					
					$content = get($URL);  	
					unless ($content) {
						sleep 5;
						LogMsg("Could not get content for $key...trying again");
						$content = get($URL);    
					}				
				} else {
					LogMsg(indent(1)."Ping to $key at $alt_IP was NOT successful - skipping");  
				}
			} else {
				LogMsg(indent(1)."Ping to $key at $IP was NOT successful - skipping");  
				push(@email_messages, "\nCould not ping $key at $IP\n");				
			}   					
		}
		my $filename="unknown";
		my $filesize="unknown";
		my @tmp=();
      

        my $review_missing=1;   # Set this variable.  Clear it if we find the "REVIEW" line in the report.
        my $sysInfoString="";
 
        my $backupType=""; 
		my $report_date_error="";
        $need2checkttvsftp1=1;        
		$store_count++;    
    
		if ($content) {
			if ($verbose) {
				LogMsg("Got content for $key");
			}
			@results=();
			@noted=();
			@ups_results=();
			@ws_results=();
			@reg_results=();
			@email_alert_messages=();
            @alert_results=();
			@printer_results=();
			@eDSR_results=();
			@verifone_results=();
			my @content_info = split(/\<br(\>| \/\>)/,$content) ;            
			foreach my $c (@content_info) {
				chomp($c);	
                if ($storenum == 99) {
                    # This is key because the leading zero's are necessary 
                    $storenum="0099";
                }
=pod			# This section commented out when we got rid of reading from the store list					 
                my $server=$store_info_hash{$storenum}->[3];
                if ($verbose) {
                 
                    my $ref=$store_info_hash{$storenum};
                    print "Info for $storenum: (@$ref)\n";
                }

                if ($status_only) {                        
                    unless ($server =~ /edna/i) {					 
                        LogMsg("Store $storenum runs on a register - skipping");
						push(@email_messages, "\nStore $storenum runs on a register - skipping");
                        $store_count--;   
                        next KEY;
                    }
                }
=cut				
				if ($report =~ /monitor/i) {
					if ($c =~ /Report Date/) {						
						my @tmp=split(/\s+/,$c);
						$report_date=$tmp[-1];
						unless ($report_date eq $date_label3) {							
							$error_count++;
							$report_date_error="WARNING: Report date is $report_date but expected $date_label3";
							$ednaIssues_hash{"Report date is $report_date but expected $date_label3"}.="$storenum ";
							#LogMsg(indent(1)."WARNING: Report date is $report_date but expected $date_label3");
							#push(@email_messages, "\n\t-->ERROR #$error_count: Report date is $report_date but expected $date_label3\n");  						
						}
					}
				}        
 
				if (($c =~ /RESULTS:/) || ($c =~ /WARN/i) || ($c =~ /ERROR/i) ) {					
					if ($c =~ / UPS /) {
						# Collect UPS issues separately
						push(@ups_results,"$c");
					} elsif ($c =~ /: WorkStation - /) {
						# Collect WorkStation issues separately
						push(@ws_results,"$c");
					} elsif (($c =~ /: reg. - /) || ($c =~ /reg..log/) || ($c =~ /reg._monitor.log/)  || ($c =~ /register log/)){	
						# Collect Register issues separately
						push(@reg_results,"$c");
						# A fair bit of parsing follows as the WARNING may be at the first part of the line or in the middle 
						# of the line.  And, it could have WARNING at the first part and ERROR in the middle
						if ($c =~ /WARN/i) {								
							# Keep a hash so we can easily see how many instances of a particular error were seen
							my @wtmp=split(/WARNING: /,$c);							
							my $warning;
							if ($#wtmp == 1) {
								$warning=$wtmp[1];
								if ($warning =~ /ERROR/i) {									
									my @wtmp2=split(/ERROR: /,$warning);									
									if ($#wtmp2 == 1) {
										$warning=$wtmp2[1];
									} elsif ($#wtmp2 > 1) {
										$warning=$wtmp2[2];
									}																
								}
							} elsif ($#wtmp > 1) {
								$warning=$wtmp[2];
							}							
							$warning =~ s/\)//g;							
							if ($warning =~ /Windows dump/) {									
								@wtmp=split(/\s+/,$warning);
								# Need to take the count off of the front
								shift @wtmp;
								$warning=join(' ',@wtmp);								
							}
							$reg_warn_hash{$warning}++;
						}
					} else {
					
						push(@results,"$c");
						if ($c =~ /WARN/i) {	
							# Keep a hash so we can easily see how many instances of a particular error were seen
							my @wtmp=split(/WARNING: /,$c);
							if ($c =~ /Windows dump/) {
								shift @wtmp;
								my $warning=join(' ',@wtmp);								
								$edna_warn_hash{$warning}++;
							}						
						}	
						if ($c =~ /WARN/i) {								
							# Keep a hash so we can easily see how many instances of a particular error were seen
							my @wtmp=split(/WARNING: /,$c);	
							if (($c =~ /LVL1/) || ($c =~ /LVL2/))
							{								
								shift @wtmp;
								my $warning=join(' ',@wtmp);								
								if ($c =~ /LVL1/)
								{
									# Trim off the LVL tag
									$warning =~ s/- LVL1//;
									$level1Issues_hash{$warning}.="$storenum ";	
								}
								if ($c =~ /LVL2/)
								{
									$warning =~ s/- LVL2//;									
									$level2Issues_hash{$warning}.="$storenum ";	
								}								
							}
							foreach my $issue (@edna_issue_watch_list) {															
								if ($c =~ /$issue/) {									
									shift @wtmp;
									my $warning=join(' ',@wtmp);	 									
									$ednaIssues_hash{$warning}.="$storenum ";							 								
								}						
							}
						}														
					} 					
				}
				if (($c =~ /NOTICE:/) && ($c =~ / UPS /)) {
					# Collect UPS issues
					push(@ups_results,"$c");					
				}
				# Collect miscellaneous entries for the miscellaneous report				
				if ($c =~ /MISC:/) {	
					my @tmp=split(/:/,$c);			
					push(@misc_results,"\n$tmp[1]");
				 
					if ($tmp[1] =~ /MixMatch/i) {
						my $msg = substr($c,10);
						push(@mixmatch_results,"\n$msg");					
					}
 			
				}		
				if ($c =~ /BSF:/) {
					# Collect a noted event
					#push(@noted,"$c");
					push(@misc_results,"$c");					
				}	
				if ($c =~ /NOTICE:/) {
					# Collect a noted event
					#push(@noted,"$c");
					push(@misc_results,"$c");					
				}			
				if ($c =~ /ALERT:/) {
					# Collect an alert			
 			
					push(@alert_results,"$c");										
				}	
				if ($c =~ /PRINTER:/) {
					# Collect printer info		
					my $record=substr($c,10);
					push(@printer_results,"\t$record\n");					
				}			
				if ($c =~ /eDSR:/) {
					# Collect eDSR info							
					my $record=substr($c,7);					
					push(@eDSR_results,"\t$record\n");					
				}		
				if ($c =~ /VERIFONE:/) {
					# Collect VERIFONE info												
					push(@verifone_results,"$c");					
				}						
				if (($c =~ /RESULTS:/) && ($c =~ /mlink/i)) {
					$backupType=" - Mlink";
				}   
				if (($c =~ /RESULTS:/) && ($c =~ /iDrive/i)) {					
					if (($c =~ /FAILED/) || ($c =~ /Error/i)) {
						$backupType=" - iDrive FAILED ";							
					} else {						
						$backupType=" - iDrive";
					}
				}        
				if ($c =~ /SFTP process spawned/) {
					$backupType.=" - SFTP";
                    push(@sftp_stores,$storename);                    
				} 
               
				if ($c =~ /ISSUE:/) {
                    @tmp=split(/:/,$c);        
                    if ($tmp[2]) {
                        push(@issues_found,"$storenum|$key|$tmp[1] $tmp[2]|NEW");                    
                    } else {
                        push(@issues_found,"$storenum|$key|$tmp[1]|NEW");                    
                    }
					
				} 
   
                if ($c =~ /zip file size:/) {                    
                    $_=$c;					
                    @tmp=split();
                    $filesize=$tmp[$#tmp];
                    $filesize=~ s/ //g;                                    
                    $filesize_hash{$storename}=$filesize;                         
                }
                if ($c =~ /Creating Zip File/) {
                    $_=$c;
                    @tmp=split();
                    $filename=$tmp[3];
                    $filename=~ s/ //g;                    
                    $filename_hash{$storename}=$filename; 
                }          
                if ($c =~ /Primary Backup:/) {
                    # - If the backup was a copy and did not create a zip, we would not have a file size from the above.  This is the new method. 
                    $_=$c;
                    @tmp=split();                    
                    $filename=$tmp[-1];                    
                    $filename=~ s/ //g;                    
                    $filename_hash{$storename}=$filename; 
                }                                 
                if ($c =~ /Triversity version/) {
                    # Check that the version of the software is what is expected.
            		@tmp=split(/\s+/,$c);
					$found_version=$tmp[$#tmp];	
                    unless(("$found_version" eq "$expected_version") || ("$found_version" eq "$expected_version_sap"))  {
                        push(@misc_errors,"$storenum $key $found_version");
                    }
                }
                if ($c =~ /exceptions_manifest:/i) {
                    # Check that the exceptions_manifest is the correct version
                    @tmp=split(/:/,$c);
					$found_version=lc($tmp[1]);	
                    unless ($found_version eq $e_man_sum) {
                       push(@misc_errors,"$storenum $key exceptions manifest does not match expected: $e_man_sum found: $found_version");
                    }                    
                }
                if ($c =~ /store_manifest:/i) {
                    # Check that the store_manifest is the correct version
                    @tmp=split(/:/,$c);
					$found_version=lc($tmp[1]);	
                    unless ($found_version eq $s_man_sum) {
                       push(@misc_errors,"$storenum $key store manifest does not match expected: $s_man_sum found: $found_version");
                    }
                }
				
                if ($c =~ /System Info -/i) {  					
                    chomp($c);  					
					if ($verbose) {
						LogMsg("$c");
					}
                    my @array=split(/\s+/,$c);
                    my $label='';
                    my $value='';                    
					my $value2='';  
                    my $found_label=0;
                    for (my $x=0; $x<=$#array; $x++) {
                        if ($found_label) {
                            $label.="$array[$x] ";
                        }
                        if ($array[$x] =~ /-/) {
                            # The label is the next item
                            $found_label=1;
                        }                                              
                    }   
                    if ($label =~ /:/) {                      
                        # Get the value after the colon
                        @tmp=split(/:/,$label);
                        $label=$tmp[0];
                        $value=$tmp[1];	
						if (defined($tmp[2])){
							$value2=$tmp[2];	
						}							
                    }                        					
                    if ($value) {                    
                        $value=~s/ //g;     						
                        update_storeinfo_db($storenum, "$label", "$value", "$value2");
                    }
                }        
                if ($c =~ /Computer Info -/i) {
                    chomp($c);                   
                    my @array=split(/\s+/,$c);
                    my $label;
                    my $value;                    
                    my $found_label=0;
                    for (my $x=0; $x<=$#array; $x++) {
                        if ($found_label) {
                            $label.="$array[$x] ";
                        }
                        if ($array[$x] =~ /-/) {
                            # The label is the next item
                            $found_label=1;
                        }                                              
                    }
 
                    if ($label =~ /:/) {
                        unless ($label =~ /bios/i) {
                            # The sysInfoString gets reported in the email but we are not interested in BIOS info here
                            $sysInfoString.="$label ";  
                        }                        
                        # Get the value after the colon
                        @tmp=split(/:/,$label);
                        $label=$tmp[0];
                        $value=$tmp[1];
                    }                        
                    update_storeinfo_db($storenum, "$label", "$value");                       
                }                 
                if ($c =~ /Throughput:/) { 
                    @tmp=split(/\s+/,$c);
                    my $throughput=$tmp[-2];
					my $date=$tmp[2];
 
					if (($date eq $date_label) || ($date eq $previous_date_label)) {						
						# Need to check the date label since the backup only occurs if the store has closed.  If the store
						# did not open for the day, without a date check we would just keep recording the last backup's
						# throughput repeatedly.
						$throughput_hash{$storenum}=$throughput;    # Record the throughput or speed of the backup      
						# System Stats: get added to the storeInfo table in the pos_support database each time they are collected.
						update_storeinfo_db($storenum, "System Stats: Throughput", "$throughput");                        
					} 
                }
                if ($c =~ /No Internet Count:/) { 
                    @tmp=split(/\s+/,$c);
                    my $count=$tmp[-1];               
                    $noInternet_hash{$storenum}=$count;   
                    # System Stats: get added to the storeInfo table in the pos_support database each time they are collected.
                    #update_storeinfo_db($storenum, "System Stats: No Internet Count", "$count");                        
                }    
                if ($c =~ /System Stats/) { 
                    # System statistics that we wish to record
                    @tmp=split(/-/,$c);
                    my $stat_title=$tmp[1];
                    @tmp=split(/:/,$stat_title);
                    $stat_title=$tmp[0];
                    my $stat=$tmp[1];
					my $stat2='';
					if (defined($tmp[2])) {
						$stat2 = $tmp[2];
					}
                    $stat_title=~s/ //g;
                    $stat=~s/ //g;
					$stat2=~s/ //g;
                    # System Stats: get added to the storeInfo table in the pos_support database each time they are collected.
                    update_storeinfo_db($storenum, "System Stats: $stat_title", "$stat","$stat2");  
                }
 
                if ($c =~ /WARNING.*No Internet access/) {
                    # Get the no Internet count                    
                    @tmp=split(/\s+/,$c); 
                    for (my $x=0; $x<=$#tmp; $x++) {
                        if ($tmp[$x] =~ /Found/i) {
                            $noInternet_hash{$storenum}=$tmp[($x+1)];    # Record the number of no internet access errors                                            
                        }
                    }

                    
                }
				if ($c =~ /REVIEW:/) {
					# This is the final review of the backup report
                    $review_missing=0;                    
                    my $msg="";
                    my $uptime;

                    if ($c =~ /updates/) {						
                        # Find out how many updates are waiting to be installed
                        @tmp=split(/-/,$c);
                        for (my $x=3; $x<=$#tmp; $x++) {
                            $msg.=" - $tmp[$x]";   							
                        }        
                        $uptime=$tmp[5];						
						@tmp=split(/\s+/,$uptime);  
						$uptime=$tmp[2];   						
                        $uptime=~s/ //g;                         						                        
						if ($uptime) {
							@tmp=split(/d/,$uptime);
							$uptime=$tmp[0];                        
							$upDay_hash{$storenum}=$uptime;    # Record the number of days up  							
						}
                    }
                    
                   	if ($report =~ /backup/i) {	
                        $msg=$backupType;
                    }
					if ($c =~ /OPEN$/) {
						unless ($c =~ /Store is OPEN$/) {
							$msg.=" - Store is OPEN";
						}
					}					
					if ($c =~ /CLOSED$/) {
						unless ($c =~ /Store is CLOSED$/) {
							$msg.=" - Store is CLOSED";
						}
					}						
					if ($c =~ /PASSED/) {
 
						# Check to see if there was a report date error.
						if ($report_date_error) {
							LogMsg("\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 					
							push(@email_messages, "\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 
							push(@email_messages,"\n$report_date_error");							
						} else {
							if ($verbose) {						 
								LogMsg("$store_count - $storenum - $key - PASSED $msg"); 	
							}
							push(@email_messages, "\n$store_count - $storenum - $key - PASSED $msg");  
						}
						foreach (@noted) {
							# Put the noted events in the report
							push(@email_messages,"$_");
						}	
						# If the UPS message is a NOTICE rather than a WARNING, it will be handled here.
						if ($#ups_results > -1) {
							push(@email_messages_ups, "\n\n$store_count  : $storenum - $key - $label");  	
						}						
						foreach (@ups_results) {
							push(@email_messages_ups,"$_");							
						}	
						if ($#ws_results > -1) {
							push(@email_messages_ws, "\n\n$store_count  : $storenum - $key - $label");  	
						}								
						foreach (@ws_results) {
							push(@email_messages_ws,"$_");							
						}	
						if ($#reg_results > -1) {
							push(@email_messages_reg, "\n\n$store_count  : $storenum - $key - $label");  	
						}								
						foreach (@reg_results) {
							push(@email_messages_reg,"$_");							
						}		
 						
						                       
					} else {
 
						# Unless it passed, send out the significant lines in an email
						$error_count++;                       
                        LogMsg("\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 					
						push(@email_messages, "\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 	
						if ($#ups_results > -1) {
							push(@email_messages_ups, "\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 	
						}
						if ($#ws_results > -1) {
							push(@email_messages_ws, "\n\n$store_count  : $storenum - $key - $label");  	
							#push(@email_messages_ws, "\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 	
						}
						if ($#reg_results > -1) {
							push(@email_messages_reg, "\n\n$store_count  : $storenum - $key - $label");  
							#push(@email_messages_reg, "\n\n$store_count -->ERROR #${error_count}: $storenum - $key - $label"); 	
						}						
						foreach (@noted) {
							# Put the noted events in the report
							push(@email_messages,"$_");
						}								
						foreach (@results) {							 
							push(@email_messages,"$_");
						}	
						foreach (@ups_results) {
							push(@email_messages_ups,"$_");							
						}
						foreach (@ws_results) {
							push(@email_messages_ws,"$_");							
						}	
						foreach (@reg_results) {
							push(@email_messages_reg,"$_");							
						}							

						
                        push(@email_messages, "\n");                                              
						if ($label =~ /company/i) {
							$cs_error_count++;							
						}
					}
					if ($report =~ /monitor/i) {
						# Labels for the misc messages
 						
						if ($#misc_results > 0) {
							push(@email_misc_messages, "\n\n$store_count: $storenum - $key - $label"); 							 						 
							foreach my $m (@misc_results) {
								# Put the misc events in the misc report								
								push(@email_misc_messages,"$m");							 	
							}
							
						}
						# Now clear the array
						@misc_results='';	
						#
						# Collect the MixMatch messages
						if ($#mixmatch_results > -1) {
							push(@email_messages_mixmatch, "\n\n$store_count: $storenum - $key - $label"); 	
							 
							foreach my $m (@mixmatch_results) {
								# Put the misc events in the misc report					
								 
								push(@email_messages_mixmatch,"$m");							 	
							}
							
						}
						# Now clear the array
						@mixmatch_results='';	
						#						
						
						# Collect the printer info
						#		
											
						if ($#printer_results > -1) {												
							#push(@email_printer_messages,"Printer Info for $key\n");
							push(@email_printer_messages,"\n$store_count: $storenum - $key - $label\n"); 							
							foreach my $a (@printer_results) { 								
								# Put the printer info in the printer report
								if ($a) {									
									push(@email_printer_messages,"$a");																	
								}									
							}							
						}
						# Now clear the array
						@printer_results='';	

						# Collect the eDSR info
						#													
						if ($#eDSR_results > -1) {																			
							push(@email_eDSR_messages,"\n$company_store_count: #$storenum - $key - "); 							
							foreach my $a (@eDSR_results) { 								
								# Put the eDSR info in the eDSR report
								if ($a) {										
									push(@email_eDSR_messages,"$a");																	
								}									
							}							
						}
						# Now clear the array
						@eDSR_results='';	
						
						# Collect the VeriFone info
						#													
						if ($#verifone_results > -1) {																			
							push(@email_verifone_messages,"\n$store_count: #$storenum - $key - \n"); 							
							foreach my $a (@verifone_results) { 								
								# Put the eDSR info in the eDSR report
								if ($a) {	
									# Here we remove the white space in front of the text we want to report.
									# This is only for formatting purposes.
									while ($a =~ /^\s/) {
										$a =~ s/^\s//;										
									}
									push(@email_verifone_messages,"\t$a\n");																	
								}									
							}	
			 
						} else {
							push(@email_verifone_messages,"\n$store_count: #$storenum - $key - \n"); 	
						}
						# Now clear the array
						@verifone_results='';							
						
						# Collect the alerts
						#						
						if ($#alert_results > -1) {	
							push(@email_alert_messages,"Alerts for $key\n");																	
							foreach my $a (@alert_results) {								 
								# Put the alert events in the alert report
								if ($a) {
									push(@email_alert_messages,"$a");																	
								}									
							}							
						}
						# Now clear the array
						@alert_results='';							
					}					
				}                   
			}	# End of foreach content
			
			#if ($report =~ /backup/i) {	
			#	print REPORT "$storenum $filename $filesize\n";
			#}          
            if ($review_missing) {
            	$error_count++;
                LogMsg("$store_count -->ERROR #${error_count}: $label $storenum $key - No Review found!");
                push(@email_messages, "\n\n$store_count -->ERROR #${error_count}: $label $storenum $key - No Review found!\n");
				$ednaIssues_hash{"No Review found!"}.="$storenum ";	
				#debug
				$level1Issues_hash{"No $report Review found!"}.="$storenum ";	
				if ($label =~ /company/i) {
					$cs_error_count++;							
                }                                
            }          
			# Todo: Now we have the filename and size.  Write this somewhere where we can read it in again to check the polling backup.
			#
			#	Email alerts
			#
			 
			if ($#email_alert_messages > -1) {
                my $eam_count=($#email_alert_messages + 1);
                LogMsg("$eam_count alerts for $storenum - adding to email alert hash");
				my @array=@email_alert_messages;
                push(@array,"\nreport version ".SCRIPT_VERSION."\n");
				$email_alert_hash{$storenum}=\@array;				 
				@email_alert_messages=();				 
			}
			
		 
		} else {
            LogMsg("Could not get content from $key");

			$error_count++;
			$label="contract";
			 
            if ($store_info_hash{$storenum}->[1] eq "Company") {
                $label="--> Company Store <--";							
                $cs_error_count++;	            
            }     
 
            LogMsg("$store_count -->ERROR #${error_count}: Could not contact $storenum - $key at $IP - $label");
			$ednaIssues_hash{"Could not contact"}.="$storenum ";		 
			$level1Issues_hash{"Could not contact"}.="$storenum ";
			push(@email_messages, "\n\n$store_count -->ERROR #${error_count}: Could not contact $key at $IP - $label\n");
			if ($label =~ /Company/) {
				push(@email_eDSR_messages,"\n\n$company_store_count -->ERROR Could not contact $storenum - $key - $label\n"); 	
			}
		}  # End of if content 
        if ($report =~ /monitor/i) {
            if ($sysInfoString) {
                push(@SysInfo,"$storenum $key $sysInfoString");
            } else {
                push(@SysInfo,"$storenum $key - No System Info");
            } 
        }
                  
	}	# End of store loop
	#if ($report =~ /backup/i) {
	#	close REPORT;
	#}  
}

 

sub get_store_info_hash {
	# Need to retire the storelist_file - So, populate the store_info_hash here
	if ($mssql_dbh) {
		# To the store number, associate the store name and whether it is Company or Contract.
		my $query = "select StoreId, StoreName, StoreCode from rtblstores where StoreCode in ('V1','V2') and EndDate is NULL";
		my $tbl_ref = execQuery_arrayref($mssql_dbh, $query);
		if (recordCount($tbl_ref) > 0)  {
		   for (my $x=1; $x<=recordCount($tbl_ref); $x++) {
			  my $line_ref = @$tbl_ref[$x];
			  if ($line_ref) {
				 my @line = @$line_ref;
				 if ($#line > -1) {
					my $skip=0;
					my $store_line = "";
					my $storenum = $line[0];
					my $storename = $line[1];
					if ("$line[2]" eq "V1") {
					   $store_line = "$line[1] - Company";
					} elsif ("$line[2]" eq "V2") {
					   $store_line = "$line[1] - Contract";
					} else {
					   $skip=1;
					}
					unless ($skip) {
					   $store_id_hash{$line[0]} = "$store_line";
					   # Early on we had used the store_info_hash so to support some of that old code,
					   # populate that here as well.
					   $store_info_hash{$storenum}=["$storename","$store_line"];  
					}
				 }
			  }
		   }
		}
	} elsif (-f $store_info_file) {
		open(INFO,"$store_info_file");
		my @store_info_list=(<INFO>);
		close INFO;
		foreach my $s (@store_info_list) {
			chomp($s);
			my @tmp=split(/,/,$s);
			my $label = $tmp[0];
			my $storenum = $tmp[1];
			my $store_line = $tmp[2];
			my $StoreCode = $tmp[3];			
			if ($label eq "StoreCode") {
				$store_id_hash{$storenum} = "$store_line";	
			}
		}
	}
}
 
sub read_todays_backup {
	my @daily_report_info=();
	my $storenum="";
	my $s="";
	my @line="";
	open (INPUT, "$daily_report") or die ("Cannot open $daily_report");
	@daily_report_info=(<INPUT>);
	close INPUT;
	foreach $s (@store_list) {
		$store_count++;
		foreach (@daily_report_info) {
			@line=split();
			my $store=$line[0];
			my $filename=$line[1];
			my $filesize=$line[2];
			if ($s eq $store) {
				# The first item on the line is the store number					
				push(@email_messages, " - $store $store_id_hash{$store} \n");				
				verify_mlink_backup($store, $filename, $filesize);
			}
		}
	}
}
 
sub verify_mlink_backup {
	my $store = shift;
	my $filename = shift;
	$filename = lc($filename);
	$filename =~ s/\s//g;
	my $filesize = shift;
	my @dir = ();
	my $d = "";
	my @file_info = ();
	my $found_size = "";
	my $found_file = 0;
	my $verified = 0;
	# find the backup in the mlink folder
	my $target_dir = "${mlink_dir}/${store}";
	if ($filename eq "unknown") {
		push(@email_messages, "\tFilename for store $store is unknown!\n");
		$error_count++;
		return;
	}
 
	if (opendir MLINK, $target_dir) {
		@dir = readdir MLINK;
      foreach $d (@dir) {
      	$d = lc($d);
      	if ("$d" eq "$filename") {
         	$found_file = 1;
         	@file_info = stat "${target_dir}/${d}";
            $found_size = $file_info[7];
			 
			if ($filesize eq "unknown") {
				push(@email_messages, "\tExpected filesize for store $store is unknown!\n");
				$error_count++;
			} elsif ($found_size = $filesize) {
				# I found a file in the mlink backup directory for this store
				# where the name and the size match so we can say with
				# some certainty that the backup is good.
				#LogMsg(indent(2)."Backup Verified");
				$verified = 1;
            } else {
            	push(@email_messages, "Expected size: $filesize Found size: $found_size\n");
            }
         }
      }
		closedir MLINK;
		if ($found_file) {
			if ($verified) {
				push(@email_messages, "\tBackup Verified\n");
			} else {
				LogMsg(indent(2)."Failed to verify backup");
				push(@email_messages, " ---> Failed to verify backup error $error_count<--- \n\n");
				$error_count++;
			}
		} else {
			LogMsg(indent(2)."Failed to find $filename");
			$error_count++;
			push(@email_messages, " ---> Failed to find $filename in mlink folder!- error $error_count<--- \n\n");
		}
	} else {
        LogMsg(indent(2)."Failed to find a backup directory for $store");
        $error_count++;
 
        push(@email_messages, " ---> Failed to find a backup directory for $store $store_info_hash{$store}->[0]\n\n");
    }
}
 
sub process_po_report{    
    $date_label=$po_date_label;
    my $po_file="open_pos.${date_label}";
    my $rd_file="open_rds.${date_label}";
    my $order_dir="";
    my $storenum="";
    my $storename="";
    my $folder="";
    my @file_list=();
    my $pofile="";
    my $rdfile="";
    my @tmp = ();
    my @pofile_info=();
    my @rdfile_info=();
    my $orig_dir=cwd();
    my $rd_number="";

    
 
    LogMsg("Checking Issued PO's");
 
    foreach $storenum (sort keys(%store_info_hash)) {
        if ($#storeid_list > -1) {
            # We specified some stores to check so make certain we only check those
            unless (grep/$storenum/,@storeid_list) {
                # This store is not on the list of stores to process so skip it                    
                next;
            }
        }
        next if ($storenum == 99) ;
        next if ($storenum eq "0099");
                
        $store_count++;
 
        $storename=$store_info_hash{$storenum}->[0];
        LogMsg(indent(1)."Checking store number $storenum - $storename");
        push(@email_messages, "Checking store number $storenum - $storename\n");  
        $folder="${polled_dir}/${storenum}/processed";
        $order_dir="${folder}/ord";

        if (-e $folder) {
            if (opendir(INPUT,"$folder")) {
                @file_list = readdir INPUT;
                closedir INPUT;
                # Search through the list of files for the open po file                                             
                @tmp=grep /$po_file/, @file_list;
                if ($tmp[0]) {
                    $pofile="${folder}/${tmp[0]}";
                } else {
                    LogMsg(indent(2)."Note: Could not find $po_file");
        			#$error_count++;
        			push(@email_messages, " ---> Note: Could not find $po_file <--- \n");
                    push(@email_messages, " ---> Cannot determine current open PO's for this store <--- \n\n");
                    next;
                }                                
 
                if (open(POINPUT,"$pofile")) {
                    @pofile_info=(<POINPUT>);
                    close POINPUT;                    
                    foreach (@pofile_info) {
                        @tmp=split(/\t/,$_);               
                        if ($tmp[0] =~ /^$storenum/) {                                         
                            my $po_number=$tmp[0];
                            my $status = $tmp[4];
                            my $vendor = $tmp[5];                              
                            next unless ($status =~ /i/i);      # We want only issued purchase orders
                            next unless ($vendor == '01');      # We want only Villages orders
                            # This looks like a PO number we are interested in.
                            # See if we have received an order associated with this PO.
                            # Method Two - Run a query                            
                            my $found_order=query_po($po_number,$storename);                                 
                            unless ($found_order) {                            
                                $rd_number = query_rd($storenum, $po_number);                                                                
                                if ($rd_number eq "none") {                                      
                                        LogMsg(indent(2)."Failed to find a receiving document for po $po_number");
                                        push(@email_messages, " ---> Failed to find a receiving document for po $po_number <--- \n\n"); 
                                } else {
                                       LogMsg(indent(2)."Found receiving document $rd_number for po $po_number");
                                       push(@email_messages, " ---> Found receiving document $rd_number for po $po_number <--- \n\n"); 
                                        
                                }                                
                            } else {
                                if ($verbose) {
                                    LogMsg(indent(1)."--> Found order for $po_number for $storename");
                                }
                            }
                        }                            
                    }
                         
                } else {
                    LogMsg(indent(2)."Failed to open $pofile");
        			$error_count++;
        			push(@email_messages, " ---> Failed to open $pofile - error $error_count<--- \n\n");
                } 
            } else {
                LogMsg(indent(2)."Failed to open $folder");
                $error_count++;
                push(@email_messages, " ---> Failed to open $folder - error $error_count<--- \n\n");            
            }            
        } else {
            LogMsg(indent(2)."Failed to find $folder");
			$error_count++;
			push(@email_messages, " ---> Failed to find $folder - error $error_count<--- \n\n");
        } 
        
    }
}
 
sub query_rd {
    $date_label=$po_date_label;
    my $storenum=shift;
    my $po_requested=shift;
    my $rd_file="open_rds.${date_label}";
    my $rdfile="";  
    my @tmp = ();
    my @rdfile_info=();
    my $rd_number=0;
    my @file_list=();
    my $folder="${polled_dir}/${storenum}/processed";        
        if (-e $folder) {        
            if (opendir(INPUT,"$folder")) {
                @file_list = readdir INPUT;
                closedir INPUT;
                # Search through the list of files for the open rd file                
                @tmp=grep /$rd_file/, @file_list;
                if ($tmp[0]) {
                    $rdfile="${folder}/${tmp[0]}";
                } else {
                    LogMsg(indent(2)."Failed to find $rd_file");
        			$error_count++;
        			push(@email_messages, " ---> Failed to find $rd_file - error $error_count<--- \n\n");
                    next;
                }                                
                if (open(RDINPUT,"$rdfile")) {                
                    @rdfile_info=(<RDINPUT>);
                    close RDINPUT;                    
                    foreach (@rdfile_info) {                    
                        @tmp=split(/\t/,$_);             
                        my $po_number = $tmp[2];
                        next if ($po_number eq "poNum");                        
                        if ($po_number eq $po_requested) {                                         
                            # If we find the entry for the po we are looking for, get the document number 
                            my $status = $tmp[3];
                            my $vendor = $tmp[6];                                  
                            next unless ($status =~ /i/i);      # We want only issued purchase orders
                            next unless ($vendor == '01');      # We want only Villages orders                                                
                            $rd_number=$tmp[1];     
                            if ($rd_number eq "") {
                                $rd_number="none";
                            }
                        }                            
                    }                         
                } else {
                    LogMsg(indent(2)."Failed to open $rdfile");
        			$error_count++;
        			push(@email_messages, " ---> Failed to open $rdfile - error $error_count<--- \n\n");
                } 
            } else {
                LogMsg(indent(2)."Failed to open $folder");
                $error_count++;
                push(@email_messages, " ---> Failed to open $folder - error $error_count<--- \n\n");            
            }            
        } else {
            LogMsg(indent(2)."Failed to find $folder");
			$error_count++;
			push(@email_messages, " ---> Failed to find $folder - error $error_count<--- \n\n");
        }     
    
    return $rd_number;
}
 
sub query_po {
    my $dsn = "driver={SQL Server};Server=****;database=dbNavision;uid=****;pwd=****;";
 	my $query="";
    my $order_req=shift;
    my $storename=shift;
 
    my $storeID=substr($order_req, 0, 4);
    my $orderID=substr($order_req, 7, 4);
 
	$query = dequote(<< "	ENDQUERY");

    select [Entry No_], [Order No_],[Customer No_], [External Doc_ No_], [Status], [Order Date], [Date Time Entered], [Date Time Processed], [Comments] 
    from [Villages\$Order Buffer Header] where [Customer No_]='$storeID'
    order by [Order Date] desc
	ENDQUERY
 
	my @tmp=();	
	my $dbh_po = openODBCDriver($dsn, 'rpt', 'rpt');

	my $sth = $dbh_po->prepare($query);
   $sth->execute();
 
	my $found_po=0;
	my $order_alt1=uc($order_req);
	my $order_alt2=substr($order_alt1,0,15);
 
    while (@tmp=$sth->fetchrow_array()) {
        my $entry = $tmp[0];
        my $orderno = $tmp[1];
        my $storeno = $tmp[2];
        my $docno = $tmp[3];
        my $stat = $tmp[4];
        my $or_date = $tmp[5];
        my $datein = $tmp[6];
        my $dateproc = $tmp[7];
        my $com = $tmp[8];
        next unless ($storeno == $storeID);
 
        if (($docno =~ /$order_req/) || ($docno =~ /$order_alt1/) || ($docno =~ /$order_alt2/)) {
            $found_po=1;
			if ($verbose) {
				if ($docno =~ /$order_req/) {
					print "Found $docno to match $order_req\n";
				}
				if ($docno =~ /$order_alt1/) {
					print "Found $docno to match $order_alt1\n";
				}
				if ($docno =~ /$order_alt2/) {
					print "Found $docno to match $order_alt2\n";
				}				
			}
        }
		
	}	
    unless ($found_po) {
        LogMsg(indent(2)."Failed to find an order for po $order_req");
        $error_count++;
        push(@email_messages, " ---> Failed to find an order for po $order_req - error $error_count<--- \n\n"); 
        # Put this in the database
        #submit_issue_to_db($storeID,$storename,"Failed to find an order for po $order_req");
    }
	$dbh_po->disconnect;
    return $found_po;
}

sub send_email{
	#SMTP vars
	my $array_ref=shift;
	my $error_count=shift;
	my $errors_label="- $error_count errors ($cs_error_count Company Stores)";   
	my $specified=shift;	

	if (($specified eq "MiscMessages") 
		|| ($specified eq "UPSMessages")  
		|| ($specified eq "WSMessages")  
		|| ($specified eq "REGMessages") 
		|| ($specified eq "MixMatchMessages") 
		|| ($specified eq "PrinterMessages") 
		|| ($specified eq "eDSRMessages")
		|| ($specified eq "verifoneMessages")) 
		{
		# Don't send the errors label for MiscMessages
		$errors_label="";
	}	
	
	my $subject_label = $specified;	
	my $mail1 = $Mailcc;	
	my $mail2 = $Mailcc2;	
	my $mail3 = $Mailcc3;	
	my $mail4 = $Mailcc4;		
	if ($specified eq "MixMatchMessages") 	{
		#$mail1 = 'Blaine.Derstine@tenthousandvillages.com';
		$mail1 = 'Robin.Stieff@tenthousandvillages.com';
		$mail2 = 'Sandra.Greiner@tenthousandvillages.com';
		$mail3 = '';		
	}
	if ($specified eq "PrinterMessages") 	{
		$mail1='Jon.Barrick@tenthousandvillages.com';
		$mail2='';
		$mail3='';		
	}	
	if ($specified eq "eDSRMessages") 	{
		$mail1='Jon.Barrick@tenthousandvillages.com';
		$mail2='Gloria.Lewis@tenthousandvillages.com';
		$mail3='';		
		$store_count=$company_store_count;
	}		
	if ($specified eq "verifoneMessages") 	{
		$mail1='';
		$mail2='';
		$mail3='';			 
	}		
	if ($test_mode) {
		$mail1='';
		$mail2='';
		$mail3='';
	}	
	
    LogMsg("Sending $specified email message to $Mailserver...");	
	$specified.="\@tenthousandvillages.com";
	my @message_array=@$array_ref;
	my $smtp="";
   
 
    # Connect to the server
    

 
    $smtp = Net::SMTP->new("$Mailserver", Hello=>'backupadmin', Debug=>0);
    die "Couldn't connect to server" unless $smtp;
    unless($smtp->mail($specified)) {
        LogMsg("Error with ($specified)");
    }
    unless($smtp->to($Mailto)) {
        LogMsg("Error with ($Mailto)");
    }
    $smtp->to($mail1);
	$smtp->to($mail2);
	$smtp->to($mail3);
	$smtp->to($mail4);	
	#if ($verbose) {
		LogMsg("Mailto: $Mailto");
		LogMsg("Mailcc: $mail1");
		LogMsg("Mailcc2: $mail2");
		LogMsg("Mailcc3: $mail3");
		LogMsg("Mailcc4: $mail4");		
		
	#}

    #start mail
    if ($smtp->data()) {
		if ($verbose) {
			LogMsg("Starting mail");
		}
    } else {
        LogMsg("Error starting mail");
    }

    #send header
    if ($smtp->datasend("To: Ten Thousand Villages IT\n")) {
		if ($verbose) {
			LogMsg("Send header");
		}
    } else {
        LogMsg("Error sending header");
    }
    if ($current_report) {	
		if ($smtp->datasend("Subject:Store $subject_label Report - $store_count stores checked $errors_label\n\n")) {
			if ($verbose) {
				LogMsg("Send Subject");
			}
        } else {
            LogMsg("Error sending Subject");
        }
	} else {
		if ($smtp->datasend("Subject:Store $subject_label Report - $store_count stores checked $error_count errors\n\n")) {
			if ($verbose) {
				LogMsg("Send Subject");
			}
        } else {
            LogMsg("Error sending Subject");
        }
	}
    foreach my $line (@message_array) {
        unless($smtp->datasend("$line")) {
            LogMsg("Error sending line");
        }
    }

    # End the mail
    if ($smtp->dataend()) {
		if ($verbose) {
			LogMsg("Data end");
		}
    } else {
        LogMsg("Error ending data");
    }
    if ($smtp->quit()) {
		if ($verbose) {
			LogMsg("Quiting email");
		}
    } else {
        LogMsg("Error quiting email");
    }
    LogMsg("Finished sending.");
}

sub create_todays_report {
    LogMsg("Creating todays report");
    my $array_ref=shift;
    my $error_count=shift;
    my $specified=shift;
    my @message_array=@$array_ref;
    my $report;
    if ($specified eq "Backup") {
        $report=$today_backup_report;
    } elsif ($specified eq "Monitor") {
        $report=$today_monitor_report;
    } elsif ($specified eq "PurchaseOrders") {
        $report=$today_po_report;
    } elsif ($specified eq "MlinkBackups") {        
        $report=$today_mlink_report;
    } else {
        LogMsg("ERROR: I don't support $specified");
        return;
    }
    LogMsg(indent(1)."Creating $report");
    if (open(REPORT,">$report")) {
        print REPORT "Subject:Store $specified Report - $store_count stores checked $error_count errors\n\n";
        print REPORT "Date: $date_label2\n";
        foreach my $line (@message_array) {
            print REPORT "$line";
        }
        close REPORT;
    } else {
        LogMsg(indent(1)."Unable to open $report");
    }
    
    LogMsg("Finished todays report");
}

sub getdate{
    # Get date
    my @ltm = localtime();
    $po_date_label = sprintf("%04d%02d%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]-$day);
    #$date_label = sprintf("%04d%02d%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
    #$date_label = sprintf("%02d-%02d-%04d", $ltm[4]+1, $ltm[3], $ltm[5]+1900);
    $day_of_week=$ltm[6];
    $dayname='Daily';                      # Default: do a daily backup
    if ($ltm[6] == 0){$dayname='Weekly'};  # if it's the first day of the week, do weekly
    if ($ltm[3] == 1){$dayname='Monthly'}; # if it's the first day of the month, do monthly
    if ($ltm[6]-$day == 1) {
        # The  open_pos list is not created on Monday mornings.
        $enable_process_po=0; 
    }
    $daynum=$ltm[6];
}
 
sub find_known_issues {
    # Read in previously found issues from the known issues file
    LogMsg("Reading in known issues");
    if (-e $known_issues) {
        LogMsg(indent(1)."Found $known_issues");
        if (open(ISSUES,"$known_issues")) {
            # Exclude repaired issues.
            @known_issues= grep !/REPAIRED/, (<ISSUES>);
            LogMsg(indent(1)."Opened and read $known_issues");
        } else {
            LogMsg(indent(1)."Failed to open $known_issues");
        }
        close ISSUES;
    } else {
        LogMsg(indent(1)."Did not find $known_issues");
    }
}

sub update_known_issues {

    # Compare the issues found today with the ones previously known.  The previously known ones we read from the $known_issues file.
    # NOTE: This process should never take an issue off of the list unless is it listed as REPAIRED.
    my @current_issues=@known_issues;  
    my $new_issue_count=0;
    #my $old_issue_count=0;
    my @tmp=();
    push(@email_messages, "\n\n - - - - - - - - - - - - - - - - \n");	
    
    foreach my $new_issue (@issues_found) {                
        my $new=1;
        @tmp = split(/\|/,$new_issue); 
        next unless ($#tmp > -1);
        my $new_entry="$tmp[0]|$tmp[1]|$tmp[2]";   
        foreach my $known (@known_issues) {            
            chomp($known);
            @tmp = split(/\|/,$known);           
            next unless ($#tmp > -1);
            my $known_entry="$tmp[0]|$tmp[1]|$tmp[2]";         
            if ("$new_entry" eq "$known_entry") {  
                # This issue found is a known issue
                # Put it back on the current issue list                 
                #push(@current_issues,"$known");                
                $new=0;                            
            }            
        }        
        if ($new) {     
            # This is a new issue
            # Put this new on the current issues array            
            push(@current_issues,"${new_issue}||$date_label2\n");  
            $new_issue_count++;                     
        }         
    }	
    # Eliminated duplicates (This also removes any blank lines)
    @tmp = @current_issues;
    for (my $x=0; $x<=$#current_issues; $x++) {    
        my $found=0;
        foreach my $t (@tmp) {
            # If we have found this issue on the list once and we find it again, removing it from the list
            if ("$t" eq "$current_issues[$x]") {
                if ($found) {                    
                    $current_issues[$x]="";
                } else {
                    $found++;
                }
            }
        }
    }    
    my $issue_count=($#current_issues + 1);
    push(@email_messages, "\nIssue Count: $issue_count \n");	
    push(@email_messages, "New Issue Count: $new_issue_count \n\n");    
    if (open(ISSUES,">$known_issues")) {
        LogMsg(indent(1)."Updating $known_issues");
        my $counter=0;
        foreach my $c (@current_issues) {  
            # Don't print out blank lines.
            next if ($c =~ /^$/);
            # I don't think I need a \n on this...
            $counter++;
            print ISSUES "$c";
            push(@email_messages, "#${counter}: $c ");	
        }
    } else {
        LogMsg(indent(1)."Error opening $known_issues");
        # Try again.
        close ISSUES;
        sleep 5;
        if (open(ISSUES,">$known_issues")) {
            LogMsg(indent(1)."Updating $known_issues");
            my $counter=0;
            foreach my $c (@current_issues) {  
                next if ($c =~ /^$/);
                $counter++;
                print ISSUES "$c";
                push(@email_messages, "#${counter}: $c ");	
            }
        } else {
           LogMsg(indent(1)."Error opening $known_issues - second try");           
        }       
    }
    close ISSUES;
}
sub update_storeinfo_db {
	if ($special_only) {
		# If the special only flag is set, this function will still get called
		# but we do not want it to do anything.  
		return;
	}
    my $storeid=shift;
    my $desc=shift;
    my $data=shift;
    my $data2;
    $data2=shift;
    my @tmp;
    my $codeid;  
    #my $table;
    my $query;
    my $sth;
    
    # The default database action set here
    my $action="replace";  
    my $update_flag=0;
    my $table="storeInfo";  
    # If the description starts with "stats", these get added to the database each time.  Otherwise, they get added
    # only if they are different than what is already in the database.
    
    if ($desc =~ /stats:/i) {    
        $action="replace";
        my @tmp=split(/\:/,$desc);
        $desc=$tmp[1];    
        $table="storeStats";   
        $update_flag=1;         
        # Trim off the leading space
        if ($desc =~ /^ /) {
            $desc=~s/ //;
        }
    } elsif ($desc =~ /Connectivity/) {
        # Note:  As of 2011-06-01 - I am moving away from using the connectInfo table and using the storeStats table (above) - kdg
        # These items always get entered/inserted
        $table="connectInfo";        
        $action="insert";
        $codeid=1;
        $update_flag=1; 
    }
 
=pod 
    # See if we can connect to the database
    if ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) {
        if ($verbose) {
            LogMsg("Building query...");
        }
        
    } else {
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email_messages, "\nCannot connect to mysql on host $mysqld! \n");
        return;
    }
=cut	
    # Find  the codeId for this description or get a new one
    # Limit the description to 30 since this is the size of the database field
	$desc=substr($desc,0,29);
	
    $query = dequote(<< "    ENDQUERY");
        select
            codeId
        from
            codes
        where
            codeDesc = '$desc'
    ENDQUERY
    $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {        
        $codeid=$tmp[0];
    }       
 
    # If this is a new description, add the code
    unless ($codeid) {
=pod	# 2012-11-16 - codeId was made to be auto-increment
        LogMsg("Getting new codeId for $desc");
        # Find the highest existing codeId and increment by one
        $query = dequote(<< "        ENDQUERY");
            select
                max(codeId)
            from
                codes            
        ENDQUERY
        $sth=$dbh->prepare("$query");
        $sth->execute();
        while (@tmp = $sth->fetchrow_array()) {        
            $codeid=$tmp[0];
        }           
        # Now ad the new one
        $codeid++;    
        LogMsg("Adding codeId $codeid to the database");            
        $query = dequote(<< "        ENDQUERY");
        insert into codes
            
        values      
            ($codeid,'$desc')
        ENDQUERY
    
        unless ($dbh->do($query) > 0) {
            LogMsg(indent(2)."Database Update Error");
            LogMsg(indent(2)."Driver=ODBC");
            LogMsg(indent(2).$dbh->{Name}); 
            LogMsg(indent(2)."query=".$query);
            LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
            LogMsg(indent(2)."errstr=".$dbh->errstr);   
            if ($verbose) {
                print "Inside update_storeinfo_db\n";
            }
            push(@email_messages, "ERROR: Failed to update database with $desc for store $storeid \n");            
        } else {
            LogMsg(indent(1)."Updated database with $desc for store $storeid ");
        }   
=cut		
        LogMsg("Adding Code Desc $desc to the database");            
        $query = dequote(<< "        ENDQUERY");
        insert into codes
            set codeDesc = '$desc'
 
        ENDQUERY
    
        unless ($dbh->do($query) > 0) {
            LogMsg(indent(2)."Database Update Error");
            LogMsg(indent(2)."Driver=ODBC");
            LogMsg(indent(2).$dbh->{Name}); 
            LogMsg(indent(2)."query=".$query);
            LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
            LogMsg(indent(2)."errstr=".$dbh->errstr);   
            if ($verbose) {
                print "Inside update_storeinfo_db\n";
            }
            push(@email_messages, "ERROR: Failed to update database with $desc for store $storeid \n");            
        } else {
            LogMsg(indent(1)."Updated database with $desc for store $storeid ");
        }  
		# Now get the new code id
        $query = dequote(<< "        ENDQUERY");
            select
                codeId
            from
                codes 
			where
				codeDesc = '$desc'
        ENDQUERY
        $sth=$dbh->prepare("$query");
        $sth->execute();
        while (@tmp = $sth->fetchrow_array()) {        
            $codeid=$tmp[0];
        }    		
    }     
   
    unless ($update_flag) {
        # Need to find what the last data was for this
        $query = dequote(<< "        ENDQUERY");
        select
            infoData,infoData2
        from
            storeInfo
        where
            codeId = $codeid
        and
            storeId = $storeid
        order by 
            date desc
        limit 1            
        ENDQUERY
		
 
		
        my $sth=$dbh->prepare("$query");
        unless ($sth->execute()) {
            LogMsg("Error executing query of pos_support...");
 
            sleep 5;
            LogMsg("Trying again...");
            $sth=$dbh->prepare("$query");
            unless ($sth->execute()) {
                LogMsg("Failed to query pos_support!");
                push(@email_messages, "\nFailed to query pos_support! \n");
 
                return;
            }     
        }
        my $prev_data='';
        my $prev_data2='';
        while (@tmp = $sth->fetchrow_array()) {        
            $prev_data=$tmp[0];
            $prev_data2=$tmp[1];
        }        	 
        if (defined($prev_data)) {         
            if ("$data" eq "$prev_data") {		 
                if (defined($prev_data2)) {				 
                    if (defined($data2)) {						 
                        unless ("$data2" eq "$prev_data2") {
                            # Both are defined but they differ
                            $update_flag=1;						 
                        }
                    }
                } elsif (defined($data2)) {
					# Previously no data2
					$update_flag=1;
				}
            } else {
                # Data differs from last entry so we need to enter this
                $update_flag=1;
            }
        } else {
            # We have no previous data so obviously we need to add this
            $update_flag=1;
        }  
    }
 
    unless ($update_flag) {
        # No need to enter this data		
        return;
    }  
    
    my $fields;
    my $data_to_enter;
    if ($data2) {
        $fields="(codeId,storeId,infoData,infoData2,date)";
        $data_to_enter="($codeid,$storeid,\'$data\',\'$data2\',\'$date_label2\')  ";
    } else {
       $fields="(codeId,storeId,infoData,date)";
       $data_to_enter="($codeid,$storeid,\'$data\',\'$date_label2\')  ";
    }
	if ($verbose) {
		LogMsg("Adding info to the database");
	}
    $query = dequote(<< "    ENDQUERY");
    $action into $table
        $fields
    values      
        $data_to_enter
    ENDQUERY
   
    unless ($dbh->do($query) > 0) {
        LogMsg(indent(2)."Database Update Error");
        LogMsg(indent(2)."Driver=ODBC");
        LogMsg(indent(2).$dbh->{Name}); 
        LogMsg(indent(2)."query=".$query);
        LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
        LogMsg(indent(2)."errstr=".$dbh->errstr); 
        if ($verbose) {
            print "Inside update_storeinfo_db II \n";
        }
        push(@email_messages, "ERROR: Failed to update database with $desc for store $storeid \n");            
    } else {
		if ($verbose) {
			LogMsg(indent(1)."Updated database with $desc for store $storeid ");
		}
        #push(@email_messages, "updated database with $desc for store $storeid \n");
    }   
    #$dbh->disconnect;    
}

sub update_store_info_file {
	unless ($mssql_dbh) {
		LogMsg("Cannot connect to mssql_dbh");
		return;
	}
	 
	# Read the current values
	my $update_store_info = 0;
	if (-f $store_info_file) {
		my %tmp_store_id_hash = ();
		my %tmp_store_IP_hash = ();
		open(INFO,"$store_info_file");
		my @store_info_list=(<INFO>);
		close INFO;
		foreach my $s (@store_info_list) {
			chomp($s);
			my @tmp=split(/,/,$s);
			my $label = $tmp[0];
			my $storenum = $tmp[1];
			my $store_line = $tmp[2];
			my $StoreCode = $tmp[3];			
			if ($label eq "StoreCode") {
				$tmp_store_id_hash{$storenum} = "$store_line";	
				 
			}
		}
		foreach my $s (@store_info_list) {
			chomp($s);
			my @tmp=split(/,/,$s);
			my $label = $tmp[0];
			my $storename = $tmp[1];
			my $storenum = $tmp[2];
			my $IP = $tmp[3];			
			if ($label eq "IP") {
				$tmp_store_IP_hash{$storename}=["$IP","$storenum"];  	
			}
		}	
		foreach my $key (keys(%store_id_hash)) {
			if ($tmp_store_id_hash{$key}) {
				unless ($tmp_store_id_hash{$key} eq $store_id_hash{$key}) {
					$update_store_info++;
				}
			} else {
				$update_store_info++;
			}
		}
		foreach my $key (keys(%IP_hash)) {
			if ($tmp_store_IP_hash{$key}) {
				my $IP=$IP_hash{$key}->[0];       
				my $storenum=$IP_hash{$key}->[1];  
				my $tIP=$tmp_store_IP_hash{$key}->[0];       
				my $tstorenum=$tmp_store_IP_hash{$key}->[1];  				
				unless ($IP eq $tIP) {
					$update_store_info++;
				}
				unless ($storenum eq $tstorenum) {
					$update_store_info++;
				}				
			} else {
				$update_store_info++;
			}
		}		
	} else {
		$update_store_info = 1;
	}
	if ($update_store_info) {
		LogMsg("Updating $store_info_file");
		open(INFO,">$store_info_file");
		foreach my $key (sort keys(%store_id_hash)) {
			my $label = "StoreCode";
			my $store_line = $store_id_hash{$key};
			my $storenum = $key;
			print INFO "$label,$storenum,$store_line\n";			 
		}
		foreach my $storename (sort keys(%IP_hash)) {
			my $label = "IP";
			my $IP=$IP_hash{$storename}->[0];       
			my $storenum=$IP_hash{$storename}->[1];  
			print INFO "$label,$storename,$storenum,$IP\n";	
 			
		}		
	}
	
}
 
sub update_codes {
    # If you update the storeinfo_hash_on_update or storeinfo_hash, this updates the database with the new values
	# 2012-11-16 - I revised the definition of the codes table so I think this is no longer needed - kdg
	return;
    my %code_hash;
    my %existing_code_hash;
    my @tmp;
 
    # First, combine these two hashes into one
    foreach my $key (keys(%storeinfo_hash_on_update)) {
        $code_hash{$key} = $storeinfo_hash_on_update{$key};
 
    }
    foreach my $key (keys(%storeinfo_hash)) {
        $code_hash{$key} = $storeinfo_hash{$key};
 
    }
=pod 
    # Get the current codes
    unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) {        
        return;
    }
=cut	
    # Find known codes
    my $query="select 
        codeId,
        codeDesc
    from 
        codes
    ";    
    my $sth=$dbh->prepare("$query");
    unless ($sth->execute()) {
        LogMsg("Error executing query of pos_support...");
        sleep 5;
        LogMsg("Trying again...");
        $sth=$dbh->prepare("$query");
        unless ($sth->execute()) {
            LogMsg("Failed to query pos_support!");
            push(@email_messages, "\nFailed to query pos_support! \n");
            return;
        }     
    }    
    while (@tmp = $sth->fetchrow_array()) {   
        my $key=$tmp[1];
        my $value=$tmp[0];
        $existing_code_hash{$key} = $value;        
    }    
    #$dbh->disconnect;
    # Now, compare codes
    foreach my $key (keys(%code_hash)) {        
        if (defined($existing_code_hash{$key})) {
            unless ($existing_code_hash{$key} eq $code_hash{$key}) {                           
                LogMsg("Need to replace code $key with desc $code_hash{$key}");
                update_code_db("$code_hash{$key}","$key");            
            }
        } else {            
            LogMsg("Need to add code $key with desc $code_hash{$key}");
            update_code_db("$code_hash{$key}","$key");            
        }
    }
}

sub update_code_db {
    my $code=shift;
    my $desc=shift;
    
    my $query = dequote(<< "    ENDQUERY");
    replace into codes
        (codeId,codeDesc)
    values
        ($code,'$desc')
    ENDQUERY
=pod    
    unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) {        
        return;
    }
=cut	
    unless ($dbh->do($query) > 0) {
        LogMsg(indent(2)."Database Update Error");
        LogMsg(indent(2)."Driver=ODBC");
        LogMsg(indent(2).$dbh->{Name}); 
        LogMsg(indent(2)."query=".$query);
        LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
        LogMsg(indent(2)."errstr=".$dbh->errstr);   
        if ($verbose) {
            print "Inside update_code_db\n";
        }
        push(@email_messages, "ERROR: Failed to update database with code $code description $desc\n");            
    } 
    #$dbh->disconnect;
}

sub update_known_issues_db {
    # Update the database
    LogMsg("Updating store issues database...");
    my @tmp=();
    my @tmpk=();
    my %known_issues=();    
    my @known_issues_array=();
    my @current_issues=();
=pod    
    if ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) {
        if ($verbose) {
            LogMsg("Building query...");
        }
    } else {
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email_messages, "\nCannot connect to mysql on host $mysqld! \n");
        return;
    }
=cut    
    # Find known issues
    my $query="select store_id,store_name,description,status from storeIssues where status not like 'RESOLVED' order by store_id";    
    my $sth=$dbh->prepare("$query");
    unless ($sth->execute()) {
        LogMsg("Error executing query of pos_support...");
        sleep 5;
        LogMsg("Trying again...");
        $sth=$dbh->prepare("$query");
        unless ($sth->execute()) {
            LogMsg("Failed to query pos_support!");
            push(@email_messages, "\nFailed to query pos_support! \n");
            return;
        }     
    }
    
    while (@tmp = $sth->fetchrow_array()) {        
        $known_issues{"$tmp[0]|$tmp[2]"}=1;
        push(@known_issues_array,"@tmp");
    }

    # Compare the issues found today with the ones previously known.  The previously known ones we read from the $known_issues file.
    # NOTE: This process should never take an issue off of the list unless is it listed as REPAIRED.
    my $new_issue_count=0;
    push(@email_messages, "\n\n - - - - - - - - - - - - - - - - \n");	    
    # Issues_found are the issues found today.
    foreach my $new_issue (@issues_found) {      
        my $new=1;        
        @tmp = split(/\|/,$new_issue); 
        next unless ($#tmp > -1);
        # Look at the store number, store name, and description to determine a unique entry
      
        my $issue=$tmp[2];
        # Remove any single quotes        
        $issue=~s/\'//g;
        my $new_entry="$tmp[0]|$issue";              
        my $new_entry_data="\'$tmp[0]\',\'$tmp[1]\',\'$issue\',\'$tmp[3]\',null";             
        if ($known_issues{$new_entry}) {         
            $new=0;         
        }    
    
        if ($new) {       
            # This is a new issue
            # Put this new on the current issues array   
            push(@current_issues,"$new_entry_data");            
            $new_issue_count++;   
        }         
    }	
    if ($new_issue_count) {
        # Insert into the database here
        $query="insert into storeIssues (store_id,store_name,description,status,date) values ";   
        my $sep="";
        foreach my $c (@current_issues) {

            $query.= "$sep($c)";
            $sep=",";
        }      
        
        $sth=$dbh->prepare("$query");
        if ($sth->execute()) {            
            LogMsg(indent(1)."Update database with $new_issue_count new issues");
        } else {
            LogMsg(indent(1)."Query ($query)");
            if ($verbose) {
                print "Inside update_known_issues_db\n";
            }            
            LogMsg(indent(1)."ERROR: Failed to update database with $new_issue_count new issues");
        }
        
    }
        
    my $issue_count=($#known_issues_array + 1);
    my $current_count=($new_issue_count + $issue_count);
    push(@email_messages, "\nPrevious Issue Count: $issue_count \n");	
    push(@email_messages, "New Issue Count: $new_issue_count \n");   
    push(@email_messages, "Total Issue Count: $current_count \n\n");  
    if ($current_count) {
        push(@email_messages, "---------------Current Issues--------------------\n");
    }
    my $issue_counter=1;
    foreach my $i (@known_issues_array) {           
        push(@email_messages, "#$issue_counter: $i\n");   
        $issue_counter++;
    }
    foreach my $c (@current_issues) {    
        $c=~s/'//g;
        $c=~s/,/ /g;
        push(@email_messages, "#$issue_counter: $c\n");   
        $issue_counter++;        
    }
   # $dbh->disconnect();
}

sub submit_issue_to_db {
    # Update the database
    my $storeid=shift;
    my $storename=shift;
    my $description=shift;
    my $status="NEW";    
       
    LogMsg("Submitting an issue to store issues database...");
    my @tmp=();
    my @tmpk=();
    my %known_issues=();    
    my @known_issues_array=();
    my @current_issues=();
=pod	
    if ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) {
        if ($verbose) {
            LogMsg("Building query...");
        }
    } else {
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email_messages, "\nCannot connect to mysql on host $mysqld! \n");
        return;
    }
=cut    
    # Check to see if this is already an open issue
    my $query = dequote(<< "	ENDQUERY");

    select 
        store_id,store_name,description,status 
    from 
        storeIssues 
    where 
        status not like 'RESOLVED' 
        and description like '$description'
        and store_id like '$storeid'
    order by store_id
	ENDQUERY
    #
    my $found=0; 
  
    my $sth=$dbh->prepare("$query");
    unless ($sth->execute()) {
        LogMsg("Error executing query of pos_support...");
        sleep 5;
        LogMsg("Trying again...");
        $sth=$dbh->prepare("$query");
        unless ($sth->execute()) {
            LogMsg("Failed to query pos_support!");
            push(@email_messages, "\nFailed to query pos_support! \n");
            return;
        }     
    }
    
    while (@tmp = $sth->fetchrow_array()) {       
        if (($tmp[0] == $storeid) && ($tmp[2] eq "$description")) {
            $found=1;
            LogMsg("This issue is already found");
        }
    }
    
    unless ($found) {
        LogMsg("Adding this issue to the database");
        $query = dequote(<< "        ENDQUERY");
        insert into storeIssues
            (store_id,store_name,description,status,date)
        values
            ($storeid,'$storename','$description','$status','$date_label2')
        ENDQUERY
   
        unless ($dbh->do($query) > 0) {
            LogMsg(indent(2)."Database Update Error");
            LogMsg(indent(2)."Driver=ODBC");
            LogMsg(indent(2).$dbh->{Name}); 
            LogMsg(indent(2)."query=".$query);
            LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
            LogMsg(indent(2)."errstr=".$dbh->errstr);  
            if ($verbose) {
                print "Inside submit_issue_to_db\n";
            }                        
            push(@email_messages, "ERROR: Failed to update database with $description for store $storeid $storename\n");            
        } else {
            LogMsg(indent(1)."Updated database with $description for store $storeid $storename");
            push(@email_messages, "updated database with $description for store $storeid $storename\n");
        }            
     
    }        
    #$dbh->disconnect();
}

sub report_misc_backup_info {
    push(@email_messages, "\n\n---------------Throuput--------------------\n");
    foreach my $s (reverse sort {$throughput_hash{$a}<=>$throughput_hash{$b}} keys (%throughput_hash)) {
 
        my $storename=$store_info_hash{$s}->[0];        
        push(@email_messages, "$s - $storename: $throughput_hash{$s} bps\n");
    }

}

sub report_misc_info {
    if ($#misc_errors > -1) {    
        push(@email_messages, "\n\n---------------Miscellaneous Errors--------------------\n");
        foreach my $v (@misc_errors) {
            $error_count++;
            push(@email_messages, "-->ERROR #${error_count}: - $v\n");     
        }
    }
    if ($#SysInfo > -1) {   
        push(@email_messages, "\n\n---------------System Info--------------------\n");
        foreach my $v (@SysInfo) {
            push(@email_messages, " - $v\n");     
        }
    }    
    if ($#Tenders > -1) {   
        push(@email_messages, "\n\n---------------Tender Info--------------------\n");
        foreach my $v (@Tenders) {
            push(@email_messages, " - $v\n");     
        }     
    
    }   
    if ($#UPS > -1) {   
        push(@email_messages, "\n\n---------------UPS Connection Info--------------------\n");
        foreach my $v (@UPS) {
            push(@email_messages, " - $v\n");     
        }     
    
    }      
    if ($#TCounters > -1) {
       push(@email_messages, "\n\n---------------Traffic Counter Info--------------------\n");
        foreach my $v (@TCounters) {
            push(@email_messages, " - $v\n");     
        }      
    }
    if ($#TCounterAdj > -1) {
        push(@email_messages, "\n\n---------------Traffic Counter Adjustments--------------------\n");
        foreach my $v (@TCounterAdj) {
            push(@email_messages, " - $v\n");     
        }                
    }
    if ($#TCounterType > -1) {
        push(@email_messages, "\n\n---------------Traffic Counter Version--------------------\n");
        foreach my $v (@TCounterType) {
            push(@email_messages, " - $v\n");     
        }                
    }
    if ($#noInternet > -1) {
        push(@email_messages, "\n\n---------------No Internet Connectivity Count--------------------\n");
        foreach my $v (@noInternet) {
            push(@email_messages, " - $v\n");     
        }                
    }    
}

sub prepend_summary {

    # Some information we want to put at the beginning of the report
    if ($status_only) {
        # Don't run this if it is a status only report
        return;
    }
	LogMsg("Prepend Summary");
    # Internet connectivity
    my @tmp;
    my $count=0;
    push(@tmp, "Top Three Stores:  Days since reboot\n");
    foreach my $s (reverse sort {$upDay_hash{$a}<=>$upDay_hash{$b}} keys (%upDay_hash)) {
        my $storename=$store_info_hash{$s}->[0];        
        push(@tmp, "$s - $storename: $upDay_hash{$s} days\n");
        $count++;
        if ($count > 2) {
            last;
        }
    }
    $count=0;
    push(@tmp,"\n");
    push(@tmp, "Worst Three Stores:  No Internet Count\n");    
    foreach my $s (reverse sort {$noInternet_hash{$a}<=>$noInternet_hash{$b}} keys (%noInternet_hash)) {
        my $storename=$store_info_hash{$s}->[0];              
        push(@tmp, "$s - $storename: $noInternet_hash{$s} counts\n");
        $count++;
        if ($count > 2) {
            last;
        }        
    }
=pod	# Retired 2016-02-08
	if (keys(%ednaIssues_hash)) {
		# Other Important Messages
		push(@tmp,"\n");
		push(@tmp, "Important Issues Today:\n");    
		foreach my $issue (sort keys (%ednaIssues_hash)) {
			my $stores=$ednaIssues_hash{$issue};
			my %store_hash;
			my @stmp=split(/\s+/,$stores);
			foreach my $s (@stmp) {
				# Get the store name for this store
			 
				my $storename=$store_info_hash{$s}->[0];
				$store_hash{$s}=$storename;
			}
			foreach my $store (sort keys(%store_hash)) {
				push(@tmp, "$issue: $store - $store_hash{$store}\n");   
			}				    
		}	
	} else {
		push(@tmp,"\n");
		push(@tmp, "No Especially Important Issues Detected Today\n"); 		
	}
=cut	
	#debug
	if (keys(%level1Issues_hash)) {
		# Primary Importance Messages
		push(@tmp,"\n");
		push(@tmp, "Primary Issues Today:\n");    
		foreach my $issue (sort keys (%level1Issues_hash)) {
			my $stores=$level1Issues_hash{$issue};
			my %store_hash;
			my @stmp=split(/\s+/,$stores);
			foreach my $s (@stmp) {
				# Get the store name for this store
			 
				my $storename=$store_info_hash{$s}->[0];
				$store_hash{$s}=$storename;
			}
			foreach my $store (sort keys(%store_hash)) {
				push(@tmp, "$issue: $store - $store_hash{$store}\n");   
			}				    
		}	
	} else {
		push(@tmp,"\n");
		push(@tmp, "No Primary Issues Detected Today\n"); 		
	}	
	if (keys(%level2Issues_hash)) {
		# Secondary Importance Messages
		push(@tmp,"\n");
		push(@tmp, "Secondary Issues Today:\n");    
		foreach my $issue (sort keys (%level2Issues_hash)) {
			my $stores=$level2Issues_hash{$issue};
			my %store_hash;
			my @stmp=split(/\s+/,$stores);
			foreach my $s (@stmp) {
				# Get the store name for this store
			 
				my $storename=$store_info_hash{$s}->[0];
				$store_hash{$s}=$storename;
			}
			foreach my $store (sort keys(%store_hash)) {
				push(@tmp, "$issue: $store - $store_hash{$store}\n");   
			}				    
		}	
	} else {
		push(@tmp,"\n");
		push(@tmp, "No Secondary Issues Detected Today\n"); 		
	}		
	###
    push(@tmp,"\n---------------------------------------\n");
	if (keys(%edna_warn_hash)) {
		LogMsg("Prepend email_messages_reg");
		foreach my $r (sort keys(%edna_warn_hash)) {	
			# We filter out the stats we are particularly interested in here
			if ($r =~ /Windows dump/) {								
				push(@tmp,"There were $edna_warn_hash{$r} instances of: $r \n");			
			}
		}
		push(@tmp,"\n---------------------------------------\n");
	}	
    # Put the temp array at the beginning of the email_messages
    @email_messages=(@tmp,@email_messages);
	@tmp=();
	if (keys(%reg_warn_hash)) {
		LogMsg("Prepend email_messages_reg");
		foreach my $r (sort keys(%reg_warn_hash)) {	
			# We filter out the stats we are particularly interested in here
			if ($r =~ /Windows dump/) {			
				push(@tmp,"There were $reg_warn_hash{$r} instances of: $r \n");			
			}
		}
		push(@tmp,"\n---------------------------------------\n");
		# Put the temp array at the beginning of the email_messages_reg
		@email_messages_reg=(@tmp,@email_messages_reg);	
	}
}

sub usage {
	print "\nUsage:\n\n";
	print "processDailyReports.pl [-debug] [-t] [-c] [-b] [-m] [-n] [-po] [-v] [-day=<day>] [-storeid=<storelist>] [-e=<exclude>]\n\n";
	print "-debug\tdebug mode.  Only processes store #2760 and #2918\n";
    print "-test\t\ttest mode.  Does not send email to any cc addresses.\n";
	print "-c\t\tcurrent view.  Read from i_toolshed at each store the current backup and monitor reports.\n";
	print "-b\t\tBackup reports only\n";
	print "-monitor\tMonitor reports only\n";
	print "-mixmatch\tMixMatch reports only\n";
	print "-n\t\tNo email is sent\n";
    print "-po\t\tProcess open po list to verify the orders have been received\n";
	print "-v\t\tVerbose mode.\n";
    print "-day=<day>\tSpecify how many days in the past to process the report.  This is intended to be used\n";
    print "\t\tonly with the -po option.\n";
    print "-storeid=<storelist>\tSpecify a comma limited list of store id numbers to process.\n";
    print "-e=<exclude>\tSpecify a comma limited list of store id numbers to exclude.\n";
	print "\t\tNote: options -b and -m should not be used together.\n";
    print "-ttvsftp\tCheck the backups to ttv-sftp1 only.\n";
	print "-alert\t\tUsed in conjunction with -c and -m to also send alerts to stores and RSMs\n";
	print "-eDSR\t\teDSR report only\n";
	print "-verifone\t\tVeriFone report only\n";
 
	exit;
}

sub ping_site {
	my $target=shift;
    my @response = `ping $target`;    
    # Look for the summary line
    my $summary;
    foreach my $r (@response) {
        if ($r =~ /packets:/i) {
            my @tmp=split(/\(/,$r);
            $summary=$tmp[1];
            @tmp=split(/%/,$summary);
            $summary=$tmp[0];            
        }
    }        
    LogMsg(indent(1)."Ping to target $target had $summary % packet lost");
	if ($summary == 0) {
        # We lost no packets - Safe to connect to site
		return 1;
	} else {
        # We lost packets - Don't bother trying to connect
		return 0;
	}
}

sub process_alerts {
	unless ($mssql_dbh) {		
		return;
	}
	my $storeid=shift;
	LogMsg("Processing Alert Emails for store $storeid");
	my $ref=shift;
	my @messages=@$ref;
	my $storename=$store_info_hash{$storeid}->[0];
	$storename=lc($storename);
	$storename=~s/\.//g;
	$storename=~s/ //g;     	 
	# Get the manager email associated with this store 
	my $manager;
	my $manager_email;
	my $manager_email2 = "none";
	my $rsm_code;
	my $rsm;
	# The following hash is because there does not seem to be a reliable
	# source to get email addresses from.
	my %rsm_email_hash = (
		'Karen Dooley' => 'k.dooley2@verizon.net',
		'Jennifer Hoke Bentivogli' => 'Jennifer.Bentivogli@tenthousandvillages.com'
	);
	%rsm_email_hash = (	
		'Jennifer Hoke Bentivogli' => 'Jennifer.Bentivogli@tenthousandvillages.com',
		'Karen Dooley' => 'Karen.Dooley@tenthousandvillages.com',
		'Kate McMahon' => 'kate.mcmahon@tenthousandvillages.com',
		'Jim Gittings' => 'jim.gittings@tenthousandvillages.com',
	);	
	
	# If we need to send the email to a second RSM.  For example, if one RSM is covering for another,
	# associate the original RSM with the covering RSM here.
	#my %rsm_covering_email_hash = (
	#	'DBU' => 'donna.duerksen@tenthousandvillages.com'
	#);
	my %rsm_covering_email_hash=(
		 
	);
	# Variable to allow an RSM to get email for a store that they are not assigned to in Navision
	#my %rsm_covering_store_email_hash = (
	#	'7001' => 'dave.mcclintock@tenthousandvillages.com',
	#	'2048' => 'dave.mcclintock@tenthousandvillages.com',
	#	'2847' => 'dave.mcclintock@tenthousandvillages.com',
	#	'3070' => 'dave.mcclintock@tenthousandvillages.com',
	#	'5290' => 'dave.mcclintock@tenthousandvillages.com',
	#	'7060' => 'dave.mcclintock@tenthousandvillages.com',
	#);	
	my %rsm_covering_store_email_hash=();	
	# If we need to send the manager email to a second address.  For example, if a store staff is covering for the manager
	# associate the original manager with the covering staff here.
	my %manager_covering_email_hash = (
		'manager.media@tenthousandvillages.com' => 'media@tenthousandvillages.com'
	);	
	
	
	my $rsm_email;
	my $rsm_email2 = "none";
	my @email_note;
	my $query = "
	SELECT  
		  [Contact]
		  ,[E-Mail]
		  ,[Customer Posting Group]
	FROM 
		[dbNavision].[dbo].[Villages\$Customer]
	where 
		[No_] = '$storeid'
	";	
	#my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
 
    
	my $sth = $mssql_dbh->prepare($query);
 
	unless ($sth->execute()) {
		LogMsg(indent(1)."WARNING: Could not connect to dbNavision - Cannot process alerts");
		#print "\nCould not connect to dbNavision - Cannot process alerts\n";
		return;
	}   
 
    while (my @tmp=$sth->fetchrow_array()) {
		$manager=$tmp[0];
		$manager_email=$tmp[1];
	}
	# Here the manager email may be sent to a second person if the manager is going to be out for a while.
	# See if there is a covering RSM
	if ($manager_covering_email_hash{$manager_email}) {
		$manager_email2=$manager_covering_email_hash{$manager_email};
	}	
 
	###
	# Get the RSM for this store
	$query= "select 	
		distinct(vc.[Salesperson Code]),
		vsp.[Name]
	FROM [dbNavision].[dbo].[Villages\$Customer]  vc
	join [dbNavision].[dbo].[Villages\$Salesperson_Purchaser] vsp on vc.[Salesperson Code] = vsp.[Code]
	where 
		vc.[No_] = '$storeid' 

	";	
	$sth = $mssql_dbh->prepare($query);
	$sth->execute();
 
    while (my @tmp=$sth->fetchrow_array()) {
		$rsm_code=$tmp[0];
		$rsm=$tmp[1];
	}	
	#$dbh->disconnect;
	my @tmp=split(/\s+/,$rsm);
	$rsm_email=lc("$tmp[0]"."."."$tmp[1]"."\@tenthousandvillages.com");
	
	# See if there is a covering RSM
	if ($rsm_covering_email_hash{$rsm_code}) {
		$rsm_email=$rsm_covering_email_hash{$rsm_code};
	}
	# See if there is a covering RSM by StoreId
	if ($rsm_covering_store_email_hash{$storeid}) {
		$rsm_email=$rsm_covering_store_email_hash{$storeid};
	}	
	
	
	
	###
	# Unfortunately, the RSM's email address does not appear to be maintained in Navision so we will get them from The Wire
    my $host="192.168.21.25";
	$host="192.168.37.27";	# New IP for The Wire when it moved to the DMZ - '15-01-09 - kdg
    my $user="thereporter";
    my $pw="thereporter";
    my $db_selection="wire"; 

    my $dbh_wire=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");	
	
	$query="
		select mail from users where mail like '$rsm_email'
	";
	$sth = $dbh_wire->prepare($query);
	$sth->execute();
	
	my $email_confirmed=0;
    while (my @tmp=$sth->fetchrow_array()) {
		my $found=lc($tmp[0]);
		 
		if ($rsm_email eq $found) {
			$email_confirmed=1;
		}
	}		
	unless ($email_confirmed) {
		# Check to see if we have a known email for the rsm		
		if ($rsm_email_hash{$rsm}) {		
			$rsm_email = $rsm_email_hash{$rsm};
			$email_confirmed=1;		
		}
	}
	
	$dbh_wire->disconnect;
	my $alert_count=0;
	my $alert_label="ALERT";
	foreach $email (@messages) {
		if ($email =~ /alert:/i) {
			$alert_count++;
		}
	}	
	if ($alert_count > 1) {
		$alert_label="ALERTS";
	}
	
	# Add some basic notes to the alert message
	my $msg="\n\n\nhttp://edna/cgi-bin/i_reports.pl?report=ALERT_RPT\n";
	push(@messages,$msg);
	$msg="\ncontact pos.support\@tenthousandvillages.com \nor call POS Support at (717) 859-8111 if you need assistance.\n";
	push(@messages,$msg);
	if  ($nomail) {
		LogMsg("Alert messages: ");
		if ($rsm_email) {
			print "Send to RSM: $rsm_email\n";
		}
		if ($rsm_email2) {
			unless ($rsm_email2 eq "none") {
				print "Send to RSM2: $rsm_email2\n";
			}
		}	
		if ($manager_email) {
			print "Send to Manager: $manager_email\n";
		}	
		if ($manager_email2) {
			unless ($manager_email2 eq "none") {
				print "Send to Manager: $manager_email2\n";
			}
		}				
		foreach $email (@messages) {
			print "$email";
		}	
	} else {
		if ($email_confirmed) {
 
			send_email_note(\@messages, "$storeid : $storename - $alert_count $alert_label", $manager_email, $rsm_email,$storeid, $rsm_email2, $manager_email);		
 
		} else {
			# Send Kevin an email alerting him to the fact that we could not confirm the email for the RSM
			push(@email_note,"Cannot confirm email address for $rsm as $rsm_email");
			send_email_note(\@email_note, "$storeid : $storename - NOTE");	
			send_email_note(\@messages, "$storeid : $storename - $alert_count $alert_label", $manager_email, "",$storeid);		
		}	
	}


	###
}

sub check_dbh {
	# Just to check connectivity to database to get RSM info
	my $host="192.168.37.27";	# New IP for The Wire when it moved to the DMZ - '15-01-09 - kdg
    my $user="thereporter";
    my $pw="thereporter";
	 
    my $db_selection="wire"; 
	my $rsm_email="donna.duerksen\@tenthousandvillages.com";
    my $dbh_wire=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");	
	unless ($dbh_wire) {
		LogMsg("Failed to create connection to $host $db_selection");
		return;
	}
	my $query="
		select mail from users where mail like '$rsm_email'
	";
	my $sth = $dbh_wire->prepare($query);
	$sth->execute();
	
	my $email_confirmed=0;
    while (my @tmp=$sth->fetchrow_array()) {
		my $found=lc($tmp[0]);
		print "$found\n"; 
	}		
}

sub send_email_note{
	#SMTP vars
	my $array_ref=shift;
	my $specified=shift;	 
	my $manager_email=shift;
	my $rsm_email=shift;
	my $storeid=shift;
	my $rsm_email2=shift;
	my $manager_email2=shift;
	my $sending_email="pos.support\@tenthousandvillages.com";
	#$specified.="\@tenthousandvillages.com";
	my @message_array=@$array_ref;
	my $smtp="";
	my $include_kevin=1;
	if ($test_mode) {
		$manager_email='';
		$rsm_email='';
		$rsm_email2='';
		$manager_email2='';
	}	   
    # Connect to the server
    
    LogMsg("Sending email message to $Mailserver...");
 
    $smtp = Net::SMTP->new("$Mailserver", Hello=>'backupadmin', Debug=>0);
    die "Couldn't connect to server" unless $smtp;
    unless($smtp->mail($sending_email)) {
        LogMsg("Error with ($sending_email)");
    }
	# Here is where we send to the administrator (Mailto) if the email is a "NOTE"
	if ($specified =~ /NOTE/) {
		if ($smtp->to($Mailto)) {
			LogMsg("Sent $specified to $Mailto");
		} else {
			LogMsg("Error sending email to ($Mailto)");
		}
	}
	if (1) {
		if ($manager_email) {
			# Here is where the email is sent to the store manager.
			if ($smtp->to($manager_email)) {
				LogMsg("Sent $specified to manager: $manager_email");
			} else {
				LogMsg("WARNING: Error sending email to manager: ($manager_email)");
			}	
		}
		#$smtp->to($manager_email);
		if ($manager_email2) {
			unless ($manager_email2 eq "none") {
				# Here is where the email is sent to a second address.
				
				if ($smtp->to($manager_email2)) {
					LogMsg("Sent $specified to address: $manager_email2");
				} else {
					LogMsg("WARNING: Error sending email to address ($manager_email2)");
				}	
			}
		}			
		if ($rsm_email) {
			# Here is where the email is sent to the RSM.
			#$smtp->to($rsm_email); 
			if ($smtp->to($rsm_email)) {
				LogMsg("Sent $specified to RSM: $rsm_email");
			} else {
				LogMsg("WARNING: Error sending email to RSM ($rsm_email)");
			}			
		}
		if ($rsm_email2) {
			unless ($rsm_email2 eq "none") {
				# Here is where the email is sent to the RSM.
				#$smtp->to($rsm_email); 
				if ($smtp->to($rsm_email2)) {
					LogMsg("Sent $specified to RSM: $rsm_email2");
				} else {
					LogMsg("WARNING: Error sending email to RSM ($rsm_email2)");
				}	
			}
		}		
	 
		if ($include_kevin) {
			if ($smtp->to($Mailto)) {
				LogMsg("Sent $specified to Admin: $Mailto");
			} else {
				LogMsg("WARNING: Error sending email to Admin ($Mailto)");
			}			
		}
	}
 
    #start mail
    if ($smtp->data()) {
		if ($verbose) {
			LogMsg("Starting mail");
		}
    } else {
        LogMsg("Error starting mail");
    }

	if (defined($storeid)) {
		#send header
		if ($smtp->datasend("To: Ten Thousand Villages Store #$storeid\n")) {
			if ($verbose) {
				LogMsg("Send header");
			}
		} else {
			LogMsg("Error sending header");
		}
	}
	if ($smtp->datasend("Subject:Store $specified\n\n")) {
		if ($verbose) {
			LogMsg("Send Subject");
		}
	} else {
		LogMsg("Error sending Subject");
	}
	 
    foreach my $line (@message_array) {	 
        unless($smtp->datasend("$line")) {
            LogMsg("Error sending line");
        }
    }

    # End the mail
    if ($smtp->dataend()) {
		if ($verbose) {
			LogMsg("Data end");
		}
    } else {
        LogMsg("Error ending data");
    }
    if ($smtp->quit()) {
		if ($verbose) {
			LogMsg("Quiting email");
		}
    } else {
        LogMsg("Error quiting email");
    }
    LogMsg("Finished sending.");
}
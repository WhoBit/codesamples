#!/usr/bin/perl -w

## wsmonitor.pl
##
## Used to monitor state of backoffice workstations in the store - kdg
##
## 2012-06-06 - v1.0.0 - created - kdg
## 2012-06-13 - v1.0.1 - Added some registry changes for XP for Symantec AntiVirus - kdg
## 2012-06-14 - v1.0.2 - Skip blank lines reading SAV logs - kdg
## 2012-06-15 - v1.0.3 - Revised updating to get .bat, .pl, or .reg files from server - kdg
## 2012-07-09 - v1.0.4 - Revised save_log to use hostname - kdg
## 2012-07-11 - v1.0.5 - Disabled setting possible_virus by flag - kdg
## 2012-10-09 - v1.0.6 - Disabled warnings about saving log.  Disabled notice about finding def update in log - kdg
## 2012-10-15 - v1.0.7 - Enabled warning about TTV is not configured as a TCPIPRemoteName in pcAnywhere - kdg
## 2012-10-29 - v1.0.8 - Changed warning about virus update to a notice - kdg
## 2012-12-20 - v1.0.9 - Added some support for Symantec Endpoint Protection - kdg
## 2012-12-28 - v1.0.10 - Disabled checking for backup purging for Symantec Endpoint Protection.  Set logrolloverdays for SEP - kdg
## 2012-12-31 - v1.0.11 - Enabled quarantine and backup purging for SEP - kdg
## 2013-01-03 - v1.0.12 - Enabled quarantine and backup purging for SEP 32bit - kdg
## 2013-01-25 - v1.0.13 - Improved determining which antivirus is used - kdg
## 2013-01-31 - v1.0.14 - Collecting Windows 7 architecture - kdg
## 2013-03-26 - v1.0.15 - Incorporating read_symantec_log - kdg
## 2013-04-01 - v1.0.16 - Reformatting ISSUE: for antivirus issues - kdg
## 2013-04-23 - v1.0.17 - Added check_perl_updates - kdg
## 2013-04-30 - v1.1.0 - Added call to vpdn_lu.exe - kdg
## 2013-05-08 - v1.1.1 - Added gather_sysinfo.  Disabled check_pcAnywhere - kdg
## 2013-05-14 - v1.1.2 - Revised how the symantec log is determined.  Some cleanup of obsolete code. - kdg
## 2013-05-17 - v1.1.3 - Corrected locating antivirusquarantine - kdg
## 2013-05-21 - v1.1.4 - Added update_tool_alt.  Refined searching registry for Symantech - kdg
## 2013-05-28 - v1.1.5 - Improvements to finding the correct registry settings in checking antivirus - kdg
## 2013-06-26 - v1.1.6 - Corrections to defining fix_file.  Checking for virus scans - kdg
## 2013-06-28 - v1.1.7 - Added check_windows_updates - kdg
## 2013-07-01 - v1.1.8 - Added check for custom scheduled scan - kdg
## 2013-07-02 - v1.1.9 - Warn about no scan only on Monday (for now at least) - kdg
## 2013-07-23 - v1.1.10 - Fixed finding custom scheduled scan for LanDesk - kdg
## 2013-07-26 - v1.1.11 - Added $no_action - kdg
## 2013-07-31 - v1.2.0 - Beginning to add support for SEP 12 - kdg
## 2013-08-07 - v1.2.1 - Check for unmanaged SEP - kdg
## 2013-08-09 - v1.2.2 - Using library - kdg
## 2013-08-12 - v1.2.3 - Getting virus defs on Windows 7 64-bit - kdg
## 2013-08-16 - v1.2.4 - Some refinements for Windows 7 64-bit - kdg
## 2013-08-18 - v1.2.5 - SEP 12 now has a warning - kdg
## 2013-08-21 - v1.2.6 - Win7 gets virus defs from registry - kdg
## 2013-08-26 - v1.2.7 - Moved getting virus defs to get_def_info - kdg
## 2013-08-27 - v1.2.8 - Updates for Bo5hallway - kdg
## 2013-09-16 - v1.2.9 - Added hostname to log - kdg
## 2013-09-20 - v1.2.10 - Re-enabled schedule_updates - kdg
## 2013-09-21 - v1.2.11 - Save local copy of the log.  Updated checking windows updates. - kdg
## 2013-11-06 - v1.2.12 - Corrected a problem with save_log - kdg
## 2013-11-14 - v1.2.13 - Updated path to perl modules on Edna - kdg
## 2013-11-15 - v1.3.0 - Added check_reboots - kdg
## 2013-12-13 - v1.3.1 - Adjusted check_windows_updates to get_update_count from a function - kdg
## 2013-12-24 - v1.3.2 - Added check_syncback - kdg
## 2013-12-26 - v1.3.3 - Refined parsing & logging in check_syncback - kdg
## 2013-12-26 - v1.3.4 - Added check_antivirus_scan - kdg
## 2013-12-27 - v1.4.0 - Added check_misc - kdg
## 2013-12-30 - v1.4.1 - Revised checking syncback on an XP system - kdg
## 2014-01-02 - v1.4.2 - Revised to exclude Cumming from check_syncback - kdg
## 2014-01-03 - v1.4.3 - Excluding some more systems from certain checks - kdg
## 2014-01-03 - v1.4.4 - Added check_hostname and updated check_syncback - kdg
## 2014-01-06 - v1.5.0 - Added data_backup support - kdg
## 2014-01-08 - v1.5.1 - Minor logging update - kdg
## 2014-01-10 - v1.6.0 - Activate the Administrator login.  Create data_backup task - kdg
## 2014-01-14 - v1.7.0 - Added weekly task, checking system protection, checking network location - kdg
## 2014-01-15 - v1.7.1 - Added check_scheduled_tasks - kdg
## 2014-01-16 - v1.7.2 - Added check_eventlog - kdg
## 2014-01-17 - v1.7.3 - Record last logon for Administrator - kdg
## 2014-01-20 - v1.7.4 - Added system_info_file - kdg
## 2014-01-21 - v1.7.5 - Cleanup and small corrections.  Added check_public_desktop - kdg
## 2014-01-22 - v1.7.6 - Automated creation of system image backup.  Clear the public desktop.  Call weekly_task if necessary - kdg
## 2014-01-27 - v1.7.7 - Updated checking eventlog.  Disabled some other checks for now - kdg
## 2014-01-30 - v1.8.0 - Disabling SyncBack tasks - kdg
## 2014-02-03 - v1.9.0 - Added check_backups.  Updated check_software - kdg
## 2014-02-05 - v1.9.1 - ISSUE: for unable to create restore point.  run weekly task at the beginning of the script. - kdg
## 2014-02-10 - v1.9.2 - Added mydoc_log variable - kdg
## 2014-02-12 - v1.9.3 - Updated accepted_software_list - kdg
## 2014-02-13 - v1.9.4 - Updated check_syncback to delete task if it cannot be disabled.  Updated accepted_software_list - kdg
## 2014-02-13 - v1.10.0 - Added check_villages - kdg
## 2014-02-14 - v1.10.1 - Revised error reporting in check_villages - kdg
## 2014-02-17 - v1.10.2 - Numerous revisions for XP workstations - kdg
## 2014-02-18 - v2.10.3 - Added support for syncing rdp & docx files from Edna - kdg
## 2014-02-20 - v2.10.4 - Updated valid software list - kdg
## 2014-02-24 - v2.10.5 - Update to logging customscheduledscan - kdg
## 2014-02-28 - v2.10.6 - Numerous small adjustments - kdg
## 2014-03-03 - v2.10.7 - More updates to checking software - kdg
## 2014-03-04 - v2.10.8 - Added check_firefox - kdg
## 2014-03-06 - v2.10.9 - Added check_cpu - kdg
## 2014-03-11 - v2.10.10 - Updated check_firefox - kdg
## 2014-03-19 - v2.10.11 - Some improvements to check_diskinfo - kdg
## 2014-03-22 - v2.10.12 - Collecting Mac Address - kdg
## 2014-03-23 - v2.11.0 - Added run_benchmark & save_record - kdg
## 2014-03-27 - v2.11.1 - Revised check_diskinfo - kdg
## 2014-04-01 - v2.11.2 - Corrected logging of benchmark results - kdg
## 2014-04-02 - v2.11.3 - Improved check_backups to find all of the important settings we want - kdg
## 2014-04-04 - v2.11.4 - Collecting total physical memory - kdg
## 2014-04-07 - v2.11.5 - Run benchmarks in all ^5 stores as a test - kdg
## 2014-04-25 - v2.11.6 - Added disable_system_image_task function - kdg
## 2014-04-29 - v2.11.7 - Updated check_data_backup to start supporting Ephrata - kdg
## 2014-05-02 - v2.11.8 - Collecting nic_driver - kdg
## 2014-05-07 - v2.11.9 - Prevented XP systems from getting involved with weekly_tasks - kdg
## 2014-05-13 - v2.11.10 - Don't warn about update count unless the windows update file is old - kdg
## 2014-05-15 - v2.11.11 - Some corrections to check_data_backup to Ephrata - kdg
## 2014-06-16 - v2.11.12 - More work on check_data_backup for Ephrata - kdg
## 2014-07-21 - v2.11.13 - Account for eph-gm at Ephrata. - kdg
## 2014-08-22 - v2.11.14 - Adding check_remote_app - kdg
## 2014-10-16 - v2.12.0 - Beginning revisions for Revised Network Scheme - kdg
## 2014-10-17 - v2.12.1 - Added appDir to check_software - kdg
## 2014-10-30 - v2.12.2 - Revisions to check_data_backup for revised network scheme - kdg
## 2014-11-06 - v2.12.3 - Ignore application hang in event log - kdg
## 2014-11-14 - v2.12.4 - Added ignore_tasks to check_eventlog - kdg
## 2014-11-18 - v2.12.5 - Added accepted_hash to check_software - kdg
## 2014-11-20 - v2.12.6 - Updated accepted_hash - kdg
## 2014-11-28 - v2.12.7 - Updated the list of accepted software again - kdg
## 2014-12-09 - v2.12.8 - Disabled part of check_software for now - kdg
## 2014-12-15 - v2.12.9 - Revised check_network function - kdg
## 2014-12-23 - v2.12.10 - Some corrections for checking CustomScheduledScan for 32-bit systems. - kdg
## 2014-12-30 - v2.12.11 - Record SEP version - kdg
## 2014-12-31 - v2.12.12 - Updated accepted_software_list - kdg
## 2015-01-06 - v2.12.13 - Added various store specific acceptions to the software list - kdg
## 2015-01-07 - v2.12.14 - Updated get_update_count to collect warning messages from WindowsUpdateLog - kdg
## 2015-01-21 - v2.12.15 - Corrections to schedule_update - kdg
## 2015-02-02 - v2.13.0 - Added sep_install - kdg
## 2015-02-04 - v2.13.1 - Enabled sep_install in Cranston and Sioux Falls - kdg
## 2015-02-05 - v2.13.2 - Limit sep_install to occur before 7:00am.  Updated check_for_update to check wsmonitor.pl script - kdg
## 2015-02-06 - v2.13.3 - Adjusted drive space max in check_diskinfo for Burlington - kdg
## 2015-02-09 - v2.13.4 - Enabled sep_install for stores starting with 2 - kdg
## 2015-02-11 - v2.14.0 - Added the antivirus_only option.  Added query_service and start_service and check that BFE is running. - kdg
## 2015-02-12 - v2.14.1 - Updated check_firewall to check SEP firewall - kdg
## 2015-02-16 - v2.14.2 - Added warning if SEP12 is not installed - kdg
## 2015-02-17 - v2.14.3 - Added acceptance of Microsoft Visual Studio 2010 Tools for Office Runtime (x64) -kdg
## 2015-02-19 - v2.14.4 - Opened the window a bit for SEP12 installation - kdg
## 2015-02-24 - v2.14.5 - Adjusted source location of SEP12 for revised_network systems - kdg
## 2-15-02-27 - v2.14.6 - Some corrections to sep_install - kdg
## 2015-03-02 - v2.14.7 - Added acceptance of Microsoft Visual Studio 2010 Tools for Office Runtime (x64) -kdg
## 2015-03-03 - v2.14.8 - Some updates regarding checking Windows Updates pending and listing whether .Net is installed - kdg
## 2015-03-09 - v2.14.9 - Updated check_for_update to get vbs scripts as well - kdg
## 2015-03-10 - v2.14.10 - Added -basic argument.  Modified max for drive D: depending on total capacity - kdg
## 2015-03-10 - v2.15.0 - Added check_net_install function - kdg
## 2015-03-11 - v2.15.1 - Corrected checking firewall for 32-bit workstations - kdg
## 2015-04-23 - v2.15.2 - Changed def date limit for bashdefs from 44 to 60 - kdg
## 2015-04-28 - v2.15.3 - Created check_share - kdg
## 2015-05-11 - v2.15.4 - Added arch to Windows XP - kdg
## 2015-05-13 - v2.15.5 - Added software_only - kdg
## 2015-05-22 - v2.15.6 - Added exclusion_list to check_villages - kdg
## 2015-06-04 - v2.15.7 - Check the data backup script and remove it if it is obsolete - kdg
## 2015-06-11 - v2.15.8 - Added services_only - kdg
## 2015-06-12 - v2.16.0 - Added ability to request a reboot.  send_reboot_request - kdg
## 2015-06-18 - v2.16.1 - Revised some logging in send_reboot_request - kdg
## 2015-07-03 - v2.16.2 - Added recording of reboots and reboot requests - kdg
## 2015-07-06 - v2.16.3 - Adjusted memory used threshold from 75 to 85 - kdg
## 2015-07-07 - v2.16.4 - Added symantec_repair_strings and symantec_reboot_strings - kdg
## 2015-07-09 - v2.16.5 - Added -reboot option - kdg
## 2015-07-22 - v2.16.6 - Added -wu option - kdg
## 2015-07-24 - v2.16.7 - Added check_misc_file_updates function - kdg
## 2015-08-10 - v2.17.0 - Adding collect_info and checking current FireFoxRelease - kdg
## 2015-08-11 - v2.17.1 - Revised check_for_update to bring in .exe files - kdg
## 2015-08-12 - v2.18.0 - Added check_tasks - kdg
## 2015-08-13 - v2.18.1 - Updated taskkill and another attempt at removing updates - kdg
## 2015-08-14 - v2.18.2 - Updated valid_task_hash.  Enabled check_tasks.  Updated check_scheduled_tasks - kdg
## 2015-08-17 - v2.18.3 - Updated valid_task_hash. - kdg
## 2015-08-18 - v2.18.4 - Updated check_tasks for Ephrata - kdg
## 2015-08-19 - v2.19.0 - Added check_edna - kdg
## 2015-08-20 - v2.19.1 - Updated valid_task_hash - kdg
## 2015-08-24 - v2.19.2 - Corrections to check_edna.  Added installed_firefox_major variable - kdg
## 2015-08-26 - v2.19.3 - Updated accepted_hash and enabled checking Roaming.  Updated find_user - kdg
## 2015-08-27 - v2.20.0 - Added cleanup - kdg
## 2015-08-28 - v2.20.1 - Updated valid_task_hash.  Revised checking for reboot requests - kdg
## 2015-09-01 - v2.20.2 - Renamed accepted_hash to accepted_app_hash.  Updated valid_task_hash.  Updated cleanup() - kdg
## 2015-09-02 - v2.20.3 - Updated accepted_app_hash and added move_hash to cleanup - kdg
## 2015-09-08 - v2.20.4 - Updated accepted_app_hash.  Some adjustments to find_user - kdg
## 2015-09-11 - v2.20.5 - Updated accepted_app_hash.  Updated find_user  - kdg
## 2015-09-16 - v2.20.6 - Updated accepted_app_hash for Operationsmgr - kdg
## 2015-09-25 - v2.20.7 - Updated accepted_app_hash for Boston - kdg
## 2015-10-20 - v2.20.8 - Updated checking software for EPHRATA-ASSTMGR - kdg
## 2015-10-20 - v2.21.0 - Added check_running_services - kdg
## 2015-11-05 - v2.21.1 - Updated some accepted apps and tasks - kdg
## 2015-11-09 - v2.21.2 - Updated valid_task_hash - kdg
## 2015-11-11 - v2.21.3 - Updated valid_task_hash - kdg
## 2015-11-17 - v2.21.4 - Added Triversity Inc files to cleanup - kdg
## 2015-12-08 - v2.22.0 - Updated valid_task_hash.  Added gwx_uninstall - kdg
## 2015-12-18 - v2.23.0 - Added set_start function - kdg
## 2015-12-29 - v2.23.1 - Added AutomaticUpdates in check_running_services - kdg
## 2015-12-30 - v2.23.2 - Whitelisted a process for Ephrata - kdg
## 2016-01-06 - v2.23.3 - Disabled checking software and tasks due to resource contstraints - kdg
## 2016-01-11 - v2.23.4 - Re-enabled checking software (once a week) and checking tasks - kdg
## 2016-02-10 - v2.23.5 - Added ftlus - kdg
## 2016-02-11 - v2.23.6 - Added check_master_browser - kdg
## 2016-02-12 - v2.23.7 - Using cacls to check share - kdg


use constant SCRIPT_VERSION 	=> "2.23.7";
use Cwd;
use File::Copy;
use File::Basename;
use Getopt::Long;
use Net::SMTP;
use DBI;
use TTV::utilities qw(openODBCDriver dequote execQuery_arrayref recordCount trim LogMsg indent setConfig);
use TTV::lookupdata;
use TTV::library;
use strict;
use IO::Socket;
use Time::Local;  

################
## Variables
################
my $help=0;
my $proto=0;
my $verbose=0;
my $warning_count=0;
my $OS;
my $arch=0;
my $debug_mode=0;
my $no_action=0;
my $force=0;

my $antivirus_only=0;
my $network_only=0;
 
my $software_only=0;
my $no_precheck=0;
 
my $services_only=0;
my $tasks_only=0;
my $schedule_only=0;
my $event_only=0;
my $reboot_requested=0;
my $windows_updates_only=0;

# Get the current time in seconds
my $timeseconds=time();
my @ltm = localtime();
my $day_of_week=$ltm[6];
my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);
my @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
my $previous_date_label = sprintf("%04d-%02d-%02d", $previous_day_ltm[5]+1900, $previous_day_ltm[4]+1, $previous_day_ltm[3]);
my @previous_previous_day_ltm=localtime($timeseconds - (86400 * 2)); # Two days ago
my $previous_previous_date_label = sprintf("%04d-%02d-%02d", $previous_previous_day_ltm[5]+1900, $previous_previous_day_ltm[4]+1, $previous_previous_day_ltm[3]);
my $current_year=$ltm[5]+1900;
my $current_month=$ltm[4]+1;
my $current_day=$ltm[3];
my $current_hour = $ltm[2];
my $previous_day_year=$previous_day_ltm[5]+1900;
my $previous_day_month=$previous_day_ltm[4]+1;
my $previous_day_day=$previous_day_ltm[3];
my $data_backup_script="c:/temp/scripts/data_backup.bat";
my $weekly_task="c:/temp/scripts/weekly_task.bat";
my $system_image_task="c:/temp/scripts/system_image_task.bat";
my $system_registry_info="C:/Temp/scripts/system_registry_info.txt";
my $system_network_info="C:/Temp/scripts/system_network_info.txt";
my $system_info_file="//Edna/temp/install/register_utilities/site_info.txt";
my $local_system_info="c:/temp/scripts/workstationInfo.txt";
my %local_system_hash=();
my $update_source="//Edna/temp/install/workstation_utilities";
my $pm_source="//Edna/temp/install/scripts/pm";
my $pcAnywhereTool="//Edna/temp/install/pcAnywhere/removeIPpcAnywhere.bat";
my $storenum=0;
my $storeStatus="unknown";	
my $privoxy_running=0;
my $data_backup_set=0;
my $need_to_reboot=0;
my $reboot_request_log="c:/temp/scripts/reboot_requests.log";
my $reboot_log="c:/temp/scripts/reboot.log";
 
my %monitor_record_hash;
my $monitor_record = "c:/temp/scripts/monitor_record.txt";
my $revised_network=0;
my $share_dir="c:/temp/share";
my $share_temp_dir="c:/temp/share/temp";
my $officebackup_dir="c:/temp/share/Officebackup";

my %services_hash = (
 
);
 
chomp(my $hostname = `hostname`); 

my $goRC = GetOptions(
	"help"=>\$help, 
    "proto"=>\$proto,
	"verbose"=>\$verbose,
	"debug"=>\$debug_mode,
	"no"=>\$no_action,
	"force"=>\$force,
	"basic"=>\$no_precheck, 
	"antivirus"=>\$antivirus_only,
	"software"=>\$software_only,
	"services" => \$services_only,
	"tasks" => \$tasks_only,
	"procs" => \$tasks_only,
	"schedule" => \$schedule_only,
	"event" => \$event_only,
	"reboot" => \$reboot_requested,
	"wu" => \$windows_updates_only,
	"network" => \$network_only
    );
 
if (($goRC == 0) || ($help)) {
	usage();
   exit;
} 
 
if ($proto) {
	print "\n\nRunning proto...\n";
	precheck();			
	#send_reboot_request();
	#gather_sysinfo();
	#check_windows_updates();
	#check_data_backup();
	#check_misc();
	#check_remote_app();
	#disable_system_image_task();
	#check_software();
	#check_firefox();
	#check_gwx_install();
	#check_edna();
	#cleanup();
	#check_reboots();
	#check_master_browser();
	#check_for_update();
	#collect_info();
	#check_antivirus();
	#check_software();
	#check_windows_updates();
	#check_firewall();
	#check_net_install();
	#check_diskinfo();
	#check_administrator();
	#check_villages();
	#check_syncback();
	#run_weekly_task();
	#check_backups();
	save_record();
	print "Finished running proto...\n";
 
	exit;
}

if ($reboot_requested) {
	print "\n\nRunning send_reboot_request...\n";
	send_reboot_request();	 
	print "Finished running send_reboot_request...\n";
	exit;
}
if ($software_only) {
	print "\n\nRunning check_software...\n";
	precheck();	
	check_software(); 
	print "Finished running check_software...\n";
	exit;
}
if ($network_only) {
	print "\n\nRunning check_network...\n";
	precheck();	
	check_weekly_task();
	run_weekly_task();
	check_network();
 
	print "Finished running check_network...\n";
	exit;
}
if ($antivirus_only) {
	print "\n\nRunning check_antivirus...\n";
	precheck();	 
	check_antivirus();
	print "Finished running check_antivirus...\n";
	exit;
}
if ($services_only) {
	print "\n\nRunning check_services...\n";
	precheck();	 
	check_services();
	check_running_services();
	print "Finished running check_services...\n";
	exit;
}
if ($tasks_only) {
	print "\n\nRunning check_tasks...\n";
	precheck();	 
	check_tasks();
	print "Finished running check_tasks...\n";
	exit;
}

if ($schedule_only) {
	print "\n\nRunning check_scheduled_tasks...\n";
	precheck();	 	
	check_scheduled_tasks();	
	print "Finished running check_scheduled_tasks...\n";
	exit;
}
if ($event_only) {
	print "\n\nRunning check_eventlog...\n";
	precheck();	 	
	check_eventlog();	
	print "Finished running check_eventlog...\n";
	exit; 
}
if ($windows_updates_only) {
	print "\n\nRunning check_eventlog...\n";
	precheck();	 	
	check_windows_updates();	
	print "Finished running check_eventlog...\n";
	exit; 
}
 
if ($help) {
	usage();
   exit;
}

 
################
## Main
################
 
LogMsg("$0 started version:".SCRIPT_VERSION);
precheck();
run_weekly_task();
check_reboots();
check_software();	
check_data_backup();
#check_backups();	Now being called from check_diskinfo
check_syncback();	 
check_antivirus();
check_services(); 
check_running_services();
check_tasks();	
check_scripts();
check_windows_updates();
#check_pcAnywhere();
check_firewall();	
check_registry();
check_for_update();
check_misc();
check_eventlog();
check_weekly_task();
check_scheduled_tasks();
check_network();
check_system_protection();
run_benchmarks();
gather_sysinfo();
cleanup();
save_record();
 
LogMsg("RESULTS: $warning_count issues found on $hostname");
LogMsg("$0 completed");


save_log();



################
## Functions
################

sub usage {
	print "\nUSAGE: regmonitor.pl [options]\n";
    print "\nA tool to monitor the backoffice workstation in the store.\n";
	print "Optional Parameters:\n";
	print "  --help     Show this help. \n";	
	print "  -network	Check network location.\n";
	print "  -force		Run checks that would not otherwise normally be schedule to run.\n";
	print "  -antivirus	Run the antivirus check only.\n";
	print "  -service   Run the services check only.\n";
	print "  -schedule  Run the check scheduled tasks only.\n";
	print "  -software  Only Check Software installed.\n";
	print "  -event     Check the event log only.\n";
	print "  -basic		Skips some of the more intensive parts of the precheck function.\n";
	print "  -reboot    Set a reboot request window.\n";
	print "  -wu		Check windows updates log.\n";
	print "  -proc		Check currently running processes.\n";
}

sub check_reboots {
	my $cmd = "net statistics workstation";
	my @cmd_info = `$cmd`;
	my $last_reboot;
	foreach my $c (@cmd_info) {
		chomp($c);
		if ($c =~ /Statistics since/) {
			my @tmp=split(/\s+/,$c);
			$last_reboot="$tmp[2] $tmp[3] $tmp[4]";
			LogMsg("Last reboot: $last_reboot");
		}
	}    
	if (-f $reboot_request_log) {
		open(LOG,"$reboot_request_log");
		my @log_info=(<LOG>);
		close LOG;
		my @reboot_request_list;
		my $recent_count=0;
		foreach my $line (@log_info) {
			chomp($line);
			my @tmp=split(/\s+/,$line);
			my $request=$tmp[-1];
			push(@reboot_request_list,$request);
			my @request_ltm = localtime($request);			
			my $request_date_label=sprintf("%04d-%02d-%02d",  $request_ltm[5]+1900,$request_ltm[4]+1, $request_ltm[3]);
			LogMsg(indent(1)."Found Reboot Request on $request_date_label");
			my $age=(($timeseconds - $request) / 86400);	# How many days ago this request was
			if ($age < 7) {
				LogMsg(indent(2)."This was $age days ago");
				$recent_count++;
			}

		}
		if ($recent_count > 3) {
			LogMsg("WARNING: Found $recent_count reboot requests in the past week");
			$warning_count++;
		}		
	} else {
		LogMsg("Did not find $reboot_request_log");
	}
	if (-f $reboot_log) {
		open(LOG, "$reboot_log");
		my @reboot_info=(<LOG>);
		close LOG;
		my %reboot_hash;
		foreach my $reboot (@reboot_info) {
			chomp($reboot);
			$reboot_hash{$reboot}=1;
		}
		my $counter=0;
		if (keys(%reboot_hash) > 1) {
			LogMsg("The recent reboots for this system have been:");
		}
		foreach my $reboot (sort keys(%reboot_hash)) {
			LogMsg(indent(1)."$reboot");
		}
	} else {
		LogMsg("Did not find $reboot_log");
	}
}

sub precheck {

	my $warns=0;
	# Determine network revision
	
	my @ip_info=`ipconfig /all`;
	foreach my $i (@ip_info) {
		 
		if ($i =~ /subnet mask/i) {
			my @tmp=split(/: /,$i);
			my $subnetMask=$tmp[1];
			if ($subnetMask =~ /255.255.255.240/) {
				$revised_network=1;	
				if ($verbose) {
					print "Revised network found\n";
				}
			}
		}
	}
	
	# Check the basic folder structure
	if (-d "c:/temp") {
		unless (-d "c:/temp/scripts") {
			LogMsg("Making c:/temp/scripts...");
			mkdir("c:/temp/scripts");			
		}
	}
 
	if ($revised_network) {
		LogMsg("Revised network");
		unless (-d $share_dir) {
			LogMsg("Creating $share_dir");
			mkdir $share_dir;	
			# This is a good time to remove the old backup script as well.
			# A new one will be created later.
			if (-f $data_backup_script) {
				LogMsg("Removing old backup script.");
				unlink $data_backup_script;
			}
		}
		if (-d $share_dir) {
			if ($verbose) {
				LogMsg("Found $share_dir");
			}
			$system_info_file="${share_dir}/site_info.txt";
			$update_source="${share_dir}/workstation_utilities";
			$pm_source="${share_dir}/pm";
			$pcAnywhereTool="${share_dir}/removeIPpcAnywhere.bat";
			unless (-d $officebackup_dir) {
				LogMsg("Creating $officebackup_dir");
				mkdir $officebackup_dir;				
			}
			unless (-d $officebackup_dir) {	
				LogMsg("WARNING: Failed to create $officebackup_dir");
				$warns++;			
			}
			# Create the share temp folder.  Anything found in this folder
			# will be moved to the temp folder by the check_misc_file_updates function			
			unless (-d $share_temp_dir) {
				LogMsg("Creating $share_temp_dir");
				mkdir $share_temp_dir;	
 
			}	
			unless (-d $share_temp_dir) {	
				LogMsg("WARNING: Failed to create $share_temp_dir");
				$warns++;			
			}		
 
			unless (check_share("$share_dir")) {
				LogMsg("WARNING: Failed to share $share_dir");
				$warns++;			
			}
 
			if (-f $data_backup_script) {
				my $remove_request=0;
				# Check that this is the current version of the script
				open(BAT,"$data_backup_script");
				my @bat_info=(<BAT>);
				close BAT;
				foreach my $b (@bat_info) {
					if ($b =~ /xcopy/i) {
						unless ($b =~ /xcopy c/i) {
							$remove_request++;
						}
					}					
				}
				if ($remove_request) {
					LogMsg("Removing old backup script.");
					unlink $data_backup_script;
				}
			}	
	
		} else {
			LogMsg("WARNING: Failed to locate $share_dir");
			$warns++;
		}
	}	
	# Collect Site Info
	if (-f $system_info_file) {
		# Read in the site info
		LogMsg("Reading $system_info_file...");
		open(REC,"$system_info_file");
		my @site_info=(<REC>);
		close REC;
		foreach my $r (@site_info) {
			chomp($r);
			my @tmp=split(/\s+/,$r);
			if ($tmp[0] =~ /storenum/) {
				$storenum=$tmp[1];
				LogMsg("Running on store $storenum");
			}
			if ($tmp[0] =~ /storeStatus/) {
				$storeStatus=$tmp[1];
				LogMsg("Running on a $storeStatus store");
			}	
			if ($tmp[0] =~ /privoxy/) {
				$privoxy_running=$tmp[1];
			}			
		}
	} else {
		LogMsg("Did not find $system_info_file");
	}		
	if (-f $local_system_info) {
		# Read in the local system info
		LogMsg("Reading $local_system_info...");
		open(REC,"$local_system_info");
		my @site_info=(<REC>);
		close REC;
		foreach my $r (@site_info) {
			chomp($r);
			if ($r =~ /\=/) {
				my @tmp=split(/=/,$r);			
				$local_system_hash{$tmp[0]} = $tmp[1];					
			}
		}
	} else {
		LogMsg("Did not find $local_system_info");
	}		
	
	#my @system_info=`systeminfo`;
	# Determine OS
	my @system_info=`wmic os get Caption /value`;
	
	foreach my $si (@system_info) {
	 			
			if ($si =~ /Windows 7/) {
				$OS="win7";	
				$arch=get_arch();
				if ($arch) {
					$OS="$OS $arch";
				}
			}
			
			if ($si =~ /Windows XP/) {
				$OS="XP";
				$arch="32-bit";
				unless (($hostname eq "BO5Hallway") 
					||($hostname eq "4201-Cumming ") 
					|| ($hostname eq "ephratadesign") 
					||($hostname eq "LancasterBO2")
					||($hostname eq "EPHRATA-ASSTMGR")
					||($hostname eq "4201-Cumming")
					||($hostname eq "4657-S-Windsor")
					 
					) {
					LogMsg("WARNING: OS on $hostname is $OS");             
					$warns++;    				
				}	
			}			
 	
	}
	if ($OS) {
		LogMsg("System Info: OS: $OS"); 	
	}
	# Read Monitor Record
	if (-f $monitor_record) {
		# Read in the record of previous tests
		LogMsg("Reading $monitor_record...");
		open(REC,"$monitor_record");
		my @record_info=(<REC>);
		close REC;
		foreach my $r (@record_info) {
			my @tmp=split(/\s+/,$r);
			$monitor_record_hash{$tmp[0]}=$tmp[1];
		}
	} else {
		LogMsg("Did not find $monitor_record");
	}	
	unless ($no_precheck) {
		# Skip the more intensive parts of precheck
		# Get the amount of memory
		my $total_memory=0;
		my $cmd = "systeminfo";
		my @mem_info=`$cmd`;
		my $conventional;
		my $contiguous;	
		foreach my $m (@mem_info) {
			chomp($m);		
			if ($m =~ /Total Physical Memory/i) {
				my @tmp=split(/:/,$m);
				$total_memory=$tmp[1];
				while ($total_memory =~ /^ /) {
					$total_memory =~ s/^ //;
				}					 
			} 	
		} 
		if ($total_memory) {		 
			LogMsg(indent(1)."System Info - Total Memory: $total_memory");
		} else {
			LogMsg(indent(1)."Failed to determine total memory");
		}	
	}

	if ($warns) {
        LogMsg("WARNING: $warns issues found in pre-check");
		$warning_count++;
    } 	
}

sub check_software {
	unless ($day_of_week == 0) {
		# Only run once a week due to resource constraints
		return;
	}
	LogMsg("Checking software...");
	my $warns=0;

    # This is checking that certain software is not present
 
    my @unwanted_list=(
        "java",
		"BuzzSearch",
		"NewPlayer",
		"MySearchdial",
		"MyPC Backup",
		"Re-markit",
		"pcAnywhere"
		
    );
	my %accepted_app_hash=(
		"7-Zip" => '1',
		"Apple Computer" => '1',
		"Brother" => '1',
		"Browny02" => '1',
		"ControlCenter4" => '1',
		"CCleaner" => '1',
		"Common Files" => '1',
		"desktop.ini" => '1',
		"DVD Maker" => '1',
		"EmieBrowserModeList" => '1',
		"EmieSiteList" => '1',
		"EmieUserList" => '1',
		"Hewlett-Packard" => '1',
		"hp" => '1',
		"HP Games" => '1',
		"Internet Explorer" => '1',
		"Macromedia" => '1',
		"Microsoft Analysis Services" => '1',
		"Microsoft Games" => '1',
		"Microsoft Office" => '1',
		"Microsoft Silverlight" => '1',
		"Microsoft SQL Server Compact Edition" => '1',
		"Microsoft Sync Framework" => '1',
		"Microsoft Synchronization Services" => '1',
		"MSBuild" => '1',
		"Online Services" => '1',
		"PlayReady" => '1',
		"Realtek" => '1',
		"Reference Assemblies" => '1',
		"Symantec" => '1',
		"SyncBack" => '1',
		"Uninstall Information" => '1',
		"Windows Defender" => '1',
		"Windows Journal" => '1',
		"Windows Mail" => '1',
		"Windows Media Player" => '1',
		"Windows NT" => '1',
		"Windows Photo Viewer" => '1',
		"Windows Portable Devices" => '1',
		"Windows Sidebar" => '1',
		"Citrix" => '1',
		"Common Files" => '1',
		"Cyberlink" => '1',
		"Creative" => '1',  
		"desktop.ini" => '1',
		"Foxit Reader" => '1',
		"Google" => '1',
		"Hewlett-Packard" => '1',
		"HP" => '1',
		"InstallShield Installation Information" => '1',
		"Intel" => '1',
		"Internet Explorer" => '1',
		"IrfanView" => '1',
		"K-NFB Reading Technology Inc" => '1',
		"Macrium" => '1',
		"MSXML 4.0" => '1',
		"Microsoft" => '1',
		"Microsoft Analysis Services" => '1',
		"Microsoft Office" => '1',
		"Microsoft Silverlight" => '1',
		"Microsoft Visual Studio 8" => '1',
		"Microsoft.NET" => '1',
		"Mozilla Firefox" => '1',
		"Mozilla Maintenance Service" => '1',
		"MSBuild" => '1',
		"Notepad++" => '1',
		"Online Services" => '1',
		"ossec-agent" => '1',
		"PC Wizard" => '1',
		"PDF Complete" => '1',
		"PlayReady" => '1',
		"POS Manager" => '1',
		"Realtek" => '1',
		"Reference Assemblies" => '1',
		"Sales Journal Viewer" => '1',
		"Samsung Printers" => '1',
		"Symantec" => '1',
		"System Explorer" => '1',
		"Temp" => '1',
		"TightVNC" => '1',
		"UltraVNC" => '1',
		"Uninstall Information" => '1',
		"VLC" => '1',
		"Windows Defender" => '1',
		"Windows Mail" => '1',
		"Windows Media Player" => '1',
		"Windows NT" => '1',
		"Windows Photo Viewer" => '1',
		"Windows Portable Devices" => '1',
		"Windows Sidebar" => '1',
		"XnView" => '1',
		"Application Data" => '1',
		"CrashDumps" => '1',
		"Diagnostics" => '1',
		"DigitalPersona" => '1',
		"GDIPFONTCACHEV1.DAT" => '1',
		"Google" => '1',
		"Hewlett-Packard" => '1',
		"Hewlett-Packard_Company" => '1',
		"History" => '1',
		"IconCache.db" => '1',
		"Microsoft" => '1',
		"Microsoft Help" => '1',
		"Mozilla" => '1',
		"PDFC" => '1',
		"RemEngine" => '1',
		"Symantec" => '1',
		"Temp" => '1',
		"Temporary Internet Files" => '1',
		"VirtualStore" => '1',
		"Xobni" => '1',
		"McAfee Security Scan Plus" => '1',
		"S10 Password Vault" => '1',
		"Windows Virtual PC" => '1',
		"Adobe" => '1',
		"CyberLink" => '1',
		"Foxit Software" => '1',
		"Media Center Programs" => '1',
		"LiveUpdate" => '1',
		"Symantec Endpoint Protection" => '1',
		"GWX" => '1',
		"Identities" => '1',
		"Skype" => '1',
		"S10 Software" => '1',
		"vlc" => '1',
		"Nuance" => '1',
		"Dropbox" => '1',
		"Brownie" => '1',
		"PC-FAX TX" => '1',
		"FLEXnet" => '1',
		"Zeon" => '1',
		"Apps" => '1',
		"PeerNetworking" => '1',
		"McAfee Security Scan" => '1',
		"Belarc" => '1',
		"Mozilla Firefox.bak" => '1',
		"FoxIt Reader" => '1',
		 
		"epson" => '1',
		"EPSON" => '1',
 
		
	
	);
	if ($storenum == 2760) {
		$accepted_app_hash{"Microsoft IntelliType Pro"} = '1';		
		$accepted_app_hash{"Picasa3"} = '1';
		$accepted_app_hash{"keyfile3.drm"} = '1';
		$accepted_app_hash{"Programs"} = '1';
		
	} 	 
	if ($storenum == 5120) {
		$accepted_app_hash{"InstallShield"} = '1';		
	} 
	if ($storenum == 4745) {
		$accepted_app_hash{"InterVideo"} = '1';		
		$accepted_app_hash{"M5000"} = '1';	
	} 	
	
 
	if ($storenum == 5263) {
		$accepted_app_hash{"TP-LINK"} = '1';		
	} 
	if ($storenum == 3979) {
		$accepted_app_hash{"Microsoft Mouse and Keyboard Center"} = '1';		
	} 	
	if ($storenum == 3933) {
		$accepted_app_hash{"HpUpdate"} = '1';		
	} 		
	if ($storenum == 4981) {
		$accepted_app_hash{"Foxit"} = '1';		
		$accepted_app_hash{"Microsoft OffCAT"} = '1';
		$accepted_app_hash{"ActiveState"} = '1';
	} 		
	if ($storenum == 4862) {			
		$accepted_app_hash{"Microsoft OffCAT"} = '1';				
	} 		
	if ($storenum == 4639) {
		$accepted_app_hash{"Axantum"} = '1';		
		$accepted_app_hash{"McAfee Security Scan"} = '1';
		$accepted_app_hash{"Malwarebytes Anti-Malware"} = '1';
	} 	 	
	if ($storenum == 7037) {
		$accepted_app_hash{"Microsoft Policy Platform"} = '1';		
		$accepted_app_hash{"Windows Identity Foundation"} = '1';
		$accepted_app_hash{"adobe"} = '1';
		$accepted_app_hash{"Malwarebytes Anti-Malware"} = '1';
		$accepted_app_hash{"Microsoft SQL Server"} = '1';
		$accepted_app_hash{"Microsoft Visual Studio"} = '1';
		$accepted_app_hash{"Microsoft Works"} = '1';
		$accepted_app_hash{"OpenIt"} = '1';
		$accepted_app_hash{"Scribe"} = '1';
		$accepted_app_hash{"Windows Identity Foundation"} = '1';
		$accepted_app_hash{"ElevatedDiagnostics"} = '1';
		$accepted_app_hash{"GSMiscDownload"} = '1';
		$accepted_app_hash{"Programs"} = '1';
		$accepted_app_hash{"DigitalSites"} = '1';
		$accepted_app_hash{"FolderSync"} = '1';
		$accepted_app_hash{"InstallShield"} = '1';
		$accepted_app_hash{"Systweak"} = '1';
		$accepted_app_hash{"Targus"} = '1';
 
	} 	 	
	
	if (($storenum == 7037) && ($hostname eq "Operationsmgr")) {
		$accepted_app_hash{"AMD"} = '1';	
		$accepted_app_hash{"Apple Software Update"} = '1';	
		$accepted_app_hash{"ArcSoft"} = '1';	
		$accepted_app_hash{"ATI Technologies"} = '1';	
		$accepted_app_hash{"Best Software"} = '1';	
		$accepted_app_hash{"ComPlus Applications"} = '1';	
		$accepted_app_hash{"Fingerprint Sensor"} = '1';	
		$accepted_app_hash{"HPQ"} = '1';	
		$accepted_app_hash{"InterVideo"} = '1';	
		$accepted_app_hash{"lotus"} = '1';	
		$accepted_app_hash{"Messenger"} = '1';	
		$accepted_app_hash{"Microsoft CAPICOM 2.1.0.2"} = '1';	
		$accepted_app_hash{"microsoft frontpage"} = '1';	
		$accepted_app_hash{"Microsoft Visual Studio"} = '1';	
		$accepted_app_hash{"Microsoft Works"} = '1';	
		$accepted_app_hash{"Movie Maker"} = '1';	
		$accepted_app_hash{"MSECache"} = '1';	
		$accepted_app_hash{"MSN"} = '1';	
		$accepted_app_hash{"MSN Gaming Zone"} = '1';	
		$accepted_app_hash{"NetMeeting"} = '1';	
		$accepted_app_hash{"Outlook Express"} = '1';	
		$accepted_app_hash{"Program Shortcuts"} = '1';	
		$accepted_app_hash{"QuickTime"} = '1';	
		$accepted_app_hash{"Support Tools"} = '1';	
		$accepted_app_hash{"Symantec AntiVirus"} = '1';	
		$accepted_app_hash{"VideoLAN"} = '1';	
		$accepted_app_hash{"Windows Desktop Search"} = '1';	
		$accepted_app_hash{"Windows Media Connect 2"} = '1';
		$accepted_app_hash{"WindowsUpdate"} = '1';	
		$accepted_app_hash{"xerox"} = '1';	
		$accepted_app_hash{"Yahoo!"} = '1';	
 		$accepted_app_hash{"Microsoft Policy Platform"} = '1';	
		$accepted_app_hash{"WinZip"} = '1';	
		$accepted_app_hash{"real"} = '1';	
		$accepted_app_hash{"SaveShare"} = '1';	
		$accepted_app_hash{"symantec"} = '1';	
		$accepted_app_hash{"SymSilent"} = '1';	
		$accepted_app_hash{"WildTangent Games"} = '1';	
	}
	if (($storenum == 7037) && ($hostname eq "EPHRATA-ASSTMGR")) {			
		$accepted_app_hash{"AMD"} = '1';
		$accepted_app_hash{"Apple Software Update"} = '1';
		$accepted_app_hash{"ArcSoft"} = '1';
		$accepted_app_hash{"ATI Technologies"} = '1';
		$accepted_app_hash{"Best Software"} = '1';
		$accepted_app_hash{"Compaq"} = '1';
		$accepted_app_hash{"ComPlus Applications"} = '1';
		$accepted_app_hash{"Fingerprint Sensor"} = '1';
		$accepted_app_hash{"HPQ"} = '1';
		$accepted_app_hash{"InterVideo"} = '1';
		$accepted_app_hash{"J_ava.junk"} = '1';
		$accepted_app_hash{"lotus"} = '1';
		$accepted_app_hash{"Messenger"} = '1';
		$accepted_app_hash{"Microsoft CAPICOM 2.1.0.2"} = '1';
		$accepted_app_hash{"microsoft frontpage"} = '1';
		$accepted_app_hash{"Movie Maker"} = '1';
		$accepted_app_hash{"MSECache"} = '1';
		$accepted_app_hash{"MSN"} = '1';
		$accepted_app_hash{"MSN Gaming Zone"} = '1';
		$accepted_app_hash{"NetMeeting"} = '1';
		$accepted_app_hash{"Outlook Express"} = '1';
		$accepted_app_hash{"Program Shortcuts"} = '1';
		$accepted_app_hash{"QuickTime"} = '1';
		$accepted_app_hash{"Support Tools"} = '1';
		$accepted_app_hash{"Symantec AntiVirus"} = '1';
		$accepted_app_hash{"VideoLAN"} = '1';
		$accepted_app_hash{"Windows Desktop Search"} = '1';
		$accepted_app_hash{"Windows Media Connect 2"} = '1';
		$accepted_app_hash{"WindowsUpdate"} = '1';
		$accepted_app_hash{"xerox"} = '1';
		$accepted_app_hash{"Yahoo!"} = '1';	
	}
    my $program_files_dir="C:/program files";
	my $program_files86_dir="C:/Program Files (x86)";
	my $appDir1="C:/Users/Villages/AppData/Local";
	my $appDir2="C:/Users/Villages/AppData/Roaming";
	my $Symantec_dir="C:/Program Files (x86)/Symantec";
	my @dir_list=("$program_files_dir","$program_files86_dir","$appDir1","$appDir2","$Symantec_dir");	
	foreach my $dir (@dir_list) {
		if ($verbose) {
			LogMsg(indent(1)."Reading $dir...");
		}
		opendir(PROGS,"$dir");
		my @prog_list=readdir(PROGS);
		close(PROGS);
		foreach my $p (@prog_list) {
			next if ($p eq ".");
			next if ($p eq ".."); 
			foreach my $u (@unwanted_list) {				 
				if ($p =~ /$u/i) {
					LogMsg("WARNING: Found ($p) installed in $dir (unwanted)");             
					$warns++;            
				}
			}
			if (1) {	# Disabling until I get this list better sorted out - kdg				
				unless ($accepted_app_hash{$p}) {
					LogMsg("WARNING: Found ($p) installed in $dir (unaccepted)");             
					$warns++;   				
				}
			}
		}	
	}
 
	# Check to see what software is installed
 
 
	my @software_list;
	my @accepted_software_list=(
		"CCleaner",
		"7-Zip",		
		"HP Client Services",
		"Symantec Endpoint Protection",		
		"Microsoft Silverlight",
		"HP Auto",
		"PlayReady PC Runtime amd64",
		"Windows Mobile Device Center",
		"64 Bit HP CIO Components Installer",
		"HP ProtectTools Security Manager",
		"Device Access Manager for HP ProtectTools",
		"PaperPort Image Printer 64-bit",
		"TightVNC",
		"HP ProtectTools Security Manager",
		"Drive Encryption For HP ProtectTools",
		"HP Vision Hardware Diagnostics",
		"Network64",
		"Microsoft Application Error Reporting",
		"IrfanView",
		"Windows Live ID Sign-in Assistant",
		"Microsoft Visual C",
		"Adobe Flash Player",
		"WinZip",
		"Foxit Reader",
		"LiveUpdate 3.3",
		"Notepad\\+\\+",
		"Triversity Electronic Journal",
		"Triversity POS-Store Manager",
		"VLC media player",
		"XnView",
		"Intel(R) Management Engine Components",
		"ActivePerl",
		"MSXML 4.0",
		"Realtek High Definition Audio Driver",
		"Intel(R) Graphics Media Accelerator Driver",
		"Brother MFL-Pro Suite",
		"32 Bit HP CIO Components Installer",
		"Intel.*Management Engine Components",
		"Intel.*Graphics Media Accelerator Driver",
		"Brother HL-3045CN",
		"Adobe",
		"Brownie",
		"Foxit",
		"Foxit Software",
		"Nuance",
		"Creative",
		"McAfee Security Scan Plus",
		"Microsoft Mouse and Keyboard Center",
		"Microsoft IntelliType Pro 8.2",
		"AxCrypt 1.7.2867.0",
		"Microsoft Visual Studio 2010 Tools for Office Runtime (x64)",
		"Microsoft Visual Studio 2010 Tools for Office Runtime (x86)"
						
	);
	my @xp_accepted_software_list=(
		"CCleaner",
		"7-Zip",
		"Notepad\\+\\+",
		
		"HP Client Services",
		"Symantec Endpoint Protection",		
		"Microsoft Silverlight",
		"HP Auto",
		"PlayReady PC Runtime amd64",
		"Windows Mobile Device Center",
		"64 Bit HP CIO Components Installer",
		"HP ProtectTools Security Manager",
		"Device Access Manager for HP ProtectTools",
		"PaperPort Image Printer 64-bit",
		"TightVNC",
		"HP ProtectTools Security Manager",
		"Drive Encryption For HP ProtectTools",
		"HP Vision Hardware Diagnostics",
		"Network64",
		"Microsoft Application Error Reporting",
		"Windows Internet Explorer 7",
		"Windows Internet Explorer 8",
		"Adobe Flash Player 10 ActiveX",
		"Adobe Flash Player 10 Plugin",
		"ATI - Software Uninstall Utility",
		"ATI Display Driver",
		"IrfanView",
		"LiveReg \\(Symantec Corporation\\)",
		"LiveUpdate 3.3 \\(Symantec Corporation\\)",
		"M5000 Programmer",
		"Spybot - Search & Destroy",
		"VideoLAN VLC media player",
		"Windows Genuine Advantage",
		"Windows Media Format Runtime",
		"Windows Media Player",
		"ATI Control Panel",
		"Symantec pcAnywhere",
		"32 Bit HP CIO Components Installer",
		"LightScribe System Software",
		"ATI Problem Report Wizard",
		"Microsoft Office Professional",
		"HP Help and Support",
		"Adobe Reader",
		"ActivePerl 5.8.8 Build 820",
		"Brother HL-4050CDN",
		"Realtek High Definition Audio Driver",
		"High Definition Audio Driver Package",
		"WinZip",
		"WebFldrs XP",
		"VSO Inspector",
		"QuickTime",
		"VCRedistSetup",
		"neroxml",
		"Windows Live ID Sign-in Assistant",
		"Microsoft Visual C",	
		
	);	
	my @HP_accepted_software_list=(
		"HP Desktop Keyboard",
		"HP Remote Solution",
		"HP Customer Experience Enhancements",
		"HP Setup",
		"HPAsset component for HP Active Support Library",
		"HP Support Information",
		"HP Support Assistant",
		"HP MAINSTREAM KEYBOARD",
		"HP Odometer",
		"HP Connect Solutions",
		"HP Remote Solution",
		"HP Vision Hardware Diagnostics",
		"HP Auto"
 
	);
	my @additional_accepted_software_list=(
		 
		"InterVideo", 
		"M5000",		
		"InterVideo WinDVD 8",
		"LiveReg (Symantec Corporation)",
		"M5000 Programmer",
		"Mozilla Maintenance Service",
		"Xobni",
		"Network",
		"BPDSoftware",
		"Scan",
		"ActiveCheck component for HP Active Support Library",
		"Toolbox",
		"BPDSoftware_Ini",		
		"Recovery Manager",
		"4500_Help",
		"bpd_scan",
		"InterVideo WinDVD 8",		
		"ProductContext",		 
		"6000E609_eDocs",	 		
		"Xobni Core",
		"WebReg",
		"Adobe AIR",		 
		"BPDSoftware",
		"PlayReady PC Runtime x86",
		"Microsoft_VC90_CRT_x86",
		"Officejet J4500 Series",		
		"BufferChm",
		"J4500",		
	);
	my @additional_accepted_software_list_II=(
		 
		"FastStone Image Viewer 4.8", 
		"PDF Complete Special Edition",		
		"Zinio Reader 4",
		"Windows Automated Installation Kit",
		"Google Chrome",
		"PaperPort Image Printer",
		"Google Update Helper"
 		
	);	
	
	if ($OS eq "XP") {
		@accepted_software_list	= @xp_accepted_software_list;
	}
	# Various Store Exceptions:
	if ($storenum == 4745) {	
		push(@accepted_software_list,@HP_accepted_software_list);		
		push(@accepted_software_list,@additional_accepted_software_list);
		push(@accepted_software_list,"LiveReg.*Symantec Corporation");
	}
	if ($storenum == 4177) {	
		push(@accepted_software_list,@HP_accepted_software_list);		
		push(@accepted_software_list,@additional_accepted_software_list);
		push(@accepted_software_list,@additional_accepted_software_list_II);
		push(@accepted_software_list,"Norton Internet Security");		
	}	
	if ($storenum == 4862) {	 
		push(@accepted_software_list,"Sprint SmartView");		
	}	
	if ($storenum == 7037) {	 
		push(@accepted_software_list,"UltraVnc");		
		push(@accepted_software_list,"Microsoft Policy Platform");		
	}			
	if ($storenum == 4639) {	 
		push(@accepted_software_list,"AxCrypt 1.7.2976.0");				
	}			
	if ($storenum == 2098) {	 
		push(@accepted_software_list,"EPSON Printer Software");				
	}			 	
	my $found_net_4_5_1=0;
	my $found_net_4_5_2=0;   
	# Are system backups configured?  Read from the system_registry_info.txt file
	if (-f $system_registry_info) {
		LogMsg(indent(1)."Reading $system_registry_info");
		open("INFO","$system_registry_info");
		my @registry_info=(<INFO>);
		close INFO;
		my $target="Uninstall";
		my $target_found=0;
		
		foreach my $r (@registry_info) {
			chomp($r);
			if ($target_found) {				
				if ($r =~ /DisplayName/) {
					my @tmp=split(/REG_SZ/,$r);
					my $sw=$tmp[-1]; 
					while ($sw =~ /^ /) {
						$sw =~ s/^ //g;
					}
					next if ($sw =~ /^Update/i);
					next if ($sw =~ /^Security Update/i);
					next if ($sw =~ /^Service Pack/i);
					next if ($sw =~ /^Microsoft Office/i);
					next if ($sw =~ /^Definition Update for Microsoft Office/i);
					next if ($sw =~ /^Microsoft .* Redistributable/i);
					if ($sw =~ /^Microsoft .Net Framework/i) {					
						LogMsg("- Found $sw");
						if ($sw =~ /4.5.1/) {
							$found_net_4_5_1=1;							
						}
						if ($sw =~ /4.5.2/) {
							$found_net_4_5_2=1;							
						}						
					}
					next if ($sw =~ /^Microsoft .Net Framework/i);
					next if ($sw =~ /^HP Officejet/i);
					next if ($sw =~ /HP ProtectTools/i);
					next if ($sw =~ /Mozilla Firefox/i);
										
					if ($OS eq "XP") {
						next if ($sw =~ /Windows XP/i);
						next if ($sw =~ /HotFix for Microsoft/i);
						next if ($sw =~ /Microsoft .Net Framework/i);
						next if ($sw =~ /Windows Defender/i);
						next if ($sw =~ /MSXML/i);
						next if ($sw =~ /Triversity/i);
						next if ($sw =~ /Tweak UI/i);
						next if ($sw =~ /Windows Updates/i);
						next if ($sw =~ /Security Update for Microsoft Windows/i);
						next if ($sw =~ /Security Update for Windows/i);						
					}
					push(@software_list,$sw);
				} 			
			}
			if ($r =~ /HKEY_LOCAL_MACHINE/) {
				$target_found=0;
			}
			if ($r =~ /$target/) {
				$target_found=1;
			}
		}
 		foreach my $s (@software_list) {
			my $accepted=0;
			foreach my $a (@accepted_software_list) {
				if (($s =~ /$a/) || ($s eq "$a")) {
					$accepted=1;
					if ($verbose) {
						print "Found $s installed - accepted\n";
					}
				}
			}
			unless ($accepted) {
				LogMsg("WARNING: $s is installed but is not expected");
				$warns++;
			}			
		}
	} else {
		unless ($OS eq "XP") {				
			LogMsg("WARNING: Failed to locate $system_registry_info file");
			$warns++;
		}
	}
	# Check the version of FireFox
	$warns+=check_firefox();
	# Check .Net
	my $flag="c:/temp/Net_install_request";
 	if ($found_net_4_5_1) {
		# If 4.5.1 is installed, request upgrade to 4.5.2
		
		if ($found_net_4_5_2) {
			if (-f $flag) {
				LogMsg("Removing $flag");
				unlink $flag;
			}
		} else {
			unless (-f $flag) {
				# Set a flag to install .Net 4.5.2				
				open (FLAG,">$flag");
				close FLAG;			
			}
		}
	} 	
	if ($found_net_4_5_2) {
		if (-f $flag) {
			LogMsg("Removing $flag");
			unlink $flag;
		}
	} 	
	# Check for specific updates
	$warns+=check_updates();
 
	if ($warns) {
        LogMsg("WARNING: $warns issues found checking software");
		$warning_count++;
    }  else {
	    LogMsg("$warns issues found checking software");
	}	
  
}

sub check_firefox {
###		
	# Check for FireFox and the version
 
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\"";
	my $found_mozilla=0;	
    my $found_java=0;
	my @info=`$cmd`; 
	my $warns=0;
	my $userName="Villages";
	if (($storenum == 7037) && ($hostname eq "Operationsmgr")) {
		$userName="Lynnellen"
	}
 
	if ($?) {
		#LogMsg("WARNING: Could not find Local_Machine Software settings in the registry");  
		#$warns++;     
	} else {   		
		foreach my $i (@info) { 
			if ($i =~ /mozilla/i) {
				unless ($i =~ /mozillaplugins/i) {					
					$found_mozilla=1;
				}
			}
			if ($i =~ /javasoft/i) {
					LogMsg("Found Java");				 			
					$found_java=1;
 
			}            
		}
	}
 
	if ($found_mozilla) {
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Mozilla\"";		  
		@info=`$cmd`;  
		my $firefox="unknown";
		if ($?) {
			LogMsg("WARNING: Could not find Mozilla settings in the registry");  
			$warns++;    
 
		} else {   
			LogMsg(indent(1)."Found FireFox");		
			foreach my $i (@info) { 
				if ($i =~ /firefox [0-9]/i) {
					my @tmp=split(/\s+/,$i);
					$firefox=$tmp[2];	
					my @ftmp=split(/\./,$firefox);
					my $installed_firefox_major=$ftmp[0];
					my $installed_firefox_minor=$ftmp[2];
					if ($local_system_hash{FireFoxRelease}) {
						my @ltmp=split(/\./,$local_system_hash{FireFoxRelease});
						my $current_firefox_major=$ltmp[0];
						my $current_firefox_minor=$ltmp[2];
						my $diff=($current_firefox_major - $installed_firefox_major);
						
						if ($diff > 1) {
							# Warn if the major number is more than one version different.
							unless (($local_system_hash{FireFoxRelease} eq $firefox) || ($local_system_hash{FireFoxRevision} eq $firefox)) {
								LogMsg("WARNING: FireFox is version $firefox but the latest is $local_system_hash{FireFoxRelease}");
								$warns++;
							}
						} elsif ($diff == 1) {
							if ($current_firefox_minor > 4) {
								# If the major number is only one different but the minor number is over 4, also warn
								unless (($local_system_hash{FireFoxRelease} eq $firefox) || ($local_system_hash{FireFoxRevision} eq $firefox)) {
									LogMsg("WARNING: FireFox is version $firefox but the latest is $local_system_hash{FireFoxRelease}");
									$warns++;
								}
							}
						} else {
							LogMsg("FireFox is version $firefox but the latest is $local_system_hash{FireFoxRelease}");
						}
					}
					LogMsg("System Info: Mozilla Firefox Version: $firefox");									
				}
			}
		}
		
		#if ($firefox eq "unknown") {
		
			# A more intensive search for the firefox version
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\" /s";		  
			@info=`$cmd`;  	
			my $target_found=0;
			foreach my $i (@info) { 
				if ($i =~ /displayname.*firefox/i) {
					$target_found=1;
				}
				if ($target_found) {
					if ($i =~ /DisplayVersion/i) {						
						my @tmp=split(/\s+/,$i);
						$firefox=$tmp[-1];					
						LogMsg("System Info: Mozilla Firefox Version: $firefox");							
					}
					if ($i =~ /DisplayName/i) {
						$target_found=0;
					}					
				}
			}			
		#}
		# In case it is installed in user space
		
		#if ($firefox eq "unknown") {
			
			my $uid = get_user("$userName");
			if ($uid) {
				my @tmp=split(/\\/,$uid);
				
				my $user=$tmp[1];
				if ($user) {
					# A more intensive search for the firefox version
					$cmd="reg query \"HKEY_USERS\\$user\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\" /s";	
				
					@info=`$cmd`;  	
					$target_found=0;
					foreach my $i (@info) { 
						if ($i =~ /displayname.*firefox/i) {
							$target_found=1;	
							LogMsg(indent(1)."FireFox found in user space");
						}
						if ($target_found) {
							if ($i =~ /DisplayVersion/i) {							
								my @tmp=split(/\s+/,$i);
								$firefox=$tmp[-1];											
								LogMsg("System Info: Mozilla Firefox Version: $firefox");							
							}
							if ($i =~ /DisplayName/i) {
								$target_found=0;
							}
						}
					}	
				} else {
					LogMsg(indent(1)."Failed to find User $userName");
				}
			} else {
				LogMsg(indent(1)."Failed to get UserID for $userName");
			}
		#}	
		if ($firefox eq "unknown") {		
			LogMsg(indent(1)."Unable to determine FireFox Version - Must not be installed");
			#$warns++;
		} else 	{
			my @tmp=split(/\./,$firefox);
			if ($tmp[0] < 20) {
				LogMsg(indent(1)."ISSUE: Firefox needs to be updated - current version: $firefox");
				#$warns++;
			}
		}
	
	}
	return $warns;
}

sub check_updates {
	# This was designed to uninstall the Get Windows 10 update but 
	# I found it ineffective.  Instead, I am just having the GWX.exe process
	# terminated.  For now, there is nothing to do here. - kdg
	return 0;
	
	my $warns=0;
	# Check if specific updates are installed.
	my @unwanted_updates=("kbid=3035583");
	my @wanted_updates=();
	my %uninstall_hash=(
		"kbid=3035583" => '1',
	);
	
	my $cmd="wmic qfe list /format:list";
	my @wmic_info=`$cmd`;
	foreach my $w (@wmic_info) {
		chomp($w);
		foreach my $u (@unwanted_updates) {			
			if ($w =~ /$u/) {
				if ($uninstall_hash{$u}) {
					uninstall_update($u);
				} else {
					LogMsg("WARNING: Found update $u to be installed");
					$warns++;
				}
			}
		}				
	}
	foreach my $wu (@wanted_updates) {
		my $found=0;
		foreach my $w (@wmic_info) {
			chomp($w);			
			if ($w =~ /$wu/) {
				$found=1;
			}
		}
		unless ($found) {
			LogMsg("WARNING: Update $wu is not installed");
			$warns++;		
		}			
	}		
	
	return $warns;
}

sub uninstall_update {
	my $kb=shift;
	my @tmp=split(/=/,$kb);
	$kb=$tmp[1];	
	my $cmd="wusa /uninstall /kb:$kb /quiet /norestart";
	$cmd="start /wait wusa /uninstall /kb:3035583 /quiet /norestart";
	LogMsg("Issuing cmd: $cmd");
	system($cmd);	
}

sub get_arch {
	my $cmd="wmic os get OSArchitecture";
	my @info=`$cmd`;
	my $arch=0;
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /64-bit/) {
			$arch="64-bit";
		}
		if ($i =~ /32-bit/) {
			$arch="32-bit";
		}				
	}
	return $arch;
}

sub check_antivirus { 
    my $warns=0;
	my $expected_version = 12;
	my $accepted_version = 11;
	my $install_enabled=0;	
	my $success_flag="c:/temp/sep12_installed.txt";	
    my $antivirus_version=check_antivirus_version(); 
	unless (($antivirus_version =~ /^$expected_version/) || ($antivirus_version =~ /^$accepted_version/)) {
		unless ($hostname eq "4201-Cumming") {
			LogMsg("NOTICE: Antivirus version $antivirus_version is installed but version $expected_version is expected.");
			#$warns++;			
		}		
	}
	#if ((($storenum =~ /^2/) ||  ($storenum =~ /^4756/)) && ($day_of_week > 0) && ($day_of_week < 6) ) {
	#if (($storenum =~ /^$day_of_week/) && ($day_of_week > 0) && ($day_of_week < 6) ) {	
	if  (($day_of_week > 0) && ($day_of_week < 6) ) {		
		if ($current_hour < 7) {
			$install_enabled=1;	 
		} else {
			LogMsg("Install would be enabled but it is too late in the day");
		}
	}	
	if ($force) {
		print "Enabling SEP upgrade...\n";
		$install_enabled=1;
	}	
	LogMsg("System Info: SEP: $antivirus_version");
	unless ($antivirus_version =~ /^$expected_version/) {
		#LogMsg("NOTICE: SAV Expected version $expected_version but found $antivirus_version ");
		LogMsg("WARNING: SAV Expected version $expected_version but found $antivirus_version ");
		$warns++;			
		my $sourcefile="//Edna/temp/install/SEP12/SEP12.zip";
		if ($revised_network) {
			$sourcefile="c:/temp/install/SEP12/SEP12.zip";
		}					
		if ($arch eq "64-bit") {
			$sourcefile="//Edna/temp/install/SEP12_64/SEP12_64.zip";
			if ($revised_network) {
				$sourcefile="c:/temp/install/SEP12_64/SEP12_64.zip";
			}			
		}
		if (-f $sourcefile) {
			if ($install_enabled) {
				LogMsg("Launching SEP upgrade...");
                sep_install($expected_version);
 
			} else {
				LogMsg("NOTICE: $sourcefile is present but install is not enabled for today.");
			}					
		} else {
			LogMsg("NOTICE: Antivirus version $antivirus_version is installed but version $expected_version is expected. ($sourcefile not found)");
			#$warns++;			
		}			
	}	
    if ($antivirus_version =~ /^11/) {
        check_antivirus_sep11();
	} elsif ($antivirus_version =~ /^12/) {
		#unless ($hostname eq "4201-Cumming") {
		#	LogMsg("WARNING: Antivirus is SEP 12");
		#	$warns++;
		#}
		check_antivirus_sep($antivirus_version);
    } elsif ($antivirus_version == 10) {		
		LogMsg("WARNING: Antivirus is SAV 10");
		$warns++;		
		check_antivirus_sav();
	} else {
		LogMsg("WARNING: SAV Antivirus version $antivirus_version is not supported");
        $warns++;
	}
    unless ($antivirus_version) {
        LogMsg("WARNING: SAV Failed to determine version of antivirus application");
        $warns++;
    }   
	if ($warns) {
		$warning_count++;
	}    
}

sub check_antivirus_old {
    my $antivirus_version=0;
	my $antivirus="C:/Program Files/Common Files/Symantec Shared/VirusDefs/definfo.dat";
	if (-f $antivirus) {
        $antivirus_version="10";        
    } 
    $antivirus="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection";
	if (-d $antivirus) {
        # Read this directory to get the current version
        
        opendir(SEP,"$antivirus");
		my @dir_list=readdir(SEP);
		close SEP;
        foreach my $d (@dir_list) {
            next if ($d eq ".");
            next if ($d eq "..");
            if ($d =~ /\d/) {      				
                if (-d "${antivirus}/${d}") {                 
					 
                    if (-d "${antivirus}/${d}/SRTSP") {                     
                        $antivirus_version=$d;
                    }
                }
            }            
        }              
    }     
	LogMsg("Antivirus version $antivirus_version found");
    if ($antivirus_version =~ /^12/) {
        check_antivirus_sep($antivirus_version);
    } elsif ($antivirus_version =~ /^11/) {
        check_antivirus_sep11();		
    } elsif ($antivirus_version == 10) {
		check_antivirus_sav();
	} else {
		# If sep is found, assume 11 if we did not determine it was 12
		
		my $sdir="C:/Program Files (x86)/Symantec/Symantec Endpoint Protection";
		if (-d $sdir) {
			check_antivirus_sep11();
			$antivirus_version="11";
		} else {
			LogMsg("WARNING: Antivirus version $antivirus_version is not supported");
			$warning_count++;
		}
	}
    unless ($antivirus_version) {
        LogMsg("WARNING: Failed to determine version of antivirus application");
        $warning_count++;
    }    
}
=pod
sub check_antivirus_sav {
	# Check that the antivirus definitions are up to date
	LogMsg("Checking Symantec antivirus definitions");
	my $warns=0;
 	my $antivirusquarantine;	
	my $found_landesk=0;	
	# Check the date of the antivirus definitions
	my $antivirus="C:/Program Files/Common Files/Symantec Shared/VirusDefs/definfo.dat";
 
	if ((-f $antivirus) && ($OS eq "XP")) {
		my $virus_updated=0;
			
		my @ltm = localtime();		
		my $month=$ltm[4]+1;
		my $year=$ltm[5]+1900;	
		my $last_month=($month - 1);
		my $last_month_year=$year;
		if ($month == 1) {
			$last_month=12;
			$last_month_year=($year - 1);
		}
		# Look for the log files for this month
		open(DEF,"$antivirus");
		my @file_info=(<DEF>);
		close DEF;
		if ($verbose) {
			LogMsg(indent(1)."Read contents of $antivirus");
		}
		my $deftime;

		foreach my $line (@file_info) {	
			if ($verbose) {
				LogMsg(indent(1)."Scanning line $line...");
			}			
 
			if ($line =~ /CurDefs/) {
				if ($verbose) {
					LogMsg("$line");
				}
				# What is the date of the current defs
				my @tmp=split(/=/,$line);
				my $date=$tmp[1]; 
				my $defyear=substr($date,0,4);
				my $defmonth=substr($date,4,2);
				my $defday=substr($date,6,2); 
				if ($verbose) {
					LogMsg("Determined date to be year: $defyear month: $defmonth day: $defday");
				}
				my $timemonth=($defmonth - 1);								
				$deftime = timelocal(0,0,0,$defday,$timemonth,$defyear);	
				# Calculate how old the definitions are
				my $timeseconds=time();
				my $timediff=($timeseconds - $deftime);	# This is how many seconds old the definitions are.
				# The definitions should not be more than two weeks old
				my $limit=(86400 * 22);
				my $notice_limit=(86400 * 15);
				my $update_now=0;
				if ($timediff > $limit) {
					LogMsg(indent(1)."WARNING: SAV Virus definitions are from ${defyear}-${defmonth}-${defday}");
					LogMsg(indent(1)."ISSUE: Virus definitions are from ${defyear}-${defmonth}-${defday}");
					$warns++;
					$update_now=1;
				} elsif ($timediff > $notice_limit) {
					LogMsg(indent(1)."NOTICE: SAV Virus definitions are currently from ${defyear}-${defmonth}-${defday} ");
					$update_now=1;
							
				} else {					 
					LogMsg(indent(1)."Virus definitions are from ${defyear}-${defmonth}-${defday}");					 
				}				
				if ($update_now) {
					# Run the update here
					my $update_tool="c:/program files/symantec antivirus/vpdn_lu.exe";
					my $update_tool_alt="C:/Program Files/Symantec/LiveUpdate/luall.exe";
					if (-f $update_tool) {
						unless ($no_action) {
							LogMsg(indent(1)."Calling virus definition update with $update_tool...");
							my $cmd="\"$update_tool\" /s";						
							system($cmd);
						}
					} elsif (-f $update_tool_alt) {
						unless ($no_action) {
							LogMsg(indent(1)."Calling virus definition update with $update_tool...");
							my $cmd="\"$update_tool_alt\" -s";						
							system($cmd);					
						}
					} else {
						LogMsg(indent(1)."WARNING: Failed to locate $update_tool or $update_tool_alt");
						$warns++;
					}					
				}
			}
		} 
		unless ($deftime) {
			LogMsg(indent(1)."WARNING: SAV Failed to determine date of virus definitions");
			$warns++;									
		}		
	} elsif ($OS eq "XP") {
		LogMsg(indent(1)."WARNING: SAV Failed to find $antivirus");
		$warns++;		
	}
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Intel\" /s";
	my @info=`$cmd`;  
 


	my $found_custom_scan=0;
	if ($?) {
		LogMsg("Could not find Intel settings in the registry");  
  
	} else {   		
		foreach my $i (@info) { 
			chomp($i);						
			if ($i =~ /Quarantine/) {
				$found_landesk=1;						
			}
		}
	}	

	if ($found_landesk) {
		LogMsg("Found LandDesk");
	 
		$antivirusquarantine="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/7.5/Quarantine";							     
		my $antivirusquarantine1="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec AntiVirus Corporate Edition/7.5/Quarantine";
		if (-d $antivirusquarantine1) {			 
			$antivirusquarantine=$antivirusquarantine1;
		}  
 	
		
		###
		# Check the log
		###			
	 
		my $antiviruslog="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec AntiVirus Corporate Edition/7.5/Logs";	
 
		$warns+=read_symantec_log($antiviruslog);		
		
		
		###
		# Check the purge settings
		###	
		my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\Quarantine\"";
		  
		my @info=`$cmd`;  
		my $fix_file="c:/temp/scripts/quarantine_purge.reg";
		my $fix_needed=0;
		if ($?) {
			LogMsg("WARNING: SAV Could not find Quarantine settings in the registry");  
			$warns++;    
		} else {   		
			foreach my $i (@info) {        
				chomp($i);			
				if ($i =~ /BackupItemPurgeEnabled/) {
					# See if this is set
					my @tmp=split(/\s+/,$i);
					my $setting=$tmp[-1];
					$setting=~ s/\)//g;				
					if ($setting eq "0x0") {
						if (-f $fix_file) {
							LogMsg(indent(1)."Setting backup purging...");
							if (fix_registry($fix_file)) {
								LogMsg(indent(2)."Fix completed");
							} else {
								LogMsg(indent(2)."Fix errored");
							}
							###
							# Now check the settings again
							###
							@info=`$cmd`;  
							if ($?) {
								LogMsg("WARNING: SAV Could not find Quarantine settings in the registry");  
								$warns++;        
							} else {   
								foreach my $i (@info) {        
									chomp($i);			
									if ($i =~ /BackupItemPurgeEnabled/) {
										# See if this is set
										my @tmp=split(/\s+/,$i);
										my $setting=$tmp[-1];
										$setting=~ s/\)//g;				
										if ($setting eq "0x0") {								
											LogMsg(indent(1)."WARNING: SAV Backup purging is not set");
											$warns++;
										} else {
											LogMsg(indent(1)."Backup purging has been set");
										}									
									}								   
								} 
							}	
						} else {
							LogMsg(indent(1)."WARNING: SAV Backup purging is not set but fix file is not found.");
							$warns++;					
						}
						###
					} elsif ($setting eq "0x1") {				
						LogMsg(indent(1)."Backup purging is enabled");
					}
				}               
			} 
		}	
		
	 	
		###
		###
		# Checking Quarantine
		###
			
		if (-d $antivirusquarantine) {	
			# Look for the Quarantined files
			opendir(UPLOAD,"$antivirus");
			my @dir_list=readdir(UPLOAD);
			close UPLOAD;	
			my $q_count=0;
			foreach my $file (@dir_list) {
				next if ($file eq ".");
				next if ($file eq "..");
			
				if ($file =~ /VBN/) {
					$q_count++;
					my $target="${antivirus}/${file}";
					my @file_info=stat($target);
					my $file_date_seconds=$file_info[9];	
					my @file_date_info=localtime($file_date_seconds); 
					my $file_date = sprintf("%04d-%02d-%02d", $file_date_info[5]+1900, $file_date_info[4]+1, $file_date_info[3]);
					# If the quarantine was in the past 24 hours, sound a warning
					my $current_timeseconds=time();				
					my $file_age = (($current_timeseconds - $file_info[9]) / 86400);				
					$file_age = sprintf("%.2f", $file_age);
					 
					$warns++;
					LogMsg(indent(1)."WARNING: SAV Found quarantine of file on $file_date");
							
					
					if ($verbose) {
						print "$file $file_date\n";					
					}				
				}				
			}		 
			LogMsg(indent(1)."Found $q_count quarantined items");
			 
		} else {
			LogMsg(indent(1)."WARNING: SAV Failed to find $antivirusquarantine");
			$warns++;		
		}			
		
		
		###
		# Check Log Rollover options in registry
		###
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\"";    
		@info=`$cmd`;  
		$fix_file="c:/temp/scripts/SAV_LogFileRollOverDays.bat";
		$fix_needed=0;
		my $expected_setting="0x190";
		if ($?) {
			LogMsg("WARNING: SAV Error trying to read the antivirus settings in the registry");  
			$warns++;   
			# It could be that Symantec does not even show up in the registry.  If that is the case, let's not even
			# go any further.
			#return;
		} else {   		
			my $found_setting=0;
			foreach my $i (@info) {        
				chomp($i);						
				if ($i =~ /LogFileRollOverDays/) {	
					$found_setting=1;
					# See if this is set
					my @tmp=split(/\s+/,$i);
					my $setting=$tmp[-1];
					$setting=~ s/\)//g;	
					@tmp=split(/x/,$setting);				
					my $dec_setting = hex($tmp[1]);
					unless ($setting eq "$expected_setting") {
						if (-f $fix_file) {
							LogMsg(indent(1)."Setting roll over days...");
							if (fix_registry_bat($fix_file)) {
								LogMsg(indent(2)."Fix completed");
							} else {
								LogMsg(indent(2)."Fix errored");
							}
							###
							# Now check the settings again
							###
							
							@info=`$cmd`;  
							if ($?) {
								LogMsg("WARNING: SAV Could not find the roll over days settings in the registry");  
								$warns++;        
							} else {   
								foreach my $i (@info) {        
									chomp($i);			
									if ($i =~ /LogFileRollOverDays/) {
										# See if this is set
										my @tmp=split(/\s+/,$i);
										my $setting=$tmp[-1];
										$setting=~ s/\)//g;	
										@tmp=split(/x/,$setting);				
										my $dec_setting = hex($tmp[1]);									
										unless ($setting eq "$expected_setting") {								
											LogMsg(indent(1)."WARNING: SAV Log Rollover settings are $dec_setting ($setting) but expected ($expected_setting)");
											$warns++;
										} else {
											LogMsg(indent(1)."Log Rollover has been set to $dec_setting days");
										}									
									}								   
								} 
							}	
						} else {
							LogMsg(indent(1)."WARNING: SAV Log Rollover is $dec_setting ($setting) but fix file is not found.");
							$warns++;					
						}
						###
					} elsif ($setting eq "$expected_setting") {				
						LogMsg(indent(1)."Log Rollover is set to $dec_setting days.");
					}
				}               
			} 
			unless ($found_setting) {

				# Check for the second setting - If this setting is not found, then we simply need to run the fix file.  The
				# fix file creates this entry and sets the first entry.  So, if this entry is missing, it probably just means
				# we need to run the bat file which should install it.
				my $key="LogFrequency";
				###			
				my $found_setting=0;
				foreach my $i (@info) {        
					chomp($i);	
					next if ($i eq '');				
					if ($i =~ /$key/) {	
						$found_setting=1;					
					}               
				} 	
				unless ($found_setting) {
					# We did not find the second setting so we are going to install both settings
					if (-f $fix_file) {
						if (fix_registry_bat($fix_file)) {
							LogMsg(indent(2)."Fix completed");
						} else {
							LogMsg(indent(2)."Fix errored");
						}	
						# Now we check it again
						@info=`$cmd`;  
						if ($?) {
							LogMsg("WARNING: SAV Could not check the roll over days settings in the registry");  
							$warns++;        
						} else { 
							$found_setting=0;
							foreach my $i (@info) {        
								chomp($i);			
								if ($i =~ /LogFileRollOverDays/) {
									$found_setting=1;
									# See if this is set
									my @tmp=split(/\s+/,$i);
									my $setting=$tmp[-1];
									$setting=~ s/\)//g;	
									@tmp=split(/x/,$setting);				
									my $dec_setting = hex($tmp[1]);									
									unless ($setting eq "$expected_setting") {								
										LogMsg(indent(1)."WARNING: SAV Log Rollover settings are $dec_setting ($setting) but expected ($expected_setting)");
										$warns++;
									} else {
										LogMsg(indent(1)."Log Rollover has been set to $dec_setting days");
									}									
								}								   
							} 
						}
						unless ($found_setting) {	
							LogMsg(indent(1)."WARNING: SAV Failed to find Roll Over settings");
							$warns++;				
						}
						
					} else {
						LogMsg(indent(1)."WARNING: SAV Log Rollover is incorrect but fix file is not found.");
						$warns++;					
					}
					###
				}
				###
			}
		}		
		###
		# Look for a custom scan
		###
		$cmd="reg query \"HKEY_CURRENT_USER\\SOFTWARE\\Intel\\LANDesk\\VirusProtect6\\CurrentVersion\\Custom Tasks\\TaskPadScheduled\" /s";
		@info=`$cmd`;  
		 
		foreach my $i (@info) { 
			chomp($i);			
			if ($i =~ /CustomScheduledScan/) {
				$found_custom_scan=1;		 
			}
		}			
	}  	
	unless ($found_custom_scan) {
		LogMsg("WARNING: No Custom Scheduled Scan found");
		$warns++
	}
	if ($warns) {
		LogMsg("WARNING: SAV There were $warns issues checking antivirus definition date");
		$warning_count++;
	}
    LogMsg("Finished checking antivirus definitions");		
}

=cut


sub check_antivirus_sep {
    my $version=shift;
 
	my $warns=0;	
    LogMsg("Checking antivirus - Symantec Endpoint Protection");
	unless ($hostname eq "4201-Cumming") {
		# Check that the installation is as unmanaged
		my $sep_found=0;
		my $smc_found=0;
		my $smc_managed=0;
		my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\"  ";    
		my @info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {        
				if ($i =~ /Symantec Endpoint Protection/) {						
					$sep_found = 1;			
				}
			} 
		}                        
			
		if ($sep_found) {
			$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\"  ";    
			@info=`$cmd`;
			unless ($?) {
				foreach my $i (@info) {        
					if ($i =~ /SMC/) {						
						$smc_found = 1;					
					}
				}		

			}                        
		}
		if ($smc_found) {
			$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\SMC\" /s";    
			@info=`$cmd`;
			unless ($?) {
				foreach my $i (@info) {
					if ($i =~ /CommunicationStatus/) {	
						$smc_managed=1;				 				
					}						 
				}		

			}                        
		}	
		if ($smc_managed) {
			LogMsg(indent(1)."WARNING: Symantec Endpoint Protection is installed as managed");
			$warns++;
		}	
	}
	# Determine directory
 
	my $antivirus_dir="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection";
	my $version_dir="";
	opendir(UPLOAD,"$antivirus_dir");
	my @dir_list=readdir(UPLOAD);		
	close UPLOAD;		
	foreach my $d (@dir_list) {
		if ($d =~ /$version/) {
			$version_dir=$d; 
		}
	}
	# Check that the antivirus definitions are up to date
	LogMsg("Checking Symantec antivirus definitions");
	
    my %def_hash=(
        'VirusDefs' => 'Virus',
        'BASHDefs' => 'Proactive Threat',
        'IPSDefs' => 'Network Threat',
    );
    my %def_limit_hash=(
        'VirusDefs' => '22',
        'BASHDefs' => '60',
        'IPSDefs' => '22',
    );	
    for my $def_dir (keys (%def_hash)) {
	 
        my $def_label=$def_hash{$def_dir};
	 
        # Check the date of the antivirus definitions					   
        my $antivirus="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/$version_dir/Data/Definitions/$def_dir/definfo.dat";
 
        if (-f $antivirus) {
            my $virus_updated=0;			
            my @ltm = localtime();		
            my $month=$ltm[4]+1;
            my $year=$ltm[5]+1900;	
            my $last_month=($month - 1);
            my $last_month_year=$year;
            if ($month == 1) {
                $last_month=12;
                $last_month_year=($year - 1);
            }
            # Look for the log files for this month
            open(DEF,"$antivirus");
            my @file_info=(<DEF>);
            close DEF;
            if ($verbose) {
                LogMsg(indent(1)."Read contents of $antivirus");
            }
            my $deftime;

            foreach my $line (@file_info) {	
                if ($verbose) {
                    LogMsg(indent(1)."Scanning line $line...");
                }			
     
                if ($line =~ /CurDefs/) {
                    if ($verbose) {
                        LogMsg("$line");
                    }
                    # What is the date of the current defs
                    my @tmp=split(/=/,$line);
                    my $date=$tmp[1]; 
                    my $defyear=substr($date,0,4);
                    my $defmonth=substr($date,4,2);
                    my $defday=substr($date,6,2); 
                    if ($verbose) {
                        LogMsg("Determined date to be year: $defyear month: $defmonth day: $defday");
                    }
                    my $timemonth=($defmonth - 1);								
                    $deftime = timelocal(0,0,0,$defday,$timemonth,$defyear);	
                    # Calculate how old the definitions are
                    my $timeseconds=time();
                    my $timediff=($timeseconds - $deftime);	# This is how many seconds old the definitions are.
                    # The definitions should not be more than two weeks old if they are set to update daily.
                    # If they are more than two weeks old and they are not set to update daily, set daily updates.
                    # If they are set to weekly and they never get set to daily, then the warning will go out after three weeks.
                    my $limit=(86400 * $def_limit_hash{$def_dir});
 
                    my $notice_limit=(86400 * 15);
                    if (($timediff > $notice_limit) && ($timediff < $limit)) {
                        LogMsg(indent(1)."NOTICE: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");						
                        #schedule_update();                    
                    } elsif  ($timediff > $limit) {           
                        LogMsg(indent(1)."WARNING: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");
                        $warns++;                        
						#schedule_update();                    
                    } else {					 
                        LogMsg(indent(1)."$def_label definitions are from ${defyear}-${defmonth}-${defday}");					 
                    }	                    
                }
            } 
            unless ($deftime) {
                LogMsg(indent(1)."WARNING: SAV Failed to determine date of $def_label definitions");
                $warns++;									
            }		
        } else {
            LogMsg(indent(1)."Did not find $antivirus");
            #$warns++;             
        }            
    }
	check_antivirus_scan("$version");	
	if ($warns) {
        $warning_count++;
    }
}


sub check_antivirus_scan {
	# Look for the CustomScheduledScan
	my $version=shift;
	my $found_custom_scan=0;
	my $scan_id=0;
	my $enabled=0;
	my $dow=0;
	my $mod=0;
	my $warns=0;
	my $cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\TaskPadScheduled\" "; 	
 
	if ($version =~ /^12/) {
		my @id_list;
		my @custom_id_list;
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\" "; 
		if ($arch eq "32-bit") {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\" "; 
		}
		my @info=`$cmd`;		
		foreach my $i (@info) {
			chomp($i);			
			my @tmp=split(/\\/,$i);
			my $id=$tmp[-1];
			if ($id) {
				push(@id_list,$id);
			}			
			if ($i =~ /CustomScheduledScan/) {
				$found_custom_scan=1;
			}
		}			
		foreach my $i (@id_list) {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\TaskPadScheduled\" "; 						
			if ($arch eq "32-bit") {
				$cmd="reg query \"HKEY_LOCAL_MACHINE\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Scheduler\\$i\\Custom Tasks\\TaskPadScheduled\" "; 						
			}			
			@info=`$cmd`;			
			foreach my $i (@info) {
				chomp($i);				
				if ($i =~ /CustomScheduledScan/) {
					$found_custom_scan=1;					
				}
			}				
		}
 
		if ($found_custom_scan) {
			LogMsg(indent(1)."Found CustomScheduledScan");
			return 1;
		} else {
			LogMsg("WARNING: Failed to find CustomScheduledScan");
			$warns++;					
		 
		}
	}
	my @info=`$cmd`;
	
	foreach my $i (@info) {
		chomp($i);		
		if ($i =~ /CustomScheduledScan/) {
			$found_custom_scan=1;
		}
	}	
	if ($found_custom_scan) {
		if ($verbose) {
			LogMsg(indent(1)."Found Custom Scan - looking for its scan id...");
		}
		$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\TaskPadScheduled\\CustomScheduledScan\" ";    
		@info=`$cmd`;		
		foreach my $i (@info) {
			chomp($i);				
			if (($i =~ /NO NAME/) || ($i =~ /default/i)) {											
				my @tmp = split(/\s+/,$i);				
				$scan_id = $tmp[-1];	
				if ($verbose) {
					print "\nScan id for CustomScheduledScan: ($scan_id)\n";
				}
			}
		}		
	} else {
		LogMsg("WARNING: Failed to find CustomScheduledScan on $hostname");
		$warns++;		
	}
	if ($scan_id) {						
		$cmd="reg query \"HKEY_CURRENT_USER\\Software\\Symantec\\Symantec Endpoint Protection\\AV\\Custom Tasks\\$scan_id\\Schedule\" ";    
		@info=`$cmd`;		
		foreach my $i (@info) {
			chomp($i);	
			
			if ($i =~ /Enabled/)  {											
				unless ($i =~ /Missed/) {
					my @tmp = split(/\s+/,$i);				
					$enabled = $tmp[-1];									
				}
			}
			if ($i =~ /DayOfWeek/) {											
				my @tmp = split(/\s+/,$i);				
				$dow = $tmp[-1];								
			}	
			if ($i =~ /MinOfDay/) {											
				my @tmp = split(/\s+/,$i);				
				$mod = $tmp[-1];						
			}				
		}	
		# Evaluate what we found
		unless ($enabled eq "0x1") {
			LogMsg("WARNING: CustomScheduledScan is not enabled");
			$warns++;
		}
		unless ($dow eq "0x0") {
			LogMsg("WARNING: CustomScheduledScan is not set for Sunday");
			$warns++;
		} else {
			LogMsg(indent(1)."CustomScheduledScan is set for Sunday");
		}
		
 
		my $dec_mod = $mod;
		$dec_mod =~ s/0x//;
		$dec_mod = (hex($dec_mod)/60);
		if (($dec_mod eq "12") || ($dec_mod eq "0")) {
			LogMsg(indent(1)."CustomScheduledScan is set for $dec_mod");
		} else {
			LogMsg(indent(1)."NOTICE: CustomScheduledScan is set for $dec_mod");
		}
 	
		if ($verbose) {
			if ($enabled) {
				LogMsg(indent(1)."Scan is enabled");
			}
			if ($dow eq "0x0") {
				unless ($mod eq "0x0") {
					my $dec_mod = $mod;					
					$dec_mod =~ s/0x//;
					$dec_mod = (hex($dec_mod)/60);
					LogMsg(indent(1)."Scan runs on Sunday at $dec_mod");					
				} else {
					LogMsg(indent(1)."Scan runs on Sunday ");
				}
			}
		}
	} else {
		LogMsg("WARNING: Failed to find Scan ID");
		$warns++;		
	}
	if ($warns) {
		$warning_count++;
	}
	
}


sub check_antivirus_sep11 {
     
	my $warns=0;
    LogMsg("Checking antivirus - Symantec Endpoint Protection 11");
	# Check that the installation is as unmanaged
	my $sep_found=0;
	my $smc_found=0;
	my $smc_managed=0;
	my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\"  ";    
	my @info=`$cmd`;
	unless ($?) {
		foreach my $i (@info) {        
			if ($i =~ /Symantec Endpoint Protection/) {						
				$sep_found = 1;			
			}
		}	
	}                        

	if ($sep_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\"  ";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {        
				if ($i =~ /SMC/) {						
					$smc_found = 1;					
				}
			}	
		}                        
	
	}
	if ($smc_found) {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\SMC\" /s";    
		@info=`$cmd`;
		unless ($?) {
			foreach my $i (@info) {
				if ($i =~ /CommunicationStatus/) {	
					$smc_managed=1;				 				
				}						 
			}	
		}                        
	
	}	
	if ($smc_managed) {
		LogMsg(indent(1)."WARNING: Symantec Endpoint Protection is installed as managed");
		$warns++;
	}
	# Check that the antivirus definitions are up to date
	LogMsg("Checking Symantec antivirus definitions");
	my $antivirus_def=0;
    my %def_hash=(
        'VirusDefs' => 'Virus',
        'BASHDefs' => 'Proactive Threat',
        'IPSDefs' => 'Network Threat',
    );
	# We are typically only installing the antivirus 
    %def_hash=(
        'VirusDefs' => 'Virus', 	 
    );	
	
	#if ($arch eq "64-bit") {
	if ($OS =~ /win7/i) {
		# If this is a 64-bit system, we will need to get the definition dates from the registry later
		# so I am truncating this hash to simply skip this section
		#%def_hash=();
	}
    for my $def_dir (keys (%def_hash)) {
        my $def_label=$def_hash{$def_dir};
        # Check the date of the antivirus definitions
        
        my $antivirus="C:/Program Files/Common Files/Symantec Shared/$def_dir/definfo.dat";
		my $antivirus_alt="C:/ProgramData/Symantec/Definitions/definfo.dat";
		
		my $antivirus_alt1="C:/Program Files/Common Files/Symantec Shared/VIRUSD~1/definfo.dat";
		my $antivirus_alt2="C:/Documents and Settings/All Users/Application Data/Symantec/Symantec Endpoint Protection/CurrentVersion/Data/Definitions/$def_dir/definfo.dat";		
		my $antivirus_alt3="C:/ProgramData/Symantec/Definitions/VirusDefs/definfo.dat";
		unless (-f $antivirus) {
			#LogMsg("Cannot find $antivirus");
			if (-f $antivirus_alt) {
				LogMsg("Found $antivirus_alt");
				$antivirus=$antivirus_alt;
			} elsif (-f $antivirus_alt1) {	
				if ($verbose) {
					print "Found $antivirus_alt1\n";			
				}
				$antivirus=$antivirus_alt1;
			} elsif (-f $antivirus_alt2) {
				if ($verbose) {
					print "Cannot locate $antivirus_alt1\n";
					print "Found $antivirus_alt2\n";
				}
				$antivirus=$antivirus_alt2;
 
			} elsif (-f $antivirus_alt3) {
				if ($verbose) {
					print "Cannot locate $antivirus_alt1\n";
					print "Found $antivirus_alt3\n";
				}
				$antivirus=$antivirus_alt3;
			} else {
				if ($verbose) {
					print "Cannot locate $antivirus_alt3\n";
				}
			}			
 
		}
        if (-f $antivirus) {
            my $virus_updated=0;			
            my @ltm = localtime();		
            my $month=$ltm[4]+1;
            my $year=$ltm[5]+1900;	
            my $last_month=($month - 1);
            my $last_month_year=$year;
            if ($month == 1) {
                $last_month=12;
                $last_month_year=($year - 1);
            }
            # Look for the log files for this month
            open(DEF,"$antivirus");
            my @file_info=(<DEF>);
            close DEF;
            if ($verbose) {
                LogMsg(indent(1)."Read contents of $antivirus");
            }
            my $deftime;

            foreach my $line (@file_info) {	
                if ($verbose) {
                    LogMsg(indent(1)."Scanning line $line...");
                }			
     
                if ($line =~ /CurDefs/) {
                    if ($verbose) {
                        LogMsg("$line");
                    }
                    # What is the date of the current defs
                    my @tmp=split(/=/,$line);
                    my $date=$tmp[1]; 
                    my $defyear=substr($date,0,4);
                    my $defmonth=substr($date,4,2);
                    my $defday=substr($date,6,2); 
                    if ($verbose) {
                        LogMsg("Determined date to be year: $defyear month: $defmonth day: $defday");
                    }
                    my $timemonth=($defmonth - 1);								
                    $deftime = timelocal(0,0,0,$defday,$timemonth,$defyear);	
                    # Calculate how old the definitions are
                    my $timeseconds=time();
                    my $timediff=($timeseconds - $deftime);	# This is how many seconds old the definitions are.
                    # The definitions should not be more than two weeks old if they are set to update daily.
                    # If they are more than two weeks old and they are not set to update daily, set daily updates.
                    # If they are set to weekly and they never get set to daily, then the warning will go out after three weeks.
                    my $limit=(86400 * 22);
                    my $notice_limit=(86400 * 15);
                    if (($timediff > $notice_limit) && ($timediff < $limit)) {
                        LogMsg(indent(1)."NOTICE: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");						
                        schedule_update();                    
                    } elsif  ($timediff > $limit) {           
                        LogMsg(indent(1)."WARNING: SAV $def_label def are from ${defyear}-${defmonth}-${defday} ");
                        $warns++;                        
						schedule_update();                    
                    } else {					 
                        LogMsg(indent(1)."$def_label definitions are from ${defyear}-${defmonth}-${defday}");					 
                    }	  
					$antivirus_def="${defyear}-${defmonth}-${defday}";
                }
            } 
            unless ($deftime) {
                LogMsg(indent(1)."WARNING: SAV Failed to determine date of $def_label definitions");
                $warns++;									
            }		
        } else {
            LogMsg(indent(1)."WARNING: SAV Failed to find $antivirus");
            $warns++;             
        }            
    }
	###
	#	Proactive Threat Definitions
	###
	my $proactive_threat_def=0;
	$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\AV\\Storages\\SymHeurProcessProtection\\RealTimeScan\"";		  
	 
	@info=`$cmd`;  
	foreach my $i (@info) {        
		chomp($i);
		if ($i =~ /ContentDate/i) {
			 
			my @tmp=split(/\s+/,$i);
			my $ptd=$tmp[-1];
			my $l=length($ptd);
			my $y; 
			my $m; 
			my $d; 
			my $r; 
			if ($l == 9) {
				$y = substr($ptd,0,2);
				$m = substr($ptd,2,2);
				$d = substr($ptd,4,2);
				$r = substr($ptd,6,3);			
			} elsif ($l == 8) {
				$y = substr($ptd,0,1);
				$m = substr($ptd,1,2);
				$d = substr($ptd,3,2);
				$r = substr($ptd,5,3);				
			}  
			if (($y) && ($m) && ($d)) {
				$y = sprintf("%02d",$y);								
				$y = "20".$y;							
				$proactive_threat_def = "$y"."-"."$m"."-"."$d"; 
				my $timemonth=($m - 1);				 
				my $deftime = timelocal(0,0,0,$d,$timemonth,$y);
				my $timeseconds=time();
				my $timediff=($timeseconds - $deftime);	
				my $limit=(86400 * 22);
				my $notice_limit=(86400 * 15);
				if (($timediff > $notice_limit) && ($timediff < $limit)) {
					LogMsg(indent(1)."NOTICE: SAV Proactive Threat Definitions are from $proactive_threat_def ");										
				} elsif  ($timediff > $limit) {           
					LogMsg(indent(1)."WARNING: SAV Proactive Threat definitions are from $proactive_threat_def ");
					$warns++;                        
				} else {					 
					LogMsg(indent(1)."Proactive Threat definitions are from $proactive_threat_def");					 
				}		
			}
		}
	}
	unless ($proactive_threat_def) {
		LogMsg(indent(1)."WARNING: SAV Proactive Threat Definitions date was not determined.");
		$warns++;
	}
	###
	#	Virus Defs
	###
#	[HKEY_LOCAL_MACHINE\SOFTWARE\Symantec\Symantec Endpoint Protection\Content\{C60DC234-65F9-4674-94AE-62158EFCA433}]		
	unless ($antivirus_def) {		
		LogMsg(indent(1)."Looking for virus definition date from the registry...");
		my $info_file="c:/temp/scripts/reg_query.txt";
		if (-f $info_file) {
			unlink $info_file;
		}
		if ($arch eq "64-bit") {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{1CD85198-26C6-4bac-8C72-5D34B025DE35}\"";		  
		} else {
			$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{C60DC234-65F9-4674-94AE-62158EFCA433}\"";		  
			#[HKEY_LOCAL_MACHINE\SOFTWARE\Symantec\Symantec Endpoint Protection\Content\{C60DC234-65F9-4674-94AE-62158EFCA433}]
		}
		 
		#if ($arch eq "64-bit") {
		$cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{1CD85198-26C6-4bac-8C72-5D34B025DE35}\"";		  
		if ($OS =~ /win7/i) {
			$cmd .= " > $info_file";
			# Create a batch file
			my $batchfile="c:/temp/scripts/task.bat";
			open (TASK,">$batchfile");
			print TASK "$cmd\n";
			close TASK;	

			my $ref=get_def_info();
			@info=@$ref;			
		} else {
			@info=`$cmd`;
		}
 
		foreach my $i (@info) {        
			chomp($i);			
			if ($i =~ /CurrentSequenceNum/i) {	
				 
				my @tmp=split(/\s+/,$i);
				my $ptd=$tmp[-1];
				my $l=length($ptd);
				my $y; 
				my $m; 
				my $d; 
				my $r; 
				if ($l == 9) {
					$y = substr($ptd,0,2);
					$m = substr($ptd,2,2);
					$d = substr($ptd,4,2);
					$r = substr($ptd,6,3);			
				} elsif ($l == 8) {
					$y = substr($ptd,0,1);
					$m = substr($ptd,1,2);
					$d = substr($ptd,3,2);
					$r = substr($ptd,5,3);				
				}  
		 
				if (($y) && ($m) && ($d)) {
					$y = sprintf("%02d",$y);
					$y = "20".$y;			
					$antivirus_def = "$y"."-"."$m"."-"."$d"; 
					
					my $timemonth=($m - 1);				 
					my $deftime = timelocal(0,0,0,$d,$timemonth,$y);
					my $timeseconds=time();
					my $timediff=($timeseconds - $deftime);	
					my $limit=(86400 * 22);
					my $notice_limit=(86400 * 15);
					if (($timediff > $notice_limit) && ($timediff < $limit)) {
						LogMsg(indent(1)."NOTICE: SAV AntiVirus Definitions are from $antivirus_def ");										
					} elsif  ($timediff > $limit) {           
						LogMsg(indent(1)."WARNING: SAV AntiVirus definitions are from $antivirus_def ");
						$warns++;                        
					} else {					 
						LogMsg(indent(1)."SAV AntiVirus definitions are from $antivirus_def");					 
					}		
				}
			}
		}

	}
	unless ($antivirus_def) {
		LogMsg(indent(1)."WARNING: SAV AntiVirus Definitions date was not determined.");
		$warns++;
	}	
	###
	check_antivirus_scan("11");	
	if ($warns) {
        $warning_count++;
    }
}

sub get_def_info {
	my $schedule_time=get_schedule_time();
	my @info;
	my $batchfile="c:/temp/scripts/task.bat";
	my $info_file="c:/temp/scripts/reg_query.txt";	
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{1CD85198-26C6-4bac-8C72-5D34B025DE35}\"";		  	
	my $cmd_alt="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\Content\\{C60DC234-65F9-4674-94AE-62158EFCA433}\"";
	#LogMsg(indent(1)."Current time: $hour:$minute:$second - Trigger at: $trigger_hour:$trigger_minute:$trigger_second");
	#my $schedule_cmd="at $trigger_hour:$trigger_minute:$trigger_second \"$batchfile\" /s";			
	my $schedule_cmd="at $schedule_time \"$batchfile\" /s";			
	`$schedule_cmd`;
	my $count_down=65;
	until ((-f $info_file) || ($count_down < 1)) {				
		sleep 1;
		$count_down--;				
	}
	if (-f $info_file) {
		LogMsg(indent(1)."Found $info_file");
		open(INFO,"$info_file");
		@info=(<INFO>);
		close INFO;	
 
	} else {
		LogMsg(indent(1)."Failed to find $info_file...try again");
		# Try one more time
		$schedule_time=get_schedule_time();
		$schedule_cmd="at $schedule_time \"$batchfile\" /s";
		`$schedule_cmd`;	

		$count_down=65;
		until ((-f $info_file) || ($count_down < 1)) {				
			sleep 1;
			$count_down--;				
		}
		
		if (-f $info_file) {
			LogMsg(indent(1)."Found $info_file after second try.");
			open(INFO,"$info_file");
			@info=(<INFO>);
			close INFO;			
		} else {		
			LogMsg(indent(1)."WARNING Cannot locate $info_file");
		}
	}
	my $ref=\@info;
	return $ref;
}

sub get_schedule_time {
	my @ltm_array = localtime();			 
	my $hour=$ltm[2];
	my $minute=$ltm[1];
	my $second=$ltm[0];
	my $trigger_hour=$hour;
	my $trigger_minute=$minute;
	my $trigger_second=$second;

	if ($minute < 59) {
		$trigger_minute++; 
	} else {
		$trigger_hour++;
		$trigger_minute=0;
	}	
	my $time="$trigger_hour:$trigger_minute";
	return $time;
}

sub schedule_update {
	# Schedule the update here
	my $update_tool="c:/program files/symantec antivirus/vpdn_lu.exe";
	$update_tool="C:/Program Files/Symantec/LiveUpdate/Luall.exe";
	my $update_tool_alt="C:/Program Files (x86)/Symantec/LiveUpdate/Luall.exe";
	if (-f $update_tool_alt) {
		$update_tool=$update_tool_alt;
	}	
	if (-f $update_tool) {
		LogMsg(indent(1)."Scheduling virus definition update...");
		my $cmd="at 6:00 \"$update_tool\" -s";		
		system($cmd);		 
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $update_tool");
		$warning_count++;
	}   
}
sub schedule_system_backup {
	# This is not implemented.
	# Schedule the backup here
	my $update_tool="c:/program files/symantec antivirus/vpdn_lu.exe";
	$update_tool="C:/Program Files/Symantec/LiveUpdate/Luall.exe";
	my $update_tool_alt="C:/Program Files (x86)/Symantec/LiveUpdate/Luall.exe";
	if (-f $update_tool_alt) {
		$update_tool=$update_tool_alt;
	}
	if (-f $update_tool) {
		LogMsg(indent(1)."Scheduling a System Backup...");
		my $cmd="at 6:00 \"$update_tool\" -s";		
		system($cmd);	
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $update_tool");
		$warning_count++;
	}   
}

sub antivirus_update {
}

sub read_symantec_log {
	LogMsg("Reading the log files...");
	###
	# Reading the log files
	###
	my $warns=0;
	my $antivirus=shift;	
	my $scanned=0;
	my $scanned_count=0;
	my $scanned_day=0;
	
	if (-d $antivirus) {
		LogMsg("Reading $antivirus...");
        my %event_hash=(
        '1' => 'GL_EVENT_IS_ALERT', 
        '2' => 'GL_EVENT_SCAN_STOP',
        '3' => 'GL_EVENT_SCAN_START',
        '4' => 'GL_EVENT_PATTERN_UPDATE',
        '5' => 'GL_EVENT_INFECTION',
        '6' => 'GL_EVENT_FILE_NOT_OPEN',
        '7' => 'GL_EVENT_LOAD_PATTERN',
        '10' => 'GL_EVENT_CHECKSUM',
        '11' => 'GL_EVENT_TRAP',
        '12' => 'GL_EVENT_CONFIG_CHANGE',
        '13' => 'GL_EVENT_SHUTDOWN',
        '14' => 'GL_EVENT_STARTUP',
        '16' => 'GL_EVENT_PATTERN_DOWNLOAD',
        '17' => 'GL_EVENT_TOO_MANY_VIRUSES',
        '18' => 'GL_EVENT_FWD_TO_QSERVER',
        '19' => 'GL_EVENT_SCANDLVR',
        '20' => 'GL_EVENT_BACKUP',
        '21' => 'GL_EVENT_SCAN_ABORT',
        '22' => 'GL_EVENT_RTS_LOAD_ERROR',
        '23' => 'GL_EVENT_RTS_LOAD',
        '24' => 'GL_EVENT_RTS_UNLOAD',
        '25' => 'GL_EVENT_REMOVE_CLIENT',
        '26' => 'GL_EVENT_SCAN_DELAYED',
        '27' => 'GL_EVENT_SCAN_RESTART',
        '28' => 'GL_EVENT_ADD_SAVROAMCLIENT_TOSERVER',
        '29' => 'GL_EVENT_REMOVE_SAVROAMCLIENT_FROMSERVER',
        '30' => 'GL_EVENT_LICENSE_WARNING',
        '31' => 'GL_EVENT_LICENSE_ERROR',
        '32' => 'GL_EVENT_LICENSE_GRACE',
        '33' => 'GL_EVENT_UNAUTHORIZED_COMM',
        '34' => 'GL_EVENT_LOG_FWD_THRD_ERR',
        '35' => 'GL_EVENT_LICENSE_INSTALLED',
        '36' => 'GL_EVENT_LICENSE_ALLOCATED',
        '37' => 'GL_EVENT_LICENSE_OK',
        '38' => 'GL_EVENT_LICENSE_DEALLOCATED',
        '39' => 'GL_EVENT_BAD_DEFS_ROLLBACK',
        '40' => 'GL_EVENT_BAD_DEFS_UNPROTECTED',
        '41' => 'GL_EVENT_SAV_PROVIDER_PARSING_ERROR',
        '42' => 'GL_EVENT_RTS_ERROR',
        '43' => 'GL_EVENT_COMPLIANCE_FAIL',
        '44' => 'GL_EVENT_COMPLIANCE_SUCCESS',
        '45' => 'GL_EVENT_SECURITY_SYMPROTECT_POLICYVIOLATION',
        '46' => 'GL_EVENT_ANOMALY_START',
        '47' => 'GL_EVENT_DETECTION_ACTION_TAKEN',
        '48' => 'GL_EVENT_REMEDIATION_ACTION_PENDING',
        '49' => 'GL_EVENT_REMEDIATION_ACTION_FAILED', 
        '50' => 'GL_EVENT_REMEDIATION_ACTION_SUCCESSFUL',
        '51' => 'GL_EVENT_ANOMALY_FINISH',
        '52' => 'GL_EVENT_COMMS_LOGIN_FAILED',
        '53' => 'GL_EVENT_COMMS_LOGIN_SUCCESS',
        '54' => 'GL_EVENT_COMMS_UNAUTHORIZED_COMM',
        '55' => 'GL_EVENT_CLIENT_INSTALL_AV',
        '56' => 'GL_EVENT_CLIENT_INSTALL_FW',
        '57' => 'GL_EVENT_CLIENT_UNINSTALL',
        '58' => 'GL_EVENT_CLIENT_UNINSTALL_ROLLBACK',
        '59' => 'GL_EVENT_COMMS_SERVER_GROUP_ROOT_CERT_ISSUE',
        '60' => 'GL_EVENT_COMMS_SERVER_CERT_ISSUE',
        '61' => 'GL_EVENT_COMMS_TRUSTED_ROOT_CHANGE',
        '62' => 'GL_EVENT_COMMS_SERVER_CERT_STARTUP_FAILED',
        '63' => 'GL_EVENT_CLIENT_CHECKIN',
        '64' => 'GL_EVENT_CLIENT_NO_CHECKIN',
        '65' => 'GL_EVENT_SCAN_SUSPENDED',
        '66' => 'GL_EVENT_SCAN_RESUMED',
        '67' => 'GL_EVENT_SCAN_DURATION_INSUFFICIENT',
        '68' => 'GL_EVENT_CLIENT_MOVE',
        '69' => 'GL_EVENT_SCAN_FAILED_ENHANCED',
        '70' => 'GL_EVENT_MAX_EVENT_NUMBER',
        '72' => 'GL_EVENT_INTERESTING_PROCESS_DETECTED_START',
        '73' => 'GL_EVENT_LOAD_ERROR_COH',
        '74' => 'GL_EVENT_LOAD_ERROR_SYKNAPPS',
        '75' => 'GL_EVENT_INTERESTING_PROCESS_DETECTED_FINISH',
        '76' => 'GL_EVENT_HPP_SCAN_NOT_SUPPORTED_FOR_OS',
        '77' => 'GL_EVENT_HEUR_THREAT_NOW_KNOWN',
        );
        my %category_hash=(
        '1' => 'GL_CAT_INFECTION',
        '2' => 'GL_CAT_SUMMARY',
        '3' => 'GL_CAT_PATTERN',
        '4' => 'GL_CAT_SECURITY',
        );
        my %category_count_hash;
		# Look for the log files for this month
		opendir(UPLOAD,"$antivirus");
		my @dir_list=readdir(UPLOAD);
		close UPLOAD;	
		# Determine the most recent log
		my $most_recent=0;
		my $most_recent_file;
        my $most_recent_file_age;
 
		foreach my $file (@dir_list) {
			next if ($file eq ".");
			next if ($file eq "..");
			my $target="${antivirus}/${file}";
			my @file_info=stat($target);
			my $file_date=$file_info[9];			
			if ($file_date > $most_recent) {
				$most_recent=$file_date;
				$most_recent_file=$file;				
			}
 
            my $current_timeseconds=time();				
            my $file_age = (($current_timeseconds - $file_info[9]) / 86400);         
            $file_age = sprintf("%.2f", $file_age); 
            if ($file eq $most_recent_file) {
                $most_recent_file_age=$file_age;
            }
			if ($verbose) {
				LogMsg("Most recent file: $most_recent_file");
				LogMsg("Age: $most_recent_file_age");
			}		
            if ($file_age < 9) {
                # Check to see if the system was scanned
                open(INPUT,"${antivirus}/${file}");
                my @file_info=(<INPUT>);
                close INPUT;	
               
                foreach my $line (@file_info) {                
					if ($line =~ /Scan Complete/) {
						$scanned=1;
						my @scan_info=split(/\s+/,$line);
						for (my $x=0; $x<=$#scan_info; $x++){
							my $word=$scan_info[$x];
							if ($word =~ /Scanned:/) {
								$scanned_count=$scan_info[($x + 1)];
								$scanned_day=$file_age;
							}
						}
					}
                }   
            }			
            if ($file_age < 1) {
                # See what else we can get from this file if it was generated in the last day
                open(INPUT,"${antivirus}/${file}");
                my @file_info=(<INPUT>);
                close INPUT;	
               
                foreach my $line (@file_info) {                
                    my @tmp=split(/,/,$line);                 
                    my $date_field=$tmp[0];
                    my @date_tmp=split(//,$date_field);
                    my $year_field=$date_tmp[0].$date_tmp[1];
                    my $month_field=$date_tmp[2].$date_tmp[3];
                    my $day_field=$date_tmp[4].$date_tmp[5];
                    my $hour_field=$date_tmp[6].$date_tmp[7];
                    my $minute_field=$date_tmp[8].$date_tmp[9];                
                    my $year=(1970 + hex($year_field));
                    my $month=(1 + hex($month_field));
                    my $day=hex($day_field);
                    my $hour=hex($hour_field);
                    my $minute=hex($minute_field);
                    my $date="${year}-${month}-${day}";

                    # Get the event number
                    my $event_number=$tmp[1];
                    my $event=$event_hash{$event_number};                
                    my $cat_number=$tmp[2];
                    my $category=$category_hash{$cat_number};                
                    $category_count_hash{$cat_number}++;
					if ($verbose) {
						print "Date: $date Event: $event Category: $category \n";
						print "$line\n";
					}	
					# Create warnings for certain events
                    # Note:  These warnings are only see if they occurred in the past day since we are looking at the
                    # file that was updated in the last day.
					my @warning_event_list=("5","17","30","31","32","33","34","38","41","43","49","52","67","73","77");
					foreach my $w (@warning_event_list) {
						if ($event_number == $w) {
							LogMsg("WARNING: SAV Date: $date Event: $event Category: $category ");
                            LogMsg("ISSUE: Date $date Event $event Category $category ");
							LogMsg("WARNING: SAV $line");
							$warns++;
						}
					}					
                }   
            }	                        
		}		 
 	
 
		my @ltm=localtime($most_recent); 
		my $file_date=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);   
        if ($most_recent_file) { 
 		
			if ($verbose) {
				LogMsg(indent(1)."Most recent Symantec log is: $most_recent_file");
			}		
            if ($most_recent_file_age > 7) {
                LogMsg(indent(1)."WARNING: SAV Most recent file is $most_recent_file_age days old");
                $warns++;
            }
            open(INPUT,"${antivirus}/${most_recent_file}");
            my @file_info=(<INPUT>);
            close INPUT;	
            if ($verbose) {
                LogMsg("Reading antivirus log file $most_recent_file");
                foreach my $line (@file_info) {
                    print "$line\n";
                }
            }
            # Read the last line in the file.  I think that the last line is the most recent activity
            my $first_line=$file_info[-1];
            if ($first_line =~ /Symantec AntiVirus has determined that the virus definitions are missing on this computer/) {
                LogMsg(indent(1)."WARNING: SAV Symantec Antivirus definitions are missing on $file_date");
                $warns++;				
            } elsif ($first_line =~ /Symantec AntiVirus has detected a problem with the virus definitions on this computer/) {
                LogMsg(indent(1)."WARNING: SAV Symantec Antivirus has detected a problem with the virus definitions on $file_date");
                $warns++;				
            } elsif ($first_line =~ /Download of virus definition file from LiveUpdate server failed./) {
                LogMsg(indent(1)."NOTICE: Symantec Antivirus has detected a problem downloading the virus definitions on $file_date");
                #$warns++;
            }                  
        } else {
            LogMsg(indent(1)."WARNING: SAV Failed to find a most recent Symantec log file");
            $warns++;
        }
 		
        foreach my $cat_number (sort keys(%category_count_hash)) {
			# Category 2 is just a summary so we can ignore these
			next if ($cat_number == 2);
			# Category 3 is a pattern which may simply be a new virus def.
			next if ($cat_number == 3);
            my $msg="Found $category_count_hash{$cat_number} category $cat_number ($category_hash{$cat_number}) events in the Symantec Antivirus Log.";
            LogMsg(indent(1)."WARNING: SAV $msg");
            LogMsg(indent(1)."ISSUE: $msg");
			$warns++;
        }
		
		unless ($scanned) {	
			# Scans should occur on Sunday so report this on Monday
			 
			if ($day_of_week == 1) {
				LogMsg(indent(1)."WARNING: No scan found in the past week");
				$warns++;
			}
		} else {
			unless ($scanned_count) {
				LogMsg(indent(1)."WARNING: No scan count determined");
				$warns++;			
			} else {
				LogMsg(indent(1)."Scanned $scanned_count files for viruses $scanned_day days ago.");
			}
		}
		 		 
	} else {
		LogMsg(indent(1)."WARNING: SAV Failed to find $antivirus");
		$warns++;		
	}	
	
	return $warns;
}
sub fix_registry {
	# This function is to execute a registry fix.  Send this function a path to a .reg file and this function will run it with regedit.exe
	my $file=shift;
	my $cmd=`regedit.exe /s $file`;
	if ($?) {
		# Failed
		return 0;
	} else {  	
		# Succeeded
		return 1;
	}
}

sub fix_registry_bat {
	# This function is to execute a registry fix.  Send this function a path to a bat file which has the appropriate reg commands.
	my $file=shift;
	my $cmd=`$file`;
	if ($?) {
		# Failed
		return 0;
	} else {  	
		# Succeeded
		return 1;
	}
}

sub check_services {
	LogMsg("Checking services ...");	
    # Check that the services that typically run for the store's server are installed but are not running
    my @service_info=`sc query state= all`;
    my $service;
    foreach my $line (@service_info) {
        chomp($line);
        if ($line =~ /^SERVICE_NAME:/) {
            my @tmp=split(/:/,$line);
            $service=lc($tmp[1]);
            $service=~s/ //g;            
            foreach my $key (sort keys(%services_hash)) {
                # I am using the variable "name" to match the service so that I don't change the variable "key"
                my $name=lc($key);
                $name=~s/ //g;                
                if ("$service" eq "$name") {                    
                    $services_hash{$key}=1;
                }
            }            
        }
    }
    foreach $service (sort keys(%services_hash)) {
        if ($services_hash{$service}) {
            #LogMsg("Found service $service"); 
			LogMsg("System Info: Service $service: Present"); 
        } else {
			LogMsg("System Info: Service $service: Not_Found"); 
            #LogMsg("WARNING: Failed to find service $service installed");
            #$warning_count++;        
        }
    }  
	# Specifically check for Symantec
	$service="SepMasterService";
	
    @service_info=`sc query $service`;
    my $state=0;
    foreach my $line (@service_info) {
        chomp($line);	
		if ($line =~ /STATE/) {
			my @tmp=split(/\s+/,$line);
			$state=$tmp[-1];			
		}
	
	}	
	if ($state) {
		unless ($state eq "RUNNING") {
			LogMsg("WARNING State of $service is $state");
		}
	} else {
		LogMsg("WARNING State of $service is undetermined");
	}
 
}


sub check_running_services {
	# Check that all running services are expected
	LogMsg("Checking running services");
 
	my $warns=0;	
	my $notes=0;
	my @expected_service_list=(
		'SymantecEndpointProtection',			
		'AutomaticUpdates',
	 
	);		
	my %start_service_hash=(
		'wuauserv' => 'wuauserv',
		'AutomaticUpdates' => 'wuauserv',
		'SepMasterService' => 'SepMasterService',
		'SymantecEndpointProtection' => 'SepMasterService',
		'PrintSpooler' => 'Spooler',
 		
	);
	my %enable_service_hash = (
		'wuauserv' => 'wuauserv',
 
	);	  	
	if ($OS eq "win7") {
		push(@expected_service_list, "TightVNCServer");
		push(@expected_service_list, "WindowsUpdate");
		$start_service_hash{TightVNCServer} = 'tvnserver';
		$enable_service_hash{tvnserver} = 'tvnserver';
	}
    # Verify services are enabled
	foreach my $s (sort keys(%enable_service_hash)) {		
		if (check_enabled_service($s)) {	
			LogMsg(indent(1)."Service $s is enabled");
		} else {
			LogMsg(indent(1)."Service $s is not enabled");
			# Enable certain services			
			LogMsg(indent(1)."Enabling Service $s ...");
			my $rc=set_start($enable_service_hash{$s},"auto");  
			unless ($rc) {		
				LogMsg(indent(1)."WARNING: Failed to enable service $s - crs1");				 
				$warns++;		
			}	
		}
	}       		
	my $cmd="net start";
	my @service_list=`$cmd`;
 
	foreach my $s (@service_list) {
		chomp($s);
		
		$s=~ s/ //g;
		next if ($s =~ /Thecommandcompletedsuccessfully/);
		next unless $s;
	}
	# Check that expected services are running
	foreach my $expected (@expected_service_list) {
		
		my $found=0;
		foreach my $s (@service_list) {
			chomp($s);
			$s=~ s/ //g;
			next unless $s;
			if ($expected eq $s) {
				$found=1;
			}
		}
		if ($found) {
			if ($verbose) {
				LogMsg(indent(1)."Found $expected running");
			}			
		} else {			
			if ($start_service_hash{$expected}) { 			
				unless (start_service($start_service_hash{$expected})) {
					LogMsg("WARNING: Failed to start $expected - crs2");
					$warns++;
					LogMsg("WARNING: $expected is not running - crs3");
					$warns++;					
				} else {
					LogMsg("NOTICE: Started $expected service - crs4");
					$notes++;
				}	 				
			} else {
				LogMsg("WARNING: $expected is not running - crs5");
				$warns++;
			}
		}
	}
   
	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking running services");
		$warning_count++;
	} else {
		LogMsg("No issues found checking running services.");
	}

}

sub check_enabled_service {
    my $service=shift;
    my $setting=shift;
    LogMsg("Checking status of $service");
    my $cmd="sc qc \"$service\"";     
 
    my $start_type;
    my @info=`$cmd`;
    my $result=0;
    foreach my $i (@info) {        
        if ($i =~ /START_TYPE/) {			
			if ($i =~ /AUTO_START/) {
				$result=1;             
			} 
			if (defined($setting)) {
				if ($i =~ /$setting/) {
					$result=1;             
				}  			
			}
        }  
    }        
    return $result;
}

sub query_service {
    # Return 1 if the service is running or 0 otherwise
    my $service=shift;       
    LogMsg("Querying $service service");
    my $state="unknown";
    my $attempt=0;
    my $querycmd="sc query \"$service\"";
    
    my @service_info=`$querycmd`;                  
	foreach my $s (@service_info) { 
		chomp($s);            
		if ($s =~ /STATE/) {           
			my @tmp=split(/ /,$s);
			chomp($state=$tmp[$#tmp]);
			LogMsg( "current state: $state");
		}
	}   	
    if ($state eq "RUNNING") {        
        return 1;
    } else {        
        return 0;        
    }   
}

sub start_service {
    my $service=shift;       
    my $state="";
    my $attempt=0;
    my $querycmd="sc query \"$service\"";
    my $startcmd="sc start \"$service\"";
    my @service_info=`$querycmd`;   
	my $rc=0;
    until (("$state" eq "RUNNING") || ($attempt > 4)) {
        $attempt++;
        LogMsg("Checking $service...");
        @service_info=`$querycmd`;         

        foreach my $s (@service_info) { 
            chomp($s);            
            if ($s =~ /STATE/) {                     
                my @tmp=split(/ /,$s);
                chomp($state=$tmp[$#tmp]);
                LogMsg("current state: $state");
            }
        }

        if ("$state" eq "STOPPED") {
            LogMsg("Starting $service...");
            `$startcmd`;
        }    
		unless ("$state" eq "RUNNING") {
			my $sleep = ($attempt * 5);
			sleep $sleep;
		}
        
    }
    if ($state eq "RUNNING") {
        LogMsg("$service has been started");
		$rc=1;
    } else {
        LogMsg("Failed to start $service");
		$rc=0;
    }
	return $rc
} 

sub check_scripts {
	LogMsg("Checking scripts...");
 
}

sub check_tasks {
	# Tasks monitor will attempt to end
	my @kill_task_list=(
	   "GWX.exe",
	   "gwxux.exe",
	);
	my %valid_task_hash=(
		'wininit.exe' => '1',
		'lsm.exe' => '1',
		'rtkaudioservice.exe' => '1',
		'rthdvbg.exe' => '1',
		'rthdvbg.exe' => '1',
		'aertsrv.exe' => '1',
		'cdi.exe' => '1',
		'wisptis.exe' => '1',
		'wisptis.exe' => '1',
		'tabtip.exe' => '1',
		'taskhost.exe' => '1',
		'dwm.exe' => '1',
		'wmpnetwk.exe' => '1',
		'iastordatamgrsvc.exe' => '1',
		'lms.exe' => '1',
		'uns.exe' => '1',
		'inputpersonalization.exe' => '1',
		'conhost.exe' => '1',
		'trustedinstaller.exe' => '1',
		'audiodg.exe' => '1',
		'searchprotocolhost.exe' => '1',
		'searchfilterhost.exe' => '1',
		'dllhost.exe' => '1',
		'iastoricon.exe' => '1',
		'privacyiconclient.exe' => '1',
		'logonui.exe' => '1',
		'taskeng.exe' => '1',
		'system' => '1',
		'smss.exe' => '1',
		'csrss.exe' => '1',
		'csrss.exe' => '1',
		'winlogon.exe' => '1',
		'services.exe' => '1',
		'lsass.exe' => '1',
		'svchost.exe' => '1',
		'hpfsservice.exe' => '1',
		'hpfkcrypt.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'umvpfsrv.exe' => '1',
		'svchost.exe' => '1',
		'spoolsv.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'ptchangefilterservice.exe' => '1',
		'hpclientservices.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'ccsvchst.exe' => '1',
		'svchost.exe' => '1',
		'tvnserver.exe' => '1',
		'wlidsvc.exe' => '1',
		'hpqwmiex.exe' => '1',
		'svchost.exe' => '1',
		'searchindexer.exe' => '1',
		'wlidsvcm.exe' => '1',
		'wudfhost.exe' => '1',
		'ccsvchst.exe' => '1',
		'dpagent.exe' => '1',
		'explorer.exe' => '1',
		'hkcmd.exe' => '1',	 
		'tvnserver.exe' => '1',
		'svchost.exe' => '1',
		'presentationfontcache.exe' => '1',
		'hphc_service.exe' => '1',
		'svchost.exe' => '1',
		'svchost.exe' => '1',
		'cmd.exe' => '1',
		'tvnserver.exe' => '1',
		'perl.exe' => '1',
		'wmiprvse.exe' => '1',
		'tasklist.exe' => '1',	
		'sidebar.exe' => '1',	
		'ssscheduler.exe' => '1',	
		'iexplore.exe' => '1',	
		'brctrlcntr.exe' => '1',	
		'brccuxsys.exe' => '1',	 		
		'firefox.exe' => '1',	
		'osppsvc.exe' => '1',	
		'pdfsvc.exe' => '1',
		'hpsa_service.exe' => '1',
		'igfxtray.exe' => '1',
		'igfxpers.exe' => '1',
		'hpsysdrv.exe' => '1',
		'hpkeyboardx.exe' => '1',
		'adobearm.exe' => '1',
		'keystatus.exe' => '1',
		'splwow64.exe' => '1',
		'armsvc.exe' => '1',
		'pdfprofiltsrvpp.exe' => '1',
		'winvnc.exe' => '1',
		'igfxsrvc.exe' => '1',
		'xcopy.exe' => '1',
		'excel.exe' => '1',
		'chrome.exe' => '1',
		'tlntsvr.exe' => '1',
		'wusa.exe' => '1',
		'bubbles.scr' => '1',
		's10passwordvault.exe' => '1',
		'bubbles.scr' => '1',
		'itype.exe' => '1',
		'isuspm.exe' => '1',
		'pptd40nt.exe' => '1',
		'pdfpro5hook.exe' => '1',
		'powerpnt.exe' => '1',
		'mstsc.exe' => '1',
		'vssvc.exe' => '1',
		'wuauclt.exe' => '1',		
		'onenotem.exe' => '1',
		'winword.exe' => '1',
		'dphostw.exe' => '1',
		'hpdrvmntsvc.exe' => '1',
		'iviregmgr.exe' => '1',
		'smc.exe' => '1',
		'printisolationhost.exe' => '1',
		'unsecapp.exe' => '1',
		'agent.exe' => '1',
		'mspub.exe' => '1',
		'brstmonw.exe' => '1',
		'brynsvc.exe' => '1',
		'pcfxdial.exe' => '1',
		'brynsvc.exe' => '1',
		'scrnsave.scr' => '1',		
		'hookldr.exe' => '1',
		'jhi_service.exe' => '1',
		'googleupdate.exe' => '1',
		'brsvc01a.exe' => '1',
		'brss01a.exe' => '1',
		'dropbox.exe' => '1',
		'foxit' => '1',
		'foxitreader.exe' => '1',
		'rundll32.exe' => '1',
		'sppsvc.exe' => '1',		
		'nuancewds.exe' => '1',
		'xnview.exe' => '1',
		'photoscreensaver.scr' => '1',
		'hpwuschd2.exe' => '1',
		'sppsvc.exe' => '1', 
		'defrag.exe' => '1',
		'i_view32.exe' => '1',
		'msspellcheckingfacility.e' => '1',
		'hpnetworkcommunicator.exe' => '1',
		'flashutil10d.exe' => '1',
		'sstext3d.scr' => '1',
		'mstordb.exe' => '1',
		'axcrypt.exe' => '1',
		'e_s40rpb.exe' => '1',
		'mscorsvw.exe' => '1',
		'defrag.exe' => '1',
		'axcrypt.exe' => '1',
		'notepad.exe' => '1',
		
	);	

	if ($storenum == 7037) {
		$valid_task_hash{"isuspm.exe"} = '1';
		$valid_task_hash{"pptd40nt.exe"} = '1';
		$valid_task_hash{"pdfpro5hook.exe"} = '1';
		$valid_task_hash{"ati2evxx.exe"} = '1';
		$valid_task_hash{"mdm.exe"} = '1';
		$valid_task_hash{"alg.exe"} = '1'; 
		$valid_task_hash{"dropbox.exe"} = '1';
		$valid_task_hash{"e_fatiada.exe"} = '1';
		$valid_task_hash{"acrord32.exe"} = '1';
		$valid_task_hash{"seaport.exe"} = '1';
		$valid_task_hash{"rthdcpl.exe"} = '1';
		
		
	}
	if ($storenum == 5263) {
		$valid_task_hash{"twcu.exe"} = '1';		
	}
	if ($storenum == 2760) {
		$valid_task_hash{"xnview.exe"} = '1';		
		$valid_task_hash{"picasa3.exe"} = '1';
	}
	if ($storenum == 2760) {
		$valid_task_hash{"xnview.exe"} = '1';		
		$valid_task_hash{"picasa3.exe"} = '1';
	}	
	if ($storenum == 3979) {
		$valid_task_hash{"ipoint.exe"} = '1';		
		$valid_task_hash{"prevhost.exe"} = '1';
	}	
	if ($storenum == 3933) {
		$valid_task_hash{"hpsupportsolutionsframewo"} = '1';		
		$valid_task_hash{"hpwucli.exe"} = '1';
	}			
 	
	 
 	my $warns=0;
	# First, terminate any tasks on the kill list
    foreach my $p (@kill_task_list) {       
        my $pid=checkPID("$p");
        if ($pid) {
            LogMsg("Killing $p");
            LogMsg("PID is $pid");
            `taskkill /F /IM $p /T`;
            sleep 1;
            $pid=checkPID("$p");
            if ($pid) {
                LogMsg("Failed to stop $p");
            } else {
               LogMsg("Stopped $p");
            }
			if ($p =~ /gwx/i) {
				# Special handling for GWX
				$warns+=check_gwx();
			}
        }
    }  
 
 	
	# Check that all running tasks are expected
	LogMsg("Checking running tasks");

	my $cmd="tasklist";
	my @task_list=`$cmd`;
 	my $start_now=0;
	my $tvn_count=0;
	foreach my $t (@task_list) { 
		my @tmp=split(/\s+/,$t);
		my $task=lc($tmp[0]);			
		if ($task eq "tvnserver.exe") {
			$tvn_count++;			
		}
		next unless $task;
		if ($start_now) {
			next if ($task =~ /windows-kb/);	# Excuse Windows updates
			unless ($valid_task_hash{$task}) {				
				LogMsg("WARNING: $task is an unexpected task running - CT1");
				$warns++; 
			}		
		}
		if ($task eq "system") {
			$start_now=1;
		}		 
	}
  
	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking running tasks");
		$warning_count++;
	} else {
		LogMsg("No issues found checking running tasks.");
	}
}

sub checkPID {
	my $taskname = shift;
	my @process_list = `tasklist`;
	my $pid = 0;	
	foreach my $line (@process_list) {	
		if ($line =~ /$taskname/i) { 			
			# Trim off the leading 0's
			$line=~s/^\s//g;			
			# The PID should be the second element
			my @tmp=split(/\s+/,$line);
			$pid=$tmp[1];												
			$pid=~ s/ //g;						
		}
	}
	
	return $pid;
}

sub check_registry {
	LogMsg("Checking registry for various settings...");	
	my $warns=0;
	LogMsg("There have been $warns issues found checking the registry");
	if ($warns) {
		$warning_count++;
	}
 
}

sub check_pcAnywhere {
	if ($OS eq "win7") {
		return;
	}
	LogMsg("Checking pcAnywhere");	
	my $no_fix=shift;
	my $warns=0;
    # Check logging - We want logging on but not port scanning
    my $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\pcAnywhere\\CurrentVersion\\Logging\\NT Events\" ";    
    my @info=`$cmd`;  
    my $logging=1;
    foreach my $i (@info) { 
        chomp($i);
        if ($i =~ /UseLogging/) {
            # We prefer to have NT Logging on
            my @tmp=split(/\s+/,$i);
            if ($tmp[-1] eq "0x0") {
                LogMsg(indent(1)."WARNING: pcAnywhere NT Logging is turned off.");                  
                $warns++;				
                $logging=0;
            }               
        } 
        if ($logging) {
            # If NT Logging is on, check that we are not logging port scanning
            if ($i =~ /ConnPortScanned/) {
                my @tmp=split(/\s+/,$i);
                if ($tmp[-1] eq "0x1") {
                    LogMsg(indent(1)."WARNING: pcAnywhere ConnPortScanned is being logged.");                       
                    $warns++;					
                }               
            }
        }
         
    }
 
    # Check some registry settings
    LogMsg("Checking pcAnywhere settings.");

    $cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\pcAnywhere\\CurrentVersion\\Remote Control\" /s";    
    @info=`$cmd`;
	my $cleanup_needed=0;

    foreach my $i (@info) {        
        if ($i =~ /TCPIPRemoteNames/) {   
			$cleanup_needed++;				
        }
    }
	if ($cleanup_needed) {
		LogMsg("Need to cleanup TCP IP Remote Names for pcAnywhere");
		
		unless (-f $pcAnywhereTool) {
			LogMsg("WARNING: Failed to locate $pcAnywhereTool");
			$warns++;
		} else {
			# Run the tool which will remove the TCPIPRemoteNames entries
			`$pcAnywhereTool`;	
			# Check the registry again			
			@info=`$cmd`;
			foreach my $i (@info) {        
				if ($i =~ /TCPIPRemoteNames/) {
					LogMsg(indent(1)."WARNING: A TCPIPRemoteName is configured in pcAnywhere");	
					$warns++;
				}
			}
		}		
	} else {
		LogMsg(indent(1)."No TCPIPRemoteNames configured");
	}

	if ($warns) {
		LogMsg("There have been $warns issues found checking pcAnywhere");
		if ($warns) {
			$warning_count++;
		}
	}  
    LogMsg("Finished checking pcAnywhere settings.");    
}

sub check_for_update {
	# Check to see if there are files we need to copy from the server.  These will be scripts or registry updates in most cases.
	# Note: For stores with a Revised Network, see the function sync_workstation_utilities in the monitor.pl script. - kdg
	my $warns=0;
	my $update_count=0;	
    my $updating_self=0;	
	LogMsg("Checking for updated files...");
 
	my $target="/temp/scripts";
    my $temp_target_file="${target}/temp_monitor.pl";    
    my $monitor_target_file; 
    my $basename=basename($0);
    my $file_ok=0;	
	if (-d $update_source) {
		# We get a listing of the files available
		opendir(SOURCE,"$update_source");
		my @source_dir=readdir(SOURCE);
		close(SOURCE);
		foreach my $file (@source_dir) {
			next if ($file eq ".");
			next if ($file eq "..");
			next unless (($file =~ /.pl$/) || ($file =~ /.bat$/) || ($file =~ /.vbs$/) ||
			($file =~ /.reg$/)|| ($file =~ /.rdp$/)|| ($file =~ /.reg$/)|| ($file =~ /.exe$/));			
			my $source_file="${update_source}/${file}";
			my $target_file="${target}/${file}";
			if (-f "$source_file") {
				if ($verbose) {
					LogMsg("Found workstation file $file on Edna");
				}
				my @file_info=stat($source_file);
				my $file_date=$file_info[9];			
				if (-f "$target_file") {
					if ($verbose) {
						LogMsg("Found local file");
					}
					my @local_file_info=stat($target_file);
					my $local_file_date=$local_file_info[9];			
					if ($file_date > $local_file_date) {
                        # If the file is the wsmonitor.pl file itself, copy it to a temporary file						
                        if ($file eq "$basename") {               
							LogMsg("Found script to update self");
    						if (copy($source_file,$temp_target_file)) {
    							$update_count++;
                                $updating_self++;
                                $monitor_target_file=$target_file;                          
								LogMsg("Wrote to $temp_target_file");    						 
    						} else {
    							LogMsg("WARNING: Error attempting to create $temp_target_file");
    							$warns++;
    						}                        
                        } else {
    						if (copy($source_file,$target_file)) {
    							$update_count++;
    							if ($verbose) {
    								LogMsg("Updated $target_file");
    							}
    						} else {
    							LogMsg("WARNING: Error attempting to update $target_file");
    							$warns++;
    						}
                        }
					} else {
						if ($verbose) {
							LogMsg("$file is not newer");
						}
					}
				} else {	
					LogMsg("Failed to locate local file $target_file");
					if (copy($source_file,$target_file)) {
						LogMsg("Copied $source_file to $target_file");
					} else {
						LogMsg("Error attempting to copy $target_file");
						$warns++;
					}						
				}
			} else {
				LogMsg("WARNING: Failed to locate file on Edna");
				$warns++;	
			}			
		}
		$monitor_record_hash{'ftlus'}=0;	# Clear this if it is set at all
	} else {
		LogMsg("WARNING: Failed to locate $update_source");
		# The name of this failure: ftlus
		$monitor_record_hash{'ftlus'}++;
		LogMsg("This error has been seen $monitor_record_hash{'ftlus'} times.");
		if ($monitor_record_hash{'ftlus'} > 5) {
			# Set reboot request
			LogMsg("Setting reboot request...");
			send_reboot_request();				
		}
		
		$warns++;
	}
    if ($updating_self) {
        # Make certain that the new perl script will not fail miserably
        
        if (-f $temp_target_file) {
			LogMsg("Checking $temp_target_file...");
            my $cmd="perl -w -c $temp_target_file 2>&1";            
            my @info=`$cmd`;          
            foreach my $i (@info) {
                if ($i =~ /syntax OK/i) {                    
                    $file_ok=1;
                }                
            }             
        }    
        if ($file_ok) {
            # The new monitor script looks like it will run so replace the original
            if (copy($temp_target_file,$monitor_target_file)) { 
                LogMsg("Updating $monitor_target_file");
                if ($verbose) {
                    LogMsg("Wrote to $monitor_target_file");
                }
            } else {
                LogMsg("WARNING: Error attempting to create $monitor_target_file");
                $warns++;
            }            
        } else {
            LogMsg("WARNING: New monitor script has syntax issues");
            $warns++;
        }
    }	
	# Check for updated perl modules
	$warns+=check_perl_updates();	
	if ($revised_network) {	
		# Check for any other files 
		$warns+=check_misc_file_updates();	
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns errors checking for updates");
		$warning_count++;
	} else {
		LogMsg("Finished checking for updates: $update_count updates");
	}
}

sub check_perl_updates {
	my $warns=0;
	my $target_dir="C:/perl/site/lib/TTV";
	 	
	my %pm_hash=();
	if (-d $pm_source) {
		if (-d $target_dir) {
			# Read the source dir
			opendir(PM,$pm_source);
			my @dir_listing=readdir PM;
			close PM;
			foreach my $file (@dir_listing) {
				next if ($file eq ".");
				next if ($file eq "..");				
				my $full_path="${pm_source}/$file";
				 
				my @file_info = stat $full_path;
				# Note: In other systems we would have used md5 to get the sum but
				# md5 may not be on the workstation so I am using the age of the file
				# Find out how many days old this file is.  (One day = 86400 seconds).
				my $file_age = $file_info[9]; 															
 
				$pm_hash{$file}=$file_age;
			}
			# Read the target dir
			opendir(PM,$target_dir);
			my @target_dir_listing=readdir PM;
			close PM;			
			foreach my $source_file (keys(%pm_hash)) {
				my $file_found=0;			
				foreach my $file (@target_dir_listing) {
					next if ($file eq ".");
					next if ($file eq "..");			
					my $full_path="${target_dir}/$file";
					my @file_info = stat $full_path;
					# Note: In other systems we would have used md5 to get the sum but
					# md5 may not be on the workstation so I am using the age of the file
					# Find out how many days old this file is.  (One day = 86400 seconds).
					my $file_age = $file_info[9]; 	
 
					if ($source_file eq $file) {
						# The file exists so see if the file is the same
						unless($file_age eq $pm_hash{$source_file}) {
							LogMsg("$file needs to be updated in $target_dir");
						} else {
							if ($verbose) {
								LogMsg("$file is already current in $target_dir");
							}
							$file_found=1;
						}						
					}  				
				}	
				unless ($file_found) {
					LogMsg("Need to copy $source_file to $target_dir");
					my $full_path="${pm_source}/$source_file";
					my $full_target="${target_dir}/$source_file";
					unless ($debug_mode) {
						copy($full_path,$full_target);			
					}
					if ($verbose) {
						LogMsg("Copy $full_path to $full_target");
					}
				}
			}
		} else {
			LogMsg("WARNING: Could not locate $target_dir");
			$warns++;	
		}
	} else {
		LogMsg("WARNING: Could not locate $pm_source");
		$warns++;			
	}
	return $warns;
}

sub check_misc_file_updates {
	# For revised network stores, getting files from Edna via the workstation
	# is not possible.  Thus we have to do things rather indirectly by copying
	# files from the Edna to the share on the workstation and then using various
	# functions on the workstation to collect them.  This is a function to 
	# handle misc files not handled by the check_for_update or the check_perl_updates
	# functions.
 
	my $warns=0;
	if ($revised_network) {	
		LogMsg("Checking for misc files from Edna");
		my $source_dir="$share_temp_dir";
		my $target_dir="C:/temp";
		# Get a listing of the files in the source dir
		opendir(SOURCE,"$source_dir");
		my @source_list=readdir(SOURCE);
		close(SOURCE);
		foreach my $file (@source_list) {			
			next if ($file eq ".");
			next if ($file eq "..");
			my $source="${source_dir}/${file}";
			my $target="${target_dir}/${file}";
			if (-f $source) {
				LogMsg("Found $file to process");
				move($source,$target);
				if (-f $target) {
					if (-f $source) {
						LogMsg("Found $file still present in $source_dir after attempted move");
						$warns++;
					}
					if (-f $target) {
						LogMsg("Found $file in $target_dir after move.");
					}
				}
			} 
		} 				
	} else {
		return 0;
	}
	return $warns;
}

sub check_for_update_orig {
	# Check to see if there is a new version of this script available
	my $warns=0;
	
	my $source_file="${update_source}/wsmonitor.pl";
	my $target_file="/temp/scripts/wsmonitor.pl";
	if (-f "$source_file") {
		LogMsg("Found workstation script on Edna");
		my @file_info=stat($source_file);
		my $file_date=$file_info[9];			
		if (-f "$target_file") {
			LogMsg("Found local script");
			my @local_file_info=stat($target_file);
			my $local_file_date=$local_file_info[9];			
			if ($file_date > $local_file_date) {
				LogMsg("There is a newer script available");
				if (copy($source_file,$target_file)) {
					LogMsg("Updated $target_file");
				} else {
					LogMsg("Error attempting to update $target_file");
					$warns++;
				}
			} else {
				LogMsg("No newer script found");
			}
		} else {	
			LogMsg("Failed to locate local file $target_file");
			if (copy($source_file,$target_file)) {
				LogMsg("Copied $target_file to $target_file");
			} else {
				LogMsg("Error attempting to copy $target_file");
				$warns++;
			}						
		}
	} else {
		LogMsg("WARNING: Failed to locate file on Edna");
		$warns++;	
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns errors checking for updates");
		$warning_count++;
	}
}
 
sub save_log {
	LogMsg("Saving Log...");
	# Save this log on the Edna server
	my $warns=0;	
	my $target_dir=$update_source;
	my $target_file="${target_dir}/${hostname}.log";
	my $local_target_file="/temp/scripts/last_monitor.log";	
	my @tmp=split(/\./,$0);
	
	my $logfile=$tmp[0];
	$logfile="${logfile}.log";			# The name of the log file for this script (with the path).
	my $basename=basename($logfile);	# The name of the logfile without the path.
	if (-f $local_target_file) {
		unlink $local_target_file;
	}
	if (copy($logfile,$local_target_file)) {
		if (-f "$local_target_file") {
			LogMsg("Successfully saved a copy locally.");
	 
		} else {
			LogMsg("Did not find $local_target_file");
			LogMsg("WARNING: Failed to create a local copy");
			$warns++;
		}
	} else {		
		LogMsg("WARNING: Failed to create a local copy");
		$warns++;
	}		
	# Save the results under My Documents
	LogMsg("Creating MyDoc Log");
	my $my_doc_dir="C:/Users/Villages/Documents";
	if ($OS eq "XP") {
		$my_doc_dir="C:/Documents and Settings/Villages/My Documents";
	}
	my $my_doc_dir_alt="c:/temp";
	unless (-d $my_doc_dir) {
		$my_doc_dir=$my_doc_dir_alt;
	}
	if (-d $my_doc_dir) {
		my $mydoc_log="${my_doc_dir}/monitor_results.txt";
		open(MYDOC,">$mydoc_log");
		print MYDOC "RESULTS: $warning_count issues found on $hostname\n";
		close MYDOC;
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $my_doc_dir");
		$warns++;
	}
	
	if (-d "$target_dir") {	
		LogMsg(indent(1)."Found $target_dir");
		if (-f $logfile) {			
			if (copy($logfile,$target_file)) {
				if (-f "$target_file") {
					LogMsg("Successfully copied log to $target_dir");
					unlink $logfile;
				} else {
					LogMsg("Did not find $target_file");
					LogMsg("NOTICE: Failed to copy log file to $target_dir");
					#$warns++;
				}
			} else {		
				LogMsg("NOTICE: Failed to copy log file to $target_dir");
				#$warns++;
			}
		}
	} else {
		LogMsg("NOTICE: Failed to locate $target_dir");
		#$warns++;
	}

	
	if ($warns) {
		LogMsg("WARNING: Found $warns errors saving log file");
		$warning_count++;
	}	
}

sub gather_sysinfo {
 
	
	my $update_log="c:/WINDOWS/WindowsUpdate.log";
	
	# Check the update log
    if (-e $update_log) {
        open(INPUT, "$update_log") ;
        my @loginfo=(<INPUT>);
        close INPUT;
		my %systemInfo=();
        foreach my $line (@loginfo) {
            # Find the computer brand
            my @tmp=();
            if ($line =~ /Computer Brand/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
                $systemInfo{Brand}=$value;
            }
            # Find the computer model
            if ($line =~ /Computer Model/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
                $systemInfo{Model}=$value;
            }
            # Find the BIOS Rev
            if ($line =~ /Bios Revision/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
                $systemInfo{BIOSRev}=$value;
            }            
            # Find the BIOS Name
            if ($line =~ /Bios Name/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
                $systemInfo{BIOSName}=$value;
            } 
            # Find the BIOS Release Date
            if ($line =~ /Bios Release Date/) {
                @tmp=split(/\=/,$line);
				chomp(my $value=$tmp[1]);
                $systemInfo{BIOSReleaseDate}=$value;
            }                         
        }
		foreach my $key(keys(%systemInfo)) {
			LogMsg("System Info: $key: $systemInfo{$key}"); 
		}
	} else {
		LogMsg("$update_log not found");
	}
	LogMsg("System Info: hostname: $hostname"); 
	# Get the system serial number
	my $cmd="wmic bios get serialnumber";
	my @info=`$cmd`;
	my $serialnumber=0;
	foreach my $i (@info) {
		chomp($i);
		next if ($i =~ /SerialNumber/i);
		next unless ($i);
		$serialnumber=$i;			
	}
	if ($serialnumber) {
		LogMsg("System Info: SerialNumber: $serialnumber"); 
	}
	# Collect the Mac Address
	my @ipconfig_info=`ipconfig /all`;
	my $MAC=0;
	foreach my $i (@ipconfig_info) {
		if ($i =~ /physical address/i) {
			unless ($i =~ /00-00-00-00/) {
				my @tmp=split(/\s+/,$i);
				$MAC=$tmp[-1];
				LogMsg("System Info: MAC: $MAC");			 
			}
		}
	}		  
	# Collect Network Card Info
 
	$cmd="wmic nic list /format:list";
	@info=`$cmd`;
	my $nic_driver='';
 
	my $found_target1=0;
	my $found_target2=0;
	my $manufacturer;
	foreach my $i (@info) {
		#chop($i);
		if ($i =~ /AdapterType=Ethernet 802.3/) {
			$found_target1=1;						
		}
		if ($i =~ /AdapterType/i) {
			unless ($i =~ /AdapterType=Ethernet 802.3/) {
				$found_target1=0;	
				$found_target2=0;									
			}
		}
		if ($found_target1) {
			if ($i =~ /Manufacturer/i) {				
				my @tmp=split(/=/,$i);
				$manufacturer=($tmp[1]);				
				if ($manufacturer) {
					if ($manufacturer =~ /microsoft/i) {
						$found_target2=0;		
					} elsif  ($manufacturer =~ /Symantec/i) {
						$found_target2=0;							
					} else {
						$found_target2=1;						 
					}					
				} else {
					$found_target2=0;					 
				}
			}
		}
		if ($found_target2) {
			if ($i =~ /^ProductName/) {		 
				my @tmp=split(/=/,$i);
				$nic_driver=($tmp[1]);				 				
			}
			if ($i =~ /ProductName=Packet Scheduler Miniport/) {
				$nic_driver='';			 
			}		
		}  		
	}
	if ($nic_driver) {
		chomp($nic_driver);
		chop($nic_driver);		
		LogMsg("System Info: NIC: $nic_driver"); 
	}	
	###	
}

 
sub check_windows_updates {
	LogMsg("Checking Windows Updates");
	my $winuplog="c:/Windows/WindowsUpdate.log";
	my $update_count=0;
	my $warns=0;
 
	if (-f $winuplog) {		
		my @file_info=stat($winuplog);
		my $current_timeseconds=time();				
		my $file_age = (($current_timeseconds - $file_info[9]) / 86400);         
		$file_age = sprintf("%.2f", $file_age); 
		if ($file_age > 7) {		
			# Second check - It seems that checking this age is not always accurate.  So, next we open
			# the log file and read the last date.
			open(LOG,"$winuplog");
			my @winlog_info=(<LOG>);
			close LOG;
			my $last_line=$winlog_info[-1];
			my @tmp=split(/\s+/,$last_line);
			my $date=$tmp[0];
			@tmp=split(/-/,$date);
			my $uyear=$tmp[0];
			my $umonth=$tmp[1];
			my $uday=$tmp[2];
			unless (($uyear == $current_year) && ($umonth == $current_month) && ($uday == $current_day)) {
				my $utime = timelocal(0,0,0,$uday,$umonth,$uyear);				
				my $uage = (($current_timeseconds - $utime) / 86400);
				if ($uage > 7) {				 
					LogMsg(indent(1)."WARNING: Update log is $uage days old!");
					$warns++;				
				} else {				
					LogMsg(indent(1)."Update log is $uage days old");
				}
			} else {
				LogMsg(indent(1)."Update log was updated on $date");
			}			
		} else {		
			LogMsg(indent(1)."Update log is $file_age days old");
		}
		$update_count=get_update_count($winuplog);
		unless ($update_count) {
			# Wait a bit and try again
			sleep 10;
			$update_count=get_update_count($winuplog,"retry");					
		}		
		LogMsg(indent(1)."Found $update_count Windows Updates");
		unless ($update_count) {
			if ($file_age > 7) {
				LogMsg(indent(1)."WARNING: Failed to find an update count");
				$warns++;		
			}
		}
		$warns+=check_gwx();
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $winuplog");
		$warns++;
	}
	
	if (($day_of_week == 1) || ($force)) {	
		# This probably does not need to be run every day.
		# It is a bit of a slow process 
		# Find what updates are pending
		LogMsg(indent(1)."Find pending updates...");
		my $tool="c:/temp/scripts/WUA_Search.vbs";
		if (-f $tool) {
			my $cmd="cscript $tool";
			my @update_info=`$cmd`;
			my $trigger = "Searching";
			my $trigger_found=0;
			my $found_KB2901983=0;
			my $found_KB3035583=0;
			foreach my $u (@update_info) {
				chomp($u);
				if ($trigger_found) {
					LogMsg("$u");
					if ($u =~ /KB2901983/) {
						$found_KB2901983=1;
					}
					if ($u =~ /KB3035583/) {
						$found_KB3035583=1;
					}					
				}
				if ($u =~ /$trigger/) {
					$trigger_found=1;
				}			
			}
			if ($found_KB2901983) {
				LogMsg("WARNING: Found KB2901983 waiting to be installed");
				$warns++;
			}
			if ($found_KB3035583) {				 
				LogMsg("WARNING: Found KB3035583 (GWX) waiting to be installed");
				$warns++;
			}			
		} else {
			LogMsg("WARNING: Failed to locate $tool");
			$warns++;
		}
		
	}
	
 	if ($warns) {
		LogMsg("WARNING: There were $warns issues found checking Windows Updates");
		$warning_count++
	} else {
		LogMsg("No issues found checking Windows Updates.");
	}		
	 
}

sub get_update_count {
	my $winuplog=shift;
	my $retry=shift;
	my $update_count=0;
	my %warning_hash;
	my $yesterday_successful_updates=0;
	my $today_successful_updates=0;
	my $warns=0;
 
	if (-f $winuplog) {
		# Read the log and determine if windows updates are happening
		open(LOG,"$winuplog");
		my @win_update_info=(<LOG>);
		close LOG;
		my $line_counter=0;
		foreach my $line (@win_update_info) {
			chomp($line);
			$line_counter++;
			my @tmp=split(/\s+/,$line);
			my $date=$tmp[0];
			@tmp=split(/-/,$date);
			my $uyear=$tmp[0];
			my $umonth=$tmp[1];
			my $uday=$tmp[2];
			if ($umonth =~ /^0/) {
				$umonth =~ s/0//;
			}
			if ($uday =~ /^0/) {
				$uday =~ s/0//;
			}			
			if ($line =~ /WARNING/) {
				my $excused=0;
 
				if ($line =~ /error = 0x00000000/) {
					$excused=1;				
				}
				if ($line =~ /AU found no suitable session to launch client/) {
					$excused=1;				
				}				
				
				unless ($excused) {
					if ((($uyear eq $current_year) && ($umonth eq $current_month) && ($uday eq $current_day)) ||
					(($uyear eq $previous_day_year) && ($umonth eq $previous_day_month) && ($uday eq $previous_day_day))) {
						my @line_tmp=split(/AU/,$line);
						my $warning=$line_tmp[1];
						if ($warning) {
							$warning_hash{$warning}++;
							if ($verbose) {
								print "Line: $line_counter\n";								
							}
						}
					}
				}
			}
			if ($line =~ /Install call completed.*reboot.*error = 0x00000000/) {
				if (($uyear eq $current_year) && ($umonth eq $current_month) && ($uday eq $current_day)) {
					$today_successful_updates++;
				}
				if (($uyear eq $previous_day_year) && ($umonth eq $previous_day_month) && ($uday eq $previous_day_day)) {
					$yesterday_successful_updates++;
				}				
			}
			if ($line =~ /updates detected/) {
				if ($line =~ /$date_label/) {					
					$update_count++;
				}
				if ($line =~ /$previous_date_label/) {					
					$update_count++;
				}
				if ($line =~ /$previous_previous_date_label/) {					
					$update_count++;
				}				
			}
 
		}	
	
		foreach my $warning (sort keys(%warning_hash)) {
			my $count=$warning_hash{$warning};
			LogMsg(indent(1)."WARNING: Found $count $warning messages");
			$warns++;
		}
	} else {
		LogMsg("WARNING: Failed to locate $winuplog");
		$warning_count++;
	}
	LogMsg("There were $yesterday_successful_updates successful updates yesterday");
	LogMsg("There were $today_successful_updates successful updates today");
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking Windows Updates for today and yesterday");
		$warning_count++;
	}
	return $update_count;
	
}

sub check_firewall {
	if ($hostname eq "4201-Cumming") {
		return;
	}
    my $warns=0;
    my $key="";
    my $start_reading=0;
	my $sep_fw_found=0;
	my $fwrunning_status=0;
 
    my %enabled_items=(      
        'Remote Desktop'=>'0',
        'Exception mode'=>'0',
        'response mode'=>'0',
        'Notification mode'=>'0',
    );
	if ($hostname eq "BO5Hallway") {
		# This is a publicly available system at Ephrata
		%enabled_items=(
			'Exception mode'=>'0',
			'response mode'=>'0',
			'Notification mode'=>'0',	 	 
		);	
	}
    
    LogMsg("Checking Firewall $arch");
 
	# Check to see if SEP12 is installed with a firewall
    my $cmd="reg query \"HKLM\\SOFTWARE\\Wow6432Node\\Symantec\\Symantec Endpoint Protection\\CurrentVersion\\public-opstate\" /s";    
	if ($arch eq "32-bit") {
		$cmd="reg query \"HKLM\\SOFTWARE\\Symantec\\Symantec Endpoint Protection\\CurrentVersion\\public-opstate\" /s";    
	}
    my @info=`$cmd`;	
	foreach my $i (@info) {
		chomp($i);	
		if ($i =~ /FWRunningStatus/) {
			my @tmp=split(/\s+/,$i);
			$sep_fw_found=1;
			$fwrunning_status=$tmp[-1];			
			
		}		
	}	
 
	if ($sep_fw_found) {
		LogMsg("Found Symantec Endpoint Protection Firewall");
		if ($fwrunning_status eq "0x1") {
			LogMsg("SEP Firewall is running");
		} else {
			LogMsg("WARNING: SEP Firewall is NOT running - Status ($fwrunning_status)");
			$warns++;
		}
	} else { 			
		my @firewall_info=`netsh firewall show config`;
		foreach my $f (@firewall_info) {
			if ($f =~ /Standard profile configuration/) {
				$start_reading=1;
			}
		  
			if ($start_reading) {            
				if ($f =~ /Operational mode/) {
					if ($f =~ /Disable/) {
						LogMsg(indent(1)."WARNING: Firewall may be disabled");
						$warns++;	
					}
				}
				if ($f =~ /Enable/) {
					foreach $key (keys(%enabled_items)) {
						if ($f =~ /$key/i) {
							$enabled_items{$key}=1;
							if ($verbose) {
								print "$f\n";
							}
						}
					}
				}
			}        
		}
		# Check that all of the enabled items were found to be enabled
		foreach $key (keys(%enabled_items)) {      
			unless ($enabled_items{$key}) {
				LogMsg(indent(1)."WARNING: item $key may be disabled");
				$warns++;	        
			}
		}
	}
  
	if ($warns) {	        
        LogMsg(indent(1)."WARNING: $warns Firewall issues found");
      
        $warning_count++;
	}  else {
       LogMsg(indent(1)."No Firewall issues found");
      
    }
    
}

sub check_weekly_task {
	if  ($OS eq "XP") {
		# This function does not work on this system
		return;			
	}	
	if ($hostname =~ /BO5Hallway/i)   {
		# Don't run at Ephrata for the time being.
		return;
	}
	LogMsg("Checking Weekly Task...");
	unless ($day_of_week == 0) {
		# Run only on Sunday
		#return;
	}	 		
	create_weekly_script();	# We call this function each time this is run.  This permits us to update the script by simply editing
							# the create_weekly_script function.
  
	# Expecting a task called weekly_task
	my $schtaskcmd="schtasks /query /fo list /v";
	my @schtask_info=`$schtaskcmd`;
	my $found_target=0;
	my $found_task=0;
	my $task;
	my $status;
	my $warns=0;
	foreach my $s (@schtask_info) {
		chomp($s);		
		if ($found_target) {
			if ($s =~ /Next Run/) {				
				my @tmp=split(/\s+/,$s);
				my $date=$tmp[3];	
				if ($OS eq "XP") {
					$date=$tmp[4];
				}						
				LogMsg(indent(1)."Task: $task Next Run: $date");
			}
			if ($s =~ /Status/i) {		
				
				my @tmp=split(/\s+/,$s);
				shift @tmp;				
				my $status = join(' ',@tmp);
				unless ($status) {
					$status = "unknown";
					if ($OS eq "XP") {
						# In XP, no status seems to be ok.
						$status = "Ready";
					}
				}				
				unless (($status =~ /Ready/i) || ($status =~ /RUNNING/i)){
					LogMsg(indent(1)."WARNING: Status for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}				
			}	
			if ($s =~ /Last Run/) {					
				#LogMsg(indent(1)."$s");				
				my @tmp=split(/\s+/,$s);								
				my $date=$tmp[3];
				if ($OS eq "XP") {
					$date=$tmp[4];
				}	
				if ($date eq "N/A") {
					LogMsg(indent(1)."WARNING: $task has never run");
				
					$warns++;				
				} else {
					@tmp=split(/\//,$date);												
					my $day=$tmp[1];
					my $month=$tmp[0];
					my $local_time_month=$month;
					$local_time_month--;
					my $year=$tmp[2];	
					if ($verbose) {
						print "Date: ($date) day ($day) month ($month) year ($year)\n";
					}
					my $lastrun = timelocal(0,0,0,$day,$local_time_month,$year);
					my $current_timeseconds=time();						
					my $last_run_age = (($current_timeseconds - $lastrun) / 86400);
					if ($last_run_age > 7) {
						LogMsg(indent(1)."WARNING: $task has not run since $year-$month-$day");
						$warns++;
					} else {
						LogMsg(indent(1)."Task: $task Last Run: $date");
					}	
				}
			}			
			if ($s =~ /Scheduled Task State: /i) {				
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];				
				unless ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}					
			}						
		}
		
		if (($s =~ /weekly_task/i) && ($s =~ /TaskName:/)) {
			$found_target=1;						
			my @tmp=split(/\\/,$s);
			$task=$tmp[1];
			if ($OS eq "XP"){
				@tmp=split(/:/,$s);				
				$task=$tmp[1];				
				$task =~ s/\s//g;				
			}			
					
			LogMsg("Found task: $task");			
			$found_task=1;
		}
		if ($s =~ /Hostname/i) {
			$found_target=0;			
		}
	}
	unless ($found_task) {	 
		set_weekly_task();		
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking weekly tasks");
		$warning_count++;
	} else {
		LogMsg("Found no issues checking weekly task");
	}
}
sub check_data_backup {
	# This function makes certain that there is a batch file created
	# to handle data backups to Edna.  It will also schedule the 
	# running of the backup script and verify that it ran.
	if (($hostname eq "4201-Cumming") && ($OS eq "XP")) {
		# This function does not work on this system
		return;			
	}	
 
	my $is_ephrata=0;
	if (($hostname =~ /BO5Hallway/i) || ($hostname =~ /^EPH/i) || ($hostname =~ /operationsmgr/i)) {
 
		$is_ephrata=1;
		# Don't run at Ephrata for the time being except on Liz's system
		unless (($hostname =~ /eph-jdirks/i) || ($hostname =~ /operationsmgr/i) ||($hostname =~ /eph-gm/i)) {
			return;
		}
	}
 
	if ($storenum == 7037) {
		$is_ephrata=1;
	}
 
	LogMsg("Checking Data Backup...");
 
 
	unless (-f $data_backup_script) {	
		if ($is_ephrata) {
			create_data_backup_script_ephrata();
		} else {
			create_data_backup_script();
		}
	}		
	# Expecting a task called data_backup
	my $schtaskcmd="schtasks /query /fo list /v";
	my @schtask_info=`$schtaskcmd`;
	my $found_target=0;
	my $found_data_backup=0;
	my $task;
	my $status;
	my $warns=0;
	foreach my $s (@schtask_info) {
		chomp($s);		
		if ($found_target) {
			if ($s =~ /Next Run/) {				
				my @tmp=split(/\s+/,$s);
				my $date=$tmp[3];	
				if ($OS eq "XP") {
					$date=$tmp[4];
				}						
				LogMsg(indent(1)."Task: $task Next Run: $date");
			}
			if ($s =~ /Status/i) {		
				
				my @tmp=split(/\s+/,$s);
				shift @tmp;				
				my $status = join(' ',@tmp);
				unless ($status) {
					$status = "unknown";
					if ($OS eq "XP") {
						# In XP, no status seems to be ok.
						$status = "Ready";
					}
				}				
				unless (($status =~ /Ready/i) || ($status =~ /RUNNING/i)){
					LogMsg(indent(1)."WARNING: Status for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}				
			}	
			if ($s =~ /Last Run/) {					
				#LogMsg(indent(1)."$s");				
				my @tmp=split(/\s+/,$s);								
				my $date=$tmp[3];
				if ($OS eq "XP") {
					$date=$tmp[4];
				}	
				if ($date eq "N/A") {
					LogMsg(indent(1)."WARNING: $task has never run");
					$warns++;				
				} else {
					@tmp=split(/\//,$date);												
					my $day=$tmp[1];
					my $month=$tmp[0];
					my $local_time_month=$month;
					$local_time_month--;
					my $year=$tmp[2];	
					if ($verbose) {
						print "Date: ($date) day ($day) month ($month) year ($year)\n";
					}
					my $lastrun = timelocal(0,0,0,$day,$local_time_month,$year);
					my $current_timeseconds=time();						
					my $last_run_age = (($current_timeseconds - $lastrun) / 86400);
					if ($last_run_age > 7) {
						LogMsg(indent(1)."WARNING: $task has not run since $year-$month-$day");
						$warns++;
					} else {
						LogMsg(indent(1)."Task: $task Last Run: $date");
					}	
				}
			}			
			if ($s =~ /Scheduled Task State: /i) {				
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];				
				unless ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}					
			}						
		}
		
		if (($s =~ /data_backup/i) && ($s =~ /TaskName:/)) {
			$found_target=1;						
			my @tmp=split(/\\/,$s);
			$task=$tmp[1];
			if ($OS eq "XP"){
				@tmp=split(/:/,$s);				
				$task=$tmp[1];				
				$task =~ s/\s//g;				
			}			
					
			LogMsg("Found task: $task");			
			$found_data_backup=1;
		}
		if ($s =~ /Hostname/i) {
			$found_target=0;			
		}
	}
	unless ($found_data_backup) {
	
		set_task();		
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking Data Backup");
		$warning_count++;
	} else {
		LogMsg("Found no issues checking Data Backup");
		$data_backup_set=1;
	}
}
  
sub check_syncback {
	if (($hostname eq "4201-Cumming") && ($OS eq "XP")) {
		# This function does not work on this system
		return;			
	}	
	if ($hostname eq "BO5Hallway") {
		return;
	}
	unless ($day_of_week == 4) {
		# Run only on Thursday
		return;
	}	
	LogMsg("Checking SyncBack...");
	# Some workstations are setup with an app called Syncback.  This checks that task
	# UPDATE: This task is being disabled - 2014-01-16 - kdg
	my $schtaskcmd="schtasks /query /fo list /v";
	my @schtask_info=`$schtaskcmd`;
	my $found_target=0;
	my $found_syncback=0;
	my $task;
	my $status;
	my $warns=0;
	my $remove_syncback=0;
	my @task_list;
	my @syncback_task_list;
	my $next_run='';
	foreach my $s (@schtask_info) {
		chomp($s);						
		if ($found_target) {		
			if ($s =~ /Next Run/) {				
				my @tmp=split(/\s+/,$s);
				my $date=$tmp[3];	
				if ($OS eq "XP") {
					$date=$tmp[4];
				}						
				LogMsg(indent(1)."Task: $task Next Run: $date");				
				if ($date eq "Disabled") {				
					$next_run=$date;					
				}
			}
			if ($s =~ /Status/i) {						
				my @tmp=split(/\s+/,$s);
				shift @tmp;				
				my $status = join(' ',@tmp);
				unless ($status) {
					$status = "unknown";
					if ($OS eq "XP") {
						# In XP, no status seems to be ok.
						$status = "Ready";
					}
				}				
				unless ($status =~ /Ready/i) {
					LogMsg(indent(1)."Status for $task is $status");
					#LogMsg(indent(1)."WARNING: Status for $task is $status");
					$remove_syncback=1;
					#$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}				
			}	
			if ($s =~ /Last Run/) {			
				#LogMsg(indent(1)."$s");				
				my @tmp=split(/\s+/,$s);								
				my $date=$tmp[3];
				if ($OS eq "XP") {
					$date=$tmp[4];
				}	
				if ($date eq "N/A") {
					LogMsg(indent(1)."WARNING: $task has never run");
					$remove_syncback=1;
					$warns++;				
				} else {				
					@tmp=split(/\//,$date);												
					my $day=$tmp[1];
					my $month=$tmp[0];
					my $local_time_month=$month;
					$local_time_month--;
					my $year=$tmp[2];	
					if ($verbose) {
						print "Date: ($date) day ($day) month ($month) year ($year)\n";
					}
					my $lastrun = timelocal(0,0,0,$day,$local_time_month,$year);
					my $current_timeseconds=time();						
					my $last_run_age = (($current_timeseconds - $lastrun) / 86400);
					if ($last_run_age > 7) {
						unless ($next_run eq "Disabled") {							
							LogMsg(indent(1)."WARNING: $task has not run since $year-$month-$day");
							$remove_syncback=1;
							$warns++;
						}
					} else {
						LogMsg(indent(1)."Task: $task Last Run: $date");
					}	
				}
			}			
			if ($s =~ /Scheduled Task State: /i) {				
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];				
				if ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$remove_syncback=1;
					$warns++;
					push(@task_list,$task);	
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
					
				}					
			}						
		}
		
		if (($s =~ /SyncBack/i) && ($s =~ /TaskName:/)) {
			$found_target=1;	
			my @tmp=split(/\\/,$s);
			$task=$tmp[1];
			if ($OS eq "XP"){
				@tmp=split(/:/,$s);				
				$task=$tmp[1];				
				$task =~ s/\s//g;	
				push(@syncback_task_list,$task);
			}								
			LogMsg("Found task: $task");						
			$found_syncback=1;
		}
		if ($s =~ /Hostname/i) {
			$found_target=0;	
			$next_run='';			
		}
	}
	unless ($found_syncback) {
		#LogMsg("ISSUE: Did not find any SyncBack tasks");
		#$warns++;
	} else {
		foreach my $t (@task_list) {			
			LogMsg("Disabling task $t");
			my $cmd="schtasks /change /disable /tn \"$t\" ";					
			system($cmd);
			if ($?) {
				LogMsg("Received an error attempting to disable $t");
				LogMsg("Deleting it instead...");
				$cmd="schtasks /delete /tn \"$t\" /f";
				system($cmd);
				if ($?) {
					LogMsg("WARNING: Received an error attempting to delete $t"); 
					$warns++;
				} else {
					LogMsg("Successfully deleted $t");
				}
			} else {
				LogMsg("Successfully disabled $t");
			}
		}
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking SyncBack");
		$warning_count++;
		if ($remove_syncback) {
			foreach my $t (@syncback_task_list) {
				my $cmd="schtasks /delete /tn \"$t\" /f";
				# Note - not executing this command just yet - kdg
			}
			
		}
	} else {
		LogMsg("Found no issues checking SyncBack");
	}
}

 
sub check_scheduled_tasks {
	if (($hostname eq "4201-Cumming") && ($OS eq "XP")) {
		# This function does not work on this system
		return;			
	}	
	if ($hostname eq "BO5Hallway") {
		return;
	}
	unless (($day_of_week == 4) || ($schedule_only)) {
		# Run only on Thursday
		return;
	}	
	LogMsg("Checking Scheduled Tasks...");
	# Sometimes "AT" tasks get added but never get completed. This checks that task
	# This function also disables the task associated with updating to Windows 10.
	my $schtaskcmd="schtasks /query /fo list /v";
	my @schtask_info=`$schtaskcmd`;
	my $found_target=0;
	my $found_target2=0;
	my $found_at_task=0;
	my $found_gwx_task=0;
	my $task;
	my $status;
	my $warns=0;
	my @at_list;
	foreach my $s (@schtask_info) {
		chomp($s);		
		if ($found_target) {
			if ($s =~ /Next Run/) {				
				my @tmp=split(/\s+/,$s);
				my $date=$tmp[3];	
				if ($OS eq "XP") {
					$date=$tmp[4];
				}						
				LogMsg(indent(1)."Task: $task Next Run: $date");
				if ($date eq "N/A") {
					LogMsg(indent(2)."Will purge $task");
					push(@at_list,$task);
				}
			}
			if ($s =~ /Status/i) {		
				
				my @tmp=split(/\s+/,$s);
				shift @tmp;				
				my $status = join(' ',@tmp);
				unless ($status) {
					$status = "unknown";
					if ($OS eq "XP") {
						# In XP, no status seems to be ok.
						$status = "Ready";
					}
				}				
				unless ($status =~ /Ready/i) {
					LogMsg(indent(1)."WARNING: Status for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}				
			}	
			if ($s =~ /Last Run/) {					
				#LogMsg(indent(1)."$s");				
				my @tmp=split(/\s+/,$s);								
				my $date=$tmp[3];
				if ($OS eq "XP") {
					$date=$tmp[4];
				}	
				if ($date eq "N/A") {
					LogMsg(indent(1)."WARNING: $task has never run");
					$warns++;				
				} else {
					@tmp=split(/\//,$date);												
					my $day=$tmp[1];
					my $month=$tmp[0];
					my $local_time_month=$month;
					$local_time_month--;
					my $year=$tmp[2];	
					if ($verbose) {
						print "Date: ($date) day ($day) month ($month) year ($year)\n";
					}
					my $lastrun = timelocal(0,0,0,$day,$local_time_month,$year);
					my $current_timeseconds=time();						
					my $last_run_age = (($current_timeseconds - $lastrun) / 86400);
					if ($last_run_age > 7) {
						LogMsg(indent(1)."WARNING: $task has not run since $year-$month-$day");
						$warns++;
						push(@at_list,$task);
					} else {
						LogMsg(indent(1)."Task: $task Last Run: $date");
					}	
				}
			}			
			if ($s =~ /Scheduled Task State: /i) {				
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];				
				unless ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$warns++;
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");
				}					
			}						
		}
		if ($found_target2) {	
			if ($s =~ /Scheduled Task State: /i) {								
				my @tmp=split(/\s+/,$s);
				my $status=$tmp[3];						
				if ($status =~ /Enabled/i) {
					LogMsg(indent(1)."WARNING: State for $task is $status");
					$warns++;
					$found_gwx_task=1;					
				} else {
					LogMsg(indent(1)."Task: $task Status: $status");					
				}					
			}						
		}		
		if (($s =~ /At\d/i) && ($s =~ /TaskName:/)) {
			$found_target=1;						
			my @tmp=split(/\\/,$s);
			$task=$tmp[1];
			if ($OS eq "XP"){
				@tmp=split(/:/,$s);				
				$task=$tmp[1];				
				$task =~ s/\s//g;				
			}			
					
			LogMsg("Found task: $task");			
			$found_at_task=1;
		
		}
		if (($s =~ /refreshgwxconfigandcontent$/) && ($s =~ /TaskName:/)) {
			$found_target2=1;						
			my @tmp=split(/\\/,$s);
			$task=$tmp[-1];						
			LogMsg("Found task: $task");								
		}		

		if ($s =~ /Hostname/i) {
			$found_target=0;
			$found_target2=0;			
		}
	}
	if ($found_at_task) {
		LogMsg("Found AT tasks");
		#LogMsg("ISSUE: Found AT tasks");
		#$warns++;
		foreach my $a (@at_list) {
			LogMsg("Deleting task $a");
			my $cmd="schtasks /delete /tn $a /f";
			system($cmd);			
		}
	}
	if ($found_gwx_task) {
		LogMsg("Found GWX tasks"); 
		LogMsg("Disabling task ");
		my $cmd="schtasks /change /disable /tn \\Microsoft\\Windows\\Setup\\gwx\\refreshgwxconfigandcontent";
		 
		system($cmd);			

	}	
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking scheduled tasks");
		$warning_count++;
	} else {
		LogMsg("Found no issues checking scheduled tasks");
	}
}

sub check_memory_orig {
	# This function has not been satifactorily completed.
    # See which applications are using the most memory
	my @task_info=`tasklist`;
	my %task_memory_hash=();
	my $total_memory_used=0;
	my $top_user;
	my $top_user_memory=0;
	foreach my $t (@task_info) {
		chomp($t);
		
		my @tmp=split(/\s+/,$t);
		my $unit=$tmp[-1];
		my $task=$tmp[0];
		my $value;
		if ($unit eq "K") {
			$value=$tmp[-2];
			$value=~s/,//g;
		}
		if ($value) {			
			$task_memory_hash{$task}=$value;
			$total_memory_used+=$value;
			if ($value > $top_user_memory) {
				$top_user=$task;
				$top_user_memory=$value;
			}
		}
	}
	LogMsg("Total Memory Used: $total_memory_used K");
	# Get the physical memory
	my @meminfo=`wmic memphysical list full /format`;
	my $physical_memory=0;
	foreach my $m (@meminfo) {
		chomp($m);
		if ($m =~ /MaxCapacity/) {
			my @tmp=split(/=/,$m);
			$physical_memory=$tmp[1];
		}
	}
	LogMsg("Total Physical Memory: $physical_memory");
	my $percentage=sprintf("%.2f",(($total_memory_used/$physical_memory) * 100));
	LogMsg("$percentage percent of memory is used");
	LogMsg("System Info: Physical Memory: $physical_memory"); 	
	
	$percentage=sprintf("%.2f",(($top_user_memory/$total_memory_used) * 100));
	LogMsg("$top_user is using $percentage percent of the memory used");
	
	my $count=0;
	my $memTotal=0;
	my $top_process='';
	my @top_three=();
    foreach my $process (reverse sort{$task_memory_hash{$a}<=>$task_memory_hash{$b}} keys(%task_memory_hash)) {            
        $count++;
        $memTotal+=$task_memory_hash{$process};
		if ($count == 1) {
			$top_process=$process;
		}
        if ($count < 4){
            LogMsg(indent(2)."$process - $task_memory_hash{$process}");
			push(@top_three,$process);					
        } else {
            if ($verbose) {
                LogMsg("$process - $task_memory_hash{$process}");
            }
        }
    }	
	
}

sub check_misc {
	LogMsg("Running Miscellaneous Checks...");
	my $warns=0;	
	unless (check_administrator()) {
		$warns++;
	}
	$warns+=check_villages();
	
	unless (check_admin()) {
		$warns++;
	}	
	#check_master_browser();
	$warns+=check_virtual_drives();	
	$warns+=check_memory();
	$warns+=check_diskinfo();
	$warns+=check_hostname();
	$warns+=check_remote_app();
	$warns+=check_net_install();	
	$warns+=check_edna();
	#$warns+=check_gwx_install();
	check_cpu();
	if ($day_of_week == 1) {
		collect_info();
	}
	#$warns+=check_public_desktop();	# This is in preparation for using remote desktop from registers which we may not end up doing. - kdg
	
	my $store_manager_rdp="C:/Users/Villages/Desktop/Store Manager.rdp";
	#unless (-f $store_manager_rdp) {	# We will publish a remote app tool instead - kdg
		#LogMsg("ISSUE: No remote desktop for Store Manager found");
		#$warns++;
	#}
	if ($warns ) {
		LogMsg("WARNING: There were $warns issues running misc checks");
		$warning_count++;
	}
}

sub check_edna {
	# Verify we can connect to Edna server and see the shares
	if ($revised_network) {
		# These stores cannot see Edna
		return 0;
	}
	
	LogMsg("Checking connection to Edna...");
	my $cmd="net view \\\\Edna";
		
	my @response_info=`$cmd 2>&1`;
	my $warns=0;
	foreach my $r (@response_info) {
		chomp($r);				
		if ($r =~ /System error/i) {
			LogMsg("WARNING: Failed to see share on Edna");
			LogMsg("WARNING: $r");
			$warns=1;
			send_reboot_request();			
		}
	}
	return $warns;
	
}

sub check_gwx {
	my $warns=0;
	# Check installed updates
	LogMsg(indent(1)."Checking installed updates for gwx...");
	my $cmd="wmic qfe get";
	my @updates_info=`$cmd`;
	my $found_KB3035583=0;
	foreach my $u (@updates_info) {
		chomp($u);
		if ($u =~ /KB3035583/) {
			$found_KB3035583=1;
		}
	}
	if ($found_KB3035583) {					
		if (gwx_uninstall()) {
			LogMsg("WARNING: Found KB3035583 (GWX) to be installed");
			$warns++;
		}
	}
	return $warns;
}

sub gwx_uninstall {
	my $cmd="reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Microsoft\\Windows\\Gwx\"";				
		
	my @info=`$cmd`;  	
	 
	my $DisableGwx=0;
	my $warns=0;
 
	LogMsg("Checking if DisableGwx configured...");
	foreach my $i (@info) {
		chomp($i);  			 
		if ($i =~ /DisableGwx/) {				
			my @tmp=split(/\s+/,$i);
			LogMsg("Found DisableGwx: $tmp[-1]");
			if ($tmp[-1] eq "0x1") {
				$DisableGwx=1;			
			}				
		} 			
	}	
	unless ($DisableGwx) {
		LogMsg("WARNING: Disabling GWX notification");
		$cmd="reg add \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Microsoft\\Windows\\Gwx\" /v DisableGwx /t REG_DWORD /d 1 /f";				
		system("$cmd");
		$warns++;
	}
	return $warns;
}

sub check_gwx_install {
	#	This tasks was disabled after I decided to simply disable the scheduled task that runs gwx.exe
	return 0;
	#
	# GWX is the update to upgrade to Windows 10.
	# I am trying to find a good way to disable this. 
	# Here I am simply trying to move the GWX.exe out of where the system expects it to be.
	my $dir1="C:/Windows/System32/GWX";
	my $dir2="C:/Windows/SysWOW64/GWX";
	my $warns=0;
	foreach my $dir ("$dir1","$dir2") {
		if (-d $dir) {
			my @file_list=();
			my $save_dir="${dir}/originals";
			unless (-d $save_dir) {
				mkdir ($save_dir);
			}
			unless (-d $save_dir) {
				LogMsg("WARNING: Failed to create $save_dir");
				$warns++;
				last;
			}
			if (-d $save_dir) {
				opendir(GWX,"$dir");
				my @source_list=readdir(GWX);
				close(GWX);		
				foreach my $s (@source_list) {					
					if ($s =~ /exe$/) {
						LogMsg("Found $s in $dir");
						move("${dir}/${s}","${save_dir}/${s}");
						if (-f "${save_dir}/${s}") {
							if (-f "${dir}/${s}") {
								LogMsg("WARNING: Failed to move $s from $dir to $save_dir");
								$warns++;
							} else {
								LogMsg("Successfully moved $s from $dir to $save_dir");
							}
						}
					}
				}
			} else {
				LogMsg("WARNING: Failed to create $save_dir");
				$warns++;
			}
			
		}
	}
	return $warns;
}

sub collect_info {
	if ($OS eq "XP") {
		LogMsg("The collect_info function does not support $OS");
		return;
	}
	# The idea is to get the current version of FireFox available at Mozilla so that we know if
	# the version installed is obsolete or not.  
	
	# The Perl version of reading the website does not work because it is https and we 
	# do not have the Crypt::SSLeay module installed.  So, I am going to utilize a C# utility
	my $warns=0;
	# Get the version of FireFox currently available if possible
	my $tool = "c:/temp/scripts/gfr.exe";
	if (-f $tool) {
		LogMsg("Running $tool...");
		my @gfr_info=`$tool`;
		my $major_release;
		my $current_release;
		foreach my $g (@gfr_info) {
			if ($g =~ /releasenotes/) {
				if ($major_release) {
					if ($g =~ /$major_release/) {						
						my @tmp=split(/\"/,$g);
						$current_release=$tmp[1];						
						@tmp=split(/\//,$current_release);						
						$current_release=$tmp[1];							
					}
				} else {					
					my @tmp=split(/\"/,$g);
					$major_release=$tmp[1];					
					@tmp=split(/\//,$major_release);					
					$major_release=$tmp[1];	
				}
			}
		}
		if ($current_release) {
			# Update the system info file
			my @local_system_info_info = ("FireFoxRelease=$current_release","FireFoxRevision=$major_release");
			my @local_system_info2=();
			if (-f $local_system_info) {
				open(INFO,$local_system_info);
				@local_system_info2=(<INFO>);
				close INFO;
				for (my $x=0; $x<=$#local_system_info2; $x++) {
					my $line=$local_system_info2[$x];
					chomp($line);
					if ($line =~ /FireFoxRelease/) {
						$local_system_info_info[$x]="";					
					}
					if ($line =~ /FireFoxRevision/) {
						$local_system_info_info[$x]="";					
					}					
				}
			}
			# Now write the current info
			LogMsg("Updating $local_system_info...");
			open(INFO,">$local_system_info");
			foreach my $l (@local_system_info_info) {
				if ($l) {
					print INFO "$l\n";	
				}
			}
			foreach my $l (@local_system_info2) {
				if ($l) {
					print INFO "$l\n";	
				}
			}			
			close INFO;
		}
		
	} else {
		LogMsg("Error: Failed to locate $tool");
		$warns++;
	}
	
	return $warns;
	# Below is the Perl version which I am keeping just for the record;
	my $URL="https://www.mozilla.org/en-US/firefox/releases/";
    LogMsg("Checking FireFox site...");
    use LWP;	
    my $browser = LWP::UserAgent->new();	
    my $content=$browser->get($URL);	
    unless ($content) {
        # If we got nothing try once more
		print "Try getting again...\n";
        sleep 1;
        $content=get($URL);
    }
	
    if ($content) {			
        # If we got content, parse it for the info we want        
		my %content_hash=%$content;
		foreach my $key (sort keys(%content_hash)) {
			print "($key)\n";
			print "($content_hash{$key})\n";
		}
        
	} else {
		LogMsg("WARNING: Failed to get info from Mozilla");
		$warns++;
	}
	
	return $warns;
}

sub check_villages {
	if ($OS eq "XP")  {
		return 0;
	}
	# List of machines which do not have a Villages user
	my @exclusion_list=(
		"Operationsmgr"
	);
	foreach my $e (@exclusion_list) {		
		if ($hostname eq "$e") {
			LogMsg("Excluding $hostname from check_villages");
			return 0;
		}
	}
 
	if ($verbose) {
		print "Checking Villages...\n";
	}
	my $warns=0;

	my $cmd="net user VILLAGES";
	my @info=`$cmd`;
	
	foreach my $i (@info) {
		chomp($i);		
		if ($i =~ /Account active/) {
			unless ($i =~ /yes/i) {	 			 
				LogMsg(indent(1)."ISSUE: VILLAGES account is not active");				
			}
		}
		if ($i =~ /Local Group Membership/) {			 	
			if (($i =~ /administrator/i) || ($i =~ /power/i)) {
				LogMsg(indent(1)."WARNING: Villages: $i");
				$warns++;
			} else {
				LogMsg(indent(1)."Villages: $i");
			}
		}
		if ($i =~ /Last logon/) {
			LogMsg(indent(1)."Villages: $i");			
		}
		if ($i =~ /The user name could not be found/) {
			LogMsg(indent(1)."Villages: Could Not be Found");			 		
		}		
	}
	# Has anyone ever logged in?  The desktop will not be their if the user has never logged on
	my $user_dir="c:/users";
	my $home_dir="";
    opendir(HOME,"$user_dir");
    my @home_list=readdir(HOME);
    close(HOME);
    foreach my $p (@home_list) {
		
		if ($p =~ /VILLAGES/i) {				
				$home_dir=$p;
		}
	}
	if ($home_dir) {		
		my $desktop="${user_dir}/${home_dir}/desktop";		
		unless (-d $desktop) {
			LogMsg(indent(1)."ISSUE VILLAGES has never logged on");
			#$return=0;
		}
	} else {
	
		LogMsg(indent(1)."WARNING: Failed to determine VILLAGES home");
		$warns++;
		
	}
	return $warns;
}

sub check_administrator {
	if ($OS eq "XP"){
		return 1;
	}
	if ($verbose) {
		print "Checking administrator...\n";
	}
	my $cmd="net user administrator";
	my @info=`$cmd`;
	my $return=1;
	foreach my $i (@info) {
		chomp($i);		
		if ($i =~ /Account active/) {
			unless ($i =~ /yes/i) {	
				# Set the administrator active
				$cmd="net user administrator /active:yes";
				@info=`$cmd`;
				foreach my $i (@info) {
					chomp($i);		
					LogMsg(indent(1)."$i");
				}				
				$cmd="net user administrator ****";
				@info=`$cmd`;
				foreach my $i (@info) {
					chomp($i);		
					LogMsg(indent(1)."$i");
				}				
 
				LogMsg(indent(1)."ISSUE: Administrator account is not active");
				$return=0;
			}
		}
		if ($i =~ /Last logon/) {
			LogMsg(indent(1)."Administrator: $i");			
		}
	}
	# Has anyone ever logged in?  The desktop will not be their if the user has never logged on
	my $user_dir="c:/users";
	my $home_dir="";
    opendir(HOME,"$user_dir");
    my @home_list=readdir(HOME);
    close(HOME);
    foreach my $p (@home_list) {
		
		if ($p =~ /administrator/i) {				
				$home_dir=$p;
		}
	}
	if ($home_dir) {		
		my $desktop="${user_dir}/${home_dir}/desktop";		
		unless (-d $desktop) {
			LogMsg(indent(1)."ISSUE Administrator has never logged on");
			#$return=0;
		}
	} else {
			LogMsg(indent(1)."WARNING: Failed to determine Administrator home");
			$return=0;	
	}
	return $return;
}

sub check_admin {	
	if ($verbose) {
		print "Checking Admin...\n";
	}
	my $cmd="net user";
	my @info=`$cmd`;
	my $return=1;
	my $found_admin=0;
	foreach my $i (@info) {
		chomp($i);		
		if ($i =~ /Admin\s/) {			
			$found_admin=1;			
		}
	}
	if ($found_admin) {
		$cmd="net user Admin";
		@info=`$cmd`;
		
		foreach my $i (@info) {
			chomp($i);		
			if ($i =~ /Account active/) {
				if ($i =~ /yes/i) {		
					LogMsg(indent(1)."ISSUE: Admin account is active");
					#$return=0;
				}
			}
		}	
	}
	return $return;
}

sub check_backups {
	if ($OS eq "XP") {
		return;
	}
	print "Checking Backups...\n";
	my $warns=0;

	# Are system backups configured?  Read from the system_registry_info.txt file
	if (-f $system_registry_info) {
		open("INFO","$system_registry_info");
		my @registry_info=(<INFO>);
		close INFO;
		my $target="WindowsBackup";
		my $target_found=0;
		my $validconfig=0;	
		my $ValidSystemImageBackup=0;
		my $BackupType=0;
		my $IncludeFutureUsers=0;
		my $UsersFullyExcluded=0;
		my $BackupFlags=0;
		my $KeepLastCPCBackup=0;
		foreach my $r (@registry_info) {
			chomp($r);
			if ($target_found) {				
				if ($r =~ /ValidConfig/) {
					my @tmp=split(/\s+/,$r);
					my $vc=$tmp[-1];
					if ($vc eq "0x1") {
						$validconfig=1;
					}
				}
				if ($r =~ /ValidSystemImageBackup/) {
					my @tmp=split(/\s+/,$r);
					my $vc=$tmp[-1];
					if ($vc eq "0x1") {
						$ValidSystemImageBackup=1;
					}
				}	
				if ($r =~ /BackupType/) {
					my @tmp=split(/\s+/,$r);
					my $vc=$tmp[-1];
					if ($vc eq "0x3") {
						$BackupType=1;
					}
				}
				if ($r =~ /IncludeFutureUsers/) {
					my @tmp=split(/\s+/,$r);
					my $setting=$tmp[-1];
					if ($setting eq "0x1") {
						$IncludeFutureUsers=1;
					}
				}	
				if ($r =~ /UsersFullyExcluded/) {
					my @tmp=split(/\s+/,$r);
					my $setting=$tmp[-1];
					if ($setting eq "0x1") {
						$UsersFullyExcluded=1;
					}
				}	
				if ($r =~ /BackupFlags/) {
					my @tmp=split(/\s+/,$r);
					my $setting=$tmp[-1];
					if ($setting eq "0x1") {
						$BackupFlags=1;
					}
				}	
				if ($r =~ /KeepLastCPCBackup/) {
					my @tmp=split(/\s+/,$r);
					my $setting=$tmp[-1];
					if ($setting eq "0x1") {
						$KeepLastCPCBackup=1;
					}
				}																	
			}
			if ($r =~ /HKEY_LOCAL_MACHINE/) {
				$target_found=0;
			}
			if ($r =~ /$target/) {
				$target_found=1;
			}
		}
		unless ($validconfig) {
			LogMsg("WARNING: Failed to locate a valid backup config");
			$warns++;	
		}	
		unless ($ValidSystemImageBackup) {
			LogMsg("WARNING: Failed to locate a valid system image backup");
			$warns++;	
		}		
		unless ($BackupType) {
			LogMsg("WARNING: Failed to find the expected backup type.");
			$warns++;	
		}		
		unless ($UsersFullyExcluded) {
			LogMsg("WARNING: Looks like backups are NOT set to exclude users.");
			$warns++;	
		}	
		unless ($BackupFlags) {
			LogMsg("WARNING: Looks like backups are set to Let Windows Choose.");
			$warns++;	
		}	
		unless ($KeepLastCPCBackup) {
			LogMsg("WARNING: Looks like backups are NOT set to just keep latest image.");
			$warns++;	
		}			
		
	} else {
		LogMsg("WARNING: Failed to locate $system_registry_info file");
		$warns++;
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking backups");
		$warning_count++;
	} 
}

sub check_recovery_partition {
	if ($verbose) {
		print "Checking Recovery Partition...\n";
	}
	# Is drive D still the HP Recovery?
	my $return=1;
	my $cmd="dir /a d:";
	my @info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /HP_RECOVERY/) {
				LogMsg(indent(1)."ISSUE: Drive D is HP_RECOVERY");
				$return=0;		
		}
	}
	return $return;	
}

sub check_virtual_drives {
 
	if ($hostname eq "BO5Hallway") {
		return 0;
	}	
	if ($OS eq "XP"){
		return 0;
	}
	my $warns=0;
	my $backup_drive=0;
	my $cmd="wmic logicaldisk list /format:list";
	my %drive_hash=();
	my @disk_info=`$cmd`;
	my $caption;
	my $volume;
	foreach my $d (@disk_info) {
		chomp($d);
		chop($d);
		if ($d =~ /Caption/i) {
			my @tmp=split(/=/,$d);
			$caption=$tmp[1];
			$caption=~s/://g;			
		}
		if (($d =~ /VolumeName/i) && ($d =~ /backup/i)) {
			$backup_drive=$caption;		  
		}		
		 
		if (($d =~ /VolumeName/i) && ($d =~ /HP_RECOVERY/i)) {		
				LogMsg(indent(1)."ISSUE: Drive $caption is HP_RECOVERY");
				 	
		}		
	}
	unless ($backup_drive) {
		LogMsg(indent(1)."ISSUE: Failed to find Backups drive");
		 		
	}
	my $dir="${backup_drive}:/WindowsImageBackup";
 
	if (-d $dir) {
		LogMsg(indent(1)."Found WindowsImageBackup");
		LogMsg("System Info: Backup: $dir"); 					 	
	} else { 
		# I could not get the system image task to work as I wanted so it is disabled - kdg
=pod		
		if ($backup_drive) {
			create_system_image_script($backup_drive);
			set_system_image_task();
			run_system_image_task();
		} else {
			LogMsg(indent(1)."ISSUE: Failed to find WindowsImageBackup");
		}
=cut		
	}
	$warns+=disable_system_image_task();	
	return $warns;	
}

sub check_backup_image_orig {
	# 
	if ($hostname eq "BO5Hallway") {
		return 1;
	}	
	my $return=1;
	
	my $dir="F:/WindowsImageBackup";
	my $dir_alt="D:/WindowsImageBackup";
	my $dir_alt2="E:/WindowsImageBackup";
	my $found_backup=0;
	foreach my $d ("$dir","$dir_alt","$dir_alt2") {
		if (-d $d) {
			LogMsg(indent(1)."Found WindowsImageBackup");
			LogMsg("System Info: Backup: $d"); 				
			$found_backup=$d;		
		}
	}
	unless ($found_backup) {
		LogMsg(indent(1)."ISSUE: Failed to find WindowsImageBackup");
		$return=0;	
	}
	return $return;	
}

sub check_memory {
	if ($hostname eq "BO5Hallway") {
		return 0;
	}
	my $cmd="systeminfo";
	my @info=`$cmd`;
	my $mem_total=0;
	my $mem_label;
	my $mem_available=0;
	my $percent_used=0;
	my $warns=0;
	foreach my $i (@info) {
		chomp($i);
		if ($i =~ /Total Physical Memory.*MB/) {
			my @tmp=split(/\s+/,$i);
			$mem_total=$tmp[3];
			$mem_total=~ s/,//g;
			$mem_label = $tmp[4];
		}
		if ($i =~ /Available Physical Memory.*MB/) {
			my @tmp=split(/\s+/,$i);
			$mem_available=$tmp[3];
			$mem_available=~ s/,//g;
		}		
	}
	if ($mem_total) {
		LogMsg("System Info: Physical Memory: $mem_total $mem_label"); 
		if ($mem_available) {
			my $mem_used=($mem_total - $mem_available);
			$percent_used=sprintf("%.2f",(($mem_used/$mem_total) * 100));
			
			if ($percent_used > 85) {
				LogMsg(indent(1)."WARNING: Memory used is $percent_used %");
				$warns++;
			} else {
				LogMsg(indent(1)."Memory used is $percent_used %");
			}
		} else {
			LogMsg(indent(1)."Unable to determine Memory Available");
		}		
	} else {
		LogMsg(indent(1)."Unable to determine Total Memory ");
	}
	return $warns;	
}

sub check_diskinfo {
	if ($OS eq "XP") {
		return 0;
	}
	my $warns=0;
	my $cmd="wmic volume list /format:list";
	my @info=`$cmd`;
	my $drive_letter;
	my $capacity;
	my $freespace;
	my $space_used;
	my $max = 80;
	if (($storenum == 4177) || ($storenum == 4745)) {
		$max = 85;
	}
	foreach my $i (@info) {
		chomp($i);
		chop($i);	
		if ($i =~ /Automount/) {
			$drive_letter=0;
			$capacity=0;
			$freespace=0;
			$space_used=0;
		}
		if ($i =~ /Capacity/) {
			my @tmp=split(/=/,$i);			
			$capacity=$tmp[1];
		}
		if ($i =~ /DriveLetter/) {
			my @tmp=split(/=/,$i);
			$drive_letter=$tmp[1];
			if ($drive_letter) {
				if ($capacity) {
					LogMsg("System Info: Drive Capacity: $drive_letter $capacity"); 				
				}
			}
		}		
		if ($i =~ /FreeSpace/) {
			my @tmp=split(/=/,$i);
			$freespace=$tmp[1];

		}	
		if ($i =~ /SystemName/) {
			if ($drive_letter) {
				
				if ($capacity) {
					if ($freespace) {						
						my $used=($capacity - $freespace);
						my $percentage = sprintf("%.2f",(($used / $capacity) * 100));
						LogMsg("System Info: Drive Utilization: $drive_letter $percentage percent used"); 
						for my $drive ("C:","D:") {
							my $cap = ($capacity / 1000000000);
							if (($drive eq "D:") && ($cap < 100)) {
								$max = 90;
							}
							if (($percentage > $max) && ($drive_letter eq "$drive")) {
								my $d=$drive;
								$d=~ s/://g;
								LogMsg("ISSUE: Drive $d Utilization $percentage percent used"); 
								LogMsg("WARNING: Drive $drive Utilization: $percentage percent used"); 
								$warns++;	
								if ($d =~ /D/i) {
									check_backups();
								}
							} else {
								if ($drive_letter eq "$drive") {
									LogMsg(indent(1)."Drive $drive: is $percentage percent used");
								}
							}						
						}
					}
				}
			}
		}
	}
	return $warns;
}

sub check_hostname {
	if ($hostname =~ /generic/i) {
		LogMsg(indent(1)."WARNING: Hostname is not customized: $hostname");
		return 1;
	}
	return 0;
}

sub check_cpu {
	LogMsg("Getting CPU info...");
	my $cmd="wmic cpu get description";	
	my @info=`$cmd`;	
	foreach my $i (@info) {
		next unless ($i =~ /\w/);
		unless ($i =~ /^Description/i) {	 
			LogMsg("System Info: CPU Description: $i"); 
		} 		
	}
	$cmd="wmic cpu get name";	
	@info=`$cmd`;	
	foreach my $i (@info) {
		next unless ($i =~ /\w/);
		unless ($i =~ /^Name/) { 
			LogMsg("System Info: CPU Name: $i"); 
		}		
	}	
}
 
sub check_public_desktop {
	# Entries here can be seen by all users.  Only certain entries should be permitted
	my $public_desktop="c:/users/public/desktop";
	my $villages_desktop="c:/users/villages/desktop";
	my $administrator_desktop="c:/users/administrator/desktop";
	my @accepted_list=(
		"desktop.ini",
		"firefox.lnk",
	);	
	my $warns=0;
	if (-d $public_desktop) {
		opendir(HOME,"$public_desktop");
		my @home_list=readdir(HOME);
		close(HOME);
		foreach my $p (@home_list) {
			next if ($p eq ".");
			next if ($p eq "..");
			my $accepted=0;
			$p=lc($p);
			foreach my $a (@accepted_list) {
				$a=lc($a);
				if ($a eq $p) {
					$accepted=1;
				}
			}			
			unless ($accepted) {
				my $source="${public_desktop}/$p";
				my $purge_allowed=0;
				if (-d $villages_desktop) {
					if (-f "${villages_desktop}/$p") {
						LogMsg(indent(1)."Found $p on $villages_desktop");	
						$purge_allowed=1;						
					} else {
						LogMsg(indent(1)."Copying $p to $villages_desktop ...");						 
						my $target="${villages_desktop}/$p";
						copy($source,$target);
						if (-f $target) {
							LogMsg(indent(2)."Success");
							$purge_allowed=1;
						} else {
							LogMsg(indent(2)."ERROR: Failed to create $target");
							$warns++;
							$purge_allowed=0;
						}
					}
				}
				if (-d $administrator_desktop) {
					if (-f "${administrator_desktop}/$p") {
						LogMsg(indent(1)."Found $p on $administrator_desktop");
						$purge_allowed++;
					} else {
						LogMsg(indent(1)."Copying $p to $administrator_desktop ...");						 
						my $target="${administrator_desktop}/$p";
						copy($source,$target);
						if (-f $target) {
							LogMsg(indent(2)."Success");
							$purge_allowed++;
						} else {
							LogMsg(indent(2)."ERROR: Failed to create $target");
							$warns++;
							$purge_allowed=0;
						}						
					}
				}		
				if ($purge_allowed == 2) {
					LogMsg(indent(1)."Deleting $source...");
					unlink $source;
				} else {
					LogMsg(indent(1)."WARNING: found $p on the  $public_desktop");
					$warns++;
				}
			}
		}
	} else {
		LogMsg(indent(1)."WARNING: Failed to locate $public_desktop");
		$warns++;
	}
	return $warns;
} 

sub set_task {
	# This function will create a scheduled task to run the data_backup script
	LogMsg("Setting data backup task...");
	my $cmd="SCHTASKS /Create /SC daily /TN data_backup /ST 03:00:00 /TR c:/temp/scripts/data_backup.bat /RU administrator /RP ****";	 
	my @cmd_info=`$cmd`;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
	}	
}

 
sub create_data_backup_script {
	LogMsg("Creating $data_backup_script");
	open(SCRIPT,">$data_backup_script");
	if ($revised_network) {	
		print SCRIPT "xcopy C:\\Users\\Villages\\Documents C:\\temp\\share\\OfficeBackup\\\"My Documents\" /M /S /I /R /Y\n";
		print SCRIPT "xcopy C:\\Users\\Villages\\Desktop C:\\temp\\share\\OfficeBackup\\Desktop /M /S /I /R /Y\n";	
	} else {
		print SCRIPT "xcopy C:\\Users\\Villages\\Documents \\\\Edna\\OfficeBackup\\\"My Documents\" /M /S /I /R /Y\n";
		print SCRIPT "xcopy C:\\Users\\Villages\\Desktop \\\\Edna\\OfficeBackup\\Desktop /M /S /I /R /Y\n";
	}
	close SCRIPT;
}

sub create_data_backup_script_ephrata {
	LogMsg("Creating $data_backup_script for Ephrata workstation...");
 
	if ($hostname eq "Operationsmgr") {
		open(SCRIPT,">$data_backup_script");
		print SCRIPT "xcopy C:\\Users\\Lynnellen\\Documents \\\\Edna\\OfficeBackup\\$hostname\\\"My Documents\" /M /S /I /R /Y\n";
		print SCRIPT "xcopy C:\\Users\\Lynnellen\\Desktop \\\\Edna\\OfficeBackup\\$hostname\\Desktop /M /S /I /R /Y\n";
		close SCRIPT;
	
	} else {
		open(SCRIPT,">$data_backup_script");
		print SCRIPT "xcopy C:\\Users\\Villages\\Documents \\\\Edna\\OfficeBackup\\$hostname\\\"My Documents\" /M /S /I /R /Y\n";
		print SCRIPT "xcopy C:\\Users\\Villages\\Desktop \\\\Edna\\OfficeBackup\\$hostname\\Desktop /M /S /I /R /Y\n";
		close SCRIPT;
	}
}
=pod
sub evaluate_backup_script {
	open(SCRIPT,"$data_backup_script");
	my @text_info=(<SCRIPT>);
	close SCRIPT;
	my $found_edna=0;
	my $found_share=0;
	foreach my $t (@text_info) {
	}
}
=cut
sub set_weekly_task {
	# This function will create a scheduled task to run the weekly_task script
	LogMsg("Setting weekly scheduled task...");
	my $cmd="SCHTASKS /Create /SC weekly /TN weekly_task /ST 02:00:00 /TR c:/temp/scripts/weekly_task.bat /RU administrator /RP ****";
 
	my @cmd_info=`$cmd`;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
	}	
}
sub create_weekly_script {
	if ($OS eq "XP") {
		return;
	}
	# Note, I have had difficulty reading some registry settings from Perl on 64bit systems.  I am trying this method:
	# 1) Create a batch file to run the query and save the results in a text file.
	# 2) Create a scheduled task to be run by administrator
	# 3) Simply read the resulting text file to get the registry settings.
	# It is not clear yet that this will work. - kdg
	LogMsg("Creating $weekly_task");
	open(SCRIPT,">$weekly_task");
	print SCRIPT "reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\SystemRestore\" > $system_registry_info\n";	
	print SCRIPT "reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\HomeGroup\\NetworkLocations\\Home\" >> $system_registry_info\n";		
	print SCRIPT "reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\HomeGroup\\NetworkLocations\\Work\" >> $system_registry_info\n";	
	print SCRIPT "reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsBackup\" /s>> $system_registry_info\n";
	print SCRIPT "reg query \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\" /s>> $system_registry_info\n";   
	print SCRIPT "nbtstat -a $hostname > $system_network_info\n";
	close SCRIPT;
}

sub set_system_image_task {
	# This function will create a scheduled task to run the system_image_task script
	LogMsg("Checking for system image task ...");
	my $cmd="SCHTASKS /Query /TN system_image_task";
	my @cmd_info=`$cmd`;
	my $found_task=0;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
		if ($c =~ /system_image_task/) {
			$found_task=1;
		}
	}	
	if ($found_task) {
		LogMsg("Found system image task set");
	} else {
		LogMsg("Setting system image scheduled task...");
		$cmd="SCHTASKS /Create /SC weekly /TN system_image_task /ST 02:10:00 /TR c:/temp/scripts/system_image_task.bat /RU administrator /RP **** /F"; 
		@cmd_info=`$cmd`;
		foreach my $c (@cmd_info) {
			LogMsg(indent(1)."$c");
		}	
	}
}

sub disable_system_image_task {
	# This function will disable the system_image_task scheduled task.
	LogMsg("Checking for system image task ...");
	my $cmd="SCHTASKS /Query /TN system_image_task";
	my @cmd_info=`$cmd`;
	my $found_task=0;
	my $warns=0;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
		if ($c =~ /system_image_task/) {
			$found_task=1;
		}
	}	
	if ($found_task) {
		LogMsg("Found system image task set");
		LogMsg("Removing system image task");
		$cmd="schtasks /delete /tn system_image_task /f"; 	
		system($cmd);
		if ($?) {
			LogMsg("WARNING: Received an error attempting to delete system_image_task"); 
			$warns++;
		} else {
			LogMsg("Successfully deleted system_image_task");
		}		
	}
	return $warns;
}
 
sub create_system_image_script { 
	my $drive=shift;
	LogMsg("Creating system_image script");
	open(SCRIPT,">$system_image_task");
	print SCRIPT "Wbadmin start backup -backupTarget:$drive: -include:C: -quiet\n";	
	close SCRIPT;
	LogMsg("Completed system image script");
}

sub run_system_image_task {
	# Check to see if it is running
	my @task_info=`tasklist`;
	my $found_running=0;
	foreach my $t (@task_info) {
		chomp($t);
		if ($t =~ /wbadmin/i) {
			$found_running=1;
		}
	}
	if ($found_running) {
		LogMsg("System image backup appears to be in progress.");
		return;
	}
	 
	LogMsg("Running system image task...");
	my $cmd = "SCHTASKS /RUN /TN system_image_task";
	LogMsg("Calling: $cmd");
	my @cmd_info=`$cmd`;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
	}	
	LogMsg("Finished");
}

sub run_weekly_task {
	if ($OS eq "XP") {
		return;
	}
	LogMsg("Running weekly task...");
	my $cmd = "SCHTASKS /RUN /TN weekly_task";
	LogMsg("Calling: $cmd");
	my @cmd_info=`$cmd`;
	foreach my $c (@cmd_info) {
		LogMsg(indent(1)."$c");
	}	
	sleep 4;
	LogMsg("Finished");
}

sub check_system_protection {
	if ($OS eq "XP") {
		return;
	}
	unless ($day_of_week == 1) {
		# Run only on Monday
		#return;
	}
	# Is system protection turned on?
	my $system_protection_setting=0;
	if (-f $system_registry_info) {
		open(INPUT,"$system_registry_info");
		my @file_info=(<INPUT>);
		close INPUT;
		foreach my $f (@file_info) {
			chomp($f);
			if ($f =~ /RPSessionInterval/) {			
				my @tmp=split(/x/,$f);				
				$system_protection_setting=$tmp[1];			
			}			
		}
		if ($system_protection_setting) {
			LogMsg("System Protection is turned on");
		} else {
			LogMsg("WARNING: System Protection is turned off");
			$warning_count++;
		}
	} else {
		LogMsg("NOTICE: Did not find $system_registry_info");
	}
	
}

sub check_network {
	if ($OS eq "XP") {
		return;
	}
	unless ($day_of_week == 1) {
		# Run only on Monday
		#return;
	}
	# What is the network location?
	my $network_location=0;
	if (-f $system_registry_info) {		
		open(INPUT,"$system_registry_info");
		my @file_info=(<INPUT>);
		close INPUT;
		my $found_work=0;
		my $found_home=0;
		my $work_enabled=0;		
		my $home_enabled=0;	
		foreach my $f (@file_info) {			
			chomp($f);			
			if ($f =~ /^HKEY/) {
				$found_work=0;
				$found_home=0;
			} 						
			if ($found_work) {				
				if ($f =~ /REG_SZ/) {					
					if ($f =~ /villages.com/i) {					
						$work_enabled=1;
					}
					if ($f =~ /Network/i) {					
						$work_enabled=1;
					}					
				}					
			}
			if ($found_home) {				
				if ($f =~ /REG_SZ/) {					
					if ($f =~ /villages.com/i) {					
						$home_enabled=1;
					}
					if ($f =~ /Network/i) {					
						$home_enabled=1;
					}					
				}					
			}			
			if ($f =~ /NetworkLocations/) { 
				if ($f =~ /Work/) {
					$found_work=1;			
				}
				if ($f =~ /Home/) {
					unless ($f =~ /HomeGroup/) {
						$found_home=1;			
					}
				}				
			}			
		}
		if ($work_enabled) {
			LogMsg("Network Location appears to be set to work.");
			if ($home_enabled) {
				LogMsg("WARNING: Network Location may be set to home  (I am confused).");
				$warning_count++;			
			}
		} else {
			LogMsg("WARNING: Network Location is not set to work.");
			$warning_count++;
			if ($home_enabled) {
				LogMsg("WARNING: Network Location may be set to home.");				
			}			
		}
	} else {
		LogMsg("NOTICE: Did not find $system_registry_info");		 
	}
	check_master_browser();
	
}

sub check_eventlog {
	if ($OS eq "XP") {
		return 0;
	}
	# This checks the last 100 Application and System events.  It then filters only for events that occured on the current or 
	# previous day if it is a Warning or an Error.  (This means that it could report an event two days in a row.)
	# Need to do: Construct a list of events of interest to only report events we are really interested in.  
	LogMsg("Checking EventLog");
 	my $warns=0;
	my $backup_space_issue=0;
	my @events_of_interest=();	# This will be the list of events to report
	my @show_task_list=(
		"Report Id:"
	);
 
	my @ignore_events=(
		"Activation context generation failed",
		"C4PRG",
		"C4SCN",
		"BrtTWN",
		"Using LRPC",	 
		"Updates to the IIS metabase",
		"LMS Service cannot connect",
		"InstanceModificationEvent",
		"The file was deleted successfully",
		"A problem prevented Customer Experience Improvement Program data",
		"has opened key.*Custom Tasks",
		"has opened key.*Disallowed",
		"has opened key.*TrustedPeople",
		"has opened key.*SystemCertificates",	
		"has opened key.*Policies",	
		"has opened key",	
		"The device, .Device.Ide.iaStor0, did not respond within the timeout period",
		"A server error occurred. Check that the server is available",
		"The time service has not synchronized the system time",
		"LiveUpdate returned a non-critical error",
		"Name resolution for the name",		
 
	);
	my @ignore_keys=(
		"Couldnotscan",
		
	);
	my @ignore_sources=(
		"Application Hang",
	);
	my %ignore_tasks=(
		"Application Crashing Event" => 'Explorer.EXE',
	);
	my @symantec_reboot_strings=(
		"Network Intrusion Prevention is not protecting machine because its driver was unloaded",		
		"Heuristic Scan or Load Failure"
	);
	my @symantec_repair_strings=(		
		"SONAR has generated an error: code 0: description: Definition Failure",		
	);	
	my $restart_symantec=0;
	foreach my $type ("Application","System") {
		LogMsg("Checking event type: $type");
		# wevtutil - Windows Events Command Line Utility
		# qe - query event
		# /rd: - direction in which events are read.  true means most recent first		   
		# /f:  - report in text format
		# /c:  - last 100 entries
		my $cmd="wevtutil qe $type /rd:true /f:text /c:100";
		my @event_info=`$cmd`;
		my %event_hash;
		my $event;
		my $found_description=0;
	
		foreach my $e (@event_info) {
			chomp($e);
			if ($e =~ /^Event\[/i) {
				my @tmp=split(/\[/,$e);
				$event=$tmp[1];
				@tmp=split(/\]/,$event);
				$event=$tmp[0];
				# This is a new entry
				if (defined($event_hash{"Level"})) {
					
					my $event_date=$event_hash{"Date"} ;
					$event_date=~s/ //g;
					my @tmpd=split(/T/,$event_date);
					$event_date=$tmpd[0];
					my $ignore_this=0;
					foreach my $i (@ignore_events) {						
						if ($event_hash{"Description"} =~ /$i/i) {
							$ignore_this=1;
						}
					}
					foreach my $i (@ignore_sources) {						
						if ($event_hash{"Source"} =~ /$i/i) {
							$ignore_this=1;
						} 
					}					
					foreach my $key (sort keys(%event_hash)) {
						foreach my $ig (@ignore_keys) {
							if ($key =~ /$ig/) {
								$ignore_this=1;
							}
						}
					}
					unless ($ignore_this) {					
						foreach my $ig (sort keys(%ignore_tasks)) {						
							my $task=$event_hash{'Task'};
							if ($task =~ /$ig/) {								
								$ignore_this=1;
							}
						}
					}
					 
					unless ($ignore_this) {
						if (($event_date eq "$previous_date_label") || ($event_date eq "$date_label")) {
							if (($event_hash{"Level"} =~ /Warning/) || ($event_hash{"Level"} =~ /Error/)) {					
								$warns++;
								foreach my $key (sort keys(%event_hash)) {						
									LogMsg(indent(1)."$key : $event_hash{$key}");									
									if ($key eq "Description") {
										# Handle Specific Events
										if ($event_hash{$key} =~ /Failed to create restore point/) {
											LogMsg(indent(1)."ISSUE: Failed to create restore point");
										} else {	
											 
											LogMsg(indent(1)."WARNING: $event_hash{$key}"); 
										}
										if ($event_hash{"Source"} =~ /Symantec/) {
											foreach my $s (@symantec_reboot_strings) {
												if ($event_hash{$key} =~ /$s/) {
													$restart_symantec=1;
												}
											}
											foreach my $s (@symantec_repair_strings) {
												if ($event_hash{$key} =~ /$s/) {
													LogMsg(indent(2)."WARNING: Symantec Endpoint Protection may need to be repaired");													
												}
											}											
										}
										foreach my $s (@show_task_list) {											
											if ($event_hash{$key} =~ /$s/) {												
												if (defined($event_hash{"Faultingapplicationname"})) {	
 												
													LogMsg(indent(2)."WARNING: $event_hash{'Faultingapplicationname'}");
												}
												if (defined($event_hash{"Task"})) {	
 												
													LogMsg(indent(2)."WARNING: $event_hash{'Task'}");
												}		
												if (defined($event_hash{"Source"})) {
 											
													LogMsg(indent(2)."WARNING: $event_hash{'Source'}");
												}	
												if (defined($event_hash{"ApplicationPath"})) {	
 												
													LogMsg(indent(2)."WARNING: $event_hash{'ApplicationPath'}");
												}													
											} 
											
										}
									}
								}			
							}
						}
					}
				}
				%event_hash=();
				$found_description=0;			
			} else {
				my $key='';
				my $value='';
				if ($found_description) {
					if ($e) {
						$value=$e;					
						$event_hash{"Description"}=$value;
					}
				}			
				if ($e =~ /:/) {
					my @tmp=split(/:/,$e);
					$key=$tmp[0];				 
					$key=~s/ //g;
					shift @tmp;
					if ($key =~ /Description/) {
						$found_description=1;					
					} else {
						$value = join(':',@tmp);
						$event_hash{"$key"}=$value;
					}
				}			 	
			}
		}
	}
	if ($warns) {
		LogMsg("WARNING: $warns warning or error Events found");
		$warning_count++;
	}
	if ($restart_symantec) {
		# Note:  Cannot stop the service.  The service may indicate it is 
		# running in which case it cannot be started.  May need to 
		# call for a reboot. - kdg
		my $service="SepMasterService";
		LogMsg("Request restart for $service");
		my $cmd="sc start $service";
		my @info=`$cmd`;
		foreach my $i (@info) {
			chomp($i);
			LogMsg("$i");
			if ($i =~ /FAILED/) {
				LogMsg("WARNING: This computer should be rebooted");
				send_reboot_request();				
			}
		}		
	}
}
 
sub send_reboot_request {
	LogMsg(indent(1)."Send Reboot Request...");
	my $srr_timeseconds=time();
	my $tool="c:/Windows/System32/qwinsta.exe";
	my $tool_alt="c:/temp/scripts/qwinsta.exe";
	unless (-f $tool) {
		if (-f $tool_alt) {
			$tool=$tool_alt;
		}
	}
	
	my $user=0;
	
	if (-e $tool) {
		LogMsg("Located $tool");
		my @who_info=`$tool 2> /temp/junk.txt`;
			
		foreach my $w (@who_info) {
			chomp($w);
			if ($w =~ /ACTIVE/i) {
				my @tmp=split(/\s+/,$w);				
				$user=$tmp[1];				
			}
		}	
	} else {
		LogMsg("Failed to Locate $tool");
	}
	
	### MSG ###
	$tool="c:/Windows/System32/msg.exe";
	$tool_alt="c:/temp/scripts/msg.exe";
	unless (-f $tool) {
		if (-f $tool_alt) {
			$tool=$tool_alt;
		}
	}	
	if (-e $tool) {
		LogMsg("Located $tool");	
		if ($user) {		
			my $cmd="$tool $user /time:86400 \"Please reboot your workstation at your earliest convenience.\n\nThank You!\nPOS Support - Akron";		
			LogMsg(indent(2)."Sending message...");
			system($cmd);
			open(LOG,">>$reboot_request_log");
			print LOG "RebootRequest $srr_timeseconds\n";
			close LOG;
		} else {
			LogMsg(indent(1)."Failed to determine user.");
		}
	} else {
		LogMsg("Failed to Locate $tool");		
	}
	LogMsg(indent(1)."Leaving Send Reboot Request.");
}

sub restart_service {
	my $service=shift;
	stop_service($service);
	start_service($service);
}

sub stop_service {
    my $service=shift;       
    my $state="";
    my $attempt=0;
	my $cmd="sc qc \"$service\"";	
    my @service_info=`$cmd`;    
    foreach my $s (@service_info) {	
        if ($s =~ /The specified service does not exist/i) {		
            return 1;
        }        
    }	
    my $querycmd="sc query \"$service\"";
    my $stopcmd="sc stop \"$service\"";
    @service_info=`$querycmd`;   
    until (("$state" eq "STOPPED") || ($attempt > 10)) {
        $attempt++;
		LogMsg("Checking $service...");
        @service_info=`$querycmd`;         

        foreach my $s (@service_info) { 
            chomp($s);            
            if ($s =~ /STATE/) {                     
                my @tmp=split(/ /,$s);
                chomp($state=$tmp[$#tmp]);
                LogMsg("current state: $state");
            }
        }

        if ("$state" eq "RUNNING") {
            LogMsg("Stopping $service...");
            `$stopcmd`;
        }    
		unless ("$state" eq "STOPPED") {
			sleep 6;
		}
        
    }
    if ($state eq "STOPPED") {
        LogMsg("$service has been stopped");
    } else {
        LogMsg("Failed to stop $service");
        return 0;        
    }
	return 1;	
}

sub reboot {
	LogMsg("rebooting here...");
    my $command="\"shutdown -r -f -t 5\"";                    
	system("$command");
}
 
sub get_user {
	my $requested_user=shift;
	if ($requested_user eq "HKEY_CURRENT_USER") {
		# This may seem silly but it allows us to use this function for any user including the current user
		return $requested_user;
	}
 
 	my $cmd="reg query \"HKEY_USERS\"";
    my @info=`$cmd`;  	
	my @users;
	my $uid='';
    if ($?) {
        LogMsg("Could not find HKEY_USERS in the registry ");  
       
    } else { 		
        foreach my $i (@info) {
			# Get a list of users
            chomp($i); 		
			my $l = length($i);			
			unless ($l < 20) {
				unless ($i =~ /_Classes/) {			
					if ($i =~ /S-1/) {			
						#print "Registry: ($i)\n";
						push(@users,$i);
					}
				}
			}
			if ($verbose) {
				#print "Registry: ($i)\n";
			} 
		}
    } 
 
	foreach my $u (@users) {	
	 
		my $tmp=find_user($u, "$requested_user");
		if ($tmp) {
			$uid=$tmp;			 			
		} 			
	}
	
	return $uid;
} 

sub find_user {
	my $u=shift;
	my $user=shift;
	my $cmd="reg query \"$u\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\"";	
	my @info=`$cmd`;  
	my $return_value=0;	 
 
	unless ($?) {
		foreach my $i (@info) {
			chomp($i);  						
			if (($i =~ /Logon User Name/) && ($i =~ /$user/i)) {		 
				$return_value=$u;
				return $return_value;
			} 
		}   
	} 
	unless ($return_value) {
		# Is Office Found?
		my $found_office=0;
		my $found_common=0;
	
		$cmd="reg query \"$u\\Software\\Microsoft\"";	
		@info=`$cmd`;  
		if ($?) {
			LogMsg("Failed to query $cmd");
		} else {
			foreach my $i (@info) {
				chomp($i);  							
				if ($i =~ /Office/i) {		 				
					$found_office=1;					
				} 
			}   
		} 			
		
		if ($found_office) {
			$cmd="reg query \"$u\\Software\\Microsoft\\Office\"";	
			@info=`$cmd`;  
			
			if ($?) {
				LogMsg("Failed to query $cmd");
			} else {
				foreach my $i (@info) {
					chomp($i);  							
					if ($i =~ /Common/) {	
						$found_common=1;	 										
					} 
				}   
			} 	
		}	
		
		if ($found_common) {
			$cmd="reg query \"$u\\Software\\Microsoft\\Office\\Common\\UserInfo\"";	
			@info=`$cmd`;  
			
			if ($?) {
				LogMsg("Failed to query $cmd");
			} else {
				foreach my $i (@info) {
					chomp($i);  							
					if (($i =~ /UserName/) && ($i =~ /$user/i)) {		 				
						$return_value=$u;
						return $return_value;
					} 
				}   
			} 	
		}
		unless ($return_value) {			
			$cmd="reg query \"$u\\Software\\Classes\\CLSID\" /s";
			@info=`$cmd`;  			
			if ($?) {
				#LogMsg("Failed to query $cmd");
			} else {
				foreach my $i (@info) {
					chomp($i);  				

					if (($i =~ /Default/) && ($i =~ /Users\\$user/i)) {		 										
						$return_value=$u;
						return $return_value;
					} 
				}   
			} 				
		}
		
	}
 
	return $return_value;
}


sub run_benchmarks {
	
	# Freq indicates how frequently the check must be run.  The value is how many days since the 
	# last successful execution before it must be executed again. 
 
	my $freq=30;
 
	# The check variable is the name of the check as recorded in the monitor_record.
	my $check="benchmark";
	my $warns=0;
	if ($monitor_record_hash{$check}) {
		my $last_pass=$monitor_record_hash{$check};
		my $days_ago = sprintf("%d",(($timeseconds - $last_pass)/86400));
		unless ((($timeseconds - $last_pass)/86400) > $freq) {
			# We do not have to run this test again 
			LogMsg("$check was run $days_ago days ago - Freq: $freq - Not checking");
			unless ($force) {return;}
		}
	}
	LogMsg("Running Benchmarks...");
	run_cpu_benchmark();
	run_hd_benchmark();

    if ($warns) {
        LogMsg(indent(1)."WARNING: $warns benchmark issues found");          
        $warning_count++;
    } else {
		$monitor_record_hash{$check}=$timeseconds;
	}		
}

sub run_cpu_benchmark {
	my $counter=0;
	my $limit = 50000;
	my $starttimeseconds=time();
	
	while ($counter < $limit)	{
		$counter++;
		for (my $x=0; $x<=1000; $x++) {
			my $tmp=sin($x/($x+2));
		}		
	}
	my $endtimeseconds=time();
	my $diff=($endtimeseconds - $starttimeseconds);
	LogMsg("System Info: CPU_Benchmark: $diff");	
}

sub run_hd_benchmark {
	my $counter=0;
	my $limit = 1000;
	my $starttimeseconds=time();
	my $file="c:/temp/hd_benchmark.txt";
	if (-f $file) {
		unlink $file;
	}
	my $cmd="echo \"1234567890\" >> $file";
	
	while ($counter < $limit)	{
		`$cmd`;
		$counter++;
	}
	if (-f $file) {
		unlink $file;
	}	
	my $endtimeseconds=time();
	my $diff=($endtimeseconds - $starttimeseconds);
	LogMsg("System Info: HD_Benchmark: $diff");	
	
}

sub save_record {
	if ($debug_mode) {
		return;
	}
	LogMsg("Saving monitor record...");
	# Save the results to the monitor record
	my $records=keys(%monitor_record_hash);
	unless ($records) {
		LogMsg("No records to update.");
		return;
	}
	if (open(REC,">$monitor_record")) {
		LogMsg("Updating $monitor_record");
		foreach my $key (sort keys(%monitor_record_hash)) {
			next unless ($key);
			if ($verbose) {
				print "Saving $key $monitor_record_hash{$key} in monitor record...\n";
			}
			print REC "$key $monitor_record_hash{$key}\n";
		}
	
	} else {
		LogMsg("WARNING: Failed to open $monitor_record");
		$warning_count++;
	}
	close REC;	
}


sub check_remote_app {
	 
    LogMsg("Checking Remote App Settings.");
	# With SAP POS 2.3, we are going to try running Store Manager on the Edna server with remote app on the workstation.  
	my $warns=0;
     
	# determine the version
	my $file1 = "c:\\\\windows\\\\system32\\\\mstsc.exe";
	my $file2 = "c:\\\\windows\\\\system32\\\\audiodg.exe";	
	my %file_hash = (
		'mstsc.exe' => $file1,
		'audiodg.exe' => $file2,		 
	);
	foreach my $f (sort(keys(%file_hash))) {
		my $filepath=$file_hash{$f};
		if (-f $filepath) {
			my $cmd="wmic datafile where Name=\"$filepath\" get Version";			 
			my @file_info=`$cmd`;
			foreach my $fi (@file_info) {
				#chomp($fi);				
				if ($fi =~ /\d/) {					
					next if ($fi =~ /Version/);					
					my @tmp=split(/\s+/,$fi);
					my $version=$tmp[0];					 
					LogMsg("System Info - $f version: $version");
				}
			} 
		}
	}	
	
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking remote app");
		$warning_count++;
	}
	return $warns;
}


sub sep_install {
	if ($OS eq "XP") {
		LogMsg("The sep_install function does not support $OS");
		return;
	}
	if (($no_action) || ($debug_mode)) {
		LogMsg("Skipping sep install as requested.");
		return 1;
	}
	 
	my $installer="//Edna/temp/install/SAVCorp/sep_ws_upgrade.pl"; 
    my $local_installer="c:/temp/scripts/sep_ws_upgrade.pl";

	my $failed_flag="c:/temp/sep12_install_failed.txt";
	my $success_flag="c:/temp/sep12_installed.txt";	
	
	if (-f $failed_flag) {
		LogMsg("WARNING: Detected the install has previously failed");
		return 0;
	}
	if (-f $success_flag) {
		LogMsg("WARNING: Detected the install has previously been completed.");
		return 0;
	}	
	# Check that BFE is running
	my $service="BFE";
	if (query_service($service)) {
		LogMsg(indent(1)."Found $service running");		
	} else {
		LogMsg(indent(1)."Starting $service");
		if (start_service($service)) {
			LogMsg(indent(2)."Started $service");	
		} else {
			LogMsg("WARNING: Failed to start $service");	
			return 0;
		}	
	}	
 		 
    if (-f $installer) {
		if ($verbose) {
			LogMsg(indent(1)."Found $installer");
		}
        # Copy the installer to the local installer
        copy("$installer","$local_installer");
 
    } else {
		if (-f $local_installer) {
			if ($verbose) {
				LogMsg(indent(1)."Found $local_installer");
			}		
		} else {
			LogMsg("Failed to locate $installer");
			return 0;
		}
    }
	if (-f $local_installer) {
        my $cmd="$local_installer";            
        LogMsg("Installing $local_installer...");
		if (-f $local_installer) {
			if ($verbose) {
				LogMsg(indent(1)."Found $local_installer");
			}
		} else {
			LogMsg(indent(1)."ERROR: Failed to locate $local_installer");
			return 0;
		}
        `$cmd`;
		
        # Check that it was indeed installed  
		if (-f $failed_flag) {
			LogMsg("WARNING: Detected the install has failed");
			return 0;
		}
		if (-f $success_flag) {
			LogMsg("Detected the install has been completed.");	 			
		}	 	
	}
		
    return 1;
}

sub check_net_install {
	my $flag="c:/temp/Net_install_request";	
	my $tool="C:/temp/scripts/net_ws_install.pl";
	my $warns=0;
	my $failed_flag="c:/temp/net_install_failed.txt";
	my $success_flag="c:/temp/net_installed.txt";	
	my $install_enabled = 0;
	if ($force) {
		$install_enabled = 1;
	}

	if (-f $failed_flag) {
		LogMsg("WARNING: Detected the .Net install has previously failed");
		return 1;
	}
	if (-f $success_flag) {
		LogMsg("WARNING: Detected the .Net install has previously been completed.");
		return 1;
	}		
	
	if (-f $flag) {
		LogMsg("Found request to install .Net 4.5.2");
		if (-f $tool) {
			LogMsg("Found $tool");
			unless ($install_enabled) {
				LogMsg("Net Installation is not currently enabled");
				return 0;
			}			
			my @response=`$tool`;
			foreach my $r (@response) {
				chomp($r);
				if ($r =~ /WARNING/) {
					my @tmp=split(/WARNING:/,$r);
					LogMsg("WARNING: $tmp[1]");
				}
				if ($r =~ /ERROR/) {
					my @tmp=split(/ERROR:/,$r);
					LogMsg("WARNING: $tmp[1]");
				}				
			}
			
			# Check that it was indeed installed  
			if (-f $failed_flag) {
				LogMsg("WARNING: Detected the Net install has failed");
				return 1;
			} elsif (-f $success_flag) {
				LogMsg("Detected the Net install has been completed.");	 			
			} else {
				LogMsg("WARNING: Net install has ended with unknown results");
				return 1;			
			}			
		} else {
			LogMsg("Failed to locate $tool");
			$warns++;
		}
		
		
	}	
	return $warns;
}

sub check_share {
	my $share=shift;
	if ($verbose) {
		LogMsg("Checking share $share_dir");
	}
	# Check share
	my $cmd="net share";
	my @info=`$cmd`;
	my $found_share=0;
	my $found_correct_share=0;
	my $found_share_everyone=0;
	foreach my $i (@info) {
		chomp($i);			
		if ($i =~ /^Share.*temp.share/i) {			
			$found_share=1;	
			if ($verbose) {
				LogMsg(indent(1)."Found $share_dir share: ($i)");
			}
		}		 
		if ($found_share) {
			# Check that the share is for everyone
			my $dir = $share_dir;
			$dir=~ s/\//\\/g;
			$cmd="cacls $dir";
			@info=`$cmd`;
			foreach my $i (@info) {
				chomp($i);
				if ($i =~ /everyone/i) {
					# Good, it is shared with everyone	
					$found_share_everyone=1;					
					if (($i =~ /CI/) && ($i =~ /F/)){
						# Good, it is share with change rights
						$found_correct_share=1;
					}
				}				 
			}
		}
	}
	if ($found_share) {
		if ($found_share_everyone) {
			unless ($found_correct_share) {
				LogMsg("WARNING: Found share to everyone but without the correct read/write permissions");
				$warning_count++;					
			}
		} else {
			LogMsg("WARNING: \\temp\\share is not shared to everyone");
			$warning_count++;		
		}	
	}
	unless ($found_share) {
		LogMsg("$share_dir is not currently shared.");
		
		my $share=$share_dir;
		$share=~ s/\//\\/g;
		LogMsg("Configure share $share...");
		my $cmd="net share Share=$share /GRANT:EVERYONE,CHANGE";
		print "$cmd\n";
		LogMsg("Sharing $share_dir");
		my @share_info=`$cmd`;    
		foreach my $s (@share_info) {
			chomp($s);
			LogMsg("$s"); 
		} 
		$cmd="net share";
		@info=`$cmd`;
		
		foreach my $i (@info) {
			chomp($i);			
			if ($i =~ /^Share.*temp.share/i) {
				
				$found_share=1;	
				if ($verbose) {
					LogMsg(indent(1)."Found $share_dir share: ($i)");
				}
			}		 
		}		
	}
	return $found_share;
}

sub cleanup {
	LogMsg("Beginning Cleanup...");
    my $warns=0;
	# If there are any files you need to delete
	my @unwanted_list = (
		"C:/Users/Villages/AppData/Roaming/NewspaperDirect/PressReader/log/nd.log",
		"C:/Users/Villages/AppData/Roaming/NewspaperDirect/PressReader/log",
		"C:/Users/Villages/AppData/Roaming/NewspaperDirect/PressReader",
		"C:/Users/Villages/AppData/Roaming/NewspaperDirect",
		"C:/Program Files (x86)/BonanzaDealsLive/CrashReports",
		"C:/Program Files (x86)/BonanzaDealsLive",
		"C:/Program Files (x86)/BonanzaDeals/uninst.exe",
		"C:/Program Files (x86)/BonanzaDeals",
		"C:/Users/Villages/AppData/Local/DCBC2A71-70D8-4DAN-EHR8-E0D61DEA3FDF.ini", 
		"C:/Users/Villages/AppData/Roaming/hpqLog/CaslInstallHelper.log",
		"C:/Users/Villages/AppData/Roaming/hpqLog",
		"C:/Users/Villages/AppData/Local/Deployment",
		"C:/ProgramData/Microsoft/Windows/Start Menu/Programs/Triversity Inc/Electronic Journal.lnk",
		"C:/ProgramData/Microsoft/Windows/Start Menu/Programs/Triversity Inc/POS-Store Manager.lnk",
		"C:/ProgramData/Microsoft/Windows/Start Menu/Programs/Triversity Inc/SQL Interactive.lnk",
		"C:/ProgramData/Microsoft/Windows/Start Menu/Programs/Triversity Inc/ODBC Administrator.lnk",
		
	);
	my %move_hash = (
		"C:/Users/Villages/AppData/Roaming/UserTile.png" => "C:/temp/UserTile.png",
	);
	foreach my $file (@unwanted_list) {
		if (-f $file) {
			LogMsg("Removing $file...");
			unless ($debug_mode) {
				unlink $file;
				if (-f $file) {
					LogMsg(indent(1)."WARNING: Could not remove $file");			 
					$warns++;
				} else {
					LogMsg(indent(1)."Successfully removed $file");
				}				
			} else {
				LogMsg("(Debug Mode)");
			}
		} elsif (-d $file) {
			# I don't like the idea of this script being able to remove directories so I am defeating this for now. - kdg
			LogMsg("Request to Remove directory $file...");
			unless ($debug_mode) {
				rmdir($file);
				sleep 5; 
				if (-d $file) {
					LogMsg(indent(1)."WARNING: Could not remove $file");
				
					$warns++;			
				} else {
					LogMsg(indent(1)."Successfully removed $file");
				}
			} else {
				LogMsg("(Debug Mode)");				
			}
		} else {
			if ($verbose) {
				print "Did not locate ($file)\n";
			}
		}		
	}	
	foreach my $file (sort keys(%move_hash)) {
		my $target=$move_hash{$file};
		LogMsg("Moving $file to $target");
		move($file,$target);
	}
}

sub set_start {
    my $service=shift;
    my $setting=shift;
 
    LogMsg("Setting $service to start to $setting...");
    my $cmd="sc config \"$service\" start= $setting";     
 
    my $start_type;
    my @info=`$cmd`;
    my $result=0;
 
    foreach my $i (@info) {        
        if ($i =~ /SUCCESS/) {
            $result=1;             
        }  
    }        
    return $result;
}

sub check_master_browser {
	LogMsg("Checking master browser...");
	my $master_browser=0;
	my @system_list=();
	if (-f $system_network_info) {
		open(INFO,"$system_network_info");
		my @info=(<INFO>);
		close INFO;
		foreach my $n (@info) {
			chomp($n);	 
			if ($n =~ /MSBROWSE/){				
				$master_browser=1;
			}		
		}
 	
	}
	
	if ($master_browser) {
		LogMsg("$hostname is the master browser");
	} else {
		LogMsg("$hostname does not appear to be a master browser");
	}
}
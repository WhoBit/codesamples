There are four Perl scripts here:
- monitor.pl - This script is used to monitor the servers in the Ten Thousand
Villages stores.  
- regmonitor.pl - This script is used to monitor the POS registers in the stores.
- wsmonitor.pl - This script is used to monitor the backoffice workstation in
company stores.
- processDailyReports.pl - This script is run in Akron to collect the results of 
the various monitor scripts.

There is also a folder: MonitorMailSamples
This folder contains three emails which represent the typical email reports
that the processDailyReports.pl script sends to inform us of status of store
systems.

These four script were created and maintained by me.  Until a few months ago,
one of my primary tasks was to monitor these reports each morning and address
concerns that they showed.  (These script still run but it is no longer my
duty to monitor the results.)

2016-04-06 - kdg


#!/bin/bash
# WireBackup.sh - updated from a version written by Raj 

# October 2008 Updates for the new Wire server
# - Updated dbnames, files, and directories to suit the new wire server - kdg
# - v2.0.0 - Tested on wired (test version of The Wire) system.
# - v2.0.1 - Using expect for the ftp backup - kdg
# - v2.0.2 - Put log file in /var/log - kdg
# - v2.0.3 - Removed call to cifsmnt and put mount command in this script - kdg
# - v2.0.4 - Adding mail and expectdmd - kdg
# - v2.0.5 - Adding encryption - kdg
# - v2.0.6 - Create tar with absolute path.  - kdg
# - v2.0.7 - Correct error with ftp when not in debug mode - kdg
# 2009-02-02 - v2.0.8 Cleaned up funcCleanupOldTobaFiles - kdg
## 2009-12-15 - v2.0.9 Setting max_allowed_packet to 16M - kdg
## 2010-02-22 - v2.0.10 - Added backup of basic config files to funcBackupCore - kdg
## 2010-02-23 - v2.0.11 - Added httpd.conf to funcBackupCore - kdg
## 2010-07-13 - v2.1.0 - Added FilesCoreBakToKeep variable to keep DBFilesCore backups from being deleted prematurely.  Modified logging- kdg
## 2010-07-19 - v2.1.1 - Changed WARNING about Toba still being mounted to NOTE - kdg
## 2010-08-03 - v2.1.2 - Added some checking of the tarball & modified some reporting - kdg
## 2011-06-07 - v2.2.0 - Revised funcCleanupOldTobaFiles and renamed the variables trying to improve our backups - kdg
## 2011-06-20 - v2.2.1 - Some adjustments to funcCleanupOldTobaFiles and how long to keep backup files - kdg
## 2011-06-30 - v2.2.2 - Disabled encryption - kdg
## 2011-08-24 - v2.2.3 - Redirecting error output of tar to /dev/null just to avoid getting emails - kdg
## 2011-09-09 - v2.3.0 - Changed the backup of webfiles to be full on Sunday and differential on other days.  Corrected some errors. - kdg
## 2015-07-31 - v2.4.0 - Defined appname.  Revised email method - kdg
#
# version: 2.4.0

backuphost="****"
backupshare="//****/****"

##############
## Functions:
##############

param_check()
	{
	while getopts cdDfFhNtvn: option
	do
		case "$option"
		in
            
            d) # Backup only the database (default)
            backuptype[0]=1 
            echo "Backup the database"          
            ;;
            f) # Backup the files folder
            backuptype[1]=1 
            echo "Backup the files folder"
            ;;
            c) # Backup only the core files
            backuptype[2]=1 
            echo "Backup the core files"
            ;;
            D) # Debug mode - Print to screen as well as to log
            debug=1;
            echo "Debug mode"
            ;;            
            F) # Send files to remote site.
            backuptype[3]=1 
            echo "Send the backup to remote site via FTP"
            ;;
            n) # Name the file
            tarnamebase=$OPTARG
            echo "FTP $tarnamebase to remote site"
            ;;
            N) # Create a tarbackup but leave it in $tmp
            remote_save=no
            echo "No remote save"
            ;;
            t) # Set the test mode variable
            test_mode=yes
            echo "Testing mode"
            ;;		
			h|*)
			funcPrintUsage
			exit
			;;
		esac
	done
	if [ "$(expr ${backuptype[0]} + ${backuptype[1]} + ${backuptype[2]})" -eq "0" ]
	then echo "Database backup only"
        backuptype[0]=1 	       
	fi   
    # Determine the name of the backup file
    if [ "${backuptype[0]}" == 1 ]
    then
            backupfilename=${backupfilename}DB 
    fi
    if [ "${backuptype[1]}" == 1 ]
    then
            backupfilename=${backupfilename}Files
    fi    
    if [ "${backuptype[2]}" == 1 ]
    then
            backupfilename=${backupfilename}Core
    fi    
    
    tarname=$tarnamebase$backupfilename-$datestamp.tgz
    tarname_enc=$tarnamebase$backupfilename-$datestamp.encrypted
    ftptarname=$tarnamebase$backupfilename-$ftpdatestamp.tgz
 
}

funcBackupDB() {
	tarincludes=$tarincludes' dbcontent.sql'	
	# Database connection information
	#dbname="thewire" # (e.g.: dbname=drupaldb)
    dbname="****" 
	dbhost="****"
	dbuser="****" # (e.g.: dbuser=drupaluser)
	dbpw="****" # (e.g.: dbuser password)
	dbuser="****"
	dbpw="****"
	
	pdebug "  Dumping drupal database:"  
	pdebug "  user:$dbuser; database:$dbname host:$dbhost "  
	cd $TMP
	mysqldump --max_allowed_packet=16M --user=$dbuser --password=$dbpw --add-drop-table $dbname > dbcontent.sql
}

funcBackupFiles() {
	tarincludes=$tarincludes' userfiles.tar'	
	if [ "$test_mode" = "yes" ]
	
	then
			pdebug "  TARing all user website files from $webfilesdir ..."  			
			touch $TMP/webfiles_timestamp
			tar cvf $TMP/userfiles.tar $webfilesdir 2> /dev/null	
	else 
		# Determine which day of the week it is
		set $(date)
		###
		if test "$1" = "Sun" 
		then
			# weekly a full backup of all files 	
			pdebug "  TARing all user website files from $webfilesdir ..."  
			# cd $webfilesdir
			touch $TMP/webfiles_timestamp
			tar cf $TMP/userfiles.tar $webfilesdir 2> /dev/null

		else
			# a differential backup:
			pdebug "  TARing all user website files from $webfilesdir that changed ..."  
			if [ -f $TMP/webfiles_timestamp ]
			then
				cd $webfilesdir
				find . -newer $TMP/webfiles_timestamp -print > $TMP/backup_list
				tar cfT $TMP/userfiles.tar $TMP/backup_list 2> /dev/null
			else
				# default to a full backup of all files 	
				pdebug "  TARing all user website files from $webfilesdir ..."  
				touch $TMP/webfiles_timestamp
				tar cf $TMP/userfiles.tar $webfilesdir 2> /dev/null		
			fi
		fi	
		###	
	fi

}

funcBackupCore() {
    # Backup some of the basic config files - kdg
    tarincludes=$tarincludes' configfiles.tar'
	pdebug "  TARing system config files ..."  
    FILE_LIST="$TMP/configfiles.tar /root/sysinfo.txt /etc/php.ini /etc/my.cnf /etc/passwd /etc/httpd/conf/httpd.conf"
    CONFIG_FILE_LIST=""
    for file in $FILE_LIST
		do
		  if [ -f $file ]
		  then
            CONFIG_FILE_LIST="$CONFIG_FILE_LIST $file"
			pdebug "adding $file to $CONFIG_FILE_LIST"	
          else
            pdebug "skipping $file"	
		  fi
        done
    tar cf $CONFIG_FILE_LIST 2> /dev/null
    
	tarincludes=$tarincludes' corefiles.tar'
	pdebug "  TARing core wire files from $webrootdir ..."  
	# cd $webrootdir
	sleep 10
    tar cf $TMP/corefiles.tar $webrootdir --exclude=files --exclude=product_images 2> /dev/null
	sleep 15
}

funcPrintUsage() {
	echo ""	
	echo "Usage: bash WireBackup.sh [OPTION SWITCHES]... filename"
	echo "Example call: bash WireBackup.sh -d -f -F mybackup"
	echo ""	
	echo "-d             Perform database backup (Default)"
	echo "-f             Perform user file backup"
	echo "-c             Perform core file backup"	
	echo "-F             Send backup to hosting site"
    echo "-n <filename>  Specify the filename to use.  The default"
    echo "               filename is $tarnamebase. The date stamp will be "
	echo "               appended to the filename in any case."	
	echo "-N             Create a tar file but leave it in $TMP."
    echo "-D             Enable debug mode which will display information"
    echo "               which typically goes only to the log file."
	echo "-t             Set the test mode flag.  For testing only."
	echo ""
	exit 0 
}

funcDelFiles() {
	numfiles="$1"
	keepcnt="$2"
	arrayname=( `echo "$3"` )
    
	if [ "$keepcnt" -lt "$numfiles" ]
	then  
		for ((i=keepcnt;i<numfiles;i+=1)); do
		pdebug "  Removing file $bkupdir/${arrayname[i]} ..."  
		rm $bkupdir/${arrayname[i]}
		done
	fi
}

funcCleanupOldTobaFiles_beta() {

    counter=0;
    for file in `ls -rt ${bkupdir}/${tarnamebase}DB*`
    do
        let "counter++"
        if [ $count > $DbBakToKeep ]
        then
            pdebug "Time to delete $file"
        fi
    done
    pdebug "Found $counter DB files"
    counter=0;
    for file in `ls -rt ${bkupdir}/${tarnamebase}*Core*`
    do
        let "counter++"
        if [ $count > $CoreBakToKeep ]
        then
            pdebug "Time to delete $file"
        fi
    done    
    pdebug "Found $counter Core files"
    counter=0;
    for file in `ls -rt ${bkupdir}/${tarnamebase}*Files*`
    do
        let "counter++"
        if [ $count > $FilesBakToKeep ]
        then
            pdebug "Time to delete $file"
        fi
    done    
    pdebug "Found $counter Files files"    
}
 

funcCleanupOldFiles() {
    counter=0;
    for file in `ls -t ${bkupdir}/${tarnamebase}DB-* 2> /dev/null`
    do
        let "counter++"        
        if [ "$counter" -gt "$DbToKeep" ]
        then            
            pdebug "Cleaning up old backups: removing $file"
            rm $file
        fi
    done        
    pdebug "Found $counter DB backups"
    counter=0;    
    for file in `ls -t ${bkupdir}/${tarnamebase}DBFiles-* 2> /dev/null`
    do
        let "counter++"       
        if [ "$counter" -gt "$DBFilesToKeep" ]
        then       
            pdebug "Cleaning up old backups: removing $file"
            rm $file
        fi
    done    
    pdebug "Found $counter DBFiles backups"      
    counter=0;
    for file in `ls -t ${bkupdir}/${tarnamebase}Core-* 2> /dev/null`
    do
        let "counter++"       
        if [ "$counter" -gt "$CoreToKeep" ]
        then            
            pdebug "Cleaning up old backups: removing $file"
            rm $file
        fi
    done    
    pdebug "Found $counter Core backups"        
    counter=0;    
    for file in `ls -t ${bkupdir}/${tarnamebase}DBCore-* 2> /dev/null`
    do
        let "counter++"       
        if [ "$counter" -gt "$DBCoreToKeep" ]
        then       
            pdebug "Cleaning up old backups: removing $file"
            rm $file
        fi
    done    
    pdebug "Found $counter DBCore backups"  
    counter=0;    
    for file in `ls -t ${bkupdir}/${tarnamebase}DBFilesCore-* 2> /dev/null`
    do
        let "counter++"       
        if [ "$counter" -gt "$DBFilesCoreToKeep" ]
        then       
            pdebug "Cleaning up old backups: removing $file"
            rm $file
        fi
    done    
    pdebug "Found $counter DBFilesCore backups"     
}


pdebug() {
	# Print debug statment to log
	#CURRENT_DATE="`date '+%F %r'` $$"
    CURRENT_DATE=`date`
	if [ $debug == 1 ]
	then
		echo "$CURRENT_DATE: $1" | tee -a $logfile
	else
		echo "$CURRENT_DATE: $1" >> $logfile
	fi
}

mount_share() {
   #
   # Check that $backuphost is mounted because we send files there
   #

   mountinfo=`mount`
   rmtmounted='no'

   if (mount | grep $backuphost)
   then
      pdebug "$backuphost is already mounted"
      rmtmounted='yes'
   else
      pdebug "Mounting share $backupshare from $backuphost..."
      if (/sbin/mount.cifs $backupshare /mnt/$backuphost -o user=thewire%iZAJ4fuN >> $logfile 2>&1)
      then
         pdebug "Checking mount..."
         if (mount | grep $backuphost)
         then
             pdebug "$backuphost mounted successfully"
             rmtmounted='yes'
         else
             pdebug "Mount appears to have failed"
         fi
      else
         pdebug "Error trying mount share!"
      fi
   fi

   if [ "$rmtmounted" = "no" ]
   then
      pdebug "ERROR! $backuphost is not mounted!"
      if [ "${backuptype[3]}" -eq "1" ]
      then
         pdebug "Doing backups to FTP site but not $backuphost"
      else
         pdebug "Cannot write backups to $backuphost..."
         pdebug "aborting..."
		 # Configure the email message
		 message="ERROR! $backuphost is not mounted!  Cannot write backups to $backuphost....aborting..."
		 subject="$appname Backup Error"
		 # Send the email
		 echo "$message" | mail -s "$subject" $adminadd		 
         #$expectcmd $mailscript "$appname Backup Error" $adminadd
         exit
      fi
   fi
}

###############
## Variables
###############
#
# RGI note - all variables are global in BASH scripting unless specifically declared local!

date=`date`
# Debug mode - No backups sent to remote site.
debug=0

# Default TAR Output File Base Name
tarnamebase=thewirebackup- #(can be overriden with command line argument)
datestamp=`date +'%Y%m%d.%H%M'`
day=`date +%d`
# Based on the day of the month, we create a datestamp.
# This is used to name the backup file.  Dividing the day by 5 
# will give us 6 different backups.  After the 6th backup, the
# datestamp will start over at 1 and will overwrite the previous
# backup with this datestamp.    The drawback is that you will
# need to look at the actual date of the file to determine when
# it was created but this makes naming backups and cleaning old
# backups easier. 
ftpdatestamp=`expr $day / 5`

# Execution directory (script start point)
startdir=`pwd`

# file path and name of log file to use
#logfile="/var/www/wire08/wire/utilities/WireBackup.log"
loghistoryfile="/var/log/wirebackup.log"
logfile="/var/log/lastwirebackup.log"

# We need a general temporary folder
# The backup tar file(s) will be initially created here but
# eventually, they all get put together in the final tarfile.
TMP="/tmp"
 
# Backup directory - where we send the files
bkupdir="/mnt/$backuphost"

# backup switch array - [db,files,core,ftp?]
#'1' in any position means we do that kind of backup...
backuptype[0]=0
backuptype[1]=0
backuptype[2]=0
backuptype[3]=0

# backup filenames array - [db,files,core]
#backupfilename[0]="theWireDbBackup"
#backupfilename[1]="theWireFilesBackup"
#backupfilename[2]="theWireCoreBackup"
backupfilename=""
#backupfilename[0]="testDbBackup"
#backupfilename[1]="testFilesBackup"
#backupfilename[2]="testCoreBackup"

# number of backups to keep for each backup type


# This is done every 3 hours through the day
DbToKeep=10
# This is done every morning Monday through Saturday
DBFilesToKeep=7
# This is done every morning Monday through Saturday
CoreToKeep=7
# This is done every Sunday Morning
DBCoreToKeep=7
# This is done once a month 
DBFilesCoreToKeep=4


# string containing names of files to include in the final tar file
tarincludes=""

# Website Files
#webrootdir="/var/www/villagewire" # (e.g.: webrootdir=/home/user/public_html)
#webfilesdir="/var/www/villagewire/files"
webrootdir="/var/www/wire08" # (e.g.: webrootdir=/home/user/public_html)
webfilesdir="/var/www/wire08/sites/default/files"

# There is a separate expect script for doing an FTP backup to AN Hosting (offsite backup location)
#FTPbackup="/var/www/wire08/wire/utilities/WireBackup.pl"
FTPbackup="/root/utilities/ftpbackup.exp";
pass="thisisthedaytheLordhasmade"
encrypt="/usr/bin/openssl des -salt -k $pass"

# An expect script to send email in case of failure
mailscript="/root/utilities/mail.exp";
expectcmd="/usr/bin/expect"

# Email address to send notifications to
adminadd="kdg@tenthousandvillages.com"

# Name of this application
appname="WireFilesBackup.sh"

# Specify whether we want to save the backup to someplace other than this system
remote_save=yes

# Test mode variable 
test_mode=no
 

###############
## Main
###############

#
# Begin logging
#

param_check $*
# Initialize the log file
date > $logfile
pdebug "Beginning drupal site backup using WireBackup.sh ..."  
pdebug "Arguments: $*"  
#funcGetParams `echo $@`

#
# Create the staging directory if it does not exist
#
if [ -d $TMP ]
then
    pdebug "Found $TMP" 
else
    pdebug "Creating the TMP dir ..."  
    mkdir $TMP
fi

if [ $remote_save == "yes" ]
then
   # Mount a share on $backuphost if we are doing remote saves
   mount_share
fi    


#
# The backups are created here
#
pdebug "Creating backups..."
if test "${backuptype[0]}" -eq "1"
then funcBackupDB 
fi
if test "${backuptype[1]}" -eq "1"
then funcBackupFiles 
fi
if test "${backuptype[2]}" -eq "1"
then funcBackupCore 
fi

#
# Create final backup file
#
pdebug "Creating final compressed (tgz) TAR file: $tarname ..."  
cd $TMP
tar czf $tarname $tarincludes

#
# Check the final backup file
#
pdebug "Checking final compressed (tgz) TAR file: $tarname ..."  
cd $TMP
if (tar tzf $tarname) 
then
    pdebug "Tar file passes basic check"
else
    pdebug "ERROR: Tar file FAILS basic check"
	# Configure the email message
	message="Checking final compressed (tgz) TAR file: $tarname ...ERROR: Tar file FAILS basic check"
	subject="$appname Wire Backup Error"
	# Send the email
	echo "$message" | mail -s "$subject" $adminadd		
    #$expectcmd $mailscript "Wire Backup Error - Tar file FAILS basic check" $adminadd
fi

if [ $remote_save == "no" ]
then
    pdebug "Exiting without remote save - no cleanup"
    exit
fi    

#
# Copy the tarfile to the backup server
#

if [ "$rmtmounted" = "yes" ]
then
    if [ -f "$TMP/$tarname" ]
    then
        pdebug "Copying $tarname to $bkupdir"
        cp $TMP/$tarname $bkupdir
    else 
        pdebug "Error:  Cannot find $tarname in $TMP!"
        pdebug "aborting..."
		# Configure the email message
		message="Error:  Cannot find $tarname in $TMP!  aborting..."
		subject="$appname Wire Backup Error"
		# Send the email
		echo "$message" | mail -s "$subject" $adminadd			
        #$expectcmd $mailscript "Wire Backup Error - Cannot find $tarname in $TMP! " $adminadd        
        exit;
    fi
else
    pdebug "Not copying to $backuphost because it is not mounted"
fi


#
# Copy the tarfile to the offsite server (if requested)
#

if test "${backuptype[3]}" -eq "1"
then
    if [ -f $TMP/$tarname ]
    then
        cd $TMP
        #pdebug "Copying $tarname to $ftptarname"
        #cp $TMP/$tarname $TMP/$ftptarname
        # Now encrypt the tarball which gets sent offsite
        # 2011-06-30 - We now FTP it to a local system so I am not concerned about encrypting it - kdg
        #pdebug "Encrypting $tarname..."
        #$encrypt -in $tarname -out $tarname_enc
        if [ -f $FTPbackup ]
        then
            pdebug "FTPing $TMP/$ftptarname"
            if [ $debug == 1 ]
            then                
                #$expectcmd $FTPbackup $tarname_enc $ftptarname | tee -a $logfile
                $expectcmd $FTPbackup $tarname $ftptarname | tee -a $logfile
            else
                #$expectcmd $FTPbackup $tarname_enc $ftptarname  >> $logfile
                $expectcmd $FTPbackup $tarname $ftptarname  >> $logfile
            fi
            
        else
            pdebug "ERROR! Could not locate $FTPbackup"
            pdebug "aborting..."
			# Configure the email message
			message="ERROR! Could not locate $FTPbackup....aborting..."
			subject="$appname Wire Backup Error"
			# Send the email
			echo "$message" | mail -s "$subject" $adminadd			
            #$expectcmd $mailscript "Wire Backup Error" $adminadd            
            exit;
        fi
    else
        pdebug "ERROR! Could not locate $tarname in $TMP"
    fi

fi

#
# Cleanup
#
pdebug "Removing files from temp dir $TMP ..."  

# Being a bit careful to only delete tar and sql files we created.  List all of the files you wish to cleanup
remove_list="corefiles.tar userfiles.tar dbcontent.sql $tarname $ftptarname $tarname_enc"

for found_file in `ls $TMP`
do
    for remove_file in $remove_list
    do
        if [ "$found_file" = "$remove_file" ]
        then
            pdebug "Cleaning up $found_file"
            rm -f $TMP/$found_file
        fi
    done    
done


if [ "$rmtmounted" = "yes" ]
then
   funcCleanupOldFiles

   pdebug "Unmounting share on backup server"
   umount /mnt/$backuphost
   if (mount | grep $backuphost)
   then
      pdebug "WARNING: $backuphost is still mounted"
   else
      pdebug "$backuphost successfully unmounted"
   fi
fi    
#
# Exit banner
#
endtime=`date`
pdebug "Backup completed, TAR file: $tarname. "  
pdebug ""

#
#   Save the daily log file to the log history file
#
cat $logfile >> $loghistoryfile

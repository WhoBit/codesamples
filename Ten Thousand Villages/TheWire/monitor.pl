#! /bin/perl

##  monitor.pl
## Script to alert if The Wire encounters issues
## 2010-01-25 - Created - kdg
## 2010-01-27 - v1.0.1 - Added check_file_folder - kdg
## 2010-01-29 - v1.0.2 - Added check_system_logs - kdg
## 2010-02-02 - v1.0.3 - Mailfrom now uses Linux-Monitor - kdg
## 2010-02-03 - v1.0.4 - Corrections to check_system_logs.  Added check_selinux. Added saz - kdg
## 2010-02-04 - v1.0.5 - Added full path to getenforce - kdg
## 2010-02-18 - v1.0.6 - Added check_manifest - kdg
## 2010-02-19 - v1.0.7 - Revisions to check_manifest - kdg
## 2010-02-22 - v1.0.8 - Added collect_sysinfo - kdg
## 2010-02-26 - v1.0.9 - Added reporting to email in check_manifest - kdg
## 2010-03-15 - v1.0.10 - Added uptime - kdg
## 2010-03-22 - v1.0.11 - Added wire_specifics - kdg
## 2010-03-29 - v1.0.12 - Added check_wire_changes - kdg
## 2010-03-30 - v1.0.13 - Added email notes to check_wire_changes - kdg
## 2010-03-31 - v1.0.14 - Added @ignore_variables and some formatting updates - kdg
## 2010-04-06 - v1.0.15 - Include previous and new variable settings in email - kdg
## 2010-04-08 - v1.0.16 - Updated ignore_variables - kdg
## 2010-05-13 - v1.0.17 - Added check_jpg() - kdg
## 2010-05-17 - v1.0.18 - Updated reporting on check_jpg & Added check_missing_jpg - kdg
## 2010-06-23 - v1.0.19 - Added check_wire_db - kdg
## 2010-07-12 - v1.0.20 - Some cleanup on check_wire_db - kdg
## 2010-07-13 - v1.0.21 - Added check_wire_backup - kdg
## 2010-08-05 - v1.0.22 - Added check_search_index (for what it is worth) - kdg
## 2010-08-11 - v1.0.23 - Added provision for checking wire and server manifests separately - kdg
## 2010-09-07 - v1.0.24 - Modifield check_catalog_sync to check both field and item sync - kdg
## 2010-09-10 - v1.0.25 - Added check_raid - kdg
## 2010-09-21 - v1.0.26 - Added check_festivalsales - kdg
## 2010-10-05 - v1.0.27 - Added check_wire_orders - kdg
## 2010-10-28 - v1.0.28 - Added check_users - kdg
## 2010-11-08 - v1.0.29 - Changed warnings about images to NOTICE - kdg
## 2010-11-08 - v1.1.0 - Added lamp_specifics - kdg
## 2010-11-16 - v1.1.1 - Added check_backups for both The Wire and LAMP1 - kdg
## 2010-11-22 - v1.1.2 - Corrections to check_backup finding correct date - kdg
## 2010-12-27 - v1.1.3 - Don't check for backups on ttv-lamp1 on Sunday.  Some reporting tweaks - kdg
## 2011-01-28 - v1.1.4 - Added itwire_specifics - kdg
## 2011-03-06 - v1.1.5 - Added check_mail - kdg
## 2011-03-08 - v1.1.6 - Improved reporting for check_mail - kdg
## 2011-03-10 - v1.1.7 - Added check_mail_sent - kdg
## 2011-03-19 - v1.1.8 - Some revisions to check_web_access - kdg
## 2011-03-21 - v1.1.9 - Added check_wire_watchdog - kdg
## 2011-04-04 - v1.1.10 - Defeated check_wire_watchdog from generating a warning since there are so many right now - kdg
## 2011-04-13 - v1.1.11 - Various adjustments to logging and running updates on itwire - kdg
## 2011-06-21 - v1.1.12 - Added secure_log_hash to check_system_logs function - kdg
## 2011-07-27 - v1.1.13 - Updated check_wire_db to check for URLs - kdg
## 2011-07-28 - v1.1.14 - Updated check_wire_db to fix URLs with -fix argument - kdg
## 2011-08-11 - v1.1.15 - Updated check_raid - kdg
## 2011-08-17 - v1.1.16 - Updated check_raid to save previous record.  Updated check_orders to better check status - kdg
## 2011-08-24 - v1.2.0 - Added check_sar - kdg
## 2011-09-02 - v1.2.1 - Tried to make warning handling a bit more consistent - kdg
## 2011-09-06 - v1.2.2 - Currected date formatting in check_sar - kdg
## 2011-10-07 - v1.2.3 - Small adjustment to check_backups to detect backups starting in WireFilesBackup.sh - kdg
## 2011-10-17 - v1.2.4 - Revised evaluating firmware state in check_raid - kdg
## 2011-11-03 - v1.2.5 - Some revisions to checking secure logs and checking raid - kdg
## 2011-12-07 - v1.2.6 - Added check_current_version - kdg
## 2011-12-14 - v1.2.7 - Added enable_updates_hash and some info about ethernet driver & adapter - kdg
## 2011-12-16 - v1.2.8 - Using @summary - kdg
## 2011-12-28 - v1.2.9 - Gave path to lspci - kdg
## 2012-01-25 - v1.2.10 - Added check_memory - kdg
## 2012-02-16 - v1.2.11 - Enabled updates for The Wire - kdg
## 2012-05-08 - v1.2.12 - Added warning about throttle being too small in catalog sync - kdg
## 2012-08-24 - v1.2.13 - Updated check_wire_orders to handle status of shipped - kdg
## 2012-10-23 - v1.3.0 - Updated check_catalog_sync to publish SKUs that should be published - kdg
## 2012-10-24 - v1.3.1 - Added check_user_sync - kdg
## 2012-11-12 - v1.3.2 - Added ip_update - kdg
## 2012-11-14 - v1.3.3 - Modified ip_update to capture the time that the store updated The Wire - kdg
## 2013-02-13 - v1.3.4 - Added auto logout settings check in check_users - kdg
## 2013-02-25 - v1.3.5 - Added check_catalog_fields - kdg
## 2013-05-08 - v1.3.6 - Added option to check RAID - kdg
## 2013-06-11 - v1.3.7 - Added $allowed to check_users - kdg
## 2013-09-05 - v1.3.8 - Modified check_search_index to show actual items not indexed - kdg
## 2013-09-05 - v1.3.9 - Added $database, $user, and $pw variables - kdg
## 2013-09-20 - v1.3.10 - Re-enabled mysqlcheck - kdg
## 2013-09-23 - v1.3.11 - Exclude thumbs.db checking manifest - kdg
## 2014-06-11 - v1.3.12 - Added detecting basic backup test in the check_backup function - kdg
## 2014-07-15 - v1.4.0 - Added check_indexed_items - kdg
## 2015-01-21 - v1.4.1 - Revised some reporting in check_raid - kdg
## 2015-03-02 - v1.4.2 - Send email regarding Abra to Leonard - kdg
## 2015-03-04 - v1.4.3 - Relabelled email to Leonard as Abra Monitor rather than Monitor - kdg
## 2015-03-13 - v1.4.4 - Added check_images - kdg
## 2015-03-16 - v1.4.5 - Reformatted check_images - kdg
## 2015-03-18 - v1.4.6 - Retired check_missing_jpg - kdg
## 2015-03-30 - v1.4.7 - Updated check_wire_watchdog to list new users created - kdg
## 2015-04-14 - v1.4.8 - Small correction to listing new users.  Added image results to summary - kdg
## 2015-04-28 - v1.4.9 - Updated check users to allow "new store interest group" - kdg
## 2015-07-31 - v1.4.10 - Updated check_backup to check connectivity to backup host - kdg
## 2015-08-05 - v1.4.11 - Disabled ping_test - kdg
## 2015-09-11 - v1.4.12 - Updated check_wire_changes to ignore cron_semiphore - kdg
## 2016-02-22 - v1.4.13 - Revised check_catalog_sync - kdg


##############
## Variables etc.
##############
use constant SCRIPT_VERSION 	=> "1.4.13";
use strict;
use Net::SMTP;
use Getopt::Long;
use Cwd;
use DBI;
use File::Basename;
use File::Copy;

my @tmp;
my @email;

my @summary;

my $send_email=1;
my $date=`date`;
my $error_log="/etc/httpd/logs/error_log";
my @ltm=localtime();
my $hour=$ltm[2];
my $day_of_week=$ltm[6];
my $orig_dir=cwd();
my $log="$orig_dir/monitor.log";
my $hostname=`hostname`;
@tmp=split(/\./,$hostname);
$hostname=$tmp[0];
my $verbose=0;
my $debug=0;
my $proto=0;
my $test_mode=0;
my $warning_count=0;
my $check_manifest=0;
my $default_checks=1;
my $sysinfo="/root/sysinfo.txt";
my $sysinfo_only=0;
my $mail_only=0;
my $help=0;
my $resize=0;
my $jpg=0;
my $thumb=0;
my $large=0;
my $pause=0;
my $skujpg=0;
my $users=0;
my $updates=0;
my $enable_updates=0;
my $itwire_only=0;
my $lamp_only=0;
my $wire_only=0;
my $specified_mail_log;
my $all_dates=0;
my $current_date=0;
my $all_logs=0;
my $log_only=0;
my $ip_update_only=0;
my $fix=0;
my $raid=0;
my $web_check=0;
my $detailed_report=0;
my $database="wire";
my $user="wiretestu";
my $pw="wiretestupww";

 
my $wire_db=0;
# Defaults
my %enable_updates_hash = (
	'thewire3' => '1',
	'itwire2' => '1',
	'itwire' => '1'
);
#SMTP vars

my $Mailserver = 'exchange.tenthousandvillages.com';
my $Mailfrom = 'Linux-Monitor@tenthousandvillages.com';
my $Mailto = 'kdg@tenthousandvillages.com';
my $Mailcc; 

 
##############
## MAIN
##############

GetOptions(
	"verbose"=>\$verbose,
    "debug"=>\$debug,
    "man"=>\$check_manifest,
    "sys"=>\$sysinfo_only,
    "test"=>\$test_mode,
    "help"=>\$help,
    "proto"=>\$proto,
    "resize"=>\$resize,
    "jpg"=>\$jpg,
    "thumb"=>\$thumb,
    "large"=>\$large,
    "pause"=>\$pause,
    "skujpg"=>\$skujpg,
    "users"=>\$users,
    "updates"=>\$updates,
    "mail"=>\$mail_only,
    "lamp"=>\$lamp_only,
    "mlog=s"=>\$specified_mail_log,
    "alldates"=>\$all_dates,
    "current"=>\$current_date,
    "alllogs"=>\$all_logs,
    "wire"=>\$wire_only,
    "itwire"=>\$itwire_only,
    "log"=>\$log_only,
	"ip_update"=>\$ip_update_only,
    "fix"=>\$fix,
    "pause"=>\$pause,
    "db"=>\$wire_db,
	"raid"=>\$raid,
	"database:s"=>\$database,
    "web"=>\$web_check,
    "detail"=>\$detailed_report
    );
    
if ($help) {
    usage();
    exit;
}    

    
LogMsg("$0 version: ".SCRIPT_VERSION);
push(@email,"$0 version: ".SCRIPT_VERSION);

if ($database eq "wirekdg") {
	$user="root";
	$pw="";
}

if ($check_manifest) {
    my $wire="/var/www/****";
    my $manifest="/root/wire_manifest.txt";
    check_manifest("$wire","$manifest");
    $manifest="/root/server_manifest.txt";
    check_manifest("/root","$manifest");     
    $default_checks=0;  # Skip the other checks
}

if ($sysinfo_only) {
    collect_sysinfo();
    $default_checks=0;  # Skip the other checks
}
 
if ($proto) {
    #$debug=1;
    $test_mode=1;
    $verbose=1; 
	#check_current_version();
	#check_backups();
	#check_wire_changes();
	check_catalog_sync();
	#check_wire_watchdog();
	
	#check_images();
	#check_thumb_size();
	#check_user_sync(); 	
	#check_indexed_items();
	#check_search_index();   
 
    $default_checks=0;  # Skip the other checks    
}
if ($raid) {
    $debug=1;
    $test_mode=1;
    $verbose=1; 	
	check_raid();
    $default_checks=0;  # Skip the other checks    
}
if ($wire_db) {
    $debug=1;
    $test_mode=1;
    $verbose=1;
    check_wire_db();
    $default_checks=0;  # Skip the other checks    
}
if ($jpg) {
    $debug=1;
    $test_mode=1;
    $verbose=1;
    check_jpg();
    $default_checks=0;  # Skip the other checks
}
if ($skujpg) {
    $debug=1;
    $test_mode=1;
    $verbose=1;
    check_missing_jpg();
    $default_checks=0;  # Skip the other checks
}
if ($users) {
    $verbose=1;
    check_users();
    $default_checks=0;  # Skip the other checks
}        
if ($updates) {
    $verbose=1;
    check_updates();
    $default_checks=0;  # Skip the other checks
}   
if ($itwire_only) {    
    itwire_specifics();
    $default_checks=0;  # Skip the other checks    
}
if ($lamp_only) {
    lamp_specifics();
    $default_checks=0;  # Skip the other checks    
}
if ($wire_only) {
    wire_specifics();
    $default_checks=0;  # Skip the other checks    
}
if ($mail_only) {
    check_mail();
    $default_checks=0;  # Skip the other checks    
}
if ($log_only) {
    check_wire_watchdog();
    $default_checks=0;  # Skip the other checks    
}
if ($ip_update_only) {
    ip_update();
	LogMsg("$0 finished ");	
	exit;     
}
if ($web_check) {
    check_web_access();
	LogMsg("$0 finished ");	
	exit;     
}
if ($default_checks) {
    if ($hostname =~ /itwire/)  {    
        check_uptime();   
        #$enable_updates=1;    
        check_updates();    
        #check_selinux();
        check_web_access();
        check_system_logs();
        collect_sysinfo();   

        $default_checks=0;  # Skip the other checks
    } 
}
if ($default_checks) { 
    check_uptime();       
    check_updates();    
    check_selinux();
    check_web_access();
    check_system_logs();
    collect_sysinfo();    
    check_backups();
    wire_specifics();
    check_mail();
    lamp_specifics();
	check_sar();
}    

LogMsg("There were $warning_count warnings");
push(@email,"\nThere were $warning_count warnings");

if ($debug) {
	print "Email \n";
	foreach my $s (@summary) {
		print "Summary: $s";
	}
	print "==========================\n";
	foreach my $e (@email) {
		print "$e";
	}
	print "\n";
} else {
    if ($send_email) {
        mailReport("$hostname: Monitor",\@summary,\@email);
    }
}
LogMsg("$0 finished ");

##############
## Functions
##############
sub usage {
    print "\nmonitor.pl\n";
    print "Monitor is typically run by cron.  It is used to perform daily checks of the server\n";
    print "\nUSAGE: monitor.pl [-ver] [-debug] [-man] [-sys] [-test]\n\n";
    print "   -ver    Verbose mode\n";
    print "   -debug  Sends no email messages out\n";
    print "   -man    Run the check of the file manifest only\n";
    print "   -sys    Compile the system info file only\n";
    print "   -test   Does not send email to Mailcc\n";   
    print "   -mail   Check root's mail.\n";  
    print "   -lamp   Run functions specific to ttv-lamp1.\n";
    print "   -wire   Run functions specific to The Wire.\n";
    print "   -itwire   Run functions specific to itwire.\n";
    
    print " - Wire specific options - \n";
    print "   -jpg    Check images\n";
    print "   -resize Used with -jpg.  Resize images if needed.\n";
    print "   -thumb  Used with -jpg.  Check thumbnails only. (Defaults to both thumbnails and large)\n";
    print "   -large  Used with -jpg.  Check large images only.\n";
    print "   -skujpg Check that there is an image for each SKU.\n";
    print "   -user   Check users.\n";
    print "   -updates Check for software updates.\n";
    print "   -log     Check the log.\n";
    print "   -db      Check the wire database.\n";
	print "   -raid    Check RAID.\n";
    print "   - Lamp specific options - \n";
    print "   -current Checks the sendmail for the current day.\n";
	print "   -database=<database> Specify a database other than the default of wire.\n";
    

}

sub check_uptime {
    my @uptime=`uptime`;
    chomp(@uptime);
    push(@email,"\n----Uptime: $uptime[0]----\n");  
    LogMsg("Uptime: $uptime[0]");
}

sub check_updates {       
    LogMsg("Checking for updates");    
    my @yum_info=`yum check-update`;
    my %update_hash;
    my @base_list;
    my @updates_list;
    my @addons_list;
    my @extras_list;
    my $update_count=0;

	# Check if there is a reboot required from a previous kernel update
	my $kernel_updated="/root/utilities/kernel_updated";
	if (-f "$kernel_updated") {
		# Get the date of the update
		my @file_info = stat "$kernel_updated";
		my $current_time = time();
		# Find out how many days old this file is.  (One day = 86400 seconds).
		my $file_age = sprintf("%.2f",(($current_time - $file_info[9]) / 86400));
        push(@email,"\nWARNING: kernel was updated $file_age days ago.  Reboot required");
		push(@summary,"\nWARNING: kernel was updated $file_age days ago. Reboot required");
		LogMsg("WARNING: kernel was updated $file_age days ago. Reboot required");
        $warning_count++;  		
	}
    foreach my $y (@yum_info) {
        chomp($y);
        my @tmp=split(/\s+/,$y);
        my $area=$tmp[-1];
        if (($area eq "base") || ($area eq "updates") || ($area eq "addons") || ($area eq "extras")) {
            my @array;
            if ($update_hash{$area}) {
                my $ref=$update_hash{$area};    
                @array=@$ref;
            }
            if ($verbose) {
				print "$y\n";
			}
            push(@array,$y);
            $update_hash{$tmp[-1]}=\@array;                            
        }                 
    }
    foreach my $area (sort keys(%update_hash)) {
        if ($update_hash{$area}) {
            my $ref=$update_hash{$area};
            my @array=@$ref;
            my $count=($#array + 1);
            $update_count+=$count;
            push(@email,"\n----There are $count $area updates available----");            
            LogMsg("There are $count $area updates available");
            foreach my $a (@array) {
                push(@email,"$a");
            }
            $send_email=1;
        }                    
    }
    if (0) {
        foreach my $y (@yum_info) {
            chomp($y);
            my @tmp=split(/\s+/,$y);
            my $area=$tmp[-1];
            if (($area eq "base") || ($area eq "updates") || ($area eq "addons") || ($area eq "extras")) {        
                push(@email,"$y");
            }            
        }
    }
    LogMsg("There are $update_count updates available");
    push(@email,"\n----There are $update_count updates available----"); 
 
    if ($enable_updates_hash{$hostname}) {
		LogMsg("Updates are set to install automatically.");
		push(@email,"Updates are set to install automatically."); 
        install_updates(\@yum_info);
    } else {
		LogMsg("Updates are not set to install automatically.");
		push(@email,"Updates are not set to install automatically."); 	
	}
}

sub install_updates {
    LogMsg("Installing updates");  
    # I had problems trying to install them all at once so I am installing them individually.  This will take longer
    # no doubt but hopefully it will be more reliable.
    my $ref=shift;
    my @update_array=@$ref;
    my $updates_count=0;
    my $base_count=0;
    my $error_count=0;
    my $type;
    my $status=1;
    my $installed_kernel=0;
    foreach my $update (@update_array) {
        my @tmp=split(/\s+/,$update);        
        if ($tmp[-1] eq "base") {
            $type="base";
            $update=$tmp[0];
        } elsif ($tmp[-1] eq "updates") {
            $type="updates";
            $update=$tmp[0];            
        } else {
            # Don't recognize this
            next;
        }
        LogMsg("Installing $update...");
    
        my @yum_info=`yum -y install $update`;    
        my $package;
        
        for (my $x=0; $x<=$#yum_info; $x++) {
            chomp(my $line=$yum_info[$x]);
            chomp(my $nextline=$yum_info[$x+1]);            
            if ($line =~ /Updated:/) {
				# Ignore the dependency updated line
				unless ($line =~ /Dependency/) {
					unless ($status) {
						# The previous update must not have been completed
						$error_count++;					
						if ($verbose) {
							LogMsg("Error count incremented on the following line:");
						}	
					}
					$status=0;
					$package=$nextline;
					LogMsg("Installing $package");
					if ($verbose) {
						LogMsg("$line");
					}	
					push(@email,"----Installing $package----");    
				}
            } elsif ($line =~ /Complete!/) {     
                $status=1;   
                if ($package =~ /kernel/) {
                    $installed_kernel=1;
					`touch /root/utilities/kernel_updated`;
                }
                LogMsg("Installed $package Complete");        
                push(@email,"----Installed $package - Complete!----"); 
                if ($type eq "base") {
                    $base_count++;
                } elsif ($type eq "updates") {
                    $updates_count++;
                }
				if ($verbose) {
					LogMsg("$line");
				}				
            } elsif ($verbose) {
				LogMsg("$line");
			}
        }    
    }
    if ($installed_kernel) {
        push(@email,"\nWARNING: kernel has been updated");
		push(@summary,"\nWARNING: kernel has been updated");
        $warning_count++;        
    }
    LogMsg("Finished installing updates: $base_count base upgrades - $updates_count updates - $error_count errors");
    push(@email,"\nFinished installing updates: $base_count base upgrades - $updates_count updates - $error_count errors"); 
    
}
sub install_updates_orig {

    LogMsg("Running yum-complete-transaction...");
    my @yum_info=`yum-complete-transaction -y`;  
    
    LogMsg("Installing updates");    
    @yum_info=`yum update -y`;    
    my $package;
    
    for (my $x=0; $x<=$#yum_info; $x++) {
        chomp(my $line=$yum_info[$x]);
        chomp(my $nextline=$yum_info[$x+1]);        
        if ($line =~ /Updated:/) {
            $package=$nextline;
            LogMsg("Installing $package");
            push(@email,"\n----Installing $package----");             
        }
        if ($line =~ /Complete!/) {     
            LogMsg("Installed $package Complete");        
            push(@email,"\n----Installed $package - Complete!----"); 
        }
    }
}

sub check_selinux {
    my $cmd="/usr/sbin/getenforce";
    chomp(my $setting=`$cmd`);      
    if ($setting =~ /Enforcing/) {
        LogMsg("selinux is $setting");
        push(@email,"\n----selinux is $setting----");
    } elsif ($setting eq "") {
        LogMsg("WARNING: selinux is undetermined");
        push(@email,"\nWARNING: selinux is undetermined");
		push(@summary,"\nWARNING: selinux is undetermined");
        $warning_count++;    
    } else {
        LogMsg("WARNING: selinux is $setting");
        push(@email,"\nWARNING: selinux is $setting");
		push(@summary,"\nWARNING: selinux is $setting");
        $warning_count++;
    }
    
}

sub check_web_access {
    LogMsg("Checking web records");
    my $access_log="/var/log/httpd/access_log";
    my $error_log="/var/log/httpd/error_log";
    # Get the date of the previous day    
    my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
    my $timeseconds=time();
    my @previous_day_ltm=localtime($timeseconds - 86400);
    my $previous_day=$previous_day_ltm[3];     
    my $previous_day_year=($previous_day_ltm[5] + 1900);   
    my $previous_day_month=$abbr[$previous_day_ltm[4]];  
    # Construct the date string to grep the access log
    my $year=($ltm[5]+1900);
    my $day=$ltm[3];
    my $month=$abbr[$ltm[4]];  
    
    # The access log has the date in the format of day/month name/year so we need to convert the month to a name
    my $date="$previous_day/$previous_day_month/$previous_day_year";
    if ($web_check) {
        $date="$day/$month/$year";
    }
    
    my @log_info;
    if (-e $access_log) {
        # Read the log file using grep from the command line
        @log_info=`grep "$date" $access_log`;
    }
    
    my %access_hash_IP;
    my %access_hash_URL;
    my %access_hash_hour;
    my %access_hash_minute;
    my %access_hash_hour_url;
    my %access_hash_minute_url;    
    my %access_hash_minute_ip;  
    foreach my $l (@log_info) {        
        my @tmp=split(/\s+/,$l);
        my $datetime=$tmp[3];
        my @tmp2=split(/:/,$datetime);
        my $hour=$tmp2[1];
        my $minute=$tmp2[2];
        my $IP=$tmp[0];
        my $url=$tmp[6];
        $access_hash_IP{$tmp[0]}++;
        $access_hash_URL{$tmp[6]}++;
        $access_hash_hour{"$date $hour"}++;
        $access_hash_minute{"$date $hour:$minute"}++;
        $access_hash_minute_url{"$date $hour:$minute $url"}++;
        $access_hash_minute_ip{"$date $hour:$minute $IP"}++;
    }
    my $count=0;
    LogMsg("Top ten IP addresses:");
    push(@email,"\n----Top ten IP addresses----");
    foreach my $IP (reverse sort {$access_hash_IP{$a}<=>$access_hash_IP{$b}} keys(%access_hash_IP)) {
        my $hostname=`host $IP`;
        @tmp=split(/\s+/,$hostname);
        $hostname=$tmp[-1];
        LogMsg("IP Address: $IP Host: $hostname count: $access_hash_IP{$IP}"); 
        push(@email,"IP Address: $IP Host: $hostname count: $access_hash_IP{$IP}"); 
        $count++;
        last if ($count > 10);          
    }
    $count=0;
    LogMsg("Top ten URLs:");
    push(@email,"\n----Top ten URLs----");    
    foreach my $URL (reverse sort {$access_hash_URL{$a}<=>$access_hash_URL{$b}} keys (%access_hash_URL)) {            
        LogMsg("URL Address: $URL count: $access_hash_URL{$URL}"); 
        push(@email,"URL Address: $URL count: $access_hash_URL{$URL}"); 
        $count++;
        last if ($count > 10);        
    }    
    $count=0;
    LogMsg("Top ten Hours:");
    push(@email,"\n----Top ten Hours----");    
    foreach my $hour (reverse sort {$access_hash_hour{$a}<=>$access_hash_hour{$b}} keys (%access_hash_hour)) {            
        LogMsg("Hour: $hour count: $access_hash_hour{$hour}"); 
        push(@email,"Hour: $hour count: $access_hash_hour{$hour}"); 
        $count++;
        last if ($count > 10);        
    }      
    $count=0;
    LogMsg("Top ten Minutes:");
    push(@email,"\n----Top ten Minutes----");    
    foreach my $minute (reverse sort {$access_hash_minute{$a}<=>$access_hash_minute{$b}} keys (%access_hash_minute)) {            
        LogMsg("Minute: $minute count: $access_hash_minute{$minute}"); 
        push(@email,"Minute: $minute count: $access_hash_minute{$minute}"); 
        $count++;
        last if ($count > 10);        
    }          
    $count=0;
    LogMsg("Top ten URL Minutes:");
    push(@email,"\n----Top ten URL Minutes----");    
    foreach my $minute (reverse sort {$access_hash_minute_url{$a}<=>$access_hash_minute_url{$b}} keys (%access_hash_minute_url)) {            
        LogMsg("Minute: $minute count: $access_hash_minute_url{$minute}"); 
        push(@email,"Minute: $minute count: $access_hash_minute_url{$minute}"); 
        $count++;
        last if ($count > 10);        
    }        
    $count=0;
    LogMsg("Top ten IP Minutes:");
    push(@email,"\n----Top ten IP Minutes----");    
    foreach my $minute (reverse sort {$access_hash_minute_ip{$a}<=>$access_hash_minute_ip{$b}} keys (%access_hash_minute_ip)) {            
        LogMsg("Minute: $minute count: $access_hash_minute_ip{$minute}"); 
        push(@email,"Minute: $minute count: $access_hash_minute_ip{$minute}"); 
        $count++;
        last if ($count > 10);        
    }   
    if ($detailed_report) {
        # A more detailed report on the web traffic
        LogMsg("Detailed Access Report:");
        foreach my $minute ( sort keys(%access_hash_minute))  {   
            my $count=$access_hash_minute{$minute};
            if ($count > 10) {
                LogMsg("Minute: $minute count: $access_hash_minute{$minute}"); 
            }
      
        }        
        LogMsg("Detailed IP Minutes Report:");
        foreach my $minute ( sort keys(%access_hash_minute_ip))  {   
            my $count=$access_hash_minute_ip{$minute};
            if ($count > 10) {
                LogMsg("Minute: $minute count: $access_hash_minute_ip{$minute}"); 
            }
      
        }


        
    }
    # Check for possible security concerns
    # The error log has the date in the format of day of week, month name day 
    $date="$month $day";    
    $date="$previous_day_month $previous_day"; 
    if (-e $error_log) {
        # Read the log file using grep from the command line
        @log_info=`grep "$date" $error_log`;
    }    
    my %error_hash;
    my %info_hash;    
    my %notice_hash;
    my %debug_hash;
    my $php_warning_count=0;
    my $php_notice_count=0;
    my $php_parse_count=0;
    foreach my $l (@log_info) { 
        chomp($l);         
        if ($l =~ /MaxClients/) {        
            push(@email,"notice: $l"); 
            push(@summary,"NOTICE:  $l\n");            
        }        
        if ($l =~ /PHP *Notice/) {        
            $php_notice_count++;
        }
        if ($l =~ /PHP *Warning/) {
            $php_warning_count++;
        }        
        if ($l =~ /PHP *Parse/) {
            $php_parse_count++;
        }           
        if ($l =~ /\[error\]/) {
            my @tmp=split(/\]/,$l);
            my $msg=$tmp[-1];
            @tmp=split(/:/,$msg);
            $msg=$tmp[0];
            $error_hash{$msg}++;
        }
        if ($l =~ /\[info\]/) {
            my @tmp=split(/\]/,$l);
            my $msg=$tmp[-1];
            @tmp=split(/:/,$msg);
            $msg=$tmp[0];
            $info_hash{$msg}++;
        }  
        if ($l =~ /\[notice\]/) {
            my @tmp=split(/\]/,$l);
            my $msg=$tmp[-1];
            @tmp=split(/:/,$msg);
            $msg=$tmp[0];
            $notice_hash{$msg}++;
        }    
        if ($l =~ /\[debug\]/) {
            my @tmp=split(/\]/,$l);
            my $msg=$tmp[-1];
            @tmp=split(/:/,$msg);
            $msg=$tmp[0];
            $debug_hash{$msg}++;
        }            
    }
    $count=0;
    if (keys(%error_hash)) {  
        LogMsg("Top error messages:");    
        push(@email,"\n----Top error messages----"); 
        foreach my $msg (reverse sort {$error_hash{$a}<=>$error_hash{$b}} keys (%error_hash)) {            
            $count++;
            if ($count < 11) { 
                LogMsg("error: $msg count: $error_hash{$msg}"); 
                push(@email,"error: $msg count: $error_hash{$msg}"); 
            }
        }
    }
    $count=0;
    if (keys(%notice_hash)) {       
        LogMsg("Top notice messages:");  
        push(@email,"\n----Top notice messages----");         
        foreach my $msg (reverse sort {$notice_hash{$a}<=>$notice_hash{$b}} keys (%notice_hash)) {            
            $count++;
            if ($count < 11) { 
                LogMsg("notice: $msg count: $notice_hash{$msg}"); 
                push(@email,"notice: $msg count: $notice_hash{$msg}"); 
            }
        }  
    }
    $count=0;
    if (keys(%info_hash)) {    
        LogMsg("Top info messages:"); 
        push(@email,"\n----Top info messages----");          
        foreach my $msg (reverse sort {$info_hash{$a}<=>$info_hash{$b}} keys (%info_hash)) {
            $count++;
            if ($count < 11) { 
                LogMsg("info: $msg count: $info_hash{$msg}"); 
                push(@email,"info: $msg count: $info_hash{$msg}");             
            }
        }
    }

    $count=0;
    if (keys(%debug_hash)) {
        LogMsg("Top debug messages:");
        push(@email,"\n----Top debug messages----");         
        foreach my $msg (reverse sort {$debug_hash{$a}<=>$debug_hash{$b}} keys (%debug_hash)) {
            $count++;
            if ($count < 11) {
                LogMsg("debug: $msg count: $debug_hash{$msg}"); 
                push(@email,"debug: $msg count: $debug_hash{$msg}"); 
            }            
        }
    }
    LogMsg("$php_warning_count PHP Warnings");
    push(@email,"$php_warning_count PHP Warnings");     
    LogMsg("$php_notice_count PHP Notices");
    push(@email,"$php_notice_count PHP Notices");   
    LogMsg("$php_parse_count PHP Parse Errors");
    push(@email,"$php_parse_count PHP Parse Errors");       
        
}

sub check_file_folder {
    # Check the file folder.  This folder on the wire is publicly readable and writeable.  We want to have some
    # sort of monitoring of what is going on there
    my $files="/var/www/****/sites/default/files";
    my $cmd="find -size +19000k -mtime -1";
    my $cmd2="find -mtime -1";
   # my $orig_dir=cwd();
    # Find the most recent files
    if (-d $files) {
        # Change directory 
        chdir("$files");
        if ($verbose) {
            LogMsg("Checking $files");
        }
        my @file_list=`$cmd`;
        if ($#file_list > -1) {
            LogMsg("Large files added in the last day:");  
            push(@email,"\n----Large Files added in the last day----");  
            foreach my $f (@file_list) {
                chomp($f);
                #my $file=`ls -l $f`;
                push(@email,$f);
                LogMsg("$f");                
            }
        } else {
            push(@email,"\n----No recent large files added----\n");
            LogMsg("No recent large files added");
            
            #  If there were no large files, look for any files added
            my @file_list=`$cmd2`;
            if ($#file_list > -1) {
                LogMsg("Files added in the last day:");  
                push(@email,"----Files added in the last day----");  
                foreach my $f (@file_list) {
                    chomp($f);
                    next if ($f eq ".");
                    next if ($f eq "..");
					next if ($f eq "./1");
                    #my $file=`ls -l $f`;
                    push(@email,$f);
                    LogMsg("$f");                
                }
            } else {
                push(@email,"\n----No recent files added----");
                LogMsg("No recent files added");

            }
            #

        }
        # Come back home
        chdir("$orig_dir");
    } else {
        if ($verbose) {
            LogMsg("$files was not found");
        }
    }
    
}

sub check_system_logs {
    
    my $timeseconds=time();
    my @previous_day_ltm=localtime($timeseconds - 86400);
    my $previous_day=$previous_day_ltm[3];    
    my $year=($ltm[5]+1900);
    my $day=$ltm[3];
    #my $month=($ltm[4]+1);
    my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
    my $month=$abbr[$ltm[4]];    
    my $previous_day_month=$abbr[$previous_day_ltm[4]];
    my %message_hash;

    
    LogMsg("Checking system logs");
    my @log_info=`last -10`;
    
    LogMsg("Checking last record:");
    push(@email,"\n----Checking 'last' record----");    
    for (my $x=0; $x<=$#log_info; $x++) {
        chomp(my $line=$log_info[$x]);
        push(@email,"$line");        
        if ($verbose) {
            LogMsg("$line");
        }
    }
       
    my $log="/var/log/messages";
    if (-e $log) {
        # The messages log has the date in the format of month name day 
        #push(@email,"\n----Checking messages log----");
        my $cmd="tail -10 $log";
        my $date="$previous_day_month $previous_day";
        open(LOG,"$log");
        @log_info=(<LOG>);       
        @log_info=grep/$previous_day_month.*$previous_day/,@log_info;
        unless ($#log_info > -1) {
            # If there were no messages for yesterday, show the 10 most recent entries
            LogMsg("Checking most recent entries messages log:");
            push(@email,"\n----Checking most recent entries in messages log----");
            @log_info=`$cmd`;            
        } else {
            LogMsg("Checking messages log for $date:");
            push(@email,"\n----Checking messages log for $date----");            
        }
        foreach my $l (@log_info) {
            chomp($l);
            #next unless ($l =~ /$date/);
            #next unless ($l =~ /$previous_day_month.*$previous_day/);
			if ($l =~ /MRMON/) {
				# Keep these separate
				my @tmp=split(/:/,$l);
				 
				$message_hash{"$tmp[3] $tmp[4]"}++;
			} else {
				push(@email,"$l");  
				if ($verbose) {
					LogMsg("$l");
				}  				
			}            
        }        
		# Summary for MRMON
 
		if (keys(%message_hash)) {
			LogMsg("---------MRMON message summaries----------------");  
			push(@email,"\n---------MRMON message summaries----------------\n");  
		}
		foreach my $m (sort keys(%message_hash)) {
			push(@email,"$m - $message_hash{$m} times");  
			if ($verbose) {
				LogMsg("$m - $message_hash{$m} times");
			}  			
		}
    } else {
        push(@email,"\nError: Could not find $log file");
    }

    $log="/var/log/secure";
    my %secure_log_hash;
    if (-e $log) {
        # The secure log has the date in the format of month name day 
        my $date="$previous_day_month $previous_day";
        open(LOG,"$log");
        @log_info=(<LOG>);
        LogMsg("Checking secure log:");
        push(@email,"\n----Checking secure log----");
        unless ($#log_info > -1) {
            push(@email,"no secure log entries for $date");
            LogMsg("no secure log entries for $date");
        }        
        foreach my $l (@log_info) {
            chomp($l);
            #next unless ($l =~ /$date/);
            next unless ($l =~ /$previous_day_month.*$previous_day/);
            my @tmp=split(/:/,$l);
			my $entry=$tmp[-1];
			# A common entry results from Nagios.  Here we manipulate it a bit:
			if ($entry =~ /Accepted publickey for nagios from 192.168.21.53 port .* ssh2/) {
				$entry="Accepted publickey for nagios from 192.168.21.53 ssh2";
			}
            $secure_log_hash{$entry}++;
            # Weed out common messages
            #next if ($l =~ /Connection closed by 192.168.21.50/);
            #push(@email,"$l");   
            if ($verbose) {
                LogMsg("$l");
            }    
        }    
        foreach my $key (sort keys(%secure_log_hash)) {
            push(@email,"Found $secure_log_hash{$key} $key");            
        }
    } else {
        push(@email,"\nError: Could not find $log file");
    }    
    
}

sub check_manifest {
    if ($hostname =~ /ttv-lamp1/)  {    
        push(@email,"\nNo provision to check file versions on $hostname"); 
        LogMsg("No provision to check file versions on $hostname"); 
        return;
    }
    # First check that this is a system hosting The Wire
    my $warns=0;
    my $wire="/var/www/****";
    my $root="/root";
    my $etc="/etc";
    my $modules_dir="${wire}/sites/all";
    if (-d $wire) {
        LogMsg("Found $wire");
    } else {
        return;
    }
    my $starting_dir=shift;
    my $manifest=shift;
    # Check ubercart
    #my $manifest="/root/wire_manifest.txt";
    # Check that the modules are the expected version
    my $cur_dir=cwd();
    
    if (-e $manifest) {
        LogMsg("Checking file versions");
        push(@email,"\n----Checking file versions: $manifest----");
         
        open(MANIFEST,"$manifest");
        my @manifest_info=(<MANIFEST>);
        close MANIFEST;
        #chdir("$modules_dir");
        chdir("$starting_dir");
        foreach my $line (@manifest_info) {
            chomp($line);
            my @tmp=split(/\s+/,$line);
            my $sum=$tmp[0];
            my $file=$tmp[1];
			next if ($file =~ /thumbs.db/i);	# Exclude this if it happens to appear on the manifest
			### Here we have a bit of a hack because when we are checking
			# the system_manifest, we will be checking the etc and root directories
			my $secondary_dir=$tmp[3];
			if ($secondary_dir eq "etc") {
				chdir("/etc");
			}
			if ($secondary_dir eq "root") {
				chdir("/root");
			}		
			###
            shift @tmp;
            my $file_label=join(' ',@tmp);            
            $file=substr($file,1);            
            #find the file
            chomp(my @found_info=`find . -name $file`);            
            my $found=0;
            if ($#found_info > -1) {
                foreach my $f (@found_info) {
                    my $cmd="/usr/bin/md5sum $f";                
                    my $found_sum=`$cmd`;                
                    my @tmp=split(/\s+/,$found_sum);
                    $found_sum=$tmp[0];
                    if ($found_sum eq $sum) {
                        LogMsg("$f matches $file_label");
                        $found=1;
                        last;
                    } elsif ($verbose) {
                        LogMsg("$f does not match $file_label");
						if ($debug) {
							sleep 1;
						}
                    }                    
                }
                if ($found) {
                    #push(@email,"$file - ok");
                    if ($verbose) {
                        LogMsg("$file - ok");                       
                    }
                    
                } else {
                    LogMsg("WARNING: -----> $file_label does not match");
                    push(@email,"--------> $file_label does not match");
                    $warns++;
					if ($debug) {
						print "WARNING: -----> $file_label does not match\n";
					}					
                } 
            } else {
                LogMsg("WARNING: -----> $file_label not found");
                push(@email,"--------> $file_label not found");
                $warns++;
				if ($debug) {					
					print "WARNING: -----> $file_label not found\n";
				}					
            }                        
        }
    } else {
        LogMsg("WARNING: Cannot locate $manifest");
        push(@email,"WARNING:  Cannot locate $manifest\n");
		push(@summary,"WARNING:  Cannot locate $manifest\n");
        $warning_count++;        
    }
    if ($warns) {
        LogMsg("WARNING: Found $warns files which do not match manifest");
        push(@email,"WARNING:  Found $warns files which do not match manifest\n");
		push(@summary,"WARNING:  Found $warns files which do not match manifest\n");
        $warning_count++;
    } else {
        push(@email,"Found $warns issues with file versions\n");
    }
    
}

sub collect_sysinfo {   
    # Gather important system information
    my @info;
    my $cmd;
    if (open(SYSINFO,">$sysinfo")) {
        # Get php info

        print SYSINFO "\n-------------------------------------------------\n";     
        print SYSINFO "php info:\n";         
        $cmd="/usr/bin/php -i";
        @info=`$cmd`;
        foreach my $i (@info) {
            chomp($i);
            print SYSINFO "$i\n";
        }        
        print SYSINFO "\n-------------------------------------------------\n";
        print SYSINFO "apache compile settings:\n";
        $cmd="/usr/sbin/apachectl -V";
        @info=`$cmd`;        
        foreach my $i (@info) {
            chomp($i);
            print SYSINFO "$i\n";
        }   
        print SYSINFO "\n-------------------------------------------------\n";
        print SYSINFO "apache loaded modules:\n";
        $cmd="/usr/sbin/apachectl -M 2>&1";
        @info=`$cmd`;        
        foreach my $i (@info) {
            chomp($i);
            print SYSINFO "$i\n";
             
        }
        print SYSINFO "\n-------------------------------------------------\n";
        print SYSINFO "Services configured:\n";
        $cmd="/sbin/chkconfig --list 2>&1";
        @info=`$cmd`;        
        foreach my $i (@info) {
            chomp($i);
            print SYSINFO "$i\n";
             
        }     
        print SYSINFO "\n-------------------------------------------------\n";
        print SYSINFO "Disk free:\n";
        $cmd="/bin/df -ah ";
        @info=`$cmd`;        
        foreach my $i (@info) {
            chomp($i);
            print SYSINFO "$i\n";
             
        }      
        print SYSINFO "\n-------------------------------------------------\n";
        print SYSINFO "Crontab:\n";
        $cmd="/usr/bin/crontab -l ";
        @info=`$cmd`;        
        foreach my $i (@info) {
            chomp($i);
            print SYSINFO "$i\n";
             
        }      
        close SYSINFO;  
              
      
     
    } else {
        LogMsg("WARNING: Failed to open sysinfo");
        $warning_count++;
    }
    
}


sub mailReport{
    my $subject=shift;
	my $summary_ref = shift;	
	my $message_ref = shift;
	my @summary_header = @$summary_ref;	
	my @message = @$message_ref;
	my $m="";
	my $smtp="";
    if ($warning_count == 1) {
        $subject.=" - $warning_count WARNING";
    } elsif ($warning_count) {
        $subject.=" - $warning_count WARNINGS";
    }
    # Connect to the server
    LogMsg("Mailing report to $Mailto $Mailcc on $Mailserver");
    if ($smtp = Net::SMTP->new("$Mailserver", Hello=>'backupadmin', Debug=>0)) {
        $smtp->mail($Mailfrom);
		LogMsg("Mailing report to $Mailto");
        $smtp->to($Mailto);		 
        unless ($test_mode) {
            if ($Mailcc) {
				LogMsg("Mailing report to $Mailto");
                $smtp->to($Mailcc);
            }            
        }
        
        #start mail
        $smtp->data();
        #send header
        $smtp->datasend("To: Ten Thousand Villages IT\n");
        $smtp->datasend("Subject: $subject\n\n");
        foreach $m (@summary_header) {
            $smtp->datasend("$m\n");
        }		
		$smtp->datasend("\n-----------------------------------------------\n");
        foreach $m (@message) {
            $smtp->datasend("$m\n");
        }

        # End the mail
        $smtp->dataend();
        $smtp->quit();
    } else {
          LogMsg("Error trying to send monitor warning!");
    }
	
}

sub LogMsg {
    my $m=shift;
    my $scriptlog="/var/log/monitor.log";
    chomp(my $date=`date`);
    open(LOG,">>$scriptlog");
    print LOG "$date : $m\n";
    if ($verbose) {
        print "$date : $m\n";
    }    
    close LOG;
}

sub check_catalog_sync {
    # Keep track of how long it takes to complete the catalog sync
    # We get that from the database:
    my $dbh="";
    my $mysqld=`hostname`;
    my $message;
    my $timestamp;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email, "\nCannot connect to mysql on host $mysqld! \n");
		push(@summary, "\nCannot connect to mysql on host $mysqld! \n");
        $warning_count++;       
        return;         
    }
    push(@email,"\n----Checking catalog sync----");
    # Check to see if this is already an open issue
    my $query = "
    select 
        message,timestamp
    from
        watchdog
    where
        type like 'catalog_sync'
    and
        message like 'Completed%'
    order by
        timestamp desc
    limit 2
        
	";
    #
    my $timeseconds=time();    
    my $found_field_sync=0; 
    my $found_item_sync=0; 
    my $sync_type;
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) { 
        $message=$tmp[0];   
        $timestamp=$tmp[1];
        if ($message =~ /field_sync/i) {
            $found_field_sync=1;
            $sync_type="field";
        }
        if ($message =~ /item_sync/i) {
            $found_item_sync=1;
            $sync_type="item";
        }        
        
        my $delta=($timeseconds - $timestamp);

        if ($delta < 86400) {
            # This reading was within the last 24 hours
            LogMsg("$message");
            push(@email, "$message\n");
        } else {
=pod		
            # The catalog sync must not have completed 
            LogMsg("WARNING:  Failed to find a $sync_type sync complete for today.");
            push(@email, "WARNING:  Failed to find a $sync_type sync complete for today. \n");
			push(@summary, "WARNING:  Failed to find a $sync_type sync complete for today. \n");
            $warning_count++;  
=cut			
        }        
    }    
=pod  
    unless ($found_field_sync) {
        LogMsg("WARNING:  Failed to find a field sync complete for today.");
        push(@email, "WARNING:  Failed to find a field sync complete for today. \n");   
		push(@summary, "WARNING:  Failed to find a field sync complete for today. \n");  
        $warning_count++;          
    }
    unless ($found_item_sync) {
        LogMsg("WARNING:  Failed to find a item sync complete for today.");
        push(@email, "WARNING:  Failed to find a item sync complete for today. \n");    
		push(@summary, "WARNING:  Failed to find a item sync complete for today. \n"); 
        $warning_count++;  
    }    
=cut	
	unless ($found_field_sync && $found_item_sync) {
		# Revised sync check
		push(@email,"\n----Checking catalog sync Again----");
		# Check to see if this is already an open issue
		my $timeseconds=time();  
		my $timelimit=($timeseconds - 86400);
		my $query = "
		select 
			message,timestamp
		from
			watchdog
		where
			type like 'catalog_sync'
		and
			message like 'Completed%'
		and
			timestamp > $timelimit
			or
			timestamp = $timelimit
		order by
			timestamp desc
 
			
		";
		#
		  
		my $sync_type;
		my $sth=$dbh->prepare("$query");
		$sth->execute();
		while (@tmp = $sth->fetchrow_array()) { 
			$message=$tmp[0];   
			$timestamp=$tmp[1];
			if ($message =~ /field_sync/i) {
				LogMsg("$message");
				$found_field_sync=1;
				$sync_type="field";
			}
			if ($message =~ /item_sync/i) {
				LogMsg("$message");
				$found_item_sync=1;
				$sync_type="item";
			}        			
		}    
	  
 	
	}
	unless ($found_field_sync) {
		LogMsg("WARNING:  Failed to find a field sync complete for today.");
		push(@email, "WARNING:  Failed to find a field sync complete for today. \n");   
		push(@summary, "WARNING:  Failed to find a field sync complete for today. \n");  
		$warning_count++;          
	}
	unless ($found_item_sync) {
		LogMsg("WARNING:  Failed to find a item sync complete for today.");
		push(@email, "WARNING:  Failed to find a item sync complete for today. \n");    
		push(@summary, "WARNING:  Failed to find a item sync complete for today. \n"); 
		$warning_count++;  
	}   	

	# Next check:  Check the throttle setting
	
    $query = "
    select 
        message,timestamp
    from
        watchdog
    where
        type like 'catalog_sync'
    and
        message like 'throttle set to:%'
    order by
        timestamp desc
    limit 2
        
	";	
	
	
	my $throttle;
    $sth=$dbh->prepare("$query");
    $sth->execute();	
    while (@tmp = $sth->fetchrow_array()) { 
        $message=$tmp[0];   
        $timestamp=$tmp[1];
		my @tmp=split(/:/,$message);
		        
        my $delta=($timeseconds - $timestamp);	

        if ($delta < 86400) {
            # This reading was within the last 24 hours
			$throttle=$tmp[1];
            LogMsg("$message");
            push(@email, "$message\n");
        }       
    }  
	# Determine how many SKUs were updated
    $query = "
    select 
        message,timestamp
    from
        watchdog
    where
        type like 'catalog_sync'
    and
        message like 'final update count:%'
    order by
        timestamp desc
    limit 2
        
	";	
	my $update_count;
    $sth=$dbh->prepare("$query");
    $sth->execute();	
    while (@tmp = $sth->fetchrow_array()) { 
        $message=$tmp[0];   
        $timestamp=$tmp[1];
		my @tmp=split(/:/,$message);		        
        my $delta=($timeseconds - $timestamp);	
        if ($delta < 86400) {
            # This reading was within the last 24 hours
			$update_count=$tmp[1];	
		}
	}
	# See if we are closing in on the throttle
	if ($throttle) {
		if ($update_count) {
			my $diff=($throttle - $update_count);
			if ($diff < 100) {
				LogMsg("WARNING:  SKU count is $update_count and throttle is only $throttle.");
				push(@email, "WARNING:  SKU count is $update_count and throttle is only $throttle.\n");   
				push(@summary, "WARNING:  SKU count is $update_count and throttle is only $throttle. \n");  
				$warning_count++; 			
			}
		}
		if ($update_count > $throttle) {
   				
		}
	} 	
 
	my $yesterday_timestamp=($timeseconds - 86400);
	# Determine which SKUs were republished and verify that they are indeed published
    $query = "
    select 
        message,timestamp
    from
        watchdog
    where
        type like 'cat_sync'
    and
        message like 'Republish:%'
	and
		timestamp > '$yesterday_timestamp'
 
        
	";	
	my @republish_list;	
	my @to_be_published_list;
    $sth=$dbh->prepare("$query");
    $sth->execute();	
    while (@tmp = $sth->fetchrow_array()) { 		
        $message=$tmp[0];   		
        $timestamp=$tmp[1];
		my @mtmp=split(/:/,$message);
		my $sku=$mtmp[1];
		$sku=~s/ //g;		
		push(@republish_list,$sku);
	}    
	# Verify that the SKU is indeed published.  If not, publish it.
	my $warns=0;
	foreach my $sku (@republish_list) {
		$query = "
		select
			title,status
		from
			node
		where
			title like '$sku'
		and
			type like 'item'
		";
		$sth=$dbh->prepare("$query");
		$sth->execute();	
		while (@tmp = $sth->fetchrow_array()) { 
			my $title = $tmp[0];
			my $status = $tmp[1];
			if ($title eq "$sku") {
				unless ($status) {					 
					# Confirmed that this is not published so put it on the list to publish
					push(@to_be_published_list,$sku);
				}
			} else {
				LogMsg("Error: $title is not $sku");
				$warns++;
			}
		}   				
	}
	# Now publish those that need it
	my $publish_count=0;
	foreach my $sku (@to_be_published_list) {
		$query = "
		update node set status = 1  
		where
			title like '$sku'
		and
			type like 'item'
		";
 
        if ($dbh->do($query)) {
			LogMsg("Published SKU: $sku");
			$publish_count++;
        } else {
			LogMsg("Error attempting to Published SKU: $sku");
            $warns++;           
        }  
	}	
	if ($publish_count) {
		LogMsg("Published $publish_count SKUs");
		push(@summary, "Published $publish_count SKUs. \n");
	}
	if ($warns) {
		LogMsg("WARNING: Found $warns issues checking catalog sync");
		$warning_count++;
	}
    $dbh->disconnect();        
}

sub check_user_sync {
    # The user_sync table is used when Abra syncs with The Wire.  Verify that it was updated. 
	my $timeseconds=time();  
	my $previous_day_time=($timeseconds - 86400);
	my $sync_count=0;
	my $warns=0;
    my $dbh="";
    my $mysqld=`hostname`;
    my $message;
    my $timestamp;
	if ($verbose) {
		LogMsg("Checking user sync with Abra");
	}
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email, "\nCannot connect to mysql on host $mysqld! \n");
		push(@summary, "\nCannot connect to mysql on host $mysqld! \n");
        $warning_count++;       
        return;         
    }
 
    push(@email,"\n----Checking User sync----");
 
    my $query = "select timestamp from user_sync order by timestamp desc limit 1";
	$query = "select count(*) from user_sync where timestamp > $previous_day_time";
	
    my $sth=$dbh->prepare("$query");
    $sth->execute();
 
    while (@tmp = $sth->fetchrow_array()) { 
 
		$sync_count=$tmp[0];
 	
	}
 
    $dbh->disconnect();        
	if ($sync_count) {
		push(@email,"There were $sync_count updates to user sync");
		if ($verbose) {
			LogMsg("There were $sync_count updates to user sync");
		}
	} else {
		# Send email to Leonard
		my @abra_email;
		my @abra_summary;		
		$Mailcc = 'jlw@tenthousandvillages.com';		
		push(@abra_email, "WARNING:  Failed to find a user sync update with Abra for today. \n");
		#push(@abra_summary, "WARNING:  Failed to find a user sync update with Abra for today. \n");	
		mailReport("$hostname: Abra Monitor",\@abra_summary,\@abra_email);	
		$Mailcc = '';
		# After sending, clear the Mailcc
		push(@email, "WARNING:  Failed to find a user sync update with Abra for today. \n");
		push(@summary, "WARNING:  Failed to find a user sync update with Abra for today. \n");
		$warns++; 
		if ($verbose) {
			LogMsg("WARNING:  Failed to find a user sync update with Abra for today.");
		}  		
	}
 
}

sub check_catalog_fields {
    # The catalog on The Wire has certain possible values set for certain fields.  Check that the values found in the 
	# content_type_item table match the list of allowed values found in the node_field table
	
	my $timeseconds=time();  
	my $previous_day_time=($timeseconds - 86400);
	my $settings=0;
	my $warns=0;
    my $dbh="";
    my $mysqld=`hostname`;
    my $message;
    my $timestamp; 
 
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email, "\nCannot connect to mysql on host $mysqld! \n");
		push(@summary, "\nCannot connect to mysql on host $mysqld! \n");
        $warning_count++;       
        return;         
    }
 
    push(@email,"\n----Checking Catalog Fields----");
	my @field_name_list;
    my $query = "select distinct(field_name) from node_field";
	 
    my $sth=$dbh->prepare("$query");
    $sth->execute();
 
    while (@tmp = $sth->fetchrow_array()) { 
		my $field=$tmp[0]; 	
 
		push(@field_name_list,$field);
 
	}
	
    $query = "select global_settings from node_field where field_name = 'field_item_country'";
	 
    $sth=$dbh->prepare("$query");
    $sth->execute();
 
    while (@tmp = $sth->fetchrow_array()) { 
		$settings=$tmp[0]; 	
 
	}	
 
	if ($settings) {
		my $not_allowed_count=0;
		my @allowed_values=();
		my @not_allowed_values=();
		my $allowed_values_entry='';
		# Parse the settings for the allowed values
		my @allowed_values_tmp=split(":",$settings);
		for (my $x=0; $x<=$#allowed_values_tmp; $x++) {
			my $term=$allowed_values_tmp[$x];			 
			if (($term =~ /allowed_values/) && ($term !~ /values_php/)) {		 
				$allowed_values_entry = $allowed_values_tmp[$x + 2];
			}
		}
		if ($allowed_values_entry) {
			my @tmp=split(/\"/,$allowed_values_entry);
			$allowed_values_entry=$tmp[1];
			my @allowed_values_tmp = split(/\n/,$allowed_values_entry);		
			foreach my $a (@allowed_values_tmp) {
				$a =~ s/\n//g;
				$a =~ s/\r//g;
				push(@allowed_values,$a);				
			}
		}

		# Get the list of actual values found
		my @found_value_list;
		$query = "select distinct(field_item_country_value) from content_type_item where field_item_type_value in ('Product')";
		 
		$sth=$dbh->prepare("$query");
		$sth->execute();		
		while (@tmp = $sth->fetchrow_array()) { 
			my $value=$tmp[0]; 				
			push(@found_value_list,$value);
	 
		}	
		# Check each of the found values to verify that it is an allowed value
		foreach my $f (@found_value_list) {
			next unless ($f);
			my $allowed=0;
			foreach my $a (@allowed_values) {
				if ($a eq $f) {
					$allowed=1;
				}
			}
			unless ($allowed) {				
				push(@email, "WARNING:  $f is a found value but is not allowed. \n");
				$not_allowed_count++;				
				push(@not_allowed_values,$f);
				# Find the items that have this value
				$query = "
					select 
						n.title 
					from 
						node n 
						join content_type_item cti on n.nid = cti.nid
					where 
						cti.field_item_country_value like '$f'
					";
				 
				$sth=$dbh->prepare("$query");
				$sth->execute();		
				while (@tmp = $sth->fetchrow_array()) { 
					my $sku=$tmp[0];
					push(@email, "WARNING:  $sku is set to $f. \n");					 												 
				}					
			}
		}
		
		if ($not_allowed_count) {
			push(@email, "WARNING:  Found $not_allowed_count values that are not allowed. \n");
			push(@summary, "WARNING:  Found $not_allowed_count Field Value(s) that is/are not allowed. \n");
			$warning_count++; 			
		} 		
	}
 
    $dbh->disconnect();  
 
}

sub check_wire_watchdog {
    # Tally the number of php errors seen and anything else of concern in the log files.
    my $warns=0;
    my $dbh="";
    my $mysqld=`hostname`;
    my $message;
    my $timestamp;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email, "\nCannot connect to mysql on host $mysqld! \n");
		push(@summary, "\nCannot connect to mysql on host $mysqld! \n");
        $warning_count++;       
        return;         
    }
    push(@email,"\n----Checking Wire Watchdog----");
    # Get all the messages since the beginning of the previous day
    my $timeseconds=time();
    my @ltm=localtime($timeseconds);
    my @previous_day_ltm=localtime($timeseconds - 86400);
    my $previous_day=$previous_day_ltm[3]; 
    my $previous_day_month=$previous_day_ltm[4];
    my $previous_day_year=($previous_day_ltm[5]+1900);
    # Now that we have determined the previous day, find the timestamp for the begining of yesterday
    use Time::Local;
    my $previousdaytime = timelocal(0,0,0,$previous_day,$previous_day_month,$previous_day_year);
 
	# Check for any new users created
    my $query = "
    select 
        w.message,
		u.name,
		from_unixtime(w.timestamp)
		
    from
        watchdog w
		join users u on w.uid = u.uid
    where
        w.timestamp > '$previousdaytime'
	and
		w.type like 'user'
	and
		w.message like 'New user%'
         
	";	
	my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) { 	
		my $message = $tmp[0];
		my @t2=split(/\>/,$message);
		my $user=$t2[1];
		@t2=split(/\</,$user);	
		$user=$t2[0];		
		my $name = $tmp[1];
		my $timestamp = $tmp[2];
		if ($verbose) {
			print "User: $user was created by $name $timestamp\n";
		}
		push(@summary, "NOTICE: User: $user was created by $name $timestamp.");  	
	}
	return;
	
    # Check all watchdog messages since the beginning of the previous day
    $query = "
    select 
        type,message,severity
    from
        watchdog

    where
        timestamp > '$previousdaytime'
         
	";
    #
    my $php_errors=0;  
    my $cron_errors=0;
    my $report_errors=0;
    my %watchdog_hash_0;
    my %watchdog_hash_1;
    my %watchdog_hash_2;
    
     $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) { 
        my $type=$tmp[0];   
        my $message=$tmp[1];
        my $severity=$tmp[2];
        if ($severity == 0) {
            $watchdog_hash_0{$type}++;
        }
        if ($severity == 1) {
            $watchdog_hash_1{$type}++;
        }    
        if ($severity == 2) {
            $watchdog_hash_2{$type}++;
            #LogMsg("WARNING:  Severity 2 entry: $type $message.");
            #push(@email, "WARNING:  Severity 2 entry: $type $message.. \n");              
        }  
        if ($type eq "php") {
            $php_errors++;
            if ($verbose) {
                print "$type $message $severity\n";
            }
        }
        if ($type eq "cron") {
            unless ($message =~ /run completed/) {            
                $cron_errors++;
                if ($verbose) {
                    print "$type $message $severity\n";
                }
            }
        }       
        if ($type eq "villagereports") {
            $report_errors++;
            if ($verbose) {
                print "$type $message $severity\n";
            }
        }           
    }    
    foreach my $key (sort keys(%watchdog_hash_0)) {
        LogMsg("Info:  There were $watchdog_hash_0{$key} priority 0 $key entries.");
        push(@email, "Info:  There were $watchdog_hash_0{$key} priority 0 $key entries.");  
    }
    foreach my $key (sort keys(%watchdog_hash_1)) {
        LogMsg("Note:  There were $watchdog_hash_1{$key} priority 1 $key entries.");
        push(@email, "Note:  There were $watchdog_hash_1{$key} priority 1 $key entries.");    
    }   
    foreach my $key (sort keys(%watchdog_hash_2)) {         
        LogMsg("WARNING:  There were $watchdog_hash_2{$key} priority 2 $key entries.");
        push(@email, "WARNING:  There were $watchdog_hash_2{$key} priority 2 $key entries.");           
        #$warns++;
    }       
  
    if  ($php_errors) {
        LogMsg("WARNING:  There were $php_errors php errors recorded.");
        push(@email, "WARNING:  There were $php_errors php errors recorded.");   
        #$warns++;          
    }
    if  ($cron_errors) {
        LogMsg("WARNING:  There were $cron_errors php errors recorded.");
        push(@email, "WARNING:  There were $cron_errors cron errors recorded.");   
        #$warns++;          
    }
    if  ($report_errors) {
        LogMsg("WARNING:  There were $report_errors php errors recorded.");
        push(@email, "WARNING:  There were $report_errors report errors recorded.");   
        #$warns++;          
    }    
    if ($warns) {
        LogMsg("WARNING:  There were $warns issues detected checking The Wire log.");
        push(@email, "WARNING:  There were $warns issues detected checking The Wire log. \n");   
		push(@summary, "WARNING:  There were $warns issues detected checking The Wire log. \n");   
        $warning_count++;         
    } 
	
    $dbh->disconnect();        
}

sub check_wire_changes {
    # Check to see if settings were changed on The Wire
    my $warns=0;
    my $dbh="";
    my %current_hash;
    my %old_hash;           
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host localhost");
        push(@email, "\nCannot connect to mysql on host localhost! \n");
		push(@summary, "\nCannot connect to mysql on host localhost! \n");
        $warning_count++;       
        return;         
    } 
    # A list of settings in the variable table we can safely ignore
    my @ignore_variables=(
        "casetracker_current_case_numbers",
        "catalog_sync_daily_timestamp",
        "catalog_sync_last_sku",
        "cron_last",
        "node_cron_last",
        "node_cron_last_nid",
        "node_cron_views_scale",
        "statistics_day_timestamp",
        "uc_store_last_report",
        "uc_store_site_id",
        "uc_extra_discount_shopper",
        "uc_extra_discount_shopper",
        "uc_regular_discount_shopper",
		"cron_semaphore"
    );
           
    push(@email,"\n----Checking variable changes----");
    # We do this by dumping the variables table each day and comparing it to the previous one
    # This table starts as a copy of the "variable" table and will be used to note when the original
    # table changes.
    my $variable_history="variable_history";
    my $found=0;
    # See if the the comparison table exists and create it if it does not.
    my $query = "show tables like \'$variable_history\'";
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {                 
        $found++;
    }          
    unless ($found) {
        # The table does not exist so create it
        $query="        
            CREATE TABLE \`$variable_history\` (
            `name` varchar(128) NOT NULL default '',
            `value` longtext NOT NULL,
            PRIMARY KEY  (`name`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8
        ";        
        if ($dbh->do($query)) {
            # Now see if the table exists:           
            $query = "show tables like \'$variable_history\'";
            $sth=$dbh->prepare("$query");
            $sth->execute();
            while (@tmp = $sth->fetchrow_array()) {                 
                $found++;
            }   
            unless ($found) {
                LogMsg("Error: Failed to create $variable_history");
                push(@email, "\nError: Failed to create $variable_history! \n");
				push(@summary, "\nError: Failed to create $variable_history! \n");
                $warning_count++; 
                return;
            }
        } else {
            LogMsg("Error trying to create $variable_history");
            push(@email, "\nError: trying to create $variable_history! \n");
			push(@summary, "\nError: trying to create $variable_history! \n");
            $warning_count++; 
            return;
        }
    } else {
        #  First get the current data
  
        $query="select * from variable order by name";
        $sth=$dbh->prepare("$query");
        $sth->execute();
        while (@tmp = $sth->fetchrow_array()) {         
            $current_hash{$tmp[0]}=$tmp[1];        
        }          
        # Now get the previous data:
        $query="select * from $variable_history order by name";
        $sth=$dbh->prepare("$query");
        $sth->execute();
        while (@tmp = $sth->fetchrow_array()) {         
            $old_hash{$tmp[0]}=$tmp[1];        
        }                             
    }

    
    if (keys(%current_hash)) {
        # If we have data for a previous run and data for today, compare the data:
        foreach my $name (sort keys(%current_hash)) {
            unless ($current_hash{$name} eq $old_hash{$name}) {
                # There are certain settings which we can safely ignore so skip those
                unless (grep/$name/,@ignore_variables) { 
                    LogMsg("WARNING: Settings for $name have changed");
                    push(@email, "WARNING: Settings for $name have changed! ");
                    push(@email, "Previous $name settings: ($old_hash{$name})");
                    push(@email, "Current $name settings: ($current_hash{$name})");
                    LogMsg("Previous $name settings: ($old_hash{$name})");
                    LogMsg("Current $name settings: ($current_hash{$name})");
                    $warns++;                                
                }                
                # Update the history table with the current setting
                # escape any single quotes                
                my $value=$current_hash{$name};              
                $value=~s/\'/\\'/g;                               
                $query="replace into $variable_history (name,value) VALUES(\'$name\',\'$value\')";
                if ($dbh->do($query)) {                
                    LogMsg("Updated $name in $variable_history");
                } else {
                    LogMsg("Error trying to update $name in $variable_history");
                    push(@email, "\nError trying to update $name in $variable_history! \n");					
                    $warns++;
                }                 
            }  
        }
    }    
    $dbh->disconnect(); 
    if ($warns) {
        LogMsg("WARNING: There were $warns variables changed on The Wire.");
        push(@email, "WARNING: There were $warns variables changed on The Wire. \n");
		push(@summary, "WARNING: There were $warns variables changed on The Wire. \n");
        $warning_count++;
    } else {
        LogMsg("There were $warns variables changed on The Wire.");
        push(@email, "There were $warns variables changed on The Wire. \n");
    }
}

sub check_wire_changes_beta {
    # Check to see if settings were changed on The Wire
    my $warns=0;
    my $dbh="";
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host localhost");
        push(@email, "\nCannot connect to mysql on host localhost! \n");
		push(@summary, "\nCannot connect to mysql on host localhost! \n");
        $warning_count++;       
        return;         
    }    
    # We do this by dumping the variables table each day and comparing it to the previous one
    my $variable_history="/var/tmp/variables.txt";
    
    my %current_hash;
    my %old_hash;    
    my @old;
    if (-f $variable_history) {
        # If the file exists, it contains data from the previous
        # time the script was run.  Read it into memory:
        open(OLD,"$variable_history");
        @old=(<OLD>);
        close OLD;   
        foreach my $o (@old) {
            chomp($o);
            @tmp=split(/�/,$o);
            $old_hash{$tmp[0]}=$tmp[1];
        }
    }
    # Now, export the current variables table:
    my $query="select * from variable order by name";        
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {         
        $current_hash{$tmp[0]}=$tmp[1];        
    }        
    

    $dbh->disconnect();   
    if ((keys(%old_hash))&&(keys(%current_hash))) {
        # If we have data for a previous run and data for today, compare the data:
        foreach my $name (sort keys(%current_hash)) {
            unless ($current_hash{$name} eq $old_hash{$name}) {
                LogMsg("WARNING: Settings for $name have changed");
                LogMsg("Previous $name settings: ($old_hash{$name})");
                LogMsg("Current $name settings: ($current_hash{$name})");
                $warns++;             
                my $p = (<STDIN>);
            } else {
                print "$name ($current_hash{$name}) ($old_hash{$name})\n";
            }
        }
    }
    if (keys(%current_hash)) {
        # Now, save the current settings
        open(OLD,">$variable_history");
        foreach my $name (sort keys(%current_hash)) {
            print OLD "${name}�${current_hash{$name}}\n";
        }
    }
    if ($warns) {
        LogMsg("WARNING: There were $warns variables changed on The Wire.");
		push(@email, "WARNING: There were $warns variables changed on The Wire.\n");
		push(@summary, "WARNING: There were $warns variables changed on The Wire.\n");
        $warning_count++;
    } else {
        LogMsg("There were $warns variables changed on The Wire.");
		push(@email, "There were $warns variables changed on The Wire.\n");	
	}
}

sub check_jpg {
    # The goal here is to check the images for the catalog.  We have two folders, thumbnails and large images.  
    # The thumbnails should be 150x150 and the large should be 600x600
    my @folders=(
        "thumbs",
        "large"
    );
    if ($thumb) {
        @folders=("thumbs");
        if ($large) {
           push(@folders,"large");
        }
    }
    if ($large) {
       @folders=("large");
    }
    my $warns=0;
    my $good_size_count=0;
    my $large_diff_limit="12";  # How much larger can it be before it complains.
    my $small_diff_limit="12";  # How much smaller can it be before it complains.
    my $resize_limit="50";
    my $path="/var/www/****/sites/default/files";
    my $tool="/usr/bin/rdjpgcom";
    unless (-f $tool) {
        LogMsg("WARNING: Could not locate $tool.");
        $warning_count++;
        return;
    }    
    LogMsg("Checking image sizes.");    
    my $expected_w="150";
    my $expected_h="150";
    foreach my $f (@folders) {
        my $dir="${path}/${f}";
        if ($f =~ /large/) {
            # Change the expected sizes;
            $expected_w="600";
            $expected_h="600";
            $large_diff_limit="50";
            $small_diff_limit="150";
            $resize_limit="200";
        }
        if (-d $dir) {
            LogMsg("Checking $f");
            #chdir($dir);
            opendir (DIR,"$dir");
            my @tmp=readdir DIR;
            my @dirinfo=sort { $a <=> $b } @tmp;
            closedir DIR;            
            foreach my $file (@dirinfo) {                
                next unless ($file =~ /.jpg$/);
                my $resize_needed=0;
                my $filepath="${dir}/${file}";
                my $cmd="$tool -verbose \"$filepath\"";
                my @fileinfo=`$cmd`;
                my $line=$fileinfo[0];
                my @tmp=split(/\s+/,$line);
                # Width should be at index 3
                # Height should be at index 5
                my $w=$tmp[3];
                my $h=$tmp[5];
                # Trim off the w & h at the end
                $w=~s/w//;
                $h=~s/h//;
                if (0) {
                    # Checking the width is currently disabled.  I think the height is the important dimension.
                    # If the image is resized, we maintain the aspect ratio - kdg
                    if ($w > $expected_w) {
                        my $diff=($w - $expected_w);
                        $diff=abs($diff);
                        if ($diff > $large_diff_limit) {
                            if ($verbose) {
                                LogMsg("$file is too wide by $diff:  Expected $expected_w but found $w");                        
                            }                            
                            $warns++;                                    
                            next;   # No need to check the height                        
                        }                   
                    } elsif ($w < $expected_w) {
                        my $diff=($expected_w -$w);
                        $diff=abs($diff);
                        if ($diff > $small_diff_limit) {                            
                            if ($verbose) {
                                LogMsg("$file is too narrow by $diff:  Expected $expected_w but found $w");
                            }                            
                            $warns++;                                                    
                            next;   # No need to check the height                                                                
                        }
                    }
                }
                
                if ($h > $expected_h) {
                    my $diff=($h - $expected_h);
                    $diff=abs($diff);
                    if ($diff > $large_diff_limit) {                    
                        if ($verbose) {
                            LogMsg("$file is too high by $diff:  Expected $expected_h but found $h");
                        }                        
                        $resize_needed=1 ;
                        $warns++;
                    }
                } elsif ($h < $expected_h) {
                    my $diff=($expected_h -$h);
                    $diff=abs($diff);
                    if ($diff > $small_diff_limit) {      
                        if ($verbose) {
                            LogMsg("$file is too short by $diff:  Expected $expected_h but found $h");
                        }                        
                        if ($diff < $resize_limit) {
                            # Don't increase the size if the images is too small
                            $resize_needed=1 ;
                        }
                        
                        $warns++;
                    }
                } else {
                    # This file is within bounds
                    $good_size_count++;
                }
                if (($resize) && ($resize_needed)){
                    resize_image("$dir","$filepath","$expected_h");   
                    pause_check();
                }
        
            }
        }
    }
    push(@email,"\n----Checking image sizes----");   
    if ($warns) {
        LogMsg("NOTICE: Found $warns images the wrong size");
        LogMsg("$good_size_count images where of acceptable size");
        push(@email, "NOTICE: Found $warns images the wrong size\n");
        #$warning_count++;
    } else {
       push(@email, "Found $warns images the wrong size\n");
    }
    LogMsg("Finished checking image sizes.");    
}

sub resize_image {    
    my $dir=shift;            # Where the file is
    my $filepath=shift; # Image to resize
    my $height=shift;   # Set to this height maintaining the aspect ratio
    my $tool="/usr/bin/convert";
    my $basename=basename($filepath);
    my $backupfile="${basename}_orig";
    unless (-f $tool) {
        LogMsg("Failed to find tool - cannot convert");
        # Don't try again
        $resize=0;
        return;
    }
    if (-f $filepath) {
        unless (-f "${dir}/${backupfile}") {
            LogMsg("Backing up $filepath to $backupfile");
            copy($filepath,"${dir}/${backupfile}");
            unless (-f "${dir}/${backupfile}") {
                LogMsg("Backup unsuccessful - resize canceled");
                return;
            }
        }
        LogMsg("Resizing $filepath");
        #chdir($dir);
        my $cmd="$tool $filepath -resize \'x$height' $filepath";
        `$cmd`;
        if ($?) {
            LogMsg("Error resizing $filepath");            
        } else {
            LogMsg("Successful");
        }
    } else {
        LogMsg("Failed to find $filepath - cannot resize");
        return;
    }
    
}

sub pause_check {
    if ($pause) {
        print "Press enter to continue\n";
        my $p=(<STDIN>);
    }
}

sub check_missing_jpg {
	# This function has been replace by check_images
	return;
    # Every item in the database should have an image if the status of the node is set
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              
        return;         
    }    
    my @sku_list;
    my $query = "
    select 
        title 
    from 
        node 
    where 
        status = 1 and 
        type like 'item'        
	";
    #
    my $found=0; 
  
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {         
        push(@sku_list,$tmp[0]);
    }        
    # Now see if there is a thumb for each SKU

    my $thumb_warns=0;        
    my $large_warns=0;
    my $path="/var/www/****/sites/default/files";
    push(@email,"\n----Checking image for each SKU----");   
    LogMsg("Checking image present for each sku...");    
    foreach my $sku (@sku_list) {
        my $imagepath="${path}/thumbs/${sku}.jpg";
        unless (-f "$imagepath") {
            if ($verbose) {
                print "Could not find $imagepath\n";
            }        
            push(@email, "No thumbnail image for $sku");  
            $thumb_warns++;            
        }
        $imagepath="${path}/large/${sku}w1.jpg";
        unless (-f "$imagepath") {
            push(@email, "No large image for $sku");  
            if ($verbose) {
                print "Could not find $imagepath\n";
            }
            $large_warns++;
        }        
    }
        
    if ($thumb_warns) {
        LogMsg("WARNING: Found $thumb_warns items with no thumbnail image");        
        push(@email, "WARNING: Found $thumb_warns items with no thumbnail image");        
    } else {
        push(@email, "Found $thumb_warns items with no thumbnail image\n");
    }    
    if ($large_warns) {
        LogMsg("WARNING: Found $large_warns items with no large image");        
        push(@email, "WARNING: Found $large_warns items with no large image");
       
    } else {
        push(@email, "Found $large_warns items with no large image\n");
    }        
    if (($large_warns) || ($thumb_warns)) {
	#debug
        $warning_count++;
    }
    LogMsg("Finished checking images present.");        
    
}

sub check_wire_db {
    # Check the wire database for problems
    my $warns=0;
    push(@email,"\n----Checking database----");   
    LogMsg("Checking database...");  
    ## Check for items where the URL ends in -0
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              
        return;         
    }     
    my %item_url_hash;
    my $query="select src,dst from url_alias where dst like 'content/_______-_'";
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {                 
        my $src=$tmp[0];
        if ($src =~ /node/) {
            my @stmp=split(/\//,$src);
            $src=$stmp[1];
            
        }
        my $dst=$tmp[1];       
        if ($dst =~ /[0-9]{4,}-0/) {            
            if ($verbose) {
                print "WARNING: Possible catalog item url issue with $dst\n";                  
                $item_url_hash{$src}=$dst;
            }
            push(@email, "WARNING: Possible catalog item url issue with $dst");            
            $warns++;                
        }               
    }   
    my %correct_url_hash;
    if ($fix) {
        print "Attempt to fix urls\n";
        foreach my $node (sort keys(%item_url_hash)) {
            $query="select type,title from node where nid = $node";
            $sth=$dbh->prepare("$query");
            $sth->execute();
            while (@tmp = $sth->fetchrow_array()) {  
                my $type=$tmp[0];
                my $title=$tmp[1];
                # If the type is item and the title matches the dst, it is likely a catalog item that needs to have the URL corrected
                if ($type eq "item") {
                    # Get the current url
                    my $current_url=$item_url_hash{$node};
                    my @ctmp=split(/\//,$current_url);
                    my $SKU=$ctmp[1];
                    @ctmp=split(/-/,$SKU);
                    $SKU=$ctmp[0];
                    if ($SKU eq $title) {
                        if ($verbose) {
                            print "SKU $SKU has incorrect URL of $item_url_hash{$node}\n";
                        }
                        $correct_url_hash{$node}="$SKU";
                    }
                }
            }
        }
        # Correct the URL
        foreach my $node (sort keys(%correct_url_hash)) {
            my $SKU=$correct_url_hash{$node};
            if ($verbose) {
                print "SKU: $SKU node: $node  dst $item_url_hash{$node}\n";
            }
            if ($pause) {
                print "\nPress Enter to fix this SKU.\n";
                my $p=(<STDIN>);
            }
            
            # correct the mail url 
            $query="update url_alias set dst = 'catalog/$SKU' where src = 'node/$node'";
            if ($dbh->do($query)) {                
                LogMsg("Corrected main url for SKU $SKU");
            } else {
                LogMsg("Error trying to correct main url for SKU $SKU");
                push(@email, "\nError trying to correct main url for SKU $SKU! \n");
                $warning_count++;
            }   
        }
    }
    if ($warns) {
        $warning_count++;
        LogMsg("There were $warns urls with possible issues");
        push(@email, "WARNING: There were $warns urls with possible issues");
		push(@summary, "WARNING: There were $warns urls with possible issues");
    } else {
        LogMsg("No issues found checking urls.");
        push(@email, "No issues found checking urls.");
    }    
    $warns=0;
    # Check for tables that might have a problem
    if (1) {
        my @check_info=`mysqlcheck wire -uwiretestu -pwiretestupww`;
        foreach my $c (@check_info) {
            chomp($c);
            my @tmp=split(/\s+/,$c);
            my $table=$tmp[0];
            my $status=$tmp[1];
            unless ($status =~ /ok/i) {
                LogMsg("table ($table) status unknown");
                push(@email, "WARNING: table $table appears to have a problem");            
                $warns++;
            }
            if ($c =~ /warning/i) {
                LogMsg(" $c");
                push(@email, "WARNING: table $table $c");
                $warns++;
            }
            if ($c =~ /error/i) {
                unless ($c =~ /user_import_errors/) {
                    LogMsg(" $c");
                    push(@email, "WARNING: table $table $c");
                    $warns++;
                }            
            }
			if ($c =~ /corrupt/i) {
				LogMsg(" $c");
				push(@email, "WARNING: May have a corrupt database table: $c");
				$warns++;				
			}
        }
    }

    if ($warns) {
        $warning_count++;
        LogMsg("There were $warns database tables with possible issues");
        push(@email, "WARNING: There were $warns database tables with possible issues");
		push(@summary, "WARNING: There were $warns database tables with possible issues");
    } else {
        LogMsg("No issues found checking database.");
        push(@email, "No issues found checking database.");
    }
    LogMsg("Finished checking database..."); 
}

sub check_backups {
    # Check the backups for problems
    my $warns=0;
    
    if (($hostname =~ /ttv-lamp1/) && ($day_of_week == 0)) {    
         push(@email,"\nNo backups on $hostname on Sunday"); 
        return;
    }
    
    push(@email,"\n----Checking backup logs----"); 
    # Find all of the backup logs
    my $logdir="/var/log";
    opendir(LOGS,"$logdir");
    my @log_listing=readdir LOGS;
    closedir LOGS;    
    my $date_string=`date`;
    my @tmp=split(/\s+/,$date_string);
   
    # Split the date into day of week, month, day, time, year
    my $dow=$tmp[0];
    my $mon=$tmp[1];
    my $day=$tmp[2];  
    my $yr=$tmp[$#tmp];
    my @key_words=(
        "error",
        "warning",
        "break-in"
    );
    my @exclusions=(
        "Toba is still mounted"
    );
    my $found_beginning=0;
    my $found_complete=0;
	my %backup_hash;
    
    foreach my $l (@log_listing) {    
        if ($l =~ /backup.log$/i) {
            ###            
            my $backup_log="${logdir}/${l}";
            if (-f $backup_log) {
                LogMsg("Checking backup log $backup_log...");  
                my $read=0;                
                open(FILE,"$backup_log");
                while (<FILE>) {
                    chomp(my $line=$_);
                    my @tmp=split(/\s+/,$line);
                    my $found_dow=$tmp[0];
                    my $found_mon=$tmp[1];
                    my $found_day=$tmp[2];
                    my $found_year=$tmp[5];
                    $found_year=~s/://g;
               
                    # Find any errors in today's backups
                    unless ($read) {   
                        #if (($found_year eq $yr) && ($found_mon eq $mon) && ($found_day eq $day) && ($found_dow eq $dow)) {
                        if (($found_year eq $yr) && ($found_mon eq $mon) && ($found_day eq $day)) {                    
                            $read=1;                            
                        }
                    }
                    next unless $read;
                    foreach my $word (@key_words) {
                        if ($line =~ /$word/i) {
                            # Exclude some known "errors"
                            foreach my $e (@exclusions) {
                                unless ($line =~ /$e/) {
                                    $warns++;
                                    push(@email, "$line");
                                    if ($verbose) {
                                        print "$line\n";
                                    }                           
                                }
                            }

                        }                    
                    }
					
                    if ($line =~ /passes basic check/i) {
						$backup_hash{"Passed Basic Check"}=$l                      
                    }					
                    if ($line =~ /backup completed/i) {
                        $found_complete=1;
                    }
                    if (($line =~ /Beginning/i) || ($line =~ /Backup Started/i)) {
                        $found_beginning=1;
                    }                    
                }        
            ###
            }  else {
                $warns++;
                LogMsg("ERROR: Could not locate $backup_log");
                push(@email, "ERROR: Could not locate $backup_log");
            }
            unless ($found_beginning) {
                $warns++;
                LogMsg("WARNING: Could not determine $backup_log began");
                push(@email, "WARNING: Could not determine $backup_log began");    
            }     
            unless ($found_complete) {
                $warns++;
                LogMsg("WARNING: Could not determine $backup_log  completed");
                push(@email, "WARNING: Could not determine $backup_log  completed");    
            }       

        }         
    }
	foreach my $b (sort keys(%backup_hash)) {
		LogMsg("$backup_hash{$b} $b");
		push(@email, "$backup_hash{$b} $b");   							
	}	
	# The following was disabled as it does not appear that The Wire can ping ttv-fsb2 - kdg
=pod	
	# Verify that we can connect to the backup location
	my $backup_location="ttv-fsb2";
	unless (ping_test($backup_location)) {
        LogMsg("WARNING: Cannot contact $backup_location.");
        push(@email, "WARNING: Cannot contact $backup_location.");
		$warns++;
	}
=cut	
    if ($warns) {
        $warning_count++;
        LogMsg("There were $warns possible issues with backups");
        push(@email, "WARNING: There were $warns possible issues with backups");
		push(@summary, "WARNING: There were $warns possible issues with backups");
    } else {
        LogMsg("No issues found checking the backups.");
        push(@email, "No issues found checking the backups.");
    }
    LogMsg("Finished checking the backups..."); 
}

sub ping_test {
	my $host_to_ping=shift;
	my $cmd="ping -c 4 $host_to_ping";
	my @ping_results=`$cmd`;
	my $ping_return=1;
	foreach my $p (@ping_results) {
		print "#debug tp 0 ($p)\n";
	}
	if ($?) {
		print "#debug tp 1 failed\n";
		# A value of 0 for $? means successful ping
		# and a postive value means failure.  So we
		# essentially return the opposite.
		$ping_return=0;
	} else {
		print "#debug tp 2 passed\n";
	}
	
	return $ping_return;
}

sub check_wire_backups_old {
    # Check the wire backup for problems
    my $warns=0;
    push(@email,"\n----Checking backup log----");   
    LogMsg("Checking backup log...");      
    my $backup_log="/var/log/lastwirebackup.log";
    if (-f $backup_log) {
        open(FILE,"$backup_log");
        while (<FILE>) {
            my $line=$_;
            
            if ($line =~ /error/i) {
                $warns++;
                push(@email, "$line");
                if ($verbose) {
                    print "$line\n";
                }
            }
            if ($line =~ /warning/i) {
                $warns++;
                push(@email, "$line");
                if ($verbose) {
                    print "$line\n";
                }
            }
        }
        
    } else {
        $warns++;
        LogMsg("ERROR: Could not locate $backup_log");
        push(@email, "ERROR: Could not locate $backup_log");
    }
    if ($warns) {
        $warning_count++;
        LogMsg("There were $warns possible issues with the backup");
        push(@email, "WARNING: There were $warns possible issues with the backup");
		push(@summary, "WARNING: There were $warns possible issues with the backup");
    } else {
        LogMsg("No issues found checking the backup.");
        push(@email, "No issues found checking the backup.");
    }
    LogMsg("Finished checking the backup..."); 
}

sub check_search_index {
    # NOTE:  This was added to track whether the site is getting indexed properly.
    # I must say that I am not certain that this function is getting the information accurately.  I got the
    # idea from the node.module  - kdg
    
    # See how much of the site has been searched
    LogMsg("Checking the search index...");     
    push(@email,"\n----Checking search index----");      
    my $dbh;
	
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              	
        return;         
    }   
	
    my $last_change;
    my $last_nid;    
    my @file_info;    
    my $total;
    my $remaining;
    my $percentage="unknown";
    my $file="search_index_record";
    my $query = "
        SELECT 
            COUNT(*) 
        FROM 
            node
        WHERE 
            status = 1
    ";
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {         
        $total=$tmp[0];
    }   

    if (-f $file) {
        open(FILE,"$file");
        @file_info=(<FILE>);
        close FILE;
    }
    my $found_last=0;
    foreach my $line (@file_info) {
        if ($line =~ /last_change/) {
            my @tmp=split(/\s+/,$line);
            $last_change=$tmp[-1];
        }
        if ($line =~ /last_nid/) {
            my @tmp=split(/\s+/,$line);
            $last_nid=$tmp[-1];
        }        
    }
 
    if (($last_nid) && ($last_change)) {
        my $last=$last_change;        
        # If we found the last nid and the last change, we can find out how much has been indexed
        $query = "
            SELECT 
                GREATEST(IF(c.last_comment_timestamp IS NULL, 0, c.last_comment_timestamp), n.changed) as last_change, 
                n.nid 
            FROM 
                node n 
                LEFT JOIN node_comment_statistics c ON n.nid = c.nid 
            WHERE 
                n.status = 1 
            AND 
                ((GREATEST(n.changed, c.last_comment_timestamp) = $last AND n.nid > $last_nid) 
            OR 
                (n.changed > $last
            OR 
                c.last_comment_timestamp > $last)) 
            ORDER BY 
                GREATEST(n.changed, c.last_comment_timestamp) ASC, 
                n.nid ASC                
        ";            
        $sth=$dbh->prepare("$query");
        $sth->execute();
        while (@tmp = $sth->fetchrow_array()) {         
            $last_change=$tmp[0];
            $last_nid=$tmp[1];
        }         
 	
    } else {

        $query = "
            SELECT 
                GREATEST(IF(c.last_comment_timestamp IS NULL, 0, c.last_comment_timestamp), n.changed) as last_change, 
                n.nid 
            FROM 
                node n 
                LEFT JOIN node_comment_statistics c ON n.nid = c.nid 
            WHERE 
                n.status = 1 
             
        ";    
        
        $sth=$dbh->prepare("$query");
        $sth->execute();
        while (@tmp = $sth->fetchrow_array()) {         
            $last_change=$tmp[0];
            $last_nid=$tmp[1];
        }  
 	

    }
    # Get the remaining
    my $last=$last_change;
    $query = "
        SELECT 
            COUNT(*) 
        FROM 
            node n 
            LEFT JOIN node_comment_statistics c ON n.nid = c.nid 
        WHERE 
            n.status = 1 
        AND 
            ((GREATEST(n.created, n.changed, c.last_comment_timestamp) = $last 
        AND     
            n.nid > $last_nid ) 
        OR 
            (n.created > $last OR n.changed > $last
        OR 
            c.last_comment_timestamp > $last))
           
    ";    
    
    $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {         
        $remaining=$tmp[0];        
    }      
  

    # Calculate percent indexed
    if ($total) {
        $percentage=sprintf("%.2f",(100 * ($total - $remaining) / $total));
        LogMsg("Site is $percentage percent indexed");
        push(@email, "Site is $percentage percent indexed");
    } else {
        LogMsg("    - ERROR: Failed to get a total"); 
        push(@email, "ERROR: Failed to get a total");
    }
	if ($debug) {
		my $last_change_date="unknown";
	
		$query = "
			SELECT 
				from_unixtime($last_change)
			   
		";    
		
		$sth=$dbh->prepare("$query");
		$sth->execute();
		while (@tmp = $sth->fetchrow_array()) {         
			$last_change_date=$tmp[0];        
		} 	
	
	
		#print "Total: $total\n";
		#print "Last NID: $last_nid\n";
		#print "Last Change: $last_change\n";
		#print "Last Change Date: $last_change_date\n";
		#print "Remaining: $remaining\n";
	}
    
    # Now update the file
    my $update=0;
    if (($last_change) && ($last_nid)) {  
        if ($last_change =~ /^[+-]?\d+$/) {
            if ($last_nid =~ /^[+-]?\d+$/) {
                # These variables exist and are numbers
                $update=1;
            }
        }
    }
     
    if ($update) {
        open(FILE,">$file");
        print FILE "last_change $last_change\n";
        print FILE "last_nid $last_nid\n";
        close FILE;
    } else {
        unlink $file;
    }
	####
	# Alternative
	####
	### Find all items not indexed
	my $unindexed_count=0;
	my @item_list=();
	my @unindexed_item_list=(); 
 
	$query = "
	SELECT 
		n.title 
	FROM   
		node n
		LEFT OUTER JOIN search_index si ON (n.title = si.word)
	WHERE 
		n.type like 'item'
	and
		n.status = 1
	and
		si.word IS NULL	
	";
	
 
	$sth=$dbh->prepare("$query");
	$sth->execute();
	while (@tmp = $sth->fetchrow_array()) { 
		my $title=$tmp[0];
        push(@email, "Found item not indexed: $title"); 
		LogMsg("Found item not indexed: $title"); 
		push(@item_list,$title); 
		$unindexed_count++;	
	} 		
 
	if ($unindexed_count) {
		push(@summary, "WARNING: Found $unindexed_count items not indexed");
		LogMsg("WARNING: Found $unindexed_count items not indexed");
		$warning_count++;	
	} else {
		push(@email, "Found all items are indexed"); 
		LogMsg("Found all items are indexed");
	}
 
	### 
 
 
	$dbh->disconnect;
    LogMsg("Finished checking the search index...");     
}

sub check_indexed_items {
	my $check_id="indexed items";
	LogMsg("Checking $check_id...");     
    push(@email,"\n----Checking $check_id----");      
    my $dbh;
	
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              	
        return;         
    }   
 
    my $query = "
        SELECT 
            si.word,
			si.score,
			si.sid
        FROM 
            search_index si
			join node n on si.word = n.title
        WHERE 
            (n.type like 'item'
			and
			n.status = '1')
 
    ";
    my $sth=$dbh->prepare("$query");
    $sth->execute();
	my %record_count_hash;
	my %score_hash;
	my %sid_hash;
    while (@tmp = $sth->fetchrow_array()) { 
		my $word=$tmp[0];
		my $score=$tmp[1];
		my $sid=$tmp[2];
		$record_count_hash{$word}++;
		$score_hash{$word}=$score;
		$sid_hash{$word}=$sid;
 
    }   
	my @prospects_list;
	my $unindexed_count=0;
	foreach my $word (sort keys(%record_count_hash)) {
		my $count=$record_count_hash{$word};
		my $score=$score_hash{$word};
		next if $count > 1;
		if ($score < 27) {
			push(@prospects_list,$word);

			
		}
	}
	foreach my $word (@prospects_list) {
		my $sid=$sid_hash{$word};
		if ($sid) {
			my $unindexed=1;
			# Check the search_dataset
			$query = "
				SELECT 
					data				
				FROM 
					search_dataset
					 
				WHERE 
					sid = $sid					
			";
			my $sth=$dbh->prepare("$query");
			$sth->execute();
 
			while (@tmp = $sth->fetchrow_array()) {
				my $line=$tmp[0];				
				my @data=split(/\s+/,$line); 
				if ($data[1] eq $word) {
					if ($data[2] eq "printerfriendly")  {
						if ($data[3] eq "version") {
							# This word is properly indexed;
							$unindexed=0;
						}
					} elsif ($data[2] eq "traveler") {
						if ($data[3] eq "s") {
							# This word is properly indexed;
							$unindexed=0;
						}					
					}
				}
			};
			if ($unindexed) {
				$unindexed_count++;			
				push(@email, "Item $word may not be indexed"); 
				LogMsg("$unindexed_count - Item $word may not be indexed");			
			}
		}
	}
	if ($unindexed_count) {
		push(@summary, "WARNING: Found $unindexed_count items possibly not indexed");
		LogMsg("WARNING: Found $unindexed_count items possibly not indexed");
		$warning_count++;	
	} else {
		push(@email, "Found all items appear to be indexed"); 
		LogMsg("Found all items appear to be indexed");
	}
 
	### 
 
 
	$dbh->disconnect;
    LogMsg("Finished checking $check_id..."); 
}

sub check_raid {
    push(@email,"\n----Checking RAID status----");  
    my $tool = "/opt/MegaRAID/CmdTool2/CmdTool2";
	my $previous_record = "/root/utilities/raid_records.txt";
	my $tmp_record = "/root/utilities/tmp_record.txt";
	my @previous_record_info;
	if (-f $previous_record) {
		# Read in the previous record
		 push(@email, "Reading in from $previous_record");
		open(PLOG,"$previous_record");
		@previous_record_info=(<PLOG>);
		close PLOG;
	} else {
		 push(@email, "Did not find $previous_record");
	}
    unless (-e "$tool") {
        LogMsg("    - ERROR: Failed to find $tool"); 
        push(@email, "ERROR: Failed to find $tool");
		push(@summary, "ERROR: Failed to find $tool");
        $warning_count++;
        return;
    }
	# Open the temporary record for logging
	open(TLOG,">$tmp_record");
    my $cmd = "$tool -PDList -aAll";
    my @raid_info=`$cmd`;
    my @tmp;
    my $id;
    my $media_err;
    my $other_err;
	my $predictive_failure_count;
    my $state="unknown";
    my $warns=0;
	my %firmware_state_hash;
    foreach my $r (@raid_info) {
        chomp($r);
        if ($r =~ /^Device Id:/i) {
            @tmp=split(/\s+/,$r);
            $id=$tmp[-1];            
        }
        if ($r =~ /^Media Error Count:/i) {
            @tmp=split(/\s+/,$r);
            $media_err=$tmp[-1];            
        } else {
            # If we have not found the media error count yet, set this value to -1
            $media_err=-1;
        }   
        if ($r =~ /^Other Error Count:/i) {
            @tmp=split(/\s+/,$r);
            $other_err=$tmp[-1];            
        } else {
            $other_err=-1;
        }
        if ($r =~ /^Predictive Failure Count:/i) {
            @tmp=split(/\s+/,$r);
            $predictive_failure_count=$tmp[-1];            
        } else {
            $predictive_failure_count=-1;
        }		
        if ($r =~ /^Firmware State:/i) {
            @tmp=split(/\s+/,$r);
            $state=$tmp[-1];      
        } else {
            # If we have not found the state yet, set this value to 0
            $state=0;
        }        
		if ($state) {
			$firmware_state_hash{$id}=$state;
		}
 	
        if ($media_err > 0) {
			# Is this a new error or was it on the previous record?
			my $p_count = 0;
			foreach my $p (@previous_record_info) {
				if ($p =~ /media errors/) {
					if ($p =~ /id: $id/) {						
						my @tmp=split(/\s+/,$p);
						$p_count=$tmp[-1];	
						push(@email, "RAID: Previous record $p - count: ($p_count)");						
					}
				}
			}
			unless ($p_count == $media_err) {
				my $diff=($media_err - $p_count);
				my $label="NOTE:";
				if ($diff > 5) {
					$label = "ERROR:";	
					$warns++;  
				} elsif ($diff > 3) {
					$label = "WARNING:";	
					$warns++;  					
				}			
				# Only warn if the error count has changed.
                LogMsg("    - $label id $id has $media_err media errors - previous count: $p_count"); 
                push(@email, "$label id $id has $media_err media errors - previous count: $p_count");                  													
			}			
			print TLOG "media errors on id: $id $media_err\n";			
			if ($verbose) {
				print  "media errors on id: $id $media_err\n";				
			}
        } elsif ($media_err > -1) {
            # If we know the media err value (i.e. it is not -1)
            if ($verbose) {
                LogMsg("    - id $id has $media_err media errors"); 
            }
        }
        if ($other_err > 0)  {
			# Is this a new error or was it on the previous record?
			my $p_count=0;
			foreach my $p (@previous_record_info) {
				if ($p =~ /other errors/) {
					if ($p =~ /id: $id/) {
						my @tmp=split(/\s+/,$p);
						$p_count=$tmp[-1];
						push(@email, "RAID: Other Err: Previous record $p - count: ($p_count)");						
					}
				}
			}
			unless ($p_count == $other_err) {
				my $diff=($other_err - $p_count);
				my $label="NOTE:";
				if ($diff > 5) {
					$label = "ERROR:";	
					$warns++;  
				} elsif ($diff > 3) {
					$label = "WARNING:";	
					$warns++;  					
				}
				# Only warn if the error count has changed.		
                LogMsg("    - $label id $id has $other_err other errors - previous count: $p_count"); 
                push(@email, "$label id $id has $other_err other errors - previous count: $p_count");                				
			}
			# Log the errors found today				
			print TLOG "other errors on id: $id $other_err\n";	
			if ($verbose) {
				print  "other errors on id: $id $other_err\n";				
			}
        } elsif ($other_err > -1) {
            if ($verbose) {
                LogMsg("    - id $id has $other_err other errors"); 
            }
        }   
        if ($predictive_failure_count > 0)  {
			# Is this a new error or was it on the previous record?
			my $p_count=0;
			foreach my $p (@previous_record_info) {
				if ($p =~ /predictive failure count/) {
					if ($p =~ /id: $id/) {
						my @tmp=split(/\s+/,$p);
						$p_count=$tmp[-1];
						push(@email, "RAID: Predictive Failure: Previous record $p - count: ($p_count)");
					}
				}
			}
			unless ($p_count == $predictive_failure_count) {	
				my $diff=($predictive_failure_count - $p_count);
				my $label="NOTE:";
				if ($diff > 5) {
					$label = "ERROR:";	
					$warns++;  
				} elsif ($diff > 3) {
					$label = "WARNING:";	
					$warns++;  					
				}			
                LogMsg("    - $label id $id has $predictive_failure_count predictive failure count - previous count: $p_count"); 
                push(@email, "$label id $id has $predictive_failure_count predictive failure count - previous count: $p_count");
                $warns++; 

			}
			print TLOG "predictive failure count on id: $id $predictive_failure_count\n";		
			if ($verbose) {
				print  "predictive failure count on id: $id $predictive_failure_count\n";	
			}			
        } elsif ($predictive_failure_count > -1) {
            if ($verbose) {
                LogMsg("    - id $id has $predictive_failure_count predictive failure count"); 
            }
        }  
		
    }
	# Evaluate the firmware state.  We expect to have 3 devices online and 1 hotspare
	my $online_count=0;
	my $hotspare_count=0;
	foreach my $hd (sort keys(%firmware_state_hash)) {
		my $state=$firmware_state_hash{$hd};
		LogMsg("    - HD ID $hd is $state");
		if ($state eq "Online") {
			$online_count++;
		} elsif ($state eq "Hotspare") {
			$hotspare_count++;
		} else {
			LogMsg("    - ERROR: id $hd has a state of $state"); 
			push(@email, "ERROR: id $hd has a state of $state");
			$warns++; 				
		}
	}
	if ($online_count < 3) {
		LogMsg("    - ERROR: There are only $online_count drives Online"); 
		push(@email, "ERROR: There are only $online_count drives Online");
		$warns++; 			
	}
	unless ($hotspare_count) {
		LogMsg("    - ERROR: There are no Hotspare drives."); 
		push(@email, "ERROR: There are no Hotspare drives.");
		$warns++; 			
	}		
	# Close the temp record
	close TLOG;
	unless ($debug) {
		# Update the previous count unless this is debug mode
		# 2015-01-21 - I am changing this to update only if an error was found - kdg
		if (($media_err) || ($other_err) || ($predictive_failure_count)) {   
			push(@email, "RAID: Overwriting previous record");		
			copy($tmp_record,$previous_record);
		} else {
			push(@email, "RAID: Not overwriting previous record");
		}
	}
    $cmd = "$tool -EncInfo -aAll";
    @raid_info=`$cmd`;
	$state="unknown";	
    foreach my $r (@raid_info) {
        chomp($r);

        if ($r =~ /Status/i) {
            @tmp=split(/\s+/,$r);
            $state=$tmp[-1];            
        }
		
	}
	if ($state eq "Normal") {
		LogMsg("    - Enclosure state - $state");
	} else {
		LogMsg("    - ERROR: Enclosure Status: $state"); 
		push(@email, "ERROR: Enclosure Status: $state");
		$warns++; 	
	}
	$cmd = "$tool -FwTermLog -Dsply -aAll";
    @raid_info=`$cmd`;
    chomp(my $date_string=`date +"%m/%d/%y"`);	
	my %error_hash;
    foreach my $r (@raid_info) {
        chomp($r);
		if ($r =~ /^$date_string/) {
			if ($r =~ /error/i) {				
				my $msg=substr($r,19);
				$error_hash{$msg}++;								
			}	
		}
	}	 
	foreach my $msg (keys(%error_hash)) {
		LogMsg("    - ERROR: $error_hash{$msg} messages: $msg"); 
		push(@email, "ERROR: $error_hash{$msg} messages: $msg");
		$warns++; 		
	}
	
    if ($warns) {
        $warning_count++;	
		LogMsg("WARNING: There were $warns issues checking RAID status.");
		push(@email, "WARNING: There were $warns issues checking RAID status.");            
		push(@summary, "WARNING: There were $warns issues checking RAID status.");
    } else {
		LogMsg("There were $warns issues checking RAID status.");
		push(@email, "There were $warns issues checking RAID status.");            
	}
    
    #LogMsg("Finished checking the RAID status.");     
}

sub check_festivalsales {
    # Numerous folks can log into festival sales.  Send a note if they change their password
    LogMsg("Checking the festival sales password...");     
    push(@email,"\n----Checking festival sales----");      
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              
        return;         
    }       
    my $file="pw_record";
    my $pw;
    my $expected_pw="none";
    my $warns=0;
    my $query = "
        SELECT 
            pass
        FROM 
            users
        WHERE 
            name like 'festivalsale'
    ";
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {         
        $pw=$tmp[0];
    }   

    if (-f $file) {
        open(FILE,"$file");
        my @file_info=(<FILE>);
        close FILE;
        foreach my $f (@file_info) {
            chomp($expected_pw=$f);
        }
    } else {
        LogMsg("Did not find $file");
    }    
    if ($expected_pw eq "none") {
        # Need to save the password we found
        open(FILE,">$file");
        print FILE "$pw\n";
        close FILE;        
    } elsif ($expected_pw eq $pw) {
        if ($verbose) {
            LogMsg("Password for festival sale matches expected.");
        }
    } else {
       if ($verbose) {
            LogMsg("WARNING: Password for festival sale ($pw) does NOT match expected ($expected_pw).");            
        }
        push(@email, "WARNING: Password for festival sale does NOT match expected.");   
        $warns++;
    }
 
    if ($warns) {
        $warning_count++;
		LogMsg("WARNING: There were $warns issues checking festival sale.");  
		push(@email, "WARNING: There were $warns issues checking festival sale.");     
		push(@summary, "WARNING: There were $warns issues checking festival sale.");  
		
    } else {
		LogMsg("There were $warns issues checking festival sale.");  
		push(@email, "There were $warns issues checking festival sale.");     
	
	}
    $dbh->disconnect();        
}

sub check_wire_orders {
    # This function checks that no orders were placed where accounts are null
    LogMsg("Checking the wire orders...");     
    push(@email,"\n----Checking wire orders----");      
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              
        return;         
    }        
    my $warns=0;
    my $ignore_users = "(
        '7',
        '754'
    )";
    my $query = "
    select 
        uco.order_id, 
        uco.uid,
        p.value as account_no,
        uco.primary_email, 
        u.name, 
        from_unixtime(uco.modified) as submit_time,         
        uco.order_status as Status
    from 
        uc_orders uco 
        left join profile_values p on uco.uid = p.uid and p.fid = (select fid from profile_fields where name='profile_account') 
        left join users u on uco.uid = u.uid         
    where
            
        uco.uid not in $ignore_users
        
       
    ";
    
    my $sth=$dbh->prepare("$query");
    
    $sth->execute();
    my %order_hash; 
    while (@tmp = $sth->fetchrow_array()) {         
        my $order=$tmp[0];
        my $account=$tmp[2];
        my $name=$tmp[4];
        my $date=$tmp[5];
        my $status=$tmp[6];
		$order_hash{$status}++;
        next if ($status eq "received");
		next if ($status eq "in_checkout");
		next if ($status eq "shipped");
        unless ($account) {
           if ($verbose) {
                LogMsg("WARNING: Order $order placed by $name on $date but account is unknown. Status: $status"); 				
            }
            push(@email, "WARNING: Order $order placed by $name on $date but account is unknown. Status: $status");   
            $warns++;
        }
        
    }       
    if ($warns) {
        $warning_count++;
		LogMsg("WARNING: There were $warns issues checking orders.");  
		push(@email, "WARNING: There were $warns issues checking orders.");  			
		push(@summary, "WARNING: There were $warns issues checking orders.");  
    } else {
		LogMsg("There were $warns issues checking orders.");  
		push(@email, "There were $warns issues checking orders.");  	
	}
	if ($verbose) {
		LogMsg("Order Status Record:");
		foreach my $status (sort keys(%order_hash)) {
			LogMsg("$order_hash{$status} orders - status: $status");
		}
	}
    
    $dbh->disconnect();    
}

sub check_users {
    # Check that all users have a group
    LogMsg("Checking the wire users...");     
    my @exclusions=(
        "1541"
    );  # Users which are allowed to have no group
    push(@email,"\n----Checking wire users----");      
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {              
        return;         
    }        
    my $warns=0;

    my $query = "
    select 
        uid,
        name,
        login
    from 
        users 
    where 
        uid not in (select uid from og_uid)
    and
        status = 1
    ";
    
    my $sth=$dbh->prepare("$query");
    
    $sth->execute();
    
    while (@tmp = $sth->fetchrow_array()) {         
        my $uid=$tmp[0];        
        my $ok=0;
        foreach my $e (@exclusions) {
            if ($uid == $e) {
                # This one is okay
                $ok=1;
            }
        }
        next if $ok;
        my $name=$tmp[1];
        my $login=$tmp[2];
        my @ltm = localtime($login);
        my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);
        my $time = sprintf("%02d:%02d:%02d", $ltm[2], $ltm[1], $ltm[0]);
        unless ($login) {
            $date_label="never";
            $time="";
        }
        if ($verbose) {            
            LogMsg("WARNING: User $name uid $uid is in no group. LastLogin: $date_label $time");            
        }
        push(@email, "WARNING: User $name uid $uid is in no group. LastLogin: $date_label $time");   
        $warns++;        
    }      
	# Check if users have autologout set inappropriately
	# Get a list of users that do not autologout
	my %exclusions=(
		'82'=>'Lula Filbert',
		'169'=>'Lynellen Stauffer'
	);
	my @not_autologout;
	my @autologout;

    $query = "
    select 
        uid,
		setting
    from 
        autologout
	order by
		uid
 
    ";
    
    $sth=$dbh->prepare("$query");
    
    $sth->execute();    
    while (@tmp = $sth->fetchrow_array()) { 
		my $uid=$tmp[0];
		my $setting=$tmp[1];			
		if ($setting) {
			push(@not_autologout,$uid); 
		} else {
			push(@autologout,$uid); 		
		} 
	}
	
	# Collect users that should timeout
	# Company Store Management that is not Corporate Staff should log out
	my %company_store_managers;
	my %corporate_staff;
	my %company_store_staff;
	my %alliance_store;
    $query = "
    select 
        nid,
		uid
    from 
        og_uid
 
    ";
    
    $sth=$dbh->prepare("$query");
    
    $sth->execute();
    
    while (@tmp = $sth->fetchrow_array()) { 
		
		my $nid=$tmp[0];
		my $uid=$tmp[1];
		if ($nid == 5930) {
			$corporate_staff{$uid}=1;				
		}  
		if ($nid == 6168) {
			$company_store_managers{$uid}=1;	
		}  
		if ($nid == 5932) {
			$company_store_staff{$uid}=1;	
		}  	
		if (($nid == 5940) || ($nid == 5931)) {
			$alliance_store{$uid}=1;
 		
		}  				
	}	
	my @should_be_autologout;
	my @should_not_be_autologout;
	# Evaluate:  Company Store Managers who are not in Corporate Staff should have autologout
	foreach my $uid (@not_autologout) {
 
		if (($company_store_managers{$uid}) || ($company_store_staff{$uid})) {
			unless(($corporate_staff{$uid}) || ($alliance_store{$uid})) {
				unless($exclusions{$uid}) {
					push(@should_be_autologout,$uid); 
				}
			}
		}  
	}
	LogMsg("Autologout check #1");
	if (0) {
		foreach my $uid (@should_be_autologout) {
			$query = "
			select 			 
				name,
				login
			from 
				users 
			where 
				uid = '$uid'
			and
				status = 1
			";
			
			$sth=$dbh->prepare("$query");
			
			$sth->execute();
			
			while (@tmp = $sth->fetchrow_array()) {
				my $name=$tmp[0];
				my $login=$tmp[1];
				unless ($name =~ /staff/i) {
					if ($verbose) {            
						LogMsg("WARNING: User $name uid $uid is not set for autologout but should be.");            
					}
					push(@email, "WARNING: User $name uid $uid is not set for autologout but should be.");   
					$warns++;  		
				}
			}
		}
	}
	LogMsg("Autologout check #2");
	# Look at other info
	foreach my $uid (@not_autologout) {
		# Filter out corporate staff & some others
		next if ($corporate_staff{$uid}) ;
		next if ($alliance_store{$uid}) ;
		next if ($exclusions{$uid});
		my $name='';
		my $job='';
		my $store='';
		my $allowed=0;	# This variable is used to excuse a user from auto-logout
		# Gather profile of this person
		$query = "
		select 			 
			name			
		from 
			users 
		where 
			uid = '$uid'
		and
			status = 1
		";
		
		$sth=$dbh->prepare("$query");
		
		$sth->execute();
		
		while (@tmp = $sth->fetchrow_array()) {
			$name=$tmp[0];			 
		}	
		next unless ($name);
		
		# What I do
		$query = "
		select 			 
			fid,
			value			
		from 
			profile_values 
		where 
			uid = '$uid'
		order by 
			fid
		";
		
		$sth=$dbh->prepare("$query");
		
		$sth->execute();

		while (@tmp = $sth->fetchrow_array()) {
			my $fid=$tmp[0];
			my $value=$tmp[1];
		 
			if ($fid == 1) {
				# This is what I do
				if ($value =~ /manager/i) {					
					unless ($value =~ /retail/i) {					
						$job="$value";
						# Filter out retail sales managers						
					}
				}

				if (($value =~ /new interest group/i) || ($value =~ /new store interest group/i)) {										 				
					$job="NIG";	
					$allowed=1;					
				}	
				if ($verbose) {
					if ($job) {
						print "$name has a job of $job\n";
					}
				}
			}	
			if ($fid == 4) {
				# Account
				$value=~ s/ //g;
				my $l=length($value);
				if (($value =~ /\d/) && ($l == 4)) {
					$store=$value;
				} else {
					unless ($allowed) {
						# Members of a new interest group need not have a store number assigned
						if ($verbose) {            
							LogMsg("WARNING: User $name uid $uid has a store setting of ($store).");            
						}
						push(@email, "WARNING: User $name uid $uid has a store setting of ($store)");   
						$warns++;  				
					}
				}
			}			
		}	
		if (0) {
			if (((($job) && ($store)) || ((($company_store_managers{$uid}) || ($company_store_staff{$uid})) && ($store))) && ($name !~ /staff/i)){
				if ($verbose) {            
					LogMsg("WARNING: User $name uid $uid is a $job at $store and is not set for autologout but should be.");            
				}
				push(@email, "WARNING: User $name uid $uid is a $job at $store and is not set for autologout but should be.");   
				$warns++;  		
			}
		}
		# Condition #1
		if (($job) && ($store)) {
			unless ($allowed) {
				if ($verbose) {            
					LogMsg("WARNING: User $name uid $uid is a $job at $store and is not set for autologout but should be.");            
				}
				push(@email, "WARNING: User $name uid $uid is a $job at $store and is not set for autologout but should be.");   
				$warns++;  	
			}
		}	
		# Condition #2
		if  ($company_store_managers{$uid}) {
			if ($verbose) {            
				LogMsg("WARNING: User $name uid $uid is a company store manager at $store and is not set for autologout but should be.");            
			}
			push(@email, "WARNING: User $name uid $uid is a company store manager at $store and is not set for autologout but should be.");   
			$warns++;  		
		}
 	
		# Condition #3	If this is company store staff but is not the "staff" login, then it should be set to autologout.
		if  (($company_store_staff{$uid}) && ($store) && ($name !~ /staff/i)){
			if ($verbose) {            
				LogMsg("WARNING: User $name uid $uid is not a staff login at $store and is not set for autologout but should be.");            
			}
			push(@email, "WARNING: User $name uid $uid is not a staff login at $store and is not set for autologout but should be.");   
			$warns++;  		
		}		
 
	}	
	
	LogMsg("Autologout check #3");
	foreach my $uid (@not_autologout) {		
		# Collect the list of roles and groups for this user
		my @roles=();
		my @groups=();		
		my %auto_logout_roles=(
			'administrative users' => "3",
			'department gardener' => "5",
			'see my reports' => "7",
			'masquerade' => "8",
			'uc administrator' => "9",
			'uc extra discount shopper' => "10",
			'uc regular discount shoopper' => "11",
			'uc supplies only shoppper' => "12",
			'see my tools' => "14",
			'see store network' => "15",
			'see catalog reference tab' => "16",
			'webform admin' => "17");
		my %auto_logout_groups=(
			'Company Store' => "5932",
			'Contract Store' => "5933",
			'Company Store Manager' => "6168",
			'Warehouse Supervisor' => "6236",
			'Content Gardener' => "7860",
			'Training Store Managers' => "9783",
			);	
		my %no_auto_logout_groups=(
			'IT' => "3028",
			'Customer Service' => "3029",
			'Purchasing' => "3030",
			'Marketing' => "3031",
			'Merchandising' => "3032",
			'Visual Merchandising' => "3033",
			'Executive Team' => "3034",
			'Sales' => "5936",
			'Artisans' => "3037",
			'Corporate Staff' => "5930",
			'Alliance Store' => "5931",
			'Larger Alliance Store' => "5940",
			'Store Board' => "6055",
			'Intranet Team' => "6112",
			'E-Commerce Team' => "6122",
			'Finance Accounting' => "6233",			
			);				
		my $corporate_staff="4";
		my $is_corporate_staff=0;
		my $should_auto_logout=0;
		my $no_auto_logout=0;
		my $name='';

		# Gather profile of this person
		$query = "
		select 			 
			name			
		from 
			users 
		where 
			uid = '$uid'
		and
			status = 1
		";
		
		$sth=$dbh->prepare("$query");		
		$sth->execute();		
		while (@tmp = $sth->fetchrow_array()) {
			$name=$tmp[0];			 
		}			
		# Role
		$query = "
		select 
			rid			
		from 
			users_roles	
		where 
			uid = '$uid'			
		";
		
		$sth=$dbh->prepare("$query");		
		$sth->execute();		
		while (@tmp = $sth->fetchrow_array()) { 	
			my $rid=$tmp[0];
			if ($auto_logout_roles{$rid}) {
				$should_auto_logout=1;
			}
			if ($rid == $corporate_staff) {
				$no_auto_logout=1;
			}
			push(@roles,$rid);
		}		
		
		# Group
		$query = "
		select 
			nid			
		from 
			og_uid	 
		where 
			uid = '$uid'			
		";
		
		$sth=$dbh->prepare("$query");		
		$sth->execute();		
		while (@tmp = $sth->fetchrow_array()) { 
			my $gid=$tmp[0];
			if ($auto_logout_roles{$gid}) {
				$should_auto_logout=1;
			}	
			if ($no_auto_logout_groups{$gid}) {
				$no_auto_logout=1;
			}														
			push(@groups,$gid);
		}	
		# By looking at their roles and groups, can we determine if they should be set to autologout?
		if ($no_auto_logout) {
			if ($verbose) {
				#LogMsg("User $name uid $uid does not need to be set for autologout");
			}
			next;
		}
		if ($should_auto_logout) {
			if ($verbose) {            
				LogMsg("WARNING: User $name uid $uid is not set for autologout but should be.");            
			}
			push(@email, "WARNING: User $name uid $uid is not set for autologout but should be.");   
			$warns++;  				
		}
	}
	
    if ($warns) {
        $warning_count++;
		LogMsg("WARNING: There were $warns issues checking users.");  
		push(@email, "WARNING: There were $warns issues checking users.");  
		push(@summary, "WARNING: There were $warns issues checking users."); 
		
    } else {
		LogMsg("There were $warns issues checking users.");  
		push(@email, "There were $warns issues checking users.");  
	
	}
    $dbh->disconnect();     
}

sub ip_update {
    # Do the things found only on The Wire
    my $wire="/var/www/****";    
    if ($hostname =~ /ttv-lamp1/) {    
        # This routine is NOT for ttv-lamp1
		if ($verbose) {
			print "This is not for $hostname\n";
		}
        return;
    }
    if (-d "$wire") {
		process_ip_updates();
	}
}

sub process_ip_updates {
	# Read the watchdog database and gather the IP addresses.  Update the store_ip_address table accordingly
	if ($verbose) {
		print "Processing IP updates...\n";
	}
	my %ip_hash=();
    my $dbh="";
    my $mysqld=`hostname`;
 
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=$database","$user","$pw")) {        
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");
        push(@email, "\nCannot connect to mysql on host $mysqld! \n");
		push(@summary, "\nCannot connect to mysql on host $mysqld! \n");
        $warning_count++;       
        return;         
    }
 
    # Get all the messages since the beginning of the previous day
    my $timeseconds=time();
    my @ltm=localtime($timeseconds);
    my @previous_day_ltm=localtime($timeseconds - 86400);
    my $previous_day=$previous_day_ltm[3]; 
    my $previous_day_month=$previous_day_ltm[4];
    my $previous_day_year=($previous_day_ltm[5]+1900);
    # Now that we have determined the previous day, find the timestamp for the begining of yesterday
    use Time::Local;
    my $previousdaytime = timelocal(0,0,0,$previous_day,$previous_day_month,$previous_day_year);
 

    # Check all watchdog messages since the beginning of the previous day
    my $query = "
    select 
        message,hostname,from_unixtime(timestamp)
    from
        watchdog
    where
        timestamp > '$previousdaytime'
	order by
		timestamp
         
	";
 
    my $update_count=0;
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (@tmp = $sth->fetchrow_array()) {            
        my $message=$tmp[0];
        my $hostname=$tmp[1];  
		my $date=$tmp[2];
		my @tmp=split(/\//,$message);
		my $storeid=$tmp[1];
		if ($tmp[0] =~ /edna/) {			
			if ($storeid =~ /[1-9][0-9][0-9][0-9]/) {				 
				$ip_hash{$storeid} = "${hostname}|${date}";
				$update_count++;
				if ($verbose) {
					print "Found $storeid $hostname\n";
				}
			}
		}
    }    
    $dbh->disconnect();  
	if ($update_count) {
		# Connect the the pos_support database
		my $mysqld="192.168.21.42";
		unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=pos_support","pos","sop")) { 
			LogMsg("Error.  Cannot connect to mysql on host $mysqld");			
			return;
		}	
		# Update the database
		foreach my $storeid (sort keys(%ip_hash)) {
			my @info=split(/\|/,$ip_hash{$storeid});
			#my $ip=$ip_hash{$storeid};
			my $ip=$info[0];
			my $date=$info[1];
			$query = "replace into store_ip_address (storeId,entryDate,IP) values ($storeid,'$date','$ip') ";
			  			
			unless ($dbh->do($query) > 0) {
				LogMsg("Database Update Error");
				LogMsg("Driver=ODBC");
				LogMsg($dbh->{Name}); 
				LogMsg("query=".$query);
				LogMsg("err=".$dbh->err."\tstate: ".$dbh->state);
				LogMsg("errstr=".$dbh->errstr);             
			} else {
				LogMsg("Updated ip address for store $storeid to $ip");				 
			}   
			
		}
	}
	
    $dbh->disconnect();        
	
	###
}

sub wire_specifics {
    # Do the things found only on The Wire
    my $wire="/var/www/****";    
    if ($hostname =~ /ttv-lamp1/) {    
        # This routine is NOT for ttv-lamp1
        return;
    }
    if (-d "$wire") {
        check_file_folder();
        my $manifest="/root/wire_manifest.txt";
        check_manifest("$wire","$manifest");
        $manifest="/root/server_manifest.txt";
        check_manifest("/root","$manifest");        
        check_catalog_sync();
		check_catalog_fields();
		check_user_sync(); 		
        check_wire_changes();
        # On monday also check these:
        #if ($ltm[6] == 1) {
            check_jpg();
            #check_missing_jpg();
			check_thumb_size();
			check_images();	
        #}
        check_wire_db();
        #check_wire_backups();  # Replaced by check_backups
        check_search_index();
		check_indexed_items();
        check_raid();
        check_wire_orders();
        check_users();
        check_wire_watchdog();
		check_current_version();
		check_memory();
    }
    
}


sub check_lamp_backups {
    # Check the lamp backup for problems
    my $warns=0;
    push(@email,"\n----Checking backup logs----"); 
    # Find all of the backup logs
    my $logdir="/var/log";
    opendir(LOGS,"$logdir");
    my @log_listing=readdir LOGS;
    closedir LOGS;    
    my $date_string=`date`;
    my @tmp=split(/\s+/,$date_string);
    # Split the date into day of week, month, day, time, year
    my $dow=$tmp[0];
    my $mon=$tmp[1];
    my $day=$tmp[2];
    my $yr=$tmp[$#tmp];
    my @key_words=(
        "error",
        "warning",
        "break-in"
    );
    my $found_beginning=0;
    my $found_complete=0;
    
    foreach my $l (@log_listing) {
        if ($l =~ /backup.log/i) {
            ###
            my $backup_log="${logdir}/${l}";
            if (-f $backup_log) {
                LogMsg("Checking backup log $backup_log...");     
                open(FILE,"$backup_log");
                while (<FILE>) {
                    chomp(my $line=$_);
                    # Find any errors in today's backups
                    next unless ($line =~ /^$dow $mon.*$day.*$yr/);         
                    foreach my $word (@key_words) {
                        if ($line =~ /$word/i) {
                            $warns++;
                            push(@email, "$line");
                            if ($verbose) {
                                print "$line\n";
                            }
                        }                    
                    }
                    if ($line =~ /backup completed/i) {
                        $found_complete=1;
                    }
                    if ($line =~ /Beginning/i) {
                        $found_beginning=1;
                    }                    
                }        
            ###
            }  else {
                $warns++;
                LogMsg("ERROR: Could not locate $backup_log");
                push(@email, "ERROR: Could not locate $backup_log");
            }
            unless ($found_beginning) {
                $warns++;
                LogMsg("WARNING: Could not determine $backup_log began");
                push(@email, "WARNING: Could not determine $backup_log began");    
            }     
            unless ($found_complete) {
                $warns++;
                LogMsg("WARNING: Could not determine $backup_log  completed");
                push(@email, "WARNING: Could not determine $backup_log  completed");    
            }            
        } 

        
    }
    if ($warns) {
        $warning_count++;
        LogMsg("There were $warns possible issues with backups");
        push(@email, "WARNING: There were $warns possible issues with backups");
		push(@summary, "WARNING: There were $warns possible issues with backups");
    } else {
        LogMsg("No issues found checking the backups.");
        push(@email, "No issues found checking the backups.");
    }
    LogMsg("Finished checking the backups..."); 
}

sub check_mail {
    # Since no one regularly logs in and reads root's mail, we need to collect it here and determine if anything is critical (if possible).
    my $warns=0;
    push(@email,"\n----Checking Root's Mail ----");     
    my $mail="/var/spool/mail/root";
    my @fatal_errors;
    my %fatal_errors_hash;
    my %cron_message_hash;
    my @cron_errors;
    if (-e $mail) {
        LogMsg("Checking mail...");
    } else {
        LogMsg("Found No $mail");
		 push(@email,"Found No $mail"); 
        return;
    }
    # Start by copying the mail file to a new file
    my @ltm = localtime();
    my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);   
    my $mail_copy="${mail}_${date_label}";
    my $increment="0";
    my $mail_copy_temp=$mail_copy;
    my $mail_count=0;
    unless ($debug) {
        while (-e $mail_copy_temp) {
            # Increment mail copy name to make certain we don't over-write an existing file
            $increment++;
            $mail_copy.="_${increment}";
            $mail_copy_temp=$mail_copy;
        }
    
        if (move($mail,$mail_copy)) {        
            LogMsg("Moved $mail to $mail_copy");    
        } else {
            LogMsg("Error moving $mail to $mail_copy");
            return;
        }            
    } else {
        print "Reading $mail_copy\n";
        # Read mail instead of mail copy
        #$mail_copy=$mail;
    }

    # Now, read the mail
    open(MAIL,"$mail_copy");
    my @mail_info=(<MAIL>);
    close MAIL;
    my $new_mail_message=0;
    my $cron_mail_found=0;
    my $cron_cmd;
    for (my $x=0; $x<=$#mail_info; $x++)  {
        chomp(my $line=$mail_info[$x]);            
        chomp(my $next_line=$mail_info[($x+1)]);
        if (($line =~ /^from/i) && ($line =~ /:/) && ($line =~ /[1-9][0-9][0-9][0-9]$/)) {
            # This is the beginning of an email message
            $mail_count++;
            $new_mail_message=1;
            $cron_mail_found=0;
        }
        if ($new_mail_message) {
            if ($line =~ /The following addresses had permanent fatal errors/) {
                # Get the address off of the next line
                my $address=$next_line;                
                $address =~ s/^\<//;
                $address=~ s/\>$//;          
                $fatal_errors_hash{$address}=1;
            }
            if (($line =~ /^Subject:/) && ($line =~ /Cron/)) {
                # This is a report from cron 
                $cron_mail_found=1;
                my @tmp=split(/\s+/,$line);  
                my $index=1;                
                $cron_cmd=$tmp[-$index];
                while ($tmp[-$index] =~ /^-/) {
                    $index++;
                    $cron_cmd=$tmp[-$index];
                }
                $cron_message_hash{$cron_cmd}++;                             
            }
        }
        if ($cron_mail_found) {
            # Check for errors reported
            if ($line =~ /fail/i) {
                push(@cron_errors,"$cron_cmd".":"." $line");
            }
        }
    }
    my $fatal_count=keys(%fatal_errors_hash);    
    LogMsg("There were $fatal_count addresses reported to root mail that have fatal errors.");
    push(@email,"There were $fatal_count addresses reported to root mail that have fatal errors.");     
    if ($verbose) {
        foreach my $address (sort keys(%fatal_errors_hash)) {
            print "$address\n";
        }
    }
 
    my $cron_count=keys(%cron_message_hash); 
    LogMsg("There were $cron_count cron reports in root mail.");
    push(@email,"There were $cron_count cron reports in root mail.");    
    if ($verbose) {
        foreach my $address (sort keys(%cron_message_hash)) {
            print "$address\n";
        }
    }        
 
    my $cron_errors=$#cron_errors;
    $cron_errors++;
    if ($cron_errors) {
        $warns++;
    }
    LogMsg("There were $cron_errors cron errors detected.");
    push(@email,"There were $cron_errors cron errors detected.");     
 
    foreach my $error (@cron_errors) {
        if ($verbose) {
            print "$error\n";    
        }
        push(@email,"$error\n");            
    }    
    if ($warns) {
        LogMsg("WARNING: Found $warns possible issues listed in root's mail");
        push(@email,"WARNING:  Found $warns possible issues listed in root's mail: $mail_copy\n");
		push(@summary,"WARNING:  Found $warns possible issues listed in root's mail: $mail_copy\n");
        $warning_count++;
    } else {
        push(@email,"Found $warns issues with root's mail.\n");
    }    
 
    LogMsg("Finished checking mail");
}

sub check_mail_sent {
    # All of the mail sent is logged in /var/log/maillog.  Create a report for the mail sent 
    my $warns=0;
    my @log_list;
    my $mail_date;
    # See what mail was sent yesterday

    my $timeseconds=time();
    my @ltm=localtime($timeseconds);
    my @previous_day_ltm=localtime($timeseconds - 86400);
    my $previous_day=$previous_day_ltm[3];  
    my $current_day=$ltm[3];
    my $year=($ltm[5]+1900);
    my $day=$ltm[3];    
    my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
    my $month=$abbr[$ltm[4]];    
    my $previous_day_month=$abbr[$previous_day_ltm[4]];    
    if ($specified_mail_log) {        
        @log_list=("$specified_mail_log"); 
    } else {  
        # Need to read all logs just to be safe
        @log_list=();
        opendir (DIR,"/var/log");
        my @file_listing=readdir DIR;
        close DIR;
        foreach my $file (@file_listing) {              
            if ($file =~ /maillog/) {
                my $l = "/var/log/${file}";                                
                push (@log_list,$l);                
            }
        }              
    }
 
    push(@email,"\n----Checking SendMail----");
    my @mail_array;
    foreach my $log (@log_list) {  
        next unless ($log);
        if (-e $log) {
            LogMsg("Checking $log ...");            
            my $error_counter=0;
            my $cmd = "grep \"^$month \*$previous_day\" $log";
            if ($all_dates) {
                $cmd = "cat $log";
            }     
            if ($current_date) {
               $cmd = "grep \"^$month \*$current_day\" $log";
            }
            my @log_info=`$cmd`;
            if ($debug) {
                print "Command is ($cmd)\n";
            }
            my $counter=0;
            foreach my $label ("Sent","Deferred") {               
                foreach my $l (@log_info) {               
                    # Some filtering to find the email address, when it was sent, and whether it was deferred or not
                    chomp($l);                                                        
                    if ($l =~ /stat=$label/i) {
                        if ($l =~ /bounce\@list.tenthousandvillages.com/) {
                            next;
                        }                     
                        my @tmp=split(/\s+/,$l);
                        $mail_date="$tmp[0] "."$tmp[1] "."$tmp[2]";
                        my $address=$tmp[6];
                        if (($address =~ /to=root,/) || ($address=~ /to=<root@/) || ($address=~ /webmgr\@tenthousandvillages/) || ($address=~ /postmaster/)) {
                            next;
                        }                                           
                        @tmp=split(/=/,$address);                        
                        $address=$tmp[1];
                        $address=~s/<//;
                        $address=~s/>//;  
                        $address=~s/,$//;
                        $address=~s/\\\\'/''/;  
                        $address=~s/'{1}/''/;
                        my @array=("$address","$label","$mail_date");                         
                        push(@mail_array,\@array);
                        $counter++;          
                        if ($debug) {
                            print "entry array (@array) counter ($counter)\n";
                        }
                        if ($counter > 25) {
                            # Every ten emails, send the array to update the database
                            # This reduces the number of calls to the database somewhat
                            #print "Press enter to update db\n";
                            #my $p=(<STDIN>);
                            unless (update_mail_db(\@mail_array)) {
                                $error_counter++;
                            }
                            # Reset things
                            $counter=0;
                            @mail_array=();
                        }         
                        #sleep 1;                       
                    } 
                
                }
            }  
            # Process any that are left
            if ($#mail_array > -1) {            
                unless (update_mail_db(\@mail_array)) {
                    $error_counter++;
                }            
            }
            LogMsg("There were $error_counter database update errors with log $log");
            
            if ($error_counter) {
                $warns++;
            }
            
        } else {
            LogMsg("Failed to locate $log.");
            push(@email,"Failed to locate $log.");     
            $warns++;
        }
    }
    if ($warns) {
        LogMsg("WARNING: Found $warns issues checking sendmail");
        push(@email,"WARNING:  Found $warns issues checking sendmail\n");
		push(@summary,"WARNING:  Found $warns issues checking sendmail\n");
        $warning_count++;
    } else {
        push(@email,"Found $warns issues checking sendmail.\n");
    }        

    LogMsg("Finished checking mail sent");     
}
 

sub update_mail_db {
    
    my $ref=shift;
    # Database connection info
    my $dbh="";
    my $mysqld="192.168.21.42";     # ttv-lamp1
    my $dbname="phplistdb";    
    my $user="listtools";
    my $pw="showmethemail";       
    unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$dbname","$user","$pw")) {
 
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");   
        return 2;
    }    
    my @mail_array=@$ref;

    my $table="sendmail_history";
    my $values_list;
    my $sep;
    my $count=0;
    my $return_code=0;

    
    foreach my $m (@mail_array) {
        # Each line in the mail_array is an entry.
        my $email=$$m[0];    
        my $send_result=$$m[1];
        my $date_processed=$$m[2]; 
        # Create the values for one entry
        
        my $values="(\'$email\',\'$send_result\',\'$date_processed\')";    
        # Append the values for each entry
        $values_list.="$sep"."$values";
        $sep=",";
        $count++;       
    }

    # Now add the new data
    my $query = "REPLACE INTO $table VALUES $values_list ";   
    if ($dbh->do($query)) {                    
        $return_code= 1;
    } else {
        # One drawback of processing a bunch at a time is that if one fails, they all fail.  Here we try to process each address
        # separately so that we don't lose them all
        LogMsg("Error trying to replace into $table.  Trying update_mail_db_II...");
        $return_code=update_mail_db_II($ref); 
    }    
    $dbh->disconnect;    
    return $return_code;
}

sub update_mail_db_II {
    
    my $ref=shift;
    # Database connection info
    my $dbh="";
    my $mysqld="192.168.21.42";     # ttv-lamp1
    my $dbname="phplistdb";    
    my $user="listtools";
    my $pw="showmethemail";       
    if ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$dbname","$user","$pw")) {
        if ($verbose) {
            LogMsg("Connected to host $mysqld...");
        }
    } else {
        LogMsg("Error.  Cannot connect to mysql on host $mysqld");   
        return 2;
    }    
    my @mail_array=@$ref;

    my $table="sendmail_history";
    my $values_list;
    my $sep;
    my $count=0;
    my $return_code=0;
    
    foreach my $m (@mail_array) {
        # Each line in the mail_array is an entry.
        my $email=$$m[0];    
        my $send_result=$$m[1];
        my $date_processed=$$m[2]; 
        # Create the values for one entry        
        my $values="(\'$email\',\'$send_result\',\'$date_processed\')";
        my $query = "REPLACE INTO $table VALUES $values ";
        if ($dbh->do($query)) {                    
            if ($verbose) {
                print "Added $count entries to $table\n";                       
            }
            $return_code= 1;
        } else {     
            LogMsg("ERROR: Failed to update table $table"); 
            LogMsg("Query: $query "); 
            $return_code= 0;
        }    
    }
    $dbh->disconnect;    
    return $return_code;
}

sub lamp_specifics {
    # These functions are only for the ttv-lamp1 system
    my $name=`hostname`;
    if ($hostname =~ /ttv-lamp1/) {    
        check_mail_sent();
    }
    
}

sub itwire_specifics {
    # These are the only valid functions for the itwire system
    my $name=`hostname`;
    if ($hostname =~ /itwire/) {    
        check_uptime();
        check_updates();
		check_current_version();
    }    
}

sub check_sar {
	# Check system activity reports
	# Check memory usage for yesterday and this morning
    LogMsg("Checking sar...");
    push(@email,"\n----Checking SAR----");	

	######################################	
    my $timeseconds=time();
    my @previous_day_ltm=localtime($timeseconds - 86400);
    my $previous_day=sprintf("%02d",$previous_day_ltm[3]);   	
	my $safile="/var/log/sa/sa".$previous_day;
	my @sar_info;
	my $cmd="sar -r -f $safile";
	if ($verbose) {
		print "Sar file ($safile)\n";
	}	
	if (-f $safile) {
		@sar_info=`$cmd`;
	} else {
        LogMsg("WARNING: Could not find $safile");
        push(@email,"WARNING: Could not find $safile\n");
		push(@summary,"WARNING: Could not find $safile\n");
        $warning_count++;	
		return;
	}
 
	# Total the memory used for yesterday
	my $count=0;
	my $memtotal=0;
	my $memory_max=0;
	my $memory_min=0;
	my $average=0;
	foreach my $s (@sar_info) {
		if ($s =~ /memused/) {
			# This is the column header
			next;
		}
		my @tmp=split(/\s+/,$s);
		# The average will be totaled at the bottom
		if ($s =~ /^Average/) {
			# The % memory used is in the 4th column			
			if ($tmp[3]) {
				$average=$tmp[3];
			}			
		} else {
			my $mem=$tmp[4];
			if ($mem > $memory_max) {
				$memory_max=$mem;
			}
			if ($memory_min) {
				if ($mem < $memory_min) {
					$memory_min = $mem;
				}
			} else {
				$memory_min = $mem;
			}
		}
 
	}
 
	LogMsg("Memory usage for yesterday averaged $average percent.  Max: $memory_max - Min: $memory_min");
	push(@email,"Memory usage for yesterday averaged $average percent.  Max: $memory_max - Min: $memory_min\n");
	
	######################################
	# Now get the stats for today:
	@sar_info=();
	$cmd="sar -r ";
 
	if (-f $safile) {
		@sar_info=`$cmd`;
	} else {
        LogMsg("WARNING: Could not find $safile");
        push(@email,"WARNING: Could not find $safile\n");
		push(@summary,"WARNING: Could not find $safile\n");
        $warning_count++;	
		return;
	}
 
	# Total the memory used for today
	$count=0;
	$memtotal=0;
	$memory_max=0;
	$memory_min=0;
	$average=0;	
	foreach my $s (@sar_info) {
		if ($s =~ /memused/) {
			# This is the column header
			next;
		}
		my @tmp=split(/\s+/,$s);
		# The average will be totaled at the bottom
		if ($s =~ /^Average/) {
			# The % memory used is in the 4th column			
			if ($tmp[3]) {
				$average=$tmp[3];
			}
		} else {
			my $mem=$tmp[4];
			if ($mem > $memory_max) {
				$memory_max=$mem;
			}
			if ($memory_min) {
				if ($mem < $memory_min) {
					$memory_min = $mem;
				}
			} else {
				$memory_min = $mem;
			}
		}
	}
 
	LogMsg("Memory usage for today average $average percent.  Max: $memory_max - Min: $memory_min");
	push(@email,"Memory usage for today average $average percent.\n");
	
}

sub check_slow_log {
    LogMsg("Checking slow log...");
    push(@email,"\n----Checking slow log----");	
	my $slow_log="/var/lib/mysql/mysqld-slow.log";
	unless (-f $slow_log) {		
        LogMsg("WARNING: Could not find $slow_log");
        push(@email,"WARNING: Could not find $slow_log\n");
		push(@summary,"WARNING: Could not find $slow_log\n");
        $warning_count++;	
		return;
	}
	my $tool="/usr/bin/mysqldumpslow";
	unless (-f $tool) {		
        LogMsg("WARNING: Could not find $tool");
        push(@email,"WARNING: Could not find $tool\n");
		push(@summary,"WARNING: Could not find $tool\n");
        $warning_count++;	
		return;
	}	
	# Now see which queries had the largest count
	my $cmd="$tool $slow_log";
	my @slow_info=`$cmd`;
	
	# Parse through the info
	my $max_count=0;
	my $max_count_slow_query;
	my $max_slow=0;
	my $max_slow_query;
	my $max_count_read=0;
	my $max_slow_read=0;
	my %slow_hash;
	foreach my $s (@slow_info) {
		if ($s =~ /^Count/) {
			# Turn off the read in case it was on.
			$max_count_read=0;
			$max_slow_read=0;
			my @tmp=split(/\s+/,$s);
			my $count=$tmp[1];
			my $time=$tmp[3];
			if ($count > $max_count) {
				$max_count=$count;
				# Record the query (turn on the read)
				$max_count_read=1;
				$max_count_slow_query='';
			}
			$time=~s/\(//g;
			$time=~s/\)//g;
			$time=~s/s//g;
			if ($time > $max_slow) {
				$max_slow=$time;
				# Record the query
				$max_slow_read=1;
				$max_slow_query='';
			}
			if ($time > 0) {
				$slow_hash{$time}=$count;
			}
		}
		if ($max_slow_read) {
			$max_slow_query.=$s;
		}	
		if ($max_count_read) {
			$max_count_slow_query.=$s;
		}
	}
	LogMsg("Slowest query was $max_slow seconds");
	print "Slowest query: $max_slow_query\n";
    push(@email,"Slowest query was $max_slow seconds");	
    push(@email,"$max_slow_query");		
	LogMsg("Most frequent slow query was seen $max_count times");
	print "Most frequent slow query: $max_count_slow_query\n";
    push(@email,"Most frequent slow query was seen $max_count times");	
    push(@email,"$max_count_slow_query");		
	# Show the distribution of the 10 slowest queries
	my $count=0;
	push(@email,"\nSlow Query Stats:");	
	LogMsg("Slow Query Stat:");
    foreach my $s (reverse sort {$slow_hash{$a}<=>$slow_hash{$b}} keys (%slow_hash)) {
		$count++;
		push(@email,"Times seen: $slow_hash{$s} - Execution Time: $s");	
		print " Times seen: $slow_hash{$s} - Execution Time: $s\n";        
		last if ($count >= 10);        
    }

}

sub check_current_version {
	unless ($test_mode) {
		unless ($day_of_week == 1) {
			# We do not need to collect this info every day
			return;
		}
	}
	# Report the current version of certain key modules
    LogMsg("Checking versions");
    push(@email,"\n----Checking versions----");		
	LogMsg("php info:");
	push(@email,"\nphp info:");		
	my $cmd="php -v";
	my @info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		LogMsg("$i");
		push(@email,"$i");			
	}
	LogMsg("apache info:");
	push(@email,"\napache info:");	
	$cmd="apachectl -V";
	@info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		LogMsg("$i");
		push(@email,"$i");			
	}	
	LogMsg("mysql info:");
	push(@email,"\nmysql info:");		
	$cmd="mysql -V";
	@info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		LogMsg("$i");
		push(@email,"$i");			
	}	
	LogMsg("Linux Kernel info:");
	push(@email,"\nLinux Kernel info:");		
	$cmd="uname -rv";
	@info=`$cmd`;
	foreach my $i (@info) {
		LogMsg("$i");
		push(@email,"$i");			
	}		
	# Now get drupal info
	LogMsg("Drupal info:");
	push(@email,"\nDrupal info:");		
	my $dir="/var/www/****/modules";
	chdir($dir);
	$cmd="grep -r \"version =\" *";
	@info=`$cmd`;
	my %version_hash;
	foreach my $i (@info) {
		if ($i =~ /info:version/) {
			my @tmp=split(/\//,$i);
			my $module=$tmp[0];
			@tmp=split(/\s+/,$i);
			my $version=$tmp[-1];
			unless ($version eq "VERSION") {
				$version_hash{$module}=$version;
			}			
		}		
	}	
	foreach my $module (sort keys(%version_hash)) {
		my $version=$version_hash{$module};
		LogMsg("Module: $module - Version: $version");
		push(@email,"Module: $module - Version: $version");		
	}
	LogMsg("Drupal Module info:");
	push(@email,"\nDrupal Module info:");		
	$dir="/var/www/****/sites/all/modules";	
	chdir($dir);
	$cmd="grep -r \"version =\" *";
	@info=`$cmd`;
	%version_hash=();
	foreach my $i (@info) {
		if ($i =~ /info:version/) {
			my @tmp=split(/\//,$i);
			my $module=$tmp[0];
			@tmp=split(/\s+/,$i);
			my $version=$tmp[-1];
			unless ($version eq "VERSION") {
				$version_hash{$module}=$version;
			}			
		}		
	}	
	foreach my $module (sort keys(%version_hash)) {
		my $version=$version_hash{$module};
		LogMsg("Module: $module - Version: $version");
		push(@email,"Module: $module - Version: $version");		
	}	
	LogMsg("Network Driver info:");
	push(@email,"\nNetwork Driver info:");		
	$cmd="ethtool -i eth0";
	@info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		LogMsg("$i");
		push(@email,"$i");		
	}
	LogMsg("Network Adapter info:");
	push(@email,"\nNetwork Adapter info:");		
	$cmd="/sbin/lspci -v | grep Ethernet ";
	@info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		LogMsg("$i");
		push(@email,"$i");		
	}	
}

sub check_memory {
    push(@email,"\n----Checking Memory----");	
	# Check the memory settings
	my $cmd="/sbin/sysctl -a | grep vm.min_free_kbytes";
	chomp(my $meminfo=`$cmd`);
	my @tmp=split(/\s+/,$meminfo);
	$meminfo=$tmp[-1];	
	push(@email,"vm.min_free_kbytes: $meminfo");	
	$cmd="/sbin/sysctl -a | grep vm.swappiness";
	chomp($meminfo=`$cmd`);
	@tmp=split(/\s+/,$meminfo);
	$meminfo=$tmp[-1];	
	push(@email,"vm.swappiness: $meminfo");	
}

sub check_images {
	# Check that every active SKU has an image
	# Check that every SKU with an image has both a large and a thumbnail
	# Check that the thumbnail is smaller than the large image.
    my $warns=0;
	my @active_sku_list=();
    my $thumbpath="/var/www/****/sites/default/files/thumbs";
	my $largepath="/var/www/****/sites/default/files/large";		
	my @missing_thumb=();
	my @missing_large=();
	my @too_large_small=();
	my %wrong_large_one=();
	my %wrong_large_two=();
	
    my $dbh="";
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=wire","wiretestu","wiretestupww")) {        
        LogMsg("Error.  Cannot connect to mysql on host localhost");
        push(@email, "\nCannot connect to mysql on host localhost! \n");
		push(@summary, "\nCannot connect to mysql on host localhost! \n");
        $warning_count++;       
        return;         
    }    	
	# Find the active SKUs
	my $query="
		select 
			n.title 
		from 
			node n
			join content_type_item cti on n.nid = cti.nid
		where  
			n.type like 'item'
		and
			n.status = 1
		and			
			cti.field_item_status_value in ('Active')
		and
			cti.field_travelers_find_value not like 'Yes'			
		and
			cti.field_item_type_value like 'Product'
		and
			cti.field_item_ionhand_value > 0 
		";  
		#cti.field_item_status_value in ('Active','Future')		
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	while (@tmp = $sth->fetchrow_array()) { 
		my $title=$tmp[0];
		push(@active_sku_list,$title);						
	}  	
	foreach my $sku (@active_sku_list) {
		my $thumbfile="${thumbpath}/${sku}".".jpg";
		my $largefile="${largepath}/${sku}"."w1.jpg";
		my $largefile1="${largepath}/${sku}"."W1.jpg";		
		my $largefile2="${largepath}/${sku}"."wi.jpg";		
		unless (-f $thumbfile) {
			push(@missing_thumb,$sku);
		}
		unless (-f $largefile) {
			if (-f $largefile1) {
				$wrong_large_one{$sku}=$largefile1;
			}
			if (-f $largefile2) {
				$wrong_large_two{$sku}=$largefile2;
			}			
			push(@missing_large,$sku);
		}		
	}
	foreach my $sku (@missing_thumb) {
		push(@email, "missing thumbnail for ($sku)");	
		push(@summary, "missing thumbnail for ($sku)");	
		$warns++;
	}
	foreach my $sku (@missing_large) {
		push(@email, "missing large image for ($sku)");		
		push(@summary, "missing large image for ($sku)");	
		$warns++;
	}
	foreach my $sku (sort keys(%wrong_large_one)) {		
		my $largepath="/var/www/****/sites/default/files/large";	
		push(@email, "Found $wrong_large_one{$sku} rather than $largepath\n");
		$warns++;
	}
	foreach my $sku (sort keys(%wrong_large_two)) {		
		my $largepath="/var/www/****/sites/default/files/large";	
		push(@email, "Found $wrong_large_two{$sku} rather than $largepath\n");		
		$warns++;
	}	
    if ($warns) {	
        push(@email, "WARNING: Found $warns issues checking item images\n");
		push(@summary, "WARNING: Found $warns issues checking item images\n");			
        $warning_count++;
    } else {
       push(@email, "Found no issues checking images \n");
    }
    LogMsg("Finished checking images."); 	
	$dbh->disconnect;	
}

sub check_thumb_size {  
    my $warns=0;
    my $dbh="";
    unless ($dbh=DBI->connect("DBI:mysql:host=localhost;database=wire","wiretestu","wiretestupww")) {        
        LogMsg("Error.  Cannot connect to mysql on host localhost");
        push(@email, "\nCannot connect to mysql on host localhost! \n");
		push(@summary, "\nCannot connect to mysql on host localhost! \n");
        $warning_count++;       
        return;         
    }    	
    my $large_size_count=0; 
    my $thumbpath="/var/www/****/sites/default/files/thumbs";
	my $largepath="/var/www/****/sites/default/files/large";
	my $thumb_size_limit=28000;
	my $large_size_min=20000;
	my %large_thumb_hash;
	my @sku_list;
 
	my $dir="$thumbpath";
 
	if (-d $dir) {
		 LogMsg("Checking thumbnail image sizes.");
		#chdir($dir);
		opendir (DIR,"$dir");
		my @tmp=readdir DIR;
		my @dirinfo=sort { $a <=> $b } @tmp;
		closedir DIR;            
		foreach my $file (@dirinfo) {                
			next unless ($file =~ /.jpg$/);
			my $filepath="${thumbpath}/${file}";
			my @file_info = stat($filepath);
			my $file_size=$file_info[7];
			if ($file_size > $thumb_size_limit) {				
				$large_thumb_hash{$file}=$file_size; 				
			} 			 	
		}
	} else {
		push(@email, "WARNING: failed to locate $dir\n");
		$warns++;
	}
	# See if these are active skus	
	foreach my $item (sort keys(%large_thumb_hash)) {
		my $size = $large_thumb_hash{$item};
		my @tmp=split(/\./,$item);
		my $sku=$tmp[0];
		next if ($sku =~ /w1/);
		#$sku =~ s/w1$//;
		# Check to see if this is an active sku
		my $query="
			select 
				n.title 
			from 
				node n
				join content_type_item cti on n.nid = cti.nid
			where  
				n.type like 'item'
			and
				n.title like '$sku'
			and
				n.status = 1
			and
				cti.field_item_status_value in ('Active','Future')
			and
				cti.field_travelers_find_value not like 'Yes'			
			and
				cti.field_item_type_value like 'Product'				
			";        
		my $sth=$dbh->prepare("$query");
		$sth->execute();
		while (@tmp = $sth->fetchrow_array()) { 
			my $title=$tmp[0];
			push(@sku_list,$title);	
			$large_size_count++;			
		}      				
	}
 
    push(@email,"\n----Checking image sizes----");  
    if ($large_size_count) {
        LogMsg("WARNING: Found $large_size_count images the wrong size");
        #LogMsg("$good_size_count images where of acceptable size");
        push(@email, "WARNING: Found $large_size_count images the wrong size\n");
		push(@summary, "WARNING: Found $large_size_count images the wrong size\n");
        $warning_count++;
		if ($large_size_count < 100) {
			push(@email, "@sku_list");
			push(@email,"\n");
		}
    } else {
       push(@email, "Found $warns images the wrong size\n");
    }	
    if ($warns) {
        #LogMsg("NOTICE: Found $warns issues checking thumbnail image sizes");
        #LogMsg("$good_size_count images where of acceptable size");
        push(@email, "WARNING: Found $warns other issues while checking thumbnail image sizes\n");
		push(@summary, "WARNING: Found $warns other issues while checking thumbnail image sizes\n");
        $warning_count++;
    }
    LogMsg("Finished checking image sizes."); 	
	$dbh->disconnect;
}

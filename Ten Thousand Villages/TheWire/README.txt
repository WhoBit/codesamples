The Wire is a web server accessible from the Internet.  On of its many functions is to provide
reports to corporate and store staff.

I have been involved with this server to some degree from its beginning.  

In this folder are four scripts:
- monitor.pl - A Perl script I developed to help me monitor the status of the Wire server 
and website.
- WebFormSubmissionReport.php - This is a code fragment from a larger report file.  I included
this because it is an example of a report that I added to this report file.
- WhoHasRole.php - Another code fragment from the same report file.  A simple report to help
me see who is in what group and has what role.
- WireBackup.sh - A bash script that was created before I came but one that I modified.  It
is used to create backups of the Wire.

The nearly all of my PHP programming at Villages was on The Wire.  The majority of that
work was in creating reports such as the WebFormSumissionReport and the WhoHasRole or updating
existing reports.  In a few cases, we actually modified PHP code in Drupal modules.

2016-04-06 - kdg

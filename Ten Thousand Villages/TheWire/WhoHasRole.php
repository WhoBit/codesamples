<?php
/**
 * Who has a role form definition 
 */
function villages_whohasrole_report() {
    global $user;
    if (!is_corporate_staff($user->uid) ) {  
        drupal_set_message(t('Sorry, you do not have access to this report.'), 'error');               
        return; 
    }   
    $form = array();
    # Find the roles
    $result = db_query("select name, rid from role where rid>2 order by name");
    $roles_array = array();
    while ($roles = db_fetch_object($result)) {
        $roles_array["$roles->rid"] = "$roles->name";
    }        
    # Find the groups    
    $sql="SELECT title,nid FROM {node} WHERE type = '%s' order by type";
    $type='group';     
    $result = db_query($sql,$type);
    $groups_array = array();
    $groups_array["ANY"] = "Any Group";
    while ($group = db_fetch_object($result)) {
        $groups_array["$group->nid"] = "$group->title";      
    }            
    $form['rolecode'] = array(
         '#title' => t("Role"),
         '#type' => 'select',
         '#options' => $roles_array,
         );     
    $form['groupcode'] = array(
         '#title' => t("Group"),
         '#type' => 'select',
         '#options' => $groups_array,
         );            
    $form['description'] = array(
         '#type' => 'markup',
         '#value' => '<p>'. t('Choose a role on The Village Wire and get a list of those who have this role.<br>
         Select a group to see who in this group has this role.') .'</p>',
         );
    $form['submit'] = array(
         '#type' => 'submit',
         '#value' => t('Submit'),
         );    
    return $form;
}

/**
 *  Who has a role validation. Any validation done in the external form could be done here instead...
 */
function villages_whohasrole_report_validate($form_id, $form_values) {
}

/**
 *  Who  has a role form processing 
 */
function villages_whohasrole_report_submit($form_id, $form_values) {
   //$groupid = 'NONE';
   $roleid = $form_values['rolecode'];
   $groupid = $form_values['groupcode'];
   return "reports/mytools/whohasrole/result/$roleid/$groupid";
}

function villages_whohasrole_report_page($roleid,$groupid)  {
     
    $data = '';    
    $rolename;
    $grouptitle;
    /////////////////////
    # Get the name of the role
    if ($groupid == "ANY") {        
        $result = db_query("select name from role where rid = $roleid");

    } else {
        # See who has this role and is also in the specified group        
        $result = db_query("select name from role where rid = $roleid");        
        $sql="SELECT title FROM {node} WHERE nid = '%d'";        
        $result2 = db_query($sql,$groupid);
        while ($group = db_fetch_object($result2)) {
            $grouptitle = "$group->title";
        }               
    }
        
    #$rolename = $userdata->name;
    while ($roles = db_fetch_object($result)) {
        $rolename = "$roles->name";
    }        
    $data .= "<h3>specified role: $rolename</h3><br />";
    if ($grouptitle) {
        $data .= "<h3>specified group: $grouptitle</h3><br />";
    }

    //number of results per page
    $objects_per_page = 40;
    // The Query
    $myquery = " select u.uid, u.name, p12.value as company, p1.value as jobtitle,              
                    u.mail, u.created, u.login, u.status 
                from 
                    users u 
                    left join users_roles ur on u.uid = ur.uid 
                    left join profile_values p12 on u.uid = p12.uid and p12.fid=12 
                    left join profile_values p1 on u.uid = p1.uid and p1.fid=1
                where 
                    ur.rid = $roleid
                order by 
                    u.name ";

    //echo "$myquery <br />";
    if ($grouptitle) {
        # Check group also
        $myquery = "select 
                        u.uid, u.name, p12.value as company, p1.value as jobtitle,              
                        u.mail, u.created, u.login, u.status 
                    from 
                        users u 
                        join og_uid ou on u.uid = ou.uid
                        left join users_roles ur on u.uid = ur.uid 
                        left join profile_values p12 on u.uid = p12.uid and p12.fid=12 
                        left join profile_values p1 on u.uid = p1.uid and p1.fid=1
                    where 
                        ur.rid = $roleid
                        and ou.nid = $groupid
                    order by 
                        u.name ";

    }
    $result = db_query($myquery);
    
    // Initialize variables:
    $datatable = '';
    $datarows = '';
    $datacount = 0;
    
        
    while ($userdata = db_fetch_object($result)) {

        $uid = $userdata->uid;
        $name = $userdata->name;
        $email = $userdata->mail;
        # I don't see where company is used so I removed it - kdg 2008-10-08
        #$company = $userdata->company;
        # The following line was the datarow for company
        #<td valign=top><small>$company</small></td>
        # The following line was the datatable for company
        #<th>Company Name</th>                 
        $position = $userdata->jobtitle;
        $created = $userdata->created;
        $created_date = date("M d Y", $created);
        $login = $userdata->login;
        $login_date =  date("M d Y", $login) ;
        $status = $userdata->status;
        $datacount++;
        $datarows .= "
                <tr>
                <td valign=top><small><a href='/user/$uid/edit'>$uid</a></small></td>
                <td valign=top><small><a href='mailto:$email'>$name</a></small></td>
       
                <td valign=top><small>$position</small></td>
                <td valign=top><small>$status</small></td>
                <td valign=top><small>$created_date </small></td>
                <td valign=top><small>$login_date</small></td>
                </tr>
                ";
       }
    $datatable .= "       
    <table>
       <tr>
           <th>User ID</th>
           <th>User Name/E-mail</th>

           <th>Position</th>
           <th>Status</th>
           <th>Date Added</th>
           <th>Last Access</th>
        </tr>
    </thead>
    <tbody>

   ";
    $data .= "<h3>Total Count: $datacount</h3><br />";
    $data .=  "$datatable";
    $data .=  "$datarows";
    $data .=  "</tbody>";
    $data .=  "</table>";
   /////////////////////
   
 
   return t($data);
   
}   
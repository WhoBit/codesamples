<?php 
/**
 * Webform Submission Report form definition 
 */
function villages_webformsub_report() {
    global $user;
    $my_storeid=villagereports_get_id($user->uid);  

		
	$my_start = strtotime('-1 month');
	$my_end = strtotime('-1 day');  

	$form = array();

	$form['start_date'] = array(
        '#title' => t("Start date"),
        '#type' => 'date',
        '#length' => 10,
        '#default_value' => array(
		'month' => date('n', $my_start),
		'day' => date('j', $my_start),
		'year' => date('Y', $my_start) )
        );
	$form['end_date'] = array(
        '#title' => t("End date"),
        '#type' => 'date',
        '#length' => 10,
        '#default_value' => array(
		'month' => date('n', $my_end),
		'day' => date('j', $my_end),
		'year' => date('Y', $my_end) )
        );
 
   $form['webform_opt_in'] = array(
         '#title' => t("OPT IN Webforms"),
         '#type' => 'select',      
         '#options' => villages_parm('opt-in'),
         );  
/*  
	$form['webform'] = array(
         '#title' => t("All Webforms"),
         '#type' => 'select',      
         '#options' => villages_parm('webform'),
		);          
*/
 	
	if (is_corporate_staff($user->uid))  {
		$form['description'] = array(
			'#type' => 'markup',
			'#value' => '<p>'. t('This report shows webform submissions for the OPT IN form selected above for the store selected below.  
			<br />Leave the form selection blank to find any form.  Leave the store selection blank to select from any store.') .'</p>',
			);    	
		$form['store_id'] = array(
            '#title' => t("Store ID"),
            '#type' => 'textfield',
            '#length' => 6, 			   
            );

            
	} elseif ($my_storeid) { 
		$form['description'] = array(
			'#type' => 'markup',
			'#value' => '<p>'. t('This report shows webform submissions for the form selected above for your store for a specified period of time.') .'</p>',
			);    
		$form['store_id'] = array(
            '#type' => 'value',
            '#value' => villagereports_get_id($user->uid),
            );  
	} else {
        # If there is no store id associated with this account, let them know.
        $form['description'] = array(
        '#type' => 'markup',
        '#value' => '<p>'. t('You do not have a store ID set for your account.') .'</p>',
        );
        # Clear these parts of the form
        $form['qtr'] = array();
        $form['year'] = array();
        return $form;
    }
   
	$form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
        );       
	return $form;
}

/**
 * Webform Submission Report validation. None really needed...
 */
function villages_webformsub_report_validate($form_id, $form_values) {
}

/**
 * Webform Submission Report form processing.
 */
function villages_webformsub_report_submit($form_id, $form_values) {

	$start_array = $form_values['start_date'];
	$startdate = $start_array['year'].'-'.$start_array['month'].'-'.$start_array['day'];
	$end_array = $form_values['end_date'];
	$enddate = $end_array['year'].'-'.$end_array['month'].'-'.$end_array['day'];
	$storeselection = $form_values['store_id'];	 
	$webform = $form_values['webform'];
    $webform_opt_in = $form_values['webform_opt_in'];
 

	# use this return statement to stays within Drupal and do your processing in the villages_gcardact_report_page function
	return "reports/myreports/webformsub/result/$startdate/$enddate/$storeselection/$webform/$webform_opt_in";
      
}

/**
 * Webform Submission Report results display.
 */
function villages_webformsub_report_page($startdate, $enddate, $storeselection, $webform, $webform_opt_in)  {
   
	$data = "";	
 
	###
	# Query for the results of the webform selected.
	if ($webform_opt_in) {
		$webform=$webform_opt_in;
	}
 	
	$query = "select title from node where nid = '$webform'";
	$result = db_query($query);

	$show_data=0;
	$titlewhere = " n.title like '%opt-in%' or n.title like '%opt in%')  ";	
	if ($productdata = db_fetch_object($result)) {
		$title = $productdata->title;        			
	}      
 
	if ($title) {			
		$titlewhere = "ws.nid = '$webform'";
		$data.="Webform Selected: ".$title."<br />";
		
	} else {
		$titlewhere = " (n.title like '%opt-in%' or n.title like '%opt in%')  ";
		$titleselect = "n.title as 'Webform',";
		$nidselect = "ws.nid as 'nid',";
		$data.="Webform Selected: Any Opt In Webform<br />";
		$show_any=1;
	}
	 
	if ($storeselection) {	
		$storewhere = " and ws.uid in (select uid from profile_values where value like '$storeselection' and fid = 4) ";
		$data .= "Store Selection: $storeselection<br />";		
	} else {
		$data .= "Store Selection: Any Store<br />";	
	}	
	if ($startdate) {
		$startwhere = " and ws.submitted > UNIX_TIMESTAMP('$startdate') ";
		$data.="From ".$startdate." ";
	}
	if ($enddate) {
		$endwhere = " and ws.submitted < UNIX_TIMESTAMP('$enddate') ";
		$data.="To ".$enddate."<br />";
	}		

	$query = "
		select 
			$titleselect
			$nidselect
			ws.sid,
			u.name,			
			pv.value as 'store',
			FROM_UNIXTIME(ws.submitted) as 'submitted' 
		from 
			webform_submissions ws
			join users u on u.uid = ws.uid
			left outer join profile_values pv on pv.uid = ws.uid and fid = 4
			join node n on n.nid = ws.nid
		where 
			$titlewhere
			$startwhere
			$endwhere
			$storewhere
			
			
		";

	#$data .= "<br />($query)<br />";
	$result = db_query($query);	
	
	while ($resultdata = db_fetch_object($result)) {
		$sid = $resultdata->sid; 							
		$name = $resultdata->name; 	
		$store = $resultdata->store;				
		$submitted = $resultdata->submitted; 
		$show_data=1;
		if ($show_any) {
			$Webform = $resultdata->Webform; 	
			$webform = $resultdata->nid; 
			$sid_link = "<a href = /node/$webform/submission/$sid>$sid</a><br />"; 				
			$datarows1 .= "
			<tr> 		 			 
				<td>$name</td>
				<td>$store</td>
				<td>$Webform</td>
				<td>$sid_link</td>
				<td>$submitted</td>							 
			</tr>";	
		
		} else {
			$sid_link = "<a href = /node/$webform/submission/$sid>$sid</a><br />"; 	
			$datarows1 .= "
			<tr> 		 			 
				<td>$name</td>
				<td>$store</td>
				<td>$sid_link</td>
				<td>$submitted</td>							 
			</tr>";	
		}		
	}
	if ($show_any) {	
		$datatable1 .= "	
		<table>
		   <thead>
			  <tr> 		
				<th>User</th>
				<th>Store</th>
				<th>Webform</th>
				<th>Submission</th>
				<th>Date Submitted</th>
		 
			  </tr>
		   </thead>
		<tbody>
		";	
	
	} else {
		$datatable1 .= "	
		<table>
		   <thead>
			  <tr> 		
				<th>User</th>
				<th>Store</th>
				<th>Submission</th>
				<th>Date Submitted</th>
		 
			  </tr>
		   </thead>
		<tbody>
		";	
	}
 
		
	if ($show_data) { 
		$data .= $datatable1;
		$data .= $datarows1;
		$data .= "</tr>";
		$data .= "</table>"; 
	} else {
		$data .= "--- NO RESULTS TO SHOW ---<br />";
	}			
 
    # Display results
	mssql_free_result($result);
	mssql_close($c);    
	return t($data);
   
}

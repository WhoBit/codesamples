#!c:/perl/bin/perl
##
##  i_reports_server.pl -- a CGI script to report information about the status of
## the server in the store.
##

## 2009-08-19 - v1.0.0 - Created - kdg
## 2009-08-24 - v1.0.1 - Added DynDnsInfo - kdg
## 2009-08-28 - v1.0.2 - Added script version at the bottom - kdg
## 2009-09-10 - v1.0.3 - Added gethost to find where store manager is open - kdg
## 2009-10-07 - v1.0.4 - Corrections:  Changed a LogMsg to ElderLog.  Changed service_name to lowercase - kdg
## 2009-10-14 - v1.0.5 - Disabled Apache test - Modified traffic count status to show all sensors - kdg
## 2009-10-15 - v1.0.6 - Correctly disconnecting from DB - kdg
## 2009-12-07 - v1.0.7 - Remove spaces from ip address of traffic counter as it was causing problems if there were any. - kdg
## 2010-01-07 - v1.0.8 - Revised how traffic counter names are found - kdg
## 2010-02-05 - v1.0.9 - Added time traffic counter was offline - kdg
## 2010-11-18 - v1.0.10 - Correction to calculating time counter was offline - kdg
## 2010-12-29 - v1.0.11 - Updated reporting of SAF file being present - kdg
## 2011-06-02 - v1.0.12 - Revised if cannot connect to compucount, now it reports that - kdg
## 2011-08-29 - v1.1.0 - Added checking eod proc.  Script now updates automatically  - kdg
## 2011-09-06 - v1.1.1 - Revised to show credit card authorization path and exposed toggle tool.
## 2011-10-03 - v1.1.2 - Revised so that store closed is not counted as an error - kdg
## 2011-10-07 - v1.1.3 - Revised so that there will be no check of the traffic counter if none is configured in elderconfig - kdg
## 2011-10-12 - v1.1.4 - Improvements to logic for checking credit card authorization status - kdg
## 2011-10-28 - v1.1.5 - Revised to show error if the traffic counter can be accessed but no counters are found - kdg
## 2012-04-11 - v1.1.6 - Revised the interpretation of the EOD in the EOD.log - kdg
## 2012-11-05 - v1.1.7 - Try to get IP address from DynDns without checking if dyndnsclient.ini exists - kdg
## 2012-11-06 - v1.1.8 - Added $transnet_expected variable to account for Lawrence not running Transnet - kdg
## 2013-01-18 - v1.1.9 - Adding support for SAP POS 2.3 - kdg
## 2013-01-29 - v1.1.10 - Removed warning from printing to the log - kdg
## 2013-04-04 - v1.1.11 - Added support for Archbold - kdg
## 2013-05-13 - v1.1.12 - Added SAP_POS_transnet_dir and SQL12 variables - kdg
## 2013-06-05 - v1.2.0 - More cleanup and added check if signed on to Transnet - kdg
## 2013-06-07 - v1.2.1 - More work to cleanup access to the database - kdg
## 2013-08-27 - v1.2.3 - Revised to find store manager open on SAP POS 2.3 - kdg
## 2013-09-18 - v1.2.4 - Removed 5003 from not_using_transnet - kdg
## 2013-10-17 - v1.2.5 - Added BASICSTATUS variation in case the database is not running - kdg
## 2013-11-29 - v1.2.6 - Modified check_signed_on to indicate signed on if a credit txn was approved - kdg
## 2014-04-02 - v1.2.7 - Disabled the buttons to toggle over to dial-up - kdg
## 2014-05-01 - v1.2.8 - Replaced DynDnsClient -  IP Address with VPN to Akron status - kdg
## 2014-10-01 - v1.2.9 - Disabled Check Credit Card Authorization Path - kdg
## 2014-11-20 - v1.2.10 - Added not why we cannot show the external IP address - kdg
## 2015-04-13 - v1.2.11 - Changed labels for SAF present - kdg
## 2015-07-20 - v1.2.12 - Added a check for the Air Card - kdg
## 2015-08-13 - v1.2.13 - Checking xps.log and xps.log.1 for Store Manager - kdg

use constant SCRIPT_VERSION 	=> "1.2.13";

use strict;
use CGI "meta";
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use TTV::utilities;
use TTV::cgiutilities qw(printResult_arrayref2 formatCurrency formatNumeric ElderLog);
use TTV::lookupdata;
use Math::Round qw( nearest );
use Time::Local; 
use File::Copy;

# Define the database engine
my $SQL_ENG = setSql();

my $xpsDir=getXPSDir();
my $parmDir=getParmDir();

# declare global variables
my $q = new CGI;    # create new CGI object.
my $query;          # global variable for storing SQL query string.
my @links;          # global variable for storing the links array.
my $tbl_ref;        # global variable for storing a reference to a query result.
my $sth;            # excecute statement handle object.
my $bdate;          # businessdate

my @ltm = localtime();
my $date = sprintf("%04d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
my $basicdate=sprintf("%04d%02d%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
my $debug_mode=0;
my $basic=0;
my $update_interval=60;  # How frequently this page updates
# colors calculated by \\saz\c\Projects\triversity\web\calccolors.pl
# my @RGB = (255,154,0);
my $dkcolor="ff9a00";
my $ltcolor="feeacc";
my $errorcolor="ffff00";
my $nocolor="FFFFFF";

# print the HTML Header
my $style="BODY{font: normal 11pt times new roman,helvetica}\n".
          "FORM{font: normal 11pt times new roman,helvetica}\n".
          "TH{font: bold 11pt arial,helvetica}\n".
          "TD{font: normal 11pt arial,helvetica}\n".
          "table{font: normal 11pt arial,helvetica}\n".
          "input{font: normal 11pt times new roman,helvetica}\n".
          ".tableborder {border-color: #EBEACC; border-style: solid; border-width: 1px;}\n".
          ".table {border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tablehead {background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px; color: #$ltcolor;}\n".
          "A.headerlink:link {color: #$ltcolor; text-decoration: none; }\n".
          "A.headerlink:active {color: #$ltcolor; text-decoration: none;}\n".
          "A.headerlink:visited {color: #$ltcolor; text-decoration: none;}\n".
          "A.datalink:link {color: #000000;}\n".
          "A.datalink:active {color: #000000;}\n".
          "A.datalink:visited {color: #000000;}\n".
          ".tabledata {background-color: #$ltcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tabledatanb {background-color: #$ltcolor; border-color: #$ltcolor; border-style: solid; border-width: 1px;}\n". # nb=no-border
          ".tabledataerror {background-color: #$errorcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
		  ".tabledatanote {background-color: #$nocolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          "table.table a.sortheader {background-color:#$dkcolor; color:#$ltcolor; text-decoration: none;}\n".
          "table.table span.sortarrow { color:#$ltcolor; text-decoration: none; }".
          ".button1 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;
          "";
print $q->header( -type=>'text/html');
print '<SCRIPT LANGUAGE="JavaScript" SRC="/CalendarPopup.js"></SCRIPT>'."\n";
print '<SCRIPT LANGUAGE="JavaScript">document.write(CalendarPopup_getStyles());</SCRIPT>'."\n";
print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

print $q->start_html(-title=>'Village Elder',
                     -author=>'kdg@tenthousandvillages.com',
                     -style=>{'code'=>$style},
                     -head=> [
                        meta({
                            -http_equiv => "refresh",
                            -content => "$update_interval"
                            })
                        ]
                     );

my $filepath = "C:\\mlink\\akron";
my $logfile = "C:\\MLINK\\akron\\i_reports_server.log";
my $transnet_dir="c:/program files/transnet";
my $SAP_POS_transnet_dir="c:/program files/SAP/transnet";
if (-d $SAP_POS_transnet_dir) {
    # If we find the SAP POS transnet directory assume that is the Transnet we want - kdg
    $transnet_dir=$SAP_POS_transnet_dir;
}

# Figure out which report to run.
my $report = ($q->param("report"))[0];

$report = "DEFAULT" unless $report;

my $str;
my $internet_connectivity;
 
my %params = $q->Vars;
foreach my $key (keys(%params)) {
   next if ($key eq "report");
   next if ($key eq "pwd");
   $str .= "&" . $key ."=". $params{$key} ;
}
ElderLog($logfile, "$report called ".  ($str gt '' ? "with $str" : '') );

print $q->start_table({-width=>"700"}), "\n";
print $q->start_Tr(), $q->start_td();

# depending on the value of the 'report' parameter,
# we will build different queries, and expect additional paramters.


if ($report eq "DEFAULT") {
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: DEFAULT
    # Description: returns Quantity on Hand data
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    print $q->p("<b>Instructions:</b> click on the report name in the list below.  ".
              "For more detailed information about a report, check out the ".
              $q->a({href=>"/pdf/3-Backoffice Procedures/VillageElderReports.pdf", -target=>"_new"}, "help") ." section"), "\n";

    # fields: [display name, script, report, report note]
    my @reportlist = (
     ["Server Status", "/cgi-bin/i_reports_server.pl", "STATUS",
      "Shows server status."
     ], 
    );


    if (1) {
        foreach my $reportref (@reportlist) {
            my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
            print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
        }
    }

    StandardFooter();
} elsif (($report eq "STATUS") || ($report eq "BASICSTATUS")){
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: STATUS
    # Description:
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    print $q->h2("Server Status Report ");
    my $transnet="Transnet";
    my $SQL="SQLAnys_SQL_ENG";
    my $SQL12="ASANYs_SQL_ENG";
    my $xpsServer="Xpress Server";
    my $saf="${xpsDir}/sdata/credit.saf";
    my $resetflag="c:/temp/safReset.flg";    
    my $passes=0;
    my $fails=0;
    my $warnings=0;
    my $sql_status=0;
    my %check_hash=();
    my %check_hash_status=();
    my %note_hash=();
    @ltm = localtime();
    $date = sprintf("%04d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
    my $time = sprintf("%02d:%02d:%02d", $ltm[2], $ltm[1], $ltm[0]); 
	my $enable_buttons=0;	
    my $test="Internet Connectivity Test:"; 
	my $transnet_expected=1;
	my @not_using_transnet=("7001");

    if ($report eq "BASICSTATUS") {
        $basic=1;
        if ($debug_mode) {
            print "Basic Status Check<br>";   
        }            
    }
    # initialize the database handle
    my $dsn;
    my $dbh;
    my $storenum;
    unless ($basic) {
        $dsn = "DSN=BACKOFF;ENG=$SQL_ENG;UID=****;PWD=****;";
        $dbh = TTV::utilities::openODBCDriver($dsn); 
    
        $storenum=getStoreNum($dbh);
    }
	if (grep /$storenum/,@not_using_transnet) {
		$transnet_expected=0;
	}
 
    if (1) {
        if ($debug_mode) {
            print "Check_Connection...<br>";   
        }       
		if (check_connection()) {
			$check_hash{"$test"}="PASSED";
			$check_hash_status{"$test"}="1";
			$note_hash{"$test"}="\&nbsp;";
			$passes++;
			$internet_connectivity=1;
		} else {        
			$check_hash{"$test"}="FAILED";        
			$check_hash_status{"$test"}="0"; 
			$note_hash{"$test"}="Credit Card authorizations via the Internet will fail!";        
			$fails++;
			#$enable_buttons=1; # The toggle feature no longer works with SAP POS 2.3 - kdg
			$internet_connectivity=0;			
		}
		if (1) {
			if ($internet_connectivity) {
				#######################
				# Check the Air Card
				#######################    
				$test="Internet Connection Test: ";    
				if (check_airCard()) {
					# Only report if it looks like it is running on the Air Card
					$check_hash{"$test"}="Air Card Active"; 
					$check_hash_status{"$test"}="0";         
					$note_hash{"$test"}="The Internet Connection appears to be via the Air Card!";
					$fails++;
				} 
			}
		}  			
		if (0) {
			#######################
			# Check Credit Card Authorization Path
			#######################    
			$test="Credit Card Authorization:"; 
            if ($debug_mode) {
                print "Check $test...<br>";   
            }                
			my $status;
			my $statusinfo;
			my $route;
	 
			($status,$statusinfo,$route,$enable_buttons)=check_store("CreditAuth","$internet_connectivity");                
			if ($status) {        
				$check_hash{"$test"}="$route"; 
				$check_hash_status{"$test"}="1"; 
				$note_hash{"$test"}="$statusinfo";
				$sql_status=1;
				$passes++;
			} else {
				$check_hash{"$test"}="$route"; 
				$check_hash_status{"$test"}="0";         
				$note_hash{"$test"}="$statusinfo";
				$fails++;
			} 
		}  		
    }
    if (1) {
        $test="SQL Database Status:";  
        if ($debug_mode) {
            print "Check $test...<br>";   
        }          
        if (check_service($SQL)) {
            $check_hash{"$test"}="RUNNING"; 
            $check_hash_status{"$test"}="1"; 
            $note_hash{"$test"}="\&nbsp;";
            $sql_status=1;
            $passes++;
        } elsif (check_service($SQL12)) {
            $check_hash{"$test"}="RUNNING"; 
            $check_hash_status{"$test"}="1"; 
            $note_hash{"$test"}="\&nbsp;";
            $sql_status=1;
            $passes++;        
        } else {
            $check_hash{"$test"}="NOT RUNNING"; 
            $check_hash_status{"$test"}="0";         
            $note_hash{"$test"}="No Database access!";
            $fails++;
        }    
        $test="Xpress Server Status:"; 
        if ($debug_mode) {
            print "Check $test...<br>";   
        }          
        
        if (check_service($xpsServer)) {
            $check_hash{"$test"}="RUNNING"; 
            $check_hash_status{"$test"}="1";         
            $note_hash{"$test"}="\&nbsp;";
            $passes++;
        } else {
            $check_hash{"$test"}="NOT RUNNING"; 
            $check_hash_status{"$test"}="0";         
            $note_hash{"$test"}="No Point of Sale transactions being handled!";
            $fails++;
         
        }      
        # Check if an saf file is present
        $test="SAF File:";  
        if ($debug_mode) {
            print "Check $test...<br>";   
        }          
        
        if (-e $saf) {             
            $check_hash{"$test"}="PRESENT";
            $check_hash_status{"$test"}="0";  
            if ($check_hash{"Xpress Server Status:"} eq "NOT RUNNING") {
                $note_hash{"$test"}="SAF present and Xpress Server is stopped!";
            } else {
                $note_hash{"$test"}="SAF present.  Should get processed at next credit or gift card transaction.";
            }        
            $fails++;
        } else {    
            $check_hash{"$test"}="NOT PRESENT";
            $check_hash_status{"$test"}="1";         
            $note_hash{"$test"}="\&nbsp;";
        }       
        $test="Transnet Status:";  
        if ($debug_mode) {
            print "Check $test...<br>";   
        }          
        
		if ($transnet_expected) {
			# We expect Transnet to be running (This is the most common)
			if (check_service($transnet)) {

                # Check that we are signed on
                my $response=check_signed_on();
                if ($response) {
                    if ($response == 1) {
                        $check_hash{"$test"}="RUNNING but Not Signed On"; 
                        $check_hash_status{"$test"}="0";         
                        $note_hash{"$test"}="Wait until XPS is signed on before running credit cards.";
                          
                    } 
                    if ($response == 2) {
                        $check_hash{"$test"}="RUNNING & Signed On"; 
                        $check_hash_status{"$test"}="1";         
                        $note_hash{"$test"}="\&nbsp;";
                        $passes++;                    
                    }                     
                } else {
                    # We do not know anything about whether we are signed on
                    $check_hash{"$test"}="RUNNING"; 
                    $check_hash_status{"$test"}="1";         
                    $note_hash{"$test"}="\&nbsp;";
                    $passes++;                
                }
                
			} else {
				$check_hash{"$test"}="NOT RUNNING";
				$check_hash_status{"$test"}="0";         
				$note_hash{"$test"}="Credit Cards cannot be authorized!";
				$fails++;
			}    
		} else {
			# In some rare exceptions, we expect Transnet NOT to be running.
			if (check_service($transnet)) {
				$check_hash{"$test"}="RUNNING"; 
				$check_hash_status{"$test"}="0";         
				$note_hash{"$test"}="Transnet is running but it is not expected to be!";
				$passes++;
			} else {
				$check_hash{"$test"}="NOT RUNNING";
				$check_hash_status{"$test"}="1";         				
				$note_hash{"$test"}="This is normal for this store.";
				$fails++;
			} 		
		}
    }

    if (0) {
        #######################
        # Check the Apache WebServer 
        #######################    
        $test="Apache WebServer: ";    
        if (check_service("Apache")) {
            $check_hash{"$test"}="RUNNING"; 
            $check_hash_status{"$test"}="1"; 
            $note_hash{"$test"}="\&nbsp;";
            $sql_status=1;
            $passes++;
        } else {
            $check_hash{"$test"}="NOT RUNNING"; 
            $check_hash_status{"$test"}="0";         
            $note_hash{"$test"}="The Elder is not working!";
            $fails++;
        } 
    }  
    unless ($basic) {   # Do Not run this section if basic status is requested
        #######################
        # Store Manager
        #######################  
        
        my $log_dir=$xpsDir;	
        if (-d "${xpsDir}/logs") {
            $log_dir="${xpsDir}/logs";	
        }		
        #my $log=sort_log("${log_dir}","xps.log");
		my $logA="${log_dir}/xps.log";
		my $logB="${log_dir}/xps.log.1";
		my $store_manager_open="0"; 
		# Look through the xps and xps1 log files to find the last 
		# time that Store Manager was used.
		foreach my $log ("$logA","$logB") {
			last if $store_manager_open;
			if (-e "$log") {
				if (open(FILE,"$log")) {
					my $line;
					my $prev_line;
				   
					my @contents=(<FILE>);			
					close FILE;
				 
					for (my $x=0; $x<=$#contents; $x++) {
						chomp($line=$contents[$x]);     
						chomp($prev_line=$contents[($x - 1)]); 
		 
						# Look for store manager opening				
						if ($line =~ /StoreManager Deluxe Connections: 1/) { 					
							if ($prev_line =~ /Xpress Server Connection/) {					
								my @tmp=split(/\s+/,$prev_line);                        
								$store_manager_open="$tmp[$#tmp]";  # Record the IP address from where it was opened                        						
							} else {
							   $store_manager_open="IP address undetermined";  # Record the IP address from where it was opened
							}                    
						}
						if ($line =~ /StoreManager Deluxe Connections: 0/) {   				 			
							$store_manager_open="0"; 
						}   
					} # End of iterating through contents
					$test="Store Manager: "; 
					if ($debug_mode) {
						print "Check $test...<br>";   
					}          					
				} # End of if open $log
			} # End of if $log
		}
		if ($store_manager_open) {				
			$check_hash{"$test"}="OPEN"; 
			$check_hash_status{"$test"}="1"; 
			my $host=gethost($store_manager_open);
			$note_hash{"$test"}="Store Manager open on $host";
			$sql_status=1;
			$passes++;
		} else {			
			$check_hash{"$test"}="NOT OPEN"; 
			$check_hash_status{"$test"}="1";         
			$note_hash{"$test"}="No one using Store Manager (That I can tell)";
		}   		
       
        #######################
        # Check DynDNS info for stores using it
        #######################
        #if (-e "/mlink/akron/DynDnsClient.ini") {
            #$test="DynDnsClient -  IP Address:";
=pod		# This test will not work since we are blocking access to the Internet from Edna
			$test="Internet: External IP Address:";
            # Get the ip address in current use
            my $current_ip=get_ip();			
 			
            if ($current_ip) {
                $check_hash{"$test"}="$current_ip"; 
                $check_hash_status{"$test"}="1";         
                $note_hash{"$test"}="\&nbsp;";
                $passes++;   
            } else {
                $check_hash{"$test"}="Failed to Determine IP"; 
                $check_hash_status{"$test"}="0";         
                $note_hash{"$test"}="Possible problems with Akron connecting to you.";
                $fails++;
            }
=cut
			
			$test="Internet: VPN to Akron:";
            if ($debug_mode) {
                print "Check $test...<br>";   
            }          
            

			my $vpn_ip="$akron_ip_hash{'sftp'}";
			my $vpn=check_vpn($vpn_ip);

 		
            if ($vpn) {
                $check_hash{"$test"}="VPN Connected"; 
                $check_hash_status{"$test"}="1";         
                $note_hash{"$test"}="\&nbsp;";
                $passes++;   
            } else {
                $check_hash{"$test"}="Failed to Determine VPN Status"; 
                $check_hash_status{"$test"}="0";         
                $note_hash{"$test"}="Possible problems with Akron connecting to you.";
                $fails++;
            }			
         
        #}
        #######################
        # Check EOD log to see if EOD process in in progress 
        #######################
        if ($debug_mode) {
            print "Check End of Day...<br>";   
        }          
        
        my $eodlog="${xpsDir}/EOD.log";
        if (-e "$eodlog") {
            $test="End of Day Process:";
            open(INPUT,"$eodlog");
            my @log_info=(<INPUT>);
            close INPUT;
            
            # Parse the log to see if the EOD is in progress
            my $finished=0;
            my $running;
            my $time;
            foreach my $l (@log_info) {
                chomp($l);			
                if (($l =~ /FINISHED EODPROC.BAT/) || ($l =~ /CREATING EOD COMPLETE FLAG/)) {
                    $finished=1;				
                }					
                
                if ($l =~ /STARTING/) {
                    $finished=0;
                    # What did it start?
                    my @tmp=split(/starting/i,$l);
                    $running=$tmp[1]; 
                    # Get the time
                    @tmp=split(/\s+/,$l);
                    $time="$tmp[0] $tmp[1]";
                }            
            }
     
            if ($finished) {
                $check_hash{"$test"}="EOD Process complete"; 
                $check_hash_status{"$test"}="1";         
                $note_hash{"$test"}="$time";
                   
            } else {
                $check_hash{"$test"}="Running $running"; 
                $check_hash_status{"$test"}="0";         
                $note_hash{"$test"}="Started at $time";
                $fails++;
            }
         
        }
    }
     
    #######################
    # The following tests require SQL
    #######################
    if (1) {
        if ($sql_status) {    
            $test="Store Status:";
            if (check_store("state")) {
                $check_hash{"$test"}="OPEN"; 
                $check_hash_status{"$test"}="1";         
                $note_hash{"$test"}="\&nbsp;";
                $passes++;
                # If the store is open, check the business date
                $test="Business Date:";
                $bdate=check_store("date");
                $check_hash{"$test"}="$bdate"; 
                if ($bdate eq $date) {
                    $check_hash_status{"$test"}="1";         
                    $note_hash{"$test"}="\&nbsp;";
                    $passes++;
                } else {
                    $check_hash_status{"$test"}="0";         
                    $note_hash{"$test"}="Businessdate is not today's date!";
                    $fails++;
                }    
            } else {
                $check_hash{"$test"}="CLOSED";
                $check_hash_status{"$test"}="2";         
                $note_hash{"$test"}="\&nbsp;";				
                #$fails++;
            }   
            $test="Traffic Counter Status:";
            # See if we have a traffic counter and if so, how many sensors we have
            my ($tCounter,$ref,$tStatus)=check_store("tcounter");            
            my @tcNames=@$ref;
            if ($debug_mode) {
                print "TP 0: tcNames (@tcNames) </br>";
            }
			if ($tCounter) {
				# Proceed only if we have a traffic counter
				if ($#tcNames > -1) {
					foreach my $tcCounterName (@tcNames) {
						my $status;
						my $statusinfo;
						# If we have a counter, see if we can get the status
					   
						$test="Traffic Counter $tcCounterName Status:";
						
						($status,$statusinfo)=check_store("Count","$tcCounterName");                
						if ($status) {
							$statusinfo=uc($statusinfo);
							$check_hash{"$test"}="$statusinfo"; 
							$check_hash_status{"$test"}="1";         
							$note_hash{"$test"}="\&nbsp;";
							$passes++;
						} else {
							if ($statusinfo =~ /BLOCKED/i) {
								$statusinfo = "Beam is blocked";
								$note_hash{"$test"}="The sensor beam seems to be obstructed";
							}
							if ($statusinfo =~ /OFFLINE/i) {
								#$statusinfo = "Unit Offline";
								$note_hash{"$test"}="The counter's receiver can not be contacted.";
							}  
							if ($statusinfo eq "undetermined") {
								$statusinfo = "Traffic Counter Status Undetermined";
								$note_hash{"$test"}="Could not get status of the Traffic Counter";
							}                 
							$check_hash{"$test"}="$statusinfo";
							$check_hash_status{"$test"}="0";                                 
							$fails++;
						}
					}
				}  else {
					$check_hash{"$test"}="$tStatus";
					$check_hash_status{"$test"}="0";   
					$note_hash{"$test"}="Could not get status of the Traffic Counter";      
					$fails++;                
				}       
			} else {						
				unless ($tStatus eq "NA") {
					# We could not find any counters but unless the status is NA, it means
					# that it looks like a counter is configured.  In which case, this is an error.
					$check_hash{"$test"}="$tStatus";
					$check_hash_status{"$test"}="0";   
					$note_hash{"$test"}="Could not get status of the Traffic Counter";      
					$fails++;   				
				}
			}
        }
    }
    #######################
    # Show the results 
    #######################
    if (1) {   
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, 
            $q->b("Server Status Report")),
            $q->td({-class=>'tablehead', -align=>'left'}, "Notes" )
            );
        print $q->Tr(                  
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'right'}, "Date of last check:"),
            $q->td({-class=>'tabledata', -align=>'left'}, "$date $time" ),
            $q->td({-class=>'tabledata', -align=>'left'}, "(Page updates about every $update_interval seconds.)" ),
            );
        foreach my $key (sort keys(%check_hash)) {	
            if ($check_hash_status{$key} == 1) {
                print $q->Tr(                  
                    $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'right'}, "$key"),
                    $q->td({-class=>'tabledata', -align=>'left'}, "$check_hash{$key}" ),
                    $q->td({-class=>'tabledata',  -align=>'left'}, "$note_hash{$key}" )
                    );
			} elsif ($check_hash_status{$key} == 2) {				
                print $q->Tr(                  
                    $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'right'}, "$key"),
                    $q->td({-class=>'tabledatanote', -align=>'left'}, "$check_hash{$key}" ),
                    $q->td({-class=>'tabledata', -align=>'left'}, "$note_hash{$key}" )                
                    );			
            } else {
                print $q->Tr(                  
                    $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'right'}, "$key"),
                    $q->td({-class=>'tabledataerror', -align=>'left'}, "$check_hash{$key}" ),
                    $q->td({-class=>'tabledata', -align=>'left'}, "$note_hash{$key}" )                
                    );
            }
            
        }
                             
        print $q->end_table(),"\n";
    }    
    print $q->p("There were $fails errors found");
=pod	# These buttons have been disabled since the dial-up option is not supported in SAP POS 2.3 - 2014-04-02 - kdg
    if (($enable_buttons == 1) || ($enable_buttons == 2)) {        
        print $q->p($q->a({-href=>"/cgi-bin/i_reports_server.pl?report=safMonitor", -class=>"button1"}, "History").
               "Review Internet Connectivity History for Today." );
 
        print $q->p($q->a({-href=>"/cgi-bin/i_ping_test.pl", -class=>"button1"}, "Live").
               "View current Internet Connectivity." );	
	}
    if ($enable_buttons == 2) {
		# Different labels for this button
        print $q->p($q->a({-href=>"/cgi-bin/i_tools_giftcards.pl?report=TOGGLEROUTE1", -class=>"button1"}, "Toggle").
               "Toggle Credit Card Authorization to Dial-up." );	    	
    } elsif ($enable_buttons == 3) {
        print $q->p($q->a({-href=>"/cgi-bin/i_tools_giftcards.pl?report=TOGGLEROUTE1", -class=>"button1"}, "Toggle").
               "Toggle Credit Card Authorization back to URL." );	    
    }		
=cut    
    if ($dbh) {
        $dbh->disconnect;
    }
    StandardFooter();
   
} elsif ($report eq "safMonitor") {
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: safMonitor
    # Description: Parse the safMonitor log file to show when there was problems connecting to the Internet
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    print $q->h2("Internet Connectivity History Report ");
 
    ElderLog($logfile, "Checking safMonitor log ");    
    my $logfile="c:/mlink/akron/safMonitor.log";
	my $noI_count=0;
	if (-e $logfile) {
		open(INPUT,"$logfile");
		my @log_info=(<INPUT>);
		close INPUT;
		# Look through the logfile for connectivity issues today
		foreach my $l (@log_info) {
			if ($l =~ /$basicdate/) {
				# If the log entry was today
				if ($l =~ /WARNING: Appears to be no Internet access/) {
					my @tmp=split(/-/,$l);
					print "$tmp[0]<br />";
					$noI_count++;
				}
			}
		}
		print $q->p("There were $noI_count instances where there was no Internet connectivity.");
	} else {
		print $q->p("Could not locate the Internet Connectivity History Log file");
		print "(Note: This file is cleared at the beginning of each week.  It only records problems with Internet Connectivity<br />";
		print "and so if the file is not present, it may simply mean your store has had no connectivity problems so far this week.)<br />";
	}
 
	StandardFooter();	
 	
} else {
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # DEFAULT CASE
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    print $q->h1("Unknown Report Requested ($report).");
    StandardFooter();
}


ElderLog($logfile, "$report finished");

print $q->end_td(), $q->end_Tr();
print $q->end_table(), "\n";

# close the html output
print $q->end_html;
 
exit;

# end of program.

##########
# Functions
##########

sub StandardFooter {
   print "<br>\n";
   print $q->a({-href=>"javascript:parent.history.go(0)"}, "refresh");
   print $q->a({-href=>"javascript:parent.history.go(-1)"}, "go back");
   print $q->a({-href=>"/index.html", -target=>'_top'}, "go home");
   print $q->h6("Script version: ".SCRIPT_VERSION);
}

sub check_connection {       
    ElderLog($logfile, "Checking connectivity to the Internet");    
    my $url_test_site="google.com";
    system("ping $url_test_site");
    if ($?) {
        sleep 2;
        system("ping $url_test_site");
        if ($?) {
            # Check a second time
            ElderLog($logfile, indent(1)."Appears to be no Internet access"); 
            return 0;
        } else {
            ElderLog($logfile, indent(1)."Appears to be Internet access - ping to $url_test_site succeeded.");            
            return 1;
        }
    } else {
        ElderLog($logfile, indent(1)."Appears to be Internet access - ping to $url_test_site succeeded.");            
        return 1;
    }
}


sub check_service {
    my $service=shift;
    $service=lc($service);
    ElderLog($logfile, "Check $service...");
	my $service_name="";
	my $found_service=0;
	my %services_hash=();
	my $service_state="";
	my @services_info=`sc query`;
	my $s = "";
	my @line=();
    my $running=0;
    my $eventlog="${xpsDir}/event.log";
	foreach $s (@services_info) {
		if ($found_service) {
			if ($s =~ /STATE/) {
				@line=split(/\s+/,$s);
				$service_state=$line[$#line];                
				chomp($service_state);
				$services_hash{$service_name}=$service_state;
			}
			
		}
		if ($s =~ /SERVICE_NAME/) {
			$found_service=1;
			@line=split(/:/,$s);
			$service_name=$line[1];
			# Take off the leading space
			$service_name=~ s/^ //;
			# Take off the carriage return
			chomp($service_name);
            # Make it lower case
            $service_name=lc($service_name);
		}
	}
 
    # Is the  service running?    
    if ($services_hash{$service} eq "RUNNING") {    
        return 1;        
    } else {    
        return 0;
    }
    ElderLog($logfile, "Finished Checking $service.");     
} 

sub check_store {
    # Various checks on the state of the store
    my $check=shift;
    my $name=shift;
    my $open;

    # initialize the database handle
    my $dsn;
    my $dbh;
    unless ($basic) {
        $dsn = "DSN=BACKOFF;ENG=$SQL_ENG;UID=****;PWD=****;";
        $dbh = TTV::utilities::openODBCDriver($dsn);      
    }    

 
    
    if ($check eq "state") {
        # Is the store open or closed?
        $query="select storeopen from server_state";
        $sth=$dbh->prepare($query);
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array()) {
            $open=$tmp[0];
        }
        ;
        if ($open eq "Y") {
            return 1;        
        } else {
            return 0;
        }
        
    }
    if ($check eq "date") {
        # Is the business date correct?
        $query="select businessdate from server_state";
        $sth=$dbh->prepare($query);
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array()) {
            $bdate=$tmp[0];
        }
        $bdate=substr($bdate,0,10); # Grab just the date and not the time
        ;
        return "$bdate";
        
    }  
    if ($check eq "tcounter") {
        # Is there a traffic counter?
        my $tcAddress;
        my $counter=0;
        my @counter_names;
        my $counter_status="unknown";		
        $query="select cvalue from elderconfig where ckey like 'TrafficCounter|IPAddress'";
        $sth=$dbh->prepare($query);
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array()) {
            $tcAddress=$tmp[0];  
        }
		unless ($tcAddress) {
		       return 0,\@counter_names,"NA";    
		}
        # Remove any spaces from the IP address 
        $tcAddress=~s/ //g;        
        if ($debug_mode) {
            print "TP 1 tcAddress ($tcAddress)</br>";
        }        
        # Now determin how many sensors there are
        use LWP::Simple;
        my $URL="http://${tcAddress}/status";
        my $content=get($URL);
        unless ($content) {
            # If we got nothing try once more
            sleep 1;
            $content=get($URL);
        }

        if ($content) {
          
            # If we got content, parse it for the info we want
            #my @content_info=split(/\<br\>/,$content);
            my @content_info=split(/HREF/i,$content);
            foreach my $c (@content_info) {  
                # I really need to improve the way that I am parsing the data from the counter but
                # for now, this recognizes most counters.  If the counters are given names other than
                # 1Count, 2Count, etc or Front Door, Back Door, then they will not be seen.
                # If I can figure out how to better parse for the sensors, then I can make this work 
                # no matter what the names. - 2010-01-07 - kdg
                if ($c =~ /Front/) {
                    $counter++;    
                    if ($debug_mode) {
                        print "TP 2a door ($c)</br>";
                    }   
                    push(@counter_names,"Front");
					$counter_status="connected";
                }    
                if ($c =~ /Back/) {
                    $counter++;    
                    if ($debug_mode) {
                        print "TP 2b door ($c)</br>";
                    }   
                    push(@counter_names,"Back");                    
					$counter_status="connected";
                }                
                if ($c =~ /1Count/) {
                    $counter++;  
                    push(@counter_names,"1Count"); 
					$counter_status="connected";
                }
                if ($c =~ /2Count/) {
                    $counter++;                  
                    push(@counter_names,"2Count");                     
					$counter_status="connected";
                }
                if ($c =~ /3Count/) {
                    $counter++;                    
                    push(@counter_names,"3Count"); 
					$counter_status="connected";
                }                

            }
            if ($debug_mode) {
                print "TP 2d content ($#content_info)</br>";
				print "TP 2e content (@content_info)</br>";
            }            
        } else {
            if ($debug_mode) {
                print "TP 3 no content from traffic counter</br>";
            }
            $counter_status="Could Not Connect to CompuCount";
        }
        
        if ($debug_mode) {
            print "TP 4 counter ($counter) </br>";
        }        
        return $counter,\@counter_names,$counter_status;                
    }      
    if ($check =~ /Count/) {
        if ($debug_mode) {
            print "TP 5 checking count for ($name)</br>";
        }
        # What is the status of the counter
        # First, get the address
        my $tcAddress;
        my $content;
        my @content_info=();
        $query="select cvalue from elderconfig where ckey like 'TrafficCounter|IPAddress'";
        $sth=$dbh->prepare($query);
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array()) {
            $tcAddress=$tmp[0];
        }
        # Remove any spaces from the IP address 
        $tcAddress=~s/ //g;    
        # Now get the status
        use LWP::Simple;
        my $URL="http://${tcAddress}/status";
        $content=get($URL);
        unless ($content) {
            # If we got nothing try once more
            sleep 1;
            $content=get($URL);
        }
        my $status=0;
        my $statusinfo="unknown";
        my $counter;
        if ($content) {
            # If we got content, parse it for the info we want
            #@content_info=split(/\<br\>/,$content);            
            @content_info=split(/HREF/,$content);            
            $status=1;
            foreach my $c (@content_info) {       
                if ($c =~ /$name/) {
                    if ($debug_mode) {
                        print "TP 6c ($c)</br>";
                    }
                    $statusinfo="working";
                    if ($c =~ /sensor's beam is blocked/i) {
                        $statusinfo = "blocked ";
                        $status=0;
                    }
                    if ($c =~ /sensor is offline/i) {
                        $statusinfo = "$counter offline ";
                        $status=0;
                    }  
                    if ($c =~ /red/i) {
                        if ($debug_mode) {
                            print "TP 6d ($c)<br>";   
                        }                        
                        # Find the time the counter was not working
                        my @tmp=split(/red\>/,$c);
                        $c=$tmp[1];
                        @tmp=split(/\</,$c);
                        $c=$tmp[0];
                        if ($debug_mode) {
                            print "TP 6e ($c)<br>";
                        }                        
                        my $time_status;
                        
                        my @parts=gmtime($c);
                        my $hours=$parts[2];
                        my $minutes=$parts[1];
                        my $seconds=$parts[0];
                        my $days=$parts[7];
                        if ($debug_mode) {
                            print "TP 6f parts (@parts) hr ($hours) min ($minutes) sec ($seconds) day ($days)<br>";
                        }                                 
                        if ($days) {
                            $time_status.="$days days ";
                        }
                        if ($hours) {
                            $time_status.="$hours hours ";                            
                        }
                        if ($minutes) {
                            $time_status.="$minutes minutes ";                            
                        }
                        if ($seconds) {
                            $time_status.="$seconds seconds ";                            
                        }
                        $statusinfo .= "$time_status";
                        if ($debug_mode) {
                            print "TP 6g statusinfo ($statusinfo)<br>";
                        }
                        
                    }                     
                }              
            }            
        }
        ;
        return ($status,$statusinfo);
    }     
	if ($check =~ /CreditAuth/) {  
        my $status=0;
        my $statusinfo="unknown";
        my $currentRoute="unknown";
		my $button=0;
		my $internet_is_up=$name;	# Use the name variable to carry the internet connectivity status here

        # Check how the store is connected to Paymenttech
        # === === ===  PARSE XML FILE === === ===
        my $xml_file="${transnet_dir}/config/transnet.xml";        
        if (-f $xml_file) {
            open(INPUT,"$xml_file");
            my @fileinfo=(<INPUT>);
            close INPUT;
            foreach my $f (@fileinfo) {
                if (($f =~ /PaymentechRoute/) && ($f =~ /connectionName/)) {
                    # Get the connection from this line
                    if ($f =~ /Dial/i) {
                        $currentRoute="dial-up";                                        
                    } elsif ($f =~ /URL/i) {
                        $currentRoute="URL";  
                    }
                }
            }
        } else {
            return ($status,$statusinfo,$currentRoute,$button);   
        }
        if ($currentRoute) {
            if ($currentRoute eq "dial-up") {
                #if ($internet_connectivity) {
				if ($internet_is_up) {
                    # This store is on dialup but the Internet appears to work.
                    # They should be using the URL method.
                    $status=0;
                    $statusinfo="Need to toggle to URL because you are still on dial-up.";
                    $button=3;  # Show the button so they can toggle back.
                } else {
                    $status=1;
                    $statusinfo="Credit Authorization via Dial-up - Internet Down";    
					# Do not need to show the button to toggle.
					$button=1;					
                }
            } else {
				#if ($internet_connectivity) {
                if ($internet_is_up) {
                    $status=1;
                    $statusinfo="Credit Card Authorization via Internet";
					# Do not need to show any buttons
					$button=0;
                } else {
                    $status=0;
                    $statusinfo="Need to toggle to Dial-up because Internet is down.";   
					$button=2;  # Show the button so they can toggle to dial-up.					
                }
            }			
        } else {
            $status=0;
            $statusinfo="Failed to determine Authorization Method";
        }    
        return ($status,$statusinfo,$currentRoute,$button);        
    }
    if ($dbh) {
        $dbh->disconnect;
    }
}

sub check_airCard {
	# Return a positive value if it looks like the connection is NOT on the Air Card (what is desired)
	# and a 0 if it looks like it is.
	my $warns=0;
	# Check various things via the OIDs
	my $tool="c:/program files/pstools/snmpwalk.exe";
	unless (-f $tool) {		
		LogMsg("Could not check SonicWall");
		return 0;
	}		
	# Find the IP for the default gateway (the SonicWall)
	my $SonicWall;
	my @ipconfig_info=`ipconfig`;
	foreach my $i (@ipconfig_info) {
		chomp($i);		
		if ($i =~ /Default Gateway/) {		
			my @tmp=split(/\s+/,$i);
			$SonicWall=$tmp[-1];
		}
	}
	unless ($SonicWall) {		
		LogMsg("Could not check SonicWall");
		return 0;	
	}

 
	# Check to see if the U0 interface is active (On the Air Card)
	my $U0_status=0;	
	# This checks the MTU of the U0 interface
	my $cmd="\"$tool\" -Os -c public -v 2c $SonicWall iso.3.6.1.2.1.2.2.1.4.7 2> c:/temp/junk.txt";		
	my @info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		my @counttmp=split(/\s+/,$i);	
		if ($counttmp[-1]) {
			$U0_status++;		
		}		
	}	
	# This checks the Mbps of the U0 interface
	$cmd="\"$tool\" -Os -c public -v 2c $SonicWall iso.3.6.1.2.1.2.2.1.5.7 2> c:/temp/junk.txt";		
	@info=`$cmd`;
	foreach my $i (@info) {
		chomp($i);
		my @counttmp=split(/\s+/,$i);		
		if ($counttmp[-1]) {
			$U0_status++;			
		}			
	}		
	if ($U0_status) {		
		$warns++;
	}	
		
	return $warns;
}

sub get_ip {
    my $skip = 'Current IP Address';
    my $url  = 'http://checkip.dyndns.org/';
    my $program = "$0"; 
    my $version = SCRIPT_VERSION;     
    use LWP::UserAgent;
    
    # Create a user agent object
    my $ua = LWP::UserAgent->new;
    $ua->agent("$program/$version ");

    # Create a request
    my $req = HTTP::Request->new(GET => $url);

    # Pass request to the user agent and get a response back
    my $res = $ua->request($req);
    my $reply = ''; 
    my $ip = '';
    # Check the outcome of the response
    if ($res->is_success) {
       $reply = $res->content; 
    } 
    $skip  =~ s/ /\\s/is;
    $reply =~ s/^.*?${skip}//is;
    if ($reply =~ /^.*?\b(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b.*/is) {
      $ip = $1;
    }

    return $ip;
} 

sub sort_log {
    # Send the path to the file and the file to be sorted.  This function copy of the file in the temp
    # folder which has the entries in chronological order.
    my $path=shift;
    my $file=shift;
    my $ofile="${path}/${file}";    # The original file
    my $tfile="c:/temp/temp.log";
    my @contents=();
    my $line;
    my $sfile="c:/temp/$file";      # The sorted file
    my %lines_hash=();
    my $format_error=0;
    my %month_hash=(
        'Jan'=>'1',
        'Feb'=>'2',
        'Mar'=>'3',
        'Apr'=>'4',
        'May'=>'5',
        'Jun'=>'6',
        'Jul'=>'7',
        'Aug'=>'8',
        'Sep'=>'9',
        'Oct'=>'10',
        'Nov'=>'11',
        'Dec'=>'12'
    );	
    my $current_year = sprintf("%02d", $ltm[5]+1900); 
	  
    if (-e "$ofile") {
        # First make a copy of it 
        unless (copy("$ofile","$tfile")) {            
            return "error";
        }        
        open(FILE,"$tfile");
        @contents=(<FILE>);
        close FILE;  
    } else {        
        return "error";
    }

    # sort the file according to time.    
    foreach $line (@contents) {
        my @tmp=split(/\s+/,$line);
        next unless (defined($tmp[0]));
        if ($tmp[0] =~ /\//) {
            $format_error=0;    # Clear any format error counts.
            my $date=$tmp[0];
            my $time=$tmp[1]; 
            my @ttmp=split(/\:/,$time);         
            my $hour=$ttmp[0];
            my $min=$ttmp[1];
            my $sec=$ttmp[2];
            @ttmp=split(/\//,$date);                         
            my $mon=$ttmp[0];
            my $mday=$ttmp[1];
            my $year=$ttmp[2];
            # The date is the first part of the line and this is what we can work with         
            if ($tmp[2] =~ /PM/) {          
                my @ttmp=split(/\:/,$time);  
                unless ($hour == 12) {          
                    $hour+=12;
                }                        
            }          
            # $time will be an integer representing the seconds since the epoc.  We use this as the key of the hash and can sort
            # the lines (the hash value) by sorting the key.

            # Note:  The time local function is a bit interesting in that it expects the day to be the actual day of the month
            # but the months are numbered starting from 0 so we need to subtract one from the month number
            $mon--;
            # Make certain you have the values needed or the script will fail
            next unless (($sec)&&($min)&&($hour)&&($mday)&&($mon)&&($year));
            next if ($mon > 11);
            eval { $time = timelocal($sec,$min,$hour,$mday,$mon,$year) };
         
            if ($lines_hash{"$time"}) {           
                my $ref=$lines_hash{"$time"};
                push(@$ref,"$line");
                $lines_hash{"$time"}=$ref;
            } else {
                my @array=();
                push(@array,$line);
                $lines_hash{"$time"}=\@array;
            }                        
        } elsif ($tmp[0] =~ /\:/) {
            $format_error++;
        } elsif ($tmp[0] =~ /^\w/) {
			 
            my $time=$tmp[2]; 
            my @ttmp=split(/\:/,$time);         
            my $hour=$ttmp[0];
            my $min=$ttmp[1];
            my $sec=$ttmp[2];
            
            my $mon=$tmp[0];
			$mon=$month_hash{$mon};
            my $mday=$tmp[1];
            my $year=$current_year;
			$mon--;
			
            # Make certain you have the values needed or the script will fail
            next unless (($sec)&&($min)&&($hour)&&($mday)&&($mon)&&($year));
            next if ($mon > 11);
            eval { $time = timelocal($sec,$min,$hour,$mday,$mon,$year) };
         
            if ($lines_hash{"$time"}) {           
                my $ref=$lines_hash{"$time"};
                push(@$ref,"$line");
                $lines_hash{"$time"}=$ref;
            } else {
                my @array=();
                push(@array,$line);
                $lines_hash{"$time"}=\@array;
            }   			
		
        } else {
            next;
        }

    }
    
    # Now, print it out
 
    open (FILE,">$sfile");
    foreach my $key (sort keys(%lines_hash)) {
        my $ref=$lines_hash{$key};
        my @array=@$ref;
        foreach my $a (@array) {
            print FILE "$a";
        }
    }    
    close FILE;      
    return $sfile;
}

sub gethost {
    # Try to get the host name associated with the IP address given
    my $ip=shift;
    my @info=`NBTSTAT -A $ip`;
    my $host;
    foreach my $i (@info) {
        if ($i =~ /UNIQUE/) {
            # The host name should be on this line
            my @tmp=split(/</,$i);
            $host=$tmp[0];
            last;
        }
    }
    if ($host) {
        return $host;
    } else {
        # If we could not find the host, just return the IP address
        return $ip;
    }
}
 
sub ping_test {
    my $url_test_site="google.com";
	my $cmd="ping $url_test_site";
	my @ping_info=`$cmd`;
	foreach my $p (@ping_info) {
		print "$p<br />";
	}
}


sub check_signed_on {
    
    # A return of 0 indicates that the crsrv log could not be found
    # A return 0f 1 indicates that the crsrv log was found but there is no evidence that XPS is signed on to Transnet
    # A return of 2 indicates that the crsrv log was found and that XPS is signed on to Transnet
    my $crsrv="${xpsDir}/crsrv.log";
    my $crsrv2="${xpsDir}/logs/crsrv.log";    
    if (-f $crsrv2) {
        $crsrv=$crsrv2;
    }    
    unless (-f $crsrv) {     
        return 0;
    }    
    # Open the log and see if we are signed on
    my $signed_on=1;
    open(LOG,"$crsrv");
    my @crsrv_info=(<LOG>);
    close LOG;
    foreach my $c (@crsrv_info) {
        if ($c =~ /Signed On to Transnet/) {
            if ($c =~ /Not Signed On/) {
                $signed_on=1;
            } else {
                $signed_on=2;
            }
        }
		# Any of these also indicate that Transnet is connected
		if (($c =~ /Credit session complete.*approved/)
			|| ($c =~ /DbsCreditReq/)
			|| ($c =~ /SignalServiceForSAFIfReqd/)
			) {			
			$signed_on=2;
		}	
		
		# This, of course, means Transnet is not connected
		if ($c =~ /Transnet Connection Down/) {			
			$signed_on=1;
		}	
	
    }    
    return $signed_on;
}
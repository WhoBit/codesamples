#!c:/perl/bin/perl
##
##  i_tools.pl -- a CGI script used in the Product portion of the Store Web-interface
##
##  some reports use a calendar (date selection) javascript tool taken from
##     http://www.mattkruse.com/javascript/calendarpopup/
##  2008-01-23 - Added SCRIPT_VERSION - kdg
##  2008-02-14 - Changed the wording to "Items On Order" - kdg
##  2008-05-14 - Changed query in RUGSKU2 to outer left join to deal with
##               situations where multiple discounts given on a rug led to
##               it not being reported - kdg
## 2008-11-03 - v2.0.3 - Added information about the earliest data in PERFSUM - kdg
## 2009-01-20 - v2.0.4 - Added Problem SKU's - kdg
## 2009-01-27 - v2.0.5 - Revised wording for Problem SKUs report - kdg
## 2009-02-20 - v2.0.6 - Added vendor check - kdg
## 2009-02-20 - v2.0.8 - Updated SKU Vendor check - kdg
## 2009-02-20 - v2.1.0 - Revised SKU Vendor method of checking supplies and samples - kdg
## 2009-03-02 - v2.1.1 - Revised for new DeptClass scheme - kdg
## 2009-03-05 - v2.1.2 - Moved vendor_hash to lookupdata.pm - kdg
## 2009-03-13 - v2.1.3 - Revised vendor number message to be 1 through 19 - kdg
## 2009-03-16 - v2.1.4 - Show Known Vendor for given ID in SKU Vendor check #6 - kdg
## 2009-03-16 - v2.1.5 - Removed Vendor check#5, allow for plu's with multiple bar codes - kdg
## 2009-03-17 - v2.1.6 - Corrected tcount - kdg
## 2009-03-18 - v2.1.7 - Adjusted verbage with restricted vendors - kdg
## 2009-03-27 - v2.1.8 - Modified to read from elderVillagesSku table
## 2009-03-30 - v2.1.9 - Minor cleanup to checking SKU and Vendor - kdg
## 2009-05-06 - v2.0.10 - Changed Mailing list to Customer list - kdg
## 2009-05-12 - v2.0.11 - Added InvCtrl to receiving report - kdg
## 2009-05-14 - v2.0.12 - Revised some wording related to Customer List - kdg
## 2009-05-18 - v2.0.13 - Revised receiving report to change heading to Dept from Dept/Class - kdg
## 2009-05-18 - v2.0.14 - Added check #6 to problemsku - kdg
## 2009-05-19 - v2.0.15 - Changed PLU to SKU in check #6 - kdg
## 2009-08-24 - v2.0.16 - Added Server Status - Altered to allow access to some reports if sql is not running - kdg
## 2009-11-30 - v2.1.1 - Work on PROBLEMSKU with barcodes - kdg
## 2009-12-15 - v2.1.2 - Added Check # 4c  to PROBLEMSKU - kdg
## 2009-12-16 - v2.1.3 - Corrected a vendor name comparison problem -kdg
## 2010-10-28 - v2.1.4 - Added link to promo reports - kdg
## 2010-11-05 - v2.1.5 - Modified postvoid in STORECREDIT , GIFTISSUE2, GIFTREDEEM2, etc to match regnum - kdg
## 2011-02-23 - v2.1.6 - Updated check 4c of PROBLEMSKU to add QOH column - kdg
## 2011-02-28 - v2.1.7 - Enabled PROMOTION reports
## 2011-03-11 - v2.1.8 - Added QOH to check #6 in PROBLEMSKU - kdg
## 2011-03-15 - v2.1.9 - Added $SQL_ENG - kdg
## 2011-04-28 - v2.1.10 - Added Store Valuation by Akron Status
## 2011-05-05 - v2.1.11 - Added MixMatch Discounts link - kdg
## 2012-01-24 - v2.1.12 - Added link to approved vendor list - kdg
## 2012-02-17 - v2.1.13 - Added check #5a in PROBLEMSKU - kdg
## 2012-03-01 - v2.1.14 - Replaced check #4c with #5a in ProblemSKU - kdg
## 2012-04-11 - v2.2.0 - Added ALERT_RPT - kdg
## 2012-04-12 - v2.2.1 - Added some directions to alerts - kdg
## 2012-05-01 - v2.2.2 - Enabled ALERTS - kdg
## 2012-05-11 - v2.2.3 - Started FAQ in ALERT_RPT - kdg
## 2012-05-16 - v2.2.4 - Removed StandardFooters from show_faq function - kdg
## 2012-05-25 - v2.2.5 - Update FAQ with info about issuing purchase orders - kdg
## 2012-06-14 - v2.2.6 - Updated FAQ for outside vendors - kdg
## 2012-06-27 - v2.2.7 - Escaped single quotes in alerts - kdg
## 2012-08-01 - v2.2.8 - Updated SHOW_FAQ_ANSWER - kdg
## 2012-09-11 - v2.2.9 - Corrected formatting for eda in AKRON_BACKORDER2 - kdg
## 2012-10-01 - v2.2.10 - Corrected query in VALUATIONSTATUS - kdg
## 2012-10-04 - v2.3.0 - Added valuation by VMCode - kdg
## 2012-10-24 - v2.3.1 - Added FAQ #6 - kdg
## 2012-10-25 - v2.3.2 - Added FAQ #7 - kdg
## 2013-02-15 - v2.3.3 - Adjusted check for valid skus to allow letters appended to the end of the SKU - kdg
## 2013-02-20 - v2.3.4 - Revised storenum and storeStatus to be global variables.  Revised some $storenum to be $StoreNum to not interfer with $storenum - kdg
## 2013-02-26 - v2.3.5 - Ordered the valuation by vendor and Akron Status - kdg
## 2013-03-13 - v2.3.6 - Correction to an order by with a union - kdg
## 2013-07-31 - v2.3.7 - Valuation by Vendor order by VendorId - kdg
## 2013-10-14 - v2.4.0 - Changed how we connect to database to allow for reporting with SQL is not running - kdg
## 2013-10-17 - v2.4.1 - Added call to BASICSTATUS if SQL is not running - kdg
## 2013-10-18 - v2.4.2 - Excuse SKU 9999 for having no vendor assigned - kdg
## 2013-10-22 - v2.5.0 - Added Valuation by pricepoint - kdg
## 2013-11-11 - v2.5.1 - Added link to No Sales report - kdg
## 2013-10-29 - v2.5.2 - Updated Receiving Report to give Summary option - kdg
## 2013-12-20 - v2.5.3 - Account for PLU 9999 for SAP POS 2.3 - kdg
## 2014-01-17 - v2.5.4 - Updated PROBLEMSKU for Souderton #7044 & #7027 - kdg
## 2014-03-26 - v2.5.5 - Added a total at the bottom of the MISC SKU report - kdg
## 2014-04-01 - v2.5.6 - Revised getting businessdate in PERFSUM2 - kdg
## 2014-06-12 - v2.5.7 - Allow SKU 9999 to not have a vendor associated with it - kdg
## 2014-08-05 - v2.5.8 - Added FAQ #8 - kdg
## 2014-10-03 - v2.5.9 - Corrected totaling the sum for MiscSKU - kdg
## 2014-10-23 - v2.6.0 - Added CSE report - kdg
## 2014-11-20 - v2.6.1 - Enabled CSE for Lancaster - kdg
## 2015-02-17 - v2.6.2 - Added @rug_stores for the SKU and Vendor Check - kdg
## 2015-03-03 - v2.6.3 - Added check #7 to SKU and Vendor Check - kdg
## 2015-03-30 - v2.6.4 - Exposed CSE report to all stores - kdg
## 2015-04-14 - v2.6.5 - Modified query in RUGSKU2 to account for RUG DESC profile prompt #31 - kdg
## 2015-08-25 - v2.6.6 - Revised checkPID - kdg
## 2015-10-19 - v2.6.7 - Account for txnvoidmod in CSE2 - kdg
## 2015-10-26 - v2.6.8 - Updated CSE2 to account for post-voided transactions - kdg
## 2015-11-10 - v2.6.9 - Updated CSE2 to note that the CSE info is encrypted until the following day - kdg
## 2016-01-22 - v2.6.10 - Added faq #9 - kdg

use constant SCRIPT_VERSION 	=> "2.6.10";

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

use TTV::utilities;
use TTV::cgiutilities qw(printResult_arrayref2 formatCurrency formatNumeric ElderLog);
use TTV::lookupdata;
use Math::Round qw( nearest );

# Define the database engine
my $SQL_ENG = setSql();
# declare global variables
my $q = new CGI; # create new CGI object.
my $query;       # global variable for storing SQL query string.
my @links;       # global variable for storing the links array.
my $tbl_ref;     # global variable for storing a reference to a query result.
my $sth;         # excecute statement handle object.


# colors calculated by \\saz\c\Projects\triversity\web\calccolors.pl
# my @RGB = (255,154,0);
my $dkcolor="ff9a00";
my $ltcolor="feeacc";

# print the HTML Header
my $style="BODY{font: normal 11pt times new roman,helvetica}\n".
          "FORM{font: normal 11pt times new roman,helvetica}\n".
          "TH{font: bold 11pt arial,helvetica}\n".
          "TD{font: normal 11pt arial,helvetica}\n".
          "table{font: normal 11pt arial,helvetica}\n".
          "input{font: normal 11pt times new roman,helvetica}\n".
          ".tableborder {border-color: #EBEACC; border-style: solid; border-width: 1px;}\n".
          ".table {border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tablehead {background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px; color: #$ltcolor;}\n".
          "A.headerlink:link {color: #$ltcolor; text-decoration: none; }\n".
          "A.headerlink:active {color: #$ltcolor; text-decoration: none;}\n".
          "A.headerlink:visited {color: #$ltcolor; text-decoration: none;}\n".
          "A.datalink:link {color: #000000;}\n".
          "A.datalink:active {color: #000000;}\n".
          "A.datalink:visited {color: #000000;}\n".
          ".tabledata {background-color: #$ltcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tabledatanb {background-color: #$ltcolor; border-color: #$ltcolor; border-style: solid; border-width: 1px;}\n". # nb=no-border
          "table.table a.sortheader {background-color:#$dkcolor; color:#$ltcolor; text-decoration: none;}\n".
          "table.table span.sortarrow { color:#$ltcolor; text-decoration: none; }".
          ".button1 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;
          ".button2 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #FF0000; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;        
          ".redalert {color: red;)\n". 
          "";
print $q->header( -type=>'text/html');
print '<SCRIPT LANGUAGE="JavaScript" SRC="/CalendarPopup.js"></SCRIPT>'."\n";
print '<SCRIPT LANGUAGE="JavaScript">document.write(CalendarPopup_getStyles());</SCRIPT>'."\n";
print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

print $q->start_html(-title=>'Village Elder',
                     -author=>'saz@villages-mcc.org',
                     -style=>{'code'=>$style},
                     );

print '<SCRIPT LANGUAGE="JavaScript" SRC="/CalendarPopup.js"></SCRIPT>';
print '<SCRIPT LANGUAGE="JavaScript">document.write(CalendarPopup_getStyles());</SCRIPT>';

# Is the database running?
 
my $sql12="dbsrv12.exe"; 
my $sql_running=0;
if (checkPID($sql12)) {
    $sql_running=1;
}
#$sql_running=1;

my $filepath = "C:\\mlink\\akron";
my $logfile = "C:\\MLINK\\akron\\i_reports.log";

# Figure out which report to run.
my $report = ($q->param("report"))[0];
$report = "DEFAULT" unless $report;
# initialize the database handle
my $dsn; 
my $dbh;  
my $storenum;  
my $storeStatus; 
if ($sql_running) {
    $dsn = "DSN=BACKOFF;ENG=$SQL_ENG;UID=****;PWD=****;";
    $dbh = TTV::utilities::openODBCDriver($dsn);
    $storenum = getStoreNum($dbh);  
    $storeStatus=$store_cc_hash{$storenum};	
} else {
    print $q->h2({-class=>'redalert'},"NOTICE: The SQL Database is not running"  );       
}
my $str;
my %params = $q->Vars;
foreach my $key (keys(%params)) {
   next if ($key eq "report");
   next if ($key eq "pwd");
   $str .= "&" . $key ."=". $params{$key} ;
}
ElderLog($logfile, "$report called ".  ($str gt '' ? "with $str" : '') );

print $q->start_table({-width=>"400"}), "\n";
print $q->start_Tr(), $q->start_td();

# depending on the value of the 'report' parameter,
# we will build different queries, and expect additional paramters.
SWITCH: {

   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: DEFAULT
   # Description: returns Quantity on Hand data
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   if ($report eq "DEFAULT") {
=pod
      print $q->p("<b>Instructions:</b> click on the report name in the list below.  ".
                  "For more detailed information about a report, check out the ".
                  $q->a({href=>"/pdf/3-Backoffice Procedures/VillageElderReports.pdf", -target=>"_new"}, "help") ." section"), "\n";
=cut
      # Disabled reports will not show on the menu
      my @disabled_reports=(
        #"PROMOTION"
      );
      
      # fields: [display name, script, report, report note]
      my @reportlist = (
         ["Items On Order", "/cgi-bin/i_reports.pl", "AKRON_BACKORDER",
          "Prints a list of items you currently have on order in Akron, including backordered items ".
              "and orders in our system without a corresponding PO in your system."
         ],
         ["Promotions & MixMatch", "/cgi-bin/i_reports.pl", "PROMOTION",
          "Lists Configured Promotions and MixMatch Discounts"
         ],         
         ["Store Valuation", "/cgi-bin/i_reports.pl", "VALUATION",
          "A one page summary of the store's current valuation."
         ],
         ["Item Tax", "/cgi-bin/i_reports.pl", "TAXATION",
          "Lists tax codes along with the number of SKUs using that tax. Used to make sure the correct ".
             "items are being taxed the correct amount."
         ],
         ["Merchandise Credit", "/cgi-bin/i_reports.pl", "STORECREDIT",
          "Lists Merchandise Credits issued and redeemed."
         ],
         ["Gift Certificate", "/cgi-bin/i_reports.pl", "GIFTCERTIFICATES",
          "Lists Gift Certificates issued and redeemed."
         ],
         ["Zip Code", "/cgi-bin/i_reports.pl", "ZIPCODE",
          "Shows a summary of customer Zip Codes entered during the sales process."
         ],

         ["Special Customer List Report", "/cgi-bin/i_tools_mail.pl", "MAILEXPORT1",
          "Export or delete Layaway, Tax-Exempt, and other special customer records."
         ],
         ["Employee List Report", "/cgi-bin/i_tools_mail.pl", "EMPEXPORT1",
          "Exports employee records."
         ],
         ["Cost of Goods Sold", "/cgi-bin/i_reports_cost.pl", "COGS",
          "Show cost of goods sold over a given period."
         ],
         ["Inventory Adjustment", "/cgi-bin/i_reports_cost.pl", "INVADJUST",
          "Show Inventory Adjustments over a given period."
         ],
         ["Discounts", "/cgi-bin/i_reports_cost.pl", "DISCOUNTS",
          "Show Discounts over a given period."
         ],
         ["Net Sales", "/cgi-bin/i_reports_cost.pl", "SALESRPT",
          "Expands on Store Manager's \"Sales Report\" by breaking down returns, ".
          "discounts and net sales by taxable and non-taxable."
         ],
         ["No Sales", "/cgi-bin/i_reports_cost.pl", "NOSALES",
          "Show items that have not sold during a specified time period."          
         ],         
         ["Receiving", "/cgi-bin/i_reports.pl", "RECEIVING",
          "Show items received into the store over a given period."
         ],
         ["Paid In/Out", "/cgi-bin/i_reports.pl", "PAIDINOUT",
            "Show Paid in/Paid Out non-merchandise sales transactions with reasons."
         ],
         ["Non-Merchandise Donation", "/cgi-bin/i_reports.pl", "NMDONATION",
            "Show \"Donations\" non-merchandise transactions with reasons."
         ],
         ["Non-Merchandise Other", "/cgi-bin/i_reports.pl", "NMOTHER",
            "Show \"Other\" non-merchandise transactions with reasons."
         ],
         ["Layaways", "/cgi-bin/i_reports.pl", "LAYAWAY",
          "Shows (initially open) layaway transactions."
         ],
         ["Misc SKU", "/cgi-bin/i_reports.pl", "MISCSKU",
          "Shows a list of Miscellaneous SKUs rung up at the register."
         ],
         ["Rug SKU", "/cgi-bin/i_reports.pl", "RUGSKU",
          "Shows the description, cost and price of Rug SKUs rung up at the register as SKU '1199'."
         ],
         ["Daily Performance Summary", "/cgi-bin/i_reports.pl", "PERFSUM",
          "Shows average price and units per txn."
         ],
         ["No Barcode", "/cgi-bin/i_reports.pl", "NOBARCODE",
          "Shows SKUs without Barcodes."
         ],
         ["SKU and Vendor Check", "/cgi-bin/i_reports.pl", "PROBLEMSKU",
          "Shows improperly coded SKUs or Vendor ID numbers."
         ],
         ["Server Status", "/cgi-bin/i_reports_server.pl", "STATUS",
          "Shows server status."
         ],     
		["CSE", "/cgi-bin/i_reports.pl", "CSE",
		"Shows a summary of Community Shopping Event data."
		],			 
      );

        if ($sql_running) {
            print $q->p("<b>Instructions:</b> click on the report name in the list below.  ".
                  "For more detailed information about a report, check out the ".
                  $q->a({href=>"/pdf/3-Backoffice Procedures/VillageElderReports.pdf", -target=>"_new"}, "help") ." section"), "\n";
        
            # Check to see if there is a traffic counter configured
            my $tctype = getConfig($dbh, 'TrafficCounter|Type');
            if ($tctype) {
                push @reportlist,
                ["Traffic Counter", "/cgi-bin/i_reports_traffic.pl", "TCDEFAULT",
                    "Traffic Counter records."
                ];
            }
		 
			if ($storenum == 3187) {	# Store Specific reports can be put here
				#push @reportlist,
	 			
			}
            if (1) { 
                # Check to see if there are any alerts
                my $alert=0; 
                my $query = "select count(*) from elderConfig where ckey like 'ALERT:%'";
                my $sth = $dbh->prepare($query);    
                $sth->execute();  

                while (my @tmp = $sth->fetchrow_array) { 
                    $alert=$tmp[0];
                }

                if ($alert) {
                print $q->p(
                   $q->a({-href=>"/cgi-bin/i_reports.pl?report=ALERT_RPT", -class=>"button2"}, "ALERTS").
                   "NOTICE: You have $alert alerts to review." );        

                }       
            }
            $dbh->disconnect();
        } else {
            @reportlist = (
                ["Server Status", "/cgi-bin/i_reports_server.pl", "BASICSTATUS",
                "Shows server status."
                ],               
            );
        }
      
      
      if (1) {
   
         foreach my $reportref (@reportlist) {
            my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
            next if (grep /$reportname/,@disabled_reports);
            print $q->p(
               $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
               $reportnote );
         }
      }

      print $q->a({href=>"/pdf/3-Backoffice Procedures/VillageElderReports.pdf", -target=>"_new"}, "help");
      StandardFooter();
   }

=pod
sub AKRON_BACKORDER
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: AKRON_BACKORDER
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "AKRON_BACKORDER") {
      print $q->h2("Items On Order Report");

      print $q->p(
      	"This report shows a list of \"on order\" items for your store in our system that ".
        "don't have a corresponding purchase order in your system.  Items on orders generated ".
        "in Akron rather than in your store system will appear here until you receive those ".
        "orders.  This includes backordered item orders, new items orders, and Company store ".
        "orders generated by Akron staff.
      ");
#      print $q->p(
#         "Every night a new \"backorders\@akron\" pending purchase order is generated by ".
#         "Akron and polled to the store.  This purchase order contains backordered items as well ".
#         "as items that are shown to be \"on order\" in Akron but are not on a corresponding ".
#         "purchase order in Triversity (such as New Items orders).  Putting these items ".
#         "in the pending purchase order will keep the Generate Suggested Orders function ".
#         "from ordering items that are already on order. ");

#      print $q->p(
#         "When viewed in Store Manager, the \"backorders\@akron\" purchase order contains very ".
#         "little information of interest to store staff.  It just contains a list of items ".
#         "and quantity, with no detail such as the original purchase order or the estimated ".
#         "date of arrival for the item. Thus, we decided to poll down a corresponding database ".
#         "file with more information about the items on purchase order. ");

#      print $q->p(
#         "This report allows you to browse this information. If you wish to cancel backordered ".
#         "items, please use the ".
#         $q->a({-href=>"/cgi-bin/i_tools.pl?report=CANCEL_BACKORDER"},"Cancel Backorder Tool")
#         );

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"AKRON_BACKORDER2"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Options:")));

      my $view_options = $q->option({-value=>'ALL', -selected=>undef}, "All Items").
                         $q->option({-value=>'CANCEL'}, "Cancelable").
                         $q->option({-value=>'NOCANCEL'}, "un-Cancelable");
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Items to View:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"view", -size=>"1"}), $view_options)
                  );


      my $output_options = $q->option({-value=>'VERBOSE', -selected=>undef}, "Verbose").
                           $q->option({-value=>'PRINT'}, "Printable");
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Output Format:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"output", -size=>"1"}), $output_options)
                  );

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";


      StandardFooter();
   }
=pod
sub AKRON_BACKORDER2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: AKRON_BACKORDER2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "AKRON_BACKORDER2") {
      print $q->h2("List of Backorder+ items");

      my $view   = trim(($q->param("view"))[0]); # CANCEL, NOCANCEL, ALL
      my $output = trim(($q->param("output"))[0]); # PRINT, VERBOSE
      my $sortfield = trim(($q->param("sort"))[0]);
      $sortfield = "SKU" unless $sortfield;
      $sortfield = '"' . $sortfield . '"';

      #print "view = $view  output = $output<br>\n";
      my $whereclause = "";
      $whereclause = "where ordstat in ('0', '') " if ($view eq "CANCEL");
      $whereclause = "where ordstat > '0' " if ($view eq "NOCANCEL");

      my $selectclause = "*";
      my @format;

      if ($output eq "PRINT") {
         if ($view eq "CANCEL") {
            $selectclause = "item as \"SKU\", sk_desc as \"Description\", ".
                            "oldpo as \"Orig PO\", oldso as \"Orig SO\", ".
                            "convert(varchar(10),ordate,21) as \"Order Date\", qtybo as \"Qty BO\", eda" ;
            @format = ('', '', '', '', '', {'align' => 'Right', 'num' => '.0'}, '' );
         } elsif ($view eq "NOCANCEL") {
            $selectclause = "item as \"SKU\", sk_desc as \"Description\", ".
                            "oldpo as \"Orig PO\", oldso as \"Orig SO\", ".
                            "newpo as \"New PO\", newso as \"New SO\", qtybo as \"Qty BO\"" ;
            @format = ('', '', '', '', '', '', {'align' => 'Right', 'num' => '.0'} );
         } else {
            $selectclause = "item as \"SKU\", sk_desc as \"Description\", ".
                            "oldpo as \"Orig PO\", oldso as \"Orig SO\", ".
                            "newso as \"New SO\", qtybo as \"Qty BO\", eda" ;
            @format = ('', '', '', '', '', {'align' => 'Right', 'num' => '.0'}, '' );
         }
      }

        if ($output eq "VERBOSE") {
            $selectclause = "item as \"SKU\", sk_desc as \"Description\", ".
                         "oldpo as \"Orig PO\", oldso as \"Orig SO\", convert(varchar(10),ordate,21) as \"OrderDate\", ".
                         "newpo as \"New PO\", newso as \"New SO\", qtybo as \"Qty BO\", price, cost, eda, ordstat";
            @format = ('', '', '', '', '', '', '', {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''}, {'align' => 'Right', 'currency' => ''}, '', '');
        }

        $query = dequote(<< "        ENDQUERY");
            select $selectclause
            from elderBackorder
            $whereclause
            order by $sortfield
        ENDQUERY

        #print $q->pre($query), "<br>";

        $tbl_ref = execQuery_arrayref($dbh, $query);

        my $newsortURL = "/cgi-bin/i_reports.pl?report=AKRON_BACKORDER2&view=$view&output=$output&sort=\$_";
        my @headerlinks = ($newsortURL, $newsortURL, $newsortURL,  $newsortURL, $newsortURL, $newsortURL,
                     $newsortURL, $newsortURL, $newsortURL,  $newsortURL, $newsortURL, $newsortURL);

        if (recordCount($tbl_ref)) { 
        # The documentation says that the Estimated Date of Arrival field is in the format of MM/YYYY.  This next section reformats
        # the eda data accordingly.
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $field=6;
            if ($output eq "VERBOSE") {
                $field=10;
            }
            my $eda=$$thisrow[$field];
            if ($eda) {                
                my @tmp=split(/-/,$eda);
                my $m=$tmp[1];
                my $y=$tmp[0];
                $eda="$m"."/"."$y";
                $$thisrow[$field]=$eda;                
            }
        }
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); 
        }
        else { print $q->b("no records found<Br>\n"); }

        $dbh->disconnect;

        StandardFooter();
    }
=pod
sub PROMOTION
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: PROMOTION
   # Description: Promotion Report Menu
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "PROMOTION") {
        print $q->h2("Configured Promotions and MixMatch Discounts");
      my @reportlist = (
            ["Show Existing Promotions", "/cgi-bin/i_reports_promo.pl", "SHOWPROMO", 
            "Display a list of defined \"Promotions\"."
            ],
            ["Show SKUs on Promotion", "/cgi-bin/i_reports_promo.pl", "SKUsONPROMO", 
            "Displays a list of SKU that are on currently active \"Promotions\"."
            ],
            ["MixMatch Discounts", "/cgi-bin/i_reports_promo.pl", "MIXMATCHRPT",
            "Show what MixMatch discounts are configured."
            ],   
        );
      if (1) {
         foreach my $reportref (@reportlist) {
            my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
            print $q->p(
               $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
               $reportnote );
         }
      }      
      
      StandardFooter();
   }      
=pod
sub VALUATION
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: VALUATION
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "VALUATION") {
      print $q->h2("Store Valuation Summary");

      my @ltm = localtime();
      my $datetime = sprintf("%4d-%02d-%02d %02d:%02d:%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3], $ltm[2], $ltm[1], $ltm[0]);
      print $q->p("The following is the current Store valuation as of $datetime"), "\n";

      # Valuation by Vendor
      print $q->h3("Valuation Summary");

      $query = dequote(<< "      ENDQUERY");
         select
            count(*) as "SKUs",
            (select count(*) from inventory_current where qtycurrent > 0) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
      ENDQUERY

      $tbl_ref = execQuery_arrayref($dbh, $query);
      my @format = ({'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

    if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
    else { print $q->b("no records found<Br>\n"); }

    # are there SKUs with invalid vendors?
    my $skus_with_invalid_vendor = 0;
    $query = "
        select 
            count(*) as \"SKUs\" 
        from 
            plu p 
            left outer join vendor v on p.vendorid=v.vendorid 
        where 
            v.vendorId is null
            and 
            (p.plunum <> 9999 and p.propromptid <> 26)
            ";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    $skus_with_invalid_vendor = $$tbl_ref[1]->[0] if (recordCount($tbl_ref));
    my $orderby="order by v.vendorid";
    if ($skus_with_invalid_vendor) {
        # If we have SKUs with invalid vendors, then the two queries below will be united and we want the order by to 
        # only be in the second part of the query.
        $orderby='';
    }

    # Valuation by Vendor
    print $q->h3("Valuation by Vendor");

      $query = dequote(<< "      ENDQUERY");
         select
            v.VendorId, v.VendorName,
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price",
            0 as "%TotPrice"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            join vendor v on p.vendorid=v.vendorid
         group by v.vendorid, v.vendorName
         $orderby
		 
      ENDQUERY

      if ($skus_with_invalid_vendor) {
         $query .= dequote(<< "         ENDQUERY");
         union
         select
            'n/a' as "VendorId", 'unknown vendor' as "VendorName",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price",
            0 as "%TotPrice"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join vendor v on p.vendorid=v.vendorid
         where v.vendorId is null
 
         order by 1
         ENDQUERY
      }

      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @fieldsum = ('Total:', '', 0, 0, 0, 0, 0, 0, 0);

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];

         for my $field (2, 3, 4, 5, 6, 7)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
         }

      }

      my $sumprice = $fieldsum[7];
      push @$tbl_ref, \@fieldsum;

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];
         my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[7] / $sumprice * 100);
         $thisrow->[8] = $percent;

         my $thisvendorId = $thisrow->[0];
         next if ($thisvendorId eq "Total:");
         $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONVENDOR&VendorId=$thisvendorId", -class=>"datalink"}, $thisvendorId);
      }


      #my @links = ( '/cgi-bin/i_reports.pl?report=VALUATIONVENDOR&VendorId=$_' );
      @format = ('', '', {'align' => 'Right', 'num' => '.0'},
                {'align' => 'Right', 'num' => '.0'},
                {'align' => 'Right', 'num' => '.0'},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'num' => '.2'});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }


      # are there SKUs with invalid departments?
      my $skus_with_invalid_dept = 0;
      $query = "select count(*) as \"SKUs\" from plu p left outer join Department d on p.deptnum=d.deptnum where d.deptnum is null";
      $tbl_ref = execQuery_arrayref($dbh, $query);
      $skus_with_invalid_dept = $$tbl_ref[1]->[0] if (recordCount($tbl_ref));

      # Valuation by Department
      print $q->h3("Valuation by Department Group");

      $query = dequote(<< "      ENDQUERY");
         select
            dg.Description as "Group",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price",
            0 as "%TotPrice"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            join Department d on p.deptnum =d.deptnum
            join Dept_Group dg on d.groupnum=dg.groupnum
         group by dg.groupNum, dg.Description
      ENDQUERY

      if ($skus_with_invalid_dept) {
         $query .= dequote(<< "         ENDQUERY");
         union
         select
            'unknown' as "Group",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price",
            0 as "%TotPrice"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join Department d on p.deptnum =d.deptnum
         where d.deptnum is null
         ENDQUERY
      }
      $query .= " order by 1";

      $tbl_ref = execQuery_arrayref($dbh, $query);

      #               [Group, SKUs, WithQty, QOH, lastcost, avgcost price, %totalprice
      @fieldsum = ('Total:', 0, 0, 0, 0, 0, 0, 0);

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];

         for my $field (1, 2, 3, 4, 5, 6)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
         }

      }

      $sumprice = $fieldsum[6];
      push @$tbl_ref, \@fieldsum;

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];
         my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[6] / $sumprice * 100);
         $thisrow->[7] = $percent;

         my $thisgroup = $thisrow->[0];
         next if ($thisgroup eq "Total:");
         $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONGROUP&groupnum=$thisgroup", -class=>"datalink"}, $thisgroup);
      }

      #my @links = ( '/cgi-bin/i_reports.pl?report=VALUATIONGROUP&groupnum=$_' );
      @format = ('', {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'num' => '.2'});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }

      print $q->p("<b>Note:</b> To get detailed information about a given Vendor's or Group's valuation, ".
                  "click on the underlined VendorId or Group above."), "\n";

###
    # Valuation by Akron Status
    print $q->h3("Valuation by Akron Status");

	# This is the original way I did the query but it puts "None" in alphabetical order rather than at the bottom.
    $query = dequote(<< "    ENDQUERY");
     select
        uf3.Description as "Akron Status",                          
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
     from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
        left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3 
     where
        uf3.Description is not NULL        
     group by 
        uf3.codename, uf3.Description
    union
     select
        'None' as "Akron Status",                          
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
     from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
        left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3 
     where
        uf3.Description is NULL
    order by 1
    ENDQUERY
	
	# This query puts "None" at the bottom of the listing.
    $query = dequote(<< "    ENDQUERY");
     select
        uf3.Description as "Akron Status",                          
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
     from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
        left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3 
     where
        uf3.Description is not NULL        
     group by 
        uf3.codename, uf3.Description
    order by 1		
    ENDQUERY
		
    $tbl_ref = execQuery_arrayref($dbh, $query);
	
    $query = dequote(<< "    ENDQUERY");		
     select
        'None' as "Akron Status",                          
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
     from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
        left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3 
     where
        uf3.Description is NULL
    order by 1
    ENDQUERY
	
	my $tbl_ref2 = execQuery_arrayref($dbh, $query);
	# Tack this row on to the results from the first query
	my $datarow=@$tbl_ref2[1];
	push @$tbl_ref, $datarow;	
	###
 
	
    if (recordCount($tbl_ref)) { 
        @fieldsum = ('Total:', 0, 0, 0, 0, 0, 0, 0);

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            for my $field (1, 2, 3, 4, 5, 6)  {
                $fieldsum[$field] += trim($$thisrow[$field]);
            }
        }    
        $sumprice = $fieldsum[6];
        push @$tbl_ref, \@fieldsum;

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[6] / $sumprice * 100);
            $thisrow->[7] = $percent;
            my $thisstatus = $thisrow->[0];
            next if ($thisstatus eq "Total:");
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONSTATUS&status=$thisstatus", -class=>"datalink"}, $thisstatus);
        }    
        @format = ('', {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'num' => '.2'});    
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
    } else { 
        print $q->b("no records found<Br>\n"); 
    }    
###         
###
	
    # Valuation by VMCode
    print $q->h3("Valuation by VMCode");

    $query = dequote(<< "    ENDQUERY");
     select
		uf2.codename as "VMCodes",  
        uf2.Description as "VM Desc.",                          
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
     from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
        left outer join user_flag_code uf2 on uf2.userflagnum=2 and uf2.codeid=p.userflagnum2
     where
        uf2.Description is not NULL 
	
     group by 
        uf2.codename, uf2.Description
	order by 1
     
    ENDQUERY
     
    $tbl_ref = execQuery_arrayref($dbh, $query);
	
	# Now we get the items that have no VMCode assigned - these are the unknowns.
    $query = dequote(<< "    ENDQUERY"); 
     select
        'None' as "VMCode",                          
		' ' as "VM Desc.",
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
     from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
        left outer join user_flag_code uf2 on uf2.userflagnum=2 and uf2.codeid=p.userflagnum2 
     where
        uf2.Description is NULL	
    ENDQUERY
	
	$tbl_ref2 = execQuery_arrayref($dbh, $query);
	# Tack this row on to the results from the first query
	$datarow=@$tbl_ref2[1];
	push @$tbl_ref, $datarow;
	
    if (recordCount($tbl_ref)) { 
        @fieldsum = ('Total:', , 0, 0, 0, 0, 0, 0,0);

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            for my $field ( 2, 3, 4, 5, 6,7)  {
                $fieldsum[$field] += trim($$thisrow[$field]);
            }
        }    
        $sumprice = $fieldsum[7];
        push @$tbl_ref, \@fieldsum;

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[7] / $sumprice * 100);
            $thisrow->[8] = $percent;
            my $thisstatus = $thisrow->[0];
            next if ($thisstatus eq "Total:");
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONVMCODE&status=$thisstatus", -class=>"datalink"}, $thisstatus);
        }    
        @format = ('', '',
				{'align' => 'Right', 'num' => '.0'},
				{'align' => 'Right', 'num' => '.0'},
				{'align' => 'Right', 'num' => '.0'},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'num' => '.2'});    
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
    } else { 
        print $q->b("no records found<Br>\n"); 
    }  

 	################
    # Valuation by Price Point
    ################
    print $q->h3("Valuation by PricePoint");
 
    $query = dequote(<< "    ENDQUERY");
    select     
        if retailprice < 5 then 'P05' 
        else
        if retailprice < 10 then 'P10'
        else
        if retailprice < 15 then 'P15'
        else
        if retailprice < 20 then 'P20'
        else
        if retailprice < 25 then 'P25'
        else
        if retailprice < 30 then 'P30'
        else
        if retailprice < 35 then 'P35'
        else
        if retailprice < 40 then 'P40'
        else
        if retailprice < 45 then 'P45'
        else
        if retailprice < 50 then 'P50'
        else
        if retailprice < 75 then 'P55'
        else
        if retailprice < 10 then 'P60'
        else
        if retailprice < 150 then 'P65'
        else
        if retailprice < 200 then 'P70'
        else
        'P75'
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        endif
        as PricePoint,
  		
        count(p.plunum) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
         
    from 
        plu p 
        left outer join inventory_current ic on ic.plunum=p.plunum
  
	group by PricePoint
    order by 1
     
    ENDQUERY
     
    $tbl_ref = execQuery_arrayref($dbh, $query);  
    if (recordCount($tbl_ref)) { 
        @fieldsum = ('Total:', 0, 0, 0, 0, 0, 0 );

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
 
            for my $field ( 1, 2, 3, 4, 5,6 )  {
                $fieldsum[$field] += trim($$thisrow[$field]);
            }
        }    
        $sumprice = $fieldsum[6];
        push @$tbl_ref, \@fieldsum;

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[6] / $sumprice * 100);
            $thisrow->[7] = $percent;
            my $thisstatus = $thisrow->[0];
            next if ($thisstatus eq "Total:");
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONPRICEPOINT&status=$thisstatus", -class=>"datalink"}, $thisstatus);
        }    
        @format = ('',  
				{'align' => 'Right', 'num' => '.0'},
				{'align' => 'Right', 'num' => '.0'},
				{'align' => 'Right', 'num' => '.0'},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'num' => '.2'});    
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
        print $q->p("Price Point determined from store's retail price for each item as follows:");
        print "<pre>
        P05 Under \$5.00
        P10 From \$5.00 to under \$10.00
        P15 From \$10.00 to under \$15.00
        P20 From \$15.00 to under \$20.00
        P25 From \$20.00 to under \$25.00
        P30 From \$25.00 to under \$30.00
        P35 From \$30.00 to under \$35.00
        P40 From \$35.00 to under \$40.00
        P45 From \$40.00 to under \$45.00
        P50 From \$45.00 to under \$50.00
        P55 From \$50.00 to under \$75.00
        P60 From \$75.00 to under \$100.00
        P65 From \$100.00 to under \$150.00
        P70 From \$150.00 to under \$200.00
        P75 \$200.00 and over
        </pre>";
    } else { 
        print $q->b("no records found<Br>\n"); 
    }      
 
    $dbh->disconnect;
    StandardFooter();
   }


=pod
sub VALUATIONVENDOR
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: VALUATIONVENDOR
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "VALUATIONVENDOR") {

      my $VendorId = trim(($q->param("VendorId"))[0]);      

      # get vendor name
      my $vendorname = '';
      if ($VendorId eq "n/a") {
         $vendorname = 'unknown';
      } else {
         $query = "select VendorName from Vendor where VendorId='$VendorId'";

         $tbl_ref = execQuery_arrayref($dbh, $query);
         if (recordCount($tbl_ref) > 0) {
            $vendorname = @$tbl_ref[1]->[0];
         }
      }

      print $q->h1("Vendor Valuation Summary for $VendorId ($vendorname)");

      print $q->h2("Total Vendor Valuation");

      if ($VendorId eq "n/a") {
         $query = dequote(<< "         ENDQUERY");
         select
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join vendor v on p.vendorid=v.vendorid
         where v.vendorid is null
         ENDQUERY
      } else {
         $query = dequote(<< "         ENDQUERY");
         select
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join vendor v on p.vendorid=v.vendorid
         where v.vendorid = '$VendorId'
         ENDQUERY
      }

      $tbl_ref = execQuery_arrayref($dbh, $query);
      my @format = ({'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }


      print $q->h2("Vendor SKU Detail");

      if ($VendorId eq "n/a") {
         $query = dequote(<< "         ENDQUERY");
         select
            p.plunum as "SKU",
            p.pludesc as "Description",
            ic.qtycurrent as "QOH",
            ic.lastvendorcost as "lastcost",
            p.cost as "avgcost",
            p.retailprice as "price",
            ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
            p.cost*ic.qtycurrent as "ext_avgcost",
            p.retailprice*ic.qtycurrent as "ext_price"
             
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join vendor v on p.vendorid=v.vendorid
         where v.vendorid is null
         order by p.plunum
         ENDQUERY
      } else {
         $query = dequote(<< "         ENDQUERY");
         select
            p.plunum as "SKU",
            p.pludesc as "Description",
            ic.qtycurrent as "QOH",
            ic.lastvendorcost as "lastcost",
            p.cost as "avgcost",
            p.retailprice as "price",
            ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
            p.cost*ic.qtycurrent as "ext_avgcost",
            p.retailprice*ic.qtycurrent as "ext_price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join vendor v on p.vendorid=v.vendorid
         where v.vendorid = '$VendorId'
         order by p.plunum         
         ENDQUERY
      }

      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @fieldsum = ('Total:', '', 0, '', '', '', 0, 0, 0);

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];

         for my $field (2, 6, 7, 8)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
         }

         my $thissku = $thisrow->[0];
         $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_products.pl?report=PRODINFO&SKU=$thissku", -class=>"datalink"}, $thissku);
      }

      push @$tbl_ref, \@fieldsum;

      #my @links = ( '/cgi-bin/i_products.pl?report=PRODINFO&SKU=$_' );
      @format = ('', '', {'align' => 'Right', 'num' => '.0'},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''},
                 {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }

      $dbh->disconnect;
      StandardFooter();
   }
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: VALUATIONPRICEPOINT
    # Description:
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "VALUATIONPRICEPOINT") {
        my $status = trim(($q->param("status"))[0]);       
        print $q->h2("PricePoint Valuation: $status");
 
        #  Figure the where clause to find the correct pricepoint
        my $where_clause='where p.retailprice > 200';
        my $added_clause = '';
        
        my %price_hash=(
            'P05' => '5',
            'P10' => '10',
            'P15' => '15',
            'P20' => '20',
            'P25' => '25',
            'P30' => '30',
            'P35' => '35',
            'P40' => '40',
            'P45' => '45',
            'P50' => '50',
            'P55' => '75',
            'P60' => '100',
            'P65' => '150',
            'P70' => '200',                        
        );
        my %price_lower_hash=(
             
            'P10' => '5',
            'P15' => '10',
            'P20' => '15',
            'P25' => '20',
            'P30' => '25',
            'P35' => '30',
            'P40' => '35',
            'P45' => '40',
            'P50' => '45',
            'P55' => '50',
            'P60' => '75',
            'P65' => '100',
            'P70' => '150',                        
        );        
        if ($price_hash{$status}) {
            $where_clause = " where p.retailprice < $price_hash{$status}";
        }        
        if ($price_lower_hash{$status}) {
            $added_clause = " and p.retailprice >= $price_lower_hash{$status}";
        }
 
        $query = dequote(<< "        ENDQUERY");
            select
                p.plunum as "SKU",
                p.pludesc as "Description",
                p.deptnum as "Dept",
                ic.qtycurrent as "QOH",
                ic.lastvendorcost as "lastcost",
                p.cost as "avgcost",
                p.retailprice as "price",
                ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
                p.cost*ic.qtycurrent as "ext_avgcost",
                p.retailprice*ic.qtycurrent as "ext_price"
            from 
                plu p 
            left outer join 
                inventory_current ic on ic.plunum=p.plunum
            left outer join 
                Department d on p.deptnum =d.deptnum
            $where_clause 
            $added_clause

            order by 
            p.plunum
        ENDQUERY

    
        $tbl_ref = execQuery_arrayref($dbh, $query);

        my @fieldsum = ('Total:', '', '', 0, '', '', '', 0, 0, 0);

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            for my $field (3, 7, 8, 9)  {
               $fieldsum[$field] += trim($$thisrow[$field]);
            }
            my $thissku = $thisrow->[0];
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_products.pl?report=PRODINFO&SKU=$thissku", -class=>"datalink"}, $thissku);
        }
        push @$tbl_ref, \@fieldsum;
        
        my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''});

        if (recordCount($tbl_ref)) { 
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
        } else { 
            print $q->b("no records found<Br>\n"); 
        }

        $dbh->disconnect;
        StandardFooter();
    }      
=pod
sub VALUATIONVMCODE
=cut
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: VALUATIONVMCODE
    # Description:
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "VALUATIONVMCODE") {
        my $status = trim(($q->param("status"))[0]);       
        print $q->h2("VMCode Valuation: $status");
 
        # Get the code id for this status
        my $codeid = 0;
        $query = "
            select 
                codeid 
            from 
                user_flag_code 
            where 
                codename like '$status'
            and
                userflagnum = 2
            ";

        unless (($status eq "unknown") || ($status =~ /None/i)){
            $tbl_ref = execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref) > 0) {
                $codeid = @$tbl_ref[1]->[0];
            }        
        }
        
        if ($codeid) {
         $query = dequote(<< "         ENDQUERY");
         select
            p.plunum as "SKU",
            p.pludesc as "Description",
            p.deptnum as "Dept",
            ic.qtycurrent as "QOH",
            ic.lastvendorcost as "lastcost",
            p.cost as "avgcost",
            p.retailprice as "price",
            ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
            p.cost*ic.qtycurrent as "ext_avgcost",
            p.retailprice*ic.qtycurrent as "ext_price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join Department d on p.deptnum =d.deptnum
         where 
            p.userflagnum2 = $codeid
         order by 
            p.plunum
         ENDQUERY
        } else {
         $query = dequote(<< "         ENDQUERY");
         select
            p.plunum as "SKU",
            p.pludesc as "Description",
            p.deptnum as "Dept",
            ic.qtycurrent as "QOH",
            ic.lastvendorcost as "lastcost",
            p.cost as "avgcost",
            p.retailprice as "price",
            ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
            p.cost*ic.qtycurrent as "ext_avgcost",
            p.retailprice*ic.qtycurrent as "ext_price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join Department d on p.deptnum =d.deptnum
         where 
            p.userflagnum2 is null
         order by 
            p.plunum
         ENDQUERY
        }

        $tbl_ref = execQuery_arrayref($dbh, $query);

        my @fieldsum = ('Total:', '', '', 0, '', '', '', 0, 0, 0);

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            for my $field (3, 7, 8, 9)  {
               $fieldsum[$field] += trim($$thisrow[$field]);
            }
            my $thissku = $thisrow->[0];
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_products.pl?report=PRODINFO&SKU=$thissku", -class=>"datalink"}, $thissku);
        }
        push @$tbl_ref, \@fieldsum;
        
        my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''});

        if (recordCount($tbl_ref)) { 
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
        } else { 
            print $q->b("no records found<Br>\n"); 
        }

        $dbh->disconnect;
        StandardFooter();
    }      
=pod
sub VALUATIONSTATUS
=cut
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: VALUATIONSTATUS
    # Description:
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "VALUATIONSTATUS") {
        my $status = trim(($q->param("status"))[0]);       
        print $q->h2("Akron Status Valuation: $status");
 
        # Get the code id for this status
        my $codeid = 0;
        $query = "
            select 
                codeid 
            from 
                user_flag_code 
            where 
                description like '$status'
            and
                userflagnum = 3
            ";

        unless (($status eq "unknown") || ($status =~ /None/i)){
            $tbl_ref = execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref) > 0) {
                $codeid = @$tbl_ref[1]->[0];
            }        
        }
     
        
        if ($codeid) {
            $query = dequote(<< "            ENDQUERY");
            select
                p.plunum as "SKU",
                p.pludesc as "Description",
                p.deptnum as "Dept",
                ic.qtycurrent as "QOH",
                ic.lastvendorcost as "lastcost",
                p.cost as "avgcost",
                p.retailprice as "price",
                ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
                p.cost*ic.qtycurrent as "ext_avgcost",
                p.retailprice*ic.qtycurrent as "ext_price"
            from 
                plu p left 
                outer join inventory_current ic on ic.plunum=p.plunum 
            where 
                p.userflagnum3 = $codeid
            order by 
                p.plunum
            ENDQUERY
            
        } else {
            $query = dequote(<< "            ENDQUERY");
            select
                p.plunum as "SKU",
                p.pludesc as "Description",
                p.deptnum as "Dept",
                ic.qtycurrent as "QOH",
                ic.lastvendorcost as "lastcost",
                p.cost as "avgcost",
                p.retailprice as "price",
                ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
                p.cost*ic.qtycurrent as "ext_avgcost",
                p.retailprice*ic.qtycurrent as "ext_price"
            from 
                plu p 
                left outer join inventory_current ic on ic.plunum=p.plunum
                left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3                             
            where 
                uf3.Description is NULL
            order by 
                p.plunum
            ENDQUERY
        }

        $tbl_ref = execQuery_arrayref($dbh, $query);

        my @fieldsum = ('Total:', '', '', 0, '', '', '', 0, 0, 0);

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            for my $field (3, 7, 8, 9)  {
               $fieldsum[$field] += trim($$thisrow[$field]);
            }
            my $thissku = $thisrow->[0];
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_products.pl?report=PRODINFO&SKU=$thissku", -class=>"datalink"}, $thissku);
        }
        push @$tbl_ref, \@fieldsum;
        
        my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''});

        if (recordCount($tbl_ref)) { 
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
        } else { 
            print $q->b("no records found<Br>\n"); 
        }

        $dbh->disconnect;
        StandardFooter();
    }
=pod
sub VALUATIONGROUP
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: VALUATIONGROUP
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "VALUATIONGROUP") {

      my $groupnum = trim(($q->param("groupnum"))[0]);      
      $groupnum = $1 if ($groupnum =~ m/^(\d+)\-.+$/);
      $groupnum = 99 if ($groupnum eq 'MISC SKU');
      $groupnum = -1 if ($groupnum eq 'unknown');

      # get group description name
      my $description = '';
      if ($groupnum  == -1) {
         $description = 'unknown';
      } else {
         $query = "select description from Dept_Group where groupnum =$groupnum";

         $tbl_ref = execQuery_arrayref($dbh, $query);
         if (recordCount($tbl_ref) > 0) {
            $description = @$tbl_ref[1]->[0];
         }
      }

      #print $q->h1("Department Group Valuation Summary<br>for $description");

      print $q->h2("Department Group Valuation");

      if ($groupnum  == -1) {
         $query = dequote(<< "         ENDQUERY");
         select
            'unknown' as "Group",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(p.cost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join Department d on p.deptnum =d.deptnum
         where d.deptnum is null
         ENDQUERY
      } else {
         $query = dequote(<< "         ENDQUERY");
         select
            dg.Description as "Group",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(p.cost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            join Department d on p.deptnum =d.deptnum
            join Dept_Group dg on d.groupnum=dg.groupnum
         where dg.groupnum = $groupnum
         group by dg.Description
         ENDQUERY
      }

      $tbl_ref = execQuery_arrayref($dbh, $query);
      my @format = ('', {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }



      if ($groupnum  == -1) {
         print $q->h2("Department SKU Detail");
         $query = dequote(<< "         ENDQUERY");
         select
            p.plunum as "SKU",
            p.pludesc as "Description",
            p.deptnum as "Dept",
            ic.qtycurrent as "QOH",
            ic.lastvendorcost as "lastcost",
            p.cost as "avgcost",
            p.retailprice as "price",
            ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
            p.cost*ic.qtycurrent as "ext_avgcost",
            p.retailprice*ic.qtycurrent as "ext_price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            left outer join Department d on p.deptnum =d.deptnum
         where d.deptnum is null
         order by p.plunum
         ENDQUERY

         $tbl_ref = execQuery_arrayref($dbh, $query);

         my @fieldsum = ('Total:', '', '', 0, '', '', '', 0, 0, 0);

         for my $datarows (1 .. $#$tbl_ref)
         {
            my $thisrow = @$tbl_ref[$datarows];

            for my $field (3, 7, 8, 9)  {
               $fieldsum[$field] += trim($$thisrow[$field]);
            }

            my $thissku = $thisrow->[0];
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_products.pl?report=PRODINFO&SKU=$thissku", -class=>"datalink"}, $thissku);
         }

         push @$tbl_ref, \@fieldsum;

         #my @links = ( '/cgi-bin/i_products.pl?report=PRODINFO&SKU=$_' );
         my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''});

         if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
         else { print $q->b("no records found<Br>\n"); }

      } else {
         print $q->h2("Department Valuation Detail");

         $query = dequote(<< "         ENDQUERY");
         select
            d.deptdescription as "Department",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(p.cost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "price",
            0 as "%TotPrice"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            join Department d on p.deptnum =d.deptnum
            join Dept_Group dg on d.groupnum=dg.groupnum
         where dg.groupnum = $groupnum
         group by d.deptdescription
         ENDQUERY

         $tbl_ref = execQuery_arrayref($dbh, $query);

         my @fieldsum = ('Total:', 0, 0, 0, 0, 0, 0, 0);

         for my $datarows (1 .. $#$tbl_ref)
         {
            my $thisrow = @$tbl_ref[$datarows];

            for my $field (1, 2, 3, 4, 5, 6)  {
               $fieldsum[$field] += trim($$thisrow[$field]);
            }

         }

         my $sumprice = $fieldsum[6];
         push @$tbl_ref, \@fieldsum;

         for my $datarows (1 .. $#$tbl_ref)
         {
            my $thisrow = @$tbl_ref[$datarows];
            my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[6] / $sumprice * 100);
            $thisrow->[7] = $percent;

            my $thisDept = $thisrow->[0];
            next if ($thisDept eq "Total:");
            $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONDEPT&deptdesc=$thisDept", -class=>"datalink"}, $thisDept);
         }

         #my @links = ( '/cgi-bin/i_reports.pl?report=VALUATIONDEPT&deptdesc=$_' );
         my @format = ('', {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'num' => '.2'});

         if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
         else { print $q->b("no records found<Br>\n"); }

         print $q->p("<b>Note:</b> To get detailed information about a given Departments valuation, ".
                     "click on the underlined Department name above."), "\n";
      }

      $dbh->disconnect;
      StandardFooter();
   }

=pod
sub VALUATIONDEPT
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: VALUATIONDEPT
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "VALUATIONDEPT") {

      my $deptdesc = trim(($q->param("deptdesc"))[0]);      

      # get group description name
      my $deptnum = 0;
      $query = "select deptnum from department where deptdescription = '$deptdesc' ";

      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0) {
         $deptnum = @$tbl_ref[1]->[0];
      }

      #print $q->h1("Department Group Valuation Summary<br>for $description");


      if (1) {
         print $q->h2("Department Valuation Detail");

         $query = dequote(<< "         ENDQUERY");
         select
            d.deptdescription as "Department",
            count(*) as "SKUs",
            sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
            sum(ic.qtycurrent) as "QOH",
            sum(p.cost*ic.qtycurrent) as "LastCost",
            sum(p.cost*ic.qtycurrent) as "AvgCost",
            sum(p.retailprice*ic.qtycurrent) as "Price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            join Department d on p.deptnum =d.deptnum
         where d.deptnum = $deptnum
         group by d.deptdescription
         ENDQUERY

         $tbl_ref = execQuery_arrayref($dbh, $query);
         my @format = ('', {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'num' => '.0'},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''},
                       {'align' => 'Right', 'currency' => ''});

         if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
         else { print $q->b("no records found<Br>\n"); }
      }

      print $q->h2("Department SKU Detail");

      if (1) {
         $query = dequote(<< "         ENDQUERY");
         select
            p.plunum as "SKU",
            p.pludesc as "Description",
            ic.qtycurrent as "QOH",
            ic.lastvendorcost as "lastcost",
            p.cost as "avgcost",
            p.retailprice as "price",
            ic.lastvendorcost*ic.qtycurrent as "ext_lastcost",
            p.cost*ic.qtycurrent as "ext_avgcost",
            p.retailprice*ic.qtycurrent as "ext_price"
         from plu p left outer join inventory_current ic on ic.plunum=p.plunum
            join Department d on p.deptnum =d.deptnum
         where d.deptnum= $deptnum
         order by p.plunum
         ENDQUERY
      }

      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @fieldsum = ('Total:', '', 0, '', '', '', 0, 0, 0);

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];

         for my $field (2, 6, 7, 8)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
         }

         my $thissku = $thisrow->[0];
         $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_products.pl?report=PRODINFO&SKU=$thissku", -class=>"datalink"}, $thissku);
      }

      push @$tbl_ref, \@fieldsum;

      #my @links = ( '/cgi-bin/i_products.pl?report=PRODINFO&SKU=$_' );
      my @format = ('', '', {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }

      $dbh->disconnect;
      StandardFooter();
   }


=pod
sub TAXATION
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: TAXATION
   # Description: This report was created via a request from Naomi at the Champaign
   # store.  There is no report in Store Manager that will allow you to query SKU's
   # by tax code.  She wanted a report that would show her which items had no tax,
   # which had taxcode 2, etc.
   # Like the others, this is a two-tier report, the first page shows summary
   # information: the tax code and the number of skus that have that tax.
   # At this level you can click on the underlined tax code and get detailed
   # information about the skus that have that tax.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "TAXATION") {
      print $q->h1("Taxable Item Summary");      

      # ########################################
      # get list of taxes used
      # ########################################
      my $query = "select taxid, taxdesc, taxRate from tax order by taxid";
      $tbl_ref = execQuery_arrayref($dbh, $query);
      my %taxlist = ();
      for my $datarows (1 .. $#$tbl_ref) {
         my $thisrow = @$tbl_ref[$datarows];
         my $taxid = trim($$thisrow[0]);
         my $taxdesc = trim($$thisrow[1]);
         my $taxrate = trim($$thisrow[2]);
         $taxlist{$taxid} = [$taxdesc, $taxrate];
      }
      # ########################################
      # the old summary report
      # ########################################
      my @notaxlist=('1=1');
      $query = '';
      foreach my $taxid (sort(keys(%taxlist))) {
         my $taxstr = "Tax".$taxid;
         my $taxdesc = $taxlist{$taxid}->[0];
         my $taxrate = ($taxlist{$taxid}->[1])+0;
         $taxrate .= '%';

         $query .= "\nunion " if $query;
         $query .= "select '$taxstr' as \"tax\", '$taxdesc' as \"tax_desc\", '$taxrate' as \"Rate\", ".
                   "count(*) as \"num_SKUs\" from plu where $taxstr ='Y' ";
         push @notaxlist, "$taxstr='N'";
      }

      $query .= "\nunion " if $query;
      $query .= "select 'notax' as \"tax\", 'no tax' as \"tax_desc\", null as \"Rate\", count(*) as \"num_SKUs\" from plu where ".
               join(" and ", @notaxlist) . " ";
      $query .= "order by 1";
      #print "<pre>$query</pre><br>\n";

      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @format = ('', '', {'align' => 'Right'}, {'align' => 'Right', 'num' => '.0'});
      my @links = ( '/cgi-bin/i_reports.pl?report=TAXDETAIL&tax=$_' );

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, \@format); }
      else { print $q->b("no records found<Br>\n"); }

      print $q->p("<b>Note:</b> To get a detailed list of SKUs using a given tax code, click on ".
                  "the underlined Tax field above."), "\n";

      # ########################################
      # Break down by Department Group
      # ########################################
      print $q->h3("Taxable Items by DeptGroup");

      # build query:
      @format = ('', {'align' => 'Right', 'num' => '.0'}, {'align' => 'Right', 'num' => '.0'});
      my $taxfields = '';
      foreach my $taxid (sort(keys(%taxlist))) {
         my $taxstr = "Tax".$taxid;
         $taxfields .= ",(select count(*) from PLU where $taxstr='Y' and (deptnum/100)=tg.DeptGroup) as \"$taxstr\"";
         push @format, {'align' => 'Right', 'num' => '.0'};
      }
      my $notaxwhere = join(" and ", @notaxlist);

      # tried to do with with one query, but the funky 'group by's were troublesome
      # so I decided to utilize a temporary table.
      # tried to pass these to the database as three separate query, but for some reason
      # the temporary table gets lost between the creation and select from queries.
      # putting everything in one big block seems to work - as long as only one
      # result set is returned.
      $query = dequote(<< "      ENDQUERY");
      begin
         select
            min(p.deptnum/100) as DeptGroup,
            count(*) as Items
         into #tmpTaxGroups
         from PLU p
         group by (p.deptnum/100)
         order by DeptGroup;

         select
            tg.DeptGroup, dg.description, tg.Items,
            (select count(*) from PLU where $notaxwhere and (deptnum/100)=tg.DeptGroup) as "notax"
            $taxfields
         from #tmpTaxGroups tg
         left outer join Dept_Group dg on tg.DeptGroup=dg.groupNum
         order by DeptGroup;

         drop table #tmpTaxGroups;
      end
      ENDQUERY
      #print "<pre>$query</pre><br>\n";

      $tbl_ref = execQuery_arrayref($dbh, $query);

      my $tbl_ref2 = [];
      my $ref = ["DeptGroup", "Items", "NoTax"];
      foreach my $taxid (sort(keys(%taxlist))) { push @$ref, "Tax".$taxid; }
      push @$tbl_ref2, $ref;

      my $totalrec = ['Total:', 0, 0];
      foreach my $taxid (sort(keys(%taxlist))) { push @$totalrec, 0; }

      for my $datarows (1 .. $#$tbl_ref) {
         my $thisrow = @$tbl_ref[$datarows];
         my @row = trim(@$thisrow);
         my $DeptGroupId = $row[0];
         my $DeptGroupDesc = $row[1];
         my $DeptGroup = substr('00'.$DeptGroupId,-2);
         $DeptGroup .= '-'. trim(substr($DeptGroupDesc, index($DeptGroupDesc,'-')+1)) if ($DeptGroupDesc);
         my $CountLink = $row[2];
         $CountLink = $q->a({-href=>"/cgi-bin/i_reports.pl?report=TAXDETAIL&DeptGroup=$DeptGroupId", -class=>"datalink"}, $row[2]) if ($row[2] != 0);
         my $noTaxLink = $row[3];
         $noTaxLink =  $q->a({-href=>"/cgi-bin/i_reports.pl?report=TAXDETAIL&DeptGroup=$DeptGroupId&tax=notax", -class=>"datalink"}, $row[3]) if ($row[3] != 0);

         my $ref = [$DeptGroup, $CountLink, $noTaxLink];

         foreach my $taxid (sort(keys(%taxlist))) {
            my $TaxLink = $row[3+$taxid];
            $TaxLink = $q->a({-href=>"/cgi-bin/i_reports.pl?report=TAXDETAIL&DeptGroup=$DeptGroupId&tax=Tax$taxid", -class=>"datalink"}, $row[3+$taxid]) if ($row[3+$taxid] != 0);
            push @$ref, $TaxLink;
         }
         push @$tbl_ref2, $ref;
         for my $i (1.. (2 + scalar(keys(%taxlist))) ) {
            $totalrec->[$i] +=  $row[$i+1];
         }
      }

      push @$tbl_ref2, $totalrec;

      if (recordCount($tbl_ref2)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref2, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }

      print $q->p("<b>Note:</b> To get a detailed list of SKUs using a given tax code for a given DeptGroup, ".
                  "click on the underlined numbers above."), "\n";


      $dbh->disconnect;
      StandardFooter();
   }

=pod
sub TAXDETAIL
=cut
   elsif ($report eq "TAXDETAIL") {
      print $q->h1("Taxable Item Detail");      
      my $tax = trim(($q->param("tax"))[0]);
      my $DeptGroup = trim(($q->param("DeptGroup"))[0]);

      my $sortfield = trim(($q->param("sort"))[0]);
      $sortfield = "SKU" unless $sortfield;

      $query = "select taxid, taxdesc from tax order by taxid";
      my $tbl_ref = execQuery_arrayref($dbh, $query);
      my @taxlist;
      for my $datarows (1 .. $#$tbl_ref) {
        my $taxid = @$tbl_ref[$datarows]->[0];
        push @taxlist, "tax".$taxid;
      }

      my $whereclause = '';
      if ($tax) {
         $whereclause .= " and " if $whereclause;
         if ($tax eq 'notax') {
            $whereclause .= join("='N' and ", @taxlist)."='N'";
         } else {
            $whereclause .= "$tax = 'Y'";
         }
      }

      if ($DeptGroup ne '') {
         $whereclause .= " and " if $whereclause;
         $whereclause .= "(deptnum/100) = $DeptGroup";
      }

      $whereclause = "where ". $whereclause if ($whereclause);
      my $selecttaxlist = join(", ", @taxlist);

      $query = dequote(<< "      ENDQUERY");
         select
            plunum as "SKU", pludesc as "description", deptnum as "dept",
            $selecttaxlist
         from plu
         $whereclause
         order by $sortfield
      ENDQUERY

      #print $q->code($query),"<br>\n";
      $tbl_ref = execQuery_arrayref($dbh, $query);
      my @links = ( '/cgi-bin/i_products.pl?report=PRODINFO&SKU=$_' );
      my $newsortURL = "/cgi-bin/i_reports.pl?report=TAXDETAIL&tax=$tax&DeptGroup=$DeptGroup&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL);
      for my $i (1.. scalar(@taxlist)){ push @headerlinks, $newsortURL; }

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '', \@headerlinks); }
      else { print $q->b("no records found<Br>\n"); }

      $dbh->disconnect;
      StandardFooter();
   }

=pod
sub STORECREDIT
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: STORECREDIT
   # Description: John Hood, the CPA for the Kansas City store, pointed out that
   # there were no reports in Store Manager that lists Store Credits that have
   # been issued or redeemed.
   #
   # "What that creates is a liability to the store that can be redeemed at
   # any time.  Therefore a liability should be shown on the books to reflect
   # that. ... A report should be available indicating the total amount of
   # credits outstanding."
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "STORECREDIT") {
      print $q->h1("Merchandise Credit Report");      
      my $sortfieldname = trim(($q->param("sort"))[0]);
      $sortfieldname = "refnum" unless $sortfieldname;

      # type      token value
      # --------- ----- -------------------
      # TenderId      9 Merchandise Credit
      # TenderId     12 Merch Credit Redeem

      # ###############################################
      # # find voided transactions in the date range.
      # ###############################################
      $query = dequote(<< "      ENDQUERY");
         select
            mt.storenum, pt.transnum
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);

      my %postvoided;
      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];
         my $StoreNum  = trim($$thisrow[0]);
         my $transnum  = trim($$thisrow[1]);

         my $key = "$StoreNum-$transnum";
         $postvoided{$key} = 1;
      }


      # ###############################################
      # # Get a list of Merchandis Credits
      # ###############################################

      # create a hash to store merchandise credits
      # refnum => ["date", "cashier", "amount"]
      my %mc_hash;

      # get merchandise Credit information
      $query = dequote(<< "      ENDQUERY");
         select
            refnum,
            convert(varchar(19), itemdatetime, 21) as "Date",
            storenum,
            transnum,
            salesprsnid,
            tenderamt
         from
            Txn_Method_of_Payment
         where
            tenderid = 9 and
            txnvoidmod = 0
         order by itemdatetime
      ENDQUERY

      #print "<pre>$query</pre><br>\n";
      $tbl_ref = execQuery_arrayref($dbh, $query);

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];

         my $refnum   = trim($$thisrow[0]);
         my $date     = trim($$thisrow[1]);
         my $StoreNum = trim($$thisrow[2]);
         my $transnum = trim($$thisrow[3]);
         my $cashier  = trim($$thisrow[4]);
         my $amount   = trim($$thisrow[5]);

         next if (exists($postvoided{"$StoreNum-$transnum"}));

         # refnum => ["date", "cashier", "amount"]
         $mc_hash{$refnum} = [$date, $cashier, $amount];
      }

      # ###############################################
      # # Get a list of Merchandis Redeems
      # ###############################################

      # create a hash to store merchandise redeems
      # refnum => ["date", "cashier", "amount"]
      my %mr_hash;

      # get merchandise Redeem information
      $query = dequote(<< "      ENDQUERY");
         select
            refnum,
            convert(varchar(19), itemdatetime, 21) as "Date",
            storenum,
            transnum,
            salesprsnid,
            tenderamt
         from
            Txn_Method_of_Payment
         where
            tenderid = 12 and
            txnvoidmod = 0
         order by itemdatetime
      ENDQUERY

      #print "<pre>$query</pre><br>\n";
      $tbl_ref = execQuery_arrayref($dbh, $query);

      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];

         my $refnum   = trim($$thisrow[0]);
         my $date     = trim($$thisrow[1]);
         my $StoreNum = trim($$thisrow[2]);
         my $transnum = trim($$thisrow[3]);
         my $cashier  = trim($$thisrow[4]);
         my $amount   = trim($$thisrow[5]);

         next if (exists($postvoided{"$StoreNum-$transnum"}));

         my $key = $refnum;
         if (exists($mr_hash{$refnum})) {
            # this refnum was redeemed previously.
            my $counter = 2;
            while (exists($mr_hash{$refnum."|".$counter})) {$counter++}
            $key = $refnum."|".$counter;
         }

         # refnum => ["date", "cashier", "amount"]
         $mr_hash{$key} = [$date, $cashier, $amount];
      }
      #foreach my $refnum (sort(keys(%mr_hash))) {
      #   my $value = $mr_hash{$refnum};
      #   print "$refnum => ", join (', ', @$value) ."<br>\n";
      #}

      # ###############################################
      # # Put them together
      # ###############################################

      my $tbl_ref2 = [];
      # loop through Issued credits and
      foreach my $refnum (sort(keys(%mc_hash))) {

         my $IssueRef = $mc_hash{$refnum};
         my ($IssueDate, $IssueCashier, $IssueAmt) = @$IssueRef;

         my ($RedeemDate, $RedeemCashier, $RedeemAmt);
         my $AmtSum = $IssueAmt;

         if (exists($mr_hash{$refnum})) {
            my $RedeemRef = $mr_hash{$refnum};
            ($RedeemDate, $RedeemCashier, $RedeemAmt) = @$RedeemRef;

            $AmtSum += $RedeemAmt;

            delete $mr_hash{$refnum};
         }

         my $ref = [$refnum, $IssueDate, $IssueCashier, $IssueAmt,
                    $RedeemDate, $RedeemCashier, $RedeemAmt, $AmtSum];
         push @$tbl_ref2, $ref;

      }

      # loop through left over redeemed credits
      foreach my $refnum (sort(keys(%mr_hash))) {
         my $RedeemRef = $mr_hash{$refnum};
         my ($RedeemDate, $RedeemCashier, $RedeemAmt) = @$RedeemRef;

         my $AmtSum = $RedeemAmt;

         $refnum = substr($refnum,0,index($refnum,'|')) if ( index($refnum,'|') >= 0);

         my $ref = [$refnum, '', '', '',
                    $RedeemDate, $RedeemCashier, $RedeemAmt, $AmtSum];
         push @$tbl_ref2, $ref;
      }

      # sort & print list.
      my %sortfields = ("Refnum"=>1, "IssueDate"=>2, "ICashier"=>3, "IAmount"=>-4,
                        "RedeemDate"=>5, "RCashier"=>6, "RAmount"=>-7, "AmtSum"=>-8);

      my $sortfield = 1;
      $sortfield = $sortfields{$sortfieldname} if exists($sortfields{$sortfieldname});

      my @tbl_ref3 = sort {if ($sortfield < 0) {$a->[abs($sortfield)-1] <=> $b->[abs($sortfield)-1] }
                           else {$a->[$sortfield-1] cmp $b->[$sortfield-1] }
                          }
                     @$tbl_ref2;


      my $tbl_ref4 = [];
      my $ref = ["Refnum", "IssueDate", "ICashier", "IAmount",
                 "RedeemDate", "RCashier", "RAmount", "AmtSum"];
      push @$tbl_ref4, $ref;
      my ($SumIAmount, $SumRAmount, $SumAmtSum) = (0, 0, 0);
      foreach my $rec_ref (@tbl_ref3) {
         push @$tbl_ref4, $rec_ref;
         $SumIAmount += $rec_ref->[3];
         $SumRAmount += $rec_ref->[6];
         $SumAmtSum  += $rec_ref->[7];
      }

      $ref = ["Total", "", "", $SumIAmount,"", "", $SumRAmount, $SumAmtSum];
      push @$tbl_ref4, $ref;

      my @format = ('', '', '', {'align' => 'Right', 'currency'=>''},
                    '', '', {'align' => 'Right', 'currency'=>''}, {'align' => 'Right', 'currency'=>''});

      my $newsortURL = "/cgi-bin/i_reports.pl?report=STORECREDIT&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);

      if (recordCount($tbl_ref4)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref4, '', \@format, \@headerlinks); }
      else { print $q->b("no records found<Br>\n"); }

      $dbh->disconnect;
      StandardFooter();
   }

=pod
sub GIFTCERTIFICATES
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: GIFTCERTIFICATES
   # Description: Similar to STORECREDIT report, but dealing with Gift Certificate
   # issues and redeems.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "GIFTCERTIFICATES") {
      print $q->h1("Gift Certificate Issue/Redeem Reports");

      # fields: [display name, script, report, report note]
      my @reportlist = (
         ["Gift Certificates Issued", "/cgi-bin/i_reports.pl", "GIFTISSUE1",
          "Prints a list gift certificates issued in a given time range."
         ],
         ["Gift Certificates Redeemed", "/cgi-bin/i_reports.pl", "GIFTREDEEM1",
          "Prints a list gift certificates redeemed in a given time range."
         ]);
       foreach my $reportref (@reportlist) {
            my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
            print $q->p(
               $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
               $reportnote );
         }
       StandardFooter();
}
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: GIFTISSUE1
   # Description: Similar to STORECREDIT report, but dealing with Gift Certificate
   # issues and redeems.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
      elsif ($report eq "GIFTISSUE1") {
      print $q->h1("Gift Certificates Issued Report");
      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"GIFTISSUE2"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Data Compilation:")));
      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      
      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), dateadd(month, -1, getdate()), 21)+ '01' as "FirstDate",
         convert(char(10), dateadd(day, -1, convert(char(8), getdate(), 21)+ '01'), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );


      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      $dbh->disconnect;

      StandardFooter();
      }
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: GIFTISSUE2
   # Description: Similar to STORECREDIT report, but dealing with Gift Certificate
   # issues and redeems.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
      elsif ($report eq "GIFTISSUE2") {      
      my $sortfieldname = trim(($q->param("sort"))[0]);
      my $startdate= trim(($q->param("startdate"))[0]);
      my $enddate = trim(($q->param("enddate"))[0]);
      my $extendedend="$enddate"." 23:59:59";
      $sortfieldname = "refnum" unless $sortfieldname;
      print $q->h1("Gift Certificates Issued Report:\n  $startdate to $enddate");
      print $q->p("<b>Note:</b> The Gift Certificates Issued Report only shows gift certificates that were issued in your store
                                        using the Triversity POS system during the given time period. The reference number listed is the 10-digit number
                                        that was imprinted on the back of the gift certificate at the point of sale.");

      # ###############################################
      # # Get a list of Gift Certificate Issues, excluding postvoids
      # ###############################################



      $query = dequote(<< "      ENDQUERY");
         begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            tm.refnum,
            convert(varchar(19), tm.itemdatetime, 21),

            tm.transnum,
            tm.salesprsnid,
            tm.extsellprice
         from
            Txn_Non_Merch_Sale tm left outer join #tmpPostVoids v on tm.storenum=v.storenum and tm.transnum=v.transnum
         where
            tm.nonmerchid = 1 and
            tm.txnvoidmod = 0 and
            v.transnum is null and
            tm.itemdatetime>='$startdate' and
            tm.itemdatetime<='$extendedend'
         order by tm."$sortfieldname";
         drop table #tmpPostVoids;
      end
      ENDQUERY

      #print "<pre>$query</pre><br>\n";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    my @format = ('','','','',
                    {'align' => 'Right', 'currency' => ''},
                   );
    my $newsortURL = "/cgi-bin/i_reports.pl?report=GIFTISSUE2&startdate=$startdate&enddate=$enddate&sort=\$_";
    my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
    printResult_arrayref2($tbl_ref,'', \@format,\@headerlinks);

    $dbh->disconnect;
    StandardFooter();
}
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: GIFTREDEEM1
   # Description: Similar to STORECREDIT report, but dealing with Gift Certificate
   # issues and redeems.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
      elsif ($report eq "GIFTREDEEM1") {
      print $q->h1("Gift Certificates Redeemed Report");
      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"GIFTREDEEM2"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Data Compilation:")));
      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      
      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), dateadd(month, -1, getdate()), 21)+ '01' as "FirstDate",
         convert(char(10), dateadd(day, -1, convert(char(8), getdate(), 21)+ '01'), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );


      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      $dbh->disconnect;

      StandardFooter();
      }
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: GIFTREDEEM2
   # Description: Similar to STORECREDIT report, but dealing with Gift Certificate
   # issues and redeems.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
     elsif ($report eq "GIFTREDEEM2") {      
      my $sortfieldname = trim(($q->param("sort"))[0]);
      my $startdate= trim(($q->param("startdate"))[0]);
      my $enddate = trim(($q->param("enddate"))[0]);
      my $extendedend="$enddate"." 23:59:59";
      $sortfieldname = "refnum" unless $sortfieldname;

      print $q->h1("Gift Certificates Redeemed Report:\n  $startdate to $enddate");
      print $q->p("<b>Note:</b>The Gift Certificates Redeemed Report shows all Gift Certificates that were redeemed
                                                              in your store during the selected time period.  If your store redeems a Gift Certificate that was issued at another store,
                                                              it will appear on this report.  Since this report shows only activity in your store, it does not list as redeemed
                                                              any Gift Certificates you issued that were redeemed elsewhere.  These two facts should be taken into account
                                                              when reconciling your liability for outstanding Gift Certificates.  It is important to note that the reference number listed
                                                              on the report is the number that the redeeming cashier entered at the point of sale.  Due to human error,
                                                              this number may differ from the actual number printed on the Gift Certificate.");

      # get Gift Cert Redeem information
      $query = dequote(<< "      ENDQUERY");
        begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;
          select
            tm.refnum,
            convert(varchar(19), tm.itemdatetime, 21),

            tm.transnum,
            tm.salesprsnid,
            tm.tenderamt
         from
            Txn_Method_of_Payment tm left outer join #tmpPostVoids v on tm.storenum=v.storenum and tm.transnum=v.transnum
         where
            tm.tenderid = 8 and
            tm.txnvoidmod = 0 and
            v.transnum is null and
            tm.itemdatetime>='$startdate' and
            tm.itemdatetime<='$extendedend'
        order by tm.$sortfieldname;
        drop table #tmpPostVoids;
        end;
      ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    my @format = ('','','','',
                    {'align' => 'Right', 'currency' => ''},
                   );
    my $newsortURL="/cgi-bin/i_reports.pl?report=GIFTREDEEM2&startdate=$startdate&enddate=$enddate&sort=\$_";
    my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);

    printResult_arrayref2($tbl_ref,'', \@format,\@headerlinks);

    $dbh->disconnect;
    StandardFooter();
    }

=pod
sub ZIPCODE
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: ZIPCODE
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "ZIPCODE") {
      print $q->h1("ZipCode Report");

      print $q->p("This report prints a summary of the zip codes entered during POS transactions.<br>".
                  "Fill in the start and end date range (in format yyyy-mm-dd) of the data to look at ".
                  "or enter a blank date range to summarize all zip code entries.");

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"ZIPCODE2"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));
      
      $query = dequote(<< "      ENDQUERY");
         select convert(varchar(10),min(itemdatetime),21) as "datetime"
         from Txn_Profile_Prompt_Response
         where propromptid=8
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      my $startdate = @$tbl_ref[1]->[0];

      my @ltm = localtime(time());
      my $enddate = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
      my $today = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          # <SCRIPT LANGUAGE="JavaScript">var cal2 = new CalendarPopup("testdiv1");</SCRIPT>
                          $q->script({-language=>"JavaScript"},
                             "var cal$calcounter = new CalendarPopup(\"testdiv1\");"
                             # cal1.addDisabledDates(null, "2003-04-30");
                             # cal1.addDisabledDates("2003-06-25", null);
                          ),

                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          #<A HREF="#" onClick="cal2.select(document.forms[0].startdate,'anchor2','yyyy-MM-dd'); return false;" NAME="anchor2" ID="anchor2"><img align=top border=0 height=21 src="calendar.gif" width=34></A>

                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';


      $dbh->disconnect;

      StandardFooter();
   }
=pod
sub ZIPCODE2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: ZIPCODE2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "ZIPCODE2") {
      #print '<SCRIPT LANGUAGE="JavaScript" SRC="/sorttable.js"></SCRIPT>';
	  print $q->h1("List of ZipCodes");

      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);
      my $sortfield = trim(($q->param("sort"))[0]);
      my $desc = trim(($q->param("desc"))[0]);


      $sortfield = "Zipcode" unless $sortfield;

      # select convert(varchar(19),itemdatetime,21)as "datetime" ,transnum, profileresponse1 as "zipcode", txnvoidmod
      # from Txn_Profile_Prompt_Response
      # where propromptid=8  and profileresponse1 <> '99999'
      # order by itemdatetime
      #
      # select convert(varchar(7),itemdatetime,21) as "datetime" , profileresponse1 as "zipcode", count(*) as "count" from Txn_Profile_Prompt_Response where propromptid=8   group by "datetime", "zipcode" order by 1,2

      my $datewhere = "";
      my $daterange = "";
      if ( ($startdate) && ($enddate) ) {
         if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
         }

         $datewhere = " and tpr.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
         $daterange = " in date range $startdate - $enddate";
      }      

      $query = dequote(<< "      ENDQUERY");
         select count(*) as "Count", sum(tt.totalwotax) as "Amount"
         from Txn_Profile_Prompt_Response tpr
         left outer join Txn_Transaction_Total tt on tpr.storenum=tt.storenum and tpr.transnum=tt.transnum
         where tpr.propromptid=8 $datewhere
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      my $count = @$tbl_ref[1]->[0];
      my $amount = @$tbl_ref[1]->[1];

      if ($count == 0 ) {
         print $q->b("no records found<Br>\n");
         last SWITCH;
      }


      my %altsort = ("Zipcode"=>"1", "Count"=>"2", "%TotalCnt"=>"3", "Amount"=>"4", "%TotalAmt"=>"5", "AvgAmt"=>"6");
      $sortfield = $altsort{$sortfield} if exists($altsort{$sortfield});

      $query = dequote(<< "      ENDQUERY");
         select tpr.profileresponse1 as "Zipcode",
            count(*) as "Count",
            (count(*)*100.0 /$count) as "%TotalCnt",
            sum(tt.totalwotax) as "Amount",
            (sum(tt.totalwotax)*100.0/$amount) as "%TotalAmt",
            avg(tt.totalwotax) as "AvgAmt"
         from Txn_Profile_Prompt_Response tpr
         left outer join Txn_Transaction_Total tt on tpr.storenum=tt.storenum and tpr.transnum=tt.transnum
         where tpr.propromptid=8 and tpr.txnvoidmod=0 $datewhere
         group by tpr.profileresponse1
         order by $sortfield $desc
      ENDQUERY
      #   select profileresponse1 as "zipcode", count(*) as "count", (count(*)*100.0 /$count) as "percent"
      #   from Txn_Profile_Prompt_Response
      #   where propromptid=8 $datewhere
      #   group by profileresponse1 order by "count" desc, profileresponse1

      #print "<pre>$query</pre><br>\n";

      $tbl_ref = execQuery_arrayref($dbh, $query);
      my @format = ('',
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'num' => '.2'},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'num' => '.2'},
                    {'align' => 'Right', 'currency' => ''},
                   );

      my $newsortURL = "/cgi-bin/i_reports.pl?report=ZIPCODE2&startdate=$startdate&enddate=$enddate&sort=\$_&desc=";
      my $newsortURLorder = ($desc =~ m/DESC/) ? $newsortURL : $newsortURL." DESC";
      #my $newsortURL = "/cgi-bin/i_reports.pl?report=ZIPCODE2&startdate=$startdate&enddate=$enddate&sort=\$_";
      my @headerlinks = ($newsortURLorder, $newsortURLorder, $newsortURLorder, $newsortURLorder, $newsortURLorder, $newsortURLorder);
      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); }
      #if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref,'', \@format,'',{'tableid' => 'tableid1'}); }
      else { print $q->b("no records found<Br>\n"); }

      $dbh->disconnect;

      StandardFooter();
   }

   

=pod
sub CSE
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: CSE
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "CSE") {
      print $q->h1("CSE Report");

      print $q->p("This report prints a summary of the CSE codes entered during POS transactions."); 

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"CSE2"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));
 
      my @ltm = localtime(time());
	  my $startdate = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
      my $enddate = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
      my $today = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},                      
                          $q->script({-language=>"JavaScript"},
                             "var cal$calcounter = new CalendarPopup(\"testdiv1\");" 
                          ),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}), 
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';


      $dbh->disconnect;

      StandardFooter();
   }
=pod
sub CSE2
=cut
	# *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
	# Report: CSE2
	# Description:
	# *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
	elsif ($report eq "CSE2") {
      
		print $q->h1("List of Community Shopping Events");

		my $startdate =trim(($q->param("startdate"))[0]);
		my $enddate   =trim(($q->param("enddate"))[0]);
		my $sortfield = trim(($q->param("sort"))[0]);
		my $desc = trim(($q->param("desc"))[0]);


		$sortfield = "CSE" unless $sortfield;
 
		my $datewhere = "";
		my $daterange = "";
		if ( ($startdate) && ($enddate) ) {
			if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
				print "Startdate or Enddate format not currect";
				last SWITCH;
			}

			$datewhere = " and tpr.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
			$daterange = " in date range $startdate - $enddate";
		}      
 
		$query = dequote(<< "		ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                mt.refnum   = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
            where
                mt.misctype = 2;
            
			select 
				count(*) as "Count", 
				sum(tt.totalwotax) as "Amount"
			from 
				Txn_Profile_Prompt_Response tpr
				left outer join Txn_Transaction_Total tt on tpr.storenum=tt.storenum and tpr.transnum=tt.transnum
                left outer join #tmpPostVoids v on tpr.storenum=v.storenum and tpr.transnum=v.transnum
         
			where 
                v.transnum is null
                and
				tpr.propromptid=30 
				and
				tpr.txnvoidmod = 0
				$datewhere;
                
            drop table #tmpPostVoids;                
            end
		ENDQUERY
 		
		#print $q->pre("$query");
		$tbl_ref = execQuery_arrayref($dbh, $query);
		my $count = @$tbl_ref[1]->[0];
		my $amount = @$tbl_ref[1]->[1];

		if ($count == 0 ) {
			print $q->b("no records found<Br>\n");
			last SWITCH;
		} else {
			ElderLog($logfile, "Count of transactions with CSE: $count");		
		}

		# Determine if the information is encrypted
		my $encrypt_count=0;
		$query = dequote(<< "		ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                mt.refnum   = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
            where
                mt.misctype = 2;
            
			select 
				count(*) as "Count"				 
			from 
				Txn_Profile_Prompt_Response tpr
				left outer join Txn_Transaction_Total tt on tpr.storenum=tt.storenum and tpr.transnum=tt.transnum
                left outer join #tmpPostVoids v on tpr.storenum=v.storenum and tpr.transnum=v.transnum
         
			where 
                v.transnum is null
                and
				tpr.propromptid=30 
				and
				tpr.txnvoidmod = 0
				and
				tpr.respencrypted1 like 'Y'
				$datewhere;
                
            drop table #tmpPostVoids;                
            end
		ENDQUERY
 		
		#print $q->pre("$query");
		$tbl_ref = execQuery_arrayref($dbh, $query);
		my $encrypt_count = @$tbl_ref[1]->[0];				
		#
		
		my %altsort = ("CSE"=>"1", "Count"=>"2", "%TotalCnt"=>"3", "Amount"=>"4", "%TotalAmt"=>"5", "AvgAmt"=>"6");
		$sortfield = $altsort{$sortfield} if exists($altsort{$sortfield});

		$query = dequote(<< "		ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                mt.refnum   = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
            where
                mt.misctype = 2;
                
			select 
				tpr.profileresponse1 as "CSE",
				count(*) as "Count",
				(count(*)*100.0 /$count) as "%TotalCnt",
				sum(tt.totalwotax) as "Amount",
				(sum(tt.totalwotax)*100.0/$amount) as "%TotalAmt",
				avg(tt.totalwotax) as "AvgAmt"
			from 
				Txn_Profile_Prompt_Response tpr
				left outer join Txn_Transaction_Total tt on tpr.storenum=tt.storenum and tpr.transnum=tt.transnum
                left outer join #tmpPostVoids v on tpr.storenum=v.storenum and tpr.transnum=v.transnum
			where 
                v.transnum is null
                and            
				tpr.propromptid=30 and tpr.txnvoidmod=0 $datewhere
			group by 
				tpr.profileresponse1
			order by 
				$sortfield $desc
        end
		ENDQUERY
 
		#print "<pre>$query</pre><br>\n";

		$tbl_ref = execQuery_arrayref($dbh, $query);
		my @format = ('',
					{'align' => 'Right', 'num' => '.0'},
					{'align' => 'Right', 'num' => '.2'},
					{'align' => 'Right', 'currency' => ''},
					{'align' => 'Right', 'num' => '.2'},
					{'align' => 'Right', 'currency' => ''},
				   );

		my $newsortURL = "/cgi-bin/i_reports.pl?report=CSE2&startdate=$startdate&enddate=$enddate&sort=\$_&desc=";
		my $newsortURLorder = ($desc =~ m/DESC/) ? $newsortURL : $newsortURL." DESC";
		 
		my @headerlinks = ($newsortURLorder, $newsortURLorder, $newsortURLorder, $newsortURLorder, $newsortURLorder, $newsortURLorder);
		if (recordCount($tbl_ref)) { 
			TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); 
			if ($encrypt_count) {
				print $q->p();
				print $q->p("Note: CSE data may show as encrypted on the day it is collected.  It should show as decrypted the following day.");
			}
		} else { 
			print $q->b("no records found<Br>\n"); 
		}

	}   
=pod
sub RECEIVING
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: RECEIVING
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "RECEIVING") {
      print $q->h1("Receiving Report");
      print $q->br();

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"RECEIVING2"});

      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));      

      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), getdate(), 21)+ '01' as "FirstDate",
         convert(char(10), getdate(), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"},
                              "var cal$calcounter = new CalendarPopup(\"testdiv1\");\n".
                              "cal$calcounter.offsetX = 26;\n".    # below/left: (-152, 25) above/right: (2, -105)
                              "cal$calcounter.offsetY = -115;"      # right/below: (26,2) right/above (26, -82)
                           ),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"},
                              "var cal$calcounter = new CalendarPopup(\"testdiv1\");\n".
                              "cal$calcounter.offsetX = 26;\n".    # below/left: (-152, 25) above/right: (2, -105)
                              "cal$calcounter.offsetY = -130;"      # right/below: (26,2) right/above (26, -82)
                           ),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      # Print Vendor Selection list
      $query = "select VendorId, VendorName from Vendor";
      $tbl_ref = execQuery_arrayref($dbh, $query);
      my $vendor_options = $q->option({-value=>'' -selected=>undef});
      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];
         my $VendorId = trim($$thisrow[0]);
         my $VendorName = trim($$thisrow[1]);
         $vendor_options .= $q->option({-value=>$VendorId}, "$VendorId - $VendorName");
      }
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Ven<u>d</u>or:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-accesskey=>'D', -name=>"VendorId", -size=>"1"}), $vendor_options)
                  );

=pod
      # Print Dept/Class selection list
      my $class_options = $q->option({-value=>'' -selected=>undef});
      foreach my $key (sort (keys(%class_hash))) {
         #next unless (substr($key,4,2) eq "00");
         my $value = substr($class_hash{$key},0,15);
         my $dkey;
         if (substr($key,2,4) eq "0000") { $dkey = substr($key,0,2). ("&nbsp;" x 10); }
         elsif (substr($key,4,2) eq "00") { $dkey = substr($key,0,4).("&nbsp;" x 5); }
         else { $dkey = $key }
         $class_options .= $q->option({-value=>$key }, "$dkey: $value");
      }
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "C<u>l</u>ass:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-accesskey=>'L', -name=>"class", -size=>"1"}), $class_options)
                  );
=cut
    # New Departments
      my $dept_options = $q->option({-value=>'' -selected=>undef});
      $query = "select deptnum, deptdescription from department order by deptnum";
      $tbl_ref = execQuery_arrayref($dbh, $query);     
      for my $datarows (1 .. $#$tbl_ref)
      {
         my $thisrow = @$tbl_ref[$datarows];
         my $deptnum   = trim($$thisrow[0]);
         my $deptdesc = trim($$thisrow[1]);

         substr($deptdesc,4,0) = " &nbsp;:&nbsp;" unless ($deptnum == 9999);
         if (substr($deptdesc,2,2) eq "00") { substr($deptdesc,2,2) = ("&nbsp;" x 5); }
         
         $dept_options .= $q->option({-value=>$deptnum }, "$deptdesc");
      }
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Department:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"dept", -size=>"1"}), $dept_options)
                  );    
    #

    # Option to show summary only
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-accesskey=>'D', -type=>"checkbox", -name=>"cb_summary"}) .
                   $q->font({-size=>-1}, "Summary Only")
                  ));    

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      $dbh->disconnect;

      StandardFooter();
   }

=pod
sub RECEIVING2
=cut
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: RECEIVING2
    # Description:
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "RECEIVING2") {
       

        my $startdate =trim(($q->param("startdate"))[0]);
        my $enddate   =trim(($q->param("enddate"))[0]);
        my $VendorId =trim(($q->param("VendorId"))[0]);
        my $dept  = trim(($q->param("dept"))[0]);  
        my $sortfield = trim(($q->param("sort"))[0]);
        my $cb_summary = trim(($q->param("cb_summary"))[0]);
        if ($cb_summary) {
            print $q->h1("Receiving Report: Summary");
        } else {
           print $q->h1("Receiving Report");
        }
        $sortfield = "RecvDate" unless $sortfield;
 
        my $whereclause = "";
        my $daterange = "";
        if ( ($startdate) && ($enddate) ) {
            if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
                print "Startdate or Enddate format not currect";
                last SWITCH;
            }

            $whereclause = " and r.receiveDate between '$startdate' and '$enddate 23:59:59' ";
            $daterange = " in date range $startdate - $enddate";
        }      

        if ($VendorId) {
            $whereclause .= " and " if $whereclause;
            $whereclause .= "r.vendorNum = '$VendorId'";
        }

        if ($dept) {
            $dept = substr($dept,0,2) if (substr($dept,2,2) eq "00");  # are last 2 digits 0?

            $whereclause .= " and " if $whereclause;
            if (length($dept) == 4) {
                $whereclause .= "p.deptnum = $dept";
            } else {
                $whereclause .= "floor(p.deptnum/100) = $dept ";
            }
        }

        unless ($cb_summary) {
            
            $query = dequote(<< "            ENDQUERY");
              select
                 convert(varchar(19),r.receiveDate,21) as "RecvDate",
                 r.documentNum as "DocNum",
                 r.vendorNum as "VendorId",
                 rd.plunum as "SKU",
                 p.pludesc as "Description",
                 p.deptnum as "Dept",
                 rd.qtyacc as "Qty",
                 (rd.qtyacc*rd.cost) as "ExtCost",
                 p.invcontrolled as "InvCtrl"
              from
                 receiving r
                 join receiving_dtl rd on r.rectransnum= rd.rectransnum
                 left outer join plu p on rd.plunum = p.plunum
              where
                 r.txnStatus = 'R'
                 $whereclause
              order by $sortfield
            ENDQUERY
            
            $tbl_ref = execQuery_arrayref($dbh, $query);

            # count and add summary record
            my ($sumqty, $sumextcost) = (0,0);
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
                $sumqty += trim($$thisrow[6]);
                $sumextcost += trim($$thisrow[7]);
            }
            my $ref = ['Total:', '', '', '', '', '', $sumqty, $sumextcost,''];
            push @$tbl_ref, $ref;


            my @format = ('', '', '', '', '', '',
                        {'align' => 'Right', 'num' => '.0'},
                        {'align' => 'Right', 'currency' => ''});
            my $newsortURL = "/cgi-bin/i_reports.pl?report=RECEIVING2&startdate=$startdate&enddate=$enddate&VendorId=$VendorId&dept=$dept&sort=\$_";
            my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
            if (recordCount($tbl_ref)) { 
                printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); 
            } else { 
                print $q->b("no records found<br>\n"); 
            }
        } else {
            # Get the data for the items under inventory control
            my %document_hash=();
            $query = dequote(<< "            ENDQUERY");
              select
 
                 r.documentNum as "DocNum",
                 sum(rd.qtyacc) as "UnitsRcvd",
                 sum(rd.qtyacc*rd.cost) as "CostRcvd"               
              from
                 receiving r
                 join receiving_dtl rd on r.rectransnum= rd.rectransnum
                 left outer join plu p on rd.plunum = p.plunum
                 join vendor v on v.vendorid = r.vendorNum
              where
                 r.txnStatus = 'R'
                 $whereclause
                and
                    p.invcontrolled = 'Y'
              group by DocNum 
            
            ENDQUERY
            
            $tbl_ref = execQuery_arrayref($dbh, $query);  
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
                my $docnum = $$thisrow[0];
                my $units = $$thisrow[1];
                my $cost = $$thisrow[2];                
                my @array = ("$units","$cost"); 
                $document_hash{$docnum} = \@array;
            }            
            
            $query = dequote(<< "            ENDQUERY");
              select
                 convert(varchar(19),r.receiveDate,21) as "RecvDate",
                 (convert(varchar,r.vendorNum) + ' - ' + convert(varchar,v.vendorname)) as "Vendor",
                 r.documentNum as "DocNum",
                 sum(rd.qtyacc) as "UnitsRcvd",
                 sum(rd.qtyacc*rd.cost) as "CostRcvd",
                 '' as "InvRcvd",
                 '' as "InvCost"
              from
                 receiving r
                 join receiving_dtl rd on r.rectransnum= rd.rectransnum
                 left outer join plu p on rd.plunum = p.plunum
                 join vendor v on v.vendorid = r.vendorNum
              where
                 r.txnStatus = 'R'
                 $whereclause
              group by DocNum,receiveDate, vendorNum,vendorname
              order by $sortfield
            ENDQUERY
     
            $tbl_ref = execQuery_arrayref($dbh, $query);
           

            # count and add summary record
            my ($sumqty, $sumextcost, $sumIqty, $sumIextcost) = (0,0);
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
                my $docnum = $$thisrow[2];               
                if ($document_hash{$docnum}) {                
                    $$thisrow[5] = $document_hash{$docnum}->[0];
                    $$thisrow[6] = $document_hash{$docnum}->[1];
                }
                $sumqty += trim($$thisrow[3]);
                $sumextcost += trim($$thisrow[4]);                
                $sumIqty += trim($$thisrow[5]);
                $sumIextcost += trim($$thisrow[6]);
            }
            my $ref = ['Total:', '', '',    $sumqty, $sumextcost, $sumIqty, $sumIextcost];
            push @$tbl_ref, $ref;


            my @format = ('', '', '',   
                        {'align' => 'Right', 'num' => '.0'},
                        {'align' => 'Right', 'currency' => ''},
                        {'align' => 'Right', 'num' => '.0'},
                        {'align' => 'Right', 'currency' => ''},
                        {'align' => 'Right', 'currency' => ''});
            my $newsortURL = "/cgi-bin/i_reports.pl?report=RECEIVING2&cb_summary=$cb_summary&startdate=$startdate&enddate=$enddate&VendorId=$VendorId&dept=$dept&sort=\$_";
            my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);          
            my @links = ('','', '/cgi-bin/i_reports.pl?report=RECEIVING_DTL&docnum=$_' );
 
            if (recordCount($tbl_ref)) { 
                printResult_arrayref2($tbl_ref, \@links, \@format, \@headerlinks); 
            } else { 
                print $q->b("no records found<br>\n"); 
            }        
        }
        
        StandardFooter();
    }

=pod
sub RECEIVING_DTL
=cut
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    # Report: RECEIVING_DTL
    # Description: Show receiving detail for a specific docnum
    # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "RECEIVING_DTL") {
        print $q->h1("Receiving Detail Report");

        my $docnum =trim(($q->param("docnum"))[0]);
        my $sortfield = trim(($q->param("sort"))[0]);
 
        print $q->h2("Document Number: $docnum");
        
        $sortfield = "SKU" unless $sortfield;



        $query = dequote(<< "        ENDQUERY");
          select
            convert(varchar(19),r.receiveDate,21) as "RecvDate",
            (convert(varchar,r.vendorNum) + ' - ' + convert(varchar,v.vendorname)) as "Vendor"
 
          from
            receiving r                        
            join vendor v on v.vendorid = r.vendorNum
          where
            r.documentNum = '$docnum'
                      
        ENDQUERY
        
        $tbl_ref = execQuery_arrayref($dbh, $query);
        my $receive_date = $$tbl_ref[1][0];
        my $vendor = $$tbl_ref[1][1];     
        
        
        $query = dequote(<< "        ENDQUERY");
          select
            rd.plunum as "SKU",
            p.pludesc as "Description",
            p.deptnum as "Dept",
            rd.qtyacc as "Qty",
            rd.cost as "Cost",
            (rd.qtyacc*rd.cost) as "ExtCost",
            p.invcontrolled as "InvCtrl"
          from
            receiving r
            join receiving_dtl rd on r.rectransnum= rd.rectransnum
            left outer join plu p on rd.plunum = p.plunum
            join vendor v on v.vendorid = r.vendorNum
          where
            r.documentNum = '$docnum'
            
          order by $sortfield
        ENDQUERY
        
        $tbl_ref = execQuery_arrayref($dbh, $query);

        # count and add summary record
        my ($sumqty, $sumextcost) = (0,0);
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
 
            $sumqty += trim($$thisrow[3]);
            $sumextcost += trim($$thisrow[5]);
        }
        my $ref = ['Total:', '', '',  $sumqty, '', $sumextcost,''];
        push @$tbl_ref, $ref;


        my @format = ('', '', '',  
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},
                   
                    {'align' => 'Right', 'currency' => ''});
        my $newsortURL = "/cgi-bin/i_reports.pl?report=RECEIVING_DTL&docnum=$docnum&sort=\$_";
        my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);        
        my @links = ( '/cgi-bin/i_products.pl?report=PRODINFO&SKU=$_' );
        if (recordCount($tbl_ref)) { 
            print $q->h3("Receive Date: $receive_date");
            print $q->h3("Vendor: $vendor");
            printResult_arrayref2($tbl_ref, \@links, \@format, \@headerlinks); 
        } else { 
            print $q->b("no records found<br>\n"); 
        }
        
        StandardFooter();
    }
=pod
sub PAIDINOUT
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: PAIDINOUT
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "PAIDINOUT") {
      print $q->h1("Paid in/out Report");

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"PAIDINOUT2"});

      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));      

      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), getdate(), 21)+ '01' as "FirstDate",
         convert(char(10), getdate(), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      $dbh->disconnect;

      StandardFooter();
   }

=pod
sub PAIDINOUT2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: PAIDINOUT2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "PAIDINOUT2") {
      print $q->h1("Paid in/out Report");

      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);
      my $sortfield = trim(($q->param("sort"))[0]);

      # type         token value
      # ------------ ----- ----------------------
      # NonMerchType     5 Paid In
      # NonMerchType     6 Paid Out

      $sortfield = "DateTime" unless $sortfield;

      my $whereclause = "";
      my $daterange = "";
      if ( ($startdate) && ($enddate) ) {
         if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
         }

         $whereclause = " and nms.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
         $daterange = " in date range $startdate - $enddate";
      }
      
      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            convert(varchar(19),nms.itemdatetime,21) as "DateTime",
            nms.txnnum as "TxnNum",
            IF nms.nonmerchid = 5 THEN '5-Paid In' ELSE '6-Paid Out' ENDIF as "NMId",
            nms.extSellPrice as "Price",
            ppr.profileresponse1 as "Reason",
            ppr.profileresponse2 as "Person"
         from
            Txn_Non_Merch_Sale nms
            join Txn_Profile_Prompt_Response ppr on
               nms.storenum=ppr.storenum and
               nms.transnum=ppr.transnum and
               nms.propromptid =ppr.propromptid and
               nms.itemnum = ppr.itemnum-1
            left outer join #tmpPostVoids v on nms.storenum=v.storenum and nms.transnum=v.transnum
         where
            nms.txnvoidmod = 0 and
            nms.nonmerchid in ( 5, 6)  and
            v.transnum is null
            $whereclause
	      order by $sortfield ;

         drop table #tmpPostVoids;
      end
      ENDQUERY
      #print $q->pre($query);

      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @format = ('', '', '', {'align' => 'Right', 'currency' => ''}, '', '');
      my $newsortURL = "/cgi-bin/i_reports.pl?report=PAIDINOUT2&startdate=$startdate&enddate=$enddate&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
      if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); }
      else { print $q->b("no records found<br>\n"); }
      StandardFooter();
   }


=pod
sub NMDONATION
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: NMDONATION
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "NMDONATION") {
      print $q->h1("Non-Merchandise \"Donation\" Report");

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"NMDONATION2"});

      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));      

      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), getdate(), 21)+ '01' as "FirstDate",
         convert(char(10), getdate(), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      $dbh->disconnect;

      StandardFooter();
   }

=pod
sub NMDONATION2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: NMDONATION2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "NMDONATION2") {
      print $q->h1("Non-Merchandise \"Donation\" Report");

      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);
      my $sortfield = trim(($q->param("sort"))[0]);

      # type         token value
      # ------------ ----- ----------------------
      # NonMerchType     3 Donations

      $sortfield = "DateTime" unless $sortfield;

      my $whereclause = "";
      my $daterange = "";
      if ( ($startdate) && ($enddate) ) {
         if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
         }

         $whereclause = " and nms.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
         $daterange = " in date range $startdate - $enddate";
      }
     
      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            convert(varchar(19),nms.itemdatetime,21) as "DateTime",
            nms.txnnum as "TxnNum",
            nms.extSellPrice as "Price",
            ppr1.profileresponse1 as "Reason",
            COALESCE(ppr2.profileresponse1, '') as "DonationFor"
         from
            Txn_Non_Merch_Sale nms
            join Txn_Profile_Prompt_Response ppr1
               on nms.storenum=ppr1.storenum and
                  nms.transnum=ppr1.transnum and
                  nms.propromptid =ppr1.propromptid and
                  nms.itemnum = ppr1.itemnum-1
            left outer join Txn_Profile_Prompt_Response ppr2
               on ppr1.storenum=ppr2.storenum and
                  ppr1.transnum=ppr2.transnum and
                  ppr1.propromptid= 16 and ppr2.propromptid in (12, 18) and
                  ppr1.itemnum = ppr2.itemnum-1
           left outer join #tmpPostVoids v on nms.storenum=v.storenum and nms.transnum=v.transnum
         where
            nms.txnvoidmod = 0 and
            nms.nonmerchid = 3 and
            v.transnum is null
            $whereclause
         order by $sortfield ;

         drop table #tmpPostVoids;
      end
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @format = ('', '', {'align' => 'Right', 'currency' => ''}, '', '');
      my $newsortURL = "/cgi-bin/i_reports.pl?report=NMDONATION2&startdate=$startdate&enddate=$enddate&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
      if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); }
      else { print $q->b("no records found<br>\n"); }
      StandardFooter();
   }

=pod
sub NMOTHER
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: NMOTHER
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "NMOTHER") {
      print $q->h1("Non-Merchandise \"Other\" Report");

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"NMOTHER2"});

      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));      

      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), getdate(), 21)+ '01' as "FirstDate",
         convert(char(10), getdate(), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      $dbh->disconnect;

      StandardFooter();
   }

=pod
sub NMOTHER2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: NMOTHER2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "NMOTHER2") {
      print $q->h1("Non-Merchandise \"Other\" Report");

      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);
      my $sortfield = trim(($q->param("sort"))[0]);

      # type         token value
      # ------------ ----- ----------------------
      # NonMerchType     7 Ohter

      $sortfield = "DateTime" unless $sortfield;

      my $whereclause = "";
      my $daterange = "";
      if ( ($startdate) && ($enddate) ) {
         if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
         }

         $whereclause = " and nms.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
         $daterange = " in date range $startdate - $enddate";
      }

      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            convert(varchar(19),nms.itemdatetime,21) as "DateTime",
            nms.txnnum as "TxnNum",
            nms.extSellPrice as "Price",
            ppr1.profileresponse1 as "Reason"
         from
            Txn_Non_Merch_Sale nms
            join Txn_Profile_Prompt_Response ppr1
               on nms.storenum=ppr1.storenum and
                  nms.transnum=ppr1.transnum and
                  nms.propromptid =ppr1.propromptid and
                  nms.itemnum = ppr1.itemnum-1
           left outer join #tmpPostVoids v on nms.storenum=v.storenum and nms.transnum=v.transnum
         where
            nms.txnvoidmod = 0 and
            nms.nonmerchid = 7 and
            v.transnum is null
            $whereclause
         order by $sortfield ;

         drop table #tmpPostVoids;
      end
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);

      my @format = ('', '', {'align' => 'Right', 'currency' => ''}, '', '');
      my $newsortURL = "/cgi-bin/i_reports.pl?report=NMOTHER2&startdate=$startdate&enddate=$enddate&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
      if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); }
      else { print $q->b("no records found<br>\n"); }
      StandardFooter();
   }

=pod
sub LAYAWAY
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: LAYAWAY
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "LAYAWAY") {
      my $showclosed = trim(($q->param("showclosed"))[0]);

      print $q->h1("List of ".($showclosed ? "Closed" : "Open")." Layaways");

      my %txntype = (
         1  => 'Sale',
         2  => 'Payment',
         3  => 'Pickup',
         4  => 'Item Adjust',
         5  => 'Cancel',
         6  => 'Write-off',
         7  => 'Payment Adjust',
         8  => 'Reactivate Writeoff',
         9  => 'Statement',
         10 => 'Change Information',
      );

      $query = dequote(<< "      ENDQUERY");
      select
         s.ptdnum, s.custnum, s.custname,
         p.ptdname as "PDT Type",
         convert(varchar(19), a.itemdatetime,21) as "DateTime",
         a.ptdtxntype as "TxnType",
         s.totalsale as "OrigAmt",
         s.totalpaymentamt as "Payments",
         s.lastpaymentamt as "Payment",
         t.endbalanceowing as "EndBal"
      from
         ptd_summary s
         join ptd_admin a on s.ptdnum=a.ptdnum and
            a.transnum = (select max(transnum)
                          from ptd_admin
                          where ptdnum=s.ptdnum and
                                ptdtxntype not in (9,10) and
                                txnvoidmod=0)
         left outer join ptd_total t on a.storenum=t.storenum and a.transnum=t.transnum
         left outer join layaway_parm p on s.ptdid =p.ptdid
      where
         s.ptdstatus = 1
      order by
         s.ptdnum
      ENDQUERY

      if ($showclosed) {
         $query = dequote(<< "         ENDQUERY");
         select
            s.ptdnum, s.custnum, s.custname,
            p.ptdname as "PDT Type",
            convert(varchar(19), s.updatedate,21) as "DateTime",
            tc.tmxcd_actual_dsc as "TxnType",
            s.totalsale as "OrigAmt",
            s.totalpaymentamt as "Payments",
            s.lastpaymentamt as "Payment",
            (s.totalsale-s.totalpaymentamt) as "EndBal"
         from
            ptd_summary s
            left outer join layaway_parm p on s.ptdid =p.ptdid
            left outer join tmxcodes tc on tc.tmxcd_type_code ='PDT_Status' and tc.tmxcd_code=s.ptdstatus
         where
            s.ptdstatus in (2,3,4)
         order by
            s.ptdnum
         ENDQUERY
      }

      my $any_dbh = TTV::utilities::openODBCDriver($dsn);
      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);

      # count and add summary record
      my @fieldsum = ('','','','','', '','','','',0);
      my @fields2sum = (9);
      for my $datarows (1 .. $#$tbl_ref) {
         my $thisrow = @$tbl_ref[$datarows];
         foreach my $i (@fields2sum) {
            $fieldsum[$i] += trim($$thisrow[$i]);
         }
         $$thisrow[5] = $txntype{$$thisrow[5]} if (exists($txntype{$$thisrow[5]}));
      }
      push @$tbl_ref, \@fieldsum;

      my @links = ( '/cgi-bin/i_reports.pl?report=LAYAWAYDTL&ptdnum=$_' );

      my @format = ('', '', '', '', '', '',
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, \@format); }
      else { print $q->b("no records found<Br>\n"); }

      $any_dbh->disconnect;

      unless ($showclosed) {
         print $q->a({-href=>"/cgi-bin/i_reports.pl?report=LAYAWAY&showclosed=1"}, "show Closed Layaways");
         print "<br>";
      }

      StandardFooter();
   }

=pod
sub LAYAWAYDTL
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: LAYAWAYDTL
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "LAYAWAYDTL") {
      my $ptdnum = trim(($q->param("ptdnum"))[0]);

      print $q->h1("Layaway Detail for $ptdnum");

      my %txntype = (
         1  => 'Sale',
         2  => 'Payment',
         3  => 'Pickup',
         4  => 'Item Adjust',
         5  => 'Cancel',
         6  => 'Write-off',
         7  => 'Payment Adjust',
         8  => 'Reactivate Writeoff',
         9  => 'Statement',
         10 => 'Change Information',
         'SA' => 'Sale',
         'PA' => 'Payment',
         'PU' => 'Pickup',
         'IA' => 'Item Adjust',
         'CA' => 'Cancel',
         'WO' => 'Write-off',
         'PJ' => 'Payment Adjust',
         'RW' => 'Reactivate Writeoff',
         'ST' => 'Statement',
         'CI' => 'Change Information',
      );

      my $any_dbh = TTV::utilities::openODBCDriver($dsn);

      # ### ### ###  ### ### ###  ### ### ###  ### ### ###  ### ### ###
      # Get and display Header.
      # ### ### ###  ### ### ###  ### ### ###  ### ### ###  ### ### ###
      print $q->h3("Header Information");

      $query = dequote(<< "      ENDQUERY");
      select
         s.ptdnum, s.custnum, s.custname,
         p.ptdname as "PDT Type",
         convert(varchar(19), s.updatedate,21) as "DateTime",
         tc.tmxcd_actual_dsc as "TxnStatus",
         s.totalsale as "OrigAmt",
         s.totalpaymentamt as "Payments",
         s.lastpaymentamt as "Payment",
         (s.totalsale-s.totalpaymentamt) as "EndBal"
      from
         ptd_summary s
         left outer join layaway_parm p on s.ptdid =p.ptdid
         left outer join tmxcodes tc on tc.tmxcd_type_code ='PDT_Status' and tc.tmxcd_code=s.ptdstatus
      where
         s.ptdnum = $ptdnum --.
      ENDQUERY

      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
      for my $datarows (1 .. $#$tbl_ref) {
         my $thisrow = @$tbl_ref[$datarows];
         $$thisrow[5] = $txntype{$$thisrow[5]} if (exists($txntype{$$thisrow[5]}));
      }
      my @format = ('', '', '', '', '', '',
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, \@format); }
      else { print $q->b("no records found<Br>\n"); }


      # ### ### ###  ### ### ###  ### ### ###  ### ### ###  ### ### ###
      # Get and display Activity Detail .
      # ### ### ###  ### ### ###  ### ### ###  ### ### ###  ### ### ###
      print $q->h3("Activity Detail");

      $query = dequote(<< "      ENDQUERY");
      select
         a.TransNum,
         convert(varchar(19), a.itemdatetime,21) as TranDate,
         a.cashiernum as Cashier,
         a.RegNum, a.TxnNum,
         a.ptdtxntype as TxnType,
         b.ptdtotalamt as TotalAmt,
         b.previouspayment as Payments,
         b.Payment, b.CancelFee,
         b.endbalanceowing as EndBal
      from
         ptd_admin a
         left outer join ptd_total b on a.storenum=b.storenum and a.transnum=b.transnum
      where
         a.txnvoidmod = 0 and
         a.ptdnum=$ptdnum
      order by a.Transnum;
      ENDQUERY

      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
      for my $datarows (1 .. $#$tbl_ref) {
         my $thisrow = @$tbl_ref[$datarows];
         $$thisrow[5] = $txntype{$$thisrow[5]} if (exists($txntype{$$thisrow[5]}));
      }
      @format = ('', '', '', '', '', '',
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }

      # ### ### ###  ### ### ###  ### ### ###  ### ### ###  ### ### ###
      # Get and display SKU Detail .
      # ### ### ###  ### ### ###  ### ### ###  ### ### ###  ### ### ###
      print $q->h3("SKU Detail");

      $query = dequote(<< "      ENDQUERY");
      select
         a.TransNum,
         convert(varchar(19), a.itemdatetime,21) as TranDate,
         a.ptdtxntype as TxnType,
         b.skunum as SKU,
         b.Description, b.Qty,
         b.extorigprice as OrigPrice,
         b.extsellprice as SellPrice
      from
         ptd_admin a
         join ptd_merch_entry b on a.storenum=b.storenum and a.transnum=b.transnum
      where
         a.txnvoidmod = 0 and
         a.ptdnum=$ptdnum
      order by
         a.Transnum, b.itemnum;
      ENDQUERY

      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
      for my $datarows (1 .. $#$tbl_ref) {
         my $thisrow = @$tbl_ref[$datarows];
         $$thisrow[2] = $txntype{$$thisrow[2]} if (exists($txntype{$$thisrow[2]}));
      }
      @format = ('', '', '', '', '',
                    {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},
                    {'align' => 'Right', 'currency' => ''});

      if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<Br>\n"); }


      $any_dbh->disconnect;

      StandardFooter();
   }

=pod
sub MISCSKU
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: MISCSKU
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "MISCSKU") {
      print $q->h1("Miscellaneous SKU Report");

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"MISCSKU2"});

      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), getdate(), 21)+ '01' as "FirstDate",
         convert(char(10), getdate(), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      $dbh->disconnect;

      StandardFooter();
   }
=pod
sub MISCSKU2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: MISCSKU2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "MISCSKU2") {
      print $q->h1("List of Miscellaneous SKUS");

      my $sortfield = trim(($q->param("sort"))[0]);

      $sortfield = "DateTime" unless $sortfield;
      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);

      my $whereclause = "";
      if ( ($startdate) && ($enddate) ) {
         if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
         }

         $whereclause = " and ppr.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
      }

      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            ppr.txnnum as "TxnNum",
            convert(varchar(19),ppr.itemdatetime, 21) as "DateTime",
            ms.skunum as "SKU",
            ms.qty*(ms.extsellprice/abs(ms.extsellprice)) as "Qty",
            ms.stdunitprice*(ms.extsellprice/abs(ms.extsellprice)) as "Price",
            ppr.profileresponse1 as "Description",
            ppr.profileresponse2 as "Dept"
         from
            Txn_Profile_Prompt_Response ppr
            join Txn_Merchandise_Sale ms on
               ppr.storenum=ms.storenum and
               ppr.transnum=ms.transnum and
               (ppr.itemnum = ms.itemnum+1 or ppr.itemnum = ms.itemnum+2)
            left outer join #tmpPostVoids v on ppr.storenum=v.storenum and ppr.transnum=v.transnum
         where
            ppr.txnvoidmod = 0 and
            ppr.propromptid = 26 and
            ms.deptnum='9999' and
            v.transnum is null
            $whereclause
         order by $sortfield;

         drop table #tmpPostVoids;
      end
      ENDQUERY
 
      my $any_dbh = TTV::utilities::openODBCDriver($dsn);
      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
      my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},  '', '');

      my $newsortURL = "/cgi-bin/i_reports.pl?report=MISCSKU2&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);

      my @skipline = ('','','','','','','');
      my @fieldsum = ('Total:','','',0, 0, '','');
      for my $datarows (1 .. $#$tbl_ref){
         my $thisrow = @$tbl_ref[$datarows];
		 $fieldsum[3] += trim($$thisrow[3]);							# Sum the Qty 
         $fieldsum[4] += (abs($$thisrow[3]) * $$thisrow[4]) ;	# Sum the Extended Amount
      }
      push @$tbl_ref, \@skipline;
      push @$tbl_ref, \@fieldsum;
      
      if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); }
      else { print $q->b("no records found<br>\n"); }
      $any_dbh->disconnect;

      StandardFooter();
   }

=pod
sub RUGSKU
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: RUGSKU
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "RUGSKU") {
      print $q->h1("Rug SKU Report");

      print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"RUGSKU2"});

      # use a database query to get the first and last day of the previous month.
      my ($enddate, $startdate);
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      $query = dequote(<< "      ENDQUERY");
      select
         convert(char(8), getdate(), 21)+ '01' as "FirstDate",
         convert(char(10), getdate(), 21) as "LastDate"
      ENDQUERY
      $tbl_ref = execQuery_arrayref($dbh, $query);
      if (recordCount($tbl_ref) > 0)
      {
         my $thisrow = @$tbl_ref[1];
         $startdate = trim($$thisrow[0]);
         $enddate   = trim($$thisrow[1]);
      }

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      $dbh->disconnect;

      StandardFooter();
   }
=pod
sub RUGSKU2
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: RugSKU2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "RUGSKU2") {
      print $q->h1("List of Rug SKUS");

      my $sortfield = trim(($q->param("sort"))[0]);

      $sortfield = "DateTime" unless $sortfield;
      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);

      my $whereclause = "";
      if ( ($startdate) && ($enddate) ) {
         if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
         }

         $whereclause = " and ms.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
      }

      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            ms.txnnum as "TxnNum",
            convert(varchar(19),ms.itemdatetime, 21) as "DateTime",
            ms.skunum as "SKU",
            ms.qty*(ms.extsellprice/abs(ms.extsellprice)) as "Qty",
            ms.extorigprice as "OrigPrice",
            ms.extsellprice as "SellPrice",
            (ms.extorigprice)*.75 as "Cost",
            ppr.profileresponse1 as "Description"
         from
            Txn_Merchandise_Sale ms
            join Txn_POS_Transactions pt on
            	ms.storenum=pt.storenum and
               ms.transnum=pt.transnum
            left outer join Txn_Profile_Prompt_Response ppr on
               ppr.storenum=ms.storenum and
               ppr.transnum=ms.transnum and
               (ppr.itemnum = ms.itemnum+1 or ppr.itemnum = ms.itemnum+2) and
               ppr.txnvoidmod = 0 and
			   -- Adding promptid 31 when RUG DESC prompt was created - kdg
               (ppr.propromptid = 26 or ppr.propromptid = 31)
            left outer join #tmpPostVoids v on ppr.storenum=v.storenum and ppr.transnum=v.transnum


         where
            ms.skunum='1199' and
            --ms.deptnum='1999' and  # 2009-05-11 - Revised to work with new departments - kdg
            ms.deptnum=8200 and
            pt.txnvoidmod1='0' and
            v.transnum is null
            $whereclause
         order by $sortfield;

         drop table #tmpPostVoids;
      end
      ENDQUERY

      my $any_dbh = TTV::utilities::openODBCDriver($dsn);
      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
      my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
                    {'align' => 'Right', 'currency' => ''},  {'align' => 'Right', 'currency' => ''}, {'align' => 'Right', 'currency' => ''}, '');
#WAKKA
      my $newsortURL = "/cgi-bin/i_reports.pl?report=RUGSKU2&sort=\$_";
      my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);


      my @skipline = ('','','','','','','','');
      my @fieldsum = ('Total:','','',0, 0, 0, 0, '');
      for my $datarows (1 .. $#$tbl_ref){
         my $thisrow = @$tbl_ref[$datarows];
         for my $field (3, 4, 5, 6)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
         }
      }
      push @$tbl_ref, \@skipline;
      push @$tbl_ref, \@fieldsum;

      if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); }
      else { print $q->b("no records found<br>\n"); }
      $any_dbh->disconnect;

      StandardFooter();
   }

   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: PERFSUM
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "PERFSUM") {
      print $q->h2("Daily Performance Summary Report");
            print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"PERFSUM2"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      my @ltm = localtime(time());
      my $today = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          # <SCRIPT LANGUAGE="JavaScript">var cal2 = new CalendarPopup("testdiv1");</SCRIPT>
                          $q->script({-language=>"JavaScript"},
                             "var cal$calcounter = new CalendarPopup(\"testdiv1\");"
                             # cal1.addDisabledDates(null, "2003-04-30");
                             # cal1.addDisabledDates("2003-06-25", null);
                          ),

                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$today"}),
                          #<A HREF="#" onClick="cal2.select(document.forms[0].startdate,'anchor2','yyyy-MM-dd'); return false;" NAME="anchor2" ID="anchor2"><img align=top border=0 height=21 src="calendar.gif" width=34></A>

                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$today"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";

      print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

      StandardFooter();
   }
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: PERFSUM2
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "PERFSUM2") {
      print $q->h2("Daily Performance Summary Report");
      my $startdate =trim(($q->param("startdate"))[0]);
      my $enddate   =trim(($q->param("enddate"))[0]);
      my $period    = trim(($q->param("period"))[0]);
      $period = 1440 unless ($period);

      my $selectPeriodSales = "dateadd(minute, floor(datediff( minute, '$startdate', min(txndatetime))/$period)*$period, '$startdate')";
      my $groupbySales = "datediff( minute, '$startdate', txndatetime)/$period";

      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            pt.storenum, pt.transnum, pt.txndatetime,
            count(*) as "Lines",
            sum(ms.qty) as "Qty",
            sum(ms.extSellPrice) as "Price"
         into #tmpSales
         from
            Txn_POS_Transactions pt
            join Txn_Merchandise_Sale ms on pt.storenum = ms.storenum and pt.transnum=ms.transnum
            left outer join #tmpPostVoids v on pt.storenum=v.storenum and pt.transnum=v.transnum
         where
            pt.txnvoidmod1 = 0 and
            pt.reggroupnum < 5 and
            v.transnum is null and
            pt.txndatetime >= '$startdate' and pt.txndatetime <= '$enddate 23:59:59'
         group by
            pt.storenum, pt.transnum, pt.txndatetime;

         drop table #tmpPostVoids;

         select
            $selectPeriodSales as "DateStart",
            count(*) as "Sales",
            sum(Lines) as "lines",
            sum(Qty) as "QTY",
            sum(Price) as "ExtPrice"
         into #tmpSalesByPeriod
         from
            #tmpSales
         group by $groupbySales
         order by "DateStart";

         drop table #tmpSales ;

          select
            sp.DateStart,
            sp.Sales,
            sp.QTY,
            convert( numeric(6,2), 0) as "AvgUnits",
            sp.ExtPrice,
            convert( smallmoney, 0) as "AvgPrice"
         into #tmpConvRpt
         from
         #tmpSalesByPeriod sp;

         drop table #tmpSalesByPeriod;


         update #tmpConvRpt
         set AvgUnits = 1.0 * isnull(QTY,0) / Sales,
             AvgPrice = 1.0 * isnull(ExtPrice,0) / Sales
         where isnull(Sales,0) <> 0;

         select             
			convert(varchar(10),DateStart,21) as "BusinessDate",
            ExtPrice as "NetSales",
            isnull(Sales,0) as "Transactions",
            Qty as "Units",
            AvgPrice,
            AvgUnits as "Items/Sale"
         from #tmpConvRpt;

         drop table #tmpConvRpt;
      end

      ENDQUERY
	 
	  my $rowsaffected = $dbh->do($query);
	  if ($rowsaffected eq "0E0") {
        $query="select businessdate from txn_pos_transactions order by businessdate";
        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my $earliest=0;
        while (my @tmp = $sth->fetchrow_array()) {
            unless ($earliest) {
                $earliest=$tmp[0];
                @tmp=split(/ /,$earliest);
                $earliest=$tmp[0];
            }
        }
		print $q->b("No records found for specified date range.<br>\n"); 
        print $q->b("Earliest transaction data is $earliest.<br>\n");
        print $q->p("Make certain the end date is on or after this date.");
		StandardFooter();
	  } else {
        $tbl_ref = execQuery_arrayref($dbh, $query);
        $dbh->disconnect();
        my @skipline = ('','','','','','');
        my @fieldsum = ('<b>Total:</b>',0, 0, 0, 0, 0);
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            for my $field (1, 2, 3)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
            }
        }
        $fieldsum[4]=$fieldsum[1]/$fieldsum[2];
        $fieldsum[5]=$fieldsum[3]/$fieldsum[2];
        push @$tbl_ref, \@skipline;
        push @$tbl_ref, \@fieldsum;
        #print $q->pre($query);
        my @format = (    '',
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'num' => '.2'},
            );

        if (recordCount($tbl_ref)) { 
            printResult_arrayref2($tbl_ref, '', \@format); 
        } else { 
            print $q->b("no records found<br>\n"); }
            StandardFooter();	  
        }	        
   }

=pod
sub NOBARCODE
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: NOBARCODE
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   elsif ($report eq "NOBARCODE") {
      print $q->h1("List of SKUs with no Barcodes");


      $query = dequote(<< "      ENDQUERY");
      select
         p.plunum as SKU, p.pludesc as Description, ufc.codename as VMCode, ic.qtycurrent as QOH
      from
         plu p
         left outer join user_flag_code ufc on p.userflagnum2=ufc.codeid and ufc.userflagnum=2
         left outer join inventory_current ic on ic.plunum=p.plunum
         left outer join PLU_Cross_Ref x on p.plunum=x.plunum
      where
         isnull(x.plunum,'') = ''
      ENDQUERY

      my $any_dbh = TTV::utilities::openODBCDriver($dsn);
      $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
      my @format = ('', '', {'align' => 'Right', 'num' => '.0'});

      if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, '', \@format); }
      else { print $q->b("no records found<br>\n"); }
      $any_dbh->disconnect;

      StandardFooter();
   }

=pod
sub PROBLEMSKU
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: PROBLEMSKU
   # Description: SKU's which may be incorrect in some way
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "PROBLEMSKU") {        
        my $validSkuTable="elderVillagesSKU";
        my @ltm = localtime();
        my $year = sprintf("%02d", $ltm[5]-100);  
        my $sku="";
        my $barcode="";
        my %skuhash=();
        my %storehash=();  
        my $count=0;  
        my $tcount=0;
 
        my $desc="";
        my $price="";
        my $qty="";
        my $ext="";
        my $vendorid="";
        my $vendorname="";
        my @skuentry=();
        my @tmp=();
        my $line=""; 
		my @rug_stores = (
			'7044'
		);		

        # Check that we can find the table of valid skus
        my $query = dequote(<< "        ENDQUERY");
            select * from dbo.sysobjects where id = object_id('$validSkuTable') and type = 'U'            
        ENDQUERY

    	my $tbl_ref = execQuery_arrayref($dbh, $query);

    	if (recordCount($tbl_ref) ==1) {
            print $q->h1("SKU and Vendor Check");
            $query = dequote(<< "            ENDQUERY");
                select * from $validSkuTable
            ENDQUERY
 
        	my $sth = $dbh->prepare($query);
        	$sth->execute();

            while (@tmp=$sth->fetchrow_array()) {
                $sku=$tmp[0];
                $barcode="none";
                if (defined($tmp[1])) {
                    if ($tmp[1]) {
                        $barcode=$tmp[1];                                         
                    }                     
                }  
                
                $skuhash{$sku}=$barcode;
            }

            # Now, create a hash containing barcodes found in the store
            $query = dequote(<< "            ENDQUERY");
                select p.plunum,cr.xrefnum
                from 
                    plu p 
                    left outer join plu_cross_ref cr on p.plunum=cr.plunum
                where p.vendorid like '01' order by p.plunum  
      
            ENDQUERY
            
        	$sth = $dbh->prepare($query);
        	$sth->execute();
         
        	while (@tmp=$sth->fetchrow_array()) {
                # Unless the sku is found in the valid sku hash, put it on the problem list.
                $sku=$tmp[0];
                $barcode="none";
                if ($tmp[1]) {
                    if ($tmp[1]) {
                        $barcode=$tmp[1];                                         
                    }                          
                }
                if ($storehash{$sku}) {
                    $storehash{$sku}.= ",$barcode";                    
                } else {
                    $storehash{$sku}= "$barcode";   
                }            
            }
            ##########
            # Check #1 - Find the SKU/PLU numbers for Villages items which are not recognized by Akron 
            ##########
            my @header = (
                "SKU",
                "Desc",
                "QTY",
                "Price"
            );
            my @problem_sku=(\@header); 
            $query = dequote(<< "            ENDQUERY");
                select p.plunum,ic.qtycurrent,p.pludesc,p.retailprice
                from 
                    plu p left outer join inventory_current ic on p.plunum=ic.plunum
                where p.vendorid like '01' order by p.plunum  
      
            ENDQUERY

        	$sth = $dbh->prepare($query);
        	$sth->execute();
         
        	SKU: while (@tmp=$sth->fetchrow_array()) {
                # Unless the sku is found in the valid sku hash, put it on the problem list.
                $sku=$tmp[0];
                $qty=$tmp[1];
                $qty=sprintf("%d",$qty);
                $desc=$tmp[2];                
                $price=$tmp[3];            
                $price=sprintf("%.2f",$price);
                $ext=($price * $qty);              
                   
                unless ($skuhash{$sku}) { 
                    # This is not a known sku
                    # Check to see if it is a damaged (93) or a sample (92)     
                    # Typically, these must be 7 digits, start with 92 or 93 followed by the year (second 2 digits of the year)
                    if (($sku =~ /^9[2,3]/)&& (length($sku) == 7)) {
                        my $sku2 = substr("$sku",2,2);                        
                        next if ($sku2 <= $year);                             
                    }    
 
                    #my $storenum = getStoreNum($dbh);  
                    #my $storeStatus=$store_cc_hash{$storenum};	
					if ($storeStatus eq "Contract") {
						# There are several ways that stores create SKUs based on valid Villages SKUs.
						# 1.  Add a letter to the end
						# Trim any letters off the end of the SKU
						my $new_sku=$sku;
						$new_sku =~ s/[a-z]$//;
						$new_sku =~ s/[A-Z]$//;
						next if ($skuhash{$new_sku});
					}
					# Here we provide some store specific allowances
					if ($storenum == 7044) {					
						if ($sku eq 9410000) {							
							next;							
						}
					}
					if ($storenum == 7027) {					
						if ($sku eq 9413000) {							
							next;							
						}
					}	
					foreach my $rug_store (@rug_stores) {
						if ($storenum == $rug_store) {						 
							if ($sku =~ /^1100/) {						 
								# Ignore the small rug skus assigned to Villages
								next SKU;
							}
						}
					}					
                    my @line=(
                        "$sku",
                        "$desc",
                        "$qty",
                        "\$$price"
                    );
                    push(@problem_sku,\@line);                                		
                    $count++;           
                }
        	}	
            if ($count) { 
                print $q->h4("Check #1: The following SKU's are assigned to vendor Villages (01) but are not recognized by the Ten Thousand Villages database.");
                TTV::cgiutilities::printResult_arrayref2(\@problem_sku, '', '');                  
            } else { 
                print $q->p("Check #1: No unrecognized SKU's found<br>\n"); 
            }   

            ##########
            # Check  #2 - for incorrect SKU - What looks like a Villages SKU assigned to a different Vendor
            ##########          
            $tcount+=$count;
            $count=0;
            @header = (
                "SKU",
                "Desc",
                "Qty",
                "VendorID",
                "VendorName"
            );
            @problem_sku=(\@header);
            
            $query = dequote(<< "            ENDQUERY");
                select p.plunum,ic.qtycurrent,p.pludesc,p.vendorid,v.vendorname
                from plu p join inventory_current ic on p.plunum=ic.plunum
                left outer join vendor v on p.vendorid=v.vendorid
                where p.vendorid not like '01' order by p.plunum  
      
            ENDQUERY
            $dbh = TTV::utilities::openODBCDriver($dsn);                        
        	$sth = $dbh->prepare($query);
        	$sth->execute();
         
        	while (@tmp=$sth->fetchrow_array()) {
                # If the sku is found in the valid sku hash, put it on the problem list.
                $sku=$tmp[0];
                $qty=$tmp[1];
                $qty=sprintf("%d",$qty);
                $desc=$tmp[2];
                $vendorid=$tmp[3];            
                $vendorname=$tmp[4];                            
                if ($skuhash{$sku}) { 
                    # This looks like a villages SKU but it is assigned to a different vendor            
                    my @line=(
                        "$sku",
                        "$desc",
                        "$qty",
                        "$vendorid",
                        "$vendorname"
                    );
                    push(@problem_sku,\@line);                                      
                    $count++;           
                }
        	}	


            if ($count) { 
                print $q->h4("Check #2: The following SKU's are assigned to vendors other than Villages.");
                TTV::cgiutilities::printResult_arrayref2(\@problem_sku, '', '');     
            } else { 
                print $q->p("Check #2: No Villages SKU's found to be assigned to other vendors.<br>\n"); 
            }                        
         
            ##########
            # Check #3 - for incorrect SKU - Barcode combinations
            ##########            
            $tcount+=$count;
            $count=0;
            @header = (
                "SKU",
                "Store's BarCode",
                "Akron's Barcode",                
            );    
            @problem_sku=(\@header);            

            foreach $sku (sort keys(%storehash)) {
                if (($skuhash{$sku}) && ($storehash{$sku})) {
                    # If this sku appears in both Akron and here in the store, compare the barcode
                    # Need to remember that a given SKU can have several barcodes associated with it
                    my @storeBarCodes=split(/\,/,$storehash{$sku});
                    my $found=0;
                    my $storeBarCode="";
                    foreach my $s (@storeBarCodes) {
                        $storeBarCode=$s;
                        if ($storeBarCode eq $skuhash{$sku}) {
                            $found=1;  
                        } 
                    }
                    if ($storeBarCode eq "none") {
                        # Remove the word "none" 
                        $storeBarCode="";
                    }
                    unless ($found) {
                        my @line=(
                            "$sku",
                            "$storeBarCode",
                            "$skuhash{$sku}"                                                        
                        );
                        push(@problem_sku,\@line); 
                        $count++;
                    }
                }                
            }            
       
            if ($count) {            
                print $q->h4("Check #3: The following SKU's have incorrect Bar Codes");
                TTV::cgiutilities::printResult_arrayref2(\@problem_sku, '', '');  
            } else { 
                print $q->p("Check #3: No Villages SKU's found with incorrect Bar Codes detected."); 
            }  

            ##########            
            # Check # 4 -  incorrect Vendor - No vendors should have single digit vendor numbers
            ##########            
            $tcount+=$count;
            $count=0;
            my %found_vendor_hash=();  
            @header = (
                "VendorID",
                "VendorName" 
            );
            
            my @problem_vendor=(\@header);
            $query = dequote(<< "            ENDQUERY");
                select vendorid,vendorname from vendor        
            ENDQUERY
        		
        	$sth = $dbh->prepare($query);
        	$sth->execute();
            # Build a hash of found vendors
        	while (@tmp=$sth->fetchrow_array()) {
                my $ven=$tmp[0];
                my $vname=$tmp[1];
                $found_vendor_hash{$ven}=$vname;     
            }   
                            
            foreach my $ven (sort keys(%found_vendor_hash)) {
                if (length($ven) == 1) {
                    # This looks like a problematic vendor ID
                    my @line=(
                        "$ven",
                        "$found_vendor_hash{$ven}"                                                                      
                    );
                    push(@problem_vendor,\@line);             		
                    $count++;                       
                }
            }

            if ($count) { 
                print $q->h4("Check #4: The following vendors have unacceptable ID numbers. 
                (All vendor ID's should be two digits.  e.g. 01, 02, ... 10, etc.)");  
                TTV::cgiutilities::printResult_arrayref2(\@problem_vendor, '', '');                                 
            } else { 
                print $q->p("Check #4: No invalid vendor number formats detected.<br>\n"); 
            }    
            ##########            
            # Check # 4b -  incorrect Vendor - No PLUs should have blank vendor numbers
            ##########            
            $tcount+=$count;
            $count=0;  
            @header = (
                "SKU",
                "SKU Desc" 
            );
            my $SKU;
            my $SKUDesc;
            @problem_vendor=(\@header);
            $query = dequote(<< "            ENDQUERY");
                select plunum,pludesc from plu where vendorid like ''        
            ENDQUERY
        		
        	$sth = $dbh->prepare($query);
        	$sth->execute();
            # Build a hash of found vendors
        	while (@tmp=$sth->fetchrow_array()) {
                $SKU=$tmp[0];
                $SKUDesc=$tmp[1];   
                next if ($SKU == 9999);	# Misc SKU used in SAP POS 2.3 - kdg
                my @line=(
                    "$SKU",
                    "$SKUDesc"                                                                      
                );
                push(@problem_vendor,\@line);             		
                $count++;                  
            }                   

            if ($count) { 
                print $q->h4("Check #4b: The following SKUs have blank vendor ID numbers. ");                 
                TTV::cgiutilities::printResult_arrayref2(\@problem_vendor, '', '');                                 
            }   
=pod			
            ##########            
            # Check # 4c -  incorrect Vendor - Vendor in the plu table is not in the vendor table
            ##########       
            my $vendor_list;
            my $sep;
            foreach my $ven (sort keys(%found_vendor_hash)) {            
                $vendor_list.="$sep\'$ven\'";
                $sep=",";
            }
 
            $query = dequote(<< "            ENDQUERY");
                select 
                    p.plunum,p.pludesc,p.vendorid,ic.qtycurrent as QOH
                from 
                    plu p
                    join inventory_current ic on p.plunum=ic.plunum
                where
                    p.vendorid not in ($vendor_list)
            ENDQUERY
            my $tbl_ref = execQuery_arrayref($dbh, $query);    
            if (recordCount($tbl_ref)) {         
                print $q->h4("Check #4c: The following SKUs have vendor ID numbers not in the vendor table. ");   
                my @format = ('','','',{'align' => 'Center', 'num' => '.0'});
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, '');         
            } 
=cut			
            ##########            
            # # Check #4c - See if the vendor is listed in the database
            ##########     
			# Check for SKUs with a vendor ID assigned but the vendor does not exist
			$count=0;   # Reset this counter
            @header = (   
				"SKU",
				"SKU Description",
                "VendorID",
              
            );            
            @problem_vendor=(\@header);			
			my %zombie_vendor_hash;
			
			$query = dequote(<< "			ENDQUERY");
				select 
					plunum,
					pludesc,
					vendorid 
				from 
					plu
				where
					vendorid not in (select vendorid from vendor)
				and
					plunum not in ('9999')
				
			ENDQUERY
				
			$sth = $dbh->prepare($query);
			$sth->execute();
		 
			while (@tmp=$sth->fetchrow_array()) {	
				my $sku=$tmp[0];
				my $desc=$tmp[1];
				my $vid=$tmp[2];
				my @line=("$sku","$desc","$vid");
				push(@problem_vendor,\@line);
				$count++;				
			}
            if ($count) { 
                print "<br />";
                print $q->h4("Check #4c: The following SKUs are assigned to vendors IDs are not defined in your system."); 
 
                TTV::cgiutilities::printResult_arrayref2(\@problem_vendor, '', '');  
            } else { 
                print $q->p("Check #5a: No invalid vendor ID's discovered.<br>\n"); 
            }  			
            ##########            
            # # Check #5 - See if the vendor is recognized
            ##########            
            $tcount+=$count;
            $count=0;
            @header = (              
                "VendorID",
                "Found Vendor",
                "Approved Vendor for this ID"
            );            
            @problem_vendor=(\@header);
            my $pcount=0;
            my @fstring=();
            my @kstring=();  
            # Iterate through the vendor hash (found in lookupdata.pm)
            foreach my $ven (sort keys(%vendor_hash)) {
                # $found is the vendor found using the vendor id number
                my $found=($found_vendor_hash{$ven});
                # $known is the vendor we expect for this vendor number
                my $known=($vendor_hash{$ven});      
                # If we didn't find a vendor using this number, skip to the next number
                next unless ($found);
                # Trim spaces from each vendor name                
                $found=~s/ //g;
                $known=~s/ //g;
                
                # See if the vendor found is what we expected
                unless ($found eq $known) {  
                    # This looks like a problematic vendor name
                    my @line=(
                        "$ven",
                        "$found_vendor_hash{$ven}",
                        "$vendor_hash{$ven}"
                    );
                    push(@problem_vendor,\@line);                   
                    $count++;                    
                }            
            }
       
            if ($count) { 
                print "<br />";
                print ($q->b("Check #5: The following vendors ID's are in a restricted range. 
                Their name descriptions should be changed to match the description in the \"Approved Vendor\" column.  
                (Vendor numbers from 01 to 19 are restricted to specific "),
                $q->a({-href=>"/cgi-bin/i_reference.pl?report=LISTHASH&VENDORS=VENDORS", -target=>'_top'}, "vendors"),")");
                print "<br />";
                print "<br />";
                TTV::cgiutilities::printResult_arrayref2(\@problem_vendor, '', '');  
            } else { 
                print $q->p("Check #5: No invalid vendor ID's in restricted range detected.<br>\n"); 
            }               
 		
            ##########            
            ## Check #6 - Check that department is correct
            ##########            
            $tcount+=$count;
            $count=0;
            @header = (              
                "SKU",
                "SKU Description",
                "Department"
            );            
            my @problem=(\@header);
            
            
            $query = dequote(<< "            ENDQUERY");
                select 
                    p.plunum,
                    p.pludesc,
                    p.deptnum,
                    ic.qtycurrent as QOH
                from 
                    plu p
                    join inventory_current ic on p.plunum = ic.plunum                    
                where 
                    p.deptnum not in 
                    (select deptnum from department)
            ENDQUERY
 
            $tbl_ref = execQuery_arrayref($dbh, $query);
            $pcount=recordCount($tbl_ref);
    
            if ($pcount) { 
                print $q->h4("Check #6: The following items have unknown departments");
                my @format = ('','','',{'align' => 'Center', 'num' => '.0'});
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, '');  
            } else { 
                print $q->p("Check #6: No SKUs found with unknown departments.<br>\n"); 
            }      
                        
            $tcount+=$pcount;
            ##########            
            ## Check #7 - Check that SKU numbering scheme is basically correct
            ##########            
             
            $count=0;
            $pcount=0;
            @header = ( 
				"SKU",
				"Desc",
                "VendorID",
				"Vendor"
 
            );            
            @problem_sku=(\@header);			
			if ($storeStatus eq "Company") {
				# Check that SKUs have correct vendorid
				# Get a list of vendors where we have inventory
				my @vendor_list;
				$query = dequote(<< "				ENDQUERY");
					select 
						distinct p.vendorid 
					from 
						plu p
						join inventory_current ic on p.plunum = ic.plunum
					where
						ic.qtycurrent > 0
				ENDQUERY
					
				$sth = $dbh->prepare($query);
				$sth->execute();
			 
				while (@tmp=$sth->fetchrow_array()) {
					push(@vendor_list,$tmp[0]);
				}    
				# For each of these vendors, verify that the skus start with this number
				my $warns=0;
				foreach my $vendor (@vendor_list) {
					# Several vendors do not follow this pattern and will be ignored
					next if ($vendor eq "01");	# Don't check Villages products
					next if ($vendor eq "11");	# Don't check Bunyaad products.
					next if ($vendor eq "06");	# Don't check Marketplace of India products.
					my $v=$vendor;
					# Trim off any leading 0's
					$v=~s/^0//;
					$query = dequote(<< "					ENDQUERY");
						select 
							p.plunum,p.vendorid,p.pludesc,v.vendorName
						from 
							plu p
							left outer join vendor v on p.vendorid = v.vendorid
						where
							p.vendorid like '$vendor'
						and
							p.plunum not like '$v%'
						and
							p.plunum not like '22%'
						and
							p.plunum not like '9%'
						and
							p.plunum not like '%test%'

					ENDQUERY
						
					$sth = $dbh->prepare($query);
					$sth->execute();
				 
					while (@tmp=$sth->fetchrow_array()) {  
						my $SKU=$tmp[0];
						my $desc=$tmp[2];
						my $vendorName=$tmp[3];
						my @array=("$SKU","$desc","$vendor","$vendorName");
						push(@problem_sku,\@array); 
						$count++;
					}          
				}    
				if ($count) {
					print $q->h4("Check #7: The following items have SKU numbers which do not begin with the VendorID");
					my @format = ('','','',{'align' => 'Center', 'num' => '.0'});
					TTV::cgiutilities::printResult_arrayref2(\@problem_sku, '', \@format, '');  
				} else { 
					print $q->p("Check #7: No SKUs found with SKU numbering issues.<br>\n"); 
				}      
				$tcount+=$count;
			}            
            ## End of individual error checks

            ## Check total error count            
            if ($tcount) {
                print $q->h3("All above issues should be corrected in Store Manager.  Please contact your RSM if you have questions.");
				
            } else {
                print $q->h3("No problems detected.");            
            }
            
                       
            StandardFooter();
        } else {
            print $q->h2("Error: Cannot locate $validSkuTable table");
            StandardFooter();
        }
        

   }
=pod
sub ALERT_RPT
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: ALERT_RPT
   # Description: Show alerts detected by the monitor.pl script.
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "ALERT_RPT") {  
        print $q->h1("Alerts.");        
        my $query = "select * from elderConfig where ckey like 'ALERT:%'";                   
        my $sth = $dbh->prepare($query);    
        $sth->execute();  
        my @header=(
            "Alert",
            "Action"
        );
        my @array_to_print=\@header;
        while (my @tmp = $sth->fetchrow_array) { 
            my $alert=$tmp[0];
            my $action=$tmp[1];
            $alert=substr($alert,6);
            
            my @array=("$alert","$action");
            push(@array_to_print,\@array);            
        }   
        if (recordCount(\@array_to_print)) {               
            my @links = ( '/cgi-bin/i_reports.pl?report=ALERT_CLEAR&alert=$_' );
            print $q->h4("The following issues should be corrected:");               
            printResult_arrayref2(\@array_to_print, \@links);   
            print $q->p("If you have questions about how to correct any of these issues, please contact your RSM or call POS Support in Akron at: (717) 859-8111.");
			print $q->p("After you have corrected the issue, click on the hyperlink on the Alert itself to clear the alert.");
        } else {
            print $q->p("You have no alerts at this time.");
        }       
        show_faq();
        StandardFooter();    
    }
=pod
sub ALERT_CLEAR
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: ALERT_CLEAR
   # Description: Clear selected alerts
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "ALERT_CLEAR") {  
        print $q->h1("Clear Alert");
        my $alert = ($q->param("alert"))[0]; 
        my $confirm = ($q->param("confirm"))[0];
        
        print $q->b("$alert");
        # Add a button for confirm and one for cancel
        if ($confirm) {
			# Escape single quotes
			$alert=~s/'/''/g;
            my $query = "delete from elderConfig where ckey like 'ALERT: $alert'";            
            unless ($dbh->do($query)) {
                print $q->h3("ERROR encountered trying to clear this alert");
                ElderLog($logfile, $dbh->{Name});
                ElderLog($logfile, "query=".$query);
                ElderLog($logfile, "err=".$dbh->err."\tstate: ".$dbh->state);
                ElderLog($logfile, "errstr=".$dbh->errstr);
            } else {
                print $q->p("The above alert has been cleared");
                print $q->a({-href=>"/cgi-bin/i_reports.pl?report=ALERT_RPT"}, "Return to Alerts");
                print "<br />";
            }            
        } else {
            
            print $q->p("Have you corrected this issue?");
            print $q->p("Click 'Confirm' to clear this alert.");
            
            print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"ALERT_CLEAR"});        
            print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"}); 
            print $q->input({-type=>"hidden", -name=>"alert", -value=>"$alert"}); 
            print $q->submit({-value=> "CONFIRM"});      
            print $q->end_form(), "\n";  
            
            print $q->p("To return to reports, click 'RETURN' below:");
            print $q->start_form({-action=>"/cgi-bin/i_reports.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"ALERT_RPT"});                       
            print $q->submit({-value=> "RETURN"});      
            print $q->end_form(), "\n";           
        
        }
        # 
        StandardFooter();
    }  
=pod
sub SHOW_FAQ_ANSWER
=cut
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: SHOW_FAQ_ANSWER
   # Description: Show the answer to the requested FAQ
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
    elsif ($report eq "show_faq_answer") {  
        print $q->h3("FAQ: Answer");    
        my $faq = ($q->param("faq"))[0];
        
		#my $storenum = getStoreNum($dbh);  
		#my $storeStatus=$store_cc_hash{$storenum};        
        if ($faq == 1) {
            print $q->p("To delete a pending purchase order: ");
            print $q->p("
            <li> Open Store Manager </li>            
            <li> Go to 'Inventory'</li>
            <li> Open 'Purchase Orders'</li>
            <li> Select (do not open) the purchase order from the list.</li>
            <li> Select 'Delete' from the menu on the right.</li>
            <li> Confirm that you indeed wish to delete the purchase order.</li>
            ");
            print $q->p("
            Note: Only pending purchase orders can be deleted.  If the purchase order has been issued, you will need to delete
            it from 'Receiving' in Store Manager.
            ");
        } elsif ($faq == 2) {
            print $q->p("To delete a pending receiving record: ");
            print $q->p("
            <li> Open Store Manager </li>
            <li> Go to Inventory</li>            
            <li> Open 'Receiving'</li>
            <li> Select (do not open) the receiving record from the list.</li>
            <li> Select 'Delete' from the menu on the right.</li>
            <li> Confirm that you indeed wish to delete the receiving record.</li>
            ");
            print $q->p("
            Note: Only pending receiving records can be deleted.  If the receiving record has a status of 'issued', 
            it will turn to 'pending' as soon as you open it. 
            "); 
        } elsif ($faq == 3) {
            print $q->p("<b>When purchase orders are not issued successfully:</b> ");
            print $q->p("
			When a purchase order is issued in Store Manager, three things need to happen:
			<ol>
			<li> A corresponding entry is made in receiving. </li>
			<li> A file containing the order info is created in the upload folder. (This is critical for Villages orders.) </li>
			<li> The status of the purchase order is changed to 'issued'. </li>
			</ol>
			<br />
			<p>
			If any of these three actions does not occur, there will be a problem with the order.  
			</p>
			<p>
			The failure to create the order in the upload folder is usually accompanied by a warning that will appear
			on the screen when the purchase order is issues saying 'Writing to upload failed.'  If this message is seen,
			the purchase order did not get properly issued.
			</p>
			<p>
			Note that you can go to 'order' and then 'List Orders' in The Elder to see the status of all issued or pending
			purchase orders	and receiving records in your system.			
			</p>
			<p>
			When there is an issuing failure with a Ten Thousand Villages order, it is first important to determine if Akron 
			received the order.  Typically and email is sent to confirm that Akron has received the order.  The \"Invoices
			and Credit Memos\" report on The Wire will also confirm that the order was received.  Please call Customer Service
			if for confirmation if necessary.
			</p>
			<p>
			When an order for Ten Thousand Villages fails to issue correctly <b>and does not reach Akron</b>, the order can be
			duplicated with the following steps:
			</p>
			<ol>
			<li> Exit Store Manager completely if it is currently open. </li>			
            <li> Open and sign into Store Manager again. (this helps verify a working Store Manager session.) </li>
            <li> Go to Inventory. </li>            
            <li> Open 'Purchase Orders'. </li>
			<li> Open the purchase order that had the issuing failure. </li>
			<li> Note the P.O. Number. </li>
			<li> Duplicate the order using the 'Duplicate' button on the right. </li>
			<li> Note that you have a new P.O. Number. </li>
			<li> Put a note in the 'Note' field that this is a duplicate of the original P.O. (optional) </li>
			<li> Issue this order and watch for any error messages. </li>
			<li> Run the 'List Orders' report and verify that this new purchase order is listed and shows no problems. </li>
			<li> Open the original purchase order in Store Manager. </li>
			<li> Void this order using the 'Void' button to the right. </li>
			</ol>
			<p>It is important <b>not</b> to duplicate an order if the original was received in Akron.</p>
			<li> Call POS Support at (717) 859-8111 if you need assistance. </li>
            ");
        } elsif ($faq == 4) {
            print $q->p("<b>When purchase orders are opened twice:</b> ");
			print $q->p(" This link on The Wire addresses the issue:");
			print $q->a({-href=>"//thewire.tenthousandvillages.com/company/content/outside-vendors"}, "Outside Vendors");						
        } elsif ($faq == 5) {
            print $q->p("<b>When the battery backup is low:</b> ");
			print $q->p(" This link on The Wire addresses the issue:");
			if ($storeStatus eq "Company") {
				print $q->a({-href=>"//thewire.tenthousandvillages.com/company/content/2-replacing-battery"}, "Replacing the battery");				
			} else {
				print $q->a({-href=>"//thewire.tenthousandvillages.com/contract/content/2-replacing-battery"}, "Replacing the battery");				
			}
			print $q->p("<b>When the battery test failed:</b> ");
			print $q->p("The battery backup self-test is the best way to determine the condition of the battery.  Typically, the self-test 
			is run at 6:00AM Monday morning.  If the test failed, the battery 
			most likely needs to be replaced.  See the above link on replacing the battery.");
        } elsif ($faq == 6) {
            print $q->p("<b>When there is a suggested order for Villages:</b> ");			
			print $q->p("Company stores do not typically create suggested orders for Villages product.  However, there is nothing preventing 
			this from being done by mistake.  If you find such a suggested order, please check to see if there is a valid reason for it.  If
			you determine that it must have been a mistake, simply go into Store Manager to the purchase order area and delete it.");
			print $q->p("<b>Note: If
			you happened to issue the order, it will need to be deleted from the receiving area in Store Manager rather than the purchase order
			area.)</b>");
        } elsif ($faq == 7) {
            print $q->p("<b>What is an Inventory Ctrl inconsistency? </b> ");			
			print $q->p("Sometime when the Inventory Ctrl setting is checked in Store Manager, the setting is not correctly applied
			internally.  When this occurs, the SKU will show up on the Inventory Control Summary tool in The Elder as not being on inventory
			control.  You can use this tool to set inventory control to correct it.  Or, try taking inventory control off in Store Manager
			and putting it on again.");	
        } elsif ($faq == 8) {
            print $q->p("<b>What if I forgot to receive all? </b> ");			
			print $q->p("
			<ol>
			<li>Open the receiving record in Store Manager and note which items were received.  Typically someone forgets to click \"receive all\" 
			because they first received the exceptions such as items which were short or damaged but then fogot to click the \"receive all\" 
			option before completing the receiving record.  Note any exceptions because they will need to be re-entered.</li>
			<li>Void the receiving record.</li>
			<li>Call Akron and ask them to set it back to pending.</li>
			<li>Re-enter the exceptions noted in step 1 above receive the record correctly.</li>
			</ol>
			");	
        } elsif ($faq == 9) {
            print $q->p("<b>What does it mean to have SPACE in the plunum field? </b> ");			
			print $q->p("
			
            When creating a new SKU in Store Manager, it is easy to accidentally hit the space bar and end up creating a SKU with a space
            after the intended SKU number.  In some cases, SKUs have been created with a space at the beginning of the SKU number.
            
            This situation presents a problem as some parts of the system recognize the space as a valid part of the SKU ID whereas other parts
            of the system ignore the space.  To address this, there is a process in place which will do the following:
            <li>If there is no other SKU in the system with the same SKU ID but without the space, the space will simply be removed.</li>
            <li>If there is another SKU in the system with the same SKU ID, the space is replaced with the word SPACE.</li>
            <br />
            <br />
            The latter is what has happened in this case.  What you need to do is this:
            <ol>
			<li>Look at the SKU that is like the one mentioned in this alert but without the word SPACE.</li>
			<li>Compare it to the SKU mentioned in this alert that does have the word SPACE.</li>
			<li>Check that the SKU without the word SPACE is correct. (Note: It may be that the SKU with the word SPACE is the one that 
            actually has the correct price.  In that case, the SKU without the word SPACE will need to be corrected.)</li>
			<li>After all corrections are made, delete the one with the word SPACE.  (Note: When using Store Manager to delete a SKU
            always find it and delete it in the SKU Selection Window rather than trying to delete it in the SKU edit window.)</li>            
			</ol>
			");						
        } else {
            print $q->p("Error:  The selected FAQ was not found!");
        }
		print "<br \>";
		print "<br \>";
        show_faq();
        StandardFooter();
    }
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # DEFAULT CASE
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   else {
      print $q->h1("Unknown Report Requested ($report).");
      StandardFooter();
   }
} # end SWITCH:

ElderLog($logfile, "$report finished");

print $q->end_td(), $q->end_Tr();
print $q->end_table(), "\n";

# close the html output
print $q->end_html;

exit;

# end of program.


sub StandardFooter {
   print "<br>\n";
   print $q->a({-href=>"javascript:parent.history.go(0)"}, "refresh");
   print $q->a({-href=>"javascript:parent.history.go(-1)"}, "go back");
   print $q->a({-href=>"/index.html", -target=>'_top'}, "go home");
}


sub doquery {
   my ($dbh, $query, $logfile) = @_;
   my $rc;
   unless ($rc = $dbh->do($query)) {
      ElderLog($logfile, $dbh->{Name});
      ElderLog($logfile, "query=".$query);
      ElderLog($logfile, "err=".$dbh->err."\tstate: ".$dbh->state);
      ElderLog($logfile, "errstr=".$dbh->errstr);
      return 0;
   }
   return $rc;
}
 
sub printTable {
    my $dataref=shift;
    my $headref=shift;
    my @array=@$dataref;
    my @tmp=();
    use CGI::Pretty qw/:standard :html3 *table/;
    $CGI::Pretty::INDENT = "  ";
    print "\n\n", start_table({-class=>'table', -valign=>'top', -bgcolor=>'feeacc', -cellspacing=>0}), "\n";

    print Tr( th({-bgcolor=>'ff9a00', -nowrap=>undef},  TTV::utilities::trim($headref)) ) ;
    my($counter) = 0;
    foreach my $entry (@array) {
      $counter++;
      @tmp=split(/\|/,$entry);
      my @row = ();
      for my $i (0 .. $#tmp) {
        my $field = TTV::utilities::trim($tmp[$i]);
        if (($field =~ m/^\s$/) || ($field eq ""))
        {
                $field = "&nbsp;"
        }         
        push @row, $field;         
      }
      print Tr(td({-class=>'table', -nowrap=>undef}, \@row) );
    }
    print end_table(),"\n";
} 

sub checkPID_orig {
   my $taskname = shift;
   my @process_list = `tlist`;
   my $pid = 0;

   foreach my $line (@process_list) {
      if ($line =~ /$taskname/i) {    
         $pid=1;
      }
   }
   return $pid;
}

sub checkPID {
	my $taskname = shift;
	my @process_list = `tlist`;
	my $pid = 0;	
 
	foreach my $line (@process_list) {			
		if ($line =~ /$taskname/i) { 				
			# Trim off the leading 0's
			$line=~s/^\s//g;					
			# The PID should be the first element
			my @tmp=split(/\s+/,$line);		
			$pid=$tmp[0];				
			$pid=~ s/ //g;		
		}
	}
	
	return $pid;
}

sub show_faq {
    # 
	#my $storenum = getStoreNum($dbh);  
    #my $storeStatus=$store_cc_hash{$storenum};	
    print $q->h3("Frequently Asked Questions");

    print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=1" }, "How do I delete pending purchase orders?<br />");  
    print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=2" }, "How do I delete pending receiving records?<br />");      
	print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=3" }, "What do I do when an order is not issued successfully?<br />"); 
	if ($storeStatus eq "Company") {
		print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=4" }, "What does it mean that a PO was opened twice?<br />");  
		print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=6" }, "What should I do with a suggested order for Villages?<br />"); 
	}	
	print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=5" }, "What do I do if the battery is low or the test failed?<br />"); 	
	print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=7" }, "What is an Inventory Ctrl inconsistency?<br />"); 	
	print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=8" }, "What if I forgot to receive all?<br />"); 	
    print $q->a({-href=>"/cgi-bin/i_reports.pl?report=show_faq_answer&faq=9" }, "What does it mean to have SPACE in the plunum field?<br />"); 	
    print "<br /><br />";    
}

sub getStoreNum_orig {
    my $dbh = openODBCDriver($dsn);  
    my $storenum = '';
    $query = "select storenum from sys_table where sys_key=1";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref) > 0) {
        $storenum = trim(@$tbl_ref[1]->[0]);}
    return $storenum;
    $dbh->disconnect;

}

 
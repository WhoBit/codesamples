#!c:/perl/bin/perl -w

##
##  i_toolshed.pl -- A place for various monitoring tools.
##
##  2008-04-22 - Created (based on i_stevetools.pl and intended to be called from there.) - kdg
##  2008-04-28 - Added tool to check modem - kdg
##  2008-05-07 - Using psloglist to get rebootlog - kdg
##  2008-05-19 - Added backup and montor reports - kdg
##  2008-05-20 - Temporarily disabled Modem & Reboot log sections until perl module issue is resolved - kdg
## 2008-05-29 - Correct an instance of reportrq to reportreq - kdg
## 2008-07-07 - Added support for i_toolshed_modem.pl.  This is in a separate script because not all stores will have the required modem perl module - kdg
## 2008-08-12 - 1.1.0 Added rebootlog and dir - kdg
## 2008-08-15 - 1.1.1 - Using � as eol character - kdg
## 2008-08-20 - 1.1.2 - Added MONITOR - kdg
## 2008-08-21 - 1.1.3 - Added i_reports_cost_beta - kdg
## 2008-10-21 - 1.1.4 - Don't send the query inself back when a query is requested. - kdg
## 2008-11-26 - 1.2.0 - Adding postools.pl functions - kdg
## 2008-11-28 - 1.2.1 - Added reset SAF - kdg
## 2008-12-09 - 1.2.2 - Changed reference from i_reports_cost_beta to i_report_cost_alt - kdg
## 2008-12-12 - 1.2.3 - Added reverting rec doc to pending. - kdg
## 2008-12-12 - 1.2.4 - Added detail of transaction to revert - kdg
## 2008-12-16 - 1.3.0 - Changed links to buttons - Added image - kdg
## 2008-12-18 - 1.3.1 - Changed MONITOR tool to SKU and added table - kdg
## 2009-03-18 - 1.3.2 - Added Vendor Name Adjust tool -kdg
## 2009-03-25 - 1.3.3 - Adding tool for SKUs with spaces - kdg
## 2009-03-27 - 1.3.4 - Revisions to SKUs with spaces tool - kdg
## 2009-03-30 - 1.3.5 - Added link to traffic Counter - kdg
## 2009-03-31 - 1.3.6 - Revisions to SKUs with spaces tool - kdg
## 2009-04-08 - 1.4.0 - Added tender report - kdg
## 2009-04-09 - 1.4.1 - Revisions to SKUs with spaces tool - kdg
## 2009-04-09 - 1.4.2 - Added logging for SKUs with spaces tool - kdg
## 2009-04-10 - 1.4.3 - Added Script Version to print on main page - kdg
## 2009-04-13 - 1.4.4 - Changed SKUs with spaces to NOT check skunum - kdg
## 2009-04-14 - 1.4.5 - Added single quotes to $newsku in query - kdg
## 2009-05-06 - 1.4.6 - Correcting traffic counter link - kdg
## 2009-09-01 - 1.4.7 - Added SFTP report - kdg
## 2009-09-01 - 1.4.8 - Cleaned up the reverting rec doc - kdg
## 2009-09-16 - v1.4.9 - Improvements to tool to clean up skus with spaces - kdg
## 2009-10-15 - v1.4.10 - Only revert an order that has been voided - kdg
## 2009-10-27 - v1.4.11 - Revised SKUSPACE and added reverting a Transfer to pending - kdg
## 2009-11-2 - v1.4.12 - Minor correction to getfile reporting - kdg
## 2009-11-05 - v1.4.13 - Updated to reverting a transfer document - kdg
## 2009-11-11 - v1.4.14 - Added SKUORPHAN - kdg
## 2009-11-23 - v1.4.15 - Added User Flag Tool - kdg
## 2009-11-30 - v1.4.16 - Worked on SKUSPACE to correct some problems with multi-key - kdg
## 2009-12-01 - v1.4.17 - Added BCTOOL.  Some fixes for UFTOOL - kdg
## 2009-12-02 - v1.4.20 - Small correction to BCTOOL - kdg
## 2009-12-09 - v1.4.21 - Added BCTOOL2 and updated tender report - kdg
## 2009-12-14 - v1.4.22 - Added SKUCHECK and SKUEDIT tools - kdg
## 2009-12-15 - v1.4.23 - Updated SKUEDIT to handle multiple skus - kdg
## 2009-12-23 - v1.4.24 - Added Receive All for Transfers - kdg
## 2010-01-02 - v1.4.25 - Added note if file is not found in GETFILE - kdg
## 2010-01-02 - v1.4.26 - Added SKUACTRPT - kdg 
## 2010-01-08 - v1.4.27 - Added LAYAWAY - kdg
## 2010-01-08 - v1.4.28 - Modified SKUACTRPT_DC to show returns - kdg
## 2010-01-13 - v1.4.29 - Added check_sku_on_promo - kdg
## 2010-01-19 - v1.5.0 - Added VIEW
## 2010-01-20 - v1.5.1 - Corrections to DIR - kdg
## 2010-03-01 - v1.5.2 - Modified BCTOOL2 to delete xrefnum from inventory_current and inventory_history - kdg
## 2010-03-10 - v1.5.3 - Added DETAILVAL - kdg
## 2010-03-17 - v1.5.4 - Revised message about turning off inv control for bar code - kdg
## 2010-03-25 - v1.5.5 - Added INVXREF.  Added confirmation on several tools - kdg
## 2010-03-31 - v1.5.6 - Added clockio tool - kdg
## 2010-04-12 - v1.5.7 - Added logging to GETQRY - kdg
## 2010-05-03 - v1.5.8 - Moved promotion reports to promotion tools - kdg
## 2010-05-05 - v1.5.9 - Created SKU tool sub-menu.  Added tool to purge SKUs - kdg
## 2010-05-06 - v1.5.10 - Added tax to tenderrpt - kdg
## 2010-05-07 - v.1.5.11 - More work on tenderrpt - kdg  TODO - Need to take aborted transactions out of the totals at the bottom.
## 2010-05-11 - v1.5.12 - Created basic tenderrpt.  Added purge via ASC option to orphan sku tool - kdg
## 2010-05-19 - v1.5.13 - Added check for SKUs with no vendorid to sku check tool - kdg
## 2010-05-21 - v1.5.14 - Added POSTATUS - kdg
## 2010-05-25 - v1.5.15 - Adjusted TENDERRPT to account for split tender.  Cleaned up $dbh definitions and added dbh->disconnect - kdg
## 2010-05-27 - v1.5.16 - Revised POSTATUS - kdg
## 2010-05-28 - v1.5.17 - Minor revision to POSTATUS - kdg
## 2010-06-01 - v1.5.18 - More work on POSTATUS - kdg
## 2010-06-03 - v1.5.19 - Correction to fixing SKU with spaces. Added more info to bar code tool - kdg
## 2010-06-07 - v1.5.20 - Added EVENTLOG - kdg
## 2010-06-15 - v1.5.21 - Updated PROMOVIEW to show current qty - kdg
## 2010-06-21 - v1.5.22 - Small correction to checking inventory in SKUPURGE - kdg
## 2010-08-20 - v1.5.23 - Added inventory report - kdg
## 2010-08-30 - v1.6.0 - Added SKUINVACTRPT - kdg
## 2010-09-29 - v1.6.1 - Revised tool to revert receiving to pending - kdg
## 2010-10-22 - v1.6.2 - Added past valuation reports - kdg
## 2010-10-26 - v1.6.3 - Added two tables to the layaway routine - kdg
## 2010-10-27 - v1.6.4 - Added tool to fix anonymous layaway payments - kdg
## 2010-11-03 - v1.6.5 - Fixed query in SKUSPACE - kdg
## 2010-11-10 - v1.6.6 - Added CHECKREC report - kdg
## 2010-11-11 - v1.6.7 - Added CHECKRECRANGE report - kdg
## 2010-11-29 - v1.6.8 - Accounted for post voids in tender report -kdg
## 2010-12-20 - v1.6.9 - Added logging to some sku fixes - added PROMOSALESRPT  - added to sku check tool kdg
## 2010-12-22 - v1.7.0 - Added tool to fix return and discount flag errors found in sku check - kdg
## 2010-12-27 - v1.7.1 - Ignore damaged Villages items on the return report - kdg
## 2010-12-29 - v1.7.2 - Ignore damaged Villages items on other queries in return report - kdg
## 2011-01-03 - v1.7.3 - Further refined query for returnflg and discount settings - kdg
## 2011-01-20 - v1.7.4 - Added Inventory sub menu - kdg
## 2011-01-25 - v1.7.5 - Added setting salesprsnid in fixing anonymous layaway - kdg
## 2011-02-01 - v1.7.6 - Added a link to Accounting's tax tool - kdg
## 2011-02-03 - v1.7.7 - Added link to i_tools_inventory.pl - kdg
## 2011-02-10 - v1.7.8 - Added traffic counter report - kdg
## 2011-02-16 - v1.7.9 - Added User Flag Code Name tool - kdg
## 2011-02-18 - v1.7.10 - Added verifyplu to sku check tool - kdg
## 2011-02-25 - v1.7.11 - Compare akron_status in lower case - kdg
## 2011-03-02 - v1.7.12 - Adding traffic counter adjust table - kdg
## 2011-03-16 - v1.7.13 - Added a Current Inventory Report - kdg
## 2011-03-22 - v1.7.14 - Some cleanup and added aborted transactions report - kdg
## 2011-02-24 - v1.7.15 - Added post-voided transactions report - kdg
## 2011-03-30 - v1.7.16 - Added output to file option for CURINV_RPT - kdg
## 2011-03-31 - v1.7.17 - Reformated current inventory report - kdg
## 2011-04-04 - v1.7.18 - Added MMID to SKUTOOL sku edit tool - kdg
## 2011-04-08 - v1.7.19 - Added PLU Update Report - kdg
## 2011-04-11 - v1.8.0 - Re-wrote UFTOOLLABELFIX - kdg
## 2011-04-12 - v1.8.1 - Added MIXMATCHRPT - kdg
## 2011-04-13 - v1.8.2 - Added DEPTTOOL - kdg
## 2011-04-13 - v1.8.3 - Added commonly viewed files to FILEVIEW - kdg
## 2011-04-16 - v1.8.4 - Added NCN - kdg
## 2011-04-26 - v1.8.5 - Added labels to update audit types in UPDATEAUDIT - kdg
## 2011-04-27 - v1.8.6 - Added TXN_REPORT.
## 2011-05-06 - v1.8.7 - Added Layaway Info to TXN_REPORT - kdg
## 2011-05-09 - v1.8.8 - Added more files to FILEVIEW - kdg
## 2011-05-09 - v1.8.9 - Added BarCodes to Current Inventory Report - kdg
## 2011-05-10 - v1.8.10 - Added pending Transfer Report - kdg
## 2011-05-11 - v1.8.11 - Some fixes for post-voided transactions in some reports - kdg
## 2011-05-13 - v1.8.12 - Corrections to transaction report for aborted transaction discounts.  Tweak to layaways. - kdg
## 2011-05-16 - v1.8.13 - More work on anonymous layaway but this still does not fix things - kdg
## 2011-05-19 - v1.8.14 - Modified SKUTOOL to change retail price - kdg
## 2011-05-27 - v1.8.15 - Revised info shown when resetting a rec doc - kdg
## 2011-06-02 - v1.8.16 - Added Card Transaction Report - kdg
## 2011-06-03 - v1.8.17 - Added Summary info to Card Transaction Report - kdg
## 2011-06-08 - v1.8.18 - Re-enabled the MODEM tool - kdg
## 2011-06-12 - v1.8.19 - Updated sku check to match the query in monitor.pl - kdg
## 2011-06-13 - v1.8.20 - Updated MODEM to accept ATZ1 as a valid response - kdg
## 2011-06-16 - v1.8.21 - Revised SKUINVRPT_DC - kdg
## 2011-06-21 - v1.9.0 - Revised spaces to handle skus with spaces at the beginning - kdg
## 2011-06-23 - v1.9.1 - Revised modem tools to permit entering and saving a com port - kdg
## 2011-06-24 - v1.9.2 - More revisions on handling spaces.  Added elderconfig tool - kdg
## 2011-06-28 - v1.9.3 - Added approvalcode to CTXN_QRY - kdg
## 2011-07-20 - v1.9.4 - Revised parsing transnet log with sub parse_transnet_content - kdg
## 2011-07-21 - v1.9.5 - Added nonmerch to TXN_REPORT2 - kdg
## 2011-07-25 - v1.9.6 - Simplified the query for checking verifysku setting - kdg
## 2011-07-26 - v1.9.7 - Added link for network printer - kdg
## 2011-08-02 - v1.9.8 - Adjusted query for checking for allowing discounts on SKUs - kdg
## 2011-08-08 - v1.9.9 - adding network printer 2 - kdg
## 2011-08-10 - v1.9.10 - added CLAIMS - kdg
## 2011-08-18 - v1.9.11 - Updated checking userflags to find flags that are not in lookupdata.pm - kdg
## 2011-09-20 - v1.9 12 - Some work on the UFTOOLLABEL tool to remove unknown codenames - kdg
## 2011-10-05 - v1.9.13 - Added dbtools submenu.  Added tool to check records where storeid is not correct - kdg
## 2011-10-06 - v1.10.0 - Added tax check report - kdg
## 2011-10-20 - v1.10.1 - Added a check for vendorid not in the vendor table in the sku check tool - kdg
## 2011-10-21 - v1.10.2 - Added a check for SKUs with no bar codes.  Updated specified transaction report to show taxes per item. - kdg
## 2011-10-27 - v1.10.3 - Added transnum search - kdg
## 2011-10-28 - v1.10.4 - Revised SKUTOOL to handle mmid2,3 & 4 - kdg
## 2011-11-02 - v2.0.0 - Revised the menus adding Misc Reports and Misc Tools.  Added monitor tool - kdg
## 2011-11-03 - v2.0.1 - Putting the monitor options in alphabetical order - kdg
## 2011-11-21 - v2.0.2 - Added provision for reverting transfer documents where were not voided - kdg
## 2011-11-21 - v2.0.3 - Added profile prompt info to TXN_REPORT2 - kdg
## 2011-11-23 - v2.0.4 - Adjustments to POSTATUS to handle rec docs ending in L - kdg
## 2011-11-23 - v2.1.0 - Added a tool to add SKUs to a MixMatch - kdg
## 2011-11-29 - v2.1.1 - Updated checking po status.  Added LISTORDERS - kdg
## 2011-12-07 - v2.1.2 - Added non merchandise sales report - kdg
## 2011-12-12 - v2.1.3 - Added option to select only skus w. qty to CURINV_RPT - kdg
## 2011-12-19 - v2.1.4 - Adjustments to handling undefined tax or total wo tax in TXN_REPORT2 - kdg
## 2011-12-21 - v2.1.5 - Revised checkrec - kdg
## 2012-01-05 - v2.1.6 - Added the inventory sub menu to misc tools - kdg
## 2012-01-11 - v2.1.7 - Correction to running monitor.pl - kdg
## 2012-01-19 - v2.1.8 - Cleanup processing of options in Monitor Tool.  Added Man. Adj'd column in CHECKREC2 - kdg
## 2012-01-24 - v2.1.9 - Added FIELDSRCH - kdg
## 2012-02-08 - v2.1.10 - Some adjustments to checking receiving to account for voided receiving records - kdg
## 2012-02-09 - v2.1.11 - Added BACKUP_TOOL.  Modified RECDOC to show ext cost - kdg
## 2012-02-16 - v2.2.0 - Added CHECKXFR - kdg
## 2012-02-20 - v2.2.1 - Added ability to show all network printer links - kdg
## 2012-02-23 - v2.2.2 - Refined ability to show all network device links - kdg
## 2012-03-13 - v2.2.3 - Added a few files to view - kdg
## 2012-03-20 - v2.2.4 - Formatting estimated taxes - kdg
## 2012-05-16 - v2.2.5 - Modified SKUORPHAN to find SKUs whether under IC or not - kdg
## 2012-05-17 - v2.2.6 - Added a tool to remove entries from the MISC SKU report (deleting profile prompt responses) - kdg
## 2012-06-05 - v2.2.7 - Modified UFTOOL to find SKUs where user flag is not set at all - kdg
## 2012-07-20 - v2.2.8 - Added Manager Code Report - kdg
## 2012-07-24 - v2.2.9 - Revised Manager Code Report to better handle cashier numbers - kdg
## 2012-07-27 - v2.2.10 - Added Date Discrepancy Report - kdg
## 2012-08-09 - v2.3.0 - Removed modem tool which has been replaced by the i_tools_modem.pl script - kdg
## 2012-08-14 - v2.3.1 - Modified SKUEDIT to add cost - kdg
## 2012-09-26 - v2.3.2 - Added safMonitor.log to FILEVIEW - kdg
## 2012-09-27 - v2.3.3 - Improved SKUORPHAN to make it more like monitor.pl - kdg
## 2012-10-01 - v2.3.4 - Query correction in DEPTTOOL2.  Some work with checking user flag descriptions. - kdg
## 2012-11-06 - v2.3.5 - Updated SKUCHECKto account for Traveler's Find SKUs being set to Not allowing discount - kdg
## 2012-11-07 - v2.3.6 - Began work to adapt to MS SQL - kdg
## 2012-11-26 - v2.3.7 - Added option to reset Purchase Order - kdg
## 2012-11-29 - v2.3.8 - Added SKUQOHRPT - kdg
## 2012-12-13 - v2.3.9 - Some cleanup of SKUACTRPT_DC - kdg
## 2012-12-28 - v2.3.10 - Corrected label on RESETPC - kdg
## 2012-12-31 - v2.3.11 - Added Tax Exempt Sales report - kdg
## 2013-01-18 - v2.3.12 - Adding support for SAP POS 2.3 - kdg
## 2013-01-28 - v2.3.13 - Added SKUMODRPT - kdg
## 2013-01-29 - v2.3.14 - Added a form to enter a cashier number in LAYAWAYANON2 - kdg
## 2013-02-06 - v2.4.0 - Added department number tool for Non-Villages for Company Stores - kdg
## 2013-02-07 - v2.4.1 - Added display of Villages SKUs with unknown departments - kdg
## 2013-02-18 - v2.4.2 - Added cashier number when it found by an alternative method in MGRCODE_RPT - kdg
## 2013-02-26 - v2.4.3 - Adjustments to transactions for layaways - kdg
## 2013-03-19 - v2.4.4 - Added a No Sale report - kdg
## 2013-04-02 - v2.5.0 - Revised all <br> to <br /> - kdg
## 2013-04-17 - v2.5.1 - Added register monitor logs to FILEVIEW - kdg
## 2013-04-30 - v2.5.2 - Revised FILEVIEW to support leading 0's in .jrn file - kdg
## 2013-05-16 - v2.5.3 - Revised reading Transnet log to support SAP POS 2.3 - kdg
## 2013-05-23 - v2.5.4 - Added SKU desc to checking receiving - kdg
## 2013-05-24 - v2.5.5 - Correction to non-merchandise sales report - kdg
## 2013-05-29 - v2.5.6 - Added ws_monitor.log to file view & revised standardfooter - kdg
## 2013-06-13 - v2.5.7 - Modified Returned Transactions Report to recognized aborted transactions - kdg
## 2013-06-13 - v2.5.8 - Adding tools for pending transactions - kdg
## 2013-06-25 - v2.5.9 - Updates to inventory reports - kdg
## 2013-07-23 - v2.5.10 - Added more info to the Non-merchandise Sales Report - kdg
## 2013-08-29 - v2.5.11 - Added ability to rename key in elderConfig - kdg
## 2013-09-10 - v2.5.12 - Modified CHECKRECRANGE to check only records Received - kdg
## 2013-09-25 - v2.5.13 - Modified CHECKXFRRANGE to add checking by transnum - kdg
## 2013-10-17 - v2.5.14 - Updated edit SKU to add profile prompt id - kdg
## 2013-10-31 - v2.6.0 - Added NVBCTOOL - kdg
## 2013-11-01 - v2.6.1 - Correction to NVBCTOOL - kdg
## 2013-12-22 - v2.6.2 - Correction to checking purchase order status against receiving status - kdg
## 2014-01-14 - v2.7.0 - Added VARTBLRPT to report data from elderPhyInvVariance table - kdg
## 2014-03-13 - v2.7.1 - Added DateTime label - kdg
## 2014-03-21 - v2.7.2 - Revision to searching for orphan sku - kdg
## 2014-03-25 - v2.7.3 - Some significant revisions to checking transfers - kdg
## 2014-05-23 - v2.7.4 - Set default directory for directory listing to c:/polling - kdg
## 2014-06-02 - v2.7.5 - Added po_status_hash to POSTATUS -kdg
## 2014-06-06 - v2.7.6 - Added OFFSITEINV - kdg
## 2014-06-16 - v2.7.7 - Added More workstations for Ephrata in FILEVIEW - kdg
## 2014-07-10 - v2.8.0 - Added IMPORT_SALES.  - kdg
## 2014-07-24 - v2.8.1 - More work on IMPORT_SALES.  Work on TaxExempt Sale as well - kdg
## 2014-07-29 - v2.8.2 - Added RESTARTEDNA - kdg
## 2014-07-31 - v2.8.3 - Added interface to i_tools_dsr.pl - kdg
## 2014-08-04 - v2.8.4 - Added Transnet Log to FILEVIEW - kdg
## 2014-08-05 - v2.8.5 - Added purge by vendor tool - kdg
## 2014-08-11 - v2.8.6 - Added PROMOTOUCH tool - kdg
## 2014-08-14 - v2.8.7 - Some changes and cleanup to CHECKXFRRANGE - kdg
## 2014-08-19 - v2.8.8 - Added PROMOFLUSH - kdg
## 2014-08-29 - v2.8.9 - Added MERGECLAIMS - kdg
## 2014-09-02 - v2.9.0 - Added DISCOUNTS report - kdg
## 2014-09-30 - v2.9.1 - Added provision to view pos monitor log - kdg
## 2014-09-30 - v2.9.2 - Added tool to export Mins & Maxes - kdg
## 2014-10-15 - v2.9.3 - Revised ordering of order list in PODTL - kdg
## 2014-11-04 - v2.9.4 - Began POS tool - kdg
## 2014-11-18 - v2.9.5 - Added logging for reverting a receiving document.  Changed from qtyReceived to qtyacc in CHECKRECRANGE - kdg
## 2014-11-19 - v2.9.6 - Revised reporting for SKUINVACTRPT - kdg
## 2014-12-19 - v2.10.0 - Added PROFILEPROMPT_QRY - kdg
## 2014-12-22 - v2.10.1 - Corrections to PROFILEPROMPT_QRY - kdg
## 2015-02-25 - v2.10.2 - Added Akron Status to CURINV_RPT - kdg
## 2015-03-17 - v2.10.3 - Added GETDATE
## 2015-04-08 - v2.10.4 - Added postools and windowsupdates logs to view a file - kdg
## 2015-04-27 - v2.10.5 - Added more info to PROFILEPROMPTSRCH report - kdg
## 2015-04-28 - v2.10.6 - Renamed Orphan SKU check to No Desc SKU Check - kdg
## 2015-05-18 - v2.10.7 - Some refinements to PODTL - kdg
## 2015-05-22 - v2.10.8 - Updated Backup Tool to simply set a backup flag - kdg
## 2015-10-05 - v2.11.0 - Added RESTARTBACKUP and REMOVEEODCOMPLETE - kdg
## 2015-11-09 - v2.11.1 - Updated NCN - kdg
## 2015-11-19 - v2.11.2 - Added SKIPDOWNLOAD - kdg
## 2015-11-21 - v2.11.3 - Added EFT.log to view a log - kdg
## 2015-11-24 - v2.12.0 - Added RENAMERECPO - kdg

use constant SCRIPT_VERSION 	=> "2.12.0";
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

use TTV::utilities;
use TTV::cgiutilities qw(printResult_arrayref2 formatCurrency formatNumeric ElderLog);
use TTV::lookupdata;
use File::Copy;
use File::Basename;
use Math::Round qw( nearest );

# Define the database engine
my $SQL_ENG = setSql();
my $xpsDir=getXPSDir();
my $parmDir=getParmDir(); 
my $pollingDir=getPollingDir();

# declare global variables
my $q = new CGI; # create new CGI object.
my $query;       # global variable for storing SQL query string.
my $sth;
my $scriptname="i_toolshed.pl";
my @links;       # global variable for storing the links array.
my $tbl_ref;     # global variable for storing a reference to a query result.
my $sname;		  # global variable for indicating which computer to use. kdg

# colors calculated by \\saz\c\Projects\triversity\web\calccolors.pl
# my @RGB = (206,101,0);
my $dkcolor="ce6500";
$dkcolor="660000";
my $ltcolor="ffe0c2";

# initialize the database handle
my $jdsn = "DSN=JOURNAL;ENG=$SQL_ENG;UID=****;PWD=****;";
my $dsn = "DSN=BACKOFF;ENG=$SQL_ENG;UID=****;PWD=****;";
my $dbh = openODBCDriver($dsn);
my $hostname=`hostname`;

my @report=();
my $report_file="";
my $line="";
my $command="";
my $logfile = "C:/MLINK/akron/i_toolshed.log";

# These tables have entries for plunum
my @table_list=(
"plu",
"PLU_Cross_Ref",
"promo",
"LevelCluster",
"vendorsku",
"Receiving_Dtl",
"promo_dtl",
"inventory_physical_dtl",
"price_change_dtl",
"linkrecord",
"inventory_adjustment",
"inventory_control_import",
"inventory_current",
"inventory_history",
"purchase_order_dtl",
"vendorsku_import",
"store_transfer_dtl",
"purchase_order_dtl",
"tagsworking",
"wrk_global_change",
"plu_supplemental",
"promo_dtl_nulls",
"label_queue_dtl",
"ptd_merch_entry",
"Loyalty_Program_Eligibility",
"inventory_buckets",
"inventory_current_ext",
"inventory_history_ext",
"plu_effective_date",
"instock_research_dtl",
"transfer_box",
"LevelCluster_Effective_Date",
"order_entry_dtl",
"order_guide_dtl",
"plu_shelf_location"
);
@table_list=(
"plu",
"PLU_Cross_Ref",
"promo",
"LevelCluster",
"vendorsku",
"Receiving_Dtl",
"promo_dtl",
"inventory_physical_dtl",
"price_change_dtl",
"linkrecord",
"inventory_adjustment",
"inventory_control_import",
"inventory_current",
"inventory_history",
"purchase_order_dtl",
"vendorsku_import",
"store_transfer_dtl",
"purchase_order_dtl",
"tagsworking",
"wrk_global_change",
"plu_supplemental",
"promo_dtl_nulls",
"label_queue_dtl",
"ptd_merch_entry",
"Loyalty_Program_Eligibility",
"inventory_buckets",
"inventory_current_ext",
"inventory_history_ext",
"plu_effective_date",
"instock_research_dtl",
"transfer_box",
"LevelCluster_Effective_Date",
"order_entry_dtl",
"order_guide_dtl",
);
 
my %table_multi_key=(
    'inventory_history'=>'periodstart',
    'inventory_adjustment'=>'seqnum'
);

# Following is a list of tables where there should only be one entry for a given plunum.  If there 
# is an entry for a given plunum without a space and one with a space, we cannot simply remove
# the space from the latter because it will conflict with the former.
my @table_lista=(
"plu",
"PLU_Cross_Ref",
"LevelCluster",
"linkrecord",
"inventory_current",

"wrk_global_change",
"plu_supplemental",
"Loyalty_Program_Eligibility",
"inventory_buckets",
"inventory_current_ext",
"inventory_history_ext",
"plu_effective_date",
"transfer_box",
"LevelCluster_Effective_Date",
);
# Following is a list of tables where multiple entries with plunum are acceptable.  We only need
# to change the ones with a space.
my @table_listb=(
"promo",
"vendorsku",
"Receiving_Dtl",
"promo_dtl",
"inventory_physical_dtl",
"inventory_adjustment",
"inventory_history",
"price_change_dtl",
"inventory_control_import",
"purchase_order_dtl",
"vendorsku_import",
"store_transfer_dtl",
"purchase_order_dtl",
"tagsworking",
"promo_dtl_nulls",
"label_queue_dtl",
"ptd_merch_entry",
"instock_research_dtl",
"order_entry_dtl",
"order_guide_dtl",
);
 
# The following tables have entries for skunum
my @table_list2=(
"txn_manual_inv_adj",
"txn_merchandise_inventory",
 "txn_merchandise_receiving",
"txn_transfer",
"txn_merchandise_sale",
"pending_order_cat_dtl",
"vendorsku",
"receiving_dtl",
"store_transfer_dtl",
"purchase_order_dtl",
"ptd_merch_entry",
"wrking_merchandise_sale",
"tmx_Item_movement_totals",
"Item_movement_totals",
"txn_merchandise_sale_lowest_price",    
"txn_price_change"

);


# print the HTML Header
my $style="BODY{font: normal 11pt times new roman,helvetica}\n".
          "FORM{font: normal 11pt times new roman,helvetica}\n".
          "TH{font: bold 11pt arial,helvetica}\n".
          "TD{font: normal 11pt arial,helvetica}\n".
          "table{font: normal 11pt arial,helvetica}\n".
          "input{font: normal 11pt times new roman,helvetica}\n".
          ".tableborder {border-color: #EBEACC; border-style: solid; border-width: 1px;}\n".
          ".table {border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tablehead {background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px; color: #$ltcolor;}\n".
          "A.headerlink:link {color: #$ltcolor; text-decoration: none; }\n".
          "A.headerlink:active {color: #$ltcolor; text-decoration: none;}\n".
          "A.headerlink:visited {color: #$ltcolor; text-decoration: none;}\n".
          "A.datalink:link {color: #000000;}\n".
          "A.datalink:active {color: #000000;}\n".
          "A.datalink:visited {color: #000000;}\n".
          ".tabledata {background-color: #$ltcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tabledatanb {background-color: #$ltcolor; border-color: #$ltcolor; border-style: solid; border-width: 1px;}\n". # nb=no-border
          "table.table a.sortheader {background-color:#$dkcolor; color:#$ltcolor; text-decoration: none;}\n".
          "table.table span.sortarrow { color:#$ltcolor; text-decoration: none; }".
          ".button1 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;
          ".redalert {color: red;}".
          ".greenalert {font: bold; color: green;}".          
          "";   
print $q->header( -type=>'text/html');
print '<SCRIPT LANGUAGE="JavaScript" SRC="/CalendarPopup.js"></SCRIPT>'."\n";
print '<SCRIPT LANGUAGE="JavaScript">document.write(CalendarPopup_getStyles());</SCRIPT>'."\n";
print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

print $q->start_html(-title=>'Village Elder',
                     -author=>'kdg@tenthousandvillages.com',
                     -style=>{'code'=>$style},
                     );

print '<SCRIPT LANGUAGE="JavaScript" SRC="/CalendarPopup.js"></SCRIPT>';
print '<SCRIPT LANGUAGE="JavaScript">document.write(CalendarPopup_getStyles());</SCRIPT>';



# If a report request was given.
my $reportreq=($q->param("reportreq"))[0];
unless ($reportreq) {
	$reportreq="backup_report";
}
# If a query request was given.
my $queryreq=($q->param("queryreq"))[0];
unless ($queryreq) {
	$queryreq="none";
}
# Figure out which tool to use.
my $tool= ($q->param("tool"))[0];
unless ($tool) {
	$tool="DEFAULT";
}
my $report= ($q->param("report"))[0];
if ($report) {
	$tool=$report;
}
$sname = ($q->param("sname"))[0];
unless ($sname) {
   $sname="$hostname";
}

my $txnnum= ($q->param("txnnum"))[0];

my $trafficCounter="";

########################
# MAIN
########################
###
# Try to stick a photo in here
print $q->start_table({-width=>"40"}), "\n";
#print $q->Tr($q->td($q->img({-src=>"/images/shed.jpg", -align=>"right", -border=>"0"}))) ;
print $q->Tr($q->td($q->img({-src=>"/images/shed.jpg"}))) ;
print $q->end_table(),"\n";

###

if ($tool eq "DEFAULT") {
    print $q->h2("Tool Shed version: ".SCRIPT_VERSION);
    my @ltm = localtime();
    my $date_label = sprintf("%04d-%02d-%02d %02d:%02d:%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3], $ltm[2],$ltm[1],$ltm[0]); 
	print $q->p("DateTime: $date_label");
    #  Find the traffic counter
    	
	$query = dequote(<< "	ENDQUERY");
        select cvalue from elderConfig where ckey like 'TrafficCounter|IPAddress'
  
	ENDQUERY
    my $dbh = openODBCDriver($dsn); 
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            $trafficCounter="$tmp[0]";
            $trafficCounter=~s/ //g;    # Remove any spaces
        }
    }
    # Find the network printer
    #my $HP6000=getConfig($dbh,"HP6000|IPAddress");
    #my $HP6500=getConfig($dbh,"HP6500|IPAddress");
    # Find other network devices - Any network devices configured in the elderConfig with the proper
	# format of ckey being <device>|IPAddress should be found below
    my %networkDevices;
    my $elderConfigTable = 'elderConfig';
    my $query = "Select ckey, cvalue from $elderConfigTable where ckey like '%IPAddress'";	
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        my $IP=$tmp[1];
        my $host=$tmp[0];        
        my @ptmp=split(/\|/,$host);
        $host=$ptmp[0];        
        $networkDevices{$IP}=$host;        
    }    
 
    
    my @additional_reportlist;
    # fields: [display name, script, report, report note]
    my @reportlist = (
    #print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=RPT&reportreq=backup_report"}, "View last backup report"));

         ["Business Brief Report", "/cgi-bin/i_toolshed.pl", "BBRPT",
          "View last business day report"              
         ],    
        ["Misc Reports", "/cgi-bin/i_toolshed.pl", "MISCREPORTS",
          "Miscellaneous Reports Menu"              
        ],       

         ["Directory", "/cgi-bin/i_toolshed.pl", "DIR",
          "View a directory."             
         ],
         ["File", "/cgi-bin/i_toolshed.pl", "FILEVIEW",
          "View a text file."             
         ],         
    
         #["Villages SKU check", "/cgi-bin/i_toolshed.pl", "SKU",
         # "A rough check of SKU's in the store."
         #], 


        ["Sales", "/cgi-bin/i_toolshed.pl", "SALES",
          "Sales Reports."
        ],           

    
        ["Layaway Tools", "/cgi-bin/i_toolshed.pl", "LAYAWAY",
          "View layaways with the option to delete"
        ],
        ["Order Tools", "/cgi-bin/i_toolshed.pl", "ORDERTOOLS",
          "Tools for purchase orders, receivings, and transfers"
        ],                
        ["SKU Tools & Reports", "/cgi-bin/i_toolshed.pl", "SKUTOOLS",
          "Tools to check and report on SKUs "
        ],               
        ["Promotion & MixMatch Discount Tools", "/cgi-bin/i_toolshed.pl", "PROMOTOOLS",
          "Tools to check promotions and work with MixMatch Discounts"
        ],       

        ["Database Tools", "/cgi-bin/i_toolshed.pl", "DBTOOLS",
          "Various tools for the database."
        ],  
        ["Misc Tools", "/cgi-bin/i_toolshed.pl", "MISCTOOLS",
          "Various other tools."
        ],          
        
    );

    if ($trafficCounter) {
        #print $q->a({-href=>"http://$trafficCounter", -target=>'_top'}, "Traffic Counter"); 

        @additional_reportlist = (
        ["Traffic Count Report", "/cgi-bin/i_toolshed.pl", "TCRPT",
        "View last 500 lines of the elderTraffic table" ],
        ["Traffic Count Adjust Report", "/cgi-bin/i_toolshed.pl", "TCADJRPT",
        "View last 500 lines of the elderTrafficAdj table" ],
        ); 
        unshift @reportlist, @additional_reportlist;
    }
 
	foreach my $IP (sort {$networkDevices{$a}<=>$networkDevices{$b}} keys(%networkDevices)) {
        my $host=$networkDevices{$IP};
        print "<br />";
        print $q->a({-href=>"http://$IP", -target=>'_top'}, "$host");    
    }
       

    if (1) {
        foreach my $reportref (@reportlist) {
            my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
            print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
        }
    }

    $dbh->disconnect;
    StandardFooter();
	  
	  
} elsif ($tool eq "DEFAULT2") {

	print $q->h2("Tool Shed");
	print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=RPT&reportreq=backup_report"}, "View last backup report"));
	print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=RPT&reportreq=monitor_report"}, "View last monitor report"));
	print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=DIR"}, "View a directory"));
	print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=BOOTLOG"}, "View recent boot log"));
    print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=SKU"}, "Villages SKU check"));	
    print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=RESETPC"}, "Reset pcAnywhere server"));	
    print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=RESETSAF"}, "Reset Store and Forward File"));
	print $q->li($q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=RESETRECDOC"}, "Reset a Receiving Doc back to pending"));
	if (-e "c:/program files/apache group/apache/cgi-bin/i_reports_cost_alt.pl") {
		print $q->li($q->a({-href=>"/cgi-bin/i_reports_cost_alt.pl?report=SALESRPT"}, "Net Sales Report - Finding Purged SKU's"));
		print $q->li($q->a({-href=>"/cgi-bin/i_reports_cost_alt.pl?report=COGS"}, "Cost of Goods Sold Report - Finding Purged SKU's"));
	 
	}
	
	if (-e "c:/perl/site/lib/Device/modem")  {
		print $q->li($q->a({-href=>"/cgi-bin/i_toolshed_modem.pl?tool=MODEM"}, "Modem Test"));
	}  
    print "<br />\n";
    StandardFooter();
 } elsif ($tool eq "NCN") { 
    print $q->h2("New Customer Number");
    # What is this store's number?
    my $storenum=getStoreNum($dbh);
    foreach my $s  (1 .. 5) {
        my $custno="$storenum"."$s";
        $query = dequote(<< "        ENDQUERY");
            select  
                custnum
            from
                pa_customer
            where
                custnum like '$custno%'
            order by 
                custnum desc
                
        ENDQUERY
                
        my $dbh = openODBCDriver($dsn); 		
        my $sth = $dbh->prepare($query);
        $sth->execute();        
        while (my @tmp=$sth->fetchrow_array()) {   
            my $next=$tmp[0];
            $next++;
			my $last_four=substr($next,(length($next) - 4),4);
			while ($last_four =~ /^0/) {
				$last_four =~ s/^0//;
			}
            print $q->p("The next customer number for register #${s} would be $next - enter: $last_four");            
            last;
        }
    }        
    print "<pre>
To set the next customer number for a register, run Manager Code #26 on the register. 
It will as you for your password and then for the 'Autoref Number ID'. 
There are four catagories currently in use: 
    1) Store Credit 
    2) Layaway Number 
    3) Customer Number 
    5) Gift Certificate Number

You would select '3' to reset the autogeneration of the Customer Number.
 
You are then asked whether you indeed wish to edit ID #3 and it alternately displays the 
current autogeneration setting.
 
You answer 'Y' to edit it and then enter the new sequence number (as shown above).
 
Once you do this, the screen asks you to 'Enter Autoref Number 3' to which you may simply press enter.
 
Again, you are asked if you wish to edit ID #3 and it alternately displays the new autogeneration 
setting you just entered. 

This time simply select 'N' and you will get signed out of the register and your update is complete.
 
    
    </pre><br />";
    StandardFooter();
 } elsif ($tool eq "BBRPT") { 
    print $q->h2("Business Brief");
    my $sstate="closed";
    my $bdate;
    my $lasteod;
    my $pbdate;
	$query = dequote(<< "	ENDQUERY");
        select  
            storeopen,
            businessdate,
            lasteodtime,
            prevbusinessdate
        from
            server_state
	ENDQUERY
    
    my $dbh = openODBCDriver($dsn); 		
	my $sth = $dbh->prepare($query);
	$sth->execute();
    
	while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0] eq "Y") {
            $sstate="open";
            $bdate=$tmp[1];
            my @tmpdate=split(/\./,$bdate);
            $bdate=$tmpdate[0];            
        } else {
            # The store is closed
            $lasteod=$tmp[2]; 
            my @tmpdate=split(/\./,$lasteod);
            $lasteod=$tmpdate[0];              
        }
        $pbdate=$tmp[3];
        my @tmpdate=split(/\./,$pbdate);
        $pbdate=$tmpdate[0];  
        print $q->h3("The store is currently $sstate");
        if ($bdate) {
            print $q->h3("The current business date is $bdate");
        }
        if ($pbdate) {
            print $q->h3("The last business date was $pbdate");
        }      
        if ($lasteod) {
            print $q->h3("The store closed at $lasteod");
        }                                      
    }
    # Get the last date backed up
	$query = dequote(<< "	ENDQUERY");
        select  
            cvalue            
        from
            elderConfig
        where
            ckey like 'lastBdateBackedUp'
	ENDQUERY
 	$sth = $dbh->prepare($query);
	$sth->execute();
    
	while (my @tmp=$sth->fetchrow_array()) {
        my $date=$tmp[0];
        my @tmpdate=split(/\./,$date);
        $date=$tmpdate[0];
        print $q->h3("The last business date backedup was $date");
    }
    # Get the summaries for the last business day.
    # Get just the date from the last business date.
    my @tmp=split(/\s+/,$pbdate);    
    $pbdate=$tmp[0];    
	$query = dequote(<< "	ENDQUERY");
        select  
            ts.cashiernum,
            e.empfirstname,
            e.emplastname,
            ts.businessdate,
            ts.merchtaxedamt,
            ts.merchtaxedcount,
            ts.merchnontaxedamt,
            ts.merchnontaxedcount
        from
            totals_summary ts
            join employee e on e.cashiernum = ts.cashiernum
        where
            businessdate like '$pbdate%'
	ENDQUERY
    
    $dbh = openODBCDriver($dsn); 		
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
 
    if (recordCount($tbl_ref)) {
        my @format = (
            "",
            "",
            "",
            "",
            {'align' => 'Right', 'currency' => ''},        
            {'align' => 'Right'},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right'},
        ); 
        # Get the totals
        
        my @fieldsum = ('Total:', '', '', '', 0, 0, 0, 0);
        for my $datarows (1 .. $#$tbl_ref)  {
            my $thisrow = @$tbl_ref[$datarows];

            for my $field (4, 5, 6, 7)  {
                $fieldsum[$field] += trim($$thisrow[$field]);
            }                       
        }

        push @$tbl_ref, \@fieldsum;   
        print $q->h2("Summary from the last business date: $pbdate");
        printResult_arrayref2($tbl_ref, '', \@format); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }
    $dbh->disconnect;
    StandardFooter(); 
 } elsif ($tool eq "SKU") {
 	# Tool: SKU
    my @ltm = localtime();
    my $year = sprintf("%02d", $ltm[5]-100);        
    my @problem_sku=();
    my $count=0;
    my $validskusrc="c:/mlink/akron/validskus.txt";
    my @sku_list=();
    my %skuhash=();
    my %storehash=();
    my $sku="";
    my $barcode="";
    my $desc="";
    my $price="";
    my $qty="";
    my $ext="";
    my @tmp=();
    my $line="";
    my @header=(
        "SKU",
        "DESC",        
        "QTY",
        "PRICE",
        "ExtPrice",
        "BARCODE"
    );
	#report_it("Checking Villages SKU's");	

    open (INPUT,"$validskusrc");
    @sku_list=(<INPUT>);
    foreach $line (@sku_list) {
        @tmp=split(/,/,$line);
        $sku=$tmp[0];
        $barcode=$tmp[1];        
        $skuhash{$sku}=$barcode;
    }
    close INPUT;
    	# Find the length of the SKU/PLU numbers for Villages items
	$query = dequote(<< "	ENDQUERY");
        select p.plunum,ic.qtycurrent,p.pludesc,p.retailprice,cr.xrefnum
        from plu p join inventory_current ic on p.plunum=ic.plunum
        left outer join plu_cross_ref cr on p.plunum=cr.plunum
        where p.vendorid like '01' order by p.plunum  
	ENDQUERY
    my $dbh = openODBCDriver($dsn); 		
	$sth = $dbh->prepare($query);
	$sth->execute();
 
	while (@tmp=$sth->fetchrow_array()) {
        # Unless the sku is found in the valid sku hash, put it on the problem list.
        $sku=$tmp[0];
        $qty=$tmp[1];
        $qty=sprintf("%d",$qty);
        $desc=$tmp[2];
        $price=$tmp[3];            
        $price=sprintf("%.2f",$price);
        $ext=($price * $qty);
        $ext=sprintf("%.2f",$ext);
        $barcode=$tmp[4]; 
        unless ($barcode) {
            $barcode="UNKNOWN";
        }
        $storehash{$sku}=$barcode;          
        unless ($skuhash{$sku}) {  
            # This is not a known SKU.
            # Check to see if it is a damaged (93) or a sample (92)
            next if ($sku =~ /^920/);
            next if ($sku =~ /^930/);
            next if ($sku =~ /^92$year/);
            next if ($sku =~ /^93$year/);            
            my @line=(
                "$sku",                
                "$desc",
                "$qty",
                "\$$price",
                "\$$ext",
                "$barcode"
            );
    		push (@problem_sku,\@line);            
            $count++;           
        }
	}		    
    if ($count) {
        # Print the table of problem skus here
        print $q->b("Unknown Villages SKUs");
        printTable(\@header,\@problem_sku);
    }
    $count=0; 
    @problem_sku=();
    @header=(
        "SKU",
        "Store BarCode",
        "Akron BarCode",
    );
    # Check for incorrect SKU - Barcode combinations
    foreach my $key (sort keys(%storehash)) {
        if (($skuhash{$key}) && ($storehash{$key})) {
            # If this sku appears in both Akron and here in the store, compare the barcode
            unless ($skuhash{$key} eq $storehash{$key}) {
                my @line=(
                    "$key",
                    "$storehash{$key}",
                    "$skuhash{$key}"
                    );
                push(@problem_sku,\@line);                
                $count++;
            }
        }
        
    }
    if ($count) {
        # Print the table of problem skus here
        print "<br />\n";
        print $q->b("Incorrect Bar Codes");        
        printTable(\@header,\@problem_sku);
    }    
    $dbh->disconnect;
	StandardFooter();    
    
 } elsif ($tool eq "TCRPT") {
 	# Traffic Count Report  
	$query = dequote(<< "	ENDQUERY");
        select * from eldertraffic order by starttime desc
	ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my $tbl_ref2= [];
    if (recordCount($tbl_ref)) {
        for my $datarows (0 .. 500) {
            my $thisrow = @$tbl_ref[$datarows];            
            push @$tbl_ref2, $thisrow;
        }        
        print $q->p("elderTraffic Table: 500 lastest entries");
        printResult_arrayref2($tbl_ref2); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }
    $dbh->disconnect;
	StandardFooter(); 
 } elsif ($tool eq "TCADJRPT") {
 	# Traffic Count Adjust Report  
    # First check that the traffic count adjust table even exists
    
    my $found=0;
	$query = dequote(<< "	ENDQUERY");
        select count(*) from dbo.sysobjects where name like 'eldertrafficadj' and type = 'U'        
	ENDQUERY
    $sth = $dbh->prepare($query);
 
    $sth->execute();

    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0] == 1) {
            $found++;            
        }
    }
    
    unless ($found) {
        print $q->p("No traffic count adjust table found.");
        $dbh->disconnect;
        StandardFooter(); 
        exit;
    }
	$query = dequote(<< "	ENDQUERY");
        select * from eldertrafficadj order by starttime desc
	ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my $tbl_ref2= [];
    if (recordCount($tbl_ref)) {
        for my $datarows (0 .. 500) {
            my $thisrow = @$tbl_ref[$datarows];            
            push @$tbl_ref2, $thisrow;
        }        
        print $q->p("elderTrafficAdj Table: 500 lastest entries");
        printResult_arrayref2($tbl_ref2); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }
    $dbh->disconnect;
	StandardFooter();      
 } elsif ($tool eq "MONITORO") {
 	# Tool: MONITOR
 
	my @log_data=();
	my $line="";
	my @tmp=();
	my $log="";
    my $cmd="c:/mlink/akron/monitor.pl";
    if (-e $cmd) {
        @log_data=`c:/mlink/akron/monitor.pl --debug --sku`;
    } else {
        push(@log_data,"ERROR - $cmd not found");
    }
	
 

    print $q->h2("Villages SKU check ");
    print "<pre>";
	foreach my $line (@log_data) {
        print "$line";	    
	}
	print "</pre><br />\n";
 
	print "<br />\n";
	StandardFooter();
} elsif ($tool eq "EVENTLOG") {  
	my @log_data=();
	my $line="";
	my @tmp=();
    my @eventlogs=(
        "Application",
        "System",
    );
    my @header = (
        "DateTime",  
        "Source",
        "Type",        
        "Note"        
    );
        
    foreach my $log (@eventlogs) {
        my $cmd = "psloglist -n 100 -s $log"; 
        my @array_to_print=\@header;
        @log_data=`$cmd`;
        if (@log_data > -1) {
            foreach $line (@log_data) {
                chomp($line);
                if ($line =~ /$log/) { 
                    next if ($line =~ /$log log/);                    
                    @tmp=split(/,/,$line);
                    my $source=$tmp[2];
                    my $type=$tmp[3];
                    my $event;                   
                    my $note=$tmp[8];
                    my $date=$tmp[5];
                    my @array=(
                        "$date",
                        "$source",
                        "$type",                      
                        "$note"
                    );
                    push(@array_to_print,\@array);
                }
            }           
            print $q->p("$log event log");
            printResult_arrayref2(\@array_to_print,'','','');
        } else {
            print "Found no data in $log event log.<br />";
        }
        
     
    }

 	StandardFooter();
} elsif ($tool eq "BOOTLOG") {
 	# Tool: BOOTLOG
 
	my @log_data=();
	my $line="";
	my @tmp=();
	my $log="";
 
	@log_data=`psloglist -i 6005 -n 10`;
 

   print $q->h2("System Reboot Log for $sname");
   print "<pre>";
	foreach my $line (@log_data) {
	   if ($line =~ /Time:/) {
	      $_=$line;
	      @tmp=split;
	      print "$tmp[0] $tmp[1] $tmp[2] $tmp[3]\n";
	   }
	}
	print "</pre><br />\n";
 
	print "<br />\n";
	StandardFooter();

} elsif ($tool eq "MONITOR") {
    print $q->h3("Monitor Tool");
    # Present a form to permit the running of the monitor script
 
    my $tool="c:/mlink/akron/monitor.pl";
    my $tool_options=$q->option({-value=>''}, " ");
    if (-f $tool) {
        # Get the options available
        open (TOOL,"$tool");
        my @tool_info=(<TOOL>);
        close TOOL;
        my $target="GetOptions";
        my $found_target=0;
		my %option_hash;
        foreach my $t (@tool_info) {
            chomp($t);
            if ($t =~ /GetOptions/) {
                $found_target=1;
            }
            if ($t =~ /\);/) {
                $found_target=0;
            }  
            if ($found_target) {
                # Parse the options
                next unless ($t =~ /\=>/);                
                my @tmp=split(/"/,$t);  
				my $option=$tmp[1];				
				# Some of these options have a ":" afterwards
				@tmp=split(/:/,$option);
				$option=$tmp[0];				
				# Some of these options have a "=" afterwards
				@tmp=split(/=/,$option);
				$option=$tmp[0];				
				$option_hash{$option}=1;
                
            }
        }
		# Add the options
		foreach my $option (sort keys(%option_hash)) {
			$tool_options .= $q->option({-value=>$option}, "$option");
		}
    }
 
 
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"MONITOR_EXE"});
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:"))); 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Report Option:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"report_selection", -size=>"1"}), $tool_options)
              );  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Report Option Argument:"),
            $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->input({-type=>'text', -name=>"report_argument", -size=>"5" })));   
    
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Second Report Option:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"report_selection2", -size=>"1"}), $tool_options)
              );  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Second Report Option Argument:"),
            $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->input({-type=>'text', -name=>"report_argument2", -size=>"5" })));              
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );
      
    print $q->end_table(),"\n";

    print $q->end_form(), "\n"; 
  
	StandardFooter();
} elsif ($tool eq "MONITOR_EXE") {
    my $report_selection= ($q->param("report_selection"))[0]; 
    my $report_argument= ($q->param("report_argument"))[0]; 
    my $report_selection2= ($q->param("report_selection2"))[0]; 
    my $report_argument2= ($q->param("report_argument2"))[0];     
    print $q->h3("Monitor: $report_selection $report_argument");
	my @log_data=();
	my $line="";
	my @tmp=();
	my $log="";
    my $tool="c:/mlink/akron/monitor.pl";
    my $cmd="$tool -$report_selection";
    if ($report_argument) {
        $cmd.="=$report_argument";
    }
    if ($report_selection2) {
        $cmd.=" -$report_selection2";
    }
    if ($report_argument2) {
        $cmd.="=$report_argument2";
    }    
    
	@log_data=`$cmd`;
 

  
   print "<pre>";
	foreach my $line (@log_data) {	   
        print "$line";	   
	}
	print "</pre><br />\n";
 
	print "<br />\n";
	StandardFooter(); 
} elsif ($tool eq "BACKUP_TOOL") {
    print $q->h3("Backup Tool:  Set a backup flag.");    
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"tool", -value=>"BACKUP_EXE"});
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";              
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({  -value=>"   Go!   "}))
              );
    print $q->end_table(),"\n";      
    print $q->end_form(), "\n"; 
  
	StandardFooter();
} elsif ($tool eq "BACKUP_EXE") {
    
    print $q->h3("Backup:  ");
	my $backup_attempt	= getConfig($dbh,"lastBackupAttempt"); 
	print $q->p("Backup attempt count: $backup_attempt");
	
    my $backup_flag="/temp/eod_complete.flg";
	open(FLAG,">$backup_flag");
	print FLAG "Backup Request via i_toolshed.pl";
	close FLAG;
	if (-f $backup_flag) {
		print $q->p("Successfully set the backup flag");
	} else {
		print $q->p("Failed to set the backup flag!");
	}
	StandardFooter(); 	
} elsif ($tool eq "MODEM") {
    # This tool along with the DIAL have been replaced by the i_tools_modem.pl script
     
} elsif ($tool eq "FILEVIEW") {
	print $q->h1("View a file");
    ##########################
    # Commonly viewed files    
    my $storenum = sprintf("%04d",getStoreNum($dbh));     
    my %file_hash = (
		'c:/mlink/akron/safmonitor.log' => "safMonitor Log",
		'c:/mlink/akron/windowsUpdates.log' => "Windows Updates Log",
		'c:/mlink/akron/postools.log' => "Postools Log",
        'c:/mlink/akron/monitor.log' => "Monitor Log",
		'c:/temp/monitor.rpt' => "Monitor Report",
        'c:/mlink/akron/backup.log' =>  "Backup Log",
		'c:/temp/backup_report.rpt' =>  "Backup Report",
		'c:/mlink/akron/memoryStatus.log' =>  "Memory Status Log",
        "${xpsDir}/sdata/${storenum}0100.jrn" =>  "Current Register 1 Journal",
        "${xpsDir}/sdata/${storenum}0200.jrn" =>  "Current Register 2 Journal",
        "${xpsDir}/sdata/${storenum}0300.jrn" =>  "Current Register 3 Journal",
        "${xpsDir}/sdata/${storenum}0400.jrn" =>  "Current Register 4 Journal",
        "${xpsDir}/sdata/${storenum}0500.jrn" =>  "Current Register 5 Journal",
        "${xpsDir}/sdata/00${storenum}0100.jrn" =>  "Current Register 1 Journal",
        "${xpsDir}/sdata/00${storenum}0200.jrn" =>  "Current Register 2 Journal",
        "${xpsDir}/sdata/00${storenum}0300.jrn" =>  "Current Register 3 Journal",
        "${xpsDir}/sdata/00${storenum}0400.jrn" =>  "Current Register 4 Journal",
        "${xpsDir}/sdata/00${storenum}0500.jrn" =>  "Current Register 5 Journal",        
		'c:/mlink/akron/reg1_monitor.log' =>  "Register 1 Monitor Log",
		'c:/mlink/akron/reg2_monitor.log' =>  "Register 2 Monitor Log",
		'c:/mlink/akron/reg3_monitor.log' =>  "Register 3 Monitor Log",
		'c:/mlink/akron/reg4_monitor.log' =>  "Register 4 Monitor Log",
		'c:/mlink/akron/reg5_monitor.log' =>  "Register 5 Monitor Log",
		'c:/mlink/akron/reg1_EFT.log' =>  "Register 1 EFT Log",
		'c:/mlink/akron/reg2_EFT.log' =>  "Register 2 EFT Log",
		'c:/mlink/akron/reg3_EFT.log' =>  "Register 3 EFT Log",
		'c:/mlink/akron/reg4_EFT.log' =>  "Register 4 EFT Log",
		'c:/mlink/akron/reg5_EFT.log' =>  "Register 5 EFT Log",		
		'c:/mlink/akron/reg1_posmonitor.log' =>  "Register 1 POS Monitor Log",
		'c:/mlink/akron/reg2_posmonitor.log' =>  "Register 2 POS Monitor Log",
		'c:/mlink/akron/reg3_posmonitor.log' =>  "Register 3 POS Monitor Log",
		'c:/mlink/akron/reg4_posmonitor.log' =>  "Register 4 POS Monitor Log",
		'c:/mlink/akron/reg5_posmonitor.log' =>  "Register 5 POS Monitor Log",		
		'c:/mlink/akron/ws_monitor.log' =>  "WorkStation Monitor Log",
		'c:/mlink/akron/ephrata-asstmgr_monitor.log' =>  "WorkStation ephrata-asstmgr  Monitor Log",		
		'c:/mlink/akron/ephratadesign_monitor.log' =>  "WorkStation ephratadesign  Monitor Log",
		'c:/mlink/akron/eph-jdirks_monitor.log' =>  "WorkStation eph-jdirks  Monitor Log",
		'c:/mlink/akron/operationsmgr_monitor.log' =>  "WorkStation operationsmgr  Monitor Log",
		'c:/mlink/akron/eph-gm_monitor.log' =>  "WorkStation eph-gm  Monitor Log",		
        "${xpsDir}/logs/event.log" =>  "XPS Event Log",
        "${xpsDir}/eod.log" =>  "XPS End Of Day Log",
        "${xpsDir}/backupdb.log" =>  "XPS DB Backup Log",
        "${xpsDir}/XPS.log" =>  "XPS Log",
		"${xpsDir}/logs/XPS.log" =>  "XPS Log",
		"${xpsDir}/logs/XPS.log.1" =>  "XPS Log 1",
        "${xpsDir}/crsrv.log" =>  "XPS crsrv Log",
		"${xpsDir}/logs/crsrv.log" =>  "XPS crsrv Log",         
        "${xpsDir}/convert.log" =>  "XPS convert Log",
        "${xpsDir}/xcpt.log" =>  "XPS xcpt Log",
		"c:/program files/SAP/transnet/logs/log.txt" =>  "Transnet Log",
		
    );
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"tool", -value=>"FILEVIEW2"});

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "File Requested:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'text', -name=>"filereq", -size=>"30" })));
						
    foreach my $key (sort keys(%file_hash)) {         
        if (-f $key) {
            # If we can find the file, show it as an option
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                           $q->input({-type=>"radio", -name=>"filereq2", -value=>"$key"}) .
                           $q->font({-size=>2}, "$file_hash{$key}")
                          ));
 
                           
        }
    }
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	StandardFooter();
} elsif ($tool eq "FILEVIEW2") {
    my $filereq= ($q->param("filereq"))[0]; 
    unless ($filereq) {
        $filereq= ($q->param("filereq2"))[0]; 
    }
 
    #$filereq =~ s/\//\\/g;
	if (-e "$filereq") {
        print $q->p("Viewing $filereq");
        ElderLog($logfile,"Found $filereq");
		open(REPORT,"$filereq");
		@report=(<REPORT>);
		close REPORT;
		#print "--file start--�";
 
		foreach $line (@report) {
			print "$line<br />";	
			#print "�";
 	
		}	
		#print "--file end--�";
	} else {
        ElderLog($logfile,"dif not find requested $filereq");
        #print "--file start--�";
		print "Did not find requested $filereq<br />";
        #print "--file end--�";
	}    
    StandardFooter();
####################	
} elsif ($tool eq "GETFILE") {

    
	if (-e "$reportreq") {
        ElderLog($logfile,"Found $reportreq");
		open(REPORT,"$reportreq");
		@report=(<REPORT>);
		close REPORT;
		print "--file start--�";
 
		foreach $line (@report) {
			print "$line";	
			print "�";
 	
		}	
		print "--file end--�";
	} else {
        ElderLog($logfile,"dif not find requested $reportreq");
        print "--file start--�";
		print "Did not find requested $reportreq �";
        print "--file end--�";
	}
	
} elsif ($tool eq "RPT") {
 
    if ($reportreq eq "backup_report") {
    $report_file="c:/temp/backup_report.rpt";
    } elsif ($reportreq eq "monitor_report") {
        $report_file="c:/temp/monitor.rpt";
    } elsif ($reportreq eq "sftp_report") {
        $report_file="c:/mlink/akron/posbackup.log";        
    } else {		
        print $q->p("Report request $reportreq unknown ");		
    }

	if (-e "$report_file") {
		open(REPORT,"$report_file");
		@report=(<REPORT>);
		close REPORT;		
		print $q->p("$reportreq");
		foreach $line (@report) {
			print "$line";
			print "<br />\n";
		}	
	} else {
		print $q->p("Did not find requested $reportreq");
	}

   StandardFooter();
} elsif ($tool eq "DIR") {
	print $q->h1("Directory Listing");
##########################
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"tool", -value=>"DIR2"});

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	#print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Directory Requested:")));

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Directory Requested:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'text', -name=>"dirreq", -size=>"12", -value=>"c:/polling"}),

                         )
                  );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	StandardFooter();
####################	
} elsif ($tool eq "DIR2") {
    my $dirreq= ($q->param("dirreq"))[0];    
    unless ($dirreq) {
        $dirreq='c:/temp';
    }      
	print $q->h1("Directory Listing of $dirreq");
	
	my @dir_listing=();
	if (-e $dirreq) {        
		$dirreq =~ s/\//\\/g;          
		@dir_listing=`dir "$dirreq"`;
		print "<pre>";
		foreach my $d (@dir_listing) {
			# remove "<'s" and ">'s"
			$d=~ s/<//g;
			$d=~ s/>//g;
			print "$d<br />";
		}
		print "</pre><br />";
	} else {
		print $q->p("$dirreq not found");
	}
 
	StandardFooter();

} elsif ($tool eq "GETQRY") {
	# This is to return a remote query to the database
	$sth="";
	my @tmp=();
	unless ($queryreq eq "none") {
        ElderLog($logfile, "Query requested" ); 
		# Using � as an eol character. (alt 230)

		print "query ($queryreq)�";
        ElderLog($logfile, "Query: ($queryreq)" ); 
        print "--file start--�";
        my $line_count=0;
        my $dbh = openODBCDriver($dsn); 
		$sth = $dbh->prepare($queryreq);
		if ($sth) {
			$sth->execute();
	
			while (@tmp=$sth->fetchrow_array()) {	
				print "@tmp";
				print "�";
                $line_count++;
			}		
            print "$line_count results returned�";
			print "--file end--�";
		} else {
			print "Failed to prepare query�";
            ElderLog($logfile, "Error: Failed to prepare query" );             
		}
        $dbh->disconnect;
	} else { 
		print "Did not find any query requested�";
        ElderLog($logfile, "Error: Did not find any query requested" ); 
	}

} elsif ($tool eq "RESETPC") {
	print $q->h1("Reset pcAnywhere");
	$command="/mlink/akron/postools.pl -pc";    
    run_n_print($command);
	StandardFooter();   
    
} elsif ($tool eq "RESETSAF") {
	print $q->h1("Reset Store and Forward File");
	$command="/mlink/akron/postools.pl -saf";    
    run_n_print($command);
	StandardFooter();
} elsif ($tool eq "SKIPDOWNLOAD") {
	print $q->h1("Set the Skip Download Flag");
	my $skip_download_flag="/temp/skip_download.flg";		
	open(FLAG, ">$skip_download_flag");
	close FLAG;		
	StandardFooter();		
} elsif ($tool eq "RESTARTEDNA") {
	print $q->h1("Setting Restart Request Flag...");
	my $reboot_requested_flag="c:/temp/reboot_request.flg";
	
	if (-f $reboot_requested_flag) {
		print $q->p("There is already a request to reboot.");
	} else {
		print $q->p("Setting $reboot_requested_flag");
		open(FLAG, ">$reboot_requested_flag");
		close FLAG;	
		if (-f $reboot_requested_flag) {	
			print $q->p("Reboot Request Flag Set");
			print $q->p("The Edna server should shutdown and reboot tomorrow morning sometime after polling has completed.");
		} else {
			print $q->p("Error: Failed to set Reboot Request Flag");
		}		
	} 	
	StandardFooter();	
} elsif ($tool eq "RESTARTBACKUP") {
	print $q->h1("Setting eod_complete Flag...");
	my $backup_flag="c:/temp/eod_complete.flg";
	
	if (-f $backup_flag) {
		print $q->p("There is already an eod_complete flag set.");
	} else {
		print $q->p("Setting $backup_flag");
		open(FLAG, ">$backup_flag");
		close FLAG;	
		if (-f $backup_flag) {	
			print $q->p("eod_complete Flag Set");
			print $q->p("The backup attempts will restart at the top of the hour.");
		} else {
			print $q->p("Error: Failed to set eod_complete Flag");
		}		
	} 	
	StandardFooter();		
} elsif ($tool eq "REMOVEEODCOMPLETE") {
	print $q->h1("Removing eod_complete Flag...");
	my $backup_flag="c:/temp/eod_complete.flg";	
	if (-f $backup_flag) {
		print $q->p("Removing $backup_flag...");
		unlink $backup_flag;
		sleep 1;
		if (-f $backup_flag) {		
			print $q->p("ERROR: Failed to remove $backup_flag.");
		} else {
			print $q->p("$backup_flag has been removed.");
		}
	} else {
		print $q->p("$backup_flag was not found.");
		
	} 	
	StandardFooter();	
	
	
} elsif ($tool eq "RESETPO") {
	print $q->h1("Reset Purchase Order back to Pending");
    print $q->p("The Purchase Order should be voided via Store Manager before setting it back to pending.");
 
    print $q->h4("Be certain to exit the purchase order in Store Manager before proceeding!");
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"tool", -value=>"PONUM"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Enter the Txn/Transaction Number (rectransnum) to reset:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"txnnum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
 		  
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	StandardFooter();   
} elsif ($tool eq "PONUM") {
	print $q->h3("Resetting purchase order for transaction $txnnum back to pending");
	$sth="";
	my $extCost;	
	# Get the total price
	$query="
	select 
         sum(qty * cost) 
	from 
		purchase_order_dtl  
	where 
        potransnum = $txnnum";	
    my $dbh = openODBCDriver($dsn); 
	$sth = $dbh->prepare($query);			
	if ($sth) {
		$sth->execute();
		while (my @tmp=$sth->fetchrow_array()) {			
			$extCost=sprintf("%.2f",$tmp[0]);
		}
	}
	
	$query="
	select 
        po.storenum,po.txnStatus,po.txnDate, po.poNum,po.orderDate,po.note,po.ordervalue,v.vendorname 
	from 
        purchase_order po
        join vendor v on po.vendorNum = v.vendorid
	where 
        potransnum = $txnnum";
	my $storenum="";
	my $txnStatus="";
	my $txnDate="";
	my $documentNum="";
	my $poNum="";
	my $orderDate="";
	my $note="";
	my $cost="";
	my $SKUCount="";    
	my $vendor="";
	my $qtyReceived="";
    my $qtyOrdered;
    my $shipment_cost;
    my $price;
    my $shortover;
	$sth = $dbh->prepare($query);	

	####################	
	if ($sth) {
		$sth->execute();
		while (my @tmp=$sth->fetchrow_array()) {	
			$storenum="$tmp[0]";
			if ($tmp[1] eq "P") {
				$txnStatus="PENDING";				
			} elsif ($tmp[1] eq "R")  {
				$txnStatus="RECEIVED";
			} elsif ($tmp[1] eq "I") {
				$txnStatus="ISSUED";			
			} elsif ($tmp[1] eq "V") {
				$txnStatus="VOIDED";
			}			
			$txnDate="$tmp[2]";
		 
			$poNum="$tmp[3]";
			$orderDate="$tmp[4]";
			$note="$tmp[5]";
			 
			$vendor="$tmp[7]";			 
			
		}
		if (($txnStatus eq "VOIDED")||($txnStatus eq "RECEIVED")) {
			$query="select sum(qty), sum(cost) ,count(plunum)
            from purchase_order_dtl where potransnum = $txnnum";
			$sth = $dbh->prepare($query);
			$sth->execute();
			while (my @tmp=$sth->fetchrow_array()) {					 
                $qtyOrdered=sprintf("%d",$tmp[0]);      
                $cost=sprintf("%.2f",$tmp[1]);
                $SKUCount=$tmp[2];
			}
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Order Information:")));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$storenum"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Vendor:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$vendor"));				
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Status:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnStatus"));
		 
		 
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Order Date:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$orderDate"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Note:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$note"));
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Cost:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "\$$cost"));					
     				
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Ext. Cost:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "\$$extCost"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Line Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$SKUCount"));		 			
                       
	
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Qty Ordered:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$qtyOrdered"));	
             
         
			print $q->end_table(),"\n";
          
			print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"report", -value=>"PONUM_REVERT"});
			print $q->input({-type=>'hidden', -name=>"txnnum", -value=>"$txnnum"});
			print $q->submit({-value=>"Revert to Pending"});
			print $q->end_form(), "\n";	

			####################
 	        
		} else {
			print $q->h3("Transaction $txnnum has a status of $txnStatus");
			print $q->h3("It does not need to be set to pending.");		
		}		
	} else {
		print $q->p("Error querying for $txnnum");
	}	
    $dbh->disconnect;
	StandardFooter(); 	    
} elsif ($tool eq "RESETRECDOC") {
	print $q->h1("Reset Receiving Document back to Pending");
    print $q->p("The document should be voided via Store Manager before setting it back to pending.");
 
    print $q->h4("Be certain to exit the receiving document in Store Manager before proceeding!");
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"tool", -value=>"RECDOC"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Enter the Txn/Transaction Number (rectransnum) to reset:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"txnnum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	#print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    #               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
    #                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
    #              );				  
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	StandardFooter();  
} elsif ($tool eq "RECDOC") {
	print $q->h3("Resetting receiving document for transaction $txnnum back to pending");
	$sth="";
	my $extPrice;	
	# Get the total price
	$query="
	select 
         sum(qtyReceived * retailPrice) 
	from 
		receiving_dtl  
	where 
        rectransnum = $txnnum";	
    my $dbh = openODBCDriver($dsn); 
	$sth = $dbh->prepare($query);			
	if ($sth) {
		$sth->execute();
		while (my @tmp=$sth->fetchrow_array()) {			
			$extPrice=sprintf("%.2f",$tmp[0]);
		}
	}
	
	$query="
	select 
        r.storenum,r.txnStatus,r.txnDate,r.documentNum,r.poNum,r.receiveDate,r.note,r.cost,v.vendorname 
	from 
        receiving r		
        join vendor v on r.vendorNum = v.vendorid
	where 
        rectransnum = $txnnum";
	my $storenum="";
	my $txnStatus="";
	my $txnDate="";
	my $documentNum="";
	my $poNum="";
	my $receiveDate="";
	my $note="";
	my $cost="";
	my $vendor="";
	my $qtyReceived="";
    my $qtyOrdered;
    my $shipment_cost;
    my $price;
    my $shortover;
	$sth = $dbh->prepare($query);	

	####################	
	if ($sth) {
		$sth->execute();
		while (my @tmp=$sth->fetchrow_array()) {	
			$storenum="$tmp[0]";
			if ($tmp[1] eq "P") {
				$txnStatus="PENDING";				
			} elsif ($tmp[1] eq "R")  {
				$txnStatus="RECEIVED";
			} elsif ($tmp[1] eq "I") {
				$txnStatus="ISSUED";			
			} elsif ($tmp[1] eq "V") {
				$txnStatus="VOIDED";
			}			
			$txnDate="$tmp[2]";
			$documentNum="$tmp[3]";
			$poNum="$tmp[4]";
			$receiveDate="$tmp[5]";
			$note="$tmp[6]";
			$shipment_cost=sprintf("%.2f",$tmp[7]);	
			$vendor="$tmp[8]";			 
			
		}
		if (($txnStatus eq "VOIDED")||($txnStatus eq "RECEIVED")) {
			$query="select sum(qtyReceived),sum(qtyOrdered),sum(shortover),sum(retailPrice),sum(cost) 
            from receiving_dtl where rectransnum = $txnnum";
			$sth = $dbh->prepare($query);
			$sth->execute();
			while (my @tmp=$sth->fetchrow_array()) {	
				$qtyReceived=sprintf("%d",$tmp[0]);
                $qtyOrdered=sprintf("%d",$tmp[1]);
                $shortover=sprintf("%d",$tmp[2]);
                $price=sprintf("%.2f",$tmp[3]);
                $cost=sprintf("%.2f",$tmp[4]);
			}
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Order Information:")));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$storenum"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Vendor:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$vendor"));				
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Status:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnStatus"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Date:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnDate"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Document Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$documentNum"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Receive Date:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$receiveDate"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Note:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$note"));
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Cost:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "\$$cost"));					
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Price:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "\$$price"));					
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Ext. Cost:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "\$$shipment_cost"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Ext. Price:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "\$$extPrice"));				
                       
	
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Qty Ordered:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$qtyOrdered"));	
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Qty Received:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$qtyReceived"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Qty Short/Over:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$shortover"));                
         
			print $q->end_table(),"\n";
			if ($qtyReceived > 0) {
				print $q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=TXNDETAIL&txnnum=$txnnum", -target=>'_top'}, "See detail on Transaction $txnnum"); 				   
			}			
            if ($txnStatus eq "RECEIVED") {
                print $q->h3("Transaction $txnnum has a status of $txnStatus");
                print $q->h3("Are you CERTAIN that you do not wish to void it BEFORE you revert it to pending?");
                print $q->p("(If you wish to void it first, do so in Store Manager.  Otherwise, click the 'Revert to Pending' button below.)");
            }            
			print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"report", -value=>"RECDOC_REVERT"});
			print $q->input({-type=>'hidden', -name=>"txnnum", -value=>"$txnnum"});
			print $q->submit({-value=>"Revert to Pending"});
			print $q->end_form(), "\n";	

			####################
        #} elsif ($txnStatus eq "RECEIVED") {
        #    print $q->h3("Transaction $txnnum has a status of $txnStatus");
	    #   print $q->h3("It must first be voided if you indeed wish to revert it to pending.");	        
		} else {
			print $q->h3("Transaction $txnnum has a status of $txnStatus");
			print $q->h3("It does not need to be set to pending.");		
		}		
	} else {
		print $q->p("Error querying for $txnnum");
	}	
    $dbh->disconnect;
	StandardFooter(); 	
} elsif ($tool eq "TXNDETAIL") {
	use CGI::Pretty qw/:standard :html3 *table/;	
	my @row=();
	my $field="";
	my @header=(
		"SKU",
		"qtyOrdered",
		"qtyReceived",
		"short/over",
		"reason"
	);
	$query="select plunum,qtyOrdered,qtyReceived,shortover,reason from receiving_dtl where rectransnum = $txnnum and qtyReceived > 0";
    my $dbh = openODBCDriver($dsn); 
    $sth = $dbh->prepare($query);
	$sth->execute();

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0, -border=>'1'}), "\n";
	print $q->Tr(td({-class=>'tablehead', -nowarp=>undef},\@header));
	while (my @tmp=$sth->fetchrow_array()) {
		@row=();
		foreach $field (@tmp) {
	         if (($field =~ m/^\s$/) || ($field eq ""))
	         {
	            $field = "&nbsp;"
	         }
			push (@row,$field);
		}
		print Tr(td({-class=>'tableborder'},\@row));
	}	
	print $q->end_table(),"\n";
	
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"RECDOC_REVERT"});
	print $q->input({-type=>'hidden', -name=>"txnnum", -value=>"$txnnum"});
	print $q->submit({-value=>"Revert to Pending"});
	print $q->end_form(), "\n";	
	$dbh->disconnect;
	StandardFooter(); 
	
} elsif ($tool eq "RECDOC_REVERT") {
	print $q->h3("Resetting receiving document for transaction $txnnum back to pending");
	$sth="";
	$query = "update receiving set txnStatus='P',  transmitted = '', transnum='' where rectransnum = $txnnum";
    my $dbh = openODBCDriver($dsn); 
	$sth=$dbh->prepare($query);	
	if ($sth) {
		$sth->execute();
		my $rows=$sth->rows;
		if ($rows>=1) {
			print $q->p("Successfully reverted $txnnum back to pending in receiving");
			ElderLog($logfile, "Successfully reverted $txnnum back to pending in receiving" );               
		} else {    	
			print $q->p("Error updating receiving table!");
			ElderLog($logfile, "Error updating receiving table!" );         
		}		
	} else {
		print $q->p("Error updating receiving table!");
		ElderLog($logfile, "Error updating receiving table!" ); 
	}
	$query = "update receiving_dtl set qtyreceived=0, shortover=0, reason=null, backorder='N', qtyacc=0, qtyold=0, qtyaccold=null where rectransnum = $txnnum";
	$sth=$dbh->prepare($query);	
	if ($sth) {
		$sth->execute();
		my $rows=$sth->rows;
		if ($rows>=1) {
			print $q->p("Successfully reverted $txnnum back to pending in receiving_dtl");
			ElderLog($logfile, "Successfully reverted $txnnum back to pending in receiving_dtl" );               
		} else {    	
			print $q->p("Error updating receiving_dtl table!");
			ElderLog($logfile, "Error updating receiving_dtl table!" );         
		}			
	} else {
		print $q->p("Error updating receiving_dtl table!");
		ElderLog($logfile, "Error updating receiving_dtl table!" ); 
	}
	print $q->h2("DONE");
    $dbh->disconnect;
	StandardFooter(); 	
} elsif ($tool eq "PONUM_REVERT") {

	print $q->h3("Resetting purchase order for transaction $txnnum back to pending");
	$sth="";
	$query = "update purchase_order set txnStatus='P',  transmitted = '', transnum='' where potransnum = $txnnum";
    my $dbh = openODBCDriver($dsn); 
	$sth=$dbh->prepare($query);	
 
	if ($sth) {
		$sth->execute();
		my $rows=$sth->rows;
		if ($rows>=1) {
			print $q->p("Successfully reverted $txnnum back to pending in purchase_order");
			ElderLog($logfile, "Successfully reverted $txnnum back to pending in purchase_order" );               
		} else {    	
			print $q->p("Error updating purchase_order table!");
			ElderLog($logfile, "Error updating purchase_order table!" );         
		}		
	} else {
		print $q->p("Error updating purchase_order table!");
		ElderLog($logfile, "Error updating purchase_order table!" ); 
	}
=pod    
	$query = "update purchase_order_dtl set qtyreceived=0, shortover=0, reason=null, backorder='N', qtyacc=0, qtyold=0, qtyaccold=null where rectransnum = $txnnum";
	$sth=$dbh->prepare($query);	
	if ($sth) {
		$sth->execute();
	} else {
		print $q->p("Error updating purchase_order_dtl table!");
	}
=cut    
	print $q->h2("DONE");
    $dbh->disconnect;
	StandardFooter();     
} elsif ($tool eq "RESETXREFDOC") {
	print $q->h1("Reset Store Transfer back to Pending");
    print $q->p("The transfer should be voided via Store Manager before setting it back to pending.");
 
    print $q->h4("Be certain to exit the transfer in Store Manager before proceeding!");
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"tool", -value=>"XREFDOC"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Enter the Txn/Transaction Number (ststransnum) to reset:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"txnnum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	#print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    #               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
    #                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
    #              );				  
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	StandardFooter();  
} elsif ($tool eq "XREFDOC") {    
    my $override=($q->param("override"))[0]; 
	$sth="";
	$query="
	select 
        ststransnum,documentNum,txnStatus,transferType,txnDate,storenum,transferstore
	from 
        store_transfer 

	where 
        ststransnum = $txnnum";
	my $ststransnum="";
	my $documentNum="";    
	my $txnStatus="";
    my $transferType="";    
	my $txnDate="";	
	my $storenum="";
	my $transferstore="";
    my $qtyReceived;
 	my $found=0;
    my $dbh = openODBCDriver($dsn); 
	$sth = $dbh->prepare($query);	
    
	####################	
	if ($sth) {
		$sth->execute();
		while (my @tmp=$sth->fetchrow_array()) {	
			$ststransnum="$tmp[0]";
            $documentNum="$tmp[1]";
			if ($tmp[2] eq "P") {
				$txnStatus="PENDING";				
			} elsif ($tmp[2] eq "R")  {
				$txnStatus="RECEIVED";
			} elsif ($tmp[2] eq "C")  {
				$txnStatus="COMPLETED";                
			} elsif ($tmp[2] eq "I") {
				$txnStatus="ISSUED";			
			} elsif ($tmp[2] eq "V") {
				$txnStatus="VOIDED";
			}							
			$transferType="$tmp[3]";
            if ($transferType eq "O") {
                $transferType="Out";
            }
            if ($transferType eq "I") {
                $transferType="In";
            }
            $txnDate="$tmp[4]";
			$storenum="$tmp[5]";
			$transferstore="$tmp[6]";	
            $found=1;
		}
        if ($found) {
        	print $q->h3("Resetting store transfer with transaction $txnnum back to pending");
        } else {
            print $q->h3("Did not find a transfer document with a txn of $txnnum");
            print $q->p("The txn number is the ststransnum in the store_transfer table.");
            StandardFooter(); 
            #exit;
        }
		if (($txnStatus eq "VOIDED") || ($override)) {            
			$query="select count(qtyReceived) from store_transfer_dtl where ststransnum = $txnnum and qtyReceived > 0";
			$sth = $dbh->prepare($query);
			$sth->execute();
			while (my @tmp=$sth->fetchrow_array()) {	
				$qtyReceived="$tmp[0]";
			}
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Transfer Information:")));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$ststransnum"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Document Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$documentNum"));		
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Status:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnStatus"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Date:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnDate"));
            if ($transferType eq "In") {
                my $storename=$store_name_hash{$transferstore};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "From Transfer Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$transferstore - $storename"));
                $storename=$store_name_hash{$storenum};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "To Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$storenum - $storename (This store)"));                
            }
            if ($transferType eq "Out") {
                my $storename=$store_name_hash{$storenum};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "From Transfer Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$storenum - $storename (This store)"));
                $storename=$store_name_hash{$transferstore};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "To Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$transferstore - $storename"));                
            }            
 
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transfer Type:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$transferType"));     
              
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Number of SKU's with QtyReceived:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$qtyReceived"));	

			print $q->end_table(),"\n";
			if ($qtyReceived > 0) {
				print $q->a({-href=>"/cgi-bin/i_toolshed.pl?tool=XREFDETAIL&txnnum=$txnnum", -target=>'_top'}, "See detail on Transaction $txnnum"); 				   
			}			
			print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"report", -value=>"XREFDOC_REVERT"});
			print $q->input({-type=>'hidden', -name=>"txnnum", -value=>"$txnnum"});
			print $q->submit({-value=>"Revert to Pending"});
			print $q->end_form(), "\n";	
			####################
        } elsif ($txnStatus eq "RECEIVED") {
            print $q->h3("Transaction $txnnum has a status of $txnStatus");
			print $q->h3("It must first be voided if you indeed wish to revert it to pending.");	        
		} else {
			print $q->h3("Transaction $txnnum has a status of $txnStatus");
			print $q->h3("It does not need to be set to pending.");	
            print $q->p("If you are CERTAIN that you wish to set it back to pending, you can override it below:");            
            print $q->p(
            $q->a({-href=>"$scriptname?override=1&txnnum=$txnnum&report=XREFDOC", -class=>"button1"}, "Override"));
		}		
	} else {
		print $q->p("Error querying for $txnnum");        
	}	
    $dbh->disconnect;
	StandardFooter(); 	
} elsif ($tool eq "XREFDETAIL") {
	use CGI::Pretty qw/:standard :html3 *table/;	
	my @row=();
	my $field="";
	my @header=(
		"SKU",
		"qty",
		"qtyReceived",
		"short/over"		
	);
	$query="select plunum, qty, qtyReceived,shortover from store_transfer_dtl where ststransnum = $txnnum and qtyReceived > 0";
	$sth = $dbh->prepare($query);
	$sth->execute();

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0, -border=>'1'}), "\n";
	print $q->Tr(td({-class=>'tablehead', -nowarp=>undef},\@header));
	while (my @tmp=$sth->fetchrow_array()) {
		@row=();
		foreach $field (@tmp) {
	         if (($field =~ m/^\s$/) || ($field eq "")) {
	            $field = "&nbsp;"
	         } else {
                $field=sprintf("%d",$field);
             }
			push (@row,$field);
		}
		print Tr(td({-class=>'tableborder'},\@row));
	}	
	print $q->end_table(),"\n";
	
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"XREFDOC_REVERT"});
	print $q->input({-type=>'hidden', -name=>"txnnum", -value=>"$txnnum"});
	print $q->submit({-value=>"Revert to Pending"});
	print $q->end_form(), "\n";	
	
	StandardFooter(); 
	
} elsif ($tool eq "XREFDOC_REVERT") {

	print $q->h3("Resetting store transfer for transaction $txnnum back to pending");
	$sth="";
	$query = "update store_transfer set txnStatus='P',  transnum='' where ststransnum = $txnnum";
	$sth=$dbh->prepare($query);	
    $sth->execute();
    my $rows=$sth->rows;
    if ($rows>=1) {
        print $q->p("Successfully reverted $txnnum back to pending in store_transfer");
        ElderLog($logfile, "Successfully reverted $txnnum back to pending" );               
    } else {    	
		print $q->p("Error updating store_transfer table!");
        ElderLog($logfile, "Error updating store_transfer table!" );         
	}
    $query="select * from store_transfer_dtl where ststransnum = $txnnum";
    $sth=$dbh->prepare($query);	
    $sth->execute();
    $rows=$sth->rows;
    if ($rows >=1) {
    	$query = "update store_transfer_dtl set qtyreceived=0, shortover=0  where ststransnum = $txnnum";        
        $sth=$dbh->prepare($query);	
        $sth->execute();
        my $rows=$sth->rows;
        if ($rows>=1) {
            print $q->p("Successfully reverted $txnnum back to pending in store_transfer_dtl table");
            ElderLog($logfile, "Successfully reverted $txnnum back to pending" );               
        } else {    	
            print $q->p("Error updating store_transfer_dtl table!");
            ElderLog($logfile, "Error updating store_transfer_dtl table!" );         
        }    
    }  

	print $q->h2("DONE");
	StandardFooter(); 	
} elsif ($tool eq "RECALLXREFDOC") {
	print $q->h1("Receive All for a Store Transfer");
    print $q->p("The transfer should be in a Pending state.");
 
    print $q->h4("Be certain to exit the transfer in Store Manager before proceeding!");
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"tool", -value=>"RECALLDOC"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Enter the Txn/Transaction Number (rectransnum) to effect receive all:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"txnnum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );			  
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	StandardFooter();   
} elsif ($tool eq "RECALLDOC") {
    my $txnnum= ($q->param("txnnum"))[0]; 
	$sth="";
	$query="
	select 
        ststransnum,documentNum,txnStatus,transferType,txnDate,storenum,transferstore
	from 
        store_transfer 
	where 
        ststransnum = $txnnum";
	my $ststransnum="";
	my $documentNum="";    
	my $txnStatus="";
    my $transferType="";    
	my $txnDate="";	
	my $storenum="";
	my $transferstore="";
    my $qtyReceived;
 	my $found=0;
	$sth = $dbh->prepare($query);	
    
	####################	
	if ($sth) {
		$sth->execute();
		while (my @tmp=$sth->fetchrow_array()) {	
			$ststransnum="$tmp[0]";
            $documentNum="$tmp[1]";
			if ($tmp[2] eq "P") {
				$txnStatus="PENDING";				
			} elsif ($tmp[2] eq "R")  {
				$txnStatus="RECEIVED";
			} elsif ($tmp[2] eq "C")  {
				$txnStatus="COMPLETED";                
			} elsif ($tmp[2] eq "I") {
				$txnStatus="ISSUED";			
			} elsif ($tmp[2] eq "V") {
				$txnStatus="VOIDED";
			}							
			$transferType="$tmp[3]";
            if ($transferType eq "O") {
                $transferType="Out";
            }
            if ($transferType eq "I") {
                $transferType="In";
            }
            $txnDate="$tmp[4]";
			$storenum="$tmp[5]";
			$transferstore="$tmp[6]";	
            $found=1;
		}
        if ($found) {
        	print $q->h3("Effecting 'receive all' for store transfer with transaction $txnnum");
        } else {
            print $q->h3("Did not find a transfer document with a txn of $txnnum");
            print $q->p("The txn number is the ststransnum in the store_transfer table.");
            StandardFooter(); 
            exit;
        }
		if ($txnStatus eq "PENDING") {            
			$query="select count(qtyReceived) from store_transfer_dtl where ststransnum = $txnnum and qtyReceived > 0";
			$sth = $dbh->prepare($query);
			$sth->execute();
			while (my @tmp=$sth->fetchrow_array()) {	
				$qtyReceived="$tmp[0]";
			}
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Transfer Information:")));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$ststransnum"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Document Number:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$documentNum"));		
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Status:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnStatus"));
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Date:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$txnDate"));
            if ($transferType eq "In") {
                my $storename=$store_name_hash{$transferstore};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "From Transfer Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$transferstore - $storename"));
                $storename=$store_name_hash{$storenum};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "To Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$storenum - $storename (This store)"));                
            }
            if ($transferType eq "Out") {
                my $storename=$store_name_hash{$storenum};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "From Transfer Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$storenum - $storename (This store)"));
                $storename=$store_name_hash{$transferstore};
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "To Store:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$transferstore - $storename"));                
            }            
 
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transfer Type:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$transferType"));     
              
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Number of SKU's with QtyReceived:"),
				$q->td({-class=>'tabledatanb', -align=>'left'}, "$qtyReceived"));	

			print $q->end_table(),"\n";
		
			print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"report", -value=>"EFFECT_RECALL"});
			print $q->input({-type=>'hidden', -name=>"txnnum", -value=>"$txnnum"});
			print $q->submit({-value=>"Receive All"});
			print $q->end_form(), "\n";	
			####################
        } else {
            print $q->h3("Transaction $txnnum has a status of $txnStatus");
			print $q->h3("It must be Pending before you can receive all.");	        
		}  		
	} else {
		print $q->p("Error querying for $txnnum");        
	}	
	StandardFooter(); 
} elsif ($tool eq "EFFECT_RECALL") {  
    my $txnnum= ($q->param("txnnum"))[0]; 
    print $q->h3("Receiving all items for transfer $txnnum");
    #query to fix the "update all" bug in store manager...
    my $query = "update store_transfer_dtl d 
            set d.qtyreceived=d.qty, d.shortover=0, d.retailprice = 
               (select p.retailprice from plu p where p.plunum = d.plunum)
            where ststransnum = $txnnum";
          
    my $sth = $dbh->prepare($query);
    $sth->execute();
    my $rows=$sth->rows;
    if ($rows>=1) {        
        print$q->p("Successfully updated transfer $txnnum" );               
    } else {
        print$q->p("ERROR: Failed to update transfer $txnnum" );               
    }    
    StandardFooter();   
} elsif ($tool eq "VENADJ") {

	print $q->h3("Adjusting Vendor Names");
    # See if the vendor is recognized
    my $count=0;
    my @header = (              
            "VendorID",
            "Found Vendor",
            "Approved Vendor for this ID"
        );    
    my @problem_vendor=(            
            \@header
        );
    my @fstring=();
    my @kstring=();  
    my %found_vendor_hash=(); 
    my @tmp=();

    $query = dequote(<< "    ENDQUERY");
        select vendorid,vendorname from vendor        
    ENDQUERY
        
    $sth = $dbh->prepare($query);
    $sth->execute();
    # Build a hash of found vendors
    while (@tmp=$sth->fetchrow_array()) {
        $found_vendor_hash{$tmp[0]}=$tmp[1];     
    }       
     
    foreach my $ven (sort keys(%vendor_hash)) {
        my $found=($found_vendor_hash{$ven});
        my $known=($vendor_hash{$ven});               
        next unless ($found);
        unless ($found eq $known) {  

            my @line = (
                "$ven",
                "$found_vendor_hash{$ven}",
                "$vendor_hash{$ven}"
            );
            push (@problem_vendor,\@line);                      
        
            
            $count++;                    
        }            
    }

    if ($count) { 

        print $q->p("The following vendors ID's are in a restricted range. 
        Their name descriptions should be changed to match the description in the \"Approved Vendor\" column.  
        (Vendor numbers from 01 to 19 are restricted to specific vendors)");
        my @links = ( '/cgi-bin/i_toolshed.pl?report=VENADJ2&vendor=$_' );        
        TTV::cgiutilities::printResult_arrayref2(\@problem_vendor, \@links, ''); 
              
        #printTable(\@header,\@problem_vendor); 
        
    } else { 
        print $q->p(" No invalid vendor ID's in restricted range detected.<br />\n"); 
    }         
	StandardFooter();    
} elsif ($tool eq "VENADJ2") {
    my $vendor= ($q->param("vendor"))[0];
    my %found_vendor_hash=();
	print $q->h3("Adjusting Vendor Names");
    
    $query = dequote(<< "    ENDQUERY");
        select vendorid,vendorname from vendor        
    ENDQUERY
        
    $sth = $dbh->prepare($query);
    $sth->execute();
    # Build a hash of found vendors
    while (my @tmp=$sth->fetchrow_array()) {
        $found_vendor_hash{$tmp[0]}=$tmp[1];     
    }      
         
    my $vendorname=$vendor_hash{$vendor};
    
    print $q->p("Adjusting vendor number: $vendor");
    print $q->p("Current name: \'$found_vendor_hash{$vendor}\'");
    print $q->p("Approved name: \'$vendorname\'");

    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"VENADJ3"});
    print $q->input({-type=>"hidden", -name=>"vendor", -value=>"$vendor"});
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );


    print $q->end_form(), "\n";
    
    StandardFooter(); 
} elsif ($tool eq "VENADJ3") {
    
    my $vendor= ($q->param("vendor"))[0];
    my $vendorname=$vendor_hash{$vendor};
    # In case there are any single quotes in the vendor's name:
    $vendorname =~ s/'/''/g;
    print $q->p("Adjusting vendor number: $vendor $vendorname");  
    $query = dequote(<< "    ENDQUERY");        
        update vendor set vendorname = '$vendorname' 
        where vendorid = '$vendor'
    ENDQUERY
        
    if ($dbh->do($query)) {
        print $q->h4("Done");
    } else {
        print $q->p("ERROR: $dbh->errstr");
    }    
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=VENADJ", -target=>'_top'}, "Back to Vendor Adjust");   
    StandardFooter(); 
    
} elsif ($tool eq "SKUSPACE") {
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_SPACE.log";
    print $q->h3("This script is still being developed - use with care!");
	print $q->h2("Looking for SKUs with spaces");  
    ElderLog($logfile, "Looking for SKUs with spaces" );
    # First check that no SKUs start or end with "SPACE" as a result of previous use of this tool
    my @sku_w_spaces=();    # This array will hold arrays of sku and table info.
    my $table;
    my $count1=0;    
    foreach $table (@table_list) {      
    	$query = dequote(<< "        ENDQUERY");
            select plunum from $table 
            where 
                plunum like '%SPACE'            
            or
                plunum like 'SPACE%'            
            order by plunum
        ENDQUERY
        	
    	$sth = $dbh->prepare($query);
    	$sth->execute();
     
    	while (my @tmp=$sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my @entry=(
            "$sku",
            "$table",
            "\'$sku\'"
            );
            my $found=0;
            foreach my $sref (@sku_w_spaces) {
                if (($$sref[0] eq $sku) && ($$sref[1] eq $table)) {            
                    $found=1;
                }                 
            }     
            unless ($found) {
                # Don't add it to the list if it is already there
                push(@sku_w_spaces,\@entry);            
                $count1++;             
            }
            
        } 
    }      
    if ($count1) {
        my @skuspaceheader=(
            "SKU",
            "Table",
            "SKU Display"
        );    
        my @table_to_display=\@skuspaceheader;
        foreach my $skuref (@sku_w_spaces) {
            my $sku=$$skuref[0];  
            my $table=$$skuref[1];
            my $display=$$skuref[2];                  
            push(@table_to_display,$skuref);
        }
        if ($#table_to_display > 0) {
            # Display the SKUs with SPACE 
            print $q->h3("NOTE: These SKUs need to be taken care of yet!");
            TTV::cgiutilities::printResult_arrayref2(\@table_to_display); 
        }         
    }
    # Check for  SKUs with spaces as the beginning or the end.  We need to check through all the tables where it might be.  If 
    # we find a SKU with a space and the end, make certain that there is not the same SKU without a space at the
    # end.  If there is not, then it is safe to rename SKU to delete the space at the end.
    
    @sku_w_spaces=();    # This array will hold arrays of sku and table info.
    
    $count1=0;    
    foreach $table (@table_list) {      
    	$query = dequote(<< "        ENDQUERY");
            select plunum from $table 
            where 
                plunum like '% '            
            or
                plunum like ' %'            
            order by plunum
        ENDQUERY
        	
    	$sth = $dbh->prepare($query);
    	$sth->execute();
     
    	while (my @tmp=$sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my @entry=(
            "$sku",
            "$table",
            "\'$sku\'"
            );
            
            my $found=0;
            foreach my $sref (@sku_w_spaces) {
                if (($$sref[0] eq $sku) && ($$sref[1] eq $table)) {                                      
                    $found=1;
                }                 
            }     
            unless ($found) {
                # Don't add it to the list if it is already there
                push(@sku_w_spaces,\@entry);            
                $count1++;             
            }
            
        } 
    }      
    my @skuspaceheader=(
        "SKU",
        "Table",
        "SKU Display"
    );
    my @problem_sku_simple=(\@skuspaceheader);
    my @problem_sku_complex=(\@skuspaceheader);
    if ($count1) {
        # We found some skus with spaces.
        # Now, separate this list into two types:
        # 1) Simple problems - where the SKU without the space does not exist.  We can fix this be simply removing the space. 
        # 2) Complex problems - where the SKU without the space DOES exist.  We cannot just remove the space so we change 
        # the space to the word "SPACE".  This does not solve the problem but it does allow us to now separate the SKU with the space
        # from the SKU without the space.  This is the source of the problem since Store Manager does not distinguish between the two
        # very well.
                 
        foreach my $skuref (@sku_w_spaces) {
            my $count2=0;
            my $sku=$$skuref[0];  
 
            my $table=$$skuref[1];
            my $skip_query=0;
            my $newsku=$sku;            
            $newsku=~s/ //g;    # newsku is the old sku without the space                       
            foreach my $b (@table_listb) {
 
                # If the table is in table list b, multiple entries for plunum are acceptable.  It does not mean
                # we have a "Complex problem" as defined above.               
                if ($table eq $b) {
                    $skip_query=1;      # For tables on table_listb, we do this query and skip the one found later.
                    # multi_key refers to multiple key fields in the table.
                    # plunum in tables on the table_listb can have multiple entries only if their multi-key
                    # partner is different.  Get this multi-key partner from the hash
                    my $mkey=$table_multi_key{$table};                                        
                    my $multi_key_value;
 
                    if ($mkey) {
                        $query = dequote(<< "                        ENDQUERY");
                            select plunum,$mkey from $table where plunum = '$newsku'
                        ENDQUERY
                                                 
                        $sth = $dbh->prepare($query);
                        $sth->execute();                
                        while (my @tmp=$sth->fetchrow_array()) {                    
                            $multi_key_value=$tmp[1];                           
                        }
                        
                        $query = dequote(<< "                        ENDQUERY");
                            select 
                                plunum,
                                $mkey 
                            from 
                                $table 
                            where 
                                plunum like '$sku'
                                and plunum <> '$newsku'
                        ENDQUERY
                        
                        $sth = $dbh->prepare($query);
                        $sth->execute();                
                        while (my @tmp=$sth->fetchrow_array()) {                                                      
                            if ($multi_key_value eq $tmp[1]) { 
                                # We have an instance where we have the sku in with and without a space
                                # and it is associated with the same multi-key partner.  This means it is a 
                                # complex problem
                                print "Found ($newsku) in $table<br />";
                                $count2++; 
                            }  
                        }                           
                    }                                                                
                }
            }
           
            unless ($skip_query) {

                # First look for PLU
                #foreach $table (@table_list) {       
                    $query = dequote(<< "                    ENDQUERY");
                        select plunum from $table where plunum like '$newsku'
                    ENDQUERY
                                                 
                    $sth = $dbh->prepare($query);
                    $sth->execute();                
                    while (my @tmp=$sth->fetchrow_array()) {
                        next unless ($tmp[0]);
                        print "Found ($newsku) in $table<br />";
                        $count2++;  
                     
                    }
                #}                
            }
        
            if ($count2) {
                # Put this SKU on the complex list                              
                push(@problem_sku_complex,$skuref);
            } else {
                # Put this SKU on the simple list
                # it is safe to remove the space
                push(@problem_sku_simple,$skuref);
            }             
        }
  
        if ($#problem_sku_complex > 0) {
            # Sort the array by sku
            my $count=($#problem_sku_complex + 1);
            my @sorted=sort { $$a[0] <=> $$b[0] } @problem_sku_complex;
            print $q->h3("1. These SKUs have similar entries without spaces so this tool cannot fix them.");
            print $q->p("Select a SKU to replace the actual space with the word \"SPACE\"");
            ElderLog($logfile, "Found $count SKUs with spaces which also are entered without a space." );
            my @links = ( '','','/cgi-bin/i_toolshed.pl?report=SKUSPACE3&skuid=$_' );             
            TTV::cgiutilities::printResult_arrayref2(\@sorted, \@links, ''); 
        } else {
            print $q->h3("1. No SKUs which cannot easily be fixed.");
        }
        if ($#problem_sku_simple > 0) {
            my @sorted=sort { $$a[0] <=> $$b[0] } @problem_sku_simple;
            my $count=($#problem_sku_simple + 1);
            my $sku_list;
            my $table;
            foreach my $s (@sorted) {
                my $sku=$$s[0];
                $table=$$s[1];
                $sku_list.="$sku=$table,";
            }
            print $q->h3("2. These SKUs can easily be fixed.  ");
            if ($#sorted > 1) { 
                print $q->h3("Select a SKU to fix it or click 'Fix All SKUs' at the bottom.");
            } else {
               print $q->h3("Select the SKU to fix it");
            }
            ElderLog($logfile, "Found $count SKUs with spaces which are NOT also are entered without a space." );
            my @links = ( '','',"/cgi-bin/i_toolshed.pl?report=SKUSPACE2&table=$table&skuid=".'$_' );                    
            TTV::cgiutilities::printResult_arrayref2(\@sorted, \@links, ''); 
            if ($#sorted > 1) {                
                print $q->p(
                $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUSPACE2&fixall=$sku_list", -class=>"button1"}, "Fix All SKUs")
                );
            }
        } else {
            print $q->h3("2. No SKUs which can be easily fixed.");
        }        
        
    } else {
        # Check if there are any skus with the word SPACE at the end or beginning
        @skuspaceheader=(
            "SKU",
            "Table",
        );
        @sku_w_spaces=(\@skuspaceheader);
        foreach $table (@table_list) {      
            $query = dequote(<< "            ENDQUERY");
                select plunum from $table 
                    where 
                        plunum like '%SPACE'
                    or
                        plunum like 'SPACE%'
                order by plunum
            ENDQUERY
                
            $sth = $dbh->prepare($query);
            $sth->execute();
         
            while (my @tmp=$sth->fetchrow_array()) {
                my $sku=$tmp[0];
                my @entry=(
                "$sku",
                "$table",
                );
                my $found=0;
                foreach my $sref (@sku_w_spaces) {
                    if (($$sref[0] eq $sku) && ($$sref[1] eq $table)) {            
                        $found=1;
                    }                 
                }     
                unless ($found) {
                    # Don't add it to the list if it is already there
                    push(@sku_w_spaces,\@entry);            
                    $count1++;             
                }
                
            } 
        }         
        if ($count1) {
            TTV::cgiutilities::printResult_arrayref2(\@sku_w_spaces, '', ''); 
            print $q->p("Don't forget to correct these SKUs where 'SPACE' has been inserted in the plunum.");
        } else {
            print $q->h2("No SKUs found with spaces");
            ElderLog($logfile, "No SKUs found with spaces" );
        }
        
    }
    $dbh->disconnect;
    StandardFooter(); 
    
} elsif ($tool eq "SKUSPACE2") {
    # This tool is for fixing simple skus with spaces.  That is, when there is only one entry in the table and it has a space.  We simply delete the space.
	print $q->h3("Fixing SKUs with spaces"); 
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_SPACE.log";
    ElderLog($logfile, "Fixing SKUs with spaces" );    
    my $fixall= ($q->param("fixall"))[0];  
    my $sku= ($q->param("skuid"))[0];  
    my $table= ($q->param("table"))[0];
    if ($table) {
        print $q->p("Fixing table: $table");
    }
    if ($sku) {
        print $q->p("Fixing sku: $sku");
    }
    $sku=~s/\'//g;
    my $newsku=$sku;
    $newsku=~s/ //g;
 
    # This option has not been developed yet.  I wanted a way to fix all of the SKUs at once but I have not gotten it working yet - kdg 2009-10-26
    if ($fixall) {
        # Fix all of the SKUs with simple problems        
        my @tmp=split(/,/,$fixall);
        foreach my $t (@tmp) {
            my @tmp2=split(/=/,$t);
            my $sku=$tmp2[0];
            next if ($sku eq "SKU");
            $sku=~s/\'//g;
            my $newsku=$sku;
            $newsku=~s/ //g;
            my $table=$tmp2[1];            
            $query = dequote(<< "            ENDQUERY");
                if exists (select plunum from $table where plunum = '$sku')
                update $table set plunum = '$newsku' where plunum = '$sku'
            ENDQUERY
            $sth = $dbh->prepare($query);
            $sth->execute();
            my $rows=$sth->rows;
            if ($rows>=1) {
                print "Successfully changed \'$sku\' to \'$newsku\' in $table<br/>";
                ElderLog($logfile, "Successfully changed \'$sku\' to \'$newsku\' in $table" );   
            }                 
        }
      
    } else {
        # Fix only the specified SKU in the specified table
        #foreach my $table (@table_list) {
            # Remove the space(s) at the end
            $query = dequote(<< "            ENDQUERY");
                select plunum from $table where plunum like '$sku'
                
            ENDQUERY
            $sth = $dbh->prepare($query);
            $sth->execute();
            my $rows=0;
            while (my @tmp=$sth->fetchrow_array()) {
                if ($tmp[0]) {
                    $rows++;
                }            
            }    
         
            if ($rows) {
                $query = dequote(<< "                ENDQUERY");                    
                    update $table set plunum = '$newsku' where plunum = '$sku'
                ENDQUERY
 
                $sth = $dbh->prepare($query);
                $sth->execute();
                $rows=$sth->rows;
                if ($rows>=1) {
                    print $q->p("Successfully changed \'$sku\' to \'$newsku\' in $table");
                    ElderLog($logfile, "Successfully changed \'$sku\' to \'$newsku\' in $table" );   
                }                 
            } else {
                print $q->p("Failed to find $sku in $table");
                ElderLog($logfile, "Failed to find $sku in $table" );  
            }            
        #}
    }
        
  
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUSPACE" }, "Back to SKU Space tool");     
    StandardFooter(); 
    
} elsif ($tool eq "SKUSPACE3") {
	print $q->h3("Renaming SKUs with spaces"); 
    my $sku= ($q->param("skuid"))[0]; 
    print $q->p("Select 'Go' to rename $sku to put 'SPACE' in place of the space.");
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUSPACE4"});
    print $q->input({-type=>"hidden", -name=>"sku", -value=>"$sku"});
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );


    print $q->end_form(), "\n";
    
    StandardFooter();    
} elsif ($tool eq "SKUSPACE4") {
	print $q->h3("Renaming SKUs with spaces"); 
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_SPACE.log";
    ElderLog($logfile, "Renaming SKUs with spaces" );      
    my $sku= ($q->param("sku"))[0];  
    unless ($sku) {
        print "Error: Failed to determine sku number<br />";
        print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUSPACE" }, "Back to SKU Space tool");   
        StandardFooter();
        exit;
    }
    $sku=~s/\'//g;  # Remove single quotes
 
    my $newsku=$sku;
    $newsku=~s/ //g; # Remove Spaces
    if ($sku =~ /^ /) {
        # Space at the beginning
        print $q->p("Replacing space at the beginning");
        $newsku="SPACE${newsku}";
    } elsif ($sku =~ / $/) {
        # Space at the end
        print $q->p("Replacing space at the end");
        $newsku="${newsku}SPACE";
    } else {
        print $q->h3("Error: Found no space in ($sku)");
        StandardFooter();
        exit;
    }
  

    foreach my $table (@table_list) {
        # Rename the space at the end to SPACE
        $query = dequote(<< "        ENDQUERY");
            if exists (select plunum from $table where plunum = '$sku')
            update $table set plunum = '$newsku' where plunum = '$sku'
        ENDQUERY
 
        $sth = $dbh->prepare($query);
        $sth->execute();
        my $rows=$sth->rows;        
        if ($rows>=1) {
            print $q->p("Successfully changed \'$sku\' to \'$newsku\' in $table");
            ElderLog($logfile, "Successfully changed \'$sku\' to \'$newsku\' in $table" );               
        }   
    }    
   
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUSPACE" }, "Back to SKU Space tool");         
    StandardFooter();       
} elsif ($tool eq "SKUORPHAN") {
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_ORPHAN.log";
    
	print $q->h3("Looking for SKUs which:");
    #print $q->p("- are not under inventory control");
    print $q->p("- have no description or vendor.");  
    #print $q->p("- have no inventory.");  
    ElderLog($logfile, "Looking for SKUs with no description or vendor" );
    ###
  	$query = dequote(<< "    ENDQUERY");
        select 
            p.plunum, ic.invcontrolled, ic.qtycurrent 
        from 
            plu p 
        left outer join 
            inventory_current ic on p.plunum = ic.plunum
        where 
            p.pludesc like ''
            and p.vendorid like ''
            --and ic.invcontrolled is NULL
            --and ic.qtycurrent is NULL
        order by 
            p.plunum
    ENDQUERY
    
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        my $sku_list;
        for my $datarows (1 .. $#$tbl_ref)
        {
            my $thisrow = @$tbl_ref[$datarows];
            my $sku = trim($$thisrow[0]);
            $sku_list.="$sku,";            
        }
        my @links = ( '/cgi-bin/i_toolshed.pl?report=SKUORPHAN2&skuid=$_' );             
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');
        print $q->p(
           $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN3&fixall=$sku_list", -class=>"button1"}, "Purge all of these SKUs")
           ,"Purge the SKUs using an ASC file."
        );
        print $q->p(
           $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN2&fixall=$sku_list", -class=>"button1"}, "Delete All SKUs")
           ,"Delete the SKUs with a database query. (Use this if the ASC file method fails.)"
        );        
    } else {
        print $q->p("No SKU's found without Desc");
    }
      
    ## PART II - this part is like the monitor script as of 2012-09-27
    my @orphan_list;
    my @orphan_list2;
    my @orphan_list3;

    my $warns;
	my @tmp;
    
    # Find plunums which can be ordered but which we don't have enough info about
    $query=dequote(<< "    ENDQUERY");    
    select 
        plunum
    from 
        inventory_current 
    where 
        --canorder like 'Y' 
        --and 
		plunum not in (select plunum from plu)
    ENDQUERY
    
    $sth = $dbh->prepare($query);
    $sth->execute();        
    while (@tmp = $sth->fetchrow_array) {
        if ($tmp[0]) {
            push(@orphan_list,$tmp[0]);    
        }
    } 
    # With this list of orphan plunums, check to see if they are in the plu_cross_ref either as a plunum or an xrefnum

    $query=dequote(<< "    ENDQUERY");    
    select 
        plunum
    from 
        inventory_current 
    where 
        canorder like 'Y' 
        and plunum not in (select plunum from plu_cross_ref)
    ENDQUERY
    $sth = $dbh->prepare($query);
    $sth->execute();        
    while (@tmp = $sth->fetchrow_array) {             
        push(@orphan_list2,$tmp[0]);   
    }         

    $query=dequote(<< "    ENDQUERY");    
    select 
        plunum
    from 
        inventory_current 
    where 
        canorder like 'Y' 
        and plunum not in (select xrefnum from plu_cross_ref)
    ENDQUERY
    $sth = $dbh->prepare($query);
    $sth->execute();        
    while (@tmp = $sth->fetchrow_array) {             
        push(@orphan_list3,$tmp[0]);   
    }   

    # What SKU is in all three lists?
    my $orphan_count=0;
    foreach my $SKU (@orphan_list) {
        if (grep/$SKU/,@orphan_list2) {
            if (grep/$SKU/,@orphan_list3) { 
                print "<a href=/cgi-bin/i_toolshed.pl?report=DELETE_FROM_IC&skuid=$SKU> $SKU</a>";
                print " is in inventory_current but not in plu or plu_cross_ref<br />";
                $orphan_count++;
            }
        }
    }	
	if ($orphan_count) {
		print $q->p("Click on the SKU to delete the entry from inventory_current");
	}
            
  
    ###     
    StandardFooter();  
} elsif ($tool eq "DELETE_FROM_IC") {
	print $q->h3("Deleting orphan SKUs"); 
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_ORPHAN.log";
    ElderLog($logfile, "Deleting orphan SKUs" );    
    my $fixall= ($q->param("fixall"))[0];  
    my $sku= ($q->param("skuid"))[0];  
    my $confirm= ($q->param("confirm"))[0];    
    $sku=~s/\'//g;
    my $table="inventory_current";
    unless ($sku) {
        print $q->h3("Sorry, failed to determine SKU to delete!");
        StandardFooter();
        exit;        
    }    
    if ($confirm) {
        # Fix only the specified SKU
     
            # Remove the space(s) at the end
            $query = dequote(<< "            ENDQUERY");
                delete from $table where plunum like '$sku'                
            ENDQUERY
            
            $sth = $dbh->prepare($query);
            $sth->execute();
            my $rows=$sth->rows;
            if ($rows>=1) {
                print $q->p("Successfully deleted \'$sku\' from $table");
                ElderLog($logfile, "Successfully deleted \'$sku\' from $table" );               
            }    
        
    } else {
        print $q->p("Click 'CONFIRM' to clear orphan $sku from $table.");
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"DELETE_FROM_IC"});
        print $q->input({-type=>"hidden", -name=>"skuid", -value=>"$sku"});
        print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'C', -value=>"   CONFIRM   "}))
                  );


        print $q->end_form(), "\n";        
    }
       
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN", -target=>'_top'}, "Back to orphan SKU tool");     
    StandardFooter();    
} elsif ($tool eq "SKUORPHAN2") {
	print $q->h3("Deleting orphan SKUs"); 
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_ORPHAN.log";
    ElderLog($logfile, "Deleting orphan SKUs" );    
    my $fixall= ($q->param("fixall"))[0];  
    my $sku= ($q->param("skuid"))[0];  
    my $confirm= ($q->param("confirm"))[0];    
    $sku=~s/\'//g;
    
 
    # This option has not been developed yet.  I wanted a way to fix all of the SKUs at once but I have not gotten it working yet - kdg 2009-10-26
    if ($fixall) {
        # Delete all of the orphan SKUs 
        my @tmp=split(/,/,$fixall);
        foreach my $t (@tmp) {
            my @tmp2=split(/=/,$t);
            my $sku=$tmp2[0];
            next if ($sku eq "SKU");
            $sku=~s/\'//g;            
            foreach my $table (@table_list) {
                $query = dequote(<< "                ENDQUERY");
                    delete from $table where plunum = '$sku'
                ENDQUERY
                $sth = $dbh->prepare($query);
                $sth->execute();
                my $rows=$sth->rows;
                if ($rows>=1) {
                    print $q->p("Successfully deleted \'$sku\' from $table");
                    ElderLog($logfile, "Successfully deleted \'$sku\' from $table" );               
                }      
            }                                     
        }
      
    } else {
        if ($confirm) {
            # Fix only the specified SKU
            foreach my $table (@table_list) {
                # Remove the space(s) at the end
                $query = dequote(<< "                ENDQUERY");
                    delete from $table where plunum like '$sku'                
                ENDQUERY
                
                $sth = $dbh->prepare($query);
                $sth->execute();
                my $rows=$sth->rows;
                if ($rows>=1) {
                    print $q->p("Successfully deleted \'$sku\' from $table");
                    ElderLog($logfile, "Successfully deleted \'$sku\' from $table" );               
                }    
            }        
        } else {
            print $q->p("Click 'CONFIRM' to clear orphan $sku ");
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUORPHAN2"});
            print $q->input({-type=>"hidden", -name=>"skuid", -value=>"$sku"});
            print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'C', -value=>"   CONFIRM   "}))
                      );


            print $q->end_form(), "\n";        
        }

    }
        
  
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN", -target=>'_top'}, "Back to orphan SKU tool");     
    StandardFooter();
} elsif ($tool eq "SKUORPHAN3") {
	print $q->h3("Purging SKUs"); 
    $logfile = "C:/MLINK/akron/i_toolshed_SKU_ORPHAN.log";
    ElderLog($logfile, "Purging SKUs using an ASC file" );    
    my $fixall= ($q->param("fixall"))[0];  
    my $sku= ($q->param("skuid"))[0];  
    my $confirm= ($q->param("confirm"))[0];    
    $sku=~s/\'//g;
    
    my $plutxn="c:/temp/plutxn.asc";  
    my $invncont="c:/temp/INVNCONT.ASC";  
    my $xreftxn="c:/temp/XREFTXN.asc";  
    my $plutxn2="c:/temp/PLUTXN2.asc";  
    
    if ($fixall) {
        # Purge all of the  SKUs 
        my @tmp=split(/,/,$fixall);
        foreach my $t (@tmp) {
            my @tmp2=split(/=/,$t);
            my $sku=$tmp2[0];
            next if ($sku eq "SKU");
            $sku=~s/\'//g;              
            ElderLog($logfile,"Adding $sku to ASC file for purging");
            if (open (FILE, ">>$plutxn")) {                
                print FILE "3,$sku,\n"; 
                close FILE;
            } else {
                print "Error Opening $plutxn<br />";
                StandardFooter();   
                exit;
            }
            if (open (FILE, ">>$invncont")) {            
                print FILE "3,$sku,1,,,,\n";                      
                close FILE;
            } else {
                print "Error Opening $invncont<br />";
                StandardFooter();   
                exit;
            }  
            if (open (FILE, ">>$xreftxn")) {  
                print FILE "3,$sku,\n";                     
                close FILE;
            } else {
                print "Error Opening $xreftxn<br />";
                StandardFooter();   
                exit;
            }  
            if (open (FILE, ">>$plutxn2")) {            
                print FILE "3,$sku,,\n";                    
                close FILE;
            } else {
                print "Error Opening $plutxn2<br />";
                StandardFooter();   
                exit;
            }          
                                  
            ###
        }
        print $q->p("Created ASC files to purge SKUs");

        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUPURGE2"});                       
        print $q->input({-type=>"submit", -name=>"submit", -value=>"Submit"},"To process these purges, click the 'Submit' button.");                               
        print $q->p("(NOTE: It may take up to 30 seconds for the ASC file to be processed once submitted.)");
        print $q->end_form(), "\n";   
        print $q->p();          
      
    } 
          
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN", -target=>'_top'}, "Back to orphan SKU tool");     
    StandardFooter();
} elsif ($tool eq "TENDERRPT") {    
    print $q->h3("Tender Report - Basic");
    # Ask which day to do the report fo
      print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"TENDERRPT_DC"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      my @ltm = localtime(time());
      my $month=$ltm[4]+1;
      my $year=$ltm[5]+1900;
      my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
      my $hm = 1;
      my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
      $startdate = $enddate; # for now.
 
      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
 
         
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"radio", -name=>"sort_opt", -value=>"0", -checked=>"1"}) .            
                   $q->font({-size=>2}, "Sort by Txn")));
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"radio", -name=>"sort_opt", -value=>"1",}) .            
                   $q->font({-size=>2}, "Sort by Tender")));                   
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"radio", -name=>"sort_opt", -value=>"2",}) .            
                   $q->font({-size=>2}, "Sort by Register")));                    
                  
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      #print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");
    
    
    StandardFooter();    
} elsif ($report eq "TENDERRPT_DC") {
    print $q->h3("Sales Tender Data Compilation - Beta");

    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("startdate"))[0]);      
    my $sort_opt   = trim(($q->param("sort_opt"))[0]);

    my $datewhere_ms = "";
    my $datewhere_tm = "";
    my $datewhere_db = "";
    my $datewhere_nm = "";
    my $daterange = "";
    my $orderby="tm.storenum, tm.transnum";
    if ($sort_opt == 0 ) {
        $orderby = "tm.txnnum";
    } elsif ($sort_opt == 1) {
        $orderby = "ten.tenderdesc,tm.txnnum";
    } elsif ($sort_opt == 2) {
        $orderby = "tm.regnum,tm.txnnum";
    }

    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not correct";
            StandardFooter();
            exit
        }

        $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_nm = " nm.itemdatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
        $daterange = " date range $startdate - $enddate";
    } else {
        print "Start date or End date is blank";
        StandardFooter();
        exit
    }

    print $q->p("Compiling data for $daterange.");
    
    my $dbh = openODBCDriver($dsn);

    # Get the post voids for the period
    my @post_voids;
    my @post_voided_txns;
    $query = dequote(<< "    ENDQUERY");
    
        select
            pt.txnnum,pt.transnum,vpt.txnnum,vpt.transnum         
        from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
                mt.storenum=vpt.storenum and
                mt.txnnum=vpt.txnnum and
                mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                pt.regnum = vpt.regnum and
                mt.refnum = pt.txnnum   and
                mt.itemdatetime > '$startdate' and               
                mt.misctype = 2;      
    ENDQUERY
    
    $sth = $dbh->prepare($query);    
    $sth->execute();    
    while (my @tmp=$sth->fetchrow_array()) {   
        # We need to record both the original transaction which was voided as well as the transaction which did the void.
        my @array=(
            "$tmp[0]",
            "$tmp[1]"
        );                        
        push(@post_voided_txns,\@array);   
        my @array2=(
            "$tmp[2]",
            "$tmp[3]"
        );                        
        push(@post_voids,\@array2);            
    }    
    
    # Get the sales
    $query = dequote(<< "    ENDQUERY");      
           
        select
            tm.storenum, tm.transnum, tm.txnnum,            
            convert(varchar(19), ms.itemdatetime,21) as TranDateTime,
            tm.regnum, tm.cashiernum, 
            ms.itemnum,
            ms.txnmodifier, ms.salesprsnid, ms.skunum, ms.deptnum,
            p.pludesc, p.deptnum, p.vendorid,
            ms.qty, ms.extorigprice, ms.extregprice, ms.extundiscprice, ms.extsellprice,
            ms.promonum, ms.priceoverride, ms.discounted,
            ms.tax, ms.tax2,tmp.tenderid as TenderId
        from
            Txn_POS_Transactions tm
            join Txn_Merchandise_Sale ms on tm.storenum=ms.storenum and tm.transnum=ms.transnum
            join Txn_Miscellaneous_Trans mt on
               mt.txnnum=tm.txnnum and
               mt.transnum=tm.transnum
            left outer join plu p on ms.skunum=p.plunum            
            left outer join Txn_Profile_Prompt_Response tpr on tm.storenum=tpr.storenum and tm.transnum=tpr.transnum and tpr.propromptid=8            
            left outer join txn_method_of_payment tmp on tmp.transnum=tm.transnum              
        where
            $datewhere_tm and
            ms.txnvoidmod = 0 and            
            mt.txnvoidmod = 0            
        order by 
            tm.storenum, tm.transnum;
        
    ENDQUERY

    # Find aborted transactions
    my @aborted_trans=();
    $query= dequote(<< "    ENDQUERY");   
        select 
            ms.txnnum,ms.transnum
            from 
            txn_miscellaneous_trans ms
        where              
            $datewhere_ms
            ms.txnvoidmod = 2
    ENDQUERY
        
    $sth = $dbh->prepare($query);    
    $sth->execute();    
    while (my @tmp=$sth->fetchrow_array()) {           
        my @array=(
            "$tmp[0]",
            "$tmp[1]"
        );                
        push(@aborted_trans,\@array);
    }
    # Unfortunately, I have not yet sorted out the query to get the tax right so I am getting the tax here
    $query = dequote(<< "    ENDQUERY");    
        select 
            tm.txnnum, 
            tx.tax_amt 
        from 
            Txn_POS_Transactions tm             
            join txn_tax tx on tx.transnum=tm.transnum 
        where 
             $datewhere_tm      
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my $current_txn=0; 
    my %tax_hash;
    my $tax_total;
    for my $datarows (1 .. $#$tbl_ref) {      
        my $thisrow = @$tbl_ref[$datarows];  
        my $txnnum = trim($$thisrow[0]); 
        my $tax = trim($$thisrow[1]); 
        $tax_hash{$txnnum}+=$tax;                  
    }
    # Find the non merchandise transactions
    $query = dequote(<< "    ENDQUERY");    
        select 
            tm.txnnum, 
            nm.extsellprice
        from 
            Txn_POS_Transactions tm           
            join Txn_Non_Merch_Sale nm on nm.transnum=tm.transnum 
        where 
             $datewhere_nm      
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);     
    my %nonmerch_hash;
    my $nonmerch;
    for my $datarows (1 .. $#$tbl_ref) {      
        my $thisrow = @$tbl_ref[$datarows];  
        my $txnnum = trim($$thisrow[0]); 
        $nonmerch = trim($$thisrow[1]); 
        $nonmerch_hash{$txnnum}+=$nonmerch;                  
    }
    # The main query
    $query = dequote(<< "      ENDQUERY");                       
        select 
            tm.txnnum,     
            tm.regnum,     
            tm.transnum, 
            ten.tenderdesc, 
            tmp.tenderamt,
            tmp.changeamt,     

            (tmp.tenderamt - tmp.changeamt) as "TxnAmt",
            (tmp.tenderamt - tmp.changeamt) as "NetAmt",
            tx.tax_amt as "TaxAmt",
            '' as "NonMerch",
            convert(varchar(19), ms.itemdatetime,21) as TranDateTime 
        from 
            Txn_POS_Transactions tm
            join Txn_Merchandise_Sale ms on tm.storenum=ms.storenum and tm.transnum=ms.transnum                        
            left outer join txn_method_of_payment tmp on tmp.transnum=tm.transnum            
            left outer join tender ten on ten.tenderid=tmp.tenderid    
            left outer join txn_tax tx on tx.transnum=tm.transnum
        where
            $datewhere_tm             
        order by $orderby;
      ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query); 

    
    my %tender_count=();
    my %txn_tender=();  
    my %tender_amount=();
    my %gross_tender_amount=();    
    my %tender_register=();
    my %key_hash=();
    my $tbl_ref2=[@$tbl_ref[0]];    # used to total each transaction

    $current_txn=0;
    my $current_key=0;
    my %txn_total;  # Used to keep track of totals for a given transaction
    my %report_total;   # Keep track of totals for the entire report
    my $dtotal=0;   # Discount total   
    my $tax_for_tran_total=0;
    for my $datarows (1 .. $#$tbl_ref) {      
        my $thisrow = @$tbl_ref[$datarows];  
        my $txnnum = trim($$thisrow[0]); 
        my $register= trim($$thisrow[1]);         
        my $transnum = trim($$thisrow[2]);     
        my $tender = trim($$thisrow[3]);
        my $fullamount= trim($$thisrow[4]);          
        my $changeamount= trim($$thisrow[5]);          
        my $amount= trim($$thisrow[6]); 
        my $netamount=trim($$thisrow[7]); 
        my $tax= trim($$thisrow[8]);
        # place holder in field 9 for non merchandise sales
        my $tdate= trim($$thisrow[10]); 
        my $key=$txnnum.$tender;
        # Eliminate post voids
        my $voided=0;
        my $post_void=0;
        foreach my $ref (@post_voided_txns) {
            # These transactions were post-voided
            if (($$ref[0] == $txnnum) && ($$ref[1] == $transnum)) {
                $voided=1;                                   
            }
        }            
        if ($voided) {
            next;   # Skip this transaction sinc it was voided            
        }  
        foreach my $ref (@post_voids) {
            # These transactions were post-voiding a previous one
            if (($$ref[0] == $txnnum) && ($$ref[1] == $transnum)) {
                $post_void=1;                     
            }
        }            
        if ($post_void) {
            # Appedn voided to the tender - This allows us to know what the tender was but we won't record the amounts.
            $tender.="--Voided--";
            $amount=0;
            $netamount=0;
            $voided=1;
            $tax=0;       
        }              
        my $aborted=0; 
        my $unknown=0;
       
        unless ($tender) {            
            foreach my $ref (@aborted_trans) {
                if (($$ref[0] == $txnnum) && ($$ref[1] == $transnum)) {
                    $aborted=1;                
                }
            }
            if ($aborted) {
                $tender="--Aborted--";
                $tax="";                
            } else {
                $tender="Unknown";  
                $unknown=1;
            }                 
          
            # Push this back into the ref
            $$thisrow[3]=$tender;            
        }
        
        if ($txn_tender{$txnnum}) {
            # There could be more than one tender
            my $previous_tender=$txn_tender{$txnnum};
            unless ($tender eq $previous_tender) {
                $txn_tender{$txnnum}="$tender,$previous_tender";
            }            
        } else {
            $txn_tender{$txnnum}=$tender;
        }

        unless ($current_key eq $key) {
            unless ($key_hash{$key}) {                
                $key_hash{$key}=1;
                # We have started a new transaction.  Add the total for the previous transaction
                if (($aborted) || ($unknown) || ($voided)) {
                    # Blank these values for aborted transactions
                    $tax="";
                    $nonmerch="";
                    $netamount="";
                } else {
                    $tax="";
                    $nonmerch="";
                    #$netamount="";
                    #$tax=0;
                    #$nonmerch=0;
                    unless ($current_txn == $txnnum) {
                        # Only add the tax and nonmerch amount once per transaction
                        # This is necessary because of how we handle split tender
                        $tax="$tax_hash{$txnnum}";  # Grab the tax for the transaction from the hash created earlier            
                        $nonmerch="$nonmerch_hash{$txnnum}";  # Grab the tax for the transaction from the hash created earlier            
                    }                    
                    # Reduce the netamount by the tax and the non merchandise amount                    
                    $netamount=($netamount - $tax - $nonmerch);                                        
                }
                
                my @array=(
                    "$txnnum",
                    "$register",
                    "$transnum",
                    "$tender",                    
                    "$fullamount",
                    "$changeamount",
                    "$amount",
                    "$netamount",
                    "$tax",
                    "$nonmerch",
                    "$tdate"
                );
                push(@$tbl_ref2,\@array);
                # Reset the values of the hash            
                $current_txn=$txnnum; 
                $current_key=$key;
                unless ($tender eq "--Aborted--") {                               
                    $report_total{"fullamount"}+=sprintf("%.2f",$fullamount); 
                    $report_total{"changeamount"}+=sprintf("%.2f",$changeamount);
                    $report_total{"amount"}+=sprintf("%.2f",$amount);
                    $report_total{"netamount"}+=sprintf("%.2f",$netamount);
                    $report_total{"tax"}+=sprintf("%.2f",$tax);
                    $report_total{"nonmerch"}+=sprintf("%.2f",$nonmerch);
                }    
                $tender_count{$tender}++;
                $tender_amount{$tender}+=sprintf("%.2f",($amount - $tax)); 
                $gross_tender_amount{$tender}+=sprintf("%.2f",$amount); 
            }
        }        

    }

   
    # Now add the report totals:
    my @array2=(
        "Report Total:",
        "",
        "",
        "",        
        "$report_total{'fullamount'}",
        "$report_total{'changeamount'}",
        "$report_total{'amount'}",
        "$report_total{'netamount'}",
        "$report_total{'tax'}",
        "$report_total{'nonmerch'}",        
        ""
    );
    push(@$tbl_ref2,\@array2);    
    
    
    # Reset the values of the hash    
    my @format = (
        "",
        "",
        "",
        "",
        {'align' => 'Right', 'currency' => ''},        
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},        
        "",
        ""
    ); 
    # Print the primary report here
    TTV::cgiutilities::printResult_arrayref2($tbl_ref2, '', \@format); 
    
   
    # Create the summary report here:
    $sth = $dbh->prepare($query);
    $sth->execute();

    my @header=(
    "Tender",
    "Number of Items",
    "Number of Transactions",
    "Gross Tender Amount",
    "Net Tender Amount",
    );
    my @tender_report=(\@header);
    my $tcount=0;
    my $txncount=0;
    my $ttotal=0;   # Total amount
    my $gtotal=0;   # Gross total

    my %txn_count;
    foreach my $txn (sort keys(%txn_tender)) {
        # Trying to account for split tenders.  That is, one transaction may have more than one tender.
        my @tmp=split(/,/,$txn_tender{$txn});
        foreach my $t (@tmp) {
            $txn_count{$t}++;
        }
    }
    foreach my $tender (sort keys(%tender_count)) {
        my @line=(
            "$tender",
            "$tender_count{$tender}",
            "$txn_count{$tender}",
            "\$$gross_tender_amount{$tender}",            
            "\$$tender_amount{$tender}",

        );
        $tcount+=$tender_count{$tender};
        $txncount+=$txn_count{$tender};
        $ttotal+=$tender_amount{$tender};
        $gtotal+=$gross_tender_amount{$tender};
        push(@tender_report,\@line);
    }
    $ttotal=sprintf("%.2f",$ttotal);
    $gtotal=sprintf("%.2f",$gtotal);
    my @total_line=(
        "Total",
        "$tcount",
        "$txncount",
        "\$$gtotal",
        "\$$ttotal",

    );
    push(@tender_report,\@total_line);
    TTV::cgiutilities::printResult_arrayref2(\@tender_report, '', ''); 
    print $q->p("* Gross Tender Amount is the total tender amount collected.");
    print $q->p("* Net Tender Amount is Gross less the Total Tax collected but includes non-merchandise.");
    # close the database handler.
    $dbh->disconnect;  
    StandardFooter();    
} elsif ($tool eq "TENDERRPT2") {    
    print $q->h3("Tender Report - Beta");
    # Ask which day to do the report fo
      print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"TENDERRPT2_DC"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      my @ltm = localtime(time());
      my $month=$ltm[4]+1;
      my $year=$ltm[5]+1900;
      my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
      my $hm = 1;
      my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
      $startdate = $enddate; # for now.

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr(
             $q->td(
               {
                  -class  => 'tabledatanb',
                  -nowrap => undef,
                  -align  => 'right'
               },
               "Use Partial Day:"
               ),
             $q->td(
                  { -class => 'tabledatanb', -align => 'left' },
                  $q->input( { -name => "partial", -type  => "checkbox"} ),
              )
             );
      print $q->Tr($q->td({
                  -class  => 'tabledatanb',
                  -nowrap => undef,
                  -align  => 'right'
               },
               "Sort By Txn:"
               ),
             $q->td(
                  { -class => 'tabledatanb', -align => 'left' },
                  $q->input( { -name => "txn_sort", -type  => "checkbox"} ),
              )
             );
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      #print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");
    
    
    StandardFooter();
} elsif ($report eq "TENDERRPT2_DC") {
      print $q->h3("Sales Tender Data Compilation - Beta");

      my $startdate = trim(($q->param("startdate"))[0]);
      my $enddate   = trim(($q->param("enddate"))[0]);
      my $partial   = trim(($q->param("partial"))[0]);
      my $txn_sort   = trim(($q->param("txn_sort"))[0]);

      my $datewhere_ms = "";
      my $datewhere_tm = "";
      my $datewhere_db = "";
      my $daterange = "";
      my $orderby="tm.storenum, tm.transnum";
      if ($txn_sort) {
        $orderby = "tm.txnnum";
      }
      if ($partial){
          if ( ($startdate) || ($enddate) ) {
          if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2} \d{2}:\d{2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2} \d{2}:\d{2}$/) ) {
            print "Startdate or Enddate format not correct";
            StandardFooter();
            exit
         }

         $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate' and ";
         $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate' ";
         $datewhere_db = " itemdatetime between '$startdate' and '$enddate'";
         $daterange = " date range $startdate - $enddate";
         } else {
            print "Start date or End date is blank";
            StandardFooter();
            exit
         }

      }
      else {
         if ( ($startdate) || ($enddate) ) {
            if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
               print "Startdate or Enddate format not correct";
               StandardFooter();
               exit
            }

            $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
            $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
            $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
            $daterange = " date range $startdate - $enddate";
            } else {
               print "Start date or End date is blank";
               StandardFooter();
               exit
            }

      }
      print $q->p("Compiling data for $daterange.");

      my $dbh = openODBCDriver($dsn);

      $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
               mt.storenum=vpt.storenum and
               mt.txnnum=vpt.txnnum and
               mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               pt.regnum = vpt.regnum and
               mt.refnum = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
         where
            mt.misctype = 2;            

         select
            tm.storenum, tm.transnum, tm.txnnum,
            -- I changed the source of the date - kdg
            --convert(varchar(19), tm.txndatetime,21) as TranDateTime,
            convert(varchar(19), ms.itemdatetime,21) as TranDateTime,
            tm.regnum, tm.cashiernum, 
            ms.itemnum,
            ms.txnmodifier, ms.salesprsnid, ms.skunum, ms.deptnum,
            p.pludesc, p.deptnum, p.vendorid,
            ms.qty, ms.extorigprice, ms.extregprice, ms.extundiscprice, ms.extsellprice,
            ms.promonum, ms.priceoverride, ms.discounted,
            ms.tax, ms.tax2,tmp.tenderid as TenderId
         from
            Txn_POS_Transactions tm
            join Txn_Merchandise_Sale ms on tm.storenum=ms.storenum and tm.transnum=ms.transnum
            join Txn_Miscellaneous_Trans mt on
               mt.txnnum=tm.txnnum and
               mt.transnum=tm.transnum
            left outer join plu p on ms.skunum=p.plunum
            left outer join #tmpPostVoids v on tm.storenum=v.storenum and tm.transnum=v.transnum
            left outer join Txn_Profile_Prompt_Response tpr on tm.storenum=tpr.storenum and tm.transnum=tpr.transnum and tpr.propromptid=8
            left outer join txn_method_of_payment tmp on tmp.transnum=v.transnum 
         where
            $datewhere_tm and
            ms.txnvoidmod = 0 and
            v.transnum is null and
            mt.txnvoidmod = 0
         order by tm.storenum, tm.transnum;

         drop table #tmpPostVoids;
      end
      ENDQUERY
    # Find aborted transactions
    my @aborted_trans=();
    $query= dequote(<< "    ENDQUERY");   
        select 
            ms.txnnum,ms.transnum
            from 
            txn_miscellaneous_trans ms
        where              
            $datewhere_ms
            ms.txnvoidmod = 2
    ENDQUERY
        
    $sth = $dbh->prepare($query);    
    $sth->execute();    
    while (my @tmp=$sth->fetchrow_array()) {           
        my @array=(
            "$tmp[0]",
            "$tmp[1]"
        );                
        push(@aborted_trans,\@array);
    }
        
    $query = dequote(<< "      ENDQUERY");                       
         select         
            ten.tenderdesc,
            tm.txnnum,
            ms.itemnum,            
            ms.qty, 
            ms.extorigprice,  
            ms.extsellprice, 
            tx.tax_amt,
            tm.regnum, 
            tm.cashiernum, 
            ms.skunum, 
            ms.deptnum,
            p.pludesc,            
            tm.transnum,
            convert(varchar(19), 
            ms.itemdatetime,21) as TranDateTime,
            ms.discounted,
            ms.extundiscprice,
            ms.tax,
            ms.tax2
         from
            Txn_POS_Transactions tm
            join Txn_Merchandise_Sale ms on tm.storenum=ms.storenum and tm.transnum=ms.transnum            
            left outer join plu p on ms.skunum=p.plunum              
            left outer join txn_method_of_payment tmp on tmp.transnum=tm.transnum --and tmp.itemnum = ms.itemnum
            left outer join tender ten on ten.tenderid=tmp.tenderid 
            left outer join txn_tax tx on tx.transnum=tm.transnum --and tx.itemnum = ms.itemnum
         where
            $datewhere_tm             
         order by $orderby;
      ENDQUERY
      # todo - need to sort out method of payment when we have split tender
      # todo - need to sort out tax when some things are taxed and some are not.
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my %tender_count=();
    my %txn_tender=();  
    my %tender_amount=();
    my %gross_tender_amount=();    
    my %tender_register=();
    my $tbl_ref2=[@$tbl_ref[0]];
    my @header=(
        "",
        "OriginalPrice",
        "SellPrice",
        "Tax",
        "Discount"        
    );
    my $tbl_ref3; #=(\@header);  # To record transaction totals.
    push(@$tbl_ref3,\@header);
    my $current_txn=0;
    my %txn_total;  # Used to keep track of totals for a given transaction
    my %report_total;   # Keep track of totals for the entire report
    my $dtotal=0;   # Discount total    
    for my $datarows (1 .. $#$tbl_ref) {      
        my $thisrow = @$tbl_ref[$datarows];  
        my $tender = trim($$thisrow[0]);
        my $txnnum = trim($$thisrow[1]);        
        my $origamount= trim($$thisrow[4]);
        my $amount= trim($$thisrow[5]);
        my $taxamount= trim($$thisrow[6]);
        my $register= trim($$thisrow[7]);           
        my $transnum = trim($$thisrow[12]);  
        my $discounted = trim($$thisrow[14]);  
        my $undiscprice = trim($$thisrow[15]);          
        my $discamount = ($undiscprice - $amount) if ($discounted eq 'Y');        
        unless ($tender) {
            my $aborted=0; 
            foreach my $ref (@aborted_trans) {
                if (($$ref[0] == $txnnum) && ($$ref[1] == $transnum)) {
                    $aborted=1;                
                }
            }
            if ($aborted) {
                $tender="--Aborted--";
            } else {
                $tender="Unknown";
            }                
            # Push this back into the ref
            $$thisrow[0]=$tender;
        }
  
        if ($txn_tender{$txnnum}) {
            # There could be more than one tender
            my $previous_tender=$txn_tender{$txnnum};
            unless ($tender eq $previous_tender) {
                $txn_tender{$txnnum}="$tender,$previous_tender";
            }            
        } else {
            $txn_tender{$txnnum}=$tender;
        }

        $tender_count{$tender}++;
        $tender_amount{$tender}+=sprintf("%.2f",$amount); 

        $gross_tender_amount{$tender}+=sprintf("%.2f",$amount);           
        $gross_tender_amount{$tender}+=sprintf("%.2f",$taxamount);           
        unless ($current_txn == $txnnum) {            
            if ($current_txn > 0) {            
                # We have started a new transaction.  Add the total for the previous transaction
                my @array=(
                    "Txn $current_txn Total:",
                    "",
                    "",
                    "",                    
                    "$txn_total{'orig'}",
                    "$txn_total{'amt'}",
                    "$txn_total{'tax'}",

                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",                    
                    "\$$txn_total{'disc'}",
                    "",
                    "",
                    ""
                );
                push(@$tbl_ref2,\@array);
                my @array2=(
                    "Txn $current_txn Total:",                    
                    "$txn_total{'orig'}",
                    "$txn_total{'amt'}",
                    "$txn_total{'tax'}",                            
                    "$txn_total{'disc'}",                
                );
                push(@$tbl_ref3,\@array2);
                # Reset the values of the hash
                %txn_total=();
            }            
            $current_txn=$txnnum;            
            
        }
        push(@$tbl_ref2,$thisrow);
        unless ($tender eq "--Aborted--") {
            $txn_total{"orig"}+=sprintf("%.2f",$origamount); 
            $txn_total{"amt"}+=sprintf("%.2f",$amount); 
            $txn_total{"tax"}+=sprintf("%.2f",$taxamount); 
            $txn_total{"disc"}+=sprintf("%.2f",$discamount); 
            $report_total{"orig"}+=sprintf("%.2f",$origamount); 
            $report_total{"amt"}+=sprintf("%.2f",$amount); 
            $report_total{"tax"}+=sprintf("%.2f",$taxamount); 
            $report_total{"disc"}+=sprintf("%.2f",$discamount);         
            
            $dtotal+=$discamount;   # Discount total
        }
    }
    # Add the  totals for the last transaction
    my @array=(
        "Txn $current_txn Total:",
        "",
        "",
        "",        
        "$txn_total{'orig'}",
        "$txn_total{'amt'}",
        "$txn_total{'tax'}",

        "",
        "",
        "",
        "",
        "",
        "",
        "",        
        "\$$txn_total{'disc'}",
        ""
    );
    push(@$tbl_ref2,\@array);
    my @array2=(
        "Txn $current_txn Total:",                    
        "$txn_total{'orig'}",
        "$txn_total{'amt'}",
        "$txn_total{'tax'}",                            
        "$txn_total{'disc'}",                
    );
    push(@$tbl_ref3,\@array2);    
    # Now add the report totals:
    my @array3=(
        "Report Total:",
        "",
        "",
        "",        
        "$report_total{'orig'}",
        "$report_total{'amt'}",
        "$report_total{'tax'}",

        "",
        "",
        "",
        "",
        "",
        "",
        "",        
        "\$$report_total{'disc'}",
        "",
        "",
        ""
    );
    push(@$tbl_ref2,\@array3);    
    
    
    # Reset the values of the hash
    %txn_total=();
    my @format = (
        "",
        "",
        "",
        {'align' => 'Right', 'num' => '.0'},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",

        {'align' => 'Right', 'currency' => ''},
                "",
        "",
    );      
    TTV::cgiutilities::printResult_arrayref2($tbl_ref2, '', \@format); 
    $sth = $dbh->prepare($query);
    $sth->execute();

    @header=(
    "Tender",
    "Number of Items",
    "Number of Transactions",
    "Net Sales Amount",
    "Gross Sales Amount"
    );
    my @tender_report=(\@header);
    my $tcount=0;
    my $txncount=0;
    my $ttotal=0;   # Total amount
    my $gtotal=0;   # Gross total

    my %txn_count;
    foreach my $txn (sort keys(%txn_tender)) {
        # Trying to account for split tenders.  That is, one transaction may have more than one tender.
        my @tmp=split(/,/,$txn_tender{$txn});
        foreach my $t (@tmp) {
            $txn_count{$t}++;
        }
    }
    foreach my $tender (sort keys(%tender_count)) {
        my @line=(
            "$tender",
            "$tender_count{$tender}",
            "$txn_count{$tender}",
            "\$$tender_amount{$tender}",
            "\$$gross_tender_amount{$tender}"
        );
        $tcount+=$tender_count{$tender};
        $txncount+=$txn_count{$tender};
        $ttotal+=$tender_amount{$tender};
        $gtotal+=$gross_tender_amount{$tender};
        #print $q->p("Tender $tender - $tender_count{$tender} sales");
        #print $q->p("Tender $tender - \$$tender_amount{$tender}");
        push(@tender_report,\@line);
    }
    my @total_line=(
        "Total",
        "$tcount",
        "$txncount",
        "\$$ttotal",
        "\$$gtotal"
    );
    push(@tender_report,\@total_line);
    TTV::cgiutilities::printResult_arrayref2(\@tender_report, '', ''); 
    
    my @totals;
    @header=(        
        "Total Net less Gift Card Sales",
        "Total Discounts",
        "Total Net less Gift Card Sales & Discounts",
        "Gross less Gift Card Sales",
        "Gross less Gift Card Sales & Discounts",        
    );
    push(@totals,\@header);
    my $tnet=sprintf("%.2f",($ttotal-$tender_amount{'GIFT CARD'})); 
    my $tnetd=sprintf("%.2f",($ttotal-$tender_amount{'GIFT CARD'} - $dtotal)); 
    my $tgrs=sprintf("%.2f",($gtotal-$tender_amount{'GIFT CARD'}));
    my $tgrsd=sprintf("%.2f",($gtotal-$tender_amount{'GIFT CARD'} - $dtotal));
    @total_line=(
        "\$$tnet",
        "\$$dtotal",
        "\$$tnetd",
        "\$$tgrs",
        "\$$tgrsd"
    );
    push(@totals,\@total_line);
    TTV::cgiutilities::printResult_arrayref2(\@totals, '', ''); 
    # Collect a report total for the txn totals
    my %t_hash;
    for my $datarows (1 .. $#$tbl_ref3) {
         my $thisrow = @$tbl_ref3[$datarows];    
         $t_hash{1}+=$$thisrow[1];
         $t_hash{2}+=$$thisrow[2];
         $t_hash{3}+=$$thisrow[3];
         $t_hash{4}+=$$thisrow[4];
    }
    my @array4=(
        "Total",
        "$t_hash{1}",
        "$t_hash{2}",
        "$t_hash{3}",
        "$t_hash{4}"
    );
    push(@$tbl_ref3,\@array4);    
    @format = (
        "",
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''}
        );
                     
    TTV::cgiutilities::printResult_arrayref2($tbl_ref3, '', \@format);     
    StandardFooter();
    
    
} elsif (($tool eq "SKUACTRPT") || ($tool eq "SKUINVACTRPT") || ($tool eq "SKUMODRPT")) { 
    # Note that this is used for two slightly different reports
    my $report_page="SKUACTRPT_DC";
    if ($tool eq "SKUINVACTRPT") {
        $report_page="SKUINVACTRPT_DC";
        print $q->h3("SKU Inventory Activity Report - Beta");
    } elsif ($tool eq "SKUMODRPT") {
        $report_page="SKUMODRPT_DC";
        print $q->h3("SKU Modification Report - Beta");    
    } else {
        print $q->h3("SKU Activity Report - Beta");
    }

	# Ask which day to do the report fo
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"$report_page"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

	my @ltm = localtime(time());
	my $month=$ltm[4]+1;
	my $year=$ltm[5]+1900;
	my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
	my $hm = 1;
	my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
	$startdate = $enddate; # for now.

	my $calcounter = 1;
	if ($tool eq "SKUINVACTRPT") {
		print $q->h4("Note: To see inventory on a specific day, query for the following day.  
		This is because all inventory activity for a given day is recorded at 1:00am the following day.");
	}
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
			   $q->td({-class=>'tabledatanb', -align=>'left'},
					  $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
					  $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
					  $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
							 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
							$q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
						   ),
					 )
			  );
	$calcounter++;
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
			   $q->td({-class=>'tabledatanb', -align=>'left'},
					  $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
					  $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
					  $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
							 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
							$q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
						   )
					 )
			  );

	print $q->Tr(
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;Enter <u>S</u>KU:\&nbsp"),
			   $q->td({-class=>'tabledatanb', -align=>'right'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"SKU", -size=>"12"}))
			  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
			   $q->input({-accesskey=>'D', -type=>"checkbox", -name=>"cb_like"}) .
			   $q->font({-size=>-1}, "Fin<u>d</u> SKUs starting with ...")
			  ));   
	if ($tool eq "SKUINVACTRPT") {
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"radio", -name=>"rpt_opt", -value=>"0", }) .            
                   $q->font({-size=>2}, "Show Sales")));
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"radio", -name=>"rpt_opt", -value=>"1",}) .            
                   $q->font({-size=>2}, "Show Receiving")));                   
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"radio", -name=>"rpt_opt", -value=>"2",}) .            
                   $q->font({-size=>2}, "Show Transfer Out")));  
	}	
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
			  );
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";
	#print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");


    StandardFooter();

} elsif ($report eq "SKUINVACTRPT_DC") {
    print $q->h3("SKU Inventory Activity Data Compilation - Beta");

    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);

    my $sku   = trim(($q->param("SKU"))[0]);
    my $like =  trim(($q->param("cb_like"))[0]);
	my $rpt_opt =  trim(($q->param("rpt_opt"))[0]);
	

    my $where;
	my $rpt_where;
    my $label;
    if ($sku) {
        if ($like) {
            $where=" and eia.plunum like '$sku%' ";           
            $label= "Report limited to SKUs starting with $sku";
        } else {
            $where=" and eia.plunum like '$sku' ";
            $label= "Report limited  SKU $sku";
        }
    }
	if ($rpt_opt == 0) {
		$rpt_where=" and eia.qtysold <> 0";		 
	}
	if ($rpt_opt == 1) {
		$rpt_where=" and eia.qtyreceived <> 0";		 
	}	
	if ($rpt_opt == 2) {
		$rpt_where=" and eia.qtytransferedout <> 0";		 
	}		
 
    $query = dequote(<< "    ENDQUERY");         
        select
            *
        from
            elderInvActivity eia            
        where
            inventoryDate between '$startdate' and '$enddate 23:59:59.999'  
            $where
			$rpt_where
        and
            (eia.qtysold > 0 or eia.qtyreturn >0 or eia.qtyreceived > 0 or eia.qtyordered > 0
            or eia.qtyadjusted > 0 or eia.qtytransferedin > 0 or eia.qtytransferedout > 0)
            
        order by 
            eia.plunum 
        
    ENDQUERY
	
	#print $q->pre("$query");
    my $tbl_ref = execQuery_arrayref($dbh, $query);  

    if (recordCount($tbl_ref)) { 
        
        print "This report shows what the entry in the elderInvActivity table showed for this sku<br />";
        print "on the specified date.  Only SKUs with activity are reported. <br /><br />";
        print "$label";
        my @format = ('', 
            '',   
            {'align' => 'Right', 'num' => '.0'},  
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'num' => '.0'},  
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Right', 'currency' => ''},            
            '');
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
    } else {
        print $q->p("There is no data to report");
    }    
    StandardFooter();
 
} elsif ($report eq "SKUMODRPT_DC") {
    print $q->h3("SKU Modification Report - Beta");    
    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);
    my $sku   = trim(($q->param("SKU"))[0]);
    my $datewhere = "";
    my $daterange = "";
    my $like =  trim(($q->param("cb_like"))[0]);
    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
           print "Startdate or Enddate format not correct";
           StandardFooter();
           exit;
        }
        $datewhere = " timeofupdate between '$startdate' and '$enddate 23:59:59.999'  ";
 
        $daterange = " date range $startdate - $enddate";
    } else {
       print "Start date or End date is blank";
       StandardFooter();
       exit;
    }
    my $where;
    if ($sku) {
        if ($like) {
            $where="and updateditem like '$sku%'";
        } else {
            $where="and updateditem like '$sku%'";
        }
    }    
    #print "Audit update types:<br />";
    #print "-> 1 = 'added'<br />";
    #print "-> 2 = 'modified'<br />";
    #print "-> 3 = 'deleted'<br />";
 
    
	$query = dequote(<< "	ENDQUERY");
        select * from update_audit 
        where
            $datewhere         
            $where
        order by timeofupdate desc
	ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my $tbl_ref2= [];
    if (recordCount($tbl_ref)) {
        for my $datarows (0 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];   
            if ($$thisrow[4] == 1) {
                $$thisrow[4] = "1 - added";
            }
            if ($$thisrow[4] == 2) {
                $$thisrow[4] = "2 - modified";
            }            
            if ($$thisrow[4] == 3) {
                $$thisrow[4] = "3 - deleted";
            }                        
            push @$tbl_ref2, $thisrow;
        }        
        print $q->p("update_audit Table: entries including $sku");
        printResult_arrayref2($tbl_ref2); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }
    $dbh->disconnect;    
    StandardFooter();       
} elsif ($report eq "SKUACTRPT_DC_orig") {
    print $q->h3("SKU Activity Data Compilation - Beta");

    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);

    my $sku   = trim(($q->param("SKU"))[0]);
    my $like =  trim(($q->param("cb_like"))[0]);

    my $datewhere_ms = "";
    my $datewhere_tm = "";
    my $datewhere_db = "";
    my $daterange = "";

    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
           print "Startdate or Enddate format not correct";
           StandardFooter();
           exit;
        }
        $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
        $daterange = " date range $startdate - $enddate";
    } else {
       print "Start date or End date is blank";
       StandardFooter();
       exit;
    }

    print $q->p("Compiling data for $daterange.");
    my $dbh = openODBCDriver($dsn);
    my $where;
    if ($sku) {
        if ($like) {
            $where="and p.plunum like '$sku%'";
        } else {
            $where="and p.plunum like '$sku'";
        }
    }

    my %plu_hash;
    my %dept_hash;
    my %desc_hash;
    my %sales_hash;
    my %received_hash;
    my %adjusted_hash;
    my %xfrin_hash;
    my %xfrout_hash;
    my %postvoid_hash;
    my %return_hash;
    my %transaction_hash;
	
	# To collect details
	my %date_hash;
    
    #Sales
    $query = dequote(<< "    ENDQUERY");         
         select
            p.plunum, 
            p.deptnum,
            p.pludesc,
            sum(ms.qty) as Sales,
            tpt.regnum,
            ms.txnnum
         from
            plu p                       
            join Txn_Merchandise_Sale ms on ms.skunum=p.plunum   
            join txn_pos_transactions tpt on tpt.transnum=ms.transnum and tpt.txnnum=ms.txnnum
         where
            ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999'  
            and ms.extsellprice >= 0
            $where
            
        group by p.plunum,p.deptnum,p.pludesc,tpt.regnum,ms.txnnum
        order by p.plunum
    ENDQUERY

    my $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $sales=$tmp[3];
        my $reg=$tmp[4];
        my $txn=$tmp[5];
        my $txn_key="$reg"."-"."$txn";
 
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $sales_hash{$plu}+=$sales;
        $transaction_hash{$plu}.=" $txn_key";        
    }
    
    #Returns
    $query = dequote(<< "    ENDQUERY");         
         select
            p.plunum, 
            p.deptnum,
            p.pludesc,
            sum(ms.qty) as Sales,
            tpt.regnum,
            ms.txnnum
         from
            plu p                       
            join Txn_Merchandise_Sale ms on ms.skunum=p.plunum      
            join txn_pos_transactions tpt on tpt.transnum=ms.transnum and tpt.txnnum=ms.txnnum            
         where
            ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999'  
            and ms.extsellprice <= 0
            $where
            
        group by p.plunum,p.deptnum,p.pludesc,tpt.regnum,ms.txnnum
        order by p.plunum
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $return=$tmp[3];
        my $reg=$tmp[4];
        my $txn=$tmp[5];
        my $txn_key="$reg"."-"."$txn";
        
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $return_hash{$plu}=$return;
 
        $transaction_hash{$plu}.=" $txn_key";    
              
    }    

    # Post Voids
    $query = dequote(<< "    ENDQUERY");   
         select
            p.plunum, 
            p.deptnum,
            p.pludesc,
            sum(ms.qty) as PostVoid,
            tpt.regnum,
            ms.txnnum         
         from
            plu p                       
            join Txn_Merchandise_Sale ms on ms.skunum=p.plunum   
            join txn_miscellaneous_trans mt on mt.transnum=ms.transnum and mt.txnnum=ms.txnnum
            join txn_pos_transactions tpt on tpt.transnum=ms.transnum and tpt.txnnum=ms.txnnum
         where
            ms.itemdatetime > '$startdate'        
        and 
            mt.misctype = 2
            $where
        group by p.plunum,p.deptnum,p.pludesc,tpt.regnum,ms.txnnum
        order by p.plunum
    ENDQUERY
 
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $void=$tmp[3];
        my $reg=$tmp[4];
        my $txn=$tmp[5];
        my $txn_key="$reg"."-"."$txn";
        
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $postvoid_hash{$plu}=$void;
 
        $transaction_hash{$plu}.=" $txn_key"; 
    }    
        
    # Received
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        sum(ia.qty) as Received

     from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum            
        
     where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 3
        $where
    group by p.plunum,p.deptnum,p.pludesc
    order by p.plunum
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $rec=$tmp[3];

        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $received_hash{$plu}=$rec;  
    }
    
    # Adjusted
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        sum(ia.qty) as Adjusted
     from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum            
        
     where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 5
        $where            
    group by p.plunum,p.deptnum,p.pludesc
    order by p.plunum  
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $adj=$tmp[3];

        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $adjusted_hash{$plu}=$adj;  
    }  

    # Transfer Out
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        sum(ia.qty) as XfrOut
    from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum                        
    where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 7
        $where            
    group by p.plunum,p.deptnum,p.pludesc
    order by p.plunum    
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $xout=$tmp[3];
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $xfrout_hash{$plu}=$xout;  
    }  
    
    # Transfer In
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        sum(ia.qty) as XfrIn
     from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum                        
     where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 6
        $where            
    group by p.plunum,p.deptnum,p.pludesc
    order by p.plunum  
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $xin=$tmp[3];
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $xfrin_hash{$plu}=$xin;      

     }
    my @header=(
        "SKU",
        "Dept",
        "Desc",
        "Sold",
        "Returns",
        "PostVoid",
        "Received",
        "Transfer In",
        "Transfer Out",
        "Adjusted",
        "Sales/Returns Txns"
    );
    my @sku_info=(\@header);;
    my @totals=("Total");
    foreach my $plu(sort keys(%plu_hash)) {
        my $dept=$dept_hash{$plu};
        my $desc=$desc_hash{$plu};
        my $sales=sprintf("%d",$sales_hash{$plu});
        my $return=sprintf("%d",$return_hash{$plu});
        my $received=sprintf("%d",$received_hash{$plu});
        my $xfrin=sprintf("%d",$xfrin_hash{$plu});
        my $xfrout=sprintf("%d",$xfrout_hash{$plu});
        my $adjusted=sprintf("%d",$adjusted_hash{$plu});
        my $void=sprintf("%d",$postvoid_hash{$plu});      
        my $transactions;        
   
        my $raw=$transaction_hash{$plu};
		my $transaction_count=keys(%transaction_hash);
 
        # The transactions are a string of register number and txn num
        my @tmp=split(/\s+/,$raw);
        my $sep;
        foreach my $t (@tmp) {
		 
            # Now split the register from the txnnum
            my @tmp2=split(/-/,$t);
			my $reg=$tmp2[0];
			my $txn=$tmp2[1];
			if (($reg) && ($txn)) {
				$transactions.="$sep Reg: $reg Txn: $txn";
				$sep=",";
			}
        }
  
        $totals[3]+=$sales;
        $totals[4]+=$return;
        $totals[5]+=$void;
        $totals[6]+=$received;
        $totals[7]+=$xfrin;
        $totals[8]+=$xfrout;
        $totals[9]+=$adjusted;
		$totals[10]+=$transaction_count;
        
        my @array=(
            "$plu",
            "$dept",
            "$desc",
            "$sales",
            "$return",
            "$void",
            "$received",
            "$xfrin",
            "$xfrout",
            "$adjusted",
            "$transactions"
        );
        push(@sku_info,\@array);
    }
    push(@sku_info,\@totals);
    
    if (keys(%plu_hash)) {
        #my @links = ( "$scriptname?report=SKUACTRPT_TXN&txn=$transaction_hash{$_}&".'skuid=$_' ); 
        TTV::cgiutilities::printResult_arrayref2(\@sku_info, '', ''); 
    } else {
        print $q->p("No data found");
    }
    

    StandardFooter();
	
} elsif ($report eq "SKUACTRPT_DC") {

    print $q->h3("SKU Activity Data Compilation - Beta");

    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);

    my $sku   = trim(($q->param("SKU"))[0]);
    my $like =  trim(($q->param("cb_like"))[0]);

    my $datewhere_ms = "";
    my $datewhere_tm = "";
    my $datewhere_db = "";
    my $daterange = "";

    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
           print "Startdate or Enddate format not correct";
           StandardFooter();
           exit;
        }
        $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
        $daterange = " date range $startdate - $enddate";
    } else {
       print "Start date or End date is blank";
       StandardFooter();
       exit;
    }

    print $q->p("Compiling data for $daterange.");
    my $dbh = openODBCDriver($dsn);
    my $where;
    if ($sku) {
        if ($like) {
            $where="and p.plunum like '$sku%'";
        } else {
            $where="and p.plunum like '$sku'";
        }
    }

    my %plu_hash;
    my %dept_hash;
    my %desc_hash;
    my %sales_hash;
    my %received_hash;
    my %adjusted_hash;
    my %xfrin_hash;
    my %xfrout_hash;
    my %postvoid_hash;
    my %return_hash;
    my %transaction_hash;
	
	# To collect details
	my %date_hash;

    #Sales
    $query = dequote(<< "    ENDQUERY");         
         select
            p.plunum, 
            p.deptnum,
            p.pludesc,
            ms.qty,
            tpt.regnum,
            ms.txnnum,
			convert(varchar(10),ms.itemdatetime, 21) 
         from
            plu p                       
            join Txn_Merchandise_Sale ms on ms.skunum=p.plunum   
            join txn_pos_transactions tpt on tpt.transnum=ms.transnum and tpt.txnnum=ms.txnnum
         where
            ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999'  
            and ms.extsellprice >= 0
            $where
            
 
    ENDQUERY
 
    my $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $sales=$tmp[3];
        my $reg=$tmp[4];
        my $txn=$tmp[5];
		my $date=$tmp[6];
        my $txn_key="$reg"."-"."$txn";
 
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $sales_hash{$plu}+=$sales;
        $transaction_hash{$plu}.=" $txn_key";  
		$date_hash{$date}->[0]+=$sales;
	}
  
    #Returns
    $query = dequote(<< "    ENDQUERY");         
         select
            p.plunum, 
            p.deptnum,
            p.pludesc,
            ms.qty,
            tpt.regnum,
            ms.txnnum,
			convert(varchar(10),ms.itemdatetime, 21) 			
         from
            plu p                       
            join Txn_Merchandise_Sale ms on ms.skunum=p.plunum      
            join txn_pos_transactions tpt on tpt.transnum=ms.transnum and tpt.txnnum=ms.txnnum            
         where
            ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999'  
            and ms.extsellprice <= 0
            $where
            
 
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $return=$tmp[3];
        my $reg=$tmp[4];
        my $txn=$tmp[5];
		my $date=$tmp[6];
        my $txn_key="$reg"."-"."$txn";
        
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $return_hash{$plu}+=$return;
 
        $transaction_hash{$plu}.=" $txn_key";    
		$date_hash{$date}->[1]+=$return;              
    }    

    # Post Voids
    $query = dequote(<< "    ENDQUERY");   
         select
            p.plunum, 
            p.deptnum,
            p.pludesc,
            ms.qty,
            tpt.regnum,
            ms.txnnum,
			convert(varchar(10),ms.itemdatetime, 21) 			
         from
            plu p                       
            join Txn_Merchandise_Sale ms on ms.skunum=p.plunum   
            join txn_miscellaneous_trans mt on mt.transnum=ms.transnum and mt.txnnum=ms.txnnum
            join txn_pos_transactions tpt on tpt.transnum=ms.transnum and tpt.txnnum=ms.txnnum
         where
            ms.itemdatetime > '$startdate'        
        and 
            mt.misctype = 2
            $where
 
    ENDQUERY
 
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $void=$tmp[3];
        my $reg=$tmp[4];
        my $txn=$tmp[5];
		my $date=$tmp[6];		
        my $txn_key="$reg"."-"."$txn";
        
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $postvoid_hash{$plu}+=$void;
 
        $transaction_hash{$plu}.=" $txn_key"; 
		$date_hash{$date}->[2]+=$void; 		
    }    
        
    # Received
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        ia.qty,
		convert(varchar(10),ia.adjustDate, 21) 

     from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum            
        
     where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 3
        $where
 
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $rec=$tmp[3];
		my $date=$tmp[4];
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $received_hash{$plu}+=$rec; 
		$date_hash{$date}->[3]+=$rec; 		
    }
   
    # Adjusted
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        ia.qty,
		convert(varchar(10),ia.adjustDate, 21) 
     from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum            
        
     where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 5
        $where            
 
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $adj=$tmp[3];
		my $date=$tmp[4];
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $adjusted_hash{$plu}+=$adj; 
		$date_hash{$date}->[6]+=$adj; 		
    }  

    # Transfer Out
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        ia.qty,
		convert(varchar(10),ia.adjustDate, 21) 
    from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum                        
    where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 7
        $where            
 
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $xout=$tmp[3];
		my $date=$tmp[4];
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $xfrout_hash{$plu}+=$xout; 
		$date_hash{$date}->[5]+=$xout; 		
    }  
    
    # Transfer In
    $query = dequote(<< "    ENDQUERY");     
    select
        p.plunum, 
        p.deptnum,
        p.pludesc,
        ia.qty,
		convert(varchar(10),ia.adjustDate, 21) 
     from
        plu p                                     
        join inventory_adjustment ia on p.plunum=ia.plunum                        
     where            
        ia.adjustDate between '$startdate' and '$enddate 23:59:59.999' 
        and ia.functionid = 6
        $where            
 
    ENDQUERY
                            
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        my $plu=$tmp[0];
        my $dept=$tmp[1];
        my $desc=$tmp[2];
        my $xin=$tmp[3];
		my $date=$tmp[4];
        $plu_hash{$plu}=$plu;
        $dept_hash{$plu}=$dept;
        $desc_hash{$plu}=$desc;
        $xfrin_hash{$plu}+=$xin; 
		$date_hash{$date}->[4]+=$xin; 		

     }
 	 
    my @detail_header=(
		"Date",
        "Sold",
        "Returns",
        "PostVoid",
        "Received",
        "Transfer In",
        "Transfer Out",
        "Adjusted"
 
    );	 
	my @sku_detail_info=(\@detail_header);

    foreach my $date (sort keys(%date_hash)) {

		my $ref=$date_hash{$date};
		my @array2=@$ref;		
		my $sold=$date_hash{$date}->[0];		 
		my $return=$date_hash{$date}->[1];
		my $pvoid=$date_hash{$date}->[2];	
		my $received=$date_hash{$date}->[3];
		my $txferIn=$date_hash{$date}->[4];
		my $txferOut=$date_hash{$date}->[5];
		my $adjusted=$date_hash{$date}->[6];
        my @array=(
            "$date",
            "$sold",
            "$return",
            "$pvoid",
            "$received",
            "$txferIn",
            "$txferOut",
            "$adjusted" 
        );			
        push(@sku_detail_info,\@array);	 		
	}

    if (keys(%date_hash)) {
		print $q->h4("Details:");
        TTV::cgiutilities::printResult_arrayref2(\@sku_detail_info, '', ''); 
    } else {
        print $q->p("No detail data found");
    }	
	 	
 
	# The totals
    my @header=(
        "SKU",
        "Dept",
        "Desc",
        "Sold",
        "Returns",
        "PostVoid",
        "Received",
        "Transfer In",
        "Transfer Out",
        "Adjusted",

    );
    my @sku_info=(\@header);;
    my @totals=("Total");
    foreach my $plu(sort keys(%plu_hash)) {
        my $dept=$dept_hash{$plu};
        my $desc=$desc_hash{$plu};
        my $sales=sprintf("%d",$sales_hash{$plu});
        my $return=sprintf("%d",$return_hash{$plu});
        my $received=sprintf("%d",$received_hash{$plu});
        my $xfrin=sprintf("%d",$xfrin_hash{$plu});
        my $xfrout=sprintf("%d",$xfrout_hash{$plu});
        my $adjusted=sprintf("%d",$adjusted_hash{$plu});
        my $void=sprintf("%d",$postvoid_hash{$plu});      
        my $transactions;        
   
        my $raw=$transaction_hash{$plu};
		my $transaction_count=keys(%transaction_hash);
 
        # The transactions are a string of register number and txn num
        my @tmp=split(/\s+/,$raw);
        my $sep;
        foreach my $t (@tmp) {
		 
            # Now split the register from the txnnum
            my @tmp2=split(/-/,$t);
			my $reg=$tmp2[0];
			my $txn=$tmp2[1];
			if (($reg) && ($txn)) {
				$transactions.="$sep Reg: $reg Txn: $txn";
				$sep=",";
			}
        }
  
        $totals[3]+=$sales;
        $totals[4]+=$return;
        $totals[5]+=$void;
        $totals[6]+=$received;
        $totals[7]+=$xfrin;
        $totals[8]+=$xfrout;
        $totals[9]+=$adjusted;
		#$totals[10]+=$transaction_count;
        
        my @array=(
            "$plu",
            "$dept",
            "$desc",
            "$sales",
            "$return",
            "$void",
            "$received",
            "$xfrin",
            "$xfrout",
            "$adjusted",

        );
        push(@sku_info,\@array);
    }
    push(@sku_info,\@totals);
    
    if (keys(%plu_hash)) {
        print $q->p();
		print $q->h4("Totals:");
        TTV::cgiutilities::printResult_arrayref2(\@sku_info, '', ''); 
    } else {
        print $q->p("No totals data found");
    }
    

    StandardFooter();
 	
	
} elsif ($tool eq "SKUACTRPT_TXN") {    
    print $q->h3("SKU Activity Report: Transactions ");
    my $sku   = trim(($q->param("skuid"))[0]);  
    my $txn   = trim(($q->param("txn"))[0]); 
    
    StandardFooter();
} elsif ($tool eq "CURVEN_QRY") {    
    print $q->h3("Current Vendor Report");   

    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"CURVEN_RPT"});
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:"))); 
 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"checkbox", -name=>"unload_to_file", -value=>"1",}) .            
                   $q->font({-size=>2}, "Export to a file")));  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"report_opt", -value=>"0", -checked=>"1"}) .            
               $q->font({-size=>2}, "CSV")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"report_opt", -value=>"1",}) .            
               $q->font({-size=>2}, "TSV")));  
                    
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );
      
    print $q->end_table(),"\n";

    print $q->end_form(), "\n";    
    StandardFooter();  
} elsif ($tool eq "CURVEN_RPT") {    
    print $q->h3("Current Vendor Report ");  
 
    my $unload_to_file = trim(($q->param("unload_to_file"))[0]);
 
    
    my $report_opt = trim(($q->param("report_opt"))[0]); 
    my $suffix="csv";
    if ($report_opt) {
        $suffix="tsv";
    }     
    my $unload_file="c:/temp/vendor.${suffix}";
    my $label;
    my $where;
    my $vendorname;
 
    # Get list of vendors
    
    
    my $dbh = openODBCDriver($dsn);    
    $query = "select VendorId, VendorName from Vendor";
    $tbl_ref = execQuery_arrayref($dbh, $query);    
 


    if (recordCount($tbl_ref)) {         
        print $q->p("Vendors");
        TTV::cgiutilities::printResult_arrayref2($tbl_ref);   
        if ($unload_to_file) {
            my @array_to_print=@$tbl_ref;
            open(OUTPUT,">$unload_file"); 
            #print OUTPUT "$label";
            #print OUTPUT "\nVendor List\n";
            foreach my $ref (@array_to_print) {
                my @output_array=@$ref;
                foreach my $line (@output_array) {
                    print OUTPUT "$line\t";
                }        
                print OUTPUT "\n";
            }
            close OUTPUT;
            print $q->p("Output results to $unload_file");
        }
 
 
    } else {
        print $q->p("No records found.");
    }
    StandardFooter();     
} elsif ($tool eq "CURINV_QRY") {    
    print $q->h3("Current Inventory Report - Beta");   
    # Ask which vendor to do the report for
    my $dbh = openODBCDriver($dsn);    
    $query = "select VendorId, VendorName from Vendor";
    $tbl_ref = execQuery_arrayref($dbh, $query);

    my $vendor_options = $q->option({-value=>'ALL' -selected=>undef},"All Vendors");
    for my $datarows (1 .. $#$tbl_ref)
    {
        my $thisrow = @$tbl_ref[$datarows];
        my $VendorId = trim($$thisrow[0]);
        my $VendorName = trim($$thisrow[1]);
        $vendor_options .= $q->option({-value=>$VendorId}, "$VendorId - $VendorName");
    }
    $dbh->disconnect;
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"CURINV_RPT"});
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:"))); 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Ven<u>d</u>or:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"VendorId", -size=>"1"}), $vendor_options)
              );  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"checkbox", -name=>"unload_to_file", -value=>"1",}) .            
                   $q->font({-size=>2}, "Export to a file")));  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"report_opt", -value=>"0", -checked=>"1"}) .            
               $q->font({-size=>2}, "CSV")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"report_opt", -value=>"1",}) .            
               $q->font({-size=>2}, "TSV")));  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"checkbox", -name=>"include_header", -value=>"1",}) .            
                   $q->font({-size=>2}, "Include Totals Summary Header"))); 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-type=>"checkbox", -name=>"sku_w_qty", -value=>"1",}) .            
                   $q->font({-size=>2}, "Only SKUs w. Qty")));                     
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );
      
    print $q->end_table(),"\n";

    print $q->end_form(), "\n";    
    StandardFooter();
} elsif ($tool eq "CURINV_RPT") {    
    print $q->h3("Current Inventory Report ");  
    my $dbh = openODBCDriver($dsn);  
    my $VendorId = trim(($q->param("VendorId"))[0]);
    my $unload_to_file = trim(($q->param("unload_to_file"))[0]);
    my $sku_w_qty = trim(($q->param("sku_w_qty"))[0]);    
    my $include_header = trim(($q->param("include_header"))[0]);
    my $report_opt = trim(($q->param("report_opt"))[0]); 
    my $suffix="csv";
    if ($report_opt) {
        $suffix="tsv";
    }
    my $unload_file;
    my $label;
    my $where;
    my $vendorname;
    if ($VendorId) {
        $where = "where p.vendorid = '$VendorId'";
        # Display the vendor
        $query="select VendorName from vendor where vendorid = '$VendorId'";
        
        my $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) { 
            $vendorname=$tmp[0];
        }
        $label="Inventory for Vendor $vendorname  - ID: $VendorId";        
        $unload_file="c:/temp/inventory_vendor_${VendorId}.${suffix}";        
    } else {       
       $label="Inventory for all Vendors"; 
       $unload_file="c:/temp/inventory_all_vendors.${suffix}";
    }
    if ($sku_w_qty) {
        if ($where) {
            $where .= " and ic.qtyCurrent > 0 ";
        } else {
            $where = "where ic.qtyCurrent > 0 ";
        }
    }
    print $q->p("$label");
    # Get inventory for specified vendors
 
    $query = "
        select 
            p.plunum as SKU,
            pcr.xrefnum as BarCode,
            p.pludesc as Description,
            p.vendorid as Vendor,
            p.deptnum as DeptNum,
            substr(d.deptdescription,5) as Dept,
			uf3.codename as AkronStatus,
            ic.qtycurrent as Qty,            
            ic.lastvendorcost as LastCost,
            p.cost as AvgCost,
            p.retailprice as Price,
            ic.minqty as Min,
            ic.maxqty as Max
        from
            plu p
            join inventory_current ic on p.plunum = ic.plunum
            left outer join plu_cross_ref pcr on p.plunum = pcr.plunum
            left outer join department d on d.deptnum = p.deptnum
			left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3
        $where
        order by
            p.vendorid, p.plunum
            ";
    $tbl_ref = execQuery_arrayref($dbh, $query);
 
    if (recordCount($tbl_ref)) { 
        if ($include_header) {
            # First show the inventory total
            $query = "
                select                 
                    sum(ic.qtycurrent) as Qty,               
                    sum(ic.lastvendorcost * ic.qtycurrent) as LastCost,
                    sum(p.cost * ic.qtycurrent) as AvgCost,
                    sum(p.retailprice * ic.qtycurrent) as Price
                    
                from
                    plu p
                    join inventory_current ic on p.plunum = ic.plunum
                $where
     
                    ";
            my $tbl_ref2 = execQuery_arrayref($dbh, $query); 
            my @format = (
                {'align' => 'Right', 'num' => '.0'},     
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},            
                );
            print $q->p("Inventory Totals");
            TTV::cgiutilities::printResult_arrayref2($tbl_ref2, '', \@format);   
            if ($unload_to_file) {
                my @array_to_print=@$tbl_ref2;
                open(OUTPUT,">$unload_file"); 
                print OUTPUT "$label";
                print OUTPUT "\nInventory Totals\n";
                foreach my $ref (@array_to_print) {
                    my @output_array=@$ref;              
                    foreach my $line (@output_array) {
                        print OUTPUT "$line\t";
                    }        
                    print OUTPUT "\n";
                }
                close OUTPUT;
                print $q->p("Output results to $unload_file");
            }
        }
        print $q->p("Inventory Details");
        my @format = ('', '','',
            
            {'align' => 'Left', 'num' => '.0'},  
            {'align' => 'Right', 'num' => '.0'}, 
            '',
            {'align' => 'Right', 'num' => '.0'},  
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'num' => '.0'}, 
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Right', 'currency' => ''}, 
            {'align' => 'Right', 'currency' => ''},            
            '');
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
        if ($unload_to_file) {
 
            my @array_to_print=@$tbl_ref;
            
            if ($include_header) {
                open(OUTPUT,">>$unload_file");
                print OUTPUT "\nInventory Details\n";
            } else {
                open(OUTPUT,">$unload_file");
            }
            foreach my $ref (@array_to_print) {            
                my @output_array=@$ref;         
                foreach my $line (@output_array) {
                   
                    if ($report_opt) {
                        print OUTPUT "$line\t";
                    } else {
                       print OUTPUT "$line,";
                    }
              
                }          
                print OUTPUT "\n";                
            }
            close OUTPUT;            
        }        
        # Show items with negative Qty
        $query = "
            select 
                p.plunum as SKU,
                p.pludesc as Description,
                p.vendorid as Vendor,
                p.deptnum as Dept,
                ic.qtycurrent as Qty,            
                ic.lastvendorcost as LastCost,
                p.cost as AvgCost,
                p.retailprice as Price,
                ic.minqty as Min,
                ic.maxqty as Max
            from
                plu p
                join inventory_current ic on p.plunum = ic.plunum
            $where
            and
                ic.qtycurrent < 0
            order by
                p.vendorid, p.plunum
                ";
        $tbl_ref = execQuery_arrayref($dbh, $query);    
        if (recordCount($tbl_ref)) { 
            print $q->p("Negative Inventory");
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
            if ($unload_to_file) {
                my @array_to_print=@$tbl_ref;
                open(OUTPUT,">>$unload_file");
                print OUTPUT "\nNegative Inventory\n";
                foreach my $ref (@array_to_print) {
                    my @output_array=@$ref;
                    foreach my $line (@output_array) {
                        print OUTPUT "$line\t";
                    }          
                    print OUTPUT "\n";                
                }
                close OUTPUT;            
            }             
        } else {
            print $q->p("No negative inventory found.");
        }
    } else {
        print $q->p("No records found.");
    }
    StandardFooter();    
} elsif (($tool eq "SKUINVRPT") || ($tool eq "SKUQOHRPT"))  {
    my $label="SKU Inventory Activity (BETA)";
    my $next_tool="SKUINVRPT_DC";
    if ($tool eq "SKUQOHRPT") {
        $label="SKU Inventory";
        $next_tool="SKUQOHRPT_DC";    
    }
    print $q->h3("$label Report");
    # Ask which day to do the report for
      print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"$next_tool"});
      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      my @ltm = localtime(time());
      my $month=$ltm[4]+1;
      my $year=$ltm[5]+1900;
      my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
      my $hm = 1;
      my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
      $startdate = $enddate; # for now.

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

      print $q->Tr(
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;Enter <u>S</u>KU:\&nbsp"),
                   $q->td({-class=>'tabledatanb', -align=>'right'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"SKU", -size=>"12"}))
                  );
      #print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
      #             $q->input({-accesskey=>'D', -type=>"checkbox", -name=>"cb_like"}) .
      #             $q->font({-size=>-1}, "Fin<u>d</u> SKUs starting with ...")
      #            ));                  
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      #print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");
    
    
    StandardFooter(); 
} elsif ($tool eq "SKUINVRPT_DC") {    
    print $q->h3("SKU Inventory Activity Report - Beta");  

    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);
    my $sku   = trim(($q->param("SKU"))[0]);
    
    my $datewhere_ms = "";
    my $datewhere_tm = "";
    my $datewhere_db = "";
    my $daterange = "";

    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
           print "Startdate or Enddate format not correct";
           StandardFooter();
           exit;
        }
        $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
        $daterange = " date range $startdate - $enddate";
    } else {
       print "Start date or End date is blank";
       StandardFooter();
       exit;
    }

    print $q->p("Compiling data for $daterange for SKU $sku.");
    my $dbh = openODBCDriver($dsn);
    my $where;
    unless ($sku) {
        print $q->p("Please specify a SKU");
        StandardFooter(); 
        exit;
    }    
    $query = dequote(<< "    ENDQUERY");         
         select      
            adjustdate,
            cashiernum,
    
            functionid as function,
            reasonid,
            qty,
            qtyonhand,
            seqnum
            
         from
            inventory_adjustment
         where
            adjustdate between '$startdate' and '$enddate 23:59:59.999'              
            and plunum like '$sku'
        order by
            adjustdate
    ENDQUERY

    my $tbl_ref = execQuery_arrayref($dbh, $query);  

    if (recordCount($tbl_ref)) { 
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $function  = trim($$thisrow[2]);  
            if ($function == 3) {
                $$thisrow[2]= "Received"
            }
            if ($function == 5) {
                $$thisrow[2]= "ManualAdj"
            }  
            if ($function == 6) {
                $$thisrow[2]= "TransferIn"
            }  
            if ($function == 7) {
                $$thisrow[2]= "TransferOut"
            }  
            if ($function == 8) {
                $$thisrow[2]= "PhysicalInv"
            }              
            
        }
        print $q->h3("Inventory Adjustments");
        TTV::cgiutilities::printResult_arrayref2($tbl_ref); 
    } else {
        print $q->p("There is no data to report in inventory_adjustment");
    }
     $query = dequote(<< "    ENDQUERY");         
         select      
            itemdatetime,
            salesprsnid,
            txnnum,
            qty
 
            
         from
            txn_merchandise_sale 
         where
            itemdatetime between '$startdate' and '$enddate 23:59:59.999'              
            and skunum like '$sku'
        order by
            itemdatetime
    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);  
     if (recordCount($tbl_ref)) { 
        print $q->h3("Sales records");
        TTV::cgiutilities::printResult_arrayref2($tbl_ref); 
    } else {
        print $q->p("There is no data to report in txn_merchandise_sale");
    }
    StandardFooter();     
} elsif ($tool eq "SKUQOHRPT_DC") {    
    print $q->h3("SKU Inventory Report  ");  

    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);
    my $sku   = trim(($q->param("SKU"))[0]);
    
    my $datewhere_ms = "";
    my $datewhere_tm = "";
    my $datewhere_db = "";
    my $daterange = "";

    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
           print "Startdate or Enddate format not correct";
           StandardFooter();
           exit;
        }
        $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
        $daterange = " date range $startdate - $enddate";
    } else {
       print "Start date or End date is blank";
       StandardFooter();
       exit;
    }

    print $q->p("Compiling data for $daterange for SKU $sku.");
    my $dbh = openODBCDriver($dsn);
    my $where;
    unless ($sku) {
        print $q->p("Please specify a SKU");
        StandardFooter(); 
        exit;
    }    
    $query = dequote(<< "    ENDQUERY");         
        select      
            *            
        from
            elderInvCurrent
         where
            inventoryDate between '$startdate' and '$enddate 23:59:59.999'              
            and plunum like '$sku'
        order by
            inventoryDate
    ENDQUERY

    my $tbl_ref = execQuery_arrayref($dbh, $query);  

    if (recordCount($tbl_ref)) { 
 
        print $q->h3("Inventory for $sku");
        TTV::cgiutilities::printResult_arrayref2($tbl_ref); 
    } else {
        print $q->p("There is no data to report in elderInvCurrent");
    }
    
    StandardFooter();  
} elsif ($report eq "UFTOOLLABEL") {
    print $q->h2("User Flag Description Tool - Beta"); 
     
    # Check the user flag descriptions
    foreach my $flag (1,2,3,4,6) {     
        my %hash;
        my $label;
        my %found_hash;
        my @header=(
            "CodeName",
            "Expected Description",
            "Found Description"
        );
        my @array_to_print=\@header;                
        # Reset the warns for the next user flag
        my $warns=0;    
        my $error_count=0;
        my $unknown_error_count=0;
        if ($flag == 1) {
            %hash=%seasonality_hash;
            $label="Seasonality";
        } elsif ($flag == 2) {
            %hash=%vmcode_hash;
            $label="VMCode";
        } elsif ($flag == 3) {
            %hash=%akron_status_hash;
            $label="Akron Status";
        } elsif ($flag == 4) {
            %hash=%store_status_hash;
            $label="Store Status";
        } elsif ($flag == 6) {
            %hash=%promo_hash; 
            $label="Promo";
        }   
        my @header2=("Unknown $label Codes");
        my @array_to_print2=\@header2;           
        # Check all of the code descriptions      
        $query = dequote(<< "        ENDQUERY"); 
        select 
            codename,description 
        from 
            user_flag_code
        where
            userflagnum = '$flag'
        
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array) {
            next unless(defined($tmp[0]));
            $found_hash{$tmp[0]}=$tmp[1];            
        }  
        # Compare the found with the expected.  Remember, "hash" comes from lookupdata.pm.
        foreach my $codename (sort keys(%hash)) {            
            unless (defined($found_hash{$codename})) {
                # It is okay if certain seasonality or vmcodes listed in lookupdata.pm are not found in the database
                my $found_desc="";
                if ($flag > 2) {   
                    my @array=(
                        "$codename",
                        "$hash{$codename}",
                        "$found_desc"
                    );
                    $error_count++;
                    push(@array_to_print,\@array);                                                       
                    print "ERROR: Failed to find $codename for user flag $flag<br />";                  
                }
            } else {
                # The code name is defined in each hash so see if the values are the same.
                unless (lc($found_hash{$codename}) eq lc($hash{$codename})) {
                    my @array=(
                        "$codename",
                        "$hash{$codename}",
                        "$found_hash{$codename}"
                    );
                    $error_count++;
                    push(@array_to_print,\@array);                                                    
                    print  "ERROR: Description for user flag $flag codename $codename is ($found_hash{$codename}) - expected: ($hash{$codename})<br />";
                    
                }
            }
        }
        foreach my $codename (sort keys(%found_hash)){
            # For each codename found, see if it is in the lookupdata.pm module
            unless (defined($hash{$codename})) {                
                my $expected_desc="";                
                my @array=(
                    "$codename"
                );
                $unknown_error_count++;
                push(@array_to_print2,\@array);   
                print "ERROR: Codename $codename for user flag $flag is unknown in lookupdata.pm.<br />";                                        
            }        
        }
  
        ###
        if (($error_count) || ($unknown_error_count)) {
            print $q->h3("$label Code Errors");
        } else {
           print  "No errors found with $label Codes<br />";
        }    
        if ($#array_to_print > 0) {        
            my @links;
            if ($error_count) {
                @links = ( "$scriptname?report=UFTOOLLABELFIX&flag=$flag&".'codename=$_' );                     
            }
            printResult_arrayref2(\@array_to_print, \@links ); 
            if ($error_count) {
                print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
                print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOLLABELFIX'}), "\n";  
                print $q->input({-type=>"hidden", -name=>"flag", -value=>"$flag"}), "\n";        
                print $q->input({-type=>"hidden", -name=>"fixall", -value=>"fixall"}), "\n";               
                print $q->submit({  -value=>"   Fix Them All   "});   
                print $q->end_form();
            }
        }    
        if ($#array_to_print2 > 0) {        
            my @links = ( "$scriptname?report=UFTOOLLABELRPT&flag=$flag&".'codename=$_' );                     
            printResult_arrayref2(\@array_to_print2, \@links );           
            print $q->p("To see SKUs with an unknown codename, click on the codename or click 'Fix Them All'<br />
             below to delete any unknown codename with no SKUs associated with them.");
            print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOLLABELDEL'}), "\n";  
            print $q->input({-type=>"hidden", -name=>"flag", -value=>"$flag"}), "\n";        
            print $q->input({-type=>"hidden", -name=>"fixall", -value=>"fixall"}), "\n";               
            print $q->submit({  -value=>"   Fix Them All   "});   
            print $q->end_form();            
        }       
        print "--------------------------------------------------------------------------------------------------------<br />";                     
    }    
 
    StandardFooter();     
} elsif ($report eq "UFTOOLLABELFIX") {
    print $q->h2("User Flag Description Tool");   
    my $fixall= ($q->param("fixall"))[0];  
    
    my $flag= ($q->param("flag"))[0]; 
    my $codename= ($q->param("codename"))[0]; 
    my %hash;
    if ($flag == 1) {
        %hash=%seasonality_hash;
    } elsif ($flag == 2) {
        %hash=%vmcode_hash;
    } elsif ($flag == 3) {
        %hash=%akron_status_hash;
    } elsif ($flag == 4) {
        %hash=%store_status_hash;
    } elsif ($flag == 6) {
        %hash=%promo_hash; 
    }    
    if ($fixall) {        
        # Fix all descriptions for the specified flag        
        foreach my $codename (sort keys(%hash)) {        
            my $description=$hash{$codename};
            unless ($description) {
                print $q->h3("Error: Failed to determine description for user flag $flag codename $codename");
                StandardFooter(); 
                exit;
            }            
            fix_userflag_desc($flag,$codename,$description);       
        }
    } else {        
        # We just want to fix the specified codename        
        my $description=$hash{$codename};
        unless ($description) {
            print $q->h3("Error: Failed to determine description for user flag $flag codename $codename");
            StandardFooter(); 
            exit;
        }
        fix_userflag_desc($flag,$codename,$description);
        
    }
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOLLABEL'}), "\n";          
    print $q->submit({-accesskey=>'G', -value=>"   Re-Run User Flag Label Check   "});   
    print $q->end_form();  
    StandardFooter(); 
} elsif ($report eq "UFTOOLLABELRPT") {
    print $q->h2("User Flag Label - Unknown Codename Report");       
    my $flag= ($q->param("flag"))[0]; 
    my $codename= ($q->param("codename"))[0]; 
    my $codeid;
    my $userflagnum="userflagnum".$flag;
    # First, get the code id for this codename and user flag number combination
    $query = dequote(<< "    ENDQUERY");
    select 
        codeid
    from
        user_flag_code
    where
        userflagnum = '$flag'
    and
        codename like '$codename'
    ENDQUERY
    my $sth = $dbh->prepare($query);        
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {              
        $codeid=$tmp[0];
    }
    unless ($codeid) {
        print $q->p("Error: Failed to get the codeid for user flag $flag and codename $codename");
        StandardFooter();
        exit;
    }
    # Now, display all of the SKUs that have this codeid for this userflagnum
    $query = dequote(<< "    ENDQUERY");
    select 
        plunum,
        deptnum,
        pludesc,
        '$userflagnum'
    from
        plu
    where
        '$userflagnum' = '$codeid'        
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);  
    if (recordCount($tbl_ref)) {  
        printResult_arrayref2($tbl_ref);     
    } else {
        print $q->p("No SKUs found with User Flag $flag set to $codename .");
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOLLABELDEL'}), "\n";          
        print $q->input({-type=>"hidden", -name=>"flag", -value=>"$flag"}), "\n"; 
        print $q->input({-type=>"hidden", -name=>"codename", -value=>"$codename"}), "\n"; 
        print $q->submit({-accesskey=>'G', -value=>"   Delete This Unknown Codename   "});   
        print $q->end_form();          
    }
 
    StandardFooter();  
} elsif ($report eq "UFTOOLLABELDEL") {
    print $q->h2("User Flag Label - Unknown Codename Delete");       
    my $flag= ($q->param("flag"))[0]; 
    my $fixall = ($q->param("fixall"))[0]; 
    my $codename= ($q->param("codename"))[0]; 
    if ($fixall) {
        my %hash;
        my %found_hash;
        my $label;
        if ($flag == 1) {
            %hash=%seasonality_hash;
            $label="Seasonality";
        } elsif ($flag == 2) {
            %hash=%vmcode_hash;
            $label="VMCode";
        } elsif ($flag == 3) {
            %hash=%akron_status_hash;
            $label="Akron Status";
        } elsif ($flag == 4) {
            %hash=%store_status_hash;
            $label="Store Status";
        } elsif ($flag == 6) {
            %hash=%promo_hash; 
            $label="Promo";
        }           
        # Here we want to fix all codenames that are unknown for the specified flag.  We look for any codename that is not
        # found in the lookupdata.pm module, see if any SKUs are associated with it, and if there are none, just delete the entry
        $query = dequote(<< "        ENDQUERY"); 
        select 
            codename,description 
        from 
            user_flag_code
        where
            userflagnum = '$flag'
        
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array) {
            next unless(defined($tmp[0]));
            $found_hash{$tmp[0]}=$tmp[1];            
        }  
        my @unknown_codes;
        foreach my $codename (sort keys(%found_hash)){
            
            # For each codename found, see if it is in the lookupdata.pm module
            unless (defined($hash{$codename})) { 
                push(@unknown_codes,$codename);                                                  
            }        
        }  
        foreach $codename (@unknown_codes) { 
            my $codeid;
            # First, get the code id for this codename and user flag number combination
            $query = dequote(<< "            ENDQUERY");
            select 
                codeid
            from
                user_flag_code
            where
                userflagnum = '$flag'
            and
                codename like '$codename'
            ENDQUERY
            my $sth = $dbh->prepare($query);        
            $sth->execute();                
            while (my @tmp=$sth->fetchrow_array()) {              
                $codeid=$tmp[0];
            }  
            # Now, find all of the SKUs that have this codeid for this userflagnum
            my $userflagnum="userflagnum".$flag;
            my $found_sku=0;
            $query = dequote(<< "            ENDQUERY");
            select 
                plunum,
                deptnum,
                pludesc,
                '$userflagnum'
            from
                plu
            where
                '$userflagnum' = '$codeid'        
            ENDQUERY
            $sth = $dbh->prepare($query);        
            $sth->execute();                
            while (my @tmp=$sth->fetchrow_array()) {              
                $found_sku=$tmp[0];
                print $q->p("Found $found_sku associated with $codename for flag $flag");
            } 
            unless ($found_sku) {
                # No skus associated with this codename so just delete the code name
                $query = dequote(<< "                ENDQUERY");
                delete from
                    user_flag_code         
                where
                    userflagnum = '$flag'
                and
                    codename like '$codename'        
                ENDQUERY
                if ($dbh->do($query)) {
                    print "Deleted unknown codename for user flag $flag codename $codename.<br />";
                    ElderLog($logfile,"Deleted unknown codename for user flag $flag codename $codename.");  
                } else {
                    ElderLog($logfile,"Error trying to delete unknown codename for user flag $flag codename $codename");
                    print "Database Update Error"."<br />\n";
                    print "Driver=ODBC"."<br />\n";
                    print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                    print "query=".$query."<br />\n";
                    print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                    print "errstr=".$dbh->errstr ."<br />\n";
                    StandardFooter(); 
                    exit;
                }                   
            }                            
        }
    } else {
        if (($flag) && ($codename)) {
            $query = dequote(<< "            ENDQUERY");
            delete from
                user_flag_code         
            where
                userflagnum = '$flag'
            and
                codename like '$codename'        
            ENDQUERY
            if ($dbh->do($query)) {
                print $q->p("Deleted unknown codename for user flag $flag codename $codename.");
                ElderLog($logfile,"Deleted unknown codename for user flag $flag codename $codename.");  
            } else {
                ElderLog($logfile,"Error trying to delete unknown codename for user flag $flag codename $codename");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }      
        } else {
            print $q->p("Error: Failed to get codename and/or flag");    
        }
    }
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOLLABEL'}), "\n";          
    print $q->submit({-accesskey=>'G', -value=>"   Re-Run User Flag Label Check   "});   
    print $q->end_form();     
    StandardFooter();      
} elsif ($report eq "UFTOOL") {
    print $q->h2("User Flag Tool");  
    # User flag 1
    # Check that the SKUs have the expected user flags.
    my %found_flags;
    my %expected_flags;
	my %villages_skus_found;
    my @tmp;
    my $flag;
    my $elderVillagesSKU="elderVillagesSKU";
    my $found=0;
    $query = dequote(<< "    ENDQUERY");
    select name from dbo.sysobjects where id = object_id('$elderVillagesSKU') and type = 'U'   
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) { 

        if (defined($tmp[0])) {        
            if ("$tmp[0]" eq "$elderVillagesSKU") {            
                $found=1;
            }
        }
    }
    unless($found) {
        print $q->p("Error: Could not find table $elderVillagesSKU");
        print $q->p("Cannot continue");
        StandardFooter();
        return;
    }
	# Now get a list of all of the Villages SKUs in the store
	$query = dequote(<< "    ENDQUERY"); 		
		select plunum from plu where vendorid = '01'
    ENDQUERY
 
	$sth = $dbh->prepare($query);
	$sth->execute();
	while (my @tmp=$sth->fetchrow_array()) {
		$villages_skus_found{$tmp[0]}=1;	
	}	
    foreach $flag (1,2,3) {    
        my $sku_list;
        my $uf_errors=0;
        # First, we need to verify that we can find the userflag field 
=pod        
        $query = dequote(<< "        ENDQUERY"); 
            SELECT cname FROM SYSCOLUMNS 
                where tname = '$elderVillagesSKU'
                and cname = 'userflag$flag'
        ENDQUERY

        
        $found=0;
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {        
            if (defined($tmp[0])) {        
                if ("$tmp[0]" eq "userflag$flag") {            
                    $found=1;
                }
            }
        }
        unless($found) {
            print $q->p("Error: Could not find field userflag$flag in table $elderVillagesSKU");
            next;
        }
=cut        
        # Support for SAP POS 2.3
        $query = dequote(<< "        ENDQUERY");
            select * from elderVillagesSku
        ENDQUERY
        
        my $tbl_ref2 = execQuery_arrayref($dbh, $query);                            
        my $names = @$tbl_ref2[0];                
        $found=0;
        foreach my $f (@$names) {
            if ($f eq "userflag$flag") {
                $found=1;                
            }
        }    
 
        unless($found) {
            print $q->p("Error: Could not find field userflag$flag in table $elderVillagesSKU");
            next;
        }        


        # Collect the information of how things are set currently
        $query = dequote(<< "        ENDQUERY"); 
        select 
            p.plunum,ufc.codename 
        from 
            plu p
            join user_flag_code ufc on p.userflagnum$flag = ufc.codeid
        where 
            p.vendorid = '01'
            and ufc.userflagnum = $flag
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            $found_flags{$tmp[0]}=$tmp[1];			
        }
        # Now get the expected list
        my %expected_hash;
        $query = dequote(<< "        ENDQUERY"); 
        select 
            plunum,pludesc,userflag$flag
        from 
            elderVillagesSku            
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            next unless(defined($tmp[0]));            
            my $plu=$tmp[0];    			
            my $desc=$tmp[1];
            my $setting=$tmp[2];
            if ($setting eq "") {
                $setting="Not Set";            
            }          
            my @array=(
                "$desc",
                "$setting"
            );
            $expected_hash{$plu}=\@array;
            
        }    
        # Now compare the found with the expected
        my @header=(
            
            "SKU",
            "UserFlag",
            "Desc",
            "Found",
            "Expected"      
        );
        my @error_array=(\@header);
        
        foreach my $sku (keys(%found_flags)) {
            my $desc=$expected_hash{$sku}[0];
            next unless ($expected_hash{$sku}[1]); # If the SKU is found in the store but not on our list from Akron, skip it.
            my $expected_flag=$expected_hash{$sku}[1];
            if ($flag == 2) {
             
                # This is the VMCode.  Check that the expected VMCode is a current VMCode                    
                unless ($vmcode_hash{$expected_flag}) {
                    # If this is no longer a current VMCode, just skip it
 
                    next;
                }
            }             
            unless ($found_flags{$sku} eq $expected_flag) {                           
                $uf_errors++;
                my @array=(
                    "$sku",
                    "$flag",
                    "$desc",
                    "$found_flags{$sku}",
                    "$expected_flag"
                   
                );
                push(@error_array, \@array);
                $sku_list.="$sku,$flag|";                    
            }
        }
        if ($uf_errors) {          
            my @links = ( "$scriptname?report=UFTOOL2&flag=$flag&".'skuid=$_' ); 
            TTV::cgiutilities::printResult_arrayref2(\@error_array, \@links, ''); 
            print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOL2'}), "\n";  
            print $q->input({-type=>"hidden", -name=>"fixall", -value=>"$sku_list"}), "\n";   
            print $q->submit({-accesskey=>'G', -value=>"   Fix Them All   "});   
            print $q->end_form();    
            print $q->h2("WARNING: $uf_errors errors found with userflag $flag");
        } else {
			# Now check the other way around		
			@error_array=(\@header);
			foreach my $sku (keys(%expected_hash)) {
				next unless $villages_skus_found{$sku};	# If the SKU is not in the store, skip it.

				my $desc=$expected_hash{$sku}[0];
				next unless ($expected_hash{$sku}[1]); # If the SKU is found in the store but not on our list from Akron, skip it.
				my $expected_flag=$expected_hash{$sku}[1];
                if ($flag == 2) {                   
                    # This is the VMCode.  Check that the expected VMCode is a current VMCode                    
                    unless ($vmcode_hash{$expected_flag}) {
                        # If this is no longer a current VMCode, just skip it                
                        next;
                    }
                }                
				if ($found_flags{$sku} eq "") {
					$found_flags{$sku}="Not Set";
				}
					   
				unless ($found_flags{$sku} eq $expected_flag) {                          
					$uf_errors++;
					my @array=(
						"$sku",
						"$flag",
						"$desc",
						"$found_flags{$sku}",
						"$expected_flag"
					   
					);
					push(@error_array, \@array);
					$sku_list.="$sku,$flag|";                    
				}
			}
			if ($uf_errors) {          
				my @links = ( "$scriptname?report=UFTOOL2&flag=$flag&".'skuid=$_' ); 
				TTV::cgiutilities::printResult_arrayref2(\@error_array, \@links, ''); 
				print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
				print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOL2'}), "\n";  
				print $q->input({-type=>"hidden", -name=>"fixall", -value=>"$sku_list"}), "\n";   
                print $q->input({-type=>"hidden", -name=>"flag", -value=>"$flag"}), "\n";  
				print $q->submit({-accesskey=>'G', -value=>"   Fix Them All   "});   
				print $q->end_form();
        
				print $q->h2("WARNING: $uf_errors errors found with userflag $flag");
			} else {
				print $q->p("No errors found with userflag $flag");
			}			            
        }
	
    }    

    StandardFooter();
    
} elsif ($report eq "UFTOOL2") {
    print $q->h3("User Flag Tool - Beta");  
    my @tmp;
    my $fixall= ($q->param("fixall"))[0];  
    my $sku= ($q->param("skuid"))[0]; 
    my $flag= ($q->param("flag"))[0];      
    $sku=~s/\'//g;    
    if ($fixall) {        
        @tmp=split(/\|/,$fixall);
        foreach my $t (@tmp) {            
            my @array=split(/,/,$t);
            next unless ($array[0]);
            my $sku=$array[0];
            my $flag;
            if ($array[1]) {
                $flag=$array[1];
            } else {
                print $q->p("Error: Did not get the flag for $sku");
                next;
            }     
            update_userflag($sku,$flag);                   
        }   

    } else {        
        update_userflag($sku,$flag);
    }
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>'UFTOOL'}), "\n";          
    print $q->submit({-accesskey=>'G', -value=>"   Re-Run User Flag Check   "});   
    print $q->end_form();        
    print "<br />";
    StandardFooter();
} elsif (($report eq "BCTOOL") || ($report eq "NVBCTOOL")) {
    print $q->h3("Bar Code Tool"); 
    my $fix= ($q->param("fix"))[0]; 
    my $fixall= ($q->param("fixall"))[0]; 
    my $fix2= ($q->param("fix2"))[0]; 
    my $fixall2= ($q->param("fixall2"))[0];     
    my $fixsku= ($q->param("skuid"))[0]; 
    my %barcode_hash;
    my %storeBarcode_hash;
    my $asc="${xpsDir}/parm/xreftxn.asc";
    my $trigger="${xpsDir}/parm/newparm.trg";
    my $table="elderVillagesSKU";
    my $where = "where p.vendorid = '01'";
    if ($report eq "NVBCTOOL") {
        $table = "elderNonVillagesSKU";
        $where = '';
    }
    # Get the bar codes Akron has listed for villages SKUs
    $query = dequote(<< "    ENDQUERY");
    select 
        evs.plunum, evs.barcode 
    from
        $table evs
 
    ENDQUERY
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        if (defined($tmp[1])) {        
            $barcode_hash{$tmp[0]}=$tmp[1];
        }
    }    
    # Get the bar codes that the store has for these skus.  Remember, their might be more than one bar code per SKU.
    $query = dequote(<< "    ENDQUERY");
    select 
        p.plunum, pcr.xrefnum 
    from
        plu p
        left outer join plu_cross_ref pcr on p.plunum=pcr.plunum
    $where 
 
    ENDQUERY
    
    $sth = $dbh->prepare($query);
    
    eval {$sth->execute()};
    unless ($@) {
        while (my @tmp=$sth->fetchrow_array()) {        
            if (defined($tmp[1])) {      
                if ($storeBarcode_hash{$tmp[0]}) {
                    $storeBarcode_hash{$tmp[0]}.=",$tmp[1]"; # If we have a bar code already, append this one
                } else {
                    $storeBarcode_hash{$tmp[0]}=$tmp[1];
                }
                
            } else {
                $storeBarcode_hash{$tmp[0]}="none";
            }
        }   
    } else {
        print "Error with query ($query)<br /> ";
        StandardFooter(); 
        exit;
    }
    
 
    
    my @headder=(
    "SKU",
    "Expected BarCode",
    "Found BarCode"
    );
    my @barcode_errors=(\@headder);
    # Now compare the bar codes

    foreach my $SKU (keys(%storeBarcode_hash)) {
        # Extract the bar codes that the store has
        my @barcodes=split(/,/,$storeBarcode_hash{$SKU});
        # Akron only has one bar code for this SKU but see if that one is on this list
        next unless (defined($barcode_hash{$SKU})); # Skip if we do not have a bar code defined for this sku.
        next unless ($barcode_hash{$SKU}); # Skip if we do not have a known bar code for this sku.
        my $found=grep/$barcode_hash{$SKU}/,@barcodes;
        my $found_barcode=$storeBarcode_hash{$SKU};
        if ($found_barcode eq "none") {
            $found_barcode="";
        }
        unless ($found) {
            my @array=(
                "$SKU",
                "$barcode_hash{$SKU}",
                "$found_barcode"
            );        
            push(@barcode_errors,\@array);
        }
    }
    
    if ($#barcode_errors) {
        if ($fix) { # The general fix flag
            if (open(ASC,">$asc")) {
                # Create an ASC file to                 
                #foreach my $ref (@barcode_errors) {
                for (my $x=1; $x<=$#barcode_errors; $x++) {
                    my $ref=$barcode_errors[$x];
                    my $sku=$$ref[0];
                    my $barcode=$$ref[1];                    
                    if ($fixall) {  # The fix all flag
                        # Put in all of the SKUs to fix    
                        ElderLog($logfile,"Setting the barcode for SKU $sku to $barcode");  
                        print ASC "2,$sku,$barcode,Y\n";
                    } elsif ($fixsku) {    # The flag to fix a specific sku
                        # Only fix the sku specified                        
                        if ($sku eq $fixsku) {                            
                            ElderLog($logfile,"Setting the barcode for SKU $sku to $barcode");  
                            print ASC "1,$sku,$barcode,Y\n";
                        }
                    }
                    
                }
                close ASC;
                # Create the trigger file
                if (open(TRG,">$trigger")) {
                    print TRG "trigger";
                    close TRG;
                    my $trg_count=0;
                    while (-e $trigger) {
                        print $q->p("Waiting for $trigger to be processed...");
                        sleep 10;
                        $trg_count++;
                        if ($trg_count > 4) {
                            print $q->p("Error: $trigger has not been processed yet");
                            StandardFooter();
                            exit;
                        }
                    }
                    print $q->p("The asc file has been processed");
                    print $q->p("Re-run this test to verify it has been fixed");
                    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
                    print $q->input({-type=>"hidden", -name=>"report", -value=>$report}), "\n";            
                    print $q->submit({-accesskey=>'G', -value=>"   Re-Run BarCode Check   "}); 
                    print $q->p();
                } else {
                    print $q->p("Error: Could not create $trigger");
                    StandardFooter();
                    exit;
                }
            } else {
                print $q->p("ERROR: Could not open $asc");
                StandardFooter();
                exit;
            }
            
        } else {
            
            print $q->p("Select a specific SKU to fix or select the 'Fix Them All ' button at the bottom. ");    
            print $q->p("(Note: Either way, the fix creates an xreftxn.asc file which will take up to 30 seconds to process.)");
        
            my @links = ( "$scriptname?report=$report&fix=fix&".'skuid=$_' ); 
            TTV::cgiutilities::printResult_arrayref2(\@barcode_errors, \@links, ''); 
            print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>$report}), "\n";  
            print $q->input({-type=>"hidden", -name=>"fix", -value=>"fix"}), "\n";   
            print $q->input({-type=>"hidden", -name=>"fixall", -value=>"fixall"}), "\n";               
            print $q->submit({-accesskey=>'G', -value=>"   Fix Them All   "});   
            print $q->end_form();
        }
        
    } else {
        print $q->p("No bar code errors found with Villages SKUs");
    }
    StandardFooter();      
} elsif ($report eq "BCTOOL2") {
    print $q->h3("Bar Code Tool - Beta - captureplu and invcontrolled fields"); 
    my $fix= ($q->param("fix"))[0]; 
    my $fixall= ($q->param("fixall"))[0];     
    my $fixsku= ($q->param("skuid"))[0]; 
    
    # Check whether we should fix first and then display
    if ($fix) {   
        # Fix a specified SKU
        # Get the xrefnum
        my $xrefnum;
        $query = dequote(<< "        ENDQUERY");           
            select xrefnum from plu_cross_ref where plunum like '$fixsku'
        ENDQUERY
        $sth = $dbh->prepare($query);        
        $sth->execute();    
        while (my @tmp=$sth->fetchrow_array()) {           
            $xrefnum=$tmp[0];
        }
        unless ($xrefnum) {
            print $q->p("ERROR: Failed to find the xrefnum for $fixsku");
            ElderLog($logfile,"ERROR: Failed to find the xrefnum for $fixsku");  
            StandardFooter(); 
            exit;
        }
        # Clear these from inventory_history
        $query = dequote(<< "        ENDQUERY");
            delete from inventory_history where plunum like '$xrefnum'            
        ENDQUERY
        
        if ($dbh->do($query)) {
            print $q->p("Corrected inventory_history settings for $xrefnum");
            ElderLog($logfile,"Corrected inventory_history settings for $xrefnum");  
        } else {
            ElderLog($logfile,"Error trying to clear inventory_history for $xrefnum");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }     
        # Clear from inventory_current
        $query = dequote(<< "        ENDQUERY");
            delete from inventory_current where plunum like '$xrefnum'  
        ENDQUERY
               
        if ($dbh->do($query)) {
            print $q->p("Corrected inventory_current settings for $xrefnum");
            ElderLog($logfile,"Corrected inventory_current settings for $xrefnum");  
        } else {
            ElderLog($logfile,"Error trying to clear inventory_current for $xrefnum");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }           
        # Correct the settings 
        $query = dequote(<< "        ENDQUERY");
            update plu_cross_ref set captureplu = 'Y', invcontrolled = 'N'
            where plunum like '$fixsku'
        ENDQUERY
        if ($dbh->do($query)) {
            print $q->p("Fixed captureplu and invcontrolled settings for xrefnum $fixsku");
            ElderLog($logfile,"Fixed captureplu and invcontrolled settings for $fixsku");  
        } else {
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }    
        # Clear the flag
        $fix=0;
    } 
    if ($fixall) {        
        # Fix all of the SKUs
        # Get a list of xrefnums
        my @xref_list;

        $query = dequote(<< "        ENDQUERY");           
            select xrefnum from plu_cross_ref where invcontrolled = 'Y'
        ENDQUERY
        $sth = $dbh->prepare($query);        
        $sth->execute();    
        while (my @tmp=$sth->fetchrow_array()) {           
            push(@xref_list,$tmp[0]);
        }
        # Clear these from inventory_history
        foreach my $xrefnum (@xref_list) {            
            $query = dequote(<< "            ENDQUERY");
                delete from inventory_history where plunum like '$xrefnum'                
            ENDQUERY
            if ($dbh->do($query)) {
            print $q->p("Deleted $xrefnum from inventory_history");
            ElderLog($logfile,"Deleted $xrefnum from inventory_history"); 
            } else {
                ElderLog($logfile,"Error trying to clear inventory_history for $xrefnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }       
            # Clear these from inventory_current
            $query = dequote(<< "            ENDQUERY");
                delete from inventory_current where plunum like '$xrefnum'                 
            ENDQUERY
            if ($dbh->do($query)) {
            print $q->p("Deleted $xrefnum from inventory_current");
            ElderLog($logfile,"Deleted $xrefnum from inventory_current"); 
            } else {
                ElderLog($logfile,"Error trying to clear inventory_current for $xrefnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }       
        }
        # Correct the settings 
        $query = dequote(<< "        ENDQUERY");
            update plu_cross_ref set captureplu = 'Y', invcontrolled = 'N'
        ENDQUERY
        if ($dbh->do($query)) {
            print $q->p("Fixed captureplu and invcontrolled settings for all SKUs");
            ElderLog($logfile,"Fixed captureplu and invcontrolled settings all SKUs");  
        } else {
            ElderLog($logfile,"Error trying to correct captureplu and invcontrolled settings for all SKUs");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }       
        # Clear the flag
        $fixall=0;
    }    
     
    $query = dequote(<< "    ENDQUERY");
        select 
            px.plunum,p.pludesc,v.vendorname,ic.qtycurrent,px.xrefnum,px.captureplu,px.invcontrolled 
        from 
            plu_cross_ref px
            left outer join plu p on px.plunum = p.plunum
            left outer join vendor v on p.vendorid = v.vendorid
            left outer join inventory_current ic on px.plunum = ic.plunum
        where 
            px.captureplu not like 'Y'
            or
            px.invcontrolled not like 'N'
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);  

    my $note=0;
    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];   
        if ($$thisrow[-1] eq "Y") {
            $$thisrow[-1] = "Y*";
            $note=1;
        }        
    }
    if (recordCount($tbl_ref)) { 

        my @links = ( "$scriptname?report=BCTOOL2&fix=fix&".'skuid=$_' ); 
        print $q->p("These records have the captureplu set to 'N' <i><b>or</b></i> the invcontrolled field set to 'Y'");
        print $q->p("Select a specific SKU to fix or select the 'Fix Them All ' button at the bottom. ");   
        
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '', ''); 
        if ($note) {
            print "*Note: Turning off invcontrol will delete all inventory history and current which is stored under the xrefnum<br />";
            print "(This should not be a problem, however.  Inventory records kept under the SKU will NOT be cleared.)<br />";
        }
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'BCTOOL2'}), "\n";          
        print $q->input({-type=>"hidden", -name=>"fixall", -value=>"fixall"}), "\n";               
        print $q->submit({-accesskey=>'G', -value=>"   Fix Them All   "});   
        print $q->end_form();
        
    } else { 
        print $q->b("no records found where captureplu or invcontrolled fields are incorrectly set.<br />\n"); 
        # Display an option to view inventory current and inventory history tables;
        print $q->p(
               $q->a({-href=>"$scriptname?report=INVXREF", -class=>"button1"}, "Check inventory"). "Run a check for Xref numbers as SKUs in inventory_current and inventory_history"
                );
    }    
    StandardFooter(); 
} elsif ($report eq "DEPTTOOL") {
    print $q->h3("Department Tool   ");  
    my $warns=0;
    # Check that the SKUs have the expected departments.
    my %found_dept;
    my %expected_dept;
    my @tmp;
 	my $storenum = getStoreNum($dbh); 
    my $elderVillagesSKU="elderVillagesSKU";
    my $found=0;
    $query = dequote(<< "    ENDQUERY");
    select name from dbo.sysobjects where id = object_id('$elderVillagesSKU') and type = 'U'   
    ENDQUERY
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        if (defined($tmp[0])) {        
            if ("$tmp[0]" eq "$elderVillagesSKU") {            
                $found=1;
            }
        }
    }
    unless($found) {
        print $q->p("Error: Could not find table $elderVillagesSKU");
        StandardFooter();       
        exit;
    }

    my $sku_list;
    my $uf_errors=0;
    # First, we need to verify that we can find the class field 
  
    # Support for SAP POS 2.3
    $query = dequote(<< "    ENDQUERY");
        select * from elderVillagesSKU
    ENDQUERY
        
    my $tbl_ref2 = execQuery_arrayref($dbh, $query);                                
    my $names = @$tbl_ref2[0];                
    
    $found=0;
    foreach my $f (@$names) {
        if ($f eq "class") {
            $found=1;                
        }
    }  
    
    unless($found) {
        print $q->p("Error: Could not find field class in table $elderVillagesSKU");
        StandardFooter();       
        exit;
    }      
    
    # Collect the information of how things are set currently
    $query = dequote(<< "    ENDQUERY"); 
    select 
        p.plunum,p.deptnum,
        ic.canorder,ic.minqty,ic.maxqty,ic.qtycurrent
    from 
        plu p
        join inventory_current ic on p.plunum = ic.plunum
    where 
        p.vendorid in  ('01')        
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (@tmp = $sth->fetchrow_array) {
        my $plu=$tmp[0];       
        my $dept=$tmp[1];
        my $iso=$tmp[2];
        my $min=$tmp[3];
        my $max=$tmp[4];
        my $qty=$tmp[5];
        
        my @array=(            
            "$dept",
            "$iso",
            "$min",
            "$max",
            "$qty"
        );
        $found_dept{$plu}=\@array;  
       
    }
    # Now get the expected list
    my %expected_hash;
    $query = dequote(<< "    ENDQUERY"); 
    select 
        plunum,pludesc,class
    from 
        elderVillagesSku            
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (@tmp = $sth->fetchrow_array) {
        next unless(defined($tmp[0]));
        
        my $plu=$tmp[0];
        my $desc=$tmp[1];
        my $setting=$tmp[2];
        if ($setting eq "") {
            $setting="Not Set";            
        }          
        my @array=(
            "$desc",
            "$setting"
        );
        $expected_hash{$plu}=\@array;        
    }    
    my @header = (
        "SKU",
        "Description",
        "Expected Dept",
        "Found Dept",
        "ISO",
        "Min",
        "Max",
        "Qty"
    );
    my @array_to_print=\@header;
    my $expected_dept;
    foreach my $sku (keys(%found_dept)) {
        my $desc=$expected_hash{$sku}[0];
        next unless ($expected_hash{$sku}[1]); # If the SKU is found in the store but not on our list from Akron, skip it.           
        $expected_dept=$expected_hash{$sku}[1];
        my $found_dept=$found_dept{$sku}[0];
        my $found_iso=$found_dept{$sku}[1];
        my $found_min=$found_dept{$sku}[2];
        my $found_max=$found_dept{$sku}[3];
        my $found_qty=$found_dept{$sku}[4];
        unless ($found_dept eq $expected_dept) {  
			# Because Ephrata has the OutOfCrate with a different dept number,
			# we need to ignore those since they will look incorrect.
			unless (($storenum == 7037) && ($found_dept == 8400)) {		
				my @array=(
					"$sku",
					"$desc",
					"$expected_dept",
					"$found_dept",
					"$found_iso",
					"$found_min",
					"$found_max",
					"$found_qty"
				);
				$warns++;                
				push(@array_to_print,\@array);  
			}
        }
    }   
    if ($warns) {
        print $q->p("Villages SKUs found with incorrect department numbers.");  
         my @format = ('', '', '','',
            {'align' => 'Center'}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Center', 'num' => '.0'}, 

            );
        my @links = ( "$scriptname?report=DEPTTOOL2&fix=1&sku=\$_" );                     
        printResult_arrayref2(\@array_to_print, \@links, \@format );  
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'DEPTTOOL2'}), "\n";          
        print $q->input({-type=>"hidden", -name=>"fixall", -value=>"fixall"}), "\n";               
        print $q->submit({  -value=>"   Fix Them All   "});   
        print $q->end_form();        
    } else {
        print $q->p("No Villages SKUs found with incorrect department numbers.");
    }
 
	## Check for unknown departments
	$query = dequote(<< "	ENDQUERY");
		select 
			p.plunum,
			p.pludesc,
			p.deptnum,
            ic.canorder,
            ic.minqty,
            ic.maxqty,
            ic.qtycurrent
		from 
			plu p		
            left outer join inventory_current ic on p.plunum = ic.plunum
		where 
			p.deptnum not in 
			(select deptnum from department)
	ENDQUERY
	
    $sth = $dbh->prepare($query);
    $sth->execute();
  
    @header = (
        "SKU",
        "Description", 
        "Found Dept",
        "ISO",
        "Min",
        "Max",
        "Qty"
    );
    @array_to_print=\@header;    
    $warns=0;
    while (@tmp = $sth->fetchrow_array) {
		my $sku=$tmp[0];
		my $desc=$tmp[1];
		my $dept=$tmp[2];
        my $iso=$tmp[3];
        my $min=$tmp[4];
        my $max=$tmp[5];
        my $qty=$tmp[6];
 
        my @array=(
            "$sku",
            "$desc",                
            "$dept",
            "$iso",
            "$min",
            "$max",
            "$qty"
        );                   
        push(@array_to_print,\@array);          
        $warns++;
	}   
    if ($warns) {
        print $q->p("Villages SKUs found with unknown department numbers.");  
         my @format = ('', '', '','',
            {'align' => 'Center'}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Center', 'num' => '.0'}, 

            );
                         
        printResult_arrayref2(\@array_to_print,'', \@format );         
    }    
    StandardFooter();     

} elsif ($report eq "NVDEPTTOOL") {
    print $q->h3("Non-Villages Department Tool ");  
    my $warns=0;
    # Check that the SKUs have the expected departments.
    my %found_dept;
    my %expected_dept;
    my @tmp;
 
    my $elderNonVillagesSku="elderNonVillagesSKU";
    my $found=0;
    $query = dequote(<< "    ENDQUERY");
    select name from dbo.sysobjects where id = object_id('$elderNonVillagesSku') and type = 'U'   
    ENDQUERY
    $sth = $dbh->prepare($query);
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {        
        if (defined($tmp[0])) {        
            if ("$tmp[0]" eq "$elderNonVillagesSku") {            
                $found=1;
            }
        }
    }
    unless($found) {
        print $q->p("Error: Could not find table $elderNonVillagesSku");
        StandardFooter();       
        exit;
    }

    my $sku_list;
    my $uf_errors=0;
    # First, we need to verify that we can find the class field 
  
    # Support for SAP POS 2.3
    $query = dequote(<< "    ENDQUERY");
        select * from elderNonVillagesSku
    ENDQUERY
        
    my $tbl_ref2 = execQuery_arrayref($dbh, $query);                                
    my $names = @$tbl_ref2[0];                
    
    $found=0;
    foreach my $f (@$names) {
        if ($f eq "class") {
            $found=1;                
        }
    }  
    
    unless($found) {
        print $q->p("Error: Could not find field class in table $elderNonVillagesSku");
        StandardFooter();       
        exit;
    }      


    # Collect the information of how things are set currently
    $query = dequote(<< "    ENDQUERY"); 
    select 
        p.plunum,p.deptnum,
        ic.canorder,ic.minqty,ic.maxqty,ic.qtycurrent
    from 
        plu p
        join inventory_current ic on p.plunum = ic.plunum
    where 
        p.vendorid not in  ('01')        
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (@tmp = $sth->fetchrow_array) {
        my $plu=$tmp[0];       
        my $dept=$tmp[1];
        my $iso=$tmp[2];
        my $min=$tmp[3];
        my $max=$tmp[4];
        my $qty=$tmp[5];
        
        my @array=(
            
            "$dept",
            "$iso",
            "$min",
            "$max",
            "$qty"
        );
        $found_dept{$plu}=\@array;  
       
    }
    # Now get the expected list
    my %expected_hash;
    $query = dequote(<< "    ENDQUERY"); 
    select 
        plunum,pludesc,class
    from 
        elderNonVillagesSku            
    ENDQUERY

    $sth = $dbh->prepare($query);
    $sth->execute();
    while (@tmp = $sth->fetchrow_array) {
        next unless(defined($tmp[0]));
        
        my $plu=$tmp[0];
        my $desc=$tmp[1];
        my $setting=$tmp[2];
        if ($setting eq "") {
            $setting="Not Set";            
        }          
        my @array=(
            "$desc",
            "$setting"
        );
        $expected_hash{$plu}=\@array;        
    }    
    my @header = (
        "SKU",
        "Description",
        "Expected Dept",
        "Found Dept",
        "ISO",
        "Min",
        "Max",
        "Qty"
    );
    my @array_to_print=\@header;
    my $expected_dept;
    foreach my $sku (keys(%found_dept)) {
        my $desc=$expected_hash{$sku}[0];
        next unless ($expected_hash{$sku}[1]); # If the SKU is found in the store but not on our list from Akron, skip it.           
        $expected_dept=$expected_hash{$sku}[1];
        my $found_dept=$found_dept{$sku}[0];
        my $found_iso=$found_dept{$sku}[1];
        my $found_min=$found_dept{$sku}[2];
        my $found_max=$found_dept{$sku}[3];
        my $found_qty=$found_dept{$sku}[4];
        unless ($found_dept eq $expected_dept) {  
			my @array=(
				"$sku",
				"$desc",
				"$expected_dept",
				"$found_dept",
				"$found_iso",
				"$found_min",
				"$found_max",
				"$found_qty"
			);
			$warns++;                
			push(@array_to_print,\@array);  			
        }
    }    
    if ($warns) {
        print $q->p("Villages SKUs found with incorrect department numbers.");  
        my @format = ('', '', '','',
            {'align' => 'Center'}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Center', 'num' => '.0'}, 
            {'align' => 'Center', 'num' => '.0'}, 

            );
        my @links = ( "$scriptname?report=NVDEPTTOOL2&fix=1&sku=\$_" );                     
        printResult_arrayref2(\@array_to_print, \@links, \@format );  
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'NVDEPTTOOL2'}), "\n";          
        print $q->input({-type=>"hidden", -name=>"fixall", -value=>"fixall"}), "\n";               
        print $q->submit({  -value=>"   Fix Them All   "});   
        print $q->end_form();        
    } else {
        print $q->p("No Villages SKUs found with incorrect department numbers.");
    }
    StandardFooter();     
} elsif ($report eq "DEPTTOOL2") {
    print $q->h3("Department Tool   "); 
    my $fix= ($q->param("fix"))[0]; 
    my $fixall= ($q->param("fixall"))[0];     
    my $fixsku= ($q->param("sku"))[0]; 
 
    # Check whether we should fix first and then display
    if (($fix) && ($fixsku)) { 
        # Find the correct department for this sku
        $query = dequote(<< "        ENDQUERY");
            select class from elderVillagesSku
            where plunum = '$fixsku'
        ENDQUERY
        my $tbl_ref = execQuery_arrayref($dbh, $query);  
        my $correct_dept=$$tbl_ref[1][0];
        # Fix a specified SKU     
        $query = dequote(<< "        ENDQUERY");
            update plu set deptnum = '$correct_dept' 
            where plunum = '$fixsku'
        ENDQUERY
        
        if ($dbh->do($query)) {
            print $q->p("Corrected department number for SKU $fixsku to $correct_dept.");
            ElderLog($logfile,"Corrected department number for $fixsku");  
        } else {
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }    
        # Clear the flag
        $fix=0;
    } 
    if ($fixall) {        
        # Fix all of the SKUs
        my %found_dept;
        my @tmp;
        # Collect the information of how things are set currently
        $query = dequote(<< "        ENDQUERY"); 
        select 
            p.plunum,p.deptnum
        from 
            plu p        
        where 
            p.vendorid = '01'        
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            $found_dept{$tmp[0]}=$tmp[1];
        }
        # Now get the expected list
        my %expected_hash;
        $query = dequote(<< "        ENDQUERY"); 
        select 
            plunum,pludesc,class
        from 
            elderVillagesSku            
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            next unless(defined($tmp[0]));            
            my $plu=$tmp[0];
            my $desc=$tmp[1];
            my $setting=$tmp[2];
            if ($setting eq "") {
                $setting="Not Set";            
            }          
            my @array=(
                "$desc",
                "$setting"
            );
            $expected_hash{$plu}=\@array;        
        }    
        my $expected_dept;
        foreach my $sku (keys(%found_dept)) {
            my $desc=$expected_hash{$sku}[0];
            next unless ($expected_hash{$sku}[1]); # If the SKU is found in the store but not on our list from Akron, skip it.           
            $expected_dept=$expected_hash{$sku}[1];
            unless ($found_dept{$sku} eq $expected_dept) {  
                # Correct this department
                $query = "update plu set deptnum = $expected_dept where plunum = '$sku'";
                if ($dbh->do($query)) {
                    print $q->p("Updated deptnum to $expected_dept for SKU $sku");
                    ElderLog($logfile,"Updated deptnum to $expected_dept for $sku");  
                } else {
                    ElderLog($logfile,"Error trying to correct deptnum for $sku");
                    print "Database Update Error"."<br />\n";
                    print "Driver=ODBC"."<br />\n";
                    print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                    print "query=".$query."<br />\n";
                    print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                    print "errstr=".$dbh->errstr ."<br />\n";
                    StandardFooter(); 
                    exit;
                }                
            }
        }        
        # Clear the flag
        $fixall=0;
    }    
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>'DEPTTOOL'});  
    print $q->submit({-accesskey=>'G', -value=>"   Re-Run Department Check   "});   
    print $q->end_form(); 
    StandardFooter(); 
	
} elsif ($report eq "NVDEPTTOOL2") {
    print $q->h3("Non-Villages Department Tool  "); 
    my $fix= ($q->param("fix"))[0]; 
    my $fixall= ($q->param("fixall"))[0];     
    my $fixsku= ($q->param("sku"))[0]; 
 
    # Check whether we should fix first and then display
    if (($fix) && ($fixsku)) { 
        # Find the correct department for this sku
        $query = dequote(<< "        ENDQUERY");
            select class from elderNonVillagesSku
            where plunum = '$fixsku'
        ENDQUERY
        my $tbl_ref = execQuery_arrayref($dbh, $query);  
        my $correct_dept=$$tbl_ref[1][0];
        # Fix a specified SKU     
        $query = dequote(<< "        ENDQUERY");
            update plu set deptnum = '$correct_dept' 
            where plunum = '$fixsku'
        ENDQUERY
        
        if ($dbh->do($query)) {
            print $q->p("Corrected department number for SKU $fixsku to $correct_dept.");
            ElderLog($logfile,"Corrected department number for $fixsku");  
        } else {
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }    
        # Clear the flag
        $fix=0;
    } 
    if ($fixall) {        
        # Fix all of the SKUs
        my %found_dept;
        my @tmp;
        # Collect the information of how things are set currently
        $query = dequote(<< "        ENDQUERY"); 
        select 
            p.plunum,p.deptnum
        from 
            plu p        
        where 
            p.vendorid not in ('01')
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            $found_dept{$tmp[0]}=$tmp[1];
        }
        # Now get the expected list
        my %expected_hash;
        $query = dequote(<< "        ENDQUERY"); 
        select 
            plunum,pludesc,class
        from 
            elderNonVillagesSku            
        ENDQUERY

        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            next unless(defined($tmp[0]));            
            my $plu=$tmp[0];
            my $desc=$tmp[1];
            my $setting=$tmp[2];
            if ($setting eq "") {
                $setting="Not Set";            
            }          
            my @array=(
                "$desc",
                "$setting"
            );
            $expected_hash{$plu}=\@array;        
        }    
        my $expected_dept;
        foreach my $sku (keys(%found_dept)) {
            my $desc=$expected_hash{$sku}[0];
            next unless ($expected_hash{$sku}[1]); # If the SKU is found in the store but not on our list from Akron, skip it.           
            $expected_dept=$expected_hash{$sku}[1];
            unless ($found_dept{$sku} eq $expected_dept) {  
                # Correct this department
                $query = "update plu set deptnum = $expected_dept where plunum = '$sku'";
                if ($dbh->do($query)) {
                    print $q->p("Updated deptnum to $expected_dept for SKU $sku");
                    ElderLog($logfile,"Updated deptnum to $expected_dept for $sku");  
                } else {
                    ElderLog($logfile,"Error trying to correct deptnum for $sku");
                    print "Database Update Error"."<br />\n";
                    print "Driver=ODBC"."<br />\n";
                    print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                    print "query=".$query."<br />\n";
                    print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                    print "errstr=".$dbh->errstr ."<br />\n";
                    StandardFooter(); 
                    exit;
                }                
            }
        }        
        # Clear the flag
        $fixall=0;
    }    
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>'NVDEPTTOOL'});  
    print $q->submit({ -value=>"   Re-Run Department Check   "});   
    print $q->end_form(); 
    StandardFooter(); 
} elsif ($report eq "INVXREF") {
    print $q->h3("Checking for Xrefnum as SKU in Inventory Current"); 
    $query = dequote(<< "    ENDQUERY");
        select 
            ic.plunum,
            pcr.plunum as "Matching SKU",
            p.pludesc,
            p.vendorid,
            ic.qtycurrent,
            '' as "Qty under SKU"
        from 
            inventory_current ic 
            join plu_cross_ref pcr on ic.plunum = pcr.xrefnum 
            join plu p on pcr.plunum = p.plunum 
        where 
            length(ic.plunum) > 10
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);  
    if (recordCount($tbl_ref)) {  
        # Find the qty in invetory under the matching SKU number
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $sku  = trim($$thisrow[1]);            
            $query="select qtycurrent from inventory_current where plunum = '$sku'";
            my $sth = $dbh->prepare($query);        
            $sth->execute();                
            while (my @tmp=$sth->fetchrow_array()) {              
                $$tbl_ref[$datarows][5] = $tmp[0];
            }
        }        
        #    
        print $q->p("These plunum entries in inventory_current appear to be Xref numbers.  <br />
        Click on the plunum to clear it from inventory_current
        or select the 'Clear Them All' button at the bottom.");
        my @links = ( "$scriptname?report=INVXREF2&table=inventory_current&".'skuid=$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '', '');   
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'INVXREF4'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"table", -value=>"inventory_current"}), "\n";   
        print $q->submit({-accesskey=>'C', -value=>"   Clear Them All   "});   
        print $q->end_form();        
    } else { 
        print $q->b("no Xref numbers found in inventory_current"); 
        
    }        
    if (0) {  # This section does not seem important and is currently disabled - kdg
    print $q->h3("Checking for Xrefnum as SKU in Inventory History"); 
    $query = dequote(<< "    ENDQUERY");
        select 
            ih.plunum,
            ih.periodstart,
            pcr.plunum as "Matching SKU",
            p.pludesc,
            p.vendorid,
            ih.closeqty,
            '' as "Qty under SKU"            
        from 
            inventory_history ih
            join plu_cross_ref pcr on ih.plunum = pcr.xrefnum 
            join plu p on pcr.plunum = p.plunum 
        where 
            length(ih.plunum) > 10
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);  
    if (recordCount($tbl_ref)) {       
        # Find the qty in invetory under the matching SKU number
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $sku  = trim($$thisrow[2]);            
            $query="select closeqty from inventory_history where plunum = '$sku'";
            my $sth = $dbh->prepare($query);        
            $sth->execute();                
            while (my @tmp=$sth->fetchrow_array()) {              
                $$tbl_ref[$datarows][5] = $tmp[0];
            }
        }    
        print $q->p("These plunum entries in inventory_history appear to be Xref numbers.  
        <br />Click on the plunum to clear it from inventory_history
        or select the 'Clear Them All' button at the bottom.");
        my @links = ( "$scriptname?report=INVXREF2&table=inventory_history&".'skuid=$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '', '');   
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'INVXREF4'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"table", -value=>"inventory_history"}), "\n";   
        print $q->submit({-accesskey=>'R', -value=>"   Clear Them All   "});   
        print $q->end_form();        
    } else { 
        print $q->b("no Xref numbers found in inventory_history"); 
    }         
    }
    StandardFooter();     
} elsif ($report eq "INVXREF2") {
    my $plunum= ($q->param("skuid"))[0]; 
    my $table= ($q->param("table"))[0]; 
    print $q->h3("Inventory Fix Tool - Clearing inventory under Xrefnum "); 
    print $q->p("Confirm that you wish to clear the entry for $plunum from $table");
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>'INVXREF3'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"skuid", -value=>$plunum}), "\n";     
    print $q->input({-type=>"hidden", -name=>"table", -value=>$table}), "\n";  
    print $q->submit({-accesskey=>'C', -value=>"   Clear   "});   
    print $q->end_form();
    print $q->start_form({-method=>"post", -name=>"form1", -action=>"javascript:parent.history.go(-1)"}),"\n";                           
    print $q->submit({-accesskey=>'A', -value=>"   Abort   "});   
    print $q->end_form();

    StandardFooter(); 
    
} elsif ($report eq "INVXREF3") {
    my $plunum= ($q->param("skuid"))[0]; 
    my $table= ($q->param("table"))[0]; 
    print $q->h3("Inventory Fix Tool - Clearing inventory under Xrefnum "); 
    print $q->p("Clearing the entry for $plunum from $table"); 
    $query="delete from $table where plunum like '$plunum'";
    if ($dbh->do($query)) {
        print $q->p("Deleted  $plunum from $table");
        ElderLog($logfile,"Deleted  $plunum from $table");  
        # Display an option to view inventory current and inventory history tables;
        print $q->p(
               $q->a({-href=>"$scriptname?report=INVXREF", -class=>"button1"}, "Check inventory"). "Run a check for Xref numbers as SKUs in inventory_current and inventory_history"
                );
    } else {
        ElderLog($logfile,"Error trying to clear $table for $plunum");
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n";
        StandardFooter(); 
        exit;
    }           
    StandardFooter(); 
} elsif ($report eq "INVXREF4") {    
    my $table= ($q->param("table"))[0]; 
    
    print $q->h3("Inventory Fix Tool - Clearing inventory under Xrefnum "); 
    print $q->p("Clearing all the entries for Xrefnums from $table"); 
    $query="delete from inventory_current where length(plunum) > 10";

    if ($dbh->do($query)) {
        print $q->p("Deleted Xrefnum froms $table");
        ElderLog($logfile,"Deleted Xrefnums from $table");  
    # Display an option to view inventory current and inventory history tables;
    print $q->p(
           $q->a({-href=>"$scriptname?report=INVXREF", -class=>"button1"}, "Check inventory"). "Run a check for Xref numbers as SKUs in inventory_current and inventory_history"
            );
    } else {
        ElderLog($logfile,"Error trying to clear Xrefnums from $table ");
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n";
        StandardFooter(); 
        exit;
    }            
       
    StandardFooter();     
} elsif ($report eq "SKUCHECK") {
    print $q->h3("SKU Check Tool "); 
	# SKUs with no barcode
	# Certain outside vendors do not have bar codes.  The are excluded
	# For Contract stores, just check villages items
	my $andwhere=" and	p.vendorid not like '07' and p.vendorid not like '02'";
    my $storenum = getStoreNum($dbh);  
    my $storeStatus=$store_cc_hash{$storenum};
    if ($storeStatus eq "Contract") {
		$andwhere=" and p.vendorid like '01' ";
	}
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent 
        from 
            plu p
            join inventory_current ic on ic.plunum = p.plunum
            join elderAkronSkuInfo asi on p.plunum = asi.sku
        where 
            p.plunum not in (select plunum from plu_cross_ref) 
        and
            p.deptnum not like '80%' 
        and
            p.plunum not like '92%'
        and
            p.plunum not like '22%'        
        and
            ic.canorder like 'Y' 
		$andwhere
			
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) {        
        print $q->p("These SKU entries have no barcodes");
        print $q->p("Use the webtool to publish SKUs to fix them. ");    
        printResult_arrayref2($tbl_ref, '', '', '');   
          
    } else { 
        print $q->b("no SKUs with missing barcodes.<br />\n"); 
    }        
	# SKUs with no description
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,evs.pludesc as "Expected Desc" 
        from 
            plu p
            left outer join elderVillagesSKU evs on p.plunum = evs.plunum
            left outer join inventory_current ic on p.plunum = ic.plunum
        where 
            p.pludesc like ''
            or
            p.pludesc is NULL
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) {        
        my $sku_list;
        for my $datarows (1 .. $#$tbl_ref)
        {
            my $thisrow = @$tbl_ref[$datarows];
            my $sku = trim($$thisrow[0]);
            $sku_list.="$sku,";            
        }    
        print $q->p("These SKU entries have no description");
        print $q->p("Use the SKU edit tool to fix them. ");    
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '', '');   
        print $q->p(
           $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN3&fixall=$sku_list", -class=>"button1"}, "Purge all of these SKUs")
           ,"Purge the SKUs using an ASC file."
        );           
    } else { 
        print $q->b("no SKUs with no desciption.<br />\n"); 
    }    
	# SKUs with no vendor id
    # Excuse the misc SKU 9999
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,evs.pludesc as "Expected Desc" 
        from 
            plu p
            left outer join elderVillagesSKU evs on p.plunum = evs.plunum
            left outer join inventory_current ic on p.plunum = ic.plunum
        where 
            p.plunum not in ('9999')
            and 
            (p.vendorid like ''
            or
            p.vendorid is NULL)            
            
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) { 
        my $sku_list;
        for my $datarows (1 .. $#$tbl_ref)
        {
            my $thisrow = @$tbl_ref[$datarows];
            my $sku = trim($$thisrow[0]);
            $sku_list.="$sku,";            
        }    
        print $q->p("These SKU entries have no vendorid");
        print $q->p("Use the SKU edit tool to fix them. ");    
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '', '');  
        print $q->p(
           $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUORPHAN3&fixall=$sku_list", -class=>"button1"}, "Purge all of these SKUs")
           ,"Purge the SKUs using an ASC file."
        );        
    } else { 
        print $q->b("no SKUs with no vendorid.<br />\n"); 
    }   
	# SKUs with vendor id not in the vendor table
    # Excuse the misc SKU 9999
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,evs.pludesc as "Expected Desc" 
        from 
            plu p
            left outer join elderVillagesSKU evs on p.plunum = evs.plunum
            left outer join inventory_current ic on p.plunum = ic.plunum
			
        where 
            p.vendorid not in (select vendorid from vendor)
            and
             p.plunum not in ('9999')
 
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) { 
        my $sku_list;
        for my $datarows (1 .. $#$tbl_ref)
        {
            my $thisrow = @$tbl_ref[$datarows];
            my $sku = trim($$thisrow[0]);
            $sku_list.="$sku,";            
        }    
        print $q->p("These SKU entries have vendorid set to a vendor not in the vendor table.");
        print $q->p("Select a SKU to use the SKU edit tool. ");  
        my @links = ( "$scriptname?report=SKUEDIT&skuid=".'$_' ); 
        printResult_arrayref2($tbl_ref, \@links, '', '');  
       
    } else { 
        print $q->b("no SKUs with unknown vendorid.<br />\n"); 
    }  	
    ####################
    # Returns and Exchanges
    ####################
    my $fix_returns;
    # Check for no return flags
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",uf4.codename as "StoreStatus",uf3.codename as "AkronStatus",p.returnflg
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
            left outer join user_flag_code uf4 on uf4.userflagnum=4 and uf4.codeid=p.userflagnum4
            left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3               
        where 
            p.returnflg = 'N'
        and 
            p.invcontrolled = 'Y'            
        and
            (p.plunum not like '93%' and p.vendorid like '01')
        and
            (p.plunum not like '99_____')    
        and
            (p.plunum not like '22_____')         
            
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) {         
        $fix_returns++;
        print $q->p("These SKU entries have return flag set to 'N'");           
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_returns=$fix_returns&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");
        print $q->p();
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_returns", -value=>"$fix_returns"}), "\n";           
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with return flag set to 'N'.<br />\n"); 
    }      
    # Check for no allow discount flags
    my $fix_allow_discount;
    $andwhere='';
    $storenum = getStoreNum($dbh);  
    $storeStatus=$store_cc_hash{$storenum};
    if ($storeStatus eq "Contract") {
        # Contract stores are allowed to set SKUs to disallow discounts if they have an Akron status other than ACTIVE
        $andwhere =" and uf3.codename like 'ACTIVE' and uf4.codename not like 'MARKDOWN'";
    }     
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",uf4.codename as "StoreStatus",uf3.codename as "AkronStatus",p.allowdisc
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
            left outer join user_flag_code uf4 on uf4.userflagnum=4 and uf4.codeid=p.userflagnum4
            left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3 
            join elderVillagesSKU evs on p.plunum = evs.plunum
        where 
            p.allowdisc = 'N'
        and 
            p.invcontrolled = 'Y'            
        and
            (p.plunum not like '93%' and p.vendorid like '01')
        and
            (p.plunum not like '92%' and p.vendorid like '01')               
        and
            (p.plunum not like '99_____')    
        and
            (p.plunum not like '22_____')     
        and
            (evs.attributes not like '%TF%' and evs.userflag3 like 'ACTIVE')
        $andwhere            
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) { 
        $fix_allow_discount++;
        print $q->p("These SKU entries have allow discount flag set to 'N'");           
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_allow_discount=$fix_allow_discount&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');  
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";          
        print $q->input({-type=>"hidden", -name=>"fix_allow_discount", -value=>"$fix_allow_discount"}), "\n";           
        print $q->submit({-accesskey=>'G', -value=>"   Fix "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with  allow discount flag set to 'N'.<br />\n"); 
    }   
    # Check for no disallow sale flag
    my $fix_disallow_sale;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",uf4.codename as "StoreStatus",uf3.codename as "AkronStatus",p.disallowsell
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
            left outer join user_flag_code uf4 on uf4.userflagnum=4 and uf4.codeid=p.userflagnum4
            left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3               
        where 
            p.disallowsell = 'Y'
        and 
            p.invcontrolled = 'Y'            
        and
            (p.plunum not like '93%' and p.vendorid like '01')
        and
            (p.plunum not like '99_____')    
        and
            (p.plunum not like '22_____')            
            
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) { 
        $fix_disallow_sale++;
        print $q->p("These SKU entries have disallow sell flag set to 'Y'");           
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_disallow_sale=$fix_disallow_sale&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();     
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";    
        print $q->input({-type=>"hidden", -name=>"fix_disallow_sale", -value=>"$fix_disallow_sale"}), "\n";           
        print $q->submit({-accesskey=>'G', -value=>"   Fix "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with  allow  disallow sell flag set to 'Y'.<br />\n"); 
    }       
    # Check for no disallow exchange flag
    my $fix_disallow_exchange;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",uf4.codename as "StoreStatus",uf3.codename as "AkronStatus",p.disallowexchange
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
            left outer join user_flag_code uf4 on uf4.userflagnum=4 and uf4.codeid=p.userflagnum4
            left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3            
        where 
            p.disallowexchange = 'Y'
        and 
            p.invcontrolled = 'Y'            
        and
            (p.plunum not like '93%' and p.vendorid like '01')
        and
            (p.plunum not like '99_____')    
        and
            (p.plunum not like '22_____')             
            
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) { 
        $fix_disallow_exchange++;   
        print $q->p("These SKU entries have disallow exchange flag set to 'Y'");           
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_disallow_exchange=$fix_disallow_exchange&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_disallow_exchange", -value=>"$fix_disallow_exchange"}), "\n";           
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with  allow  disallow exchange flag set to 'Y'.<br />\n"); 
    }           
    # Check for no employee discount flag
    my $fix_employee_discount;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",uf4.codename as "StoreStatus",uf3.codename as "AkronStatus",p.empdiscountallowed
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
            left outer join user_flag_code uf4 on uf4.userflagnum=4 and uf4.codeid=p.userflagnum4
            left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3
        where 
            p.empdiscountallowed = 'N'
        and 
            p.invcontrolled = 'Y'            
        and
            (p.plunum not like '93%' and p.vendorid like '01')
        and
            (p.plunum not like '92%' and p.vendorid like '01')               
        and
            (p.plunum not like '99_____')    
        and
            (p.plunum not like '22_____')  
        $andwhere
            
    ENDQUERY
    # A slightly different query for contract stores
 
    $tbl_ref = execQuery_arrayref($dbh, $query);  
    if (recordCount($tbl_ref)) { 
        $fix_employee_discount++;      
        print $q->p("These SKU entries have employee discount allowed flag set to 'N'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_employee_discount=$fix_employee_discount&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_employee_discount", -value=>"$fix_employee_discount"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with employee discount allowed flag set to 'N'.<br />\n"); 
    }        
    # Check for verifyplu flag
    my $fix_verifyplu;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.verifyplu
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.verifyplu = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_verifyplu++;      
        print $q->p("These SKU entries have verifyplu flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_verifyplu=$fix_verifyplu&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_verifyplu", -value=>"$fix_verifyplu"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with verifyplu flag set to 'Y'.<br />\n"); 
    }     
    ##############
    # Check for usedeptflag
    ##############
    my $fix_usedeptflag;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.usedeptflg
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.usedeptflg = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_usedeptflag++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_usedeptflag=$fix_usedeptflag&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_usedeptflag", -value=>"$fix_usedeptflag"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with usedeptflg flag set to 'Y'.<br />\n"); 
    }   
    ##############
    # Check for scaleitem
    ##############
    my $fix_scaleitem;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.scaleitem
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.scaleitem = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_scaleitem++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_scaleitem=$fix_scaleitem&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_scaleitem", -value=>"$fix_scaleitem"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with scaleitem flag set to 'Y'.<br />\n"); 
    }   
    ##############
    # Check for coupitem
    ##############
    my $fix_coupitem;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.coupitem
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.coupitem = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_coupitem++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_coupitem=$fix_coupitem&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_coupitem", -value=>"$fix_coupitem"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with coupitem flag set to 'Y'.<br />\n"); 
    }         
    ##############
    # Check for gen coupon
    ##############
    my $fix_get_coupon;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.gencoupon
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.gencoupon = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_get_coupon++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_get_coupon=$fix_get_coupon&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_get_coupon", -value=>"$fix_get_coupon"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with gencoupon flag set to 'Y'.<br />\n"); 
    }     
    ##############
    # Check for matchmfg
    ##############
    my $fix_matchmfg;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.matchmfg
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.matchmfg = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_matchmfg++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_matchmfg=$fix_matchmfg&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_matchmfg", -value=>"$fix_matchmfg"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with matchmfg flag set to 'Y'.<br />\n"); 
    }      
    ##############
    # Check for qtypricing
    ##############
    my $fix_qtypricing;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.qtypricing
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.qtypricing = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_qtypricing++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_qtypricing=$fix_qtypricing&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_qtypricing", -value=>"$fix_qtypricing"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with qtypricing flag set to 'Y'.<br />\n"); 
    }      
    ##############
    # Check for linkitem
    ##############
    my $fix_linkitem;
    $query = dequote(<< "    ENDQUERY");
        select 
            p.plunum,p.deptnum,p.pludesc,p.vendorid,p.retailprice,ic.qtycurrent,uf5.codename as "NI Date",p.linkitemflg
        from 
            plu p            
            left outer join inventory_current ic on p.plunum = ic.plunum
            left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5
        where 
            p.linkitemflg = 'Y'                                   
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);      
    ###
    if (recordCount($tbl_ref)) { 
        $fix_linkitem++;      
        print $q->p("These SKU entries have usedeptflag flag set to 'Y'"); 
        my @links = ( "$scriptname?report=SKU_FIX_TOOL&fix_linkitem=$fix_linkitem&sku_selection=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, ''); 
        print $q->p("Select an individual SKU to fix just that SKU or select 'Fix' below to fix all of them");        
        print $q->p();        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_linkitem", -value=>"$fix_linkitem"}), "\n";   
        print $q->submit({-accesskey=>'G', -value=>"   Fix  "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();        
    } else { 
        print $q->b("no SKUs with linkitemflg flag set to 'Y'.<br />\n"); 
    }          
    if (($fix_returns) ||
        ($fix_allow_discount) ||
        ($fix_disallow_sale) ||
        ($fix_disallow_exchange) ||
        ($fix_verifyplu) ||
        ($fix_usedeptflag) ||
        ($fix_scaleitem) ||
        ($fix_coupitem) ||
        ($fix_get_coupon) ||
        ($fix_matchmfg) ||
        ($fix_qtypricing) ||
        ($fix_linkitem) ||
        ($fix_employee_discount)) {
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKU_FIX_TOOL'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_returns", -value=>"$fix_returns"}), "\n";   
        print $q->input({-type=>"hidden", -name=>"fix_allow_discount", -value=>"$fix_allow_discount"}), "\n";   
        print $q->input({-type=>"hidden", -name=>"fix_disallow_sale", -value=>"$fix_disallow_sale"}), "\n";   
        print $q->input({-type=>"hidden", -name=>"fix_disallow_exchange", -value=>"$fix_disallow_exchange"}), "\n";   
        print $q->input({-type=>"hidden", -name=>"fix_employee_discount", -value=>"$fix_employee_discount"}), "\n";   
        print $q->input({-type=>"hidden", -name=>"fix_verifyplu", -value=>"$fix_verifyplu"}), "\n"; 
        print $q->input({-type=>"hidden", -name=>"fix_usedeptflag", -value=>"$fix_usedeptflag"}), "\n"; 
        print $q->input({-type=>"hidden", -name=>"fix_scaleitem", -value=>"$fix_scaleitem"}), "\n";         
        print $q->input({-type=>"hidden", -name=>"fix_coupitem", -value=>"$fix_coupitem"}), "\n";   
        print $q->input({-type=>"hidden", -name=>"fix_get_coupon", -value=>"$fix_get_coupon"}), "\n";  
        print $q->input({-type=>"hidden", -name=>"fix_matchmfg", -value=>"$fix_matchmfg"}), "\n"; 
        print $q->input({-type=>"hidden", -name=>"fix_qtypricing", -value=>"$fix_qtypricing"}), "\n";      
        print $q->input({-type=>"hidden", -name=>"fix_linkitem", -value=>"$fix_linkitem"}), "\n";          
        print $q->submit({-accesskey=>'G', -value=>"   Fix Them All   "})," (This creates an ASC file which may take up to 30 seconds to process.)\n";   
        print $q->end_form();
 
    }
    StandardFooter();  
} elsif ($report eq "SKUNUMCHECK") {
    print $q->h3("SKU Number Check Tool "); 
	#  Check for various issues with SKU numbers
    my $sku_count=0;
    my $issue_count=0;
    my @plu_array=();
 
    $query = dequote(<< "    ENDQUERY");
        select 
            plunum 
        from 
            plu
        order by
            plunum
			
    ENDQUERY
    
    $sth=$dbh->prepare($query);	
    $sth->execute();
   
    while (my @tmp=$sth->fetchrow_array()) {      
        my $plunum=$tmp[0];
        if ($plunum) {
            # We are checking numerical plunums so skip if it contains alpha characters
            if ($plunum =~ /\D/) {
                print "SKU $plunum can be accessed only with quotes because it has letters<br />";
            } else {
                push(@plu_array,$plunum);
            }
        }  
    }
    foreach my $plunum (@plu_array) {        
        $sku_count++;
        my $plunum2;
        my $plunum3;

        my $query = dequote(<< "        ENDQUERY");
            select 
                plunum 
            from 
                plu
            where             
                plunum = $plunum
                
        ENDQUERY
                
        my $sth=$dbh->prepare($query);	
        $sth->execute();   
        
        while (my @tmp=$sth->fetchrow_array()) {         
            $plunum2=$tmp[0];            
        }
     
        unless ("$plunum2" eq "$plunum") {
            my $query = dequote(<< "            ENDQUERY");
                select 
                    plunum 
                from 
                    plu
                where             
                    plunum = '$plunum'
                    
            ENDQUERY
            my $sth=$dbh->prepare($query);	
            $sth->execute();   
            
            while (my @tmp=$sth->fetchrow_array()) {         
                $plunum3=$tmp[0];                
            }
            if ("$plunum3" eq "$plunum") {
                print "SKU $plunum can be accessed only with quotes<br />";
                $issue_count++;
            }
        }
       
    }    
 
    print $q->p("Checked $sku_count SKUs and found $issue_count issues.");
   
    StandardFooter();      
} elsif ($report eq "SKU_FIX_TOOL") {
    print $q->h3("SKU Fix Tool"); 
    my $sku_selection= ($q->param("sku_selection"))[0]; 
    my $fix_returns= ($q->param("fix_returns"))[0]; 
    my $fix_allow_discount= ($q->param("fix_allow_discount"))[0]; 
    my $fix_disallow_sale= ($q->param("fix_disallow_sale"))[0]; 
    my $fix_disallow_exchange= ($q->param("fix_disallow_exchange"))[0]; 
    my $fix_employee_discount= ($q->param("fix_employee_discount"))[0];
    my $fix_verifyplu= ($q->param("fix_verifyplu"))[0];  
    my $fix_usedeptflag= ($q->param("fix_usedeptflag"))[0];  
    my $fix_scaleitem= ($q->param("fix_scaleitem"))[0];  
    my $fix_coupitem= ($q->param("fix_coupitem"))[0];  
    my $fix_get_coupon= ($q->param("fix_get_coupon"))[0];  
    my $fix_matchmfg= ($q->param("fix_matchmfg"))[0];  
    my $fix_qtypricing= ($q->param("fix_qtypricing"))[0];  
    my $fix_linkitem= ($q->param("fix_linkitem"))[0];      
      
    my %sku_hash;
    my $sku_where;    
    if ($sku_selection) {
        $sku_where=" and plunum = '$sku_selection'";
    }
    if ($fix_usedeptflag) {
        $query = "update plu set usedeptflg = 'N' where usedeptflg = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set usedeptflg to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set usedeptflg to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }          
    if ($fix_scaleitem) {
        $query = "update plu set scaleitem = 'N' where scaleitem = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set scaleitem to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set scaleitem to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }      
    if ($fix_coupitem) {
        $query = "update plu set coupitem = 'N' where coupitem = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set coupitem to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set coupitem to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }      
    if ($fix_get_coupon) {
        $query = "update plu set gencoupon = 'N' where gencoupon = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set gencoupon to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set gencoupon to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }        
    if ($fix_matchmfg) {
        $query = "update plu set matchmfg = 'N' where matchmfg = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set matchmfg to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set matchmfg to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }      
    if ($fix_qtypricing) {
        $query = "update plu set qtypricing = 'N' where qtypricing = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set qtypricing to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set qtypricing to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }  
    if ($fix_linkitem) {
        $query = "update plu set linkitemflg = 'N' where linkitemflg = 'Y' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set linkitemflg to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set linkitemflg to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    } 
    if ($fix_returns) {
        $query = "update plu set returnflg = 'Y' where returnflg = 'N' $sku_where";   
        if ($dbh->do($query)) {
            print $q->p("Successfully set return flag to 'Y' ");                   
        } else {
            ElderLog($logfile,"Error trying to set return flag to 'Y'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }  
    }
    if ($fix_allow_discount) {        
        $query = "update plu set allowdisc = 'Y' where allowdisc = 'N' $sku_where";  
        if ($dbh->do($query)) {
            print $q->p("Successfully set allowdisc flag to 'Y' ");                   
        } else {
            ElderLog($logfile,"Error trying to set allowdisc flag to 'Y'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        } 
    }   
    if ($fix_disallow_sale) {        
        $query = "update plu set disallowsell = 'N' where disallowsell = 'Y' $sku_where";  
        if ($dbh->do($query)) {
            print $q->p("Successfully set disallowsell flag to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set disallowsell flag to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        } 
    }    
    if ($fix_disallow_exchange) {
        $query = "update plu set disallowexchange = 'N' where disallowexchange = 'Y' $sku_where";          
        if ($dbh->do($query)) {
            print $q->p("Successfully set disallowexchange flag to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set disallowexchange flag to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        } 
    }     
    if ($fix_employee_discount) {
        $query = "update plu set empdiscountallowed = 'Y' where empdiscountallowed = 'N' $sku_where";  
        if ($dbh->do($query)) {
            print $q->p("Successfully set empdiscountallowed flag to 'Y' ");                   
        } else {
            ElderLog($logfile,"Error trying to set empdiscountallowed flag to 'Y'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        } 
    }  
    if ($fix_verifyplu) {
        $query = "update plu set verifyplu = 'N' where verifyplu = 'Y' $sku_where";  
        if ($dbh->do($query)) {
            print $q->p("Successfully set verifyplu flag to 'N' ");                   
        } else {
            ElderLog($logfile,"Error trying to set verifyplu flag to 'N'");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        } 
    }      
    # We are going to create an asc file.  Make certain there is not a pre-existing one
    my $plutxn="c:/temp/plutxn.asc";          
    if (-e $plutxn) {
        unlink $plutxn;
    }
    if (-e $plutxn) {
        ElderLog($logfile,"ERROR: Failed to delete $plutxn");
    }        
    if (open (PLUTXN, ">>$plutxn")) {
        # Just haveing the system process a plutxn file seems to make the database changes take effect
        print PLUTXN "3,'no_sku',\n";                   
        close PLUTXN;
        print "Closed $plutxn<br />"; 
    } else {
        print "Error Opening $plutxn<br />";
        StandardFooter();   
        exit;
    }        
          
    ############
    # Process the ASC
    ############
    # Move the plutxn file to the parm folder
    my $parm="${xpsDir}/parm/plutxn.asc";
    move($plutxn,$parm);
 
    # Create a trigger file and wait for it to be processed
    my $trigger="${xpsDir}/parm/newparm.trg"; 
    my $count=0;        
    if (open (TRG, ">$trigger")) {
        print TRG "trigger";
        close TRG;
        print "Created trigger file<br />";
    } else {
        print "Error creating $trigger<br />";
        StandardFooter();   
        exit;
    }
    while ((-e $trigger) && ($count < 6)) {
        sleep 5;        
        $count++;
    }
    if (-e $trigger) {
        print $q->p("ERROR: Trigger file is still present.  Is XPS running?");
        StandardFooter();   
        exit;
    } else {
  
        print "Trigger file has been processed<br /><br />";
        
        print $q->start_form({-method=>"post", -name=>"form1", -action=>"$scriptname"}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>'SKUCHECK'}), "Re-run SKU Check Tool\n";  
        print $q->submit({-accesskey=>'G', -value=>"   SKU Check   "});   
        print $q->end_form();                   
    }        
 
    StandardFooter();  
} elsif ($report eq "SKUTOOL") {
    print $q->h3("SKU Tool - SKU Selection "); 
    my $add= ($q->param("add"))[0]; 
 
    my $deptnum= ($q->param("deptnum"))[0]; 
    my $pludesc= ($q->param("pludesc"))[0]; 
    my $vendorid= ($q->param("vendorid"))[0];     
    my $retailprice= ($q->param("retailprice"))[0]; 
    my $cost= ($q->param("cost"))[0]; 
    my $mmid1= ($q->param("mmid1"))[0]; 
    my $mmid2= ($q->param("mmid2"))[0];       
    my $mmid3= ($q->param("mmid3"))[0];       
    my $mmid4= ($q->param("mmid4"))[0];       
    my $reset= ($q->param("reset"))[0];      
    my $sku= ($q->param("skuid"))[0];     
    my $propromptid= ($q->param("propromptid"))[0];  
    $sku=~s/ //g; # Trim any errant spaces.    
    my $plutxn="c:/temp/plutxn.asc";    
    my $a="a";
    if ($reset) {
        if (-e $plutxn) {
            print $q->p("Deleting $plutxn");
            unlink $plutxn;
        }
        if (-e $plutxn) {
            ElderLog($logfile,"ERROR: Failed to delete $plutxn");
        } 
    }
    if (($add) && ($sku)) {
        # add sku to asc file
        if (open (PLUTXN, ">>$plutxn")) {
            #print "Opening $plutxn<br />";
            # Field 7 - MixMatch ID 1
            # Field 34 - MixMatch ID 2
            # Field 35 - MixMatch ID 3
            # Field 36 - MixMatch ID 4
            # Field 84 - Cost
            print PLUTXN "2,$sku,,$deptnum,$pludesc,$retailprice,$mmid1,,,,,,$vendorid,,$propromptid,,,,,,,,,,,,,,,,,,,$mmid2,$mmid3,$mmid4,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,$cost\n";  
    
            close PLUTXN;
            
            #print "$plutxn created<br />";
            #print "-->Dept: $deptnum<br />";
            #print "-->Desc: $pludesc<br />";
            #print "-->Vendor: $vendorid<br />";
        } else {
            print "Error Opening $plutxn<br />";
            StandardFooter();   
            exit;
        }
    }        

    my $tbl_ref;
    if (-e $plutxn) {
        # Display the SKUs currently in the plutxn
        my @header=(
        "SKU",
        "Department",
        "Description",
        "Vendorid",
        "RetailPrice",
        "MMID1",
        "ProfilePrompt",
        "Cost"
 
        );
        open (INPUT,"$plutxn");
        my @sku_info=(<INPUT>);
        close INPUT;
        push(@$tbl_ref,\@header);        
        foreach my $s (@sku_info) {            
            my @tmp=split(/,/,$s);
            my $sku=$tmp[1];
            my $dept=$tmp[3];
            my $desc=$tmp[4];
            my $retailprice=$tmp[5];
            my $mmid=$tmp[6];              
            my $venid=$tmp[12];  
            my $pprompt=$tmp[14];  
            my $cost=$tmp[83];
             
            my @array=(
            "$sku",
            "$dept",
            "$desc",
            "$venid",
            "$retailprice",
            "$mmid",
            "$pprompt",
            "$cost"
            
            );
            push(@$tbl_ref,\@array);
        }
        if (recordCount($tbl_ref)) { 
            print $q->p("SKUs currently in plutxn.asc file");
            printResult_arrayref2($tbl_ref, '', ''); 
        }
        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUTOOL"}); 
        print $q->input({-type=>"hidden", -name=>"reset", -value=>"reset"});                
        print $q->input({-type=>"submit", -name=>"submit", -value=>"Reset"},"To clear these and start over, click the 'Reset' button.");                 
        print $q->p();        
        print $q->end_form(), "\n";   
        print $q->p();
        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUEDIT"}); 
        print $q->input({-type=>"hidden", -name=>"fix", -value=>"fix"});              
        print $q->input({-type=>"submit", -name=>"submit", -value=>"Submit"},"To process these updates, click the 'Submit' button.");                               
        print $q->p("(NOTE: It may take up to 30 seconds for the ASC file to be processed once submitted.)");
        print $q->end_form(), "\n";   
        print $q->p();   
        $a="another";
    }
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUEDIT"});    
    print  $q->input({ -type=>'text', -name=>"skuid", -size=>"8", -value=>" "});        
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
    print $q->p("Enter $a SKU number to edit and click the 'Enter' button.");
    
    print $q->end_form(), "\n";   
    StandardFooter();  
} elsif ($report eq "SKUEDIT") {
    print $q->h3("SKU Tool - SKU Edit "); 
    my $fix= ($q->param("fix"))[0]; 

    my $deptnum= ($q->param("deptnum"))[0]; 
    my $pludesc= ($q->param("pludesc"))[0]; 
    my $vendorid= ($q->param("vendorid"))[0];  
    my $sku= ($q->param("skuid"))[0];     
    my $propromptid = ($q->param("propromptid"))[0]; 
    $sku=~s/ //g; # Trim any errant spaces.
    my $plutxn="c:/temp/plutxn.asc";
    if (($fix) && ($sku)) {
         
        # Create an asc file and submit it.       
        if (open (PLUTXN, ">>$plutxn")) {
            #print "Opening $plutxn<br />";
            print PLUTXN "2,$sku,,$deptnum,$pludesc,,,,,,,,$vendorid,,$propromptid\n";                        
            close PLUTXN;
            
            #print "$plutxn created<br />";
            #print "-->Dept: $deptnum<br />";
            #print "-->Desc: $pludesc<br />";
            #print "-->Vendor: $vendorid<br />";
        } else {
            print "Error Opening $plutxn<br />";
            StandardFooter();   
            exit;
        }
    }
    if ($fix) {
        # Move the plutxn file to the parm folder
        my $parm="${xpsDir}/parm/plutxn.asc";
        move($plutxn,$parm);
        # Create a trigger file and wait for it to be processed
        my $trigger="${xpsDir}/parm/newparm.trg"; 
        my $count=0;        
        if (open (TRG, ">$trigger")) {
            print TRG "trigger";
            close TRG;
            print "Created trigger file<br />";
        } else {
            print "Error creating $trigger<br />";
            StandardFooter();   
            exit;
        }
        while ((-e $trigger) && ($count < 6)) {
            sleep 5;        
            $count++;
        }
        if (-e $trigger) {
            print $q->p("ERROR: Trigger file is still present.  Is XPS running?");
            StandardFooter();   
            exit;
        } else {         
            print "Trigger file has been processed<br /><br />";
            print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUTOOL"}, "Back to Sku Edit");  
            StandardFooter();    
            exit;
        }
    } elsif ($sku) {
        print $q->p("This is the SKU you have selected to edit");   
    } else {
        print $q->p("No SKU selected");
        StandardFooter();
        exit;
    }
 
    $query = dequote(<< "    ENDQUERY");
        select 
            plunum,deptnum,pludesc,vendorid,retailprice,mmid1,mmid2,mmid3,mmid4,cost,propromptid 
        from 
            plu p
        where 
            plunum = '$sku'            
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);  

    my ($retailprice,$mmid1,$mmid2,$mmid3,$mmid4,$cost);
    for my $datarows (1 .. $#$tbl_ref) {        
        my $thisrow = @$tbl_ref[$datarows];    
        $deptnum=$$thisrow[1];
        $pludesc=$$thisrow[2];
        $vendorid=$$thisrow[3];
        $retailprice=sprintf("%.2f",$$thisrow[4]);
        $mmid1=$$thisrow[5];
        $mmid2=$$thisrow[6];
        $mmid3=$$thisrow[7];
        $mmid4=$$thisrow[8];
        $cost=sprintf("%.2f",$$thisrow[9]);
        $propromptid=$$thisrow[10];
    }
    if (recordCount($tbl_ref)) {              
        my @format = ('', '', '','',
            {'align' => 'Right', 'currency' => ''},
            '','','','',
            {'align' => 'Right', 'currency' => ''}
            );
             
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, ''); 
        
        #print "<br />Edit the data below and click the 'Submit' button to create an ASC file to make the changes.<br />";
        #print "If you wish to edit several SKU's, edit the data and click the 'Add' button.  When all of the SKUs <br />";
        #print "have been added, click the 'Submit' button to submit all of the changes at once.<br />";
        #print "(NOTE: It may take up to 30 seconds for the ASC file to be processed once submitted.)<br />";
        print "<br />Edit the data below and click the 'Enter' button.<br />";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";        
        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUTOOL"});  
        #print $q->input({-type=>"hidden", -name=>"fix", -value=>"fix"});  
        print $q->input({-type=>"hidden", -name=>"add", -value=>"add"}); 
        print $q->input({-type=>"hidden", -name=>"skuid", -value=>"$sku"});  
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>4}),"\&nbsp;");
        print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Department Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"deptnum", -size=>"4", -value=>"$deptnum"}))
        
        );  
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "PLU Description:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"pludesc", -size=>"40", -value=>"$pludesc"}))
        );  
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "VendorID:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"vendorid", -size=>"2", -value=>"$vendorid"}))
        );      
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "RetailPrice:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"retailprice", -size=>"2", -value=>"\$$retailprice"}))
        );   
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Prompt ID:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"propromptid", -size=>"2", -value=>"$propromptid"}))
        );   
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Cost:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"cost", -size=>"2", -value=>"\$$cost"}))
        );          
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MMID1:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"mmid1", -size=>"2", -value=>"$mmid1"}))
        );           
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MMID2:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"mmid2", -size=>"2", -value=>"$mmid2"}))
        );              
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MMID3:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"mmid3", -size=>"2", -value=>"$mmid3"}))
        );              
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MMID4:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"mmid4", -size=>"2", -value=>"$mmid4"}))
        );                      
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({-accesskey=>'E', -value=>"   Enter   "}))
        );
         
        
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";  
   

    } else { 
        print $q->b("$sku not found.<br />\n"); 
        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUTOOL"});   
        print $q->input({-type=>"hidden", -name=>"add", -value=>"add"});   
        print $q->input({-type=>"hidden", -name=>"skuid", -value=>"$sku"});              
        print $q->input({-type=>"submit", -name=>"submit", -value=>"Add SKU"});                 
        print $q->p("To Add SKU $sku to your system, click the 'Add SKU' button");        
    }    
    StandardFooter();  
} elsif ($report eq "VENDORSKUPURGE") {
    print $q->h3("SKU Purge by Vendor Tool - Vendor Selection "); 
    my %vendor_hash;
    my %vendor_iso_hash;
    my %vendor_min_hash;
    my %vendor_max_hash;
    my %vendor_sku_hash;
    my %vendor_qty_hash;
    
    $query = "
        select 
            v.vendorid, 
            v.vendorname,
            ic.plunum,
            ic.canorder,
            ic.minqty,
            ic.maxqty,
            ic.qtycurrent
        from 
            vendor v 
            join inventory_current ic on v.vendorid = ic.lastvendor
        order by v.vendorid
        ";
        
    #print $q->pre("$query");
    $sth=$dbh->prepare($query);	
    $sth->execute();
   
    while (my @tmp=$sth->fetchrow_array()) {      
        my $vendorid=$tmp[0];
        my $vendorname=$tmp[1];
        my $key="$vendorid - $vendorname";
        $vendor_hash{$key}++;
        my $plunum=$tmp[2];
        if ($plunum) {
            $vendor_sku_hash{$key}++;
        }
        my $canorder=$tmp[3];        
        my $minqty=$tmp[4];
        my $maxqty=$tmp[5];
        my $qtycurrent=$tmp[6];
        if ($canorder eq "Y") {
            $vendor_iso_hash{$key}++;
        }
        if ($minqty > 0) {
            $vendor_min_hash{$key}++;
        }    
        if ($maxqty > 0) {
            $vendor_max_hash{$key}++;
        }  
        if ($qtycurrent > 0) {
            $vendor_qty_hash{$key}++;
        }           
    }    
    my @header=("VendorId","Count","Qty","ISO","Min","Max");
    my @array_to_print=\@header;
    my @array_to_print2=\@header;
    foreach my $vendor (sort keys(%vendor_hash)) {
        my @tmp=split(/\s+/,$vendor);
        my $vendorid=$tmp[0];
        $query = "
        select 
            count(*)
        from 
            plu 
        where
            vendorid = '$vendorid'
        
        ";
        #print $q->pre("$query");
        $sth=$dbh->prepare($query);	
        $sth->execute();
        my $count=0;
        while (my @tmp=$sth->fetchrow_array()) { 
            $count=$tmp[0];
        }
        
        my $iso = 0;
        if (defined($vendor_iso_hash{$vendor})) {
            $iso = $vendor_iso_hash{$vendor};
        } 
        my $min = 0;
        if (defined($vendor_min_hash{$vendor})) {
            $min = $vendor_min_hash{$vendor};
        } 
        my $max = 0;
        if (defined($vendor_max_hash{$vendor})) {
            $max = $vendor_max_hash{$vendor};
        }  
        my $qty = 0;
        if (defined($vendor_qty_hash{$vendor})) {
            $qty = $vendor_qty_hash{$vendor};
        }           
                
        my @array=("$vendor","$count","$qty","$iso","$min","$max");
        if (($iso == 0) && ($min == 0) && ($max == 0) && ($qty == 0)) {
            push(@array_to_print,\@array);
        } else {
            push(@array_to_print2,\@array);
        }
    }
    if (recordCount(\@array_to_print)) {       
        print $q->p("Select the vendor whose SKU's you wish to purge");
        my @links = ( "$scriptname?report=VENDORSKUPURGE2&vendor=".'$_' ); 
        printResult_arrayref2(\@array_to_print, \@links, '');
    } 
    if (recordCount(\@array_to_print2)) { 
        print $q->p("These vendors do not qualify to be purged.");
        print $q->p("Only vendors with no qty and where all SKU's have the ISO off and Min & Max set to 0 qualify.");
        my @links = ( "$scriptname?report=VENDORSKUPURGE2&vendor=".'$_' ); 
        printResult_arrayref2(\@array_to_print2);    
    } else {
        #print $q->h4("There are no Vendors where all of the SKUs have the ISO off and the Min & Max set to 0.");
    }
    StandardFooter();  
} elsif ($report eq "VENDORSKUPURGE2") {
    print $q->h3("SKU Purge by Vendor Tool - Vendor Selection Confirmation");
    my $vendor= ($q->param("vendor"))[0];  
    if ($vendor) {

        my @tmp=split(/\s+/,$vendor);
        my $vendorid=$tmp[0];
        $query="
            select
                plunum,
                pludesc
            from
                plu
            where
                vendorid = $vendorid
            order by
                plunum
        ";
        my $tbl_ref = execQuery_arrayref($dbh, $query); 
        if (recordCount($tbl_ref)) {  
            print $q->h4("Below are the SKUs assigned to $vendor");
            printResult_arrayref2($tbl_ref); 
            print $q->p("Please confirm that you wish to delete all SKU's for vendor $vendor");
            print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
            print $q->input({-type=>"hidden", -name=>"report", -value=>"VENDORSKUPURGE3"}); 
            print $q->input({-type=>"hidden", -name=>"vendorid", -value=>"$vendorid"});              
            print $q->input({-type=>"submit", -name=>"submit", -value=>"Confirmed"});                          
            print $q->end_form(), "\n";         
            
        } else {
            print $q->h4("Found no SKU's assigned to $vendor");
        }
    } else {
        print $q->h4("ERROR: Failed to determine vendor");         
    }
    StandardFooter(); 
} elsif ($report eq "VENDORSKUPURGE3") {
    print $q->h3("SKU Purge by Vendor Tool - Purge");
    my $vendorid= ($q->param("vendorid"))[0]; 
    my $plutxn="c:/temp/plutxn.asc";  
    my $invncont="c:/temp/INVNCONT.ASC";  
    my $xreftxn="c:/temp/XREFTXN.asc";  
    my $plutxn2="c:/temp/PLUTXN2.asc";      
    ###
    # Select all of the SKUs for the specified vendor
    my @sku_list;
 

    $query = "select plunum from plu where vendorid = '$vendorid'";
    $sth=$dbh->prepare($query);	
    $sth->execute();
   
    while (my @tmp=$sth->fetchrow_array()) {      
        my $plunum=$tmp[0];
        push(@sku_list,$plunum);
         
    }
    foreach my $sku (@sku_list) {
 
        # Find the xrefnum
        my @xrefnum_list;
        $query = "select xrefnum from PLU_Cross_Ref where plunum = '$sku'";
        $sth=$dbh->prepare($query);	
        $sth->execute();
       
        while (my @tmp=$sth->fetchrow_array()) {      
            push(@xrefnum_list,$tmp[0]);   
        }

        if (open (FILE, ">>$plutxn")) {   
            $query = "select deptnum,pludesc,vendorid from plu where plunum = '$sku'";
            $sth=$dbh->prepare($query);	
            $sth->execute();  
            my $deptnum;
            my $pludesc;
            my $vendorid;
            while (my @tmp=$sth->fetchrow_array()) {      
                $deptnum=$tmp[0];
                $pludesc=$tmp[1];
                $vendorid=$tmp[2];
            }             
            print FILE "3,$sku,,$deptnum,$pludesc,,,,,,,,$vendorid\n"; 
            close FILE;
        } else {
            print "Error Opening $plutxn<br />";
            StandardFooter();   
            exit;
        }
        if (open (FILE, ">>$invncont")) {            
            print FILE "3,$sku,1,,,,\n";                      
            close FILE;
        } else {
            print "Error Opening $invncont<br />";
            StandardFooter();   
            exit;
        }  
        if (open (FILE, ">>$xreftxn")) {  
            foreach my $xrefnum (@xrefnum_list) {
                print FILE "3,$sku,$xrefnum,\n";                     
            }
            
            close FILE;
        } else {
            print "Error Opening $xreftxn<br />";
            StandardFooter();   
            exit;
        }  
        if (open (FILE, ">>$plutxn2")) {            
            print FILE "3,$sku,,\n";                    
            close FILE;
        } else {
            print "Error Opening $plutxn2<br />";
            StandardFooter();   
            exit;
        }          
    }        
    print $q->p("Select enter to process the purge.");
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUPURGE2"});          
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                  
    print $q->end_form(), "\n";      
    ###
    StandardFooter(); 
} elsif ($report eq "SKUPURGE") {
    print $q->h3("SKU Purge Tool - SKU Selection "); 
    my $add= ($q->param("add"))[0]; 
    my $deptnum= ($q->param("deptnum"))[0]; 
    my $pludesc= ($q->param("pludesc"))[0]; 
    my $vendorid= ($q->param("vendorid"))[0];     
    my $reset= ($q->param("reset"))[0];      
    my $sku= ($q->param("skuid"))[0];     
    $sku=~s/ //g; # Trim any errant spaces.    
    my $plutxn="c:/temp/plutxn.asc";  
    my $invncont="c:/temp/INVNCONT.ASC";  
    my $xreftxn="c:/temp/XREFTXN.asc";  
    my $plutxn2="c:/temp/PLUTXN2.asc";  
    
    my $a="a";
    if ($reset) {
        if (-e $plutxn) {
            unlink $plutxn;
            unlink $invncont;
            unlink $xreftxn;
            unlink $plutxn2;
        }
        if (-e $plutxn) {
            print "ERROR: Failed to delete $plutxn<br />";
        } 
        if (-e $invncont) {
             print "ERROR: Failed to delete $invncont<br />";
        } 
        if (-e $xreftxn) {
             print "ERROR: Failed to delete $xreftxn<br />";
        } 
        if (-e $plutxn2) {
             print "ERROR: Failed to delete $plutxn2<br />";
        }         
    }
    if (($add) && ($sku)) {
        # add sku to asc file
        # Determine if there is any inventory
        my $qtycurrent=0;
        $query = "select qtycurrent from inventory_current where plunum = '$sku'";
        $sth=$dbh->prepare($query);	
        $sth->execute();
       
        while (my @tmp=$sth->fetchrow_array()) {      
            $qtycurrent=$tmp[0];
        }
        if ($qtycurrent > 1) {
            print $q->p("ERROR: $sku has a current qty of $qtycurrent");
            print $q->p("Cannot purge SKUs with quantity.");
            print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=SKUPURGE"}, "Back to purge tool");
            exit;
        }
        # Find the xrefnum
        my @xrefnum_list;
        $query = "select xrefnum from PLU_Cross_Ref where plunum = '$sku'";
        $sth=$dbh->prepare($query);	
        $sth->execute();
       
        while (my @tmp=$sth->fetchrow_array()) {      
            push(@xrefnum_list,$tmp[0]);   
        }

        if (open (FILE, ">>$plutxn")) {   
            $query = "select deptnum,pludesc,vendorid from plu where plunum = '$sku'";
            $sth=$dbh->prepare($query);	
            $sth->execute();  
            my $deptnum;
            my $pludesc;
            my $vendorid;
            while (my @tmp=$sth->fetchrow_array()) {      
                $deptnum=$tmp[0];
                $pludesc=$tmp[1];
                $vendorid=$tmp[2];
            }             
            print FILE "3,$sku,,$deptnum,$pludesc,,,,,,,,$vendorid\n"; 
            close FILE;
        } else {
            print "Error Opening $plutxn<br />";
            StandardFooter();   
            exit;
        }
        if (open (FILE, ">>$invncont")) {            
            print FILE "3,$sku,1,,,,\n";                      
            close FILE;
        } else {
            print "Error Opening $invncont<br />";
            StandardFooter();   
            exit;
        }  
        if (open (FILE, ">>$xreftxn")) {  
            foreach my $xrefnum (@xrefnum_list) {
                print FILE "3,$sku,$xrefnum,\n";                     
            }
            
            close FILE;
        } else {
            print "Error Opening $xreftxn<br />";
            StandardFooter();   
            exit;
        }  
        if (open (FILE, ">>$plutxn2")) {            
            print FILE "3,$sku,,\n";                    
            close FILE;
        } else {
            print "Error Opening $plutxn2<br />";
            StandardFooter();   
            exit;
        }          
    }        
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUPURGE"});  
    print $q->input({-type=>"hidden", -name=>"add", -value=>"add"});      
    print  $q->input({ -type=>'text', -name=>"skuid", -size=>"8", -value=>" "});        
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
    print $q->p("Enter $a SKU number to purge and click the 'Enter' button.");
    
    print $q->end_form(), "\n";   
    my $tbl_ref;
    if (-e $plutxn) {
        # Find the QOH for this SKU
        $query = "select qtycurrent from inventory_current where plunum = '$sku'";
        $sth=$dbh->prepare($query);	
        $sth->execute();  
        my $qoh;
        
        while (my @tmp=$sth->fetchrow_array()) {      
            $qoh=$tmp[0];
        }     
        # Display the SKUs currently in the plutxn
        my @header=(
        "SKU",
        "Department",
        "Description",
        "Vendorid",
        "Quantity"
        );
        open (INPUT,"$plutxn");
        my @sku_info=(<INPUT>);
        close INPUT;
        push(@$tbl_ref,\@header);        
        foreach my $s (@sku_info) {            
            my @tmp=split(/,/,$s);
            my $sku=$tmp[1];
            my $dept=$tmp[3];
            my $desc=$tmp[4];
            my $venid=$tmp[12];            
            my @array=(
            "$sku",
            "$dept",
            "$desc",
            "$venid",
            "$qoh"
            );
            push(@$tbl_ref,\@array);
        }
        if (recordCount($tbl_ref)) { 
            print $q->p("SKUs currently in plutxn.asc file");
            printResult_arrayref2($tbl_ref, '', ''); 
        }
        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUPURGE"}); 
        print $q->input({-type=>"hidden", -name=>"reset", -value=>"reset"});                
        print $q->input({-type=>"submit", -name=>"submit", -value=>"Reset"},"To clear these and start over, click the 'Reset' button.");                 
        print $q->p();        
        print $q->end_form(), "\n";   
        print $q->p();
        print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUPURGE2"});                       
        print $q->input({-type=>"submit", -name=>"submit", -value=>"Submit"},"To process these purges, click the 'Submit' button.");                               
        print $q->p("(NOTE: It may take up to 30 seconds for the ASC file to be processed once submitted.)");
        print $q->end_form(), "\n";   
        print $q->p();   
        $a="another";
    }

    StandardFooter();  
} elsif ($report eq "SKUPURGE2") {
    print $q->h3("SKU Purge Tool "); 

    # Move the plutxn file to the parm folder
    my $filepath="c:/temp/";
    my $invncont="c:/temp/INVNCONT.ASC";  
    my $xreftxn="c:/temp/XREFTXN.asc";  
    my $plutxn2="c:/temp/PLUTXN2.asc";  
    my $purge_archive="c:/temp/purgeArchive";
    unless (-d $purge_archive) {        
        mkdir $purge_archive;
        unless (-d $purge_archive) {            
            print "Error: Failed to make $purge_archive<br />";
        }        
    }   
    # Save a copy of files
    # get current date
    my @ltm = localtime();
    my $date = sprintf("%4d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
    my $error=0;

    foreach my $filename ("PLUTXN.ASC","INVNCONT.ASC","XREFTXN.ASC","PLUTXN2.ASC") {
        my $outfile=$filepath.$filename;
        my $parm="${xpsDir}/parm/".$filename;
        my $archivefilebase="${purge_archive}/${filename}-".$date;
        my $archivefile=$archivefilebase;
        my $instance=0;
        while (-f $archivefile) {
            $instance++;
            $archivefile=$archivefilebase."-".$instance;
        }
        if (-f $outfile) {
            copy ($outfile, $archivefile);
            unless (-f $archivefile) {
                print "Error saving $archivefile<br />";                
                $error++;
            }
        } else {
            print "Error: Could not locate $outfile<br />";
            $error++;
        }   
        move($outfile,$parm); 
        unless (-f $parm) {
            print "Error creating  $parm<br />";                
            $error++;
        }                    
    }    
    if ($error) {
        print $q->p("Aborting purge due to errors");
        ElderLog($logfile,"Aborting purge due to errors");        
    } else {
        # Create a trigger file and wait for it to be processed
        my $trigger="${xpsDir}/parm/newparm.trg"; 
        my $count=0;        
        if (open (TRG, ">$trigger")) {
            print TRG "trigger";
            close TRG;
            print "Created trigger file<br />";
        } else {
            print "Error creating $trigger<br />";
            StandardFooter();   
            exit;
        }
        while ((-e $trigger) && ($count < 6)) {
            sleep 5;        
            $count++;
        }
        if (-e $trigger) {
            print $q->p("ERROR: Trigger file is still present.  Is XPS running?");
            StandardFooter();   
            exit;
        } else {         
            print "Trigger file has been processed<br /><br />";
            StandardFooter();    
            exit;
        }
    }
    StandardFooter();    
} elsif ($report eq "LAYAWAY") {
    print $q->h3("Layaway Tool"); 
    my @reportlist = (    
    ["View Layaways", "/cgi-bin/i_reports.pl", "LAYAWAY",
    "View a layaway before deleting.  (Note: this takes you out of the toolshed so you must return to use the delete tool.)" ],
    ["Layaway Details", "/cgi-bin/i_toolshed.pl", "LAYAWAYDET",
    "Look at all table records related to a specific layaway." ], 
    ["Delete", "/cgi-bin/i_toolshed.pl", "LAYAWAY2",
    "Delete a layaway using a layaway number." ],  
    ["Anonymous Payment", "/cgi-bin/i_toolshed.pl", "LAYAWAYANON",
    "Find & correct an anonymous payment on a layaway." ],      
 
    ["Pending Transactions", "/cgi-bin/i_toolshed.pl", "PENDINGTXN",
    "Show Pending Transactions." ],    
    ["Assign Pending Transactions", "/cgi-bin/i_toolshed.pl", "PENDINGTXN_ASSIGN",
    "Assign a Pending Transaction to an Offsite." ],      
    
    
    );      

    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    
    StandardFooter(); 
} elsif ($report eq "LAYAWAY2") {
    print $q->h3("Layaway Delete Tool"); 
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"LAYAWAYDEL"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Enter the Layaway Number to delete:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"layawaynum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";    
    StandardFooter(); 
} elsif ($report eq "LAYAWAYANON") {
    print $q->h3("Layaway Anonymous Payment Report");     
    my %bad_payment_hash;
    my $transnum;
    my $ptdnum;
    my $txnnum;
    my $itemdatetime;  
    my $customer;
    my $warns=0;
    my @ptdnum_list;
    # We have seen instances where layaway payments are getting made "anonymously".  That is, that the cashier number is unknown.  This breaks things preventing future access to the layaway.
    $query=dequote(<< "    ENDQUERY");    
    select 
        
        transnum,
        ptdnum,
        txnnum,
        cashiernum,
        salesprsnid,
        itemdatetime,
        custname
    from 
        ptd_admin
    where
        ptdtxntype = 2
    and 
        (cashiernum = 0 or salesprsnid = 0)
        ;
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        print "Anonymous Payments in ptd_admin<br />";
        my @links = ( "$scriptname?report=LAYAWAYANON2&table=ptd_admin&layaway=$ptdnum&transnum=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
    } else {
        print "No anonymous payments found in ptd_admin.<br />";        
    }
    # Check txn_pos_transactions
    $query=dequote(<< "    ENDQUERY");    
    select 
        
        transnum,
 
        txnnum,
        cashiernum, 
        businessdate
        
    from 
        txn_pos_transactions
    where
        transnum in (select transnum from ptd_admin) 
    and 
        (cashiernum = 0  )
        ;
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        print "Anonymous Payments in txn_pos_transactions table.<br />";
        my @links = ( "$scriptname?report=LAYAWAYANON2&table=txn_pos_transactions&layaway=$ptdnum&transnum=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
    } else {
        print "No anonymous payments found in txn_pos_transactions.<br />";        
    }
    # txn_layaway_admin
    $query=dequote(<< "    ENDQUERY");    
    select 
        
        transnum,
 
        txnnum,
        salesprsnid, 
        itemdatetime,
        lwaynum,
        custname
        
    from 
        txn_layaway_admin
    where
 
        (salesprsnid = 0  )
        ;
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        print "Anonymous Payments in txn_layaway_admin table.<br />";
        my @links = ( "$scriptname?report=LAYAWAYANON2&table=txn_layaway_admin&layaway=$ptdnum&transnum=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
    } else {
        print "No anonymous payments found in txn_layaway_admin.<br />";        
    }
    # txn_layaway_total
    $query=dequote(<< "    ENDQUERY");    
    select 
        
        transnum,
 
        txnnum,
        salesprsnid, 
        itemdatetime,
        lwaynum,
        lwaytxntype
        
    from 
        txn_layaway_total
    where
 
        (salesprsnid = 0  )
        ;
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        print "Anonymous Payments in txn_layaway_total table.<br />";
        my @links = ( "$scriptname?report=LAYAWAYANON2&table=txn_layaway_total&layaway=$ptdnum&transnum=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
    } else {
        print "No anonymous payments found in txn_layaway_total.<br />";        
    }    
    StandardFooter(); 
} elsif ($report eq "LAYAWAYANON2") {
    print $q->h3("Layaway Anonymous Payment Report");    
    my $transnum= ($q->param("transnum"))[0]; 
    my $table= ($q->param("table"))[0];     
    my $ptdnum;
    my $anonymous_date;
    
    if (($table eq "txn_pos_transactions") || ($table eq "txn_layaway_admin") || ($table eq "txn_layaway_total")) {    
        # Show the cashier associated with the transaction just prior to this and just after this
        my $before=$transnum--;
        my $after=$transnum++;
        $query=dequote(<< "        ENDQUERY");    
        select 
            cashiernum,
            transnum,
            txnnum,            
            businessdate
        from 
            txn_pos_transactions
        where
            transnum in ('$before','$after')
        order by transnum

        ENDQUERY
        
        my $tbl_ref = execQuery_arrayref($dbh, $query); 
         
        if (recordCount($tbl_ref)) {   
            print "Transactions before and after this anonymous one.<br />";
            print "Select the cashier number you wish to use to correct the anonymous entry in the $table table.<br />";
            my @links = ( "$scriptname?report=LAYAWAYANON3&table=$table&ptdnum=$ptdnum&transnum=$transnum&cashier=".'$_' ); 
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');    
 
            print $q->p("Or, enter the cashier number you wish to associate with this transaction below:");
            print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"LAYAWAYANON3"});
            print $q->input({-type=>"hidden", -name=>"table", -value=>"$table"});
            print $q->input({-type=>"hidden", -name=>"ptdnum", -value=>"$ptdnum"});
            print $q->input({-type=>"hidden", -name=>"transnum", -value=>"$transnum"});
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Cashier:"),
                $q->td({-class=>'tabledatanb', -align=>'left'},
                $q->input({-type=>'number', -name=>"cashier", -size=>"4"}),
                                 )
                          );
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                $q->td({-class=>'tabledatanb', -align=>'left'}),
                $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                          );
            
            print $q->end_table(),"\n";

            print $q->end_form(), "\n";             
        } else {
            print "No payments found for layaway $ptdnum.<br />";        
        }        
    }
    if ($table eq "ptd_admin") {
        # Show all of the payment transactions for this layaway so get the layaway first
        $query=dequote(<< "        ENDQUERY");    
        select 
            ptdnum,
            itemdatetime        
        from 
            ptd_admin
        where
            ptdtxntype = 2
        and 
            (cashiernum = 0 or salesprsnid = 0)
        and 
            transnum = '$transnum'
        ENDQUERY
        my $sth = $dbh->prepare($query);        
        $sth->execute();                
        while (my @tmp=$sth->fetchrow_array()) {              
            $ptdnum = $tmp[0];
            $anonymous_date=substr($tmp[1],0,10);
        }
        # Now, get all of the payments for this ptdnum
        my %payment_hash;
        my $cashier=0;
        $query=dequote(<< "        ENDQUERY");    
        select 
            transnum,
            ptdnum,
            txnnum,
            cashiernum,
            itemdatetime,
            custname     
        from 
            ptd_admin
        where
            ptdtxntype = 2
        and 
            ptdnum = '$ptdnum'
        and
            cashiernum > 0
        ENDQUERY
        $sth = $dbh->prepare($query);        
        $sth->execute();                
        while (my @tmp=$sth->fetchrow_array()) {              
            my $date = substr($tmp[4],0,10);        
            if ($date eq $anonymous_date) {
                # This payment occured on the same date as the anonymouse date
                # This is the cashier to use.
                $cashier=$tmp[3];            
            }        
        }
     
        if ($cashier) {
            # If we have a cashier, we can fix the anonymous payment transaction
            $query=" 
                update 
                    ptd_admin 
                set 
                    cashiernum = $cashier, 
                    salesprsnid = $cashier 
                where 
                    ptdnum = $ptdnum 
                and 
                    transnum = $transnum";
            if ($dbh->do($query)) {
                print $q->p("Changed cashier and saleprsnid to $cashier for anonymous payment transaction for layaway $ptdnum");                   
            } else {
                ElderLog($logfile,"Error trying to correct cashier for anonymous payment for layaway $ptdnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }  
        } else {
            # If we don't have a cashier, show all of the payment transactions for the layaway and give the opportunity to pick a cashier
            $query=dequote(<< "            ENDQUERY");    
            select 
                cashiernum,
                transnum,
                ptdnum,
                txnnum,            
                itemdatetime,
                custname     
            from 
                ptd_admin
            where
                ptdtxntype = 2
            and 
                ptdnum = '$ptdnum'
            and
                cashiernum > 0
            ENDQUERY
            
            my $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) {            
                print "Payments for layaway $ptdnum<br />";
                print "Select the cashier number you wish to use to correct the anonymous payment<br />";
                my @links = ( "$scriptname?report=LAYAWAYANON3&table=$table&ptdnum=$ptdnum&transnum=$transnum&cashier=".'$_' ); 
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
            } else {
                # Show the cashier associated with the transaction just prior to this and just after this
                my $before=$transnum--;
                my $after=$transnum++;
                $query=dequote(<< "                ENDQUERY");    
                select 
                    cashiernum,
                    transnum,
     
                    txnnum,            
                    businessdate
        
                from 
                    txn_pos_transactions
                where
                    transnum in ('$before','$after')
                order by transnum
     
                ENDQUERY
                
                my $tbl_ref = execQuery_arrayref($dbh, $query); 
                if (recordCount($tbl_ref)) {   
                    print "Transactions before and after this anonymous one.<br />";
                    print "Select the cashier number you wish to use to correct the anonymous payment<br />";
                    my @links = ( "$scriptname?report=LAYAWAYANON3&table=$table&ptdnum=$ptdnum&transnum=$transnum&cashier=".'$_' ); 
                    TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');   
                  
                } else {
                    print "No payments found for layaway $ptdnum.<br />";        
                }
                
            }        
        }
    }
    StandardFooter();   
} elsif ($report eq "LAYAWAYANON3") {
    print $q->h3("Layaway Anonymous Payment Report");    
    my $ptdnum= ($q->param("ptdnum"))[0]; 
    my $table= ($q->param("table"))[0]; 
    my $cashier= ($q->param("cashier"))[0]; 
    my $transnum= ($q->param("transnum"))[0]; 
    # Update the transaction with the cashier specified.
  
    if ($cashier) {
        if ($table eq "ptd_admin") {
            # If we have a cashier, we can fix the anonymous payment transaction
            $query=" 
            update 
                ptd_admin 
            set 
                cashiernum = $cashier, 
                salesprsnid = $cashier 
            where 
                ptdnum = $ptdnum 
            and 
                transnum = $transnum";
            if ($dbh->do($query)) {
                print $q->p("Changed cashier and salesprsnid to $cashier for anonymous payment transaction for layaway $ptdnum");                   
            } else {
                ElderLog($logfile,"Error trying to correct cashier for anonymous payment for layaway $ptdnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }  
        
        } elsif ($table eq "txn_pos_transactions") {
            $query=" 
            update 
                txn_pos_transactions 
            set 
                cashiernum = $cashier              
            where 
                transnum = $transnum
            and
                cashiernum = 0
                ";
                
            if ($dbh->do($query)) {
                print $q->p("Changed cashier and salesprsnid to $cashier for anonymous payment transaction for transnum $transnum");                   
            } else {
                ElderLog($logfile,"Error trying to correct cashier for anonymous payment for transnum $transnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }          
        } elsif ($table eq "txn_layaway_admin") {
            $query=" 
            update 
                txn_layaway_admin 
            set 
                salesprsnid = $cashier              
            where 
                transnum = $transnum
            and
                salesprsnid = 0
                ";
                
            if ($dbh->do($query)) {
                print $q->p("Changed  salesprsnid to $cashier for anonymous payment transaction for transnum $transnum");                   
            } else {
                ElderLog($logfile,"Error trying to correct cashier for anonymous payment for transnum $transnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }   
        } elsif ($table eq "txn_layaway_total") {
            $query=" 
            update 
                txn_layaway_total 
            set 
                salesprsnid = $cashier              
            where 
                transnum = $transnum
            and
                salesprsnid = 0
                ";
                
            if ($dbh->do($query)) {
                print $q->p("Changed  salesprsnid to $cashier for anonymous payment transaction for transnum $transnum");                   
            } else {
                ElderLog($logfile,"Error trying to correct cashier for anonymous payment for transnum $transnum");
                print "Database Update Error"."<br />\n";
                print "Driver=ODBC"."<br />\n";
                print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                print "query=".$query."<br />\n";
                print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
                print "errstr=".$dbh->errstr ."<br />\n";
                StandardFooter(); 
                exit;
            }              
        } else {
            print $q->b("Error: $table is not supported");
        }
    } else {
        print $q->p("Error: No cashier number obtained.  Cannot correct the anonymous layaway payment.");
    }
    StandardFooter();
} elsif ($report eq "LAYAWAYDEL") {
    print $q->h3("Layaway Delete Tool");  
    my $layawaynum= ($q->param("layawaynum"))[0]; 
	print $q->h3("Deleting layaway number $layawaynum");
    ElderLog($logfile, "Call to delete layaway $layawaynum" );  
	$query = "select * from ptd_summary where ptdnum='$layawaynum'";
	$sth=$dbh->prepare($query);	
    $sth->execute();
   
    while (my @tmp=$sth->fetchrow_array()) {      
        ElderLog($logfile,"@tmp");    
    }
    
	$query = "delete from ptd_summary where ptdnum='$layawaynum'";
	$sth=$dbh->prepare($query);	
    
	if ($sth) {
		$sth->execute();
        print $q->h2("DONE");
        ElderLog($logfile,"Layaway $layawaynum deleted");  
	} else {
		print $q->p("Error deleting layaway $layawaynum!");
        ElderLog($logfile,"Error deleting Layaway $layawaynum");  
	}    
    StandardFooter(); 
} elsif ($report eq "LAYAWAYDET") {
    print $q->h3("Layaway Details Report"); 
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"LAYAWAYDET2"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Enter the Layaway Number get details on:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"layawaynum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	
	print $q->end_table(),"\n";

	print $q->end_form(), "\n";    
    StandardFooter();  
} elsif ($report eq "LAYAWAYDET2") {
    print $q->h3("Layaway Detail Report");  
    my $layawaynum= ($q->param("layawaynum"))[0]; 
	my $transnums; 
    my $ptdid;
    my $ptdnum;
    
    my $custnum;
    my $storenum;
    my @transnum_list;
    my @found_tables;
    my @not_found_tables;
    my @table_list=(
        "ptd_admin",
        "ptd_authorization", 
        
        "ptd_employee_sale",
        "ptd_merch_entry",
        "ptd_method_of_payment",
        "ptd_non_merch_sale",
        "ptd_profile_prompt_response",
        
        "ptd_total",
        "ptd_transaction_discount",
        "ptd_transaction_total",
        "ptd_user_defined_txn",
        "ptd_user_defined_txn_total",
       
        "txn_merchandise_sale",
        "txn_pos_transactions"
    );
    my @table_list2=(
        "Layaway",
        "txn_layaway_admin",
        "Txn_Layaway_Total",                
    );
    my @table_list3=(
        "ptd_summary",
        "ptd_pymt_schedule",              
    );
    my @table_list4=(
        "ptd_customer_admin",            
    );    
    my @stored_procedures=(
        "xps_get_ptd_total",
        "xps_get_ptd_trans_total",
    );
    my @stored_procedures1=(
        "ssp_select_layaway",        
    );    
    # Get the Transnum
	$query = "select transnum from txn_layaway_admin where lwaynum = '$layawaynum'";
    #print $q->pre("$query");
	$sth=$dbh->prepare($query);	
    $sth->execute();   
    my $sep="";
    while (my @tmp=$sth->fetchrow_array()) {      
        $transnums.="$sep\'$tmp[0]\'";
        $sep=",";
    }
    if ($transnums) {
        # Get the ptdid and ptdnum
        $query = "select ptdid,ptdnum,custnum,storenum,transnum from ptd_admin where transnum in ($transnums)"; 
        #print $q->pre("$query");    
        $sth=$dbh->prepare($query);	
        $sth->execute();   
        $sep="";
        while (my @tmp=$sth->fetchrow_array()) {      
            $ptdid.="$sep\'$tmp[0]\'";
            $ptdnum.="$sep\'$tmp[1]\'";
            $custnum.="$sep\'$tmp[2]\'";
            $storenum=$tmp[3];        
            push(@transnum_list,$tmp[4]);
            $sep=",";
        }
        
        foreach my $table (@table_list) {
            $query="select * from $table where transnum in ($transnums)";           
            my $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) {   
                print "records found in $table<br />";
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');
                push(@found_tables,$table);
            } else {
                print "No records found in $table<br />";
                push(@not_found_tables,$table);
            }
            print "<br />";
        }
        foreach my $table (@table_list2) {
            $query="select * from $table where lwaynum in ($layawaynum)";           
            my $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) { 
                print "records found in $table<br />";        
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');
                push(@found_tables,$table);
            } else {
                print "No records found in $table<br />";
                push(@not_found_tables,$table);
            }
            print "<br />";
        }
        # Display ptd_pymt_schedule
        foreach my $table (@table_list3) {
            $query="select * from $table where ptdid in ($ptdid) and ptdnum in ($ptdnum)";         
            my $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) { 
                print "records found in $table<br />";        
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');
                push(@found_tables,$table);
            } else {
                print "No records found in $table<br />";
                push(@not_found_tables,$table);
            }
        }
        foreach my $table (@table_list4) {
            $query="select * from $table where custnum in ($custnum)";         
            my $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) { 
                print "records found in $table<br />";        
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');
                push(@found_tables,$table);
            } else {
                print "No records found in $table<br />";
                push(@not_found_tables,$table);
            }
        }    
        foreach my $procedure (@stored_procedures) {
            foreach my $transnum (@transnum_list) {        
                $query="exec $procedure $storenum, $transnum";         
                my $tbl_ref = execQuery_arrayref($dbh, $query); 
                if (recordCount($tbl_ref)) { 
                    print "records found with $procedure for transnum $transnum <br />";        
                    TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');
                    
                } else {
                    print "No records found with $procedure for transnum $transnum <br />";
                    
                }
            }
        }  
        foreach my $procedure (@stored_procedures1) {       
            $query="exec $procedure $layawaynum";         
            my $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) { 
                print "records found with $procedure for layaway $layawaynum <br />";        
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');
                
            } else {
                print "No records found with $procedure for layaway $layawaynum <br />";
                
            }
        }       
        print "<br />";    

        if ($#found_tables > -1) {
            my $count=$#found_tables;
            $count++;
            print "Summary for layaway number $layawaynum:<br /><br />";
            print "Found records in $count tables:<br />";
            foreach my $table (@found_tables) {
                print "--> $table<br />";
            }
            print "<br />";
            $count=$#not_found_tables;
            $count++;        
            print "Found no records in $count tables:<br />";
            foreach my $table (@not_found_tables) {
                print "--> $table<br />";
            }
        } else {
            print "Found no records in any tables for $layawaynum<br />";
        }
    } else {
        print $q->p("Failed to determine transnum for layaway $layawaynum");
    }
    
    StandardFooter();  
    
} elsif ($report eq "PENDINGTXN") {
    print $q->h3("Pending Transaction Report");   
    my $table_count=0;
    my @table_list;
    # Get a list of all of the tables that have the specified field/column. 
    my $query = "
        select 
            table_name 
        from 
            SYSTABLE 
        where 
            table_name like 'ptd%';            
    ";
    
    my $sth = $dbh->prepare($query);        
    $sth->execute();        
    while (my @tmp=$sth->fetchrow_array()) {         
        $table_count++;
        push(@table_list,$tmp[0]);
    }
    foreach my $table (@table_list) {
        $query=dequote(<< "        ENDQUERY");    
        select         
            *
        from 
            $table
        
            ;
        ENDQUERY
        
        my $tbl_ref = execQuery_arrayref($dbh, $query); 
        if (recordCount($tbl_ref)) {   
            print "Pending Transactions in $table<br />";
            #my @links = ( "$scriptname?report=LAYAWAYANON2&table=ptd_admin&layaway=$ptdnum&transnum=".'$_' ); 
            #TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
            TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', '');        
        } else {
            print "No pending transactions found in $table.<br />";        
        }
    }
    
    StandardFooter(); 
} elsif ($report eq "PENDINGTXN_ASSIGN") {
    print $q->h3("Assign Transaction to an Offsite");   
    my $ptId;
    
    $query=dequote(<< "    ENDQUERY");    
    select         
        updatetxnnum,*
    from 
        ptd_summary

    
        ;
    ENDQUERY
    
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        print "Pending Transactions in ptd_summary<br />";
        my @links = ( "$scriptname?report=PENDINGTXN_ASSIGN2&layaway=".'$_' ); 
        #TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');  
        print $q->p("Select the pending transaction to assign.");
    } else {
        print "No pending transactions found in ptd_summary.<br />";        
    }
    
    StandardFooter();   
} elsif ($report eq "PENDINGTXN_ASSIGN2") {
    print $q->h3("Assign Transaction to an Offsite"); 
 
    my $ptId= ($q->param("layaway"))[0]; 
    unless ($ptId)  {
        print $q->p("Error: Lost track of the pending transaction number");
        StandardFooter();
        exit;
    }    
    # show the customer list
    
    $query=dequote(<< "    ENDQUERY");    
    select         
        custnum,
        lastname,
        firstname
    from 
        pa_customer
    where
        lastname like 'offsite'

    
        ;
    ENDQUERY
    
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {   
        print "Pending Transactions in ptd_summary<br />";
        my @links = ( "$scriptname?report=PENDINGTXN_ASSIGN3&layaway=$ptId&custnum=".'$_' ); 
        #TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');        
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, '');  
        print $q->p("Select the customer you wish to assign pending transaction #$ptId to.");
    } else {
        print "No Offsite customer configured.<br />";        
    }
    
    StandardFooter();   
} elsif ($report eq "PENDINGTXN_ASSIGN3") {
    print $q->h3("Assign Transaction to an Offsite"); 
 
    my $ptId= ($q->param("layaway"))[0]; 
    my $custnum= ($q->param("custnum"))[0]; 
    my $custname;
    unless ($ptId)  {
        print $q->p("Error: Lost track of the pending transaction number");
        StandardFooter();
        exit;
    }
 
    unless ($custnum)  {
        print $q->p("Error: Failed to get the customer number");
        StandardFooter();
        exit;
    }    
    
    # update 
    
    $query=dequote(<< "    ENDQUERY");  
    
    select                 
        lastname,
        firstname
    from 
        pa_customer
    where
        custnum like '$custnum'

    
        ;
    ENDQUERY
    my $sth = $dbh->prepare($query);        
    $sth->execute();    
 
    while (my @tmp=$sth->fetchrow_array()) { 
        my $last=$tmp[0];
        my $first=$tmp[1];
        $custname="$last".", "."$first";
    }
    $query=dequote(<< "    ENDQUERY");  
    update ptd_summary
        set custnum = $custnum,
        custname = '$custname'
    where
        updatetxnnum = $ptId
     
        ;
    ENDQUERY
    
    if ($dbh->do($query)) {
        print $q->p("Successfully assigned Pending Transaction #$ptId to $custname in ptd_summary");
    } else {
        print $q->p("Error attempting to assign pending transaction #$ptId in ptd_summary");
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n"; 
        StandardFooter();         
        exit;
    }   
    $query=dequote(<< "    ENDQUERY");  
    update ptd_admin
        set custnum = $custnum,
        custname = '$custname'
    where
        transnum = $ptId
     
        ;
    ENDQUERY
    
    if ($dbh->do($query)) {
        print $q->p("Successfully assigned Pending Transaction #$ptId to $custname in ptd_admin");
    } else {
        print $q->p("Error attempting to assign pending transaction #$ptId in ptd_admin");
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n";            
    }       
           
    StandardFooter();        
} elsif ($report eq "PROMOTOOLS") {
  	print $q->h1("Promotion & MixMatch Tools.");    
    my @reportlist = (    
        ["Promotion View", "/cgi-bin/i_toolshed.pl", "PROMOVIEW",
          "See what SKUs are on promotion at this time"
        ], 
        ["Promotion View & Validation", "/cgi-bin/i_toolshed.pl", "PROMOVIEW2",
          "See what SKUs are on promotion at this time and run a validation check (database intensive)"
        ],           
        ["Promotion Check", "/cgi-bin/i_toolshed.pl", "PROMOCHECK",
          "Check that a specific SKU appears to be currently on a promotion."
        ],
        ["Promotion Sales Report", "/cgi-bin/i_toolshed.pl", "PROMOSALESQRY",
          "Report on SKUs sold and what promotion they sold under."
        ],  
        ["Promo Refresh Tool", "/cgi-bin/i_toolshed.pl", "PROMOTOUCH",
          "Tool to refresh current promotions."
        ],   
        ["SKU Promo Flush Tool", "/cgi-bin/i_toolshed.pl", "PROMOFLUSH",
          "Tool to remove promo pricing and any other promo settings from a SKU."
        ],          
        ["MixMatch Report", "/cgi-bin/i_toolshed.pl", "MIXMATCHRPT",
          "Show what MixMatch discounts are configured."
        ],  
        ["MixMatch Tool - Add", "/cgi-bin/i_toolshed.pl", "MIXMATCHADD",
          "Tool to add SKUs to an existing MixMatch Discount."
        ],  
      
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }        
    StandardFooter();   
} elsif ($report eq "MIXMATCHRPT") {
  	print $q->h1("Mix Match Report."); 
    my $sqlany_dbh = openODBCDriver($dsn);
    
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum,
        mixMatchDesc,
        altPctOff1 as Discount,
        startDate,
        stopDate,
        mmDiscDesc as Name
    from
        mix_match
    where 
        startDate > CURRENT DATE 

    
    ENDQUERY
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
 
    if (recordCount($tbl_ref)) { 
        print $q->h2("Future Mix Match Discounts");
        my @links = ( "$scriptname?report=SHOWMMDTL&mixMatchNum=\$_" );
        printResult_arrayref2($tbl_ref, \@links ); 
    }  
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum,
        mixMatchDesc,
        altPctOff1 as Discount,
        startDate,
        stopDate,
        mmDiscDesc as Name
    from
        mix_match
    where 
        (startDate < CURRENT DATE    
        or
        startDate = CURRENT DATE)         
    and
        stopDate > CURRENT DATE
    
    ENDQUERY
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
 
    if (recordCount($tbl_ref)) { 
        print $q->h2("Current Mix Match Discounts");
        my @links = ( "$scriptname?report=SHOWMMDTL&mixMatchNum=\$_" );
        printResult_arrayref2($tbl_ref, \@links );         
    } else {
        print $q->h3("No Current Mix Match Discounts");
    }    
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum,
        mixMatchDesc,
        altPctOff1 as Discount,
        startDate,
        stopDate,
        mmDiscDesc as Name
    from
        mix_match
    where 
        stopDate < CURRENT DATE        
    
    ENDQUERY
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
 
    if (recordCount($tbl_ref)) { 
        print $q->h2("Non-Current Mix Match Discounts");
        my @links = ( "$scriptname?report=SHOWMMDTL&mixMatchNum=\$_" );
        printResult_arrayref2($tbl_ref, \@links ); 
    } else {
        print $q->h3("No Non-Current Mix Match Discounts");
    }       
    $sqlany_dbh->disconnect;
    StandardFooter();   
} elsif ($report eq "MIXMATCHADD") {
  	print $q->h1("Mix Match Tool - Add SKUs."); 
    my $sqlany_dbh = openODBCDriver($dsn);
    my @header=("mixMatchNum","Desc","Discount","StartDate","StopDate","Name");
    my @array_to_print=\@header;
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum,
        mixMatchDesc,
        altPctOff,
        altPctOff1,
        altPctOff2,
        altPctOff3,
        altPctOff4,
        altPctOff5,
        altPctOff6,
        altPctOff7,
        altPctOff8,                   
        startDate,
        stopDate,
        mmDiscDesc as Name
    from
        mix_match
    where 
        startDate > CURRENT DATE 
    
    ENDQUERY
    my $sth = $dbh->prepare($query);        
    $sth->execute();    
 
    while (my @tmp=$sth->fetchrow_array()) {         
        my $mixMatchNum=$tmp[0];
        my $mixMatchDesc=$tmp[1];
        my $altPctOff=$tmp[2];
        my $altPctOff1=$tmp[3];
        my $altPctOff2=$tmp[4];
        my $altPctOff3=$tmp[5];
        my $altPctOff4=$tmp[6];
        my $altPctOff5=$tmp[7];
        my $altPctOff6=$tmp[8];
        my $altPctOff7=$tmp[9];
        my $altPctOff8=$tmp[10]; 
        my $discount;
        if ($altPctOff) {
            $discount=$altPctOff;
        }
        if ($altPctOff1) {
            $discount=$altPctOff1;
        }
        if ($altPctOff2) {
            $discount=$altPctOff2;
        }
        if ($altPctOff3) {
            $discount=$altPctOff3;
        }
        if ($altPctOff4) {
            $discount=$altPctOff4;
        }        
        my $startDate=$tmp[11];
        my $stopDate=$tmp[12];
        my $name=$tmp[13];
        my @array=("$mixMatchNum","$mixMatchDesc","$discount","$startDate","$stopDate","$name");
        push(@array_to_print,\@array);
    }
    my $tbl_ref=\@array_to_print;
    if (recordCount($tbl_ref)) { 
        print $q->h2("Future Mix Match Discounts");
        my @links = ( "$scriptname?report=MIXMATCHADD2&mixMatchNum=\$_" );
        print $q->p("Select the MixMatchID you wish to add SKUs to");
        printResult_arrayref2($tbl_ref, \@links ); 
    }  
    @array_to_print=\@header;    
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum,
        mixMatchDesc,
        altPctOff,
        altPctOff1,
        altPctOff2,
        altPctOff3,
        altPctOff4,
        altPctOff5,
        altPctOff6,
        altPctOff7,
        altPctOff8,                   
        startDate,
        stopDate,
        mmDiscDesc as Name
    from
        mix_match
    where 
        (startDate < CURRENT DATE    
        or
        startDate = CURRENT DATE)         
    and
        stopDate > CURRENT DATE
    
    ENDQUERY
    $sth = $dbh->prepare($query);        
    $sth->execute();    
 
    while (my @tmp=$sth->fetchrow_array()) {         
        my $mixMatchNum=$tmp[0];
        my $mixMatchDesc=$tmp[1];
        my $altPctOff=$tmp[2];
        my $altPctOff1=$tmp[3];
        my $altPctOff2=$tmp[4];
        my $altPctOff3=$tmp[5];
        my $altPctOff4=$tmp[6];
        my $altPctOff5=$tmp[7];
        my $altPctOff6=$tmp[8];
        my $altPctOff7=$tmp[9];
        my $altPctOff8=$tmp[10]; 
        my $discount;
        if ($altPctOff) {
            $discount=$altPctOff;
        }
        if ($altPctOff1) {
            $discount=$altPctOff1;
        }
        if ($altPctOff2) {
            $discount=$altPctOff2;
        }
        if ($altPctOff3) {
            $discount=$altPctOff3;
        }
        if ($altPctOff4) {
            $discount=$altPctOff4;
        }        
        my $startDate=$tmp[11];
        my $stopDate=$tmp[12];
        my $name=$tmp[13];
        my @array=("$mixMatchNum","$mixMatchDesc","$discount","$startDate","$stopDate","$name");
        push(@array_to_print,\@array);
    }
    $tbl_ref=\@array_to_print;
 
    if (recordCount($tbl_ref)) { 
        print $q->h2("Current Mix Match Discounts");
        my @links = ( "$scriptname?report=MIXMATCHADD2&mixMatchNum=\$_" );
        print $q->p("Select the MixMatchID you wish to add SKUs to");
        printResult_arrayref2($tbl_ref, \@links );         
    } else {
        print $q->h3("No Current Mix Match Discounts");
    }    
 
    $sqlany_dbh->disconnect;
    StandardFooter();    
} elsif ($report eq "MIXMATCHADD2") {
  	print $q->h1("Mix Match Tool - Add SKUs."); 
    my $mixMatchNum   = trim(($q->param("mixMatchNum"))[0]);      
    my $MixMatchName;
    my $sqlany_dbh = openODBCDriver($dsn);  
    $query = dequote(<< "    ENDQUERY");
    select 
        mmDiscDesc as Name
    from
        mix_match
    where 
        mixMatchNum = '$mixMatchNum'     
    ENDQUERY
    
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
 
    if (recordCount($tbl_ref)) { 
        
        for my $datarows (1 .. $#$tbl_ref) {           
            my $thisrow = @$tbl_ref[$datarows];
            $MixMatchName = trim($$thisrow[0]);                       
        }       
    }
    print $q->p("Select Method to add SKUs to MixMatchId $mixMatchNum - $MixMatchName");
    print $q->p("Enter SKU selection separated by a coma. ");
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>$scriptname, -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHADD4"});
    print $q->input({-type=>"hidden", -name=>"MixMatchName", -value=>"$MixMatchName"});
    print $q->input({-type=>"hidden", -name=>"mixMatchNum", -value=>"$mixMatchNum"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("SKU Selection")));
    print $q->Tr($q->td({-class=>'tabledatanb'}, $q->input({-accesskey=>'8', -type=>'submit', -value=>" * "})),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "\&nbsp;Enter SKU(s):\&nbsp"),
        $q->td({-class=>'tabledatanb', -align=>'right'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"SKU", -size=>"24"})));
 
    print $q->end_form(), "\n";
   
    print $q->end_table(),"\n";
    print $q->p();

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>$scriptname, -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHADD3"});
    print $q->input({-type=>"hidden", -name=>"MixMatchName", -value=>"$MixMatchName"});
    print $q->input({-type=>"hidden", -name=>"mixMatchNum", -value=>"$mixMatchNum"});    
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("SKU Selection Criteria")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "<u>K</u>eyword:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'K', -type=>'text', -name=>"keyword", -size=>"12"}))
              );

    $query = "select VendorId, VendorName from Vendor";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);

    my $vendor_options = $q->option({-value=>'' -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $VendorId = trim($$thisrow[0]);
     my $VendorName = trim($$thisrow[1]);
     $vendor_options .= $q->option({-value=>$VendorId}, "$VendorId - $VendorName");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Ven<u>d</u>or:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-accesskey=>'D', -name=>"VendorId", -size=>"1"}), $vendor_options)
              );

    my $country_options = $q->option({-value=>'' -selected=>undef});
    foreach my $key (sort {$country_hash{$a} cmp $country_hash{$b}} (keys(%country_hash))) {
     my $value = $country_hash{$key};
     $country_options .= $q->option({-value=>$key}, "$value ($key)");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "<u>C</u>ountry:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-accesskey=>'C', -name=>"country", -size=>"1"}), $country_options)
              );

    $query = "select deptnum, deptdescription from department order by deptnum";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $dept_options = $q->option({-value=>'' -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $deptnum   = trim($$thisrow[0]);
     my $deptdesc = trim($$thisrow[1]);

     substr($deptdesc,4,0) = " &nbsp;:&nbsp;" unless ($deptnum == 9999);
     if (substr($deptdesc,2,2) eq "00") { substr($deptdesc,2,2) = ("&nbsp;" x 5); }
     
     $dept_options .= $q->option({-value=>$deptnum }, "$deptdesc");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Department:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"dept", -size=>"1"}), $dept_options)
              );


    $query = "select codeid, codename, description from user_flag_code where userflagnum =1 order by codename";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $season_options = $q->option({-value=>'' -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $codeid   = trim($$thisrow[0]);
     my $codename = trim($$thisrow[1]);
     my $codedesc = trim(substr($$thisrow[2],0,15));

     $season_options .= $q->option({-value=>$codeid }, "$codename: $codedesc");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Seasonality UF1:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"seasonality", -size=>"1"}), $season_options)
              );


    $query = "select codeid, codename, description from user_flag_code where userflagnum =2 order by codename";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $vmcode_options = $q->option({-value=>'', -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $codeid   = trim($$thisrow[0]);
     my $codename = trim($$thisrow[1]);
     #my $codedesc = trim(substr($$thisrow[2],0,15));
     my $codedesc = trim($$thisrow[2]);
     $vmcode_options .= $q->option({-value=>$codeid}, "$codename: $codedesc");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "V<u>M</u>Code UF2:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-accesskey=>'M', -name=>"vmcode", -size=>"1"}), $vmcode_options)
              );

    $query = "select codeid, codename from user_flag_code where userflagnum =3 order by codeid";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $AkronStatus_options = $q->option({-value=>'', -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $codeid   = trim($$thisrow[0]);
     my $codename = trim($$thisrow[1]);
     $AkronStatus_options .= $q->option({-value=>$codeid}, "$codename");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Akron Status UF3:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"AkronStatus", -size=>"1"}), $AkronStatus_options)
              );


    $query = "select codeid, codename from user_flag_code where userflagnum=4 order by codeid";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $StoreStatus_options = $q->option({-value=>'', -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $codeid   = trim($$thisrow[0]);
     my $codename = trim($$thisrow[1]);
     $StoreStatus_options .= $q->option({-value=>$codeid}, "$codename");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Status UF4:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"StoreStatus", -size=>"1"}), $StoreStatus_options)
              );

    $query = "select codeid, codename from user_flag_code where userflagnum =5 order by codename desc";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $dtnewit_options = $q->option({-value=>'', -selected=>undef});
    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $codeid   = trim($$thisrow[0]);
     my $codename = trim($$thisrow[1]);
     $dtnewit_options .= $q->option({-value=>$codeid}, "$codename");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "<u>N</u>ew Item Date UF5:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-accesskey=>'N', -name=>"dtnewit", -size=>"1"}), $dtnewit_options)
              );

    $query = "select codeid, codename, description from user_flag_code where userflagnum=6 order by codeid";
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    if (recordCount($tbl_ref) > 0) {
     my $PromoUF6_options = $q->option({-value=>'', -selected=>undef});
     for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];
        my $codeid   = trim($$thisrow[0]);
        my $codename = trim($$thisrow[1]);
        my $codedesc = trim(substr($$thisrow[2],0,15));
        
        $PromoUF6_options .= $q->option({-value=>$codeid}, "$codedesc ($codename)");
     }
     print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Promo UF6:"),
                  $q->td({-class=>'tabledatanb', -align=>'left'},
                         $q->Select({-name=>"PromoUF6", -size=>"1"}), $PromoUF6_options)
                 );
    }

 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
        );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    
    $sqlany_dbh->disconnect;
    StandardFooter();   
   
} elsif ($report eq "MIXMATCHADD3") {
  	print $q->h1("Mix Match Tool - Add SKUs."); 
    my $mixMatchNum   = trim(($q->param("mixMatchNum"))[0]);      
    my $MixMatchName;
    my $confirm = trim(($q->param("confirm"))[0]);
    my $keyword = trim(($q->param("keyword"))[0]);
    my $VendorId =trim(($q->param("VendorId"))[0]);
    my $country = trim(($q->param("country"))[0]);
    my $dept    = trim(($q->param("dept"))[0]); 
    my $seasonality = trim(($q->param("seasonality"))[0]);  # userflag 1      
    my $vmcode  = trim(($q->param("vmcode"))[0]); # userflag 2
    my $AkronStatus = trim(($q->param("AkronStatus"))[0]); # userflag 3
    my $StoreStatus = trim(($q->param("StoreStatus"))[0]); # userflag 4
    my $dtnewit = trim(($q->param("dtnewit"))[0]); # userflag 5
    my $PromoUF6 = trim(($q->param("PromoUF6"))[0]); # userflag 6
    unless ($keyword || $VendorId || $country || $dept || $seasonality || $vmcode ||
        $AkronStatus || $StoreStatus || $dtnewit || $PromoUF6
        ) {
        print "Please fill in at least one product lookup criteria. ";
        StandardFooter();
        exit;
    }   
    if ($keyword && (length($keyword) <= 2)) {
        print "Please enter a keyword longer than 2 characters. ".
           "(Otherwise there's a potential of returning lots of data.) ";
        StandardFooter();
        exit;
    }    
    my $sqlany_dbh = openODBCDriver($dsn);  
    ###
    # this section is where we build the Where clause.
    my $whereclause = '';
    my $addlFromClause = '';
    ## add this for the ExtPrice column - kdg
    my $addlColumn = '';

    if ($keyword) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "p.pludesc like '%$keyword%'";
    }

    if ($VendorId) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "p.VendorId = '$VendorId'";
    }

    if ($country) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "p.VendorId in ('1','01') and p.plunum like '$country%'";
    }

    if ($seasonality) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "uf1.codeid=$seasonality";
        $addlFromClause .= "     left outer join user_flag_code uf1 on uf1.userflagnum=1 and uf1.codeid=p.userflagnum1 ";
    }

    if ($dept) {
        $whereclause .= " and " if $whereclause;
        if (substr($dept,2,2) eq "00") {
            $whereclause .= "floor(p.deptnum/100) = ". (substr($dept,0,2)+0);
        } else {
            $whereclause .= "p.deptnum = ". $dept;
        }
    }

    if ($vmcode) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "uf2.codeid=$vmcode";
    }
    if ($AkronStatus) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "uf3.codeid=$AkronStatus";
        $addlFromClause .= "     left outer join user_flag_code uf3 on uf3.userflagnum=3 and uf3.codeid=p.userflagnum3 ";
    }
    if ($StoreStatus) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "uf4.codeid=$StoreStatus";
        $addlFromClause .= "     left outer join user_flag_code uf4 on uf4.userflagnum=4 and uf4.codeid=p.userflagnum4 ";
    }
    if ($dtnewit) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "uf5.codeid=$dtnewit";
    }

    if ($PromoUF6) {
        $whereclause .= " and " if $whereclause;
        $whereclause .= "uf6.codeid=$PromoUF6";
        $addlFromClause .= "     left outer join user_flag_code uf6 on uf6.userflagnum=6 and uf6.codeid=p.userflagnum6 ";
    }
 
 

    $query = "select p.plunum as \"SKU\", p.pludesc as \"Description\", p.VendorId as \"Vendor\", ".
           "right(string('0000',p.deptnum),4) as \"Dept\", ".
           "uf2.codename as \"VM Code\", p.retailprice as \"Price\", ic.qtycurrent as \"QOH\", ".
           "uf5.codename as \"NI Date\" ".


           ", (IF(ic.qtycurrent > 0) THEN (p.retailprice * ic.qtycurrent) ELSE ('') ENDIF) as \"ExtPrice\" ".

           "from plu p left outer join inventory_current ic on p.plunum=ic.plunum ".
           "     left outer join user_flag_code uf2 on uf2.userflagnum=2 and uf2.codeid=p.userflagnum2 ".
           "     left outer join user_flag_code uf5 on uf5.userflagnum=5 and uf5.codeid=p.userflagnum5 ".
           $addlFromClause.
           "where $whereclause ";
 
 
    my @format = ('', '',  '',
                {'align' => 'Right'}, # , 'num' => '6.0'
                '',
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'num' => '.0'},
                '',
                {'align' => 'Right', 'currency' => ''});
 

    $tbl_ref = TTV::utilities::execQuery_arrayref($sqlany_dbh, $query);

    if (recordCount($tbl_ref))  {
        if ($confirm) {
            my @sku_list;
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows]; 
                my $sku=$$thisrow[0];                  
                push(@sku_list,$sku);
                                                
            }
            enableSelection(\@sku_list,$mixMatchNum);
            process_ASC();
        } else {            
            printResult_arrayref2($tbl_ref);        
            
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>$scriptname, -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHADD3"}); 
            print $q->input({-type=>"hidden", -name=>"MixMatchName", -value=>"$MixMatchName"});
            print $q->input({-type=>"hidden", -name=>"mixMatchNum", -value=>"$mixMatchNum"});        
            print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});   
            print $q->input({-type=>"hidden", -name=>"keyword", -value=>"$keyword"});   
            print $q->input({-type=>"hidden", -name=>"VendorId", -value=>"$VendorId"}); 
            print $q->input({-type=>"hidden", -name=>"country", -value=>"$country"}); 
            print $q->input({-type=>"hidden", -name=>"dept", -value=>"$dept"}); 
            print $q->input({-type=>"hidden", -name=>"seasonality", -value=>"$seasonality"}); 
            print $q->input({-type=>"hidden", -name=>"vmcode", -value=>"$vmcode"}); 
            print $q->input({-type=>"hidden", -name=>"AkronStatus", -value=>"$AkronStatus"}); 
            print $q->input({-type=>"hidden", -name=>"StoreStatus", -value=>"$StoreStatus"}); 
            print $q->input({-type=>"hidden", -name=>"dtnewit", -value=>"$dtnewit"}); 
            print $q->input({-type=>"hidden", -name=>"PromoUF6", -value=>"$PromoUF6"});               
 
            print "<br />";
            print $q->submit({ -value=>"   Yes!   "});           
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            print $q->p("Select 'Yes!' to confirm that you wish to add these SKUs to MixMatch #$mixMatchNum $MixMatchName ");
            print $q->a("(Note: It will take up to 30 seconds to process the ASC file if you select this.)");
        }
    } else {
        print $q->b("no records found<br />\n");
    }    
    ###
    $sqlany_dbh->disconnect;
    StandardFooter();  
} elsif ($report eq "MIXMATCHADD4") {
  	print $q->h1("Mix Match Tool - Add SKUs."); 
    my $mixMatchNum   = trim(($q->param("mixMatchNum"))[0]);  
    my $MixMatchName   = trim(($q->param("MixMatchName"))[0]);     
    my $sku_list   = trim(($q->param("SKU"))[0]);  
    my $confirm   = trim(($q->param("confirm"))[0]);     
     
    my @tmp=split(/\,/,$sku_list);
    if ($#tmp) {
        if ($confirm) {
            enableSelection(\@tmp,$mixMatchNum);
            process_ASC();
        } else {            
            print $q->p("Selected SKUs to add:");
            foreach my $sku (@tmp) {
                print $q->a("$sku<br />");                
            }                    
            print $q->start_form({-action=>$scriptname, -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHADD4"}); 
            print $q->input({-type=>"hidden", -name=>"MixMatchName", -value=>"$MixMatchName"});
            print $q->input({-type=>"hidden", -name=>"mixMatchNum", -value=>"$mixMatchNum"});        
            print $q->input({-type=>"hidden", -name=>"SKU", -value=>"$sku_list"});  
            print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});                      
     
            print $q->submit({  -value=>"   Yes!   "}); 
            print $q->end_form(), "\n";
            #print $q->end_table(),"\n";
            print $q->p("Select 'Yes!' to confirm that you wish to add these SKUs to MixMatch #$mixMatchNum $MixMatchName ");
        }
        
    } else {
        print $q->p("Failed to get the specified SKUs to add");
    }

    
    StandardFooter();        
} elsif ($report eq "SHOWMMDTL") {
  	print $q->h1("Mix Match Detail Report.");  
    my %order_hash = (
        'plunum' => 'p.plunum',
        'deptnum' => 'p.deptnum',
        'pludesc' => 'pludesc',
        'retailprice' => 'p.retailprice',
        'qtycurrent' => 'ic.qtycurrent'
        );
    my $mixMatchNum   = trim(($q->param("mixMatchNum"))[0]);      
    my $sortfield = trim(($q->param("sort"))[0]);      
    unless ($sortfield) {
        $sortfield="plunum";
    }
    $sortfield=$order_hash{$sortfield};
    ## adding an order field to be ASC or DESC -kdg ###
    my $order = trim(($q->param("order"))[0]);           
    if (($order eq "") || ($order eq "DESC")) {
        ## Toggle the order
        $order = "ASC";
    } else {
        $order = "DESC";
    }    
    my $sqlany_dbh = openODBCDriver($dsn);
    # Give a tally of the SKUs in this Mix Match
    $query = dequote(<< "    ENDQUERY");      
    select    
        count(distinct d.groupnum) as "Number of Groups" 
    from
        plu p 
        join department d on p.deptnum = d.deptnum            
    where
        p.mmid1 = $mixMatchNum
    
    ENDQUERY
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
    my $groupCount=$$tbl_ref[1][0];
    
    $query = dequote(<< "    ENDQUERY");      
    select 
        count(p.plunum) as "Number of SKUs",
        count(distinct p.deptnum) as "Number of Departments",   
        $groupCount as "Number of Groups" ,
        max(p.retailprice) as "Max Price",
        min(p.retailprice) as "Min Price",
        avg(p.retailprice) as "Avg Price"
    from
        plu p         
    where
        p.mmid1 = $mixMatchNum
    
    ENDQUERY
    
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);
 
    if (recordCount($tbl_ref)) {     
        print $q->h2("Summary of Mix Match $mixMatchNum");
 
        my @format = ('', '',  '', 
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''}
                );
        
        printResult_arrayref2($tbl_ref, '', \@format ); 
    }           
    
    # Show all of the SKUs set to this Mix Match

  
    $query = dequote(<< "    ENDQUERY");
    select 
        p.plunum,
        p.deptnum,
        p.pludesc,
        p.retailprice,
        ic.qtycurrent
 
    from
        plu p
        join inventory_current ic on p.plunum = ic.plunum
    where
        p.mmid1 = $mixMatchNum        
    order by 
        $sortfield $order
    
    ENDQUERY
    $tbl_ref = execQuery_arrayref($sqlany_dbh, $query);

    my @format = ('', '', '', 
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'num' => '.0'} );
 
  
    my $newsortURL = "$scriptname?report=SHOWMMDTL&mixMatchNum=$mixMatchNum&order=$order&sort=\$_";
    my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);

    if (recordCount($tbl_ref)) { 
        print $q->h2("SKUs set for Mix Match $mixMatchNum");
        printResult_arrayref2($tbl_ref, '', \@format, \@headerlinks); 
    } else { 
        print $q->h2("No MixMatch $mixMatchNum");            
    }      
    $sqlany_dbh->disconnect;
    StandardFooter();
} elsif ($report eq "PROMOSALESQRY") {
  	print $q->h1("Sales Report.");   
    # Ask which day to do the report fo
      print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
      print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMOSALESRPT"});

      print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
      print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

      my @ltm = localtime(time());
      my $month=$ltm[4]+1;
      my $year=$ltm[5]+1900;
      my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
      my $hm = 1;
      my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
      $startdate = $enddate; # for now.

      my $calcounter = 1;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
      $calcounter++;
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );                  
                  
                  
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      
    
    StandardFooter(); 
} elsif ($report eq "PROMOSALESRPT") {
  	print $q->h1("Promotion Sales Report."); 
    my $startdate = trim(($q->param("startdate"))[0]);
    my $enddate   = trim(($q->param("enddate"))[0]);
 

    my $datewhere_ms = "";
    my $datewhere_tm = "";
    my $datewhere_db = "";
    my $datewhere_mt = "";
    my $daterange = "";

 
    if ( ($startdate) || ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not correct";
            StandardFooter();
            last SWITCH;
        }

        $datewhere_ms = " ms.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $datewhere_tm = " tm.txndatetime between '$startdate' and '$enddate 23:59:59.999' ";
        $datewhere_db = " itemdatetime between '$startdate' and '$enddate 23:59:59.999'";
        $datewhere_mt = " mt.itemdatetime between '$startdate' and '$enddate 23:59:59.999' and ";
        $daterange = " date range $startdate - $enddate";
    } else {
        print "Start date or End date is blank";
        StandardFooter();
        last SWITCH;
    }

    my $sqlany_dbh = openODBCDriver($dsn);
  
    $query = dequote(<< "    ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
               mt.storenum=vpt.storenum and
               mt.txnnum=vpt.txnnum and
               mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               pt.regnum = vpt.regnum and
               mt.refnum = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
         where
            mt.itemdatetime  > '$startdate' and
            mt.misctype = 2;  

         select
            tm.storenum, tm.transnum, tm.txnnum,
            -- I changed the source of the date - kdg
            --convert(varchar(19), tm.txndatetime,21) as TranDateTime,
            convert(varchar(19), ms.itemdatetime,21) as TranDateTime,
            tm.regnum, tm.cashiernum, 
            ms.itemnum,
            ms.skunum, ms.deptnum,
            p.pludesc, p.deptnum, p.vendorid,
            ms.qty, ms.extorigprice, ms.extregprice, ms.extundiscprice, ms.extsellprice,
            ms.promonum,
            ph.description
         from
            Txn_POS_Transactions tm
            join Txn_Merchandise_Sale ms on tm.storenum=ms.storenum and tm.transnum=ms.transnum
            left outer join plu p on ms.skunum=p.plunum
            left outer join #tmpPostVoids v on tm.storenum=v.storenum and tm.transnum=v.transnum
            left outer join Txn_Profile_Prompt_Response tpr on tm.storenum=tpr.storenum and tm.transnum=tpr.transnum and tpr.propromptid=8
            left outer join promo_header ph on ms.promonum=ph.promonum
         where
            $datewhere_tm and
            ms.txnvoidmod = 0 and
            v.transnum is null
            and ms.promonum > 0
         order by tm.storenum, tm.transnum;

         drop table #tmpPostVoids;
      end      
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($sqlany_dbh, $query); 
    if (recordCount($tbl_ref)) { 
        printResult_arrayref2($tbl_ref);
    } else { 
        print $q->b("no records found<br />\n"); 
    }    
    StandardFooter();     
} elsif ($report eq "SKUTOOLS") {
  	print $q->h1("SKU Tools & Reports.");    
    my @reportlist = (    
         ["SKUs w. Spaces Tool", "/cgi-bin/i_toolshed.pl", "SKUSPACE",
          "Find SKUs with spaces and fix them if we can."
         ],  
        
         ["No Desc SKUs Tool", "/cgi-bin/i_toolshed.pl", "SKUORPHAN",
          "Find SKUs with no description and no vendor."
         ], 
         ["SKU Check Tool", "/cgi-bin/i_toolshed.pl", "SKUCHECK",
          "Various SKU checks."
         ], 
            
         ["SKU Edit Tool", "/cgi-bin/i_toolshed.pl", "SKUTOOL",
          "Edit description, dept, or vendor id for existing SKUs."
         ],  
         ["SKU Purge Tool", "/cgi-bin/i_toolshed.pl", "SKUPURGE",
          "Remove a SKU by creating ASC files to delete it."
         ],   
         ["SKU Purge by Vendor Tool", "/cgi-bin/i_toolshed.pl", "VENDORSKUPURGE",
          "Remove all SKU's of a selected Vendor (Must have ISO's disabled and Min & Max to 0)"
         ],               
        ["SKU Activity Report", "/cgi-bin/i_toolshed.pl", "SKUACTRPT",
          "Reports sales, receiving, adjustments, etc for specific SKUs on a specified day."
        ],  
        ["SKU Activity Report II", "/cgi-bin/i_toolshed.pl", "SKUINVACTRPT",
          "This version gets data from the elderInvActivity table"
        ],    
        ["SKU Modification Report", "/cgi-bin/i_toolshed.pl", "SKUMODRPT",
          "Reports changes found in the update_audit table"
        ],             
         
        ["User Flag Tool", "/cgi-bin/i_toolshed.pl", "UFTOOL",
          "Tool to check and correct user flags in the store."
        ], 
        ["User Flag Description Tool", "/cgi-bin/i_toolshed.pl", "UFTOOLLABEL",
          "Tool to check and correct user flag descriptions in the store."
        ],         
        ["Bar Code Tool", "/cgi-bin/i_toolshed.pl", "BCTOOL",
          "Tool to check and correct bar codes for Village SKUs in the store."
        ],   
        ["NV Bar Code Tool", "/cgi-bin/i_toolshed.pl", "NVBCTOOL",
          "Tool to check and correct bar codes for Non-Village SKUs in the store."
        ],           
        ["Bar Code Settings Tool", "/cgi-bin/i_toolshed.pl", "BCTOOL2",
          "Tool to check and correct captureplu and inventorycontrolled settings for bar codes all SKUs"
        ], 
        ["Department Number Tool", "/cgi-bin/i_toolshed.pl", "DEPTTOOL",
          "Tool to check and correct departments for Villages SKUs"
        ], 
        ["NV Department Number Tool", "/cgi-bin/i_toolshed.pl", "NVDEPTTOOL",
          "Tool to check and correct departments for Non-Villages SKUs"
        ], 		
        ["Past Valuation Reports", "/cgi-bin/i_toolshed.pl", "VALUATION",
          "See what valuation was in the recent past."
        ],    
        ["PLU Update Report", "/cgi-bin/i_toolshed.pl", "UPDATEAUDIT",
          "See last 500 entries of the update_audit table."
        ],   
        ["Inventory", "/cgi-bin/i_toolshed.pl", "INV",
          "Inventory Reports."
        ],   
    );
    # Disabled test entries
=pod    
         ["SKU Number Check Tool", "/cgi-bin/i_toolshed.pl", "SKUNUMCHECK",
          "More SKU checks."
         ],     
=cut    
    
	# Some scripts are for Company Stores only
    my @company_only_scripts=("NVDEPTTOOL","NVBCTOOL");
    my $storenum = getStoreNum($dbh);  	
    my $storeStatus=$store_cc_hash{$storenum};	
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
		my $enable_script=1;
        foreach my $scrptnm (@company_only_scripts) {	
            if (($storeStatus eq "Contract") && ($scrptnm eq $reportname)) {                
				$enable_script=0;		
            }
        }
        if ($enable_script) {
			print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
		}
    }        
    StandardFooter();  
} elsif ($report eq "ORDERTOOLS") {
  	print $q->h1("Order Tools.");    
    my @reportlist = (    
        ["Reset Receiving Document", "/cgi-bin/i_toolshed.pl", "RESETRECDOC",
        "Useful if a received order needs to be set back to pending."
        ], 
        ["Reset Transfer Document", "/cgi-bin/i_toolshed.pl", "RESETXREFDOC",
        "Useful if a store transfer needs to be set back to pending."
        ], 	
        ["Reset Purchase Order", "/cgi-bin/i_toolshed.pl", "RESETPO",
        "Useful if a purchase order needs to be set back to pending."
        ],         
        ["Receive All Transfer Document", "/cgi-bin/i_toolshed.pl", "RECALLXREFDOC",
        "The 'receive all' function in store transfer is broken.  This does the job."
        ], 
        ["Rename Receiving PO", "/cgi-bin/i_toolshed.pl", "RENAMERECPO",
        "Change the PO number for a selected receiving record."
        ],         
        ["Check PO & Rec status", "/cgi-bin/i_toolshed.pl", "POSTATUS",
        "Check that status of PO matches the receiving record."
        ],    
        ["Open PO Status", "/cgi-bin/i_toolshed.pl", "LISTORDERS",
        "Show status of open Villages Orders."
        ], 
         
        ["Check Receiving Tables", "/cgi-bin/i_toolshed.pl", "CHECKREC",
        "Check that receiving table agrees with the inventory_adjustment table."
        ], 
        ["Check Transfer Tables", "/cgi-bin/i_toolshed.pl", "CHECKXFR",
        "Check that transfer table agrees with the inventory_adjustment table."
        ],  		
        ["Pending Transfers", "/cgi-bin/i_toolshed.pl", "PENDINGXFR",
        "Show the transfers currently pending."
        ],    
        ["Claim Database", "/cgi-bin/i_toolshed.pl", "CLAIMS",
        "Show the claims database records."
        ],         
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }        
    StandardFooter(); 
} elsif ($report eq "DBTOOLS") {
  	print $q->h1("Database Tools");    
    my @reportlist = (    
        ["ElderConfig Tool", "/cgi-bin/i_toolshed.pl", "ECONF",
          "A tool to check or modify the elderConfig table."
        ],     
        
        ["StoreNum Check Tool", "/cgi-bin/i_toolshed.pl", "STORENUMCHECK",
          "A tool to check the database for records with the incorrect store number."
        ],    
        ["Transnum Search", "/cgi-bin/i_toolshed.pl", "TNUMSRCH_QRY",
          "Show all tables having results for a specified transnum."
        ],        
        ["Field Search", "/cgi-bin/i_toolshed.pl", "FIELDSRCH_QRY",
          "Show all tables having a specified field."
        ],   
        ["Profile Prompt Clear", "/cgi-bin/i_toolshed.pl", "PROFILEPROMPT_QRY",
          "Tool to clear a profile prompt that was not decrypted."
        ],              
         
                
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }        
    StandardFooter();   
} elsif ($report eq "MISCTOOLS") {
  	print $q->h1("Misc Tools");    
    my @reportlist = (    
		["Reset pcAnywhere server", "/cgi-bin/i_toolshed.pl", "RESETPC",
		"Useful if you cannot connect to a store with pcAnywhere."
		], 		 
		["Reset SAF file", "/cgi-bin/i_toolshed.pl", "RESETSAF",
		"Useful if credit card authorizations are slow."
		], 
		["Restart Edna", "/cgi-bin/i_toolshed.pl", "RESTARTEDNA",
		"Set a flag so that Edna will reboot in the morning."
		], 
		["Restart Backup Attempts", "/cgi-bin/i_toolshed.pl", "RESTARTBACKUP",
		"Set the eod_complete flag so that the backup attempts will restart."
		], 	
		["Remove eod_complete flag", "/cgi-bin/i_toolshed.pl", "REMOVEEODCOMPLETE",
		"Remove the eod_complete flag.  (This is the opposite of the above.)"
		], 	
		["Set the skip download flag", "/cgi-bin/i_toolshed.pl", "SKIPDOWNLOAD",
		"Set a flag for the store to skip the download_sftp for one night."
		], 			
        ["New Customer Number", "/cgi-bin/i_toolshed.pl", "NCN",
          "What should be the next customer number for each register."
        ],               
        ["Vendor Name Adjust", "/cgi-bin/i_toolshed.pl", "VENADJ",
          "Adjust a vendor's name to match approved list."
        ],          
        ["Clockio", "/cgi-bin/i_toolshed.pl", "CLOCKIO",
          "Clock in/out tool."
        ], 
        ["Tax Tool", "/cgi-bin/i_tools_tax.pl", "DEFAULT",
          "The Tax Tool Accounting uses."
        ],    
        ["Modem Tool", "/cgi-bin/i_tools_modem.pl", "MODEM",
          "A tool to check the modem."
        ],  
        ["Monitor Tool", "/cgi-bin/i_toolshed.pl", "MONITOR",
          "A tool to run the monitor."
        ], 
        ["Backup Tool", "/cgi-bin/i_toolshed.pl", "BACKUP_TOOL",
          "A tool to run the backup."
        ], 		
        ["Inventory", "/cgi-bin/i_toolshed.pl", "INV",
          "Inventory Reports & Tools."
        ],  
        ["Clear Misc SKU", "/cgi-bin/i_toolshed.pl", "CLEAR_MISC_SKU_QUERY",
          "Remove an entry from the Misc SKU report."
        ],   
        ["Claim Database", "/cgi-bin/i_toolshed.pl", "CLAIMS",
        "Show the claims database records."
        ],      
        ["Merge Claim Databases", "/cgi-bin/i_toolshed.pl", "MERGECLAIMS",
        "If you have two claim databases that need to be merged."
        ],              
                
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }        
    StandardFooter();  
} elsif ($report eq "MISCREPORTS") {
  	print $q->h1("Misc Reports");    
    my @reportlist = (    
         ["Backup Report", "/cgi-bin/i_toolshed.pl", "RPT&reportreq=backup_report",
          "View last backup report"              
         ],
         ["POS Backup Report", "/cgi-bin/i_toolshed.pl", "RPT&reportreq=sftp_report",
          "View last posbackup report"              
         ],         
         ["Monitor Report", "/cgi-bin/i_toolshed.pl", "RPT&reportreq=monitor_report",
          "View last backup report"              
         ],    
         ["Boot Record", "/cgi-bin/i_toolshed.pl", "BOOTLOG",
          "View record of the most recent system boots."
         ],
         ["Event Log", "/cgi-bin/i_toolshed.pl", "EVENTLOG",
          "View the event log for the system."
         ],        
        
        ["Tender Report", "/cgi-bin/i_toolshed.pl", "TENDERRPT",
          "Reports what tenders were used for the sales on a specified day."
        ], 
        ["Tender Report II", "/cgi-bin/i_toolshed.pl", "TENDERRPT2",
          "More detailed tender report. (Needs work to deal with split tender and taxes)"
        ],          
        ["Detailed Store Valuation", "/cgi-bin/i_toolshed.pl", "DETAILVAL",
          "Show current SKUs, Qty, Cost, and Ext Cost."
        ],   

        ["Inventory", "/cgi-bin/i_toolshed.pl", "INV",
          "Inventory Reports."
        ],      
  
                
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }        
    StandardFooter();      
} elsif ($report eq "TNUMSRCH_QRY") { 
    print $q->h2("Transnum Search"); 
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TNUMSRCH"});  
        
    print  $q->input({ -type=>'text', -name=>"transnum", -size=>"10", -value=>" "});        
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
    print $q->p("Enter a Transnum to search for and click the 'Enter' button.");
    
    print $q->end_form(), "\n";    
    StandardFooter();    
} elsif ($report eq "TNUMSRCH") {    
    print $q->h2("Transnum Search");      
    my $transnum= ($q->param("transnum"))[0]; 
    my $table_count=0;
    # Get a list of all of the tables that use the "transnum" column and check if they have any entries where the specified transnum is found 
    my $query = "
        select 
            table_name 
        from 
            SYSTABLE 
        where 
            table_id in
            (select table_id from SYSCOLUMN where column_name = 'transnum')
    ";
    my $sth = $dbh->prepare($query);        
    $sth->execute();    
    my @table_list;
    while (my @tmp=$sth->fetchrow_array()) { 
        push(@table_list,$tmp[0]);
    }
    # Now check each table to see if there are records with the requested transnum
 
    foreach my $table (@table_list) {
 
        $query = "
            select * from $table 
            where 
                transnum = '$transnum'
 
        ";    
        $tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) { 
            print $q->p("Records in table $table where transnum is $transnum");
            printResult_arrayref2($tbl_ref);
            $table_count++;
        }  
    }
 
    print $q->b("Found $table_count tables with records for transnum $transnum."); 
    
    StandardFooter();    
	
	
} elsif ($report eq "PROFILEPROMPT_QRY") { 
    print $q->h2("Profile Prompt Search"); 
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"PROFILEPROMPTSRCH"});

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

	my @ltm = localtime(time());
	my $month=$ltm[4]+1;
	my $year=$ltm[5]+1900;
	my $startdate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
 
	my $calcounter = 1;
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
				);
	$calcounter++;
 
 			  
               
                  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Txn:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"txnselected", -size=>"12"}))
              );
                  
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
	print $q->end_table(),"\n";
	print $q->end_form(), "\n";   
	print $q->p("(Leave the Txn field blank to get all of the profile prompt responses for the specified date that have not been decrypted.)");
    StandardFooter(); 
} elsif ($report eq "PROFILEPROMPTSRCH") {    
    print $q->h2("Profile Prompt Search");      
    my $startdate= ($q->param("startdate"))[0]; 
    my $txnselected= ($q->param("txnselected"))[0]; 
	my $report_opt= ($q->param("report_opt"))[0]; 
	unless ($startdate) {
		print $q->h4("ERROR: Failed to determine date");
		StandardFooter(); 
		exit;
		
	}
	my $txn_where;
	if ($txnselected) {
		$txn_where = "			and
				tppr.txnnum = $txnselected ";
	} else {
		$txn_where = " and tppr.profileresponse4 like 'kIo0w1Ykva8='";
	}
 
	if ($report_opt) {
 
		unless ($txnselected) {
			print $q->h4("ERROR: No Txn specified.");
			StandardFooter(); 
			exit;			
		}
		my $query = "
			update txn_profile_prompt_response
			set profileresponse4 = ''
 
			where 
				itemdatetime like '$startdate%'
			and
				txnnum = $txnselected         
		";	
		if ($dbh->do($query)) {
			print $q->p("Successfully cleared profileresponse4 for txn $txnselected on $startdate");
			$query = "
				select 
					txnnum,
					profileresponse1,
					profileresponse2,
					profileresponse3,
					profileresponse4
				from 
					txn_profile_prompt_response 
				where 
					itemdatetime like '$startdate%'
				and
					txnnum = $txnselected            
			";
			$tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
			if (recordCount($tbl_ref)) { 
				print $q->p("Cleared record in table txn_profile_prompt_response where txnnum is $txnselected");
				printResult_arrayref2($tbl_ref);    			
			}  else {
				print $q->p("No Records in table txn_profile_prompt_response where txnnum is $txnselected");
			}				
		} else {
			print $q->p("Error attempting to clear profileresponse4 for txn $txnselected on $startdate");
			print "Database Update Error"."<br />\n";
			print "Driver=ODBC"."<br />\n";
			print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
			print "query=".$query."<br />\n";
			print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
			print "errstr=".$dbh->errstr ."<br />\n";            
		}   
	
	} else {
 
		# Get a list of all of the tables that have the specified field/column. 
		my $query = "
			select 
				tpt.txndatetime,
				tppr.txnnum,
				tpt.transnum,
				tpt.regnum,
				
				tppr.profileresponse1,
				tppr.profileresponse2,
				tppr.profileresponse3,
				tppr.profileresponse4
			from 
				txn_profile_prompt_response tppr
				join txn_pos_transactions tpt on tppr.txnnum = tpt.txnnum and tppr.transnum = tpt.transnum
			where 
				tppr.itemdatetime like '$startdate%'
			$txn_where           
		";
		#print $q->pre("$query");
		$tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) { 
			if ($txnselected) {
				print $q->p("Records in table txn_profile_prompt_response where txnnum is $txnselected");
			} elsif ($startdate) {
				print $q->p("Un-Decrypted records in table txn_profile_prompt_response for $startdate");
			}
			printResult_arrayref2($tbl_ref);

			if ($txnselected) {
				print "<br />";
				print $q->p("Is this the record you wish to clear?");
								 
				print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
				print $q->input({-type=>"hidden", -name=>"report", -value=>"PROFILEPROMPTSRCH"});  
				print $q->input({-type=>"hidden", -name=>"startdate", -value=>"$startdate"}); 
				print $q->input({-type=>"hidden", -name=>"txnselected", -value=>"$txnselected"}); 				
				print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
					   $q->input({-type=>"radio", -name=>"report_opt", -value=>"1", -checked=>"1"}) .            
					   $q->font({-size=>2}, "Clear")));
				print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
					   $q->input({-type=>"radio", -name=>"report_opt", -value=>"0",}) .            
					   $q->font({-size=>2}, "Do Not Clear")));  
				print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
										
				print $q->end_form(), "\n";    
			}
			
		}  else {
			if ($txnselected) {
				print $q->p("No Records in table txn_profile_prompt_response where txnnum is $txnselected");
			} elsif ($startdate) {
				print $q->p("No Un-Decrypted Records in table txn_profile_prompt_response where date is $startdate");
			}
		}
	 
	}
    StandardFooter();    	
} elsif ($report eq "FIELDSRCH_QRY") { 
    print $q->h2("Field Search"); 
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"report", -value=>"FIELDSRCH"});  
        
    print  $q->input({ -type=>'text', -name=>"field", -size=>"10", -value=>""});        
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
    print $q->p("Enter a field to search for and click the 'Enter' button.");
    print  $q->input({ -type=>'text', -name=>"text", -size=>"10", -value=>""});        
                   
    print $q->p("Option:  Enter a specific value to search for in this field in all of the found tables.");    
    print $q->end_form(), "\n";    
    StandardFooter();    
} elsif ($report eq "FIELDSRCH") {    
    print $q->h2("Field Search");      
    my $field= ($q->param("field"))[0]; 
    my $text= ($q->param("text"))[0]; 
    my $table_count=0;
    my @table_list;
    # Get a list of all of the tables that have the specified field/column. 
    my $query = "
        select 
            table_name 
        from 
            SYSTABLE 
        where 
            table_id in
            (select table_id from SYSCOLUMN where column_name like '$field')
    ";
    
    my $sth = $dbh->prepare($query);        
    $sth->execute();        
    while (my @tmp=$sth->fetchrow_array()) { 
        print "$tmp[0]<br />";
        $table_count++;
        push(@table_list,$tmp[0]);
    }
 
    print "<br />";
    print $q->b("Found $table_count tables with the field $field."); 
    if ($text) {
        # Find all of the tables where the specified text is found in the specified field
        $table_count=0;
        foreach my $table (@table_list) {
            $query="
                select * from $table where $field = '$text'
            ";
            my $tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref)) { 
                print $q->p("Records in table $table where field $field is $text.");
                printResult_arrayref2($tbl_ref);   
                $table_count++;
            }  
            
        }
        print "<br />";
        print $q->b("Found $table_count tables with the field \'$field\' with a value of \'$text\'.");         
    }
    StandardFooter();    
    
} elsif ($report eq "STORENUMCHECK") {    
    print $q->h2("Store Number Check Tool.");      
    my $storenum = getStoreNum($dbh);  
    my $table_count=0;
    # Get a list of all of the tables that use the "storenum" column and check if they have any entries where storenum is not this store
    my $query = "
        select 
            table_name 
        from 
            SYSTABLE 
        where 
            table_id in
            (select table_id from SYSCOLUMN where column_name = 'storenum')
    ";
    my $sth = $dbh->prepare($query);        
    $sth->execute();    
    my @table_list;
    while (my @tmp=$sth->fetchrow_array()) { 
        push(@table_list,$tmp[0]);
    }
    # Now check each table to see if there are records for other store numbers
    # Note: We ignore storenum 9999 because that means all stores
    foreach my $table (@table_list) {
        # Since the store table lists all of the stores, we expect different store numbers
        next if ($table eq "store");
        $query = "
            select * from $table 
            where 
                storenum <> '$storenum'
            and
                storenum <> 9999
        ";    
        $tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) { 
            print $q->p("Records in table $table where storenum is not $storenum");
            printResult_arrayref2($tbl_ref);
            $table_count++;
        }  
    }
 
    print $q->b("Found $table_count tables with records for a storenum that is not $storenum."); 
    
    StandardFooter();   
} elsif ($report eq "CLAIMS") {    
    print $q->h2("Claims Records.");  
    my $table = "C:\\mlink\\akron\\CLAIMED.dbf";
    my $fox_dbh = openFoxProDriver("C:\\");
    #my $query = "select id, type, qty, date from $table where id like 'IA_%'";
    my $query = "select * from $table";
    $tbl_ref = TTV::utilities::execQuery_arrayref($fox_dbh, $query);
    if (recordCount($tbl_ref)) { 
        printResult_arrayref2($tbl_ref);
    } else { 
        print $q->b("no records found<br />\n"); 
    }    
    $fox_dbh->disconnect;    
    StandardFooter();     
    
} elsif ($report eq "RENAMERECPO") {    
    print $q->h2("Rename Receiving PO.");  
    # Ask which PO to rename and what to rename it to
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"RENAMERECPO2"});

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("PO Rename Criteria:")));

  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Current PO:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},                      
        $q->input({-type=>'text', -name=>"current_po_selection", -size=>"20" }),                  
        ));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New PO:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},                      
        $q->input({-type=>'text', -name=>"new_po_selection", -size=>"20" }),                  
        ));  
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({ -value=>"   Go!   "}),
        ));
 
    print $q->end_table(),"\n";

    print $q->end_form(), "\n"; 
    StandardFooter();         
} elsif ($report eq "RENAMERECPO2") {   
 
    my $current_po_selection= ($q->param("current_po_selection"))[0]; 
    my $new_po_selection= ($q->param("new_po_selection"))[0];
    my $confirm= ($q->param("confirm"))[0];     
    print $q->h2("Rename Receiving PO from: $current_po_selection to: $new_po_selection"); 
 

    my $record_count=0;
    unless ($confirm) {
        # First see if there is a po with this current name
        my $query = "
            select * from receiving 
            where ponum like  '$current_po_selection';
        ";    
       
        $tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) { 
            print $q->p("Found these records in receiving with POs $current_po_selection");
            printResult_arrayref2($tbl_ref);
            $record_count++;
        }      
        unless ($record_count) {
            print $q->h3("ERROR: Failed to locate a receiving record with a PO of $current_po_selection");
            StandardFooter();   
            exit;
        }
        my $query = "
            select * from receiving 
            where ponum like  '%$new_po_selection%';
        ";    
       
        $tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) { 
            print $q->p("NOTE: Found these records in receiving with POs similar to $new_po_selection");
            print $q->p("(It is okay that there is already a record here but this renaming will add another one.)");
            printResult_arrayref2($tbl_ref);
            $record_count++;
        }     
        if ($record_count) {
            print $q->start_form({-action=>$scriptname, -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"RENAMERECPO2"}); 
            print $q->input({-type=>"hidden", -name=>"current_po_selection", -value=>"$current_po_selection"});
            print $q->input({-type=>"hidden", -name=>"new_po_selection", -value=>"$new_po_selection"});              
            print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});                      
     
            print $q->submit({  -value=>"   Yes!   "}); 
            print $q->end_form(), "\n";
       
            print $q->p("Select 'Yes!' to confirm that you wish to go ahead with the rename.");
        }
    }
    
    unless ($record_count) {  
        my $query = "
            update receiving 
            set ponum = '$new_po_selection' where ponum like  '$current_po_selection';
        ";
		if ($dbh->do($query)) {
			print $q->p("Successfully renamed $current_po_selection to $new_po_selection");
            $query = "
                select * from receiving 
                where ponum like  '%$new_po_selection%';
            ";    
           
            $tbl_ref = TTV::utilities::execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref)) { 
                print $q->p("Current records in receiving with POs similar to $new_po_selection");                
                printResult_arrayref2($tbl_ref);
                $record_count++;
            } else {
                print $q->p("Unknown error attempting to rename $current_po_selection to $new_po_selection");
               
            }            
            print $q->p();
 			print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=RENAMERECPO"}, "Rename another.");  
		} else {
			print $q->p("Error attempting to rename $current_po_selection to $new_po_selection");
			print "Database Update Error"."<br />\n";
			print "Driver=ODBC"."<br />\n";
			print  $dbh->{Name}."<br />\n";  
			print "query=".$query."<br />\n";
			print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
			print "errstr=".$dbh->errstr ."<br />\n"; 
          
		}   
    }
 
 
    StandardFooter();      
} elsif ($report eq "MERGECLAIMS") {    
    print $q->h2("Merge Claims Records. (BETA)"); 
    my $append_table= ($q->param("append_table"))[0];     
    my $table = "C:\\mlink\\akron\\CLAIMED.dbf";
    my $tbl_ref;
    my $append_tbl_ref;
    if (-f $table) {
        my $fox_dbh = openFoxProDriver("C:\\");
        if ($append_table) {
 
            if (-f $append_table) {
                my @append_array_candidate=();
                my @append_array=();   
                my @current_array=();                
                # Change the single slashes to double
                #$append_table =~ s/\\/\\\\/g;
                # Collect all of the entries from the specified table
           
                my $query = "SELECT * from $append_table";
                #print $q->pre("$query");
                my $sth = $fox_dbh->prepare($query);
                $sth->execute();

                while (my @tmp=$sth->fetchrow_array()) {  
                   
                    push(@append_array_candidate,\@tmp);
                }
             
                # Collect all of the current entries in the claimed database
                $query = "SELECT * from $table";
               # print $q->pre("$query");
                $sth = $fox_dbh->prepare($query);
                $sth->execute();
            
                while (my @tmp=$sth->fetchrow_array()) {  
                    push(@current_array,\@tmp);
                }      
              
                # For each entry in the append, make certain it is not already in the current database
                foreach my $append_ref (@append_array_candidate) {
                    my $found=0;
                    my @array_append_entry = @$append_ref;
                    foreach my $current_ref (@current_array) {
                        my $matches=1;
                        my @array_current_entry=@$current_ref;
                        for (my $x=0; $x<=$#array_current_entry; $x++) {
                            my $current_entry=$array_current_entry[$x];
                            my $append_candidate=$array_append_entry[$x];
                            unless ($current_entry eq $append_candidate) {
                                # This entry is different
                                $matches=0;
                                $x=$#array_current_entry;
                            }
                        }
                        if ($matches) {   
 
                            $found=1;
                            last;
                        }
                    }
                    unless ($found) {
                        # This entry should be appended
                   
                        push(@append_array,$append_ref);
                    }
                }
               
                # Append all of the new entries
                my $success_count=0;
                my $fail_count=0;
                foreach my $append_ref (@append_array) {
                    my @array=@$append_ref;
                    my $values = "(";
                    my $sep;
                    for (my $x=0; $x<=$#array; $x++) {
                        my $entry=$array[$x];
                        if ($x == 0) {
                            $entry =~ s/ //g;
                            $entry = "\'$entry\'";
                        }
                        if ($x == 1) {
                            $entry =~ s/ //g;
                             $entry = "\'$entry\'";
                        }   
                        if ($x == 2) {
                            $entry =~ s/ //g;
                            $entry = "\'$entry\'";
                        }  
                        if ($x == 3) {
                            $entry =~ s/ //g;
                            #$entry = "\"$entry\"";
                        } 
                        if ($x == 4) { 
                            # This is the date
                            my @tmp=split(/\s+/,$entry);
                            my $date = $tmp[0];
                            my $time = $tmp[1];
                            @tmp=split(/\//,$date);
                            my $year = $tmp[2];
                            my $mo = $tmp[0];
                            my $day = $tmp[1];                            
                            @tmp=split(/:/,$time);
                            my $hr = $tmp[0];
                            my $min = $tmp[1];
                            my $sec = $tmp[2];
                            my $datetime = sprintf("%4d-%02d-%02d %02d:%02d:%02d", $year, $mo, $day, $hr, $min, $sec);                            
                            $entry = "{ts '". $datetime ."'}";
                        }   
                        if ($x == 5) {    
                            $entry =~ s/ //g;                        
                             $entry = "\'$entry\'";
                        }                           
                        $values .= "$sep $entry";
                        $sep=",";
                    }
                    $values .= ")";
                    $query = "insert into $table values $values";
                   
                    print $q->pre("$query");
                    if ($fox_dbh->do($query)) {
                        print $q->p("Successfully append an entry to main claimed database file.");
                        $success_count++;
                    } else {
                        print $q->p("Error attempting to append entry to main claimed database file.");
                        print "Database Update Error"."<br />\n";
                        print "Driver=ODBC"."<br />\n";
                        print  $fox_dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
                        print "query=".$query."<br />\n";
                        print "err=".$fox_dbh->err." state: ".$fox_dbh->state."<br />\n";
                        print "errstr=".$fox_dbh->errstr ."<br />\n";            
                        $fail_count++;
                        StandardFooter();                           
                        exit;   
                    }                                         
                }              
                print $q->h4("Successfully added $success_count entries to the CLAIMED.dbf file with $fail_count failed attempts.");                     
            }  else {
                print $q->h3("Error: Failed to locate $append_table");
                StandardFooter();                           
                exit;                
            }                  
        } else {
            # Get the table to append data from
            print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MERGECLAIMS"});
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Claim Database to Merge:")));  
            
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Full path to DBF File:"),
                $q->td({-class=>'tabledatanb', -align=>'left'},                      
                $q->input({-type=>'text', -name=>"append_table", -size=>"30" }),                  
                ));              
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                    $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
            );
            print $q->end_table(),"\n";
            print $q->end_form(), "\n";            
        }
        $fox_dbh->disconnect;  
    }
    StandardFooter();     
    
} elsif ($report eq "PENDINGXFR") {    
    print $q->h3("Pending Transfers."); 
	$query = dequote(<< "	ENDQUERY");
        select 
            txnDate,
            documentNum,
            transferStore,
            note,
            transferType          
        from 
            store_transfer
        where
            txnStatus = 'P'
        
	ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {
 
        printResult_arrayref2($tbl_ref); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }    
    StandardFooter();       
} elsif ($report eq "UPDATEAUDIT") {    
    print $q->h3("Last 500 entries to update_audit table."); 
    #print "Audit update types:<br />";
    #print "-> 1 = 'added'<br />";
    #print "-> 2 = 'modified'<br />";
    #print "-> 3 = 'deleted'<br />";
    
    
	$query = dequote(<< "	ENDQUERY");
        select * from update_audit order by timeofupdate desc
	ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my $tbl_ref2= [];
    if (recordCount($tbl_ref)) {
        for my $datarows (0 .. 500) {
            my $thisrow = @$tbl_ref[$datarows];   
            if ($$thisrow[4] == 1) {
                $$thisrow[4] = "1 - added";
            }
            if ($$thisrow[4] == 2) {
                $$thisrow[4] = "2 - modified";
            }            
            if ($$thisrow[4] == 3) {
                $$thisrow[4] = "3 - deleted";
            }                        
            push @$tbl_ref2, $thisrow;
        }        
        print $q->p("update_audit Table: 500 lastest entries");
        printResult_arrayref2($tbl_ref2); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }
    $dbh->disconnect;    
    StandardFooter();       
} elsif ($report eq "CHECKREC") {    
    print $q->h3("Check receiving against inventory_adjustment table"); 
 
    # Check all records for a date range
    print $q->h4("Check a all receiving receiving records for a date range:"); 
    # Ask which day to do the report fo
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"CHECKRECRANGE"});
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    my @ltm = localtime(time());
    my $month=$ltm[4]+1;
    my $year=$ltm[5]+1900;
    my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
    my $hm = 1;
    my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
    $startdate = $enddate; # for now.

    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
            $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
            $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
            )
        )
    );
    $calcounter++;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
            $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
            $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
            )
        )
    );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"checkbox", -name=>"verbose"}) .
               $q->font({-size=>-1}, "Verbose")
              ));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
    );
    print $q->end_table(),"\n";
    print $q->end_form(), "\n";
    print "<br />";
    # Check a specific rectransnum
    print $q->h4("Check a specific receiving receiving record:"); 
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"CHECKREC2"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, 
        "Enter the Rectransnum Number to check:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"rectransnum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'C', -value=>"   Check   "}))
                  );
	
	print $q->end_table(),"\n";
	print $q->end_form(), "\n";     
    StandardFooter(); 
} elsif ($report eq "CHECKREC2") {   
    my $rectransnum= ($q->param("rectransnum"))[0];  
    print $q->h3("Check receiving against inventory_adjustment table - Rectransnum: $rectransnum");  
    
    $query = dequote(<< "    ENDQUERY");
    select 
        r.storenum,r.rectransnum, r.documentNum, r.txnStatus, r.poNum, 
        convert(varchar(19),r.txnDate,21),  r.vendorNum as VendorId,v.vendorname as Vendor
    from 
        receiving r
        join Vendor v on v.vendorid=r.vendorNum        
    where 
        r.rectransnum =$rectransnum

    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->h3("Receiving Transaction $rectransnum");
        printResult_arrayref2($tbl_ref); 
		# Get the status from this record.  This is important because if the status is V (voided) 
		# then the qty shown is actually negative
		my $status;
		if (recordCount($tbl_ref)) {  
			for my $datarows (1 .. $#$tbl_ref) {
				my $thisrow = @$tbl_ref[$datarows];
				$status=$$thisrow[3];
			}
		}
		if ($status eq "V") {
		 
			print $q->h3("Note: This record has been voided");
		}		
        # Show the detail of what was received and compare to what was adjusted on that day
        my $modtime;
        my @header=("Rec SKU","SKU Desc","Rec Qty","Inv Qty","Diff","Man. Adj'd");
        my @array_to_print=\@header;
        my %recv_hash;
        my %adj_hash;
        my %man_adj_hash;
		my %desc_hash;
        $query = dequote(<< "        ENDQUERY");
        select 
            rd.plunum,
			p.pludesc,
            rd.qtyReceived,
            rd.modtime
        from 
            receiving_dtl rd            
            join plu p on rd.plunum = p.plunum
        where 
            rd.rectransnum =$rectransnum
        and
            p.invcontrolled = 'Y'

        ENDQUERY
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
			my $desc=$tmp[1];
            my $qty=$tmp[2];   
            $modtime=substr($tmp[3],0,10);
			if ($status eq "V") {
				# need to make the qty negative
				$qty=(0 - $qty);
			}
            $recv_hash{$plunum}+=$qty;	
			$desc_hash{$plunum}=$desc;			
        }
        # Find the SKUs adjusted in on this date
        $query=dequote(<< "        ENDQUERY");    
        select 
            plunum,
            sum(qty)
        from 
            inventory_adjustment 
        where 
            adjustDate like  '$modtime%'  
        and 
            functionid = 3
        group by plunum
        order by plunum
        ENDQUERY
        
        
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];
            $adj_hash{$plunum}=$qty; 				
        }  
        # Find the inventory adjustments since this date which might have been because someone was trying to correct
        # these SKUs in inventory
        $query=dequote(<< "        ENDQUERY");    
        select 
            plunum,
            sum(qty)
        from 
            inventory_adjustment 
        where 
            adjustDate >  '$modtime'  
            
        and 
            functionid = 5
        group by plunum
        order by plunum
        ENDQUERY
        
        
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];
            $man_adj_hash{$plunum}=$qty;            
        }          
        ###
        my $diff_count=0;
        foreach my $sku (sort keys(%recv_hash)) {
			my $desc=$desc_hash{$sku};
            my $r_qty=$recv_hash{$sku};
            my $a_qty=0;
            if ($adj_hash{$sku}) {
                $a_qty=$adj_hash{$sku};
            }
            my $diff=($r_qty - $a_qty);
            if ($diff) {
                $diff_count++;
            } else {
                $diff="";
            }
            my $man;
            if ($man_adj_hash{$sku}) {
                $man=$man_adj_hash{$sku};
            }
            my @array=("$sku","$desc","$r_qty","$a_qty","$diff","$man");
            push(@array_to_print,\@array);
        }
        if (recordCount(\@array_to_print)) {
            print $q->h3("Receiving Transaction $rectransnum - Comparing Received Qty against Inventory");
            my @format = (
            '', '',
            {'align' => 'Center', 'num' => '.0'},
            {'align' => 'Center', 'num' => '.0'},
            {'align' => 'Center', 'num' => '.0'},
            {'align' => 'Center', 'num' => '.0'},
             );
             
            printResult_arrayref2(\@array_to_print,'',\@format); 
            print $q->p("There were $diff_count SKUs with differences between receiving and inventory adjustment.");
            print $q->p("Man. Adj'd column shows how many of these SKUs have been manually adjusted since this record was received.");
        } else {
            print $q->h4("No Detail for Receiving Transaction $rectransnum found!");
        }
    } else {
        print $q->h4("No header for Receiving Transaction $rectransnum found!");
    }      
  
    StandardFooter();     
} elsif ($report eq "CHECKXFR") {    
    print $q->h3("Check transfer against inventory_adjustment table"); 
 
    # Check all records for a date range
    print $q->h4("Check all transfer records for a date range:"); 
    # Ask which day to do the report fo
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"CHECKXFRRANGE"});
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    my @ltm = localtime(time());
    my $month=$ltm[4]+1;
    my $year=$ltm[5]+1900;
    my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
    my $hm = 1;
    my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
    $startdate = $enddate; # for now.

    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
            $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
            $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
            )
        )
    );
    $calcounter++;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
            $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
            $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
            )
        )
    );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"checkbox", -name=>"verbose"}) .
               $q->font({-size=>-1}, "Verbose")
              ));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
    );
    print $q->end_table(),"\n";
    print $q->end_form(), "\n";
    print "<br />";
    # Check a specific rectransnum
    print $q->h4("Check a specific transfer record:"); 
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"CHECKXFR2"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, 
        "Enter the ststransnum Number to check:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({-type=>'number', -name=>"ststransnum", -size=>"4"}),
                         )
                  );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
        $q->td({-class=>'tabledatanb', -align=>'left'}),
        $q->submit({-accesskey=>'C', -value=>"   Check   "}))
                  );
	
	print $q->end_table(),"\n";
	print $q->end_form(), "\n";     
    StandardFooter(); 
} elsif ($report eq "CHECKXFR2") {   
    my $ststransnum= ($q->param("ststransnum"))[0];  
    print $q->h3("Check transfer against inventory_adjustment table - ststransnum: $ststransnum");  
    
    $query = dequote(<< "    ENDQUERY");
    select 
        st.storenum,st.ststransnum, st.documentNum, st.txnStatus,   
        convert(varchar(19),st.txnDate,21),  st.vendorNum as VendorId,v.vendorname as Vendor
    from 
        store_transfer st
        left outer join Vendor v on v.vendorid=st.vendorNum        
    where 
        st.ststransnum =$ststransnum

    ENDQUERY
    
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->h3("Transfer Transaction $ststransnum");
        printResult_arrayref2($tbl_ref, '', '', '', {'tableid' => 'tableid1'});  
        # Show the detail of what was received and compare to what was adjusted on that day
        my $modtime;
        my @header=("Rec SKU","SKU Desc","Rec Qty","Inv Qty","Diff","Man. Adj'd");
        my @array_to_print=\@header;
        my %recv_hash;
		my %sku_desc_hash=();
        my %adj_hash;
        my %man_adj_hash;
   
		# The following handles the situation where a bar-code is used rather than a SKU
        $query = dequote(<< "        ENDQUERY");
        (select 
            p.plunum,
			p.pludesc,
            std.qtyReceived,
            st.modtime,
            st.transferType,
			std.itemcnt
        from 
            store_transfer_dtl std            
            join plu p on std.plunum = p.plunum
            join store_transfer st on std.ststransnum = st.ststransnum
        where 
            std.ststransnum =$ststransnum
        and
            std.qtyReceived <> 0
        and
            p.invcontrolled = 'Y')
		union
        (select 
            p.plunum,
			p.pludesc,
            std.qtyReceived,
            st.modtime,
            st.transferType,
			std.itemcnt
        from 
            store_transfer_dtl std            
            join plu_cross_ref pcr on pcr.xrefnum = std.plunum
			join plu p on p.plunum = pcr.plunum
            join store_transfer st on std.ststransnum = st.ststransnum
        where 
            std.ststransnum =$ststransnum
        and
            std.qtyReceived <> 0
        and
            p.invcontrolled = 'Y')		

        ENDQUERY
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[2];  
			my $desc=$tmp[1]; 
            my $type=$tmp[4];
			my $item=$tmp[5];
            if ($type eq "O") {
                # This was a transfer out so qty is negative
                $qty=(0 - $qty);
            }
            
            $modtime=substr($tmp[3],0,10);
		 
            $recv_hash{$plunum}+=$qty;
			$sku_desc_hash{$plunum}=$desc;
			
        }
		
        # Find the SKUs adjusted in on this date
        $query=dequote(<< "        ENDQUERY");    
        select 
            plunum,
            sum(qty)
        from 
            inventory_adjustment 
        where 
            adjustDate like  '$modtime%'  
        and 
            (functionid = 6
            or
            functionid = 7)
        group by plunum
        order by plunum
        ENDQUERY
                
		#print $q->pre($query);
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];
            $adj_hash{$plunum}+=$qty;            
        }  
        # Find the inventory adjustments since this date which might have been because someone was trying to correct
        # these SKUs in inventory
        $query=dequote(<< "        ENDQUERY");    
        select 
            plunum,
            sum(qty)
        from 
            inventory_adjustment 
        where 
            adjustDate >  '$modtime'  
            
        and 
            functionid = 5
        group by plunum
        order by plunum
        ENDQUERY
        
        
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];
            $man_adj_hash{$plunum}=$qty;            
        }          
        ###
        my $diff_count=0;
        foreach my $sku (sort keys(%recv_hash)) {
			my $desc=$sku_desc_hash{$sku};
            my $r_qty=$recv_hash{$sku};
            my $a_qty=0;
            if ($adj_hash{$sku}) {
                $a_qty=$adj_hash{$sku};
            }
            my $diff=($r_qty - $a_qty);
            if ($diff) {
                $diff_count++;
            } else {
                $diff="";
            }
            my $man;
            if ($man_adj_hash{$sku}) {
                $man=$man_adj_hash{$sku};
            }
			 
            my @array=("$sku","$desc","$r_qty","$a_qty","$diff","$man");
            push(@array_to_print,\@array);
        }
		 
        if (recordCount(\@array_to_print)) {
            print $q->h3("Transfering Transaction $ststransnum - Comparing Transfered Qty against Inventory");
            my @format = (
            '', '',
            {'align' => 'Center', 'num' => '.0'},
            {'align' => 'Center', 'num' => '.0'},
            {'align' => 'Center', 'num' => '.0'},
            {'align' => 'Center', 'num' => '.0'},
             );
             
            printResult_arrayref2(\@array_to_print,'',\@format); 
            print $q->p("There were $diff_count SKUs with differences between transfering and inventory adjustment.");
            print $q->p("Man. Adj'd column shows how many of these SKUs have been manually adjusted since this record was received.");
        } else {
            print $q->h4("No Detail for Transfering Transaction $ststransnum found!");
        }
    } else {
        print $q->h4("No header for Tranfering Transaction $ststransnum found!");
    }      
  
    StandardFooter();   
} elsif ($report eq "CHECKXFRRANGE") {   
    my $rectransnum= ($q->param("rectransnum"))[0];   
    my $startdate= ($q->param("startdate"))[0];   
    my $enddate= ($q->param("enddate"))[0];  
    my $verbose= ($q->param("verbose"))[0]; 
    my @tmp;
    my %records;
    my $count=0;    
    print $q->h3("Check Transfer records from $startdate to $enddate"); 
    # Collect a list of all transfer records between these dates
    # Note that the transfer date and the mod date can be quite different so I am looking at both - kdg
    $query=dequote(<< "    ENDQUERY");    
        select 
            modTime 
        from 
            store_transfer
        where
            (
                (transferDate > '$startdate'
                or
                transferDate like '$startdate%')
            and
                (transferDate < '$enddate'
                or
                transferDate like '$enddate%')        
            )
            or 
            (
                (modTime > '$startdate'
                or
                modTime like '$startdate%')
            and
                (modTime < '$enddate'
                or
                modTime like '$enddate%')              
            )
        order by transferDate
    ENDQUERY
    
    $sth = $dbh->prepare($query);    
    $sth->execute();        
    while (@tmp = $sth->fetchrow_array) {   
        my $transferDate=substr($tmp[0],0,10);
        $records{$transferDate}=1;
    }     
    $count=keys(%records);
    # Now, for each receiving record, see if the SKUs actually got into inventory
    print $q->p("Found $count dates with transfer records between $startdate and $enddate");
    if ($verbose) {
        foreach my $r (sort keys (%records)) {
            print "- $r<br />";
        }
        print "<br />";
    }
    my $total_error_count=0;
    foreach my $record (sort keys (%records)) {
        #print "Checking transfer for $record<br />";
        my %transfer_hash=();
        my %adjustment_hash=();  
		my %transnum_hash=();
        my $transfer_count=0;
        my $adjust_count=0;
        my $error_count=0;
        my %ststransnum_hash;
        #my $ststransnum;
		my $transnum;
        # Find all of the SKUs and qty transfered on this record/date
        my $query="
            (select 
                p.plunum,
                std.qtyReceived,
                std.ststransnum,
                st.transferType,
				st.transnum
            from
                store_transfer_dtl std
                join plu p on p.plunum = std.plunum
                join store_transfer st on st.ststransnum = std.ststransnum
            where                
                st.modtime like '$record%'
            and
                p.invcontrolled = 'Y'
            and
                std.qtyReceived > 0
			and
				st.txnStatus like 'C')
				
			union
            (select 
                p.plunum,
                std.qtyReceived,
                std.ststransnum,
                st.transferType,
				st.transnum
            from
                store_transfer_dtl std
				join plu_cross_ref pcr on pcr.xrefnum = std.plunum
				join plu p on p.plunum = pcr.plunum
                join store_transfer st on st.ststransnum = std.ststransnum
            where                
                st.modtime like '$record%'
            and
                p.invcontrolled = 'Y'
            and
                std.qtyReceived > 0
			and
				st.txnStatus like 'C')			
			 
            
            order by 1
        ";     
		
		#print $q->pre($query);
 
        $sth = $dbh->prepare($query);
        $sth->execute();

        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];  
            my $ststransnum = $tmp[2];
            my $transferType = $tmp[3];
			$transnum = $tmp[4];
			$transnum_hash{$transnum}=$ststransnum;
            if ($transferType eq "O") {
                # This is a transfer out so the qty is negative
                $qty=(0 - $qty);
            }
            $ststransnum_hash{$tmp[2]}=1;			
            $transfer_hash{$plunum}+=$qty;  
			print "transnum: $transnum SKU: $plunum Qty: $qty Type: $transferType<br />";
        }
		foreach $transnum (sort keys(%transnum_hash)) {
			my $ststransnum=$transnum_hash{$transnum};
		 	
			#if ($verbose) {
				print "Checking inventory adjustment transnum $transnum<br />";
			#}
			# 2013-09-25 - Added checking by transnum when I realized that the transnum of the 
			# store transfer matches the inventory adjustment record - kdg
			print "Checking transfer for record $ststransnum completed on $record<br />";        
			# Next, get the amount received from the inventory_adjustments table
 		
			$query=dequote(<< "			ENDQUERY");    
			select 
				plunum,
				sum(qty)				
			from 
				inventory_adjustment 
			where 
				--adjustDate like  '$record%'
				transnum = $transnum
			and 
				(functionid = 6
				or
				functionid = 7)
			group by plunum
			order by plunum
			ENDQUERY
 
			$sth = $dbh->prepare($query);
			$sth->execute();
			while (my @tmp=$sth->fetchrow_array()) {
				my $plunum=$tmp[0];
				my $qty=$tmp[1];					 
				$adjustment_hash{$plunum}+=$qty;
				#$adjust_count++;
			}  
 
			$transfer_count=keys(%transfer_hash);
			$adjust_count=keys(%adjustment_hash);
			 
			if ($transfer_count == $adjust_count) {
				if ($verbose) {
					print $q->p("-------> Transfer Count: $transfer_count matches Adjust Count: $adjust_count");
				}
			} else {
				print $q->p( "-------->Found $transfer_count different SKUs transfered and $adjust_count different SKUs adjusted into inventory");
			}
		}
        # Now compare the received with the adjusted
        my @problem_sku_list;
        foreach my $sku (sort keys(%transfer_hash)) { 

            my $r_qty=$transfer_hash{$sku}; 
			if ($verbose) {
				print "Checking sku: $sku - transfered $r_qty<br />";
			}			
            if ($adjustment_hash{$sku}) {                
                my $a_qty=$adjustment_hash{$sku};
				if ($verbose) {
					print "Checking sku: $sku - adjusted $r_qty<br />";
				}		
                unless ($r_qty == $a_qty) {
                    $error_count++;
                    push(@problem_sku_list,$sku);   
                    if ($verbose) {
                        print "On Record $record, SKU $sku shows $r_qty transfered but $a_qty went into inventory.<br />";
                    }                    
                }
            } else {
                if ($r_qty) {
                    $error_count++;
                    push(@problem_sku_list,$sku); 
                    if ($verbose) {
                        print "On $record, SKU $sku shows $r_qty transfered but nothing went into inventory.<br />";
                    }            
                }
            }
        } 
        #   Finally, check the inventory adjustments against receiving
        foreach my $sku (sort keys(%adjustment_hash)) { 
            my $a_qty=$adjustment_hash{$sku};
			if ($verbose) {
				print "Checking sku: $sku - adjusted $a_qty<br />";
			}				
            if ($transfer_hash{$sku}) {
                my $r_qty=$transfer_hash{$sku}; 
				if ($verbose) {
					print "Checking sku: $sku - transfered $r_qty<br />";
				}						
                unless ($r_qty == $a_qty) {
                    $error_count++;
                    if ($verbose) {
                        print "On Record $record, SKU $sku shows $r_qty transfered but $a_qty went into inventory.<br />";
                    }                    
                }  
 
            }
        }   
        
        if ($error_count) {
            # Check the problem SKUs against the list of transfer records so we can show the ones likely to be suspect (Remember, we only know which SKUs have discrepancies
            # but not which transfering records.
            my %ststransnum_hash;
            foreach my $suspect_sku (@problem_sku_list) {
                $query="
                    select 
                        ststransnum
                    from
                        store_transfer_dtl
                    where                
                        modtime like '$record%'
                    and
                        plunum like '$suspect_sku'
 
                ";        
				
                $sth = $dbh->prepare($query);
                $sth->execute();
                while (my @tmp=$sth->fetchrow_array()) {                  
                    $ststransnum_hash{$tmp[0]}=1;
                }
            }
       
            print $q->p( "!!!!!!   Found $error_count errors checking transfers on $record   !!!!!!!!");
            foreach my $ststransnum (sort keys(%ststransnum_hash)) {
                print ">>>>>>Check transfer record ";                
                print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=CHECKXFR2&ststransnum=$ststransnum "}, "$ststransnum"); 
                print "<br /><br />";
            }
            
            print "\n";
            $total_error_count++;
        }
        
    }
    print $q->p("Found $total_error_count dates with errors checking transfers for the specified date range.");
    $dbh->disconnect;   
    StandardFooter();         	
} elsif ($report eq "CHECKREC2_orig") {   
    my $rectransnum= ($q->param("rectransnum"))[0];  
    print $q->h3("Check receiving against inventory_adjustment table - Rectransnum: $rectransnum");  
    
    $query = dequote(<< "    ENDQUERY");
    select 
        r.storenum,r.rectransnum, r.documentNum, r.txnStatus, r.poNum, 
        convert(varchar(19),r.txnDate,21),  r.vendorNum as VendorId,v.vendorname as Vendor
    from 
        receiving r
        join Vendor v on v.vendorid=r.vendorNum        
    where 
        r.rectransnum =$rectransnum

    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->h3("Receiving Transaction $rectransnum");
        printResult_arrayref2($tbl_ref, '', '', '', {'tableid' => 'tableid1'});         
    } else {
        print $q->h4("No header for Receiving Transaction $rectransnum found!");
    }      
    print "<br /><br />";
     print $q->h3("Match receiving entry to adjustment entry");
    $query=dequote(<< "    ENDQUERY");    
    select 
        rd.plunum,
        rd.qtyReceived,
        ia.plunum as AdjustedSKU,
        ia.qty as AdjustedQty,
        r.receiveDate,
        case 
            when rd.qtyReceived = ia.qty 
        then 
            ' - ' 
        else 
            'Qty Error' 
        end 
            as Status
    from
        receiving_dtl rd
        join receiving r on r.storenum = rd.storenum and r.rectransnum = rd.rectransnum
        left outer join inventory_adjustment ia on dateformat(ia.adjustdate,'yyyy-mm-dd') = dateformat(r.receiveDate,'yyyy-mm-dd')
            and rd.plunum=ia.plunum 
            and rd.qtyReceived=ia.qty
        join plu p on p.plunum=rd.plunum
    where 
        r.rectransnum = $rectransnum
    and
        p.invcontrolled = 'Y'
    and
        rd.qtyReceived > 0
    order by
        rd.plunum
    ENDQUERY
    
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) { 
        print "Note: This table only shows SKUs under inventory control.<br />";
        print "Note also that if SKUs have been purged at one point and re-entered into<br />";
        print "inventory, they will show up as missing from the inventory_adjustment table.<br />";
        my @format = ('', {'align' => 'Center', 'num' => '.0'},'',{'align' => 'Center', 'num' => '.0'},'',{'align' => 'Center'});
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format);
        
    } else {
        print "No records found for this rectransnum <br />";
        
    }    
    StandardFooter(); 
} elsif ($report eq "CHECKRECRANGE") {   
    my $rectransnum= ($q->param("rectransnum"))[0];   
    my $startdate= ($q->param("startdate"))[0];   
    my $enddate= ($q->param("enddate"))[0];  
    my $verbose= ($q->param("verbose"))[0]; 
    my @tmp;
    my %receiving_records;
    my $count=0;    
    print $q->h3("Check Receiving records from $startdate to $enddate"); 
    # Collect a list of all receiving records between these dates
    $query=dequote(<< "    ENDQUERY");    
        select 
            receiveDate 
        from 
            receiving
        where
            (receiveDate > '$startdate'
            or
            receiveDate like '$startdate%')
        and
            (receiveDate < '$enddate'
            or
            receiveDate like '$enddate%')    
		and
			txnStatus like 'R'
        
        order by receiveDate
    ENDQUERY
    
    $sth = $dbh->prepare($query);    
    $sth->execute();        
    while (@tmp = $sth->fetchrow_array) {   
        my $receiveDate=substr($tmp[0],0,10);		
        $receiving_records{$receiveDate}=1;
    }     
    $count=keys(%receiving_records);
    # Now, for each receiving record, see if the SKUs actually got into inventory
    print $q->p("Found $count dates with receiving records between $startdate and $enddate");
    if ($verbose) {
        foreach my $r (sort keys (%receiving_records)) {
            print "- $r<br />";
        }
        print "<br />";
    }
    my $total_error_count=0;
    foreach my $record (sort keys (%receiving_records)) {
        print "Checking receiving for $record<br />";
        my %receiving_hash=();
        my %adjustment_hash=();  
        my $receive_count=0;
        my $adjust_count=0;
        my $error_count=0;
        my %rectransnum_hash;
        # Find all of the SKUs and qty received on this record
        my $query="
            select 
                rd.plunum,
                rd.qtyacc,
                rd.rectransnum,
				r.txnStatus
            from
                receiving_dtl rd
                join plu p on p.plunum = rd.plunum
				join receiving r on r.rectransnum = rd.rectransnum
            where                
                rd.modtime like '$record%'
            and
                p.invcontrolled = 'Y'
            and
                rd.qtyacc > 0
			and
				r.txnStatus like 'R'
            
            order by rd.plunum
        ";        
        
        #print $q->pre("$query");
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];    
            $rectransnum_hash{$tmp[2]}=1;
			my $status = $tmp[3];
			if ($status eq "V") {
				# Need to change the qty to negative
				$qty=(0-$qty);
			}
            $receiving_hash{$plunum}+=$qty;  
            #$receive_count++;
        }
        # Next, get the amount received from the inventory_adjustments table
        $query=dequote(<< "        ENDQUERY");    
        select 
            plunum,
            sum(qty)
        from 
            inventory_adjustment 
        where 
            adjustDate like  '$record%'  
        and 
            functionid = 3
        group by plunum
        order by plunum
        ENDQUERY
        
        
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            my $plunum=$tmp[0];
            my $qty=$tmp[1];
            $adjustment_hash{$plunum}=$qty;
            #$adjust_count++;
        }  
        $receive_count=keys(%receiving_hash);
        $adjust_count=keys(%adjustment_hash);
         
        unless ($receive_count == $adjust_count) {
            print $q->p( "-------->Found $receive_count different SKUs received and $adjust_count different SKUs into inventory");
        }
 
        # Now compare the received with the adjusted
        my @problem_sku_list;
        foreach my $sku (sort keys(%receiving_hash)) {  
            my $r_qty=$receiving_hash{$sku};        
            if ($adjustment_hash{$sku}) {                
                my $a_qty=$adjustment_hash{$sku};
                unless ($r_qty == $a_qty) {					
                    $error_count++;
                    push(@problem_sku_list,$sku);   
                    if ($verbose) {
                        print "On Record $record, SKU $sku shows $r_qty received but $a_qty went into inventory.<br />";
                    }                    
                }
            } else {
                if ($r_qty) {
                    $error_count++;					
                    push(@problem_sku_list,$sku); 
                    if ($verbose) {
                        print "On $record, SKU $sku shows $r_qty received but nothing went into inventory.<br />";
                    }            
                }
            }
        } 
        #   Finally, check the inventory adjustments against receiving
        foreach my $sku (sort keys(%adjustment_hash)) { 
            my $a_qty=$adjustment_hash{$sku};
            if ($receiving_hash{$sku}) {
                my $r_qty=$receiving_hash{$sku};                
                unless ($r_qty == $a_qty) {				
                    $error_count++;
                    if ($verbose) {
                        print "On Record $record, SKU $sku shows $r_qty received but $a_qty went into inventory.<br />";
                    }                    
                }  
            } else {
                if ($a_qty > 0) {				
                    $error_count++;
                    if ($verbose) {
                        print "On $record, SKU $sku shows $a_qty went into inventory but there are no receiving entries.<br />";
                    }            
                }
            }
        }   
        
        if ($error_count) {
            # Check the problem SKUs against the list of receiving records so we can show the ones likely to be suspect (Remember, we only know which SKUs have discrepancies
            # but not which receiving records.
            my %rectransnum_hash;
            foreach my $suspect_sku (@problem_sku_list) {
                $query="
                    select 
                        rd.rectransnum
                    from
                        receiving_dtl rd
						join receiving r on r.rectransnum = rd.rectransnum
                    where                
                        rd.modtime like '$record%'
                    and
                        rd.plunum like '$suspect_sku'
					and
						r.txnStatus like 'R'
 
                ";        
                $sth = $dbh->prepare($query);
                $sth->execute();
                while (my @tmp=$sth->fetchrow_array()) {
                    
                    $rectransnum_hash{$tmp[0]}=1;
                }
            }
       
            print $q->p( "!!!!!!   Found $error_count errors checking receiving on $record   !!!!!!!!");
            foreach my $rectransnum (sort keys(%rectransnum_hash)) {
                print ">>>>>>Check receiving record ";                
                print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=CHECKREC2&rectransnum=$rectransnum "}, "$rectransnum"); 
                print "<br /><br />";
            }
     
            print "\n";
            $total_error_count++;
        }
        
    }
    print $q->p("Found $total_error_count dates with errors checking receiving for the specified date range.");
    $dbh->disconnect;   
    StandardFooter();        
} elsif ($report eq "CHECKRECRANGE_orig") {   
    my $rectransnum= ($q->param("rectransnum"))[0];   
    my $startdate= ($q->param("startdate"))[0];   
    my $enddate= ($q->param("enddate"))[0];  
    my @tmp;
    my %receiving_list_hash;
    print $q->h3("Check Receiving records from $startdate to $enddate"); 
    # Collect a list of all receiving records between these dates
    $query=dequote(<< "    ENDQUERY");    
    select 
        rectransnum,documentNum
    from
        receiving              
    where 
         receiveDate between '$startdate' and '$enddate 23:59:59'
    and
        txnstatus = 'R'
    ENDQUERY
    
    $sth = $dbh->prepare($query);    
    $sth->execute();        
    while (@tmp = $sth->fetchrow_array) {   
        $receiving_list_hash{"$tmp[0]"}+= "$tmp[1]";
    }     
    # Check each record for entries in the inventory adjust table
    my @header=("Rectransnum","Issue");
    my @rec_issues_list=\@header;
    foreach my $key (sort keys(%receiving_list_hash)) {
        my $found_sku_error=0;
        my $found_qty_error=0;
        my $rectransnum=$key;
        my %received_hash;
        my $receiveDate;
        # Find the SKU and Qty received on this rectransnum
        $query=dequote(<< "        ENDQUERY");    
        select 
            rd.plunum,rd.qtyReceived,dateFormat(r.receiveDate,'yyyy-mm-dd')
        from
            receiving_dtl rd
            join receiving r on r.storenum = rd.storenum and r.rectransnum = rd.rectransnum 
            join plu p on p.plunum=rd.plunum
        where 
             r.rectransnum = $rectransnum
        and
            rd.qtyReceived > 0
        and
            p.invcontrolled = 'Y'
        ENDQUERY
        
        $sth = $dbh->prepare($query);    
        $sth->execute();        
        while (@tmp = $sth->fetchrow_array) {         
            $received_hash{"$tmp[0]"}+= "$tmp[1]";
            $receiveDate=$tmp[2];            
        }         
        
        #    Everything in the received_hash should be in the inventory_adjustment table
        foreach my $SKU (sort keys(%received_hash)) {        
            # See if there is a matching entry in the inventory_adjustment table;
            ###
            my $rec_qty=$received_hash{$SKU};
            my $found_sku=0;
            my $found_qty=0;
            $query=dequote(<< "            ENDQUERY");           
            select 
                qty
            from
                inventory_adjustment              
            where 
                 adjustDate between '$startdate' and '$enddate 23:59:59'    
            and
                functionid = 3
            and
                plunum = '$SKU'
            ENDQUERY
                        
            $sth = $dbh->prepare($query);    
            $sth->execute();        
            while (@tmp = $sth->fetchrow_array) {   
                # If we found anything, we found the sku so set that variable
                $found_sku=1;
                my $qty=$tmp[0];
                #
                if ($rec_qty == $qty) {
                    # There is, unfortunately, no way to positively
                    # connect the receiving entry with the adjustment entry
                    # but if the date is the same, the sku is the same, and the qty
                    # is the same, chances are, it is the same.
                    $found_qty=1;
                }                
            }                   

            # Verify that the qty is the same in the two tables
            if ($found_sku) {
                unless ($found_qty) {
                    # It could be that the SKU is in the rec doc twice and so we added the qty of each together
                    # Add the qty of the adjustments together and see if they match then                    
                    $query=dequote(<< "                    ENDQUERY");           
                    select 
                        sum(qty)
                    from
                        inventory_adjustment              
                    where 
                         adjustDate between '$receiveDate' and '$receiveDate 23:59:59'    
                    and
                        functionid = 3
                    and
                        plunum = '$SKU'
                    ENDQUERY
                    
                    $sth = $dbh->prepare($query);    
                    $sth->execute(); 
                    while (@tmp = $sth->fetchrow_array) {   
                        # If we found anything, we found the sku so set that variable
                        $found_sku=1;
                        my $qty=$tmp[0];                        
                        if ($rec_qty == $qty) {
                            $found_qty=1;
                        }               
                    }     
                    unless ($found_qty) {                        
                        $found_qty_error++;
                    }                   
                }                
            } else {          
                $found_sku_error++;                
            }
        }       
        
        my @array;
        if ($found_sku_error) {
            @array=("$rectransnum","SKU errors");
            push(@rec_issues_list,\@array);    
        } elsif ($found_qty_error) {
            @array=("$rectransnum","Qty errors");
            push(@rec_issues_list,\@array);    
        } 
            
    }    
    if (recordCount(\@rec_issues_list)) { 
        print $q->h4("Receiving issues found:");
         my @links = ( "$scriptname?report=CHECKREC2&rectransnum=".'$_' ); 
        TTV::cgiutilities::printResult_arrayref2(\@rec_issues_list, \@links, '');
        
    } else {
        print "No receiving issues found<br />";
        
    }    
    StandardFooter();  
    
# *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
# Report: LISTORDERS
# Description:
# *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
} elsif ($report eq "LISTORDERS") {
    
    print $q->h3("Open Villages Purchase Orders"); 
    
    my %order_hash=(
        'RecDoc' => 'r.documentNum',
        'PO' => 'r.poNum',
        'Order' => '',
        'Date' => 'Date',
        'Items' => 'Items',
        'Units' => 'Units',
        'Cost' => 'Cost',
 
        'Status' => 'r.txnStatus'
    );
 
    my $order = trim(($q->param("order"))[0]);        
    my $sortfield = trim(($q->param("sort"))[0]);  
    
    unless ($sortfield) {
        $sortfield="RecDoc";
    }
    
    $sortfield=$order_hash{$sortfield};
    ## adding an order field to be ASC or DESC -kdg ###
       
    if (($order eq "") || ($order eq "DESC")) {
        ## Toggle the order
        $order = "ASC";
    } else {
        $order = "DESC";
    }             

    my %ordstat = ( -1 => "Unknown to Akron",
                   0 => "Not Ready for WMS",
                   1 => "Ready for WMS",
                   2 => "Sent to WMS",
                   3 => "Picked Ready to Ship",
                   4 => "Shipped Ready to Invoice",
                   5 => "Picked ON HOLD",
                   6 => "Shipped ON HOLD",
                   7 => "Shipped & Invoiced"
                );
    # Query the elderRecv table to get info on orders that Akron thinks are still open
    my %elderRecv_hash;
    my $query = dequote(<< "    ENDQUERY");
    select
        ponum,
        sono,
        ordstat,
        invno,
        invdate,
        items,
        qtyShp,
        cost
    from
        elderRecv            

    ENDQUERY
    
    my $tbl_ref = execQuery_arrayref($dbh, $query);  
    if (recordCount($tbl_ref)) {  
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $ponum =   trim($$thisrow[0]);
            my $sono  =   trim($$thisrow[1]);
            my $ordstat =   trim($$thisrow[2]);
            my $invno =   trim($$thisrow[3]);
            my $invdate =   trim($$thisrow[4]);
            my $items  =   trim($$thisrow[5]);
            my $qtyShp  =   trim($$thisrow[5]);
            my $cost  =   trim($$thisrow[5]);
            $elderRecv_hash{$ponum}=$thisrow;
        }
    }
    # Query the   orders      
    my @header=("RecDoc","PO","Order","Date","Items","Units","Cost","Status","AkronStatus");
    my @array_to_print=\@header;
 
    $query = dequote(<< "    ENDQUERY");
    select
        r.documentNum, 
        r.poNum, 
        r.ReceiveDate as "Date", 
        count(*) as "Items",
        sum(rd.qtyOrdered) as "Units", 
        sum(rd.qtyOrdered*rd.cost) as "Cost",
        r.txnStatus
    from
        Receiving r
        join Receiving_dtl rd on r.rectransnum = rd.rectransnum
    where
        r.txnstatus in ('I', 'P')
    and 
        r.VendorNum in ('1', '01')
    group by 
        r.documentNum, r.poNum, r.ReceiveDate, r.txnStatus
    order by 
        $sortfield $order
    ENDQUERY
 
    $tbl_ref = execQuery_arrayref($dbh, $query);  
    my $newsortURL = "$scriptname?report=LISTORDERS&order=$order&sort=\$_";
    my @headerlinks = ($newsortURL, $newsortURL, '', $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);    
    if (recordCount($tbl_ref)) { 
        for my $datarows (1 .. $#$tbl_ref) {  
            my $thisrow = @$tbl_ref[$datarows];
            my $RecDoc =   trim($$thisrow[0]);
            my $PO  =   trim($$thisrow[1]);
            my $Date =   trim($$thisrow[2]);
            my $Items =   trim($$thisrow[3]);
            my $Units =   trim($$thisrow[4]);
            my $Cost  =   trim($$thisrow[5]);
            my $Status  =   trim($$thisrow[6]);
            my $Order="unknown";
            my $AkronStatus="unknown";
            if ($elderRecv_hash{$PO}) {
                $Order  =   $elderRecv_hash{$PO}->[1];
                $AkronStatus  =   $elderRecv_hash{$PO}->[2];
            }
            my @array=("$RecDoc","$PO","$Order","$Date","$Items","$Units","$Cost","$Status","$AkronStatus");
            push(@array_to_print,\@array);
        }
        my @format = (      
            "",
            "",
            "",  "",
            {'align' => 'Right', 'num' => ''}, 
            {'align' => 'Right', 'num' => ''},  
            {'align' => 'Right', 'currency' => ''},  
            {'align' => 'Center' }, 
            {'align' => 'Center' },                 
            "",            
        );              
        my @links = ('/cgi-bin/i_orders.pl?report=ORDERINFO&sono=$_' );                
        printResult_arrayref2(\@array_to_print, \@links, \@format, \@headerlinks); 
    } else {
        print $q->h3("No issued or pending receiving records for Villages orders.");
    }              
    StandardFooter();
     
# *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
# Report: ORDERINFO
# Description:
# *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
} elsif ($report eq "ORDERINFO") {

    print $q->h1("Order Information");
    my $sono = trim(($q->param("sono"))[0]);
    my $PO = trim(($q->param("PO"))[0]);
    my $sortfield = trim(($q->param("sort"))[0]);
    my $whereclause;
    if ($sono) {
        $whereclause="where sono = '$sono'";
    } elsif ($PO) {
        $whereclause="where ponum = '$PO'";
    } else {
        print $q->p("Failed to get the needed information from Akron about this order");
        StandardFooter();
        exit;
    }
      $sortfield = "item" unless $sortfield;

    my $query = dequote(<< "    ENDQUERY");
    select
    sono, ponum, ordstat, invno,
    convert(varchar(10),invdate,21) as invdate,
    items, qtyShp, cost
    from elderRecv
    $whereclause
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    my ($ponum, $ordstat, $invno, $invdate, $total_items, $total_qty, $total_cost) = ('', 0, '', '', 0, 0, 0);
    if (recordCount($tbl_ref) > 0) {
        my $thisrow = @$tbl_ref[1];
        $sono =    trim($$thisrow[0]);
        $ponum =   trim($$thisrow[1]);
        $ordstat = trim($$thisrow[2]);
        $invno   = trim($$thisrow[3]);
        $invdate = trim($$thisrow[4]);
        $total_items = trim($$thisrow[5]);
        $total_qty   = trim($$thisrow[6]);
        $total_cost  = trim($$thisrow[7]);
    }

    print "\n\n", $q->start_table({-valign=>'top', -cellspacing=>0, -cols=>2}), "\n";
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('Order Number:')),
       $q->td({-nowrap=>undef, -align=>'left'}, $sono));
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('PO Number:')),
       $q->td({-nowrap=>undef, -align=>'left'}, $ponum));
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('Invoice Number:')),
       $q->td({-nowrap=>undef, -align=>'left'}, $invno));
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('Invoice Date:')),
       $q->td({-nowrap=>undef, -align=>'left'}, $invdate));
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('Total Items:')),
       $q->td({-nowrap=>undef, -align=>'left'}, formatNumeric($total_items, 0, 0)));
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('Total Qty:')),
       $q->td({-nowrap=>undef, -align=>'left'}, formatNumeric($total_qty, 0, 0)));
    print $q->Tr($q->td({-nowrap=>undef, -align=>'right'}, $q->b('Total Cost:')),
       $q->td({-nowrap=>undef, -align=>'left'}, formatCurrency($total_cost)));
    print $q->end_table(),"\n";

    print $q->h3("Order Detail");

    $query = dequote(<< "    ENDQUERY");
    select
     rd.qtyord, rd.qtyshp, rd.item, rd.description,
     rd.custmemo, rd.cost, rd.extcost, rd.price,
     p.retailprice as "StorePrice"
    from
     elderRecvDtl rd
     join plu p on rd.item=p.plunum
    where sono = '$sono'
    order by $sortfield
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);
    my $tbl_ref2 = ();
    for my $datarows (0 .. $#$tbl_ref) {
        my $rref = @$tbl_ref[$datarows];
        my ($qtyord, $qtyshp, $item, $descrip, $custmemo, $cost,
         $extcost, $price, $storeprice) = trim(@$rref);

        my $ref = [];
        if ($datarows == 0) {
            # table header record
            $ref = [$qtyord, $qtyshp, $item, $descrip, $cost,
                    $extcost, $price, $storeprice];
        } else {
        # table detail records.
        my $DisplayStorePrice = '&nbsp;';
        #$descrip = substr($descrip,0,40) if (length($descrip) > 40);
        #$custmemo = substr($custmemo,0,54) if (length($custmemo) > 54);
        $DisplayStorePrice = formatCurrency($storeprice) if ($storeprice);
        $DisplayStorePrice = "<b>".$DisplayStorePrice."</b>" if ($storeprice != $price);

        $ref = [$qtyord, $qtyshp, $item, $descrip."<br /><i>".$custmemo."</i>", $cost, $extcost, $price, $DisplayStorePrice];
        }
        push @$tbl_ref2, $ref;
    }

    my @format = ({'align' => 'Right', 'num' => '.0'}, {'align' => 'Right', 'num' => '.0'}, '', '',
                {'align' => 'Right', 'currency' => ''}, {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''});

    my $newsortURL = "/cgi-bin/i_orders.pl?report=ORDERINFO&sono=$sono&sort=\$_";
    my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);

    if (recordCount($tbl_ref2)) { printResult_arrayref2($tbl_ref2, '', \@format, \@headerlinks); }
    else { print $q->b("no records found<br />\n"); }

    StandardFooter();
   
        
} elsif ($report eq "POSTATUS") {    
    print $q->h3("Check Purchase Order Status");
    my @header = (
        "Potransnum",
        "PO Number",
        "PO Status",  
        "Rec PO Number",
        "Rec Status"
    );    
    my @header1 = (
        "PO Number",
        "Status",        
    );
    my @array_to_print=\@header;
    my @tmp_array_to_print=\@header1;
    
    my %po_pending_hash;    
    my %po_issued_hash;
    
    my $storenum = getStoreNum($dbh);   
    my @tmp;
     
    #################################
    #   Check purchase orders which are Pending
    #################################
	$query = dequote(<< "	EOQ");	
	select 
        poTransnum,poNum,txnStatus
    from 
        purchase_order 
	where 
        poNum not like 'back%' 
        and txnStatus like 'P'
        and storenum = $storenum 
        order by ponum
	EOQ
  
	$sth = $dbh->prepare($query);
	$sth->execute();
    my $count=0;
    my $current_po;
    my @lost_po;
	my $lost_po_count=0;
	while (@tmp=$sth->fetchrow_array()) {	
        my $potransnum=$tmp[0];
        my $ponum=$tmp[1];
        my $status=$tmp[2];        
        # This PO is pending        
        $po_pending_hash{$potransnum}=$ponum;
                    
        if ("$current_po" eq "$tmp[0]") {
            my @array = (
                "$current_po",
                "$status"
            );
            push(@tmp_array_to_print,\@array);
            $count++;
        }
        $current_po = $tmp[0];
	}  
    if ($count) {
        print "There were more than one purchase orders using the same number<br />";
        TTV::cgiutilities::printResult_arrayref2(\@tmp_array_to_print, '', ''); 
        # Reset the array as it may be reused later.
        @tmp_array_to_print=\@header1;
    }
    
    foreach my $potransnum (keys(%po_pending_hash)) {
        my $ponum=$po_pending_hash{$potransnum};
        # Check the status of the associated receiving record
        $query = dequote(<< "        EOQ");	
        select 
            poNum,txnStatus 
        from 
            receiving 
        where 
            poNum like '$ponum'  
        EOQ
      
        $sth = $dbh->prepare($query);
        $sth->execute();
        $count=0;
        my $pending=0;
        my $found=0;
        my $rec_status;

        while (@tmp=$sth->fetchrow_array()) {
            $count++;
            $found=1;
            my $rec_po=$tmp[0];
            #$potransnum.="$tmp[0] ";  
            $rec_status="$tmp[1] ";            
            if ($rec_status =~ /P/i){
                $pending=1;                
            } 
            my @array = (
                "$potransnum",
                "$ponum",         
                "$rec_status"
            );
            push(@tmp_array_to_print,\@array);
        }
        if ($found) {        
            unless ($pending) {   
                my @array = (
                    "$ponum",
                    "pending",
                    "$ponum",
                    "$rec_status"
                );
                push(@array_to_print,\@array);
            }
        } else {
            push(@lost_po,$ponum); 
			$lost_po_count++;					
        }
        if ($count > 1) {
            print "Note: There were $count receiving records for po $ponum<br />";
            TTV::cgiutilities::printResult_arrayref2(\@tmp_array_to_print, '', ''); 
            # Reset the array as it may be reused later.
            @tmp_array_to_print=\@header1;
        }            
    }
    #################################
    #   Check purchase orders which are Issued
    #################################
    # Reset this array
	my %po_status_hash=();
    @array_to_print=\@header;  
    @tmp_array_to_print=\@header1;
    my $found_poNum;
    
	$query = dequote(<< "	EOQ");	
	select 
        poTransnum,poNum,txnStatus
    from 
        purchase_order 
	where 
        poNum not like 'back%' 
        and (txnStatus like 'I' or txnStatus like 'P')
        and storenum = $storenum 
        order by ponum
	EOQ
 
  
	$sth = $dbh->prepare($query);
	$sth->execute();
    $count=0;
    $current_po="";
	while (@tmp=$sth->fetchrow_array()) {	        
        # This PO is issued
        my $potransnum=$tmp[0];
        my $ponum=$tmp[1];
        my $status=$tmp[2];        
        
        # The potransnum is used as a key because it is unique.  The ponum may not be unique
        $po_issued_hash{$potransnum}=$ponum;  
		$po_status_hash{$potransnum}=$status; 
		
      
        if ("$current_po" eq "$ponum") {
            my @array = (
                "$current_po",
                "$status"
            );
            push(@tmp_array_to_print,\@array);
            $count++;
        }
        $current_po = $tmp[0];
	}  
    if ($count) {
        print "There were more than one purchase orders using the same number<br />";
        TTV::cgiutilities::printResult_arrayref2(\@tmp_array_to_print, '', ''); 
        # Reset the array as it may be reused later.
        @tmp_array_to_print=\@header1;
    }    

    foreach my $potransnum (keys(%po_issued_hash)) {
        my $ponum=$po_issued_hash{$potransnum};
        # Check the status of the associated receiving record
        $query = dequote(<< "        EOQ");	
        select 
            rectransnum,txnStatus,poNum 
        from 
            receiving r
			
        where 
            poNum like '$ponum' 
            or
            poNum like '$ponum%L' 
        EOQ
      
        $sth = $dbh->prepare($query);
        $sth->execute();
        $count=0;
        my $ok=0;
        my $found=0;
        my $rec_status;
        my $rectransnum;
     
        while (@tmp=$sth->fetchrow_array()) {
            $count++;
            $found=1;
            $rectransnum.="$tmp[0] ";
            $rec_status="$tmp[1] ";  
            $found_poNum=$tmp[2];
            if ($found_poNum =~ /$ponum.*L/) {
                my @t=split(/\s+/,$found_poNum);
                unless ($t[0] eq $ponum) {
                    next;
                }                
            }
            # If the receiving record is Issued or Pending, that is acceptable
            if ($rec_status =~ /I/i){
                $ok=1;                
            } elsif ($rec_status =~ /P/i) {
                $ok=1;     
            }
            my @array = (
                "$ponum",
                "$rec_status"
            );
            push(@tmp_array_to_print,\@array);            
        }
        if ($found) {        
            unless ($ok) {     
                my @array = (
                    "$potransnum",
                    "$ponum",
					"$po_status_hash{$potransnum}",                    
                    "$found_poNum",
                    "$rec_status"
                );
                push(@array_to_print,\@array);     
            }
        } else {
            push(@lost_po,$ponum);
			$lost_po_count++;	
        }
        if ($count > 1) {
            print "Note: There were $count receiving records for po $ponum<br />";
            TTV::cgiutilities::printResult_arrayref2(\@tmp_array_to_print, '', '');  
        }           
    }  
    my $tbl_ref=\@array_to_print;
    if (recordCount($tbl_ref)) {   
        print $q->p("These purchase orders have a status which does not agree with that of the associated receiving record.");
        my @links = ( "$scriptname?&report=PODTL&potransnum=".'$_' ); 
        my @format = ('', '',{'align' => 'Center'},{'align' => 'Center'});
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, \@links, \@format);    
    } else {
        print $q->p("No purchase orders with mis-matched status");
    }
    if ($lost_po_count) {
        print $q->p("Click on the po below to try to find the associated receiving record.");
    }
    foreach my $po (@lost_po) {
        print $q->a("Could not find receiving record for: ");
        print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=LOSTPO&ponum=$po "}, "$po");  
        print "<br />";        
    }
    StandardFooter();   
} elsif ($report eq "LOSTPO") {   
    my $ponum = trim(($q->param("ponum"))[0]);  
    # Try to find a receiving record associated with $ponum
    # First get info about this po
    
    $query = dequote(<< "    ENDQUERY");
    select 
        po.potransnum, po.transnum, po.txnStatus, convert(varchar(19),po.orderDate,21),  
        po.vendorNum
        
    from 
        Purchase_order po
 
    where 
        po.poNum=$ponum
    ENDQUERY
    
    $tbl_ref = execQuery_arrayref($dbh, $query);
    my $potransnum;
    my $transnum;
    my $orderDate;
    my $vendor;
    if (recordCount($tbl_ref)) {
        print $q->p("Purchase Order Info for PO $ponum");
        my @links = ( "/cgi-bin/i_stevetools.pl?report=PODTL&potransnum=".'$_' ); 
        printResult_arrayref2($tbl_ref, \@links);  
        print $q->a("Click on the potransnum to see details."),"<br />";   
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows]; 
            $potransnum=$$thisrow[0];
            $transnum=$$thisrow[1];
            $orderDate=substr($$thisrow[3],0,10);
            $vendor=$$thisrow[4];                        
        }   
        # Find some details about this purchase order
        $query = dequote(<< "        ENDQUERY");
        select 
            pod.plunum, 
            pod.qty             
        from 
            purchase_order_dtl pod
            join purchase_order po on po.potransnum = pod.potransnum
        where 
            po.poNum='$ponum'         
        ENDQUERY
        
        $tbl_ref = execQuery_arrayref($dbh, $query);    
        my %po_hash;

        if (recordCount($tbl_ref)) {    
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
                my $plunum = trim($$thisrow[0]);
                my $qty = trim($$thisrow[1]);
                $po_hash{$plunum}+=$qty;
            }
        }
        # Find all receiving records which might match this one.
        $query = dequote(<< "        ENDQUERY");
        select 
            r.rectransnum, r.transnum, r.txnStatus, convert(varchar(19),r.txnDate,21),  
            r.vendorNum,
            r.documentNum,
            r.poNum
        from 
            receiving r      
        where 
            r.vendorNum=$vendor
        and
            r.txnDate > '$orderDate'
        ENDQUERY
        
        $tbl_ref = execQuery_arrayref($dbh, $query);  
        my $output_ref;
        if (recordCount($tbl_ref)) {
            push(@$output_ref,$$tbl_ref[0]);    # Push the table header on
            ###                      
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];                
                my $rectransnum = trim($$thisrow[0]);
                my %rec_hash;
                my $match=0;
                my $miss=0;
                # Check to see if the items in the original PO are even close to this receiving document
                $query="select plunum, qtyordered from receiving_dtl where rectransnum = '$rectransnum'";
                my $tbl_ref2 = execQuery_arrayref($dbh, $query);  
                if (recordCount($tbl_ref2)) { 
                    for my $datarows (1 .. $#$tbl_ref2) {
                        my $thisrow = @$tbl_ref2[$datarows];
                        my $plunum=$$thisrow[0];
                        my $qtyordered=$$thisrow[1];
                        $rec_hash{$plunum}+=$qtyordered;
                    }
                }
                foreach my $sku (sort keys(%po_hash)) {
                    if ($rec_hash{$sku}) {
                        $match++;
                        if ($po_hash{$sku} == $rec_hash{$sku}) {
                            $match++;
                        } else {
                            $miss++;
                        }
                    } else {
                        $miss++;
                    }
                }
                print $q->p("Receiving: $rectransnum Misses: $miss Matches: $match");                
                if ($match > $miss) {                    
                    push(@$output_ref,$$tbl_ref[$datarows]);    # Push the table row on
                }
            }                   
            ###
            if (recordCount($output_ref)) {
                print $q->p("Possibly matching receiving records for PO $ponum");
                my @links = ( "/cgi-bin/i_stevetools.pl?report=RECDOCDTL&rectransnum=".'$_' ); 
                printResult_arrayref2($output_ref, \@links); 
                print $q->a("Click on the rectransnum to see details."),"<br />";                
            } else {
                print $q->p("Found no good matches in receiving records for PO $ponum");
            }      
        } else {
            print $q->p("Found no possibly matching receiving records for PO $ponum");
        }
        ###
    } else {
        print $q->h4("No header for purchase order $ponum found!");
    }
    StandardFooter();       
} elsif ($report eq "PODTL") {   
    my $potransnum = trim(($q->param("potransnum"))[0]);
    
    my $recstatus;
    my $ponumber;
    my %order_hash;
    my %receive_hash;
    print $q->h3("Purchase Order $potransnum");
    # First give the header info
    $query = dequote(<< "    ENDQUERY");
    select 
        po.storenum, po.potransnum, po.poNum, po.txnStatus, convert(varchar(19),po.txnDate,21),  
        vendorNum as VendorId ,v.vendorName as Vendor,po.origin as Origin
    from 
        Purchase_order po
    join 
        Vendor v on po.vendorNum=v.vendorid
    where 
        po.potransnum=$potransnum
    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    # Need to Get the detail for the matching receiving record so get the poNum
    my @poNum;
    for my $datarows (1 .. $#$tbl_ref)
    {
        my $thisrow = @$tbl_ref[$datarows];
        my $po = trim($$thisrow[2]);
        push(@poNum,$po);
    }
    if (recordCount($tbl_ref)) {
        printResult_arrayref2($tbl_ref, '', '', '', {'tableid' => 'tableid1'});         
    } else {
        print $q->h4("No header for purchase order $potransnum found!");
    }
    print "<br />\n";
    print $q->h3("Detail for purchase order $potransnum");        
    $query = "select 
    itemCnt as Item, plunum as SKU, qty,cost,modtime
    from purchase_order_dtl where potransnum = $potransnum
    order by plunum";
    $tbl_ref = execQuery_arrayref($dbh, $query); 
    if (recordCount($tbl_ref)) {
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $SKU = trim($$thisrow[1]); 
            my $QTY = trim($$thisrow[2]); 
            $order_hash{$SKU}=$QTY;			
        }
        
        printResult_arrayref2($tbl_ref, '', '', '', {'tableid' => 'tableid1'});         
    } else {
        print $q->h4("No detail for purchase order $potransnum found!");
    }
    foreach my $po (@poNum) {
        $ponumber=$po;        
        my $rectransnum;
        ##
        $query = dequote(<< "        ENDQUERY");
        select 
            r.storenum,r.rectransnum, r.documentNum, r.txnStatus, r.poNum, 
            convert(varchar(19),r.txnDate,21),  r.vendorNum as VendorId,v.vendorname as Vendor
        from 
            receiving r
            join Vendor v on v.vendorid=r.vendorNum
        where
			r.poNum like '$po%'		
 
        ENDQUERY
        
		#print $q->pre($query);
        $tbl_ref = execQuery_arrayref($dbh, $query);
		my @header=(
			"storenum",
			"rectransnum",
			"documentNum",
			"txnStatus",
			"poNum",
			"txnDate",
			"VendorId",
			"Vendor"
		);
		my @array_to_print=\@header;
		

        for my $datarows (1 .. $#$tbl_ref)
        {
            my $thisrow = @$tbl_ref[$datarows];
            $rectransnum = trim($$thisrow[1]); 
            $recstatus=trim($$thisrow[3]);
            my $found_po=trim($$thisrow[4]);        
            my @t=split(/\s+/,$found_po);
            next unless ($t[0] eq $po);
			push(@array_to_print,$thisrow);
        }
        if (recordCount(\@array_to_print)) {
            print $q->h3("Matching Receiving Transaction with PO $po");
            printResult_arrayref2(\@array_to_print, '', '', '', {'tableid' => 'tableid1'});         
        } else {
            print $q->h3("No header for Receiving Transaction with PO $po found!");
        } 		
=pod		
        if (recordCount($tbl_ref)) {
            print $q->h3("Matching Receiving Transaction with PO $po");
            printResult_arrayref2($tbl_ref, '', '', '', {'tableid' => 'tableid1'});         
        } else {
            print $q->h3("No header for Receiving Transaction with PO $po found!");
        } 
=cut		
        print "<br />\n";
        
		if ($rectransnum) {
			print $q->h3("Detail for Receiving Transaction with PO $po rectransnum $rectransnum");
			$query = "
			select 
				itemCnt as Item, 
				plunum as SKU, 
				qtyOrdered,
				qtyReceived,
				shortover,
				reason,
				backorder,
				cost,
				retailPrice,
				modtime
			from 
				receiving_dtl 
			where 
				rectransnum = $rectransnum
			order by 
				plunum";
			$tbl_ref = execQuery_arrayref($dbh, $query); 
			if (recordCount($tbl_ref)) {
				for my $datarows (1 .. $#$tbl_ref) {
					my $thisrow = @$tbl_ref[$datarows];
					my $SKU = trim($$thisrow[1]); 
					my $QTY = trim($$thisrow[2]); 
					$receive_hash{$SKU}=$QTY;					
				}        
				printResult_arrayref2($tbl_ref, '', '', '', {'tableid' => 'tableid1'});         
			} else {
				print $q->h4("No detail for Receiving Document $rectransnum found!");
			}
		}
        ##
    }
    if ($#poNum < 1) {
        # Compare the order hash with the receive hash to make certain it looks like the correct order
        my $match=0;
        my $error_count=0;
        foreach my $sku (sort keys(%order_hash)) {			
            unless (defined($receive_hash{$sku})) {
                print "SKU: $sku is listed in the purchase order but is not on the receiving document.<br />";
                $error_count++;
            } else {
				$match++;
			}
        }
=pod		
        if ($match) {
            $match=0;
            foreach my $sku (sort keys(%receive_hash)) {
                unless (defined($order_hash{$sku})) {   
                    print "SKU: $sku is listed in the receiving document but is not on the purchase order.<br />";
                    $error_count++;                    
                }
            }        
        }
=cut 
        print $q->p("Found $error_count discrepancies and $match matches between purchase order and receiving record");
        $match=0;
        $error_count=0;		
		foreach my $sku (sort keys(%receive_hash)) {
			unless (defined($order_hash{$sku})) {   
				print "SKU: $sku is listed in the receiving document but is not on the purchase order.<br />";
				$error_count++;                    
			} else {
				$match++;
			}
		}  
		print $q->p("Found $error_count discrepancies and $match matches between receiving record and purchase order");		
        
        if ($recstatus eq "R")  {
            #Give option to change po status:
            
            print $q->p("Click on 'Receive' to change the status of the PO to 'received'");
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"STATUSMOD"});
            print $q->input({-type=>"hidden", -name=>"potransnum", -value=>"$potransnum"});
            print $q->input({-type=>"hidden", -name=>"ponumber", -value=>"$ponumber"});
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";

            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                $q->td({-class=>'tabledatanb', -align=>'left'}),
                $q->submit({-accesskey=>'R', -value=>"   Receive!   "}))
                          );
            
            print $q->end_table(),"\n";

            print $q->end_form(), "\n";     
        } else {
            print $q->p("Status of Receiving Record is not Received");
            print $q->p("Cannot set Purchase Order to Received");
        }
    } else {
        print $q->p("Found more than one Receiving Record associated with purchase order");
    }
    
    StandardFooter();
} elsif ($report eq "STATUSMOD") { 
    my $potransnum= ($q->param("potransnum"))[0];  
    my $ponumber= ($q->param("ponumber"))[0]; 
    print $q->p("Setting potransnum $potransnum PO $ponumber to received...");
    $query = dequote(<< "    ENDQUERY");    
    update 
        purchase_order
    set 
        txnStatus = 'R' 
    where
        potransnum = $potransnum
    ENDQUERY
    
    if ($dbh->do($query)) {
        print $q->p("Successfully set status to Received");
    } else {
        print $q->p("Error attempting to set status to Received");
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n";            
    }      
    
    StandardFooter();
} elsif (($report eq "PROMOVIEW")||($report eq "PROMOVIEW2")) {
    my $validation=0;
    if ($report eq "PROMOVIEW2") {
        print $q->h1("Promotion View & Validation.");
        $validation=1;
    } else {
        print $q->h1("Promotion View.");
    }
  	
    print $q->p("These SKUs are on promotion at this time");
    my %sku_hash;
    my @ltm = localtime();
    my $date_label = sprintf("%04d-%02d-%02d %02d:%02d:%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3], $ltm[2],$ltm[1],$ltm[0]);    
    # Get the ptdid and ptdnum
    $query = dequote(<< "    ENDQUERY"); 
    select 
        pd.Plunum, pd.description, pd.promonum, pd.changeamt, ph.description, ph.effectiveDate, ph.promoExpire, ic.qtyCurrent 
    from 
        promo_dtl pd
        join promo_header ph on pd.promonum = ph.promonum
        left outer join inventory_current ic on pd.plunum = ic.plunum
    where 
        pd.effectivedate < '$date_label'
    and
        pd.promoexpire > '$date_label'
    order by pd.promonum
    ENDQUERY

    my $tbl_ref = execQuery_arrayref($dbh, $query); 
    my @not_working;
    my @problems;
    if (recordCount($tbl_ref)) { 
        if ($validation) {            
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];  
                if ($$thisrow[0]) {
                    my $sku=$$thisrow[0];
                    my $rc=check_sku_on_promo($sku);
                    if ($rc == 0) {
                        push(@not_working,$sku);
                    } elsif ($rc ==2) {
                       push(@problems,$sku);
                    } 
                } else {
                    my $sku="unknown_sku_@$thisrow";
                    push(@problems,$sku);
                }
                
            }
        }
        my @format = ('', '',  '',
            {'align' => 'Left', 'num' => ''},
            '',  '', '',
            {'align' => 'Left', 'num' => ''},
            );
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format);
        if ($#not_working > -1) {
        
            print $q->p("WARNING: These SKUs may not be working!");
            foreach my $s (@not_working) {
                print "--> $s<br />";
            }
        }  
        if ($#problems > -1) {
        
            print $q->p("NOTE: There were problems checking these SKUs:");
            foreach my $s (@problems) {
                print "--> $s<br />";
            }
        }   
        if (($#not_working == -1)&&($#problems == -1)) {
           print $q->p("All SKUs appear to be working");
        }
    } else {
        print $q->p("No SKUs found on promotion at this time");
    }
    $dbh->disconnect; 
  	StandardFooter();  
} elsif ($report eq "PROMOCHECK") {
  	print $q->h1("Promotion Check.");
    my $sku= ($q->param("sku"))[0];
    unless ($sku) {
        print $q->p("Enter a SKU to check");
        print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMOCHECK"});
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));    
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;Enter <u>S</u>KU:\&nbsp"),
            $q->td({-class=>'tabledatanb', -align=>'right'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"sku", -size=>"12"}))
        );            
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
        );
        print $q->end_table(),"\n";
        print $q->end_form(), "\n";
        exit;
    }
    
    my $vendorid;
    my $category;
    my $group;
    my $dept;
    my $uf1;
    my $uf2;
    my $uf3;
    my $uf4;
    my $uf5;
    my $uf6;
    
    $query="
    select 
        p.plunum,p.vendorid, c.catagorynum,d.groupnum,d.deptnum, 
        p.userflagnum1, p.userflagnum2, p.userflagnum3, p.userflagnum4, p.userflagnum5, p.userflagnum6
    from 
        plu p
        left outer join department d on p.deptnum = d.deptnum
        left outer join dept_group dg on d.groupnum = dg.groupnum
        left outer join category c on c.catagorynum = dg.catagorynum
    where
        p.plunum = '$sku'";
	$sth=$dbh->prepare($query);	

    $sth->execute();   

    while (my @tmp=$sth->fetchrow_array()) {      
        $sku="$tmp[0]";
        $vendorid="$tmp[1]";
        $category="$tmp[2]";
        $group="$tmp[3]";
        $dept="$tmp[4]";
        $uf1="$tmp[5]";
        $uf2="$tmp[6]";
        $uf3="$tmp[7]";
        $uf4="$tmp[8]";
        $uf5="$tmp[9]";
        $uf6="$tmp[10]";
    }
        
    # If the value is not set, set it to null
    unless ($sku) {
        print $q->p("Error: SKU number undefined");
        StandardFooter();
        exit;
    }
    unless ($vendorid) {
        $vendorid="null";
    }    
    unless ($category) {
        $category="null";
    }    
    unless ($group) {
        $group="null";
    }    
    unless ($dept) {
        $dept="null";
    }    
    unless ($uf1) {
        $uf1="null";
    }    
    unless ($uf2) {
        $uf2="null";
    }    
    unless ($uf3) {
        $uf3="null";
    }        
    unless ($uf4) {
        $uf4="null";
    }   
    unless ($uf5) {
        $uf5="null";
    }   
    unless ($uf6) {
        $uf6="null";
    }       
    $query = "call dba.bosdb_hunt_promo(\@as_skuNum=$sku, \@as_vendor=$vendorid, \@ai_category=$category, ".
          "\@ai_group=$group, \@ai_deptNum=$dept, ".
          "\@as_userFlag1=$uf1, ".
          "\@as_userFlag2=$uf2, ".
          "\@as_userFlag3=$uf3, ".
          "\@as_userFlag4=$uf4, ".
          "\@as_userFlag5=$uf5, ".
          "\@as_userFlag6=$uf6, ".
          "); commit; ";

    ElderLog($logfile, indent(1).$query);

    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        printResult_arrayref2($tbl_ref, '', '');

    } else { 
        print $q->b("SKU number $sku does not appear to be on promotion<br />\n"); 
    }
    
  	StandardFooter(); 
} elsif ($report eq "DETAILVAL") {
  	print $q->h1("Detailed Store Valuation.");  
    my ($cat1,$cat2,$cat3,$cat4);
    my $whereclause;    
    my $run_query=0;    # Don't permit the final query to run until we are ready
    
    $cat1= ($q->param("cb_1"))[0];
    $cat2= ($q->param("cb_2"))[0];
    $cat3= ($q->param("cb_3"))[0];
    $cat4= ($q->param("cb_4"))[0];
    
    if (($cat1 eq '') && ($cat2 eq '')&&($cat3 eq '') && ($cat4 eq '')) {        
        # No categories are defined.  Need to find them
        my %category_hash;    
        $query="
        select 
            catagoryNum,description 
        from
            category
        ";
        
        $sth=$dbh->prepare($query);	

        $sth->execute();   

        while (my @tmp=$sth->fetchrow_array()) {           
            $category_hash{$tmp[0]}=$tmp[1];
        }
        # If we have more than one category, query which categories to include
        if ((keys %category_hash) > 1) {
            
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"DETAILVAL"});
            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selection Criteria")));

         
            foreach my $key (sort keys(%category_hash)) {                
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                           $q->input({-type=>"checkbox", -name=>"cb_$key"}) .
                           $q->font({-size=>-1}, "Include Catagory $category_hash{$key}")
                          ));
            }
              
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                    $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                    );


            print $q->end_form(), "\n";
            print $q->end_table(),"\n";    
        } else {
            # There is only one category, permit the query to run
            $run_query=1;
        }
    } else {        
        # Category has been specified.  Now we can do the query

        my @category_list;
        if ($cat1){
            push(@category_list,"1");
        }
        if ($cat2){
            push(@category_list,"2");
        }
        if ($cat3){
            push(@category_list,"3");
        }
        if ($cat4){
            push(@category_list,"4");
        }
        if ($#category_list > -1) {
            # A category was specified.
            print $q->p("Categories specified:");
            my %category_hash;
            $query="
            select 
                catagoryNum,description 
            from
                category
            ";
            
            $sth=$dbh->prepare($query);	

            $sth->execute();   

            while (my @tmp=$sth->fetchrow_array()) {           
                $category_hash{$tmp[0]}=$tmp[1];
            }            
            
            my $category;
            my $sep;
            foreach my $c (@category_list) {
                $category.="$sep $c";
                $sep=",";
                print "  - $category_hash{$c}<br />";
            }
            print $q->p();
            $whereclause="where p.deptnum in 
                (select deptnum from Department where groupNum in 
                    (select groupNum from Dept_Group where catagorynum in ($category)))";
            # Enable the query
            $run_query=1;
        }
    }
    if ($run_query) {
        $query="
        select 
            ic.plunum, p.deptnum, p.pludesc, ic.qtycurrent, p.cost, (p.cost * ic.qtycurrent) as ExtCost
        from 
            plu p             
            left outer join inventory_current ic on ic.plunum = p.plunum
            $whereclause

        ";
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            my @format = ('', '',  '',
                        {'align' => 'Right', 'num' => ''},
                        {'align' => 'Right', 'currency' => ''},
                        {'align' => 'Right', 'currency' => ''},
                        );
            my @fieldsum = ('<b>Total:</b>','', '', '', '', 0);
            
            for my $datarows (1 .. $#$tbl_ref){
                my $thisrow = @$tbl_ref[$datarows];            
                $fieldsum[5] += trim($$thisrow[5]);                        
            }        
            
            #push @$tbl_ref, \@fieldsum;
            my $valuation=sprintf("%.2f",$fieldsum[5]);
            #print $q->p("Total Valuation: \$$fieldsum[5] - $#$tbl_ref SKU's");
            print $q->p("Total Average Cost: \$$valuation - $#$tbl_ref SKU's");
            printResult_arrayref2($tbl_ref, '', \@format, '');
            
            print $q->p("Total Average Cost: \$$valuation  - $#$tbl_ref SKU's");
        } else {
            print $q->b("no records found<br />\n"); 
            StandardFooter();	
        }    
    }
    
    
} elsif ($report eq "PROMOFLUSH") {  
    print $q->h3("SKU Promo Flush"); 
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMOFLUSH2"});    
    print  $q->input({ -type=>'text', -name=>"skuid", -size=>"8", -value=>" "});        
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
    print $q->p("Enter $a SKU number to flush and click the 'Enter' button.");
    
    print $q->end_form(), "\n";  
    StandardFooter();	
} elsif ($report eq "PROMOFLUSH2") {  
    print $q->h3("SKU Promo Flush"); 
    my $sku= $q->param("skuid"); 
    $sku =~ s/ //g; # Trim out any spaces
    $query = dequote(<< "    ENDQUERY");
        update plu set
            promoprice = 0,
             
            promonum = 0,
       
            promodetailid = 0,
            promoapplyprice = 0,
            promoPriceMethod = 0,
            promoPriceAdjust = 0,
            promoPercentAdjust = 0,
            promoPricingTarget = 0
        where 
            plunum = '$sku'
        
    ENDQUERY
 
    print $q->pre("$query");
    if ($dbh->do($query)) {
        print $q->p("Done - Successfully flushed all promos from SKU: $sku");
 
    } else {
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n";            
    }  
    StandardFooter();	     
} elsif ($report eq "PROMOTOUCH") {  
    print $q->h3("Promo Refresh");
    my $promotxn="${parmDir}/promotxn.asc";
 
    if (-f $promotxn) {
        print $q->h4("Found a promotxn.asc file already existing in the parm folder.  Please process this before using this tool.");
        StandardFooter();	
        exit;
    } else {
        open(PROMOTXN,">$promotxn");        
        # This creates a request to remove a non-existent promo.  The processing of this request will refresh promotions in general.
        print PROMOTXN "3,10/01/01,10/01/02,00:01,23:59,,,,,,,,,0,0,,,,,,N,,,,,,,,,,GoFish,1,,,,,,\n";
        close PROMOTXN;
        
         process_ASC();
    }
   
    StandardFooter();	
} elsif ($report eq "CLOCKIO") {   
    print $q->h1("CLOCKIO Tool.");  
    my @tmp;
    # Check that the clock seq number is not out of sync with the sys_numbers table
    # Get the DCLK number from sys_nubers
    my $dclk;
	$query = dequote(<< "	ENDQUERY");
        select  
            sysNum
        from
            sys_numbers
        where
            numType like 'DCLK'
	ENDQUERY
		
	$sth = $dbh->prepare($query);
	$sth->execute();
	while (@tmp = $sth->fetchrow_array) {
        $dclk=$tmp[0];
    }
    # Get the last clock_seq number from the clockio table
    my $clock_seq;
	$query = dequote(<< "	ENDQUERY");
        select  
            max(clock_seq)
        from
            clockio

	ENDQUERY
		
	$sth = $dbh->prepare($query);
	$sth->execute();
	while (@tmp = $sth->fetchrow_array) {
        $clock_seq=$tmp[0];
    }    
    unless ($dclk == $clock_seq) {
        print $q->h3("ERROR: The clock_seq does not match the DCLK number.");  
        my @header=(
            "Clock Seq",
            "DCLK"        
        );
        my @array=(
            "$clock_seq",
            "$dclk"
        );
        my @table=(\@header,\@array);
        TTV::cgiutilities::printResult_arrayref2(\@table); 
        print $q->p("Click 'FIX' to sync the DCLK number with the clock_seq number ");
        
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"CLOCKIO2"});
        print $q->input({-type=>"hidden", -name=>"clock_seq", -value=>"$clock_seq"});        
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'C', -value=>"   FIX   "}))
                  );
        print $q->end_form(), "\n"; 
    } else {
       print $q->h3("No errors found checking clockio.");  
    }
    StandardFooter(); 
} elsif ($report eq "CLOCKIO2") {   
    print $q->h1("CLOCKIO Tool.");  
    my $clock_seq= ($q->param("clock_seq"))[0]; 
	$query = dequote(<< "	ENDQUERY");
        update 
            sys_numbers
        set 
            sysNum = $clock_seq        
        where
            numType like 'DCLK'
	ENDQUERY
    if ($dbh->do($query)) {
        print $q->p("Done - Successfully sync'd DCLK with clock_seq $clock_seq");
        print $q->p(        
            $q->a({-href=>"$scriptname?report=CLOCKIO", -class=>"button1"}, "CLOCKIO").
            "Clock in/out tool. (Re-check DCLK)" );
    } else {
        print "Database Update Error"."<br />\n";
        print "Driver=ODBC"."<br />\n";
        print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br />\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
        print "errstr=".$dbh->errstr ."<br />\n";            
    }  
    
    StandardFooter(); 
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
   # Report: VALUATION
   # Description:
   # *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***  *** *** ***
} elsif ($report eq "VALUATION") {
    print $q->h3("Valuation Report: Date Selection");
	# Ask which day to do the report fo
	print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"VALUATION2"});
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Date Criteria:")));

    my $date_options = $q->option({-value=>'' -selected=>undef});
    $query = "select distinct(inventoryDate) from elderinvcurrent order by inventoryDate desc";
    $tbl_ref = execQuery_arrayref($dbh, $query);     
    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];
        my $date   = trim($$thisrow[0]);        
        my $day=substr($date,0,10);
        $date_options .= $q->option({-value=>$date }, "$day");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Date Selection:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},
        $q->Select({-name=>"date_selection", -size=>"1"}), $date_options)
    );   
 
            
      print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
      print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      #print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");
    
    
    StandardFooter();


} elsif ($tool eq "VALUATION2"){ 
    print $q->h2("Store Valuation Summary");
    my $date_selection = trim(($q->param("date_selection"))[0]);    
 
    print $q->p("The following is the current Store valuation as of $date_selection"), "\n";

 
    # Valuation by Vendor
    print $q->h3("Valuation Summary");

    $query = dequote(<< "    ENDQUERY");
    select
        count(*) as "SKUs",
        (select count(*) from elderinvcurrent where qtycurrent > 0) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price"
    from plu p left outer join elderinvcurrent ic on ic.plunum=p.plunum
    where
        ic.inventoryDate like '$date_selection';
    ENDQUERY
    
 
    $tbl_ref = execQuery_arrayref($dbh, $query);
    my @format = ({'align' => 'Right', 'num' => '.0'},
                {'align' => 'Right', 'num' => '.0'},
                {'align' => 'Right', 'num' => '.0'},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''});

    if (recordCount($tbl_ref)) { TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); }
    else { print $q->b("no records found<br />\n"); }

    # are there SKUs with invalid vendors?
    my $skus_with_invalid_vendor = 0;
    $query = "select count(*) as \"SKUs\" from plu p left outer join vendor v on p.vendorid=v.vendorid where v.vendorId is null";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    $skus_with_invalid_vendor = $$tbl_ref[1]->[0] if (recordCount($tbl_ref));


    # Valuation by Vendor
    print $q->h3("Valuation by Vendor");

    $query = dequote(<< "    ENDQUERY");
    select
        v.VendorId, v.VendorName,
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
    from plu p left outer join elderinvcurrent ic on ic.plunum=p.plunum
        join vendor v on p.vendorid=v.vendorid
    where
        ic.inventoryDate like '$date_selection'        
    group by v.vendorid, v.vendorName
    ENDQUERY

    if ($skus_with_invalid_vendor) {
    $query .= dequote(<< "    ENDQUERY");
    union
    select
        'n/a' as "VendorId", 'unknown vendor' as "VendorName",
        count(*) as "SKUs",
        sum(evl(0,'<',ic.qtycurrent,1,0)) "WithQty",
        sum(ic.qtycurrent) as "QOH",
        sum(ic.lastvendorcost*ic.qtycurrent) as "LastCost",
        sum(p.cost*ic.qtycurrent) as "AvgCost",
        sum(p.retailprice*ic.qtycurrent) as "Price",
        0 as "%TotPrice"
    from plu p left outer join elderinvcurrent ic on ic.plunum=p.plunum
        left outer join vendor v on p.vendorid=v.vendorid
    where v.vendorId is null
    and
        ic.inventoryDate like '$date_selection'    
    order by 1
    ENDQUERY
    }
    

    $tbl_ref = execQuery_arrayref($dbh, $query);

    my @fieldsum = ('Total:', '', 0, 0, 0, 0, 0, 0, 0);

    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];

        for my $field (2, 3, 4, 5, 6, 7)  {
            $fieldsum[$field] += trim($$thisrow[$field]);
        }
    }

    my $sumprice = $fieldsum[7];
    push @$tbl_ref, \@fieldsum;

    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];
        my $percent = ($sumprice == 0) ? 0 : nearest(.01, $$thisrow[7] / $sumprice * 100);
        $thisrow->[8] = $percent;

        my $thisvendorId = $thisrow->[0];
        next if ($thisvendorId eq "Total:");
       # $thisrow->[0] = $q->a({-href=>"/cgi-bin/i_reports.pl?report=VALUATIONVENDOR&VendorId=$thisvendorId", -class=>"datalink"}, $thisvendorId);
    }
    #my @links = ( '/cgi-bin/i_reports.pl?report=VALUATIONVENDOR&VendorId=$_' );
    @format = ('', '', {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'num' => '.2'});

    if (recordCount($tbl_ref)) { 
        TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format); 
    } else { 
        print $q->b("no records found<br />\n"); 
    }  
    StandardFooter();  
} elsif ($report eq "INV") {   
    print $q->h1("Inventory Reports & Tools");  
    my @reportlist = (
        ["Current Inventory Report", "/cgi-bin/i_toolshed.pl", "CURINV_QRY",
          "Reports Current Inventory."
        ], 
        ["Current Vendor Report", "/cgi-bin/i_toolshed.pl", "CURVEN_QRY",
          "Reports Current Vendors."
        ],               
        ["SKU Inventory Activity Report", "/cgi-bin/i_toolshed.pl", "SKUINVRPT",
          "Reports sales, receiving, adjustments, etc for specific SKUs on a specified day."
        ], 
        ["SKU Inventory Report", "/cgi-bin/i_toolshed.pl", "SKUQOHRPT",
          "Reports QOH for a specified SKU on a specified day."
        ],          
        ["Past Valuation Reports", "/cgi-bin/i_toolshed.pl", "VALUATION",
          "See what valuation was in the recent past."
        ],           
        ["Physical Inventory Report", "/cgi-bin/i_toolshed.pl", "PHYINVRPT",
          "Show physical inventory documents."
        ],   
        ["Variance Report", "/cgi-bin/i_toolshed.pl", "VARRPT",
          "Show the current variance.tsv file."
        ],   
        ["Variance Report II", "/cgi-bin/i_toolshed.pl", "VARTBLRPT",
          "Show the current variance table."
        ],   		
        ["Variance Tools", "/cgi-bin/i_tools_inventory.pl", "DEFAULT",
          "Import and work with the current variance.tsv file."
        ],    
        ["Offsite Inventory", "/cgi-bin/i_toolshed.pl", "OFFSITEINV",
          "Show Inventory Currently assigned to an Offsite Sale."
        ],    
        ["Export Mins & Maxs", "/cgi-bin/i_toolshed.pl", "EXPMINMAX",
          "Export the Min's and Max's for all SKUs under Inventory Control to a INVCONT.TAX file."
        ],      
        ["Import Mins & Maxs", "/cgi-bin/i_toolshed.pl", "IMPMINMAX",
          "Import the Min's and Max's from a INVCONT.TXT file in \temp."
        ],           
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }       
    StandardFooter(); 
} elsif ($report eq "OFFSITEINV") {   
    print $q->h1("Offsite Sale Inventory Report"); 
    # Get the list of offsites\
    my @offsite_list;
    my %offsite_inventory_hash;
    
    $query = dequote(<< "    ENDQUERY");
    select
        eo.SaleName
    from 
        elderOffsite eo
    order by 
        SaleName;
    ENDQUERY
    
    $sth = $dbh->prepare($query);
    $sth->execute();
    
    while (my @tmp=$sth->fetchrow_array()) {
        my $sale=$tmp[0];
        push(@offsite_list,$sale);        
    }   
    
###
    foreach my $SaleName (@offsite_list) {
        $query = dequote(<< "         ENDQUERY");
         begin 
            select 
               td.ststransnum,
               td.itemcnt, 
               td.plunum, 
               convert(int, td.qtyreceived) as XferOut,
               convert(int, 0) as XferIn
            into #tmpTransfers
            from 
               store_transfer t join 
               store_transfer_dtl td on t.ststransnum=td.ststransnum
            where 
               t.txnstatus = 'C' and 
               t.TransferType = 'O' and 
               t.DocumentNum like '$SaleName\%';
            
            insert #tmpTransfers
            select 
               td.ststransnum,
               td.itemcnt, 
               td.plunum, 
               0, 
               td.qtyreceived
            from 
               store_transfer t join 
               store_transfer_dtl td on t.ststransnum=td.ststransnum
            where 
               t.txnstatus = 'C' and 
               t.TransferType = 'I' and 
               t.DocumentNum like '$SaleName\%';
            
            select
               a.plunum as "SKU",
               min(p.pludesc) as Description,
			   p.retailprice,
               sum(a.XferOut - a.XferIn) as Qty
			   
             
            from
               #tmpTransfers a
               join plu p on a.plunum=p.plunum
 
            group by a.plunum,p.retailprice
            order by SKU;
            
            drop table #tmpTransfers;
         end
         ENDQUERY
       
        $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) { 
            print $q->p("Inventory for Sale $SaleName");
			my @format = (
 
				"",
				"",
				{'align' => 'Right', 'currency' => ''},              
			); 
            printResult_arrayref2($tbl_ref, '', \@format ); 
        } else { 
            #print $q->b("no records found sale $SaleName<Br>\n"); 
        }  
        $sth = $dbh->prepare($query);
        $sth->execute();
        
        while (my @tmp=$sth->fetchrow_array()) {
            my $SKU=$tmp[0];
            my $desc=$tmp[1];
            my $qty=$tmp[2];
            $offsite_inventory_hash{$SKU} += $qty;            
        } 
        
    }
    foreach my $SKU (sort keys(%offsite_inventory_hash)) {
        print "SKU: $SKU Qty: $offsite_inventory_hash{$SKU}<br />";
    }
      
   
###    
    
	StandardFooter(); 
	
    
    
} elsif ($report eq "EXPMINMAX") {   
    print $q->h1("Export Mins and Maxs"); 
    my %sku_hash;
    my $invcont = "c:/temp/INVCONT.TXT";
    
    if (-f $invcont) {
        print $q->h3("There is an invcont.txt file in the temp folder already.");
        print $q->h3("Please remove it and try again.");
        StandardFooter();
        exit;
    }
 
    $query = dequote(<< "    ENDQUERY");
    select
        plunum,
        canorder,
        minqty,
        maxqty
    from 
        inventory_current
 
    ENDQUERY
    
    $sth = $dbh->prepare($query);
    $sth->execute();
    
    while (my @tmp=$sth->fetchrow_array()) {
        my $plunum=$tmp[0];
        my $canorder=$tmp[1];
        my $minqty=sprintf("%d",$tmp[2]);
        my $maxqty=sprintf("%d",$tmp[3]);
        $sku_hash{$plunum} -> [0] = $canorder;
        $sku_hash{$plunum} -> [1] = $minqty;
        $sku_hash{$plunum} -> [2] = $maxqty;
    }   

    open(FILE,">$invcont");
    foreach my $plunum (sort keys(%sku_hash)) {
        my $canorder=$sku_hash{$plunum} -> [0];
        my $minqty=$sku_hash{$plunum} -> [1];
        my $maxqty=$sku_hash{$plunum} -> [2];
        print FILE "2,$plunum,1,$canorder,$minqty,$maxqty,0,\n";
    }
    close FILE;
 
    if (-f $invcont) {
        print $q->h4("The INVCONT.ASC file has been created and is in the /temp folder");
    }
    
	StandardFooter(); 
} elsif ($report eq "IMPMINMAX") {   
    my $confirmation = trim(($q->param("confirmation"))[0]);
    print $q->h1("Import Mins and Maxs"); 
    my %sku_hash;
    my $invcont = "c:/temp/INVCONT.TXT";
    
    unless (-f $invcont) {
        print $q->h3("There is no invcont.txt file in the temp folder yet.");
        print $q->h3("Please add it and try again.");
        StandardFooter();
        exit;
    }
    open (FILE,"$invcont");
    my @file_info=(<FILE>);
    close FILE;
    
    if ($confirmation) {
        my $count=0;
        foreach my $line (@file_info) {
            chomp($line);        
            my @tmp=split(/\,/,$line);
            foreach my $t (@tmp) {
                my $plunum = $tmp[1];
                my $canorder=$tmp[3];
                my $minqty=$tmp[4];
                my $maxqty=$tmp[5];   
                if ($canorder eq "Y") {
                    # Put the SKU under inventory control
                    unless (addToInvControl($plunum)) {
                        print $q->h4("ERROR: Failed to put $plunum under Inventory Control");
                        StandardFooter(); 
                        exit;
                    }
                }
                # Set the Min and Max
                unless (setMinMax($plunum,$canorder,$minqty,$maxqty)) {
                    print $q->h4("ERROR: Failed to set Min & Max for $plunum");
                    StandardFooter(); 
                    exit ;           
                }
            }  
            $count++;
        }
        print $q->p("Processed $count SKUs");        
    } else {         
        print $q->p("Read info from INVCONT.TXT");
        my $sku_count = ($#file_info + 1);
        print $q->p("Found $sku_count SKUs to process.");
        print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"IMPMINMAX"});
        print $q->input({-type=>"hidden", -name=>"confirmation", -value=>"confirmation"});
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Confirm processing these SKUs")));

        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
        print $q->end_table(),"\n";

        print $q->end_form(), "\n"; 
    }    
 
	StandardFooter();     
} elsif ($report eq "SALES") {   
    print $q->h1("Sales Reports");  
    my @reportlist = (
         ["Net Sales (alt)", "/cgi-bin/i_reports_cost_alt.pl", "SALESRPT",
          "Alternative Net Sales Report."
         ], 			 
         ["Cost of Goods (alt)", "/cgi-bin/i_reports_cost_alt.pl", "COGS",
          "Alternative Cost of Goods Sold Report."
         ],  
         ["Discounts Report (alt)", "/cgi-bin/i_reports_cost_alt.pl", "DISCOUNTS",
          "Alternative Discounts Report"
         ],          
         ["Aborted Transactions", "/cgi-bin/i_toolshed.pl", "ABORTEDTXN_QRY",
          "Report showing aborted transactions."
         ],    
         ["Post-Voided Transactions", "/cgi-bin/i_toolshed.pl", "VOIDEDTXN_QRY",
          "Report showing transactions which were post-voided."
         ], 
         ["Return Transactions", "/cgi-bin/i_toolshed.pl", "RETURNTXN_QRY",
          "Report showing transactions which were returns."
         ],  
         ["Merchandise Sales", "/cgi-bin/i_toolshed.pl", "MERCHSALES_QRY",
          "Report showing merchandise sold."
         ], 
         ["NonMerchandise Sales", "/cgi-bin/i_toolshed.pl", "NONMERCHSALES_QRY",
          "Report showing non-merchandise transactions."
         ],      
         ["Tax Exempt Sales", "/cgi-bin/i_toolshed.pl", "TE_SALES_QRY",
          "Report showing tax exempt transactions."
         ],  
         ["Suspended Sales", "/cgi-bin/i_toolshed.pl", "SU_SALES_QRY",
          "Report showing suspended transactions."
         ],      		 
         ["No Sales", "/cgi-bin/i_toolshed.pl", "NO_SALES_QRY",
          "Report showing SKU's which did not sell for during the selected period."
         ],          
         ["Transaction Report", "/cgi-bin/i_toolshed.pl", "TXN_QRY",
          "Report showing a specified transaction."
         ], 
         ["Card Transaction Report", "/cgi-bin/i_toolshed.pl", "CTXN_QRY",
          "Report showing transnet card transactions."
         ],    
         ["Tax Check Report", "/cgi-bin/i_toolshed.pl", "TAX_QRY",
          "Report showing tax on specified transactions."
         ], 
         ["Transactions: Manager Code", "/cgi-bin/i_toolshed.pl", "MGRCODE_QRY",
          "Report showing transactions which were where selected Manager Codes were run."
         ],
         ["Date Discrepancy Transaction Report", "/cgi-bin/i_toolshed.pl", "DATE_DISC_TXN_QRY",
          "Report showing transactions where the register date was wrong."
         ], 
        ["Import Offsite Sales", "/cgi-bin/i_toolshed.pl", "IMPORT_SALES_QRY",
          "Import Offsite Sales from a CSV file. (BETA)"
        ], 
        ["Daily Sales Report", "/cgi-bin/i_tools_dsr.pl", "DSRPT",
          "Access the DSR tool being developed for Accounting."
        ],  
        ["POS", "/cgi-bin/i_toolshed.pl", "POS",
          "Basic POS for offsite sales."
        ],            
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
            $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
            $reportnote );
    }       
    StandardFooter(); 
    exit;   
} elsif ($report eq "CTXN_QRY") {
    print $q->h1("Card Transaction Report");   
    my $log="c:/program files/transnet/logs/log.txt";
    my $saplog="c:/program files/SAP/transnet/logs/log.txt";
    my $label='Transnet';
    if (-f $saplog) {
        # If the SAP version is present, use it
        $log=$saplog;
        $label="SAP Transnet";
    }

    my %txn_count;
    my %card_count;
    if (-f $log) {
        print $q->p("Reading $label log file...");
        open (LOG,"$log");
        my @contents=(<LOG>);
        close LOG;
        parse_transnet_content(\@contents);       
    } else {
        print $q->p("Cannot locate the specified log file.");
    }
    StandardFooter(); 
    exit; 
} elsif ($report eq "TAX_QRY") {
    print $q->h1("Tax Report");
 
    # Ask which day to do the report fo
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TAX_REPORT"});

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    my @ltm = localtime(time());
    my $month=$ltm[4]+1;
    my $year=$ltm[5]+1900;    
    my $hm = 1;
    my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Date:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                      $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>" "}),
                      $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                             -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                            $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                           ),
                     )
              );
    $calcounter++;
    # Get the other criteria
    # - Register
    # - Cashier
    # - SKU
    # - Txn
 
 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},                      
        $q->input({-type=>'text', -name=>"txn_selection", -size=>"4" }),                  
        ));  
         

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );
    print $q->end_table(),"\n";

    print $q->end_form(), "\n"; 
    $dbh->disconnect;
    StandardFooter(); 
    exit;     
} elsif ($report eq "TAX_REPORT") {
    my $tran_date_label = trim(($q->param("startdate"))[0]); 
    my $txn_req = trim(($q->param("txn_selection"))[0]); 
        
    print $q->h1("Specified Transaction Tax Report");
    my $where="1 = 1";
    unless ($tran_date_label)  {
        print $q->p("Please 'go back' and select date.");
        StandardFooter(); 
        exit; 
    }    
    my $tran_req;   # Vestigial from being imported from monitor.pl
    my @tmp;
    my $verbose=1;
    my $date_req=$tran_date_label;
    my $dbh = openODBCDriver($dsn);   
    my $diff_limit=.01;
    ###
###
            print $q->p("Checking tax on transaction $txn_req on $tran_date_label..."); 
            my $warns=0;
            
            # Check for sales tax discrepancies
            
            # Get the tax rates
            my %tax_hash; 
			 my %tax_threshold_hash; 
            $query = dequote(<< "            EOQ");        
                select 
                    taxid,taxRate,taxthreshold from tax                
            EOQ
            $sth = $dbh->prepare($query);
            $sth->execute();
     
            while (@tmp=$sth->fetchrow_array()) {
				$tax_hash{$tmp[0]}=($tmp[1] / 100);
				$tax_threshold_hash{$tmp[0]}=($tmp[2]);
				if ($verbose) {
					print $q->p( "Tax: $tmp[0] $tax_hash{$tmp[0]} Threshold: $tax_threshold_hash{$tmp[0]}");
				}
            }
                  
            # Get the tax for each transaction
            my %transaction_hash;
            $query = dequote(<< "            EOQ");        
                select 
                    tms.transnum 
                from             
                    txn_merchandise_sale tms
     
                where 
                    tms.itemdatetime like '$tran_date_label%' 
                and
                    tms.txnvoidmod = 0                
            EOQ
            $sth = $dbh->prepare($query);
            $sth->execute();
            
            while (@tmp=$sth->fetchrow_array()) {
                $transaction_hash{$tmp[0]}=1;             
            }
            # Check to see if the transnum was specified
            if ($tran_req) {
                %transaction_hash=();
                $transaction_hash{$tran_req}=1;
            }           
            # If a transaction was specified, determine the transnum for that transaction
            if ($txn_req) {
                if ($date_req) {
                    %transaction_hash=();
                    $query = dequote(<< "                    EOQ");
                        select
                            transnum 
                        from 
                            txn_pos_transactions
                        where
                            txnnum = '$txn_req'
                        and
                            txndatetime like '$date_req%'
                    EOQ
                    $sth = $dbh->prepare($query);
                    $sth->execute();

                    while (@tmp=$sth->fetchrow_array()) { 
                        $transaction_hash{$tmp[0]}=1;
                    }
                }
            }
            # Get the transaction details for each transaction
            foreach my $transnum (sort keys(%transaction_hash)) {  
                my $calculated_tax_total=0;
                my $calculated_taxable_total=0;
                my $tax_total=0;
                my $taxable_total=0;  
                my $txn;            
                $query = dequote(<< "                EOQ");        
                    select 
                        tms.extsellprice,tms.skunum,p.tax1,p.tax2,p.tax3,p.tax4,p.tax5,tms.txnnum,tms.tax,tms.taxmod,tms.itemnum,tms.exttaxableamt                    
                    from             
                        txn_merchandise_sale tms
                        left outer join plu p on p.plunum = tms.skunum
                         
                    where 
                        tms.transnum = '$transnum'   
					order by
						tms.itemnum
                EOQ
                
                $sth = $dbh->prepare($query);
                $sth->execute();

                while (@tmp=$sth->fetchrow_array()) {
                    my $extsellprice=sprintf("%.2f",$tmp[0]);
                    my $sku=$tmp[1];
                    my $tax1=$tmp[2];
                    my $tax2=$tmp[3];  
					my $tax3=$tmp[4]; 
                    my $tax4=$tmp[5]; 
                    my $tax5=$tmp[6]; 
                    $txn=$tmp[7]; 
					my $was_taxed=$tmp[8];
					my $is_tax_mod=$tmp[9];
                    my $item=$tmp[10];
					my $taxable_amt=$tmp[1];
                    if ($is_tax_mod eq "Y") {
                        print $q->h3("WARNING: Tax on transnum $transnum item $item was modified.");
                        $warns++;                 
                    }
                    # Is this item taxable?
					my $is_taxed=0;
					my @check_tax;
                    my $calculated_tax=0;
					if ($tax1 eq 'Y') {
						#$check_tax=1;
                        push(@check_tax,1);
					}
					if ($tax2 eq 'Y') {
						#$check_tax=2;
                        push(@check_tax,2);
					}
					if ($tax3 eq 'Y') {
						#$check_tax=3;
                        push(@check_tax,3);
					}	
					if ($tax4 eq 'Y') {
						#$check_tax=4;
                        push(@check_tax,4);
					}	
					if ($tax5 eq 'Y') {
						#$check_tax=5;
                        push(@check_tax,5);
					}	                    
					# But, was this item taxed?
					if ($verbose) {
						print "SKU: $sku Taxed?  Tax1: $tax1 Tax2: $tax2 Tax3: $tax3 Tax4: $tax4 Tax5: $tax5<br />";
					}
                    if ($#check_tax > -1) {
                        # This item is taxable - this process can handle items taxed under more than one tax category/identifier
                        foreach my $tax_identifier (@check_tax) {
                            if ($tax_threshold_hash{$tax_identifier}) {
								if ($extsellprice < 0) {
									if ($verbose) {
										print $q->p("This looks like a return");
									}
								}
                                if (abs($extsellprice) >= $tax_threshold_hash{$tax_identifier}) {
                                    $is_taxed=1;
                                    if ($verbose) {
                                        print $q->p("Sellprice: $extsellprice Greater than or equal to threshold: $tax_threshold_hash{$tax_identifier}");
                                    }
                                }
                            } else {
                                $is_taxed=1;
                                if ($verbose) {
                                    print $q->p("SKU: $sku is taxable as $tax_identifier");
                                }							
                            }
                            if ($is_taxed) {
                                # I am using three places so that we don't get the calculated tax rounding up and causing a false error.
                                $calculated_tax+=sprintf("%.3f",($extsellprice * $tax_hash{$tax_identifier}));
                                if ($verbose) {
                                    print $q->p( "Tax Identifier: $tax_identifier Tax Rate: $tax_hash{$tax_identifier}");
                                }
                                unless ($calculated_tax) {                                
                                    print $q->h3("WARNING: Taxable amount for $sku was $calculated_tax");
                                    $warns++;                
                                }   
                                $calculated_tax_total+=$calculated_tax;
                                $calculated_taxable_total+=$extsellprice;
                                if ($verbose) {
                                    print $q->p("Txn: $txn SKU: $sku Price: $extsellprice Calculated Tax: $calculated_tax | Total Taxable: $calculated_taxable_total Total Tax: $calculated_tax_total");
                                }
                            } else {
                                if ($verbose) {
                                    print $q->p("Txn: $txn SKU: $sku is NOT taxed.");
                                }
                            }
                        }
                    }  					
                }
                # Get the tax amounts recorded for this transaction              
                $query = dequote(<< "                EOQ");        
                    select 
                        sum(tax_amt),
                        sum(taxable_amt)
                    from             
                        txn_tax                        
                    where 
                        transnum = '$transnum'              
                EOQ
                $sth = $dbh->prepare($query);
                $sth->execute();

                while (@tmp=$sth->fetchrow_array()) { 
                    if (defined($tmp[0])) {
                        $tax_total=sprintf("%.2f",$tmp[0]);
                        $taxable_total=sprintf("%.2f",$tmp[1]);                        
                        if ($verbose) {
                            print $q->p("Txn: $txn TaxTotal: $tax_total Taxable: $taxable_total");
                        }
                    }
                }
           
                # Sometimes there is a slight difference in calculated tax so ignore any difference of 1 cent
                my $diff=sprintf("%.2f",($calculated_tax_total - $tax_total));
                if ($diff > $diff_limit) {                
                    print $q->h3("WARNING: Tax for txn $txn expected to be $calculated_tax_total but found $tax_total");
                    $warns++;                 
                }
                $diff=sprintf("%.2f",($calculated_taxable_total - $taxable_total));
                if ($diff > $diff_limit) {                                
                    print $q->h3("WARNING: Taxable amount for txn $txn expected to be $calculated_taxable_total but found $taxable_total");
                    $warns++;                
                }   

            }   
            if ($warns) {                
                print $q->h3("WARNING: Detected $warns tax issues on $tran_date_label.");                        
            } else {
                print $q->p("No tax issues found.");
            }              
    ###
    StandardFooter(); 
    exit;     
} elsif ($report eq "TXN_QRY") {
    print $q->h1("Specified Transaction Report");
    # Get the register options:    
    my $dbh = openODBCDriver($dsn);    
    $query = "select max(regnum) from txn_pos_transactions where regnum < 10";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    
    my $reg_options = $q->option({-value=>'0' -selected=>undef}," ");
    for my $reg (1 .. $$tbl_ref[1][0]) {       
        $reg_options .= $q->option({-value=>$reg}, "Register $reg");
    }    
    # Get the cashier options:        
    $query = "    select cashiernum,empfirstname,emplastname from employee order by emplastname;";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    
    my $cashier_options = $q->option({-value=>'0' -selected=>undef}," ");
 
    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];
        my $cashier = trim($$thisrow[0]);
        my $first = trim($$thisrow[1]);
        my $last = trim($$thisrow[1]);
        $cashier_options .= $q->option({-value=>$cashier}, "$cashier - $first $last");
    }    
 

    # Ask which day to do the report for
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TXN_REPORT"});

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    my @ltm = localtime(time());
    my $month=$ltm[4]+1;
    my $year=$ltm[5]+1900;    
    my $hm = 1;
    my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Date:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                      $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>" "}),
                      $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                             -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                            $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                           ),
                     )
              );
    $calcounter++;
    # Get the other criteria
    # - Register
    # - Cashier
    # - SKU
    # - Txn
 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Register:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"reg_selection", -size=>"1"}), $reg_options)
              );      
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},                      
        $q->input({-type=>'text', -name=>"sku_selection", -size=>"10" }),                  
        ));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},                      
        $q->input({-type=>'text', -name=>"txn_selection", -size=>"4" }),                  
        ));  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Cashier:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"cashier_selection", -size=>"1"}), $cashier_options)
              );          

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );
    print $q->end_table(),"\n";

    print $q->end_form(), "\n"; 
    $dbh->disconnect;
    StandardFooter(); 
    exit; 
} elsif ($report eq "TXN_REPORT") {
    my $date_selection = trim(($q->param("startdate"))[0]); 
    my $reg_selection = trim(($q->param("reg_selection"))[0]); 
    my $sku_selection = trim(($q->param("sku_selection"))[0]); 
    my $cashier_selection = trim(($q->param("cashier_selection"))[0]); 
    my $txn_selection = trim(($q->param("txn_selection"))[0]); 
        
    print $q->h1("Specified Transaction Report");
    my $where="1 = 1";
    unless (($date_selection) || ($reg_selection) || ($sku_selection) || ($cashier_selection) || ($txn_selection)) {
        print $q->p("Please 'go back' and select one criteria to find a transaction.");
        StandardFooter(); 
        exit; 
    }    
    if ($date_selection) {
        $where .= " and  tpt.txndatetime like '$date_selection%'";
    }
    if ($reg_selection) {
        $where .= " and  tpt.regnum like '$reg_selection'";
    }    
    if ($cashier_selection) {
        $where .= " and  tpt.cashiernum like '$cashier_selection'";
    }  
    if ($txn_selection) {
        $where .= " and  tpt.txnnum like '$txn_selection'";
    }    
    my $merch_join;
    if ($sku_selection) {
        $where .= " and  tms.skunum like '$sku_selection'";
        $merch_join="left outer join txn_merchandise_sale tms on tms.transnum = tpt.transnum and tms.txnnum = tpt.txnnum";
    }       
    my $dbh = openODBCDriver($dsn);    
    $query = "
    select 
        distinct(string(tpt.transnum,'-',tpt.txnnum,'-',tpt.regnum)) as Transaction,
        tpt.regnum as Register,
        tpt.businessdate,
        tpt.txndatetime,
        tpt.cashiernum,
        string(e.empfirstname, ' ', e.emplastname) as Cashier,
        tmt.refnum
 
    from 
        txn_pos_transactions tpt
        left outer join txn_merchandise_sale tms on tms.transnum = tpt.transnum and tms.txnnum = tpt.txnnum
        left outer join employee e on tpt.cashiernum = e.cashiernum
        left outer join txn_miscellaneous_trans tmt on tpt.transnum = tmt.transnum and tpt.txnnum = tmt.txnnum
        --$merch_join        
    where 
        $where
    and
        tpt.regnum > 0
    and
        tpt.regnum <> 255
    --and
        --tpt.cashiernum > 0            
    order by
        tpt.txndatetime

    ";
     
    $tbl_ref = execQuery_arrayref($dbh, $query);    
    if (recordCount($tbl_ref)) {
        my @links = ( "$scriptname?report=TXN_REPORT2&selection=".'$_' );
        printResult_arrayref2($tbl_ref, \@links); 
        print $q->p("Click on the transaction to see the details of that transaction");
    } else {
        print $q->h3("No transaction records found for that criteria.");
    }     
    $dbh->disconnect;
    StandardFooter(); 
    exit; 
} elsif ($report eq "TXN_REPORT2") {
    
    my $selection = trim(($q->param("selection"))[0]);  
    my @tmp=split(/-/,$selection);
    my $transnum=$tmp[0];
    my $txnnum=$tmp[1];
    my $regnum=$tmp[2];
    my $txn_status;
    my $major_divider="------------------------------------------------------------------------";
    my $minor_divider=" ";
    my %status_hash=(
        '1' => "A PostVoid",
        '2' => "Aborted",
        '3' => "Post-Voided"
        );
    my %item_hash;
    my %discountType_hash = (
    '0' => 'Undefined discount type', 
    '1' => 'ITEM % OFF', 
    '2' => 'ITEM $ OFF', 
    '3' => 'PRICE MARKDOWN',
    '4' => 'TRANS % OFF', 
    '5' => 'TRANS $ OFF', 
    '6' => 'MFG $ COUPON', 
    '7' => 'TRANS % OFF IN EMPLOYEE SALE', 
    '8' => 'MIX MATCH ITEMS', 
    '9' => 'MFG COUPON FOR MATCHED ITEM', 
    '10' => 'SCAN MFG COUPON,NO LINK TO AN ITEM', 
    '11' => 'ITEM % OFF IN EMPLOYEE SALE', 
    '12' => 'Item % Surcharge', 
    '13' => 'Item $ Surcharge', 
    '14' => 'SURCHARGE MARKDOWN', 
    '15' => 'Transaction % Surcharge', 
    '16' => 'Transaction $ Surcharge', 
    '17' => 'TRANS % AT TOTAL', 
    '18' => 'TRANS $ AT TOTAL', 
    '19' => 'TRANS % AT TENDER', 
    '20' => 'TRANS $ AT TENDER', 
    '25' => 'Item Best Price %', 
    '26' => 'Item Best Price $', 
    );  
    my @transaction_discounts=("4","5","7","15","16","17","18","19","20");
    my @item_discounts=("1","2","3","9","10","11","12","13","25","26");
    print $q->h1("Specified Transaction Report: Transnum: $transnum - Txn: $txnnum Reg: $regnum");
    ####
    # Find all of the items in the transaction
    ####
    my $dbh = openODBCDriver($dsn);      
    # Find the tax info for this store
    my %tax_info_hash;
    $query = "
    select 
        taxid,taxRate,taxontax,taxthreshold
    from 
        tax           
    ";    
    
    my $sth = $dbh->prepare($query);        
    $sth->execute();    
    
    while (my @tmp=$sth->fetchrow_array()) { 
        my @array=("$tmp[1]","$tmp[2]","$tmp[3]");        
        $tax_info_hash{$tmp[0]}=\@array;
    }    
    # Find the date, txndate, and cashier
  
    $query = "
    select 
        tpt.businessdate,
        tpt.txndatetime,
        tpt.cashiernum,
        string(e.empfirstname, ' ', e.emplastname) as Cashier,
        tpt.txnvoidmod1

    from 
        txn_pos_transactions tpt
        left outer join employee e on tpt.cashiernum = e.cashiernum
        
    where 
        tpt.transnum = $transnum
    and
        tpt.txnnum = $txnnum
    and
        tpt.regnum = $regnum
    
    ";    
    
    $sth = $dbh->prepare($query);        
    $sth->execute();    
    my $bisdate;
    while (my @tmp=$sth->fetchrow_array()) { 
        $bisdate=$tmp[0];
        my $bdate=substr($tmp[0],0,10);
        print "Business Date: $bdate<br />";
        my $tdate=substr($tmp[1],0,19);
        print "Txn Date & Time: $tdate<br />";
        print "Cashier: $tmp[2] $tmp[3]<br />";
        # The status shows us whether this transaction was aborted is a  post-void
        $txn_status=$tmp[4];
         
    }
    
    # check for whether this transaction was post-voided
    $query = "
    select 
        tmt.transnum
    from 
        txn_miscellaneous_trans tmt   
        join txn_pos_transactions tpt on tpt.transnum = tmt.transnum
    where 
        tmt.refnum = $txnnum 
    and
        tmt.businessdate like '$bisdate'  
    and
        tpt.regnum = $regnum
    ";     
 
    $sth = $dbh->prepare($query);        
    $sth->execute();        
    
    while (my @tmp=$sth->fetchrow_array()) { 
        $txn_status=3;
    }    
	# Check if the transaction was tax exempt
	my $tax_exempt=0;
    $query = "
    select 
        tudt.udefinedtxnid
    from 
        txn_user_defined_txn tudt   
        join txn_pos_transactions tpt on tpt.transnum = tudt.transnum
    where 
        tudt.txnnum = $txnnum 
    and
        tpt.businessdate like '$bisdate'  
    and
		tudt.transnum = $transnum	
   
    ";     
 
    $sth = $dbh->prepare($query);        
    $sth->execute();        
	my $storenum = getStoreNum($dbh);
    while (my @tmp=$sth->fetchrow_array()) { 
 
		if ($tmp[0] == 3) {
			$tax_exempt=1;
		}
		if (($storenum == 7037) && ($tmp[0] == 5)) {
			$tax_exempt=1;
		}		
    } 	
 	
    # Record the total as shown in the txn_transaction_total table
    my $totalwotax;
    my $txntotaltax;
    $query = "
    select  
        totalwotax,
        txntotaltax        
    from        
        txn_transaction_total        
    where 
         transnum = $transnum
    and
         txnnum = $txnnum 
    order by
         itemnum
    ";     
    
    $sth = $dbh->prepare($query);        
    $sth->execute();  
 
    while (my @tmp=$sth->fetchrow_array()) {              
        $totalwotax=sprintf("%.2f",$tmp[0]);
        $txntotaltax=sprintf("%.2f",$tmp[1]);
    }        
    print "$major_divider<br />";
    # Find the items in the merchandise table
    $query = "
    select         
        tms.skunum,
        tms.qty,
        tms.itemnum,
        tms.extsellprice,
        tms.extorigprice,
        tms.promonum,
        tms.promoPriceAdjust,
        tms.promoPercentAdjust,
        plu.tax1,
        plu.tax2,
        plu.tax3,
        plu.tax4,
        plu.tax5
        
    from 
        txn_pos_transactions tpt        
        join txn_merchandise_sale tms on tms.transnum = tpt.transnum and tms.txnnum = tpt.txnnum   
        left outer join plu plu on plu.plunum=tms.skunum
        
    where 
        tpt.transnum = $transnum
    and
        tpt.txnnum = $txnnum
    and
        tpt.regnum = $regnum
    order by
        tms.itemnum
    ";     
    
    $sth = $dbh->prepare($query);        
    $sth->execute();  
    my $item_count=1;
    while (my @tmp=$sth->fetchrow_array()) {              
        my $sku=$tmp[0];
        my $qty=$tmp[1];
        my $itemnum=$tmp[2];
        my $sell=$tmp[3];
        my $price=$tmp[4];
        my $promonum=$tmp[5];
        my $promoPriceAdjust=$tmp[6];
        my $promoPercentAdjust=$tmp[7];
        my $tax1=$tmp[8];        
        my $tax2=$tmp[9];        
        my $tax3=$tmp[10];        
        my $tax4=$tmp[11];        
        my $tax5=$tmp[12];        
        #print "Item #${item_count}: $sku - ";
       #print "Qty: $qty<br />";
        my $estimated_tax=0;
        my $taxes_applied;
 
        if ($tax1 eq 'Y') {
            # This item is taxable under tax 1
            my $rate=($tax_info_hash{1}->[0] /100);
            $estimated_tax=sprintf("%.2f",($sell * $rate));
            $taxes_applied=1;    
        }
        if ($tax2 eq 'Y') {
            # This item is taxable under tax 2
            my $rate=($tax_info_hash{2}->[0] /100);
            my $tont=$tax_info_hash{2}->[1];            
            if ($tont eq 'Y') {
                # If tax on tax
                $estimated_tax+=sprintf("%.2f",(($sell + $estimated_tax) * $rate));
            } else {
               $estimated_tax+=sprintf("%.2f",($sell * $rate));
            }          
            $taxes_applied.=" 2";              
        }      
        if ($tax3 eq 'Y') {
            # This item is taxable under tax 3
            my $rate=($tax_info_hash{3}->[0] /100);
            my $tont=$tax_info_hash{3}->[1];
            if ($tont eq 'Y') {
                # If tax on tax
                $estimated_tax+=sprintf("%.2f",(($sell + $estimated_tax) * $rate));
            } else {
               $estimated_tax+=sprintf("%.2f",($sell * $rate));
            }         
            $taxes_applied.=" 3";                          
        } 
        if ($tax4 eq 'Y') {
            # This item is taxable under tax 4
            my $rate=($tax_info_hash{4}->[0] /100);
            my $tont=$tax_info_hash{4}->[1];
            if ($tont eq 'Y') {
                # If tax on tax
                $estimated_tax+=sprintf("%.2f",(($sell + $estimated_tax) * $rate));
            } else {
               $estimated_tax+=sprintf("%.2f",($sell * $rate));
            }          
            $taxes_applied.=" 4";                                     
        } 
        if ($tax5 eq 'Y') {
            # This item is taxable under tax 5
            my $rate=($tax_info_hash{5}->[0] /100);
            my $tont=$tax_info_hash{5}->[1];
            if ($tont eq 'Y') {
                # If tax on tax
                $estimated_tax+=sprintf("%.2f",(($sell + $estimated_tax) * $rate));
            } else {
               $estimated_tax+=sprintf("%.2f",($sell * $rate));
            }          
            $taxes_applied.=" 5";                                    
        }      
      
        my @array=("sale","$sku","$qty","$sell","$price","$promonum","$promoPriceAdjust","$promoPercentAdjust","$estimated_tax","$taxes_applied");
        $item_hash{$itemnum}=\@array;
        $item_count++;
    }    
    # What were the discounts on this transaction?
    $query = "
    select         
        ttd.disctypecode,
        ttd.discdefnum,
        ttd.choicecode,
        ttd.reasonid,
        ttd.discpercent,
        ttd.extdiscamt,
        ttd.refnum,
        ttd.extundiscprice,
        ttd.itemnum,
        ttd.txndiscprorated
    from 
        txn_pos_transactions tpt        
        join txn_transaction_discounts ttd on ttd.transnum = tpt.transnum and ttd.txnnum = tpt.txnnum  
 
    where 
        tpt.transnum = $transnum
    and
        tpt.txnnum = $txnnum
    and
        tpt.regnum = $regnum
 
    order by
        ttd.itemnum
    ";     
 
    $sth = $dbh->prepare($query);        
    $sth->execute();  
    $item_count=1;
    while (my @tmp=$sth->fetchrow_array()) {              
        my $type=$tmp[0];
        my $num=$tmp[1];
        my $choice=$tmp[2];
        my $reason=$tmp[3];
        my $percent=$tmp[4];
        my $amount=$tmp[5];
        my $refnum=$tmp[6];
        my $price=$tmp[7];
 
        my $itemnum=$tmp[8];
        my $txndiscprorated=$tmp[9];
 
        my @array=("discount","$type","$num","$choice","$reason","$percent","$amount","$refnum","$price","$txndiscprorated" );
        $item_hash{$itemnum}=\@array;
        $item_count++;
    }     
    # Get the Payment Info
    $query = "
    select         
        t.tenderdesc,
        tmp.tenderamt,
        tmp.changeamt,
        tmp.exchangeamt,
        tmp.itemnum 
    from        
        txn_method_of_payment tmp 
        join tender t on t.tenderid = tmp.tenderid       
    where 
        tmp.transnum = $transnum
    and
        tmp.txnnum = $txnnum 
    order by
        tmp.itemnum
    ";     
    
    $sth = $dbh->prepare($query);        
    $sth->execute();  
    $item_count=1;
    while (my @tmp=$sth->fetchrow_array()) {              
        my $tender=$tmp[0];
        my $tendermnt=$tmp[1];
        my $changeamt=$tmp[2];
        my $exchangeamt=$tmp[3];
        my $itemnum=$tmp[4];
        my @array=("payment","$tender","$tendermnt","$changeamt","$exchangeamt");
        $item_hash{$itemnum}=\@array;
        $item_count++;
    }      
    # Get the Tax Info
    $query = "
    select  
        taxable_amt,
        tax_amt,
        tax_id,
        taxrate,
        itemnum
    from        
        txn_tax        
    where 
         transnum = $transnum
    and
         txnnum = $txnnum 
    order by
         itemnum
    ";     
    
    $sth = $dbh->prepare($query);        
    $sth->execute();  
    $item_count=1;
    while (my @tmp=$sth->fetchrow_array()) {              
        my $taxable_amt=$tmp[0];
        my $tax_amt=$tmp[1];
        my $tax_id=$tmp[2];
        my $taxrate=$tmp[3];
        my $itemnum=$tmp[4];
        my @array=("tax","$taxable_amt","$tax_amt","$tax_id","$taxrate");
        $item_hash{$itemnum}=\@array;
        $item_count++;
    }          
    # Was this a layaway?
    my $found_layaway=0;
    my %layaway_hash;
    my @layaway_array;
    $query = "
    select          
        tla.lwaytxntype,
        tla.lwaynum,
        tla.custname,
        tla.startbalanceowing,
        tla.itemnum 
    from 
        txn_pos_transactions tpt        
        join txn_layaway_admin tla on tla.transnum = tpt.transnum and tla.txnnum = tpt.txnnum           
    where 
        tpt.transnum = $transnum
    and
        tpt.txnnum = $txnnum
    and
        tpt.regnum = $regnum
 
    order by
        tla .itemnum
    ";     
 
    $sth = $dbh->prepare($query);        
    $sth->execute(); 
    while (my @tmp=$sth->fetchrow_array()) { 
        $found_layaway=$tmp[1];
        @layaway_array=@tmp;
    }    
 
    if ($found_layaway) {
        my $layaway_txn_type;
        # Get the basic Layaway Info        
        $query = "
        select          
            pme.extsellprice,
            pme.skunum,
            p.pludesc,
            pt.payment,
            pt.taxamt
        from             
            ptd_merch_entry pme
            join plu p on pme.skunum = p.plunum
            left outer join ptd_total pt on pt.transnum = pme.transnum
        where 
            pme.transnum in (select transnum from txn_layaway_admin where lwaytxntype = 'SA' and lwaynum = $found_layaway)             
        ";          
        $sth = $dbh->prepare($query);        
        $sth->execute(); 
        while (my @tmp=$sth->fetchrow_array()) { 
            # Grabbing some things we brought in from a previous query...
            my $type=$layaway_array[0];
            my $num=$layaway_array[1];
            my $customer=$layaway_array[2];
            my $owing=$layaway_array[3];
            my $itemnum=$layaway_array[4];        
            my $extsellprice=$tmp[0];
            my $skunum=$tmp[1];
            my $pludesc=$tmp[2]; 
            my $payment=$tmp[3];
            my $tax=$tmp[4];
            $layaway_txn_type=$type;
            my @array=("layaway1","$num","$customer","$extsellprice","$owing","$skunum","$pludesc","$payment","$tax");
            $item_hash{$itemnum}=\@array;             
        }            
    
        
        # Get the Layaway payment info
        $query = "
        select          
            tlt.lwaytotalamt,
            tlt.lwaypayments,
            tlt.payment,
            tlt.endbalanceowing,
            tlt.taxamt,
            tlt.itemnum 
        from 
            txn_pos_transactions tpt        
            join txn_layaway_total tlt on tlt.transnum = tpt.transnum and tlt.txnnum = tpt.txnnum           
        where 
            tpt.transnum = $transnum
        and
            tpt.txnnum = $txnnum
        and
            tpt.regnum = $regnum
     
        order by
            tlt.itemnum
        ";     
     
        $sth = $dbh->prepare($query);        
        $sth->execute();  
        my $item_count=1;
        while (my @tmp=$sth->fetchrow_array()) {              
            my $total=$tmp[0];
            my $lpaymnt=$tmp[1];
            my $paymnt=$tmp[2];
            my $owing=$tmp[3];
            my $tax=$tmp[4];
            my $itemnum=$tmp[5];
            my @array=("layaway2","$layaway_txn_type","$total","$lpaymnt","$paymnt","$owing","$tax");
            $item_hash{$itemnum}=\@array;
            $item_count++;
        }         
        # Get the payment type info for layaway
        $query = "
        select          
            t.tenderdesc,
            pmp.tenderamt,
            pmp.changeamt,
            pmp.exchangeamt,
            pmp.itemnum 
        from 
            ptd_method_of_payment pmp      
            join tender t on t.tenderid = pmp.tenderid        
        where 
            pmp.transnum = $transnum
     
        order by
            pmp.itemnum
        ";     
     
        $sth = $dbh->prepare($query);        
        $sth->execute();  
        $item_count=1;
        while (my @tmp=$sth->fetchrow_array()) {              
            my $tender=$tmp[0];
            my $tendermnt=$tmp[1];
            my $changeamt=$tmp[2];
            my $exchangeamt=$tmp[3];
            my $itemnum=$tmp[4];
            my @array=("layaway3","$tender","$tendermnt","$changeamt","$exchangeamt");
            $item_hash{$itemnum}=\@array;
            $item_count++;
        }      
    }
    # What were the non-merchandise items?
 
    $query = "
    select 
        nm.nonmerchdesc,
 
        tms.qty,
        tms.itemnum,
        tms.extsellprice,
        tms.extorigprice
    from 
        txn_pos_transactions tpt        
        join txn_non_merch_sale tms on tms.transnum = tpt.transnum and tms.txnnum = tpt.txnnum    
        join non_merch nm on tms.nonmerchid = nm.nonmerchid
    where 
        tpt.transnum = $transnum
    and
        tpt.txnnum = $txnnum
    and
        tpt.regnum = $regnum
    order by
        tms.itemnum
    ";     
    
    $sth = $dbh->prepare($query);        
    $sth->execute();  
 
    while (my @tmp=$sth->fetchrow_array()) {              
        my $nonmerch=$tmp[0];
        my $qty=$tmp[1];
        my $itemnum=$tmp[2];
        my $sell=$tmp[3];
        my $price=$tmp[4];
        my @array=("nonmerch","$nonmerch","$qty","$sell","$price");
        $item_hash{$itemnum}=\@array;
        $item_count++;
    }        
    # Were there any profile prompt responses for this transaction?
    my %pp_hash=(
    '1'=>'LOCAL CHARGE INFO', 
    '2'=>'DELIVERY PROMPTS',
    '3'=>'STORE NUMBER',
    '4'=>'STORE USE',
    '5'=>'CHECK INFO',
    '6'=>'PAID IN/OUT',
    '7'=>'RETURN INFO',
    '8'=>'PURCHZIPCODE',
    '9'=>'COMPETITIVE PRICE',
    '10'=>'CUST INFO',
    '11'=>'NM-OTHER',
    '12'=>'NM-DONAT-OTHER',
    '13'=>'GI - ALL',
    '14'=>'GI - ZIPONLY',
    '15'=>'GI - ALLBUTSTATE',
    '16'=>'NM-DONAT',
    '17'=>'RETURN INFO.',
    '18'=>'NMD-LIVINGGIFT',
    '19'=>'CASH COUNT',
    '20'=>'TAX EXEMPT',
    '21'=>'ITEM INFORM',
    '23'=>'PAID OUTS',
    '25'=>'ASSOCIATE INFO',
    '26'=>'SKU DESC',
    '27'=>'NMD-VILLAGES',
    '28'=>'NMD-MCC',
    '71'=>'URL FraudWatch',
    '99'=>'MICR'
        
    );
    $query = "
    select 
        itemnum,
        propromptid,
        profileresponse1,
        profileresponse2
    from 
        txn_profile_prompt_response
    where 
        transnum = $transnum
    and
        txnnum = $txnnum 
    order by
        itemnum
    ";     
    
    $sth = $dbh->prepare($query);        
    $sth->execute();  
 
    while (my @tmp=$sth->fetchrow_array()) {  
        my $itemnum=$tmp[0];    
        my $propromptid=$tmp[1];
        my $prompt_text=$pp_hash{$propromptid};
        my $profileresponse1=$tmp[2];
        my $profileresponse2=$tmp[3];
 
        my @array=("proprompt","$propromptid","$prompt_text","$profileresponse1","$profileresponse2");
        $item_hash{$itemnum}=\@array;
        $item_count++;
    }          
    
    # Iterate through the item hash and report
    my %transaction_discount_info_hash;
    my %totals_hash;
	my $layaway_payment=0;
    foreach my $item (sort { $a <=> $b} keys(%item_hash)) {         
        my $ref=$item_hash{$item};
        my @array=@$ref;
        if ($array[0] eq "sale") {
            # Show the sales info
            my $sku=$array[1];
            my $qty=sprintf("%d",$array[2]);            
            my $sell=sprintf("%.2f",$array[3]);
            my $price=sprintf("%.2f",$array[4]);  
            my $promonum=$array[5];
            my $promoPriceAdjust=sprintf("%.2f",$array[6]); 
            my $promoPercentAdjust=$array[7];  
            my $estimatedTax=$array[8];  
            my $taxesApplied=$array[9];
            if (abs($estimatedTax) > 0) {
                $taxesApplied="Tax ID: $taxesApplied";
            }
            #print "$minor_divider<br />";                        
            print "Sale Info: SKU: $sku Qty: $qty  Price: \$$price Sell Price: \$$sell Tax: \$$estimatedTax $taxesApplied<br />";  
            if ($promonum) {
                print "--->Promo Info: Number: $promonum   Amt: \$$promoPriceAdjust  Percent: $promoPercentAdjust   <br />";  
            }
            $totals_hash{"Estimated Tax"}+=$estimatedTax;
            $totals_hash{"Sell Price"}+=$sell;
            $totals_hash{"PromoPriceAdjust"}+=$promoPriceAdjust;
        }
   
        if ($array[0] eq "nonmerch") {
            # Show the sales info
            my $nonmerch=$array[1];
            my $qty=sprintf("%d",$array[2]);            
            my $sell=sprintf("%.2f",$array[3]);
            my $price=sprintf("%.2f",$array[4]);  
                  
            print "Non Merch Info: NM Item: $nonmerch Qty: $qty  Price: \$$price Sell Price: \$$sell<br />";  
        
            $totals_hash{"NonMerchPrice"}+=$sell;            
        } 
        if ($array[0] eq "proprompt") {
            # Show the profile prompt response info                                 
            my $propromptid=$array[1];
            my $prompt_text=$array[2];
            my $profileresponse1=$array[3];      
            my $profileresponse2=$array[4];              
            print "--->Profile Prompt Info:  PromptId: $propromptid Prompt: $prompt_text Response 1: $profileresponse1 Response 2: $profileresponse2<br />";  
         
        }             
      
        if ($array[0] eq "discount") {
            # Show the discount info
            my $type=$array[1];
            my $num=$array[2];
            my $choice=$array[3];
            my $reason=$array[4];
            my $percent=sprintf("%d",$array[5]);
            my $amount=sprintf("%.2f",$array[6]);
            my $refnum=$array[7];
            my $price=sprintf("%.2f",$array[8]);
            my $txndiscprorated=$array[9];
            my $ddesc=$array[10];
            my $is_transaction_discount=0;
 
            foreach my $t (@transaction_discounts) {
                if ($type == $t) {
                    $is_transaction_discount=1;
                }
            }
            unless ($percent) {
                # The MixMatch discounts do not record the discount percent in the txn_transaction_discounts table.
                # I think we could look it up in the Mix_Match table by the refnum but we can also calculate it from the
                # price                    
                if ($price != 0)  {                   
                    $percent=abs(sprintf("%d",(($amount / $price) * 100)));                    
                } else {
                    $percent = "unknown";
                }
                
            }
            
            my @array=("Transaction Discount:$discountType_hash{$type}", "Percent: $percent");
            $transaction_discount_info_hash{$type}=\@array;                
            if ($is_transaction_discount) {
                if ($txndiscprorated eq "Y") {
                    # Transaction discounts have an item line for each SKUs sold as well as the total at the end of the transaction.
                    # This check prevents the total at the end from printing.
                    print "--->Discount Info: Type: $discountType_hash{$type}   Amt: \$$amount   <br />";  
                    $totals_hash{"TransactionDiscounts"}+=$amount;                
                }
               
            } else {
                print "--->Discount Info: Type: $discountType_hash{$type}   Amt: \$$amount   <br />"; 
                $totals_hash{"OtherDiscounts"}+=$amount;                
            }
 
        }   
        if ($array[0] eq "payment") {
            # Show the  payment  info
            my $tender=$array[1];
            my $tendermnt=sprintf("%.2f",$array[2]);
            my $changeamt=sprintf("%.2f",$array[3]);
            my $exchangeamt=sprintf("%.2f",$array[4]);
            $totals_hash{"Payment"}+=($tendermnt - $changeamt - $exchangeamt); 
            print "--->Payment Info: Tender: $tender   Amount: \$$tendermnt  Change: \$$changeamt Exchange: \$$exchangeamt  <br />";  
        }   
        if ($array[0] eq "tax") {
            # Show the  tax  info
            my $taxable_amt=sprintf("%.2f",$array[1]);
            my $tax_amt=sprintf("%.2f",$array[2]);
            my $tax_id=$array[3];
            my $taxrate=$array[4];
            $totals_hash{"Tax"}+=$tax_amt;  
            print "--->Tax Info: TaxableAmt: \$$taxable_amt   TaxAmt: \$$tax_amt  TaxID: $tax_id TaxRate: $taxrate  <br />";  
        }               

     
        if ($array[0] eq "layaway1") {
            # Show the layaway info    
           
            my $num=$array[1];
            my $customer=$array[2];
            my $sell=sprintf("%.2f",$array[3]);  
            my $owing=sprintf("%.2f",$array[4]); 
            my $sku=$array[5];
            my $desc=$array[6];
            my $pay=sprintf("%.2f",$array[7]); 
            my $tax=sprintf("%.2f",$array[8]); 
            print "--->Layaway Info:    Number: $num Customer: $customer <br />";
            print "---------->SKU: $sku Desc: $desc StartAmt: \$$sell Tax: \$$tax InitialPayment: \$$pay Owe: \$$owing <br />";  
        }
        if ($array[0] eq "layaway2") {
            # Show the layaway info        
            my $type = $array[1];
            my $total=sprintf("%.2f",$array[2]);
            my $lpaymnt=sprintf("%.2f",$array[3]);
            my $paymnt=sprintf("%.2f",$array[4]);
            my $owing=sprintf("%.2f",$array[5]);
            my $tax=sprintf("%.2f",$array[6]);   
            print "--->Layaway Transaction Info: Type:  $type Total:  \$$total   LayAwayPayments: \$$lpaymnt  ThisPayment: \$$paymnt Owe: \$$owing Tax: \$$tax   <br />";  
			if ($owing) {
				# If this is a payment but there more more owed, then assume that there is no tax paid.
				$layaway_payment=1;
				$totals_hash{'Estimated Tax'}=0;				
			}
        }        
        if ($array[0] eq "layaway3") {
            # Show the layaway payment  info
            my $tender=$array[1];
            my $tendermnt=sprintf("%.2f",$array[2]);
            my $changeamt=sprintf("%.2f",$array[3]);
            my $exchangeamt=sprintf("%.2f",$array[4]);
            print "--->Layaway Payment Info: Tender: $tender   Amount: \$$tendermnt  Change: \$$changeamt Exchange: \$$exchangeamt  <br />";  
        }              
        my $last_item=$array[0];
    }
    print "$major_divider<br />";
    foreach my $key (sort keys(%transaction_discount_info_hash)) {
        my $ref=$transaction_discount_info_hash{$key};
        my @array=@$ref;
        print "@array<br />";
    }
    print "<br />";
    # Print Totals:
    if ($totals_hash{'TransactionDiscounts'}){print "Total TransactionDiscounts : \$$totals_hash{'TransactionDiscounts'}<br />"};
    if ($totals_hash{'OtherDiscounts'}){print "Total OtherDiscounts : \$$totals_hash{'OtherDiscounts'}<br />"};
    if ($totals_hash{'PromotionDiscounts'}){print "Total PromotionDiscounts : \$$totals_hash{'PromotionDiscounts'}<br />"};
    if ($totals_hash{'Sell Price'}){print "Total Sell Price : \$$totals_hash{'Sell Price'}<br />"};
	# If this was a tax exempt transactions, clear the estimated tax
	if ($tax_exempt) {
		print $q->h4("This transaction was tax-exempt");
		$totals_hash{'Estimated Tax'}=0;
	}  
	
    # Compare the Sell Price total with the total without tax
	my $diff=sprintf("%d",($totals_hash{'Sell Price'}-$totalwotax));
 
	if (abs($diff) > 0) {
        $diff=sprintf("%.2f",($totals_hash{'Sell Price'}-$totalwotax));
        if ($totalwotax eq "") {
            $totalwotax="undefined";
        }        
		
		unless ($layaway_payment) {
			print $q->h3("Warning: Sell price (\$$totals_hash{'Sell Price'}) does not agree with actual total without tax: ($totalwotax)    diff: \$$diff");
		}
    }	
	$totals_hash{'Estimated Tax'}=sprintf("%.2f",$totals_hash{'Estimated Tax'});
	$totals_hash{'Tax'}=sprintf("%.2f",$totals_hash{'Tax'});	
    if ($totals_hash{'Tax'}){	
        print "Total Tax : \$$totals_hash{'Tax'}<br />";
        if ($totals_hash{'Estimated Tax'}) {
            # Check that the estimated tax agress with the total tax collected
            my $diff=sprintf("%.2f",($totals_hash{'Estimated Tax'}-$totals_hash{'Tax'}));
          
            if ($diff > 0) {
                print $q->h3("Warning: Estimated tax was ($totals_hash{'Estimated Tax'}) but collected tax was ($totals_hash{'Tax'})  diff: \$$diff");
            } else {
                print $q->p("Tax check passed");
            }
        } else {
            # Check that the txntotaltax agrees
            unless ($totals_hash{'Estimated Tax'} eq "$txntotaltax") {
                print $q->h3("Warning: Estimated tax was ($totals_hash{'Estimated Tax'}) does not agree with total tax for txn: $txntotaltax");
            }
        }
    } elsif ($totals_hash{'Estimated Tax'}) {
        if ($totals_hash{'Tax'} eq "") {
            $totals_hash{'Tax'} = "undefined";
        }
        print $q->h3("Warning: Estimated tax was ($totals_hash{'Estimated Tax'}) but collected tax was ($totals_hash{'Tax'}) ");
    }
    if ($totals_hash{'Payment'}){print "Total Payment : \$$totals_hash{'Payment'}<br />"};    
    #print "Total OtherDiscounts : \$$totals_hash{'OtherDiscounts'}<br />";
    #print "Total PromotionDiscounts : \$$totals_hash{'PromoPriceAdjust'}<br />";
    #print "Total Sell Price: \$$totals_hash{'Sell Price'}<br />";
    #print "Total Tax: \$$totals_hash{'Tax'}<br />";  
    #print "Total Payment: \$$totals_hash{'Payment'}<br />";      
 
    # Was this transaction aborted?
    if ($txn_status) {
        print $q->h3("This transaction was $status_hash{$txn_status}");
    }
    $dbh->disconnect;    
    StandardFooter(); 
    exit;    
} elsif ($report eq "NO_SALES_QRY")    {  

    my $this_report=$report;
    my $next_report;
    my $label;
	
    if ($report eq "NO_SALES_QRY") {        
        $next_report="NO_SALES_RPT";
        $label="No Sales";
    }      
      
    my $mgrcode_options = $q->option({-value=>'0' -selected=>undef}," ");
   
    print $q->h1("$label Transactions Reports");
    # Ask which day to do the report for
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"$next_report"});

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    my @ltm = localtime(time());
    my $month=$ltm[4]+1;
    my $year=$ltm[5]+1900;
    my $enddate = sprintf("%4d-%02d", $year, $month, $ltm[3]);
    my $hm = 1;
    my $startdate = sprintf("%4d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
    $startdate = $enddate; # for now.

    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start month:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
    $calcounter++;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End month:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
  
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
    # Show options
 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"report_opt", -value=>"0", -checked=>"1"}) .            
               $q->font({-size=>2}, "By Vendor")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"report_opt", -value=>"1",}) .            
               $q->font({-size=>2}, "By Department")));                   
    
    print $q->end_table(),"\n";

    print $q->end_form(), "\n";
      #print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");
    print $q->p("Note: This report is by month.  Select any date of the month you wish but the report
                will show all of the data for that entire month.");
    
    StandardFooter();      
} elsif (($report eq "ABORTEDTXN_QRY")||($report eq "VOIDEDTXN_QRY") || 
    ($report eq "RETURNTXN_QRY")|| ($report eq "MERCHSALES_QRY") || 
    ($report eq "NONMERCHSALES_QRY") || ($report eq "MGRCODE_QRY")
	 || ($report eq "DATE_DISC_TXN_QRY") || ($report eq "TE_SALES_QRY")
	  || ($report eq "SU_SALES_QRY")
	 
 
	) 
	
	
    {  

    my $this_report=$report;
    my $next_report;
    my $label;
    if ($report eq "ABORTEDTXN_QRY") {        
        $next_report="ABORTEDTXN_RPT";
        $label="Aborted";
    }
    if ($report eq "VOIDEDTXN_QRY") {        
        $next_report="VOIDEDTXN_RPT";
        $label="Voided";
    }  
    if ($report eq "RETURNTXN_QRY") {        
        $next_report="RETURNTXN_RPT";
        $label="Returned";
    }  
    if ($report eq "MERCHSALES_QRY") {        
        $next_report="MERCHSALES_RPT";
        $label="Merchandise";
    }   
    if ($report eq "NONMERCHSALES_QRY") {        
        $next_report="NONMERCHSALES_RPT";
        $label="Non-Merchandise";
    }  
    if ($report eq "DATE_DISC_TXN_QRY") {        
        $next_report="DATE_DISC_TXN_RPT";
        $label="Date Discrepancy";
    }  
    if ($report eq "TE_SALES_QRY") {        
        $next_report="TE_SALES_RPT";
        $label="Tax Exempt";
    }  	
	if ($report eq "SU_SALES_QRY") {        
        $next_report="SU_SALES_RPT";
        $label="Suspended";
    }  	
      
    my $mgrcode_options = $q->option({-value=>'0' -selected=>undef}," ");
 
    if ($report eq "MGRCODE_QRY") { 
 
        if (defined(%manager_code_hash)) {
            $next_report="MGRCODE_RPT";
            $label="Manager Code";
            foreach my $code (sort keys(%manager_code_hash)) {
                my $desc=$manager_code_hash{$code};
                $mgrcode_options .= $q->option({-value=>$code}, "$code - $desc");
            }          
        } else {
            print $q->h3("Please update the lookupdata.pm file to support this feature");
            StandardFooter(); 
            exit;
        }
    }   
    print $q->h1("$label Transactions Reports");
    # Ask which day to do the report for
    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"$next_report"});

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    my @ltm = localtime(time());
    my $month=$ltm[4]+1;
    my $year=$ltm[5]+1900;
    my $enddate = sprintf("%4d-%02d-%02d", $year, $month, $ltm[3]);
    my $hm = 1;
    my $startdate = sprintf("%4d-%02d-%02d", ($month-$hm < 1 ? $year-1 : $year), ($month-$hm < 1 ? $month+12-$hm :$month-$hm), 1);
    $startdate = $enddate; # for now.

    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
    $calcounter++;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );
    if ($report eq "MGRCODE_QRY") {        
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Manager Code:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"mgrcode", -size=>"1"}), $mgrcode_options)
              ); 
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({  -type=>"checkbox", -name=>"cb_no_reg0"}) .
                   $q->font({-size=>-1}, "Exclude Reg 0")
                  ));   
 			  
 
    }    
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
    print $q->end_table(),"\n";

      print $q->end_form(), "\n";
      #print $q->p("<b>Note:</b> Partial dates must entered in the format <nobr>\"YYYY-MM-DD hh:mm\"</nobr> using the 24 Hour Clock time system.");
    
    
    StandardFooter();  
} elsif ($report eq "ABORTEDTXN_RPT") {   
    print $q->h1("Aborted Transactions Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    
    my $datewhere="where ptp.txndatetime > '$startdate' and ptp.txndatetime < '$enddate 23:59:59'";
    # Show the aborted transactions
    $query = "
    select 
        convert(varchar(19), ptp.txndatetime, 21),
        ptp.cashiernum,
        e.empfirstname,
        e.emplastname,
        ptp.transnum,
        ptp.txnnum,
        tms.skunum,
        tms.extsellprice
 
    from 
        txn_pos_transactions ptp
        join txn_merchandise_sale tms on ptp.transnum = tms.transnum and ptp.txnnum = tms.txnnum
        left outer join employee e on e.cashiernum = ptp.cashiernum 
    $datewhere
    and
        ptp.txnvoidmod1 > 0
    
        
    ";
    
    $tbl_ref = execQuery_arrayref($dbh, $query);
    # Get the totals

    my @fieldsum = ('Total:', '', '', '', '', '',  '',0);
    for my $datarows (1 .. $#$tbl_ref)  {
        my $thisrow = @$tbl_ref[$datarows];        
        $fieldsum[7] += trim($$thisrow[7]);        
    }

    push @$tbl_ref, \@fieldsum;  
    if (recordCount($tbl_ref) > 1) {
        my @format = (
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            {'align' => 'Right', 'currency' => ''},              
        ); 
        printResult_arrayref2($tbl_ref, '', \@format); 
    } else {
        print $q->h3("No aborted transaction records found.");
    }    
    StandardFooter();  
} elsif ($report eq "VOIDEDTXN_RPT") {   
    print $q->h1("Post-Voided Transactions Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of voided transactions
    $query = "
    begin
 
        select
            pt.transnum, pt.txnnum, mt.itemnum
        into #tmpPostVoids
        from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
               mt.storenum=vpt.storenum and
               mt.txnnum=vpt.txnnum and
               mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               pt.regnum = vpt.regnum and
               mt.refnum = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
        where            
            mt.misctype = 2
        and
            mt.itemdatetime > '$startdate' and itemdatetime < '$enddate 23:59:59'
            ;            

        select 
            tms.itemdatetime,
            tms.transnum,
            tms.txnnum,
            tms.itemnum,
            tms.salesprsnid,
            e.empfirstname,
            e.emplastname,
            tms.skunum,
            tms.qty,
            tms.extsellprice
        from
            txn_merchandise_sale tms
            join txn_pos_transactions tpt on tms.transnum=tpt.transnum and tms.txnnum=tpt.txnnum 
            join #tmpPostVoids v on tms.transnum=v.transnum and tms.txnnum=v.txnnum and tms.itemnum = v.itemnum
            left outer join employee e on tms.salesprsnid = e.cashiernum
            
        where 
            tms.itemdatetime > '$startdate'
        order by tms.itemdatetime
            ;
    end
            
    ";
 
    
    $tbl_ref = execQuery_arrayref($dbh, $query);
    # Get the totals

    my @fieldsum = ('Total:', '', '', '', '', '',  '', '', 0);
    for my $datarows (1 .. $#$tbl_ref)  {
        my $thisrow = @$tbl_ref[$datarows];        
        $fieldsum[8] += trim($$thisrow[8]);  
        $fieldsum[9] += trim($$thisrow[9]);          
    }

    push @$tbl_ref, \@fieldsum;  
    if (recordCount($tbl_ref) > 1) {
        my @format = (
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            {'align' => 'Right', 'currency' => ''},              
        ); 
        printResult_arrayref2($tbl_ref, '', \@format); 
    } else {
        print $q->h3("No post-voided transaction records found.");
    }    
    StandardFooter(); 
} elsif ($report eq "RETURNTXN_RPT") {   
    print $q->h1("Returned Transactions Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of voided transactions
    $query = "
        select 
            tms.itemdatetime,
            tms.transnum,
            tms.txnnum,
            tms.itemnum,
            tms.salesprsnid,
            e.empfirstname,
            e.emplastname,
            tms.skunum,
            tms.qty,
            tms.extsellprice
        from
            txn_merchandise_sale tms
            join txn_pos_transactions tpt on tms.transnum=tpt.transnum and tms.txnnum=tpt.txnnum            
            left outer join employee e on tms.salesprsnid = e.cashiernum
            
        where 
            tms.itemdatetime > '$startdate'
        and
            tms.itemdatetime < '$enddate 23:59:59'
        and
            tms.extsellprice < 0
		and
			tpt.txnvoidmod1 = 0			   
        order by 
            tms.itemdatetime
            
    ";
 
    
    $tbl_ref = execQuery_arrayref($dbh, $query);
    # Get the totals

    my @fieldsum = ('Total:', '', '', '', '', '',  '', '', 0);
    for my $datarows (1 .. $#$tbl_ref)  {
        my $thisrow = @$tbl_ref[$datarows];        
        $fieldsum[8] += trim($$thisrow[8]);  
        $fieldsum[9] += trim($$thisrow[9]);          
    }

    push @$tbl_ref, \@fieldsum;  
    if (recordCount($tbl_ref) > 1) {
        my @format = (
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            {'align' => 'Right', 'currency' => ''},              
        ); 
        printResult_arrayref2($tbl_ref, '', \@format); 
    } else {
        print $q->h3("No returned transaction records found.");
    }    
    StandardFooter();  
} elsif ($report eq "MERCHSALES_RPT") {   
    print $q->h1("Merchandise Sales Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of voided transactions
    $query = dequote(<< "    ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
                mt.storenum=vpt.storenum and
                mt.txnnum=vpt.txnnum and
                mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                pt.regnum = vpt.regnum and
                mt.refnum = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            where
                mt.misctype = 2;  
                
            select 
                tms.skunum,            
                p.pludesc,
                sum(tms.qty) as Qty,
                tms.extsellprice as SellPrice,
                sum(tms.extsellprice) as Sales
            from
                txn_merchandise_sale tms
                left outer join txn_pos_transactions tpt on tms.transnum=tpt.transnum and tms.txnnum=tpt.txnnum  
                left outer join plu p on p.plunum = tms.skunum                
                left outer join #tmpPostVoids v on tpt.storenum=v.storenum and tpt.transnum=v.transnum                
            where 
                tms.itemdatetime > '$startdate'
            and
                tms.itemdatetime < '$enddate 23:59:59'
     
            and
                tpt.txnvoidmod1 = 0            
            and
                v.transnum is null
            group by
                tms.skunum,p.pludesc,tms.qty,tms.extsellprice
            order by 
                tms.skunum;
                
            drop table #tmpPostVoids;
        end
    ENDQUERY
 
    $tbl_ref = execQuery_arrayref($dbh, $query);
    # Get the totals
    my %sku_hash;
    my @fieldsum = ('Total:', '', 0, "", 0,);
    for my $datarows (1 .. $#$tbl_ref)  {
        my $thisrow = @$tbl_ref[$datarows]; 
        my $price=$$thisrow[3];
        if ($price < 0) {
            # This is a return
            $$thisrow[2]=(0 - $$thisrow[2]);
        }
        my $sku=$$thisrow[0];
        my $qty=$$thisrow[2];
        $sku_hash{$sku}+=$qty;
        $fieldsum[2] += trim($$thisrow[2]);  
        $fieldsum[4] += trim($$thisrow[4]);          
    }

    push @$tbl_ref, \@fieldsum;  
    if (recordCount($tbl_ref) > 1) {
        my @format = (
  
            "",
            "",
            {'align' => 'Right', 'num' => ''},  
            {'align' => 'Right', 'currency' => ''},  
            {'align' => 'Right', 'currency' => ''},             
        ); 
		
        my @links = ( "$scriptname?report=MERCH_DETAIL&startdate=$startdate&enddate=$enddate&selection=".'$_' );        
        printResult_arrayref2($tbl_ref, \@links, \@format); 
        foreach my $sku (sort keys(%sku_hash)) {
            print "SKU: $sku Qty: $sku_hash{$sku}<br />";
        }
    } else {
        print $q->h3("No returned transaction records found.");
    }    
    StandardFooter(); 
} elsif ($report eq "MERCH_DETAIL") {	
    print $q->h1("Merchandise Sales Reports"); 
    my $sku_selection = trim(($q->param("selection"))[0]); 
	my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    $query = dequote(<< "    ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
                mt.storenum=vpt.storenum and
                mt.txnnum=vpt.txnnum and
                mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                pt.regnum = vpt.regnum and
                mt.refnum = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            where
                mt.misctype = 2;  
                
            select 
				tpt.businessdate,
				tms.itemdatetime,
				tms.transnum,
				tms.txnnum,
                tms.skunum,            
                p.pludesc,
                tms.qty as Qty,
                tms.extsellprice as SellPrice,
                tms.extsellprice as Sales
            from
                txn_merchandise_sale tms
                left outer join txn_pos_transactions tpt on tms.transnum=tpt.transnum and tms.txnnum=tpt.txnnum  
                left outer join plu p on p.plunum = tms.skunum                
                left outer join #tmpPostVoids v on tpt.storenum=v.storenum and tpt.transnum=v.transnum                
            where 
                tms.itemdatetime > '$startdate'
            and
                tms.itemdatetime < '$enddate 23:59:59'
     
            and
                tpt.txnvoidmod1 = 0            
            and
                v.transnum is null
			and
				tms.skunum = '$sku_selection'
            order by 
                tms.itemdatetime;
                
            drop table #tmpPostVoids;
        end
    ENDQUERY
 
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref) > 1) {
        my @format = (
  
            "",
            "",
            {'align' => 'Right', 'num' => ''},  
            {'align' => 'Right', 'currency' => ''},  
            {'align' => 'Right', 'currency' => ''},             
        ); 
    
        printResult_arrayref2($tbl_ref, '', \@format); 
 
    } else {
        print $q->h3("No records found for $sku_selection.");
    }    	
	
    StandardFooter(); 	
} elsif ($report eq "NONMERCHSALES_RPT") {   
    print $q->h1("Non-Merchandise Sales Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of voided transactions
    $query = dequote(<< "    ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
                mt.storenum=vpt.storenum and
                mt.txnnum=vpt.txnnum and
                mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                pt.regnum = vpt.regnum and
                mt.refnum = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            where
                mt.misctype = 2;  
                
            select 
				tnms.itemdatetime, 
                tnms.nonmerchid,            
                nm.nonmerchdesc,
                sum(tnms.qty) as Qty,
                tnms.extsellprice as SellPrice,
                sum(tnms.extsellprice) as Sales
            from
                txn_non_merch_sale tnms
                left outer join txn_pos_transactions tpt on tnms.transnum=tpt.transnum and tnms.txnnum=tpt.txnnum  
                left outer join non_merch nm on nm.nonmerchid = tnms.nonmerchid                
                left outer join #tmpPostVoids v on tpt.storenum=v.storenum and tpt.transnum=v.transnum                
            where 
                tnms.itemdatetime > '$startdate'
            and
                tnms.itemdatetime < '$enddate 23:59:59'
     
            and
                tpt.txnvoidmod1 = 0            
            and
                v.transnum is null
            group by
                tnms.itemdatetime,tnms.nonmerchid,nm.nonmerchdesc,tnms.qty,tnms.extsellprice
            order by 
                tnms.nonmerchid;
                
            drop table #tmpPostVoids;
        end
    ENDQUERY
 
 
     $query = dequote(<< "    ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
                mt.storenum=vpt.storenum and
                mt.txnnum=vpt.txnnum and
                mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                pt.regnum = vpt.regnum and
                mt.refnum = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            where
                mt.misctype = 2;  
                
            select 
				tnms.itemdatetime, 
                tnms.nonmerchid,            
                nm.nonmerchdesc,
                tnms.qty as Qty,
                tnms.extsellprice as SellPrice,
                tnms.extsellprice as Sales,
				tpt.regnum,
				tpt.cashiernum,
				emp.empfirstname,
				emp.emplastname,
				tpt.transnum,
				tpt.txnnum
            from
                txn_non_merch_sale tnms
                left outer join txn_pos_transactions tpt on tnms.transnum=tpt.transnum and tnms.txnnum=tpt.txnnum 
				left outer join employee emp on emp.cashiernum=tpt.cashiernum
                left outer join non_merch nm on nm.nonmerchid = tnms.nonmerchid                
                left outer join #tmpPostVoids v on tpt.storenum=v.storenum and tpt.transnum=v.transnum                
            where 
                tnms.itemdatetime > '$startdate'
            and
                tnms.itemdatetime < '$enddate 23:59:59'
     
            and
                tpt.txnvoidmod1 = 0            
            and
                v.transnum is null
 
            order by 
                tnms.nonmerchid;
                
            drop table #tmpPostVoids;
        end
    ENDQUERY
	
    $tbl_ref = execQuery_arrayref($dbh, $query);
    # Get the totals
    my %id_hash;
    my @fieldsum = ('Total:', "", "", "", 0,0,'','','','','','');
    for my $datarows (1 .. $#$tbl_ref)  {
        my $thisrow = @$tbl_ref[$datarows]; 
 
        my $id=$$thisrow[2];
        my $qty=$$thisrow[3];
        $id_hash{$id}+=$qty;
        #$fieldsum[2] += trim($$thisrow[2]);  
        $fieldsum[4] += trim($$thisrow[4]);  
        $fieldsum[5] += trim($$thisrow[5]);          
    }

    push @$tbl_ref, \@fieldsum;  
    if (recordCount($tbl_ref) > 1) {
        my @format = (
  
            "",
            {'align' => 'Left', 'num' => ''},
            '',
            {'align' => 'Left', 'num' => ''},                    
            {'align' => 'Right', 'currency' => ''},  
            {'align' => 'Right', 'currency' => ''},              
        ); 
        printResult_arrayref2($tbl_ref, '', \@format); 
        print "<br />";
        foreach my $id (sort keys(%id_hash)) {
            print "NonMerchId: $id Qty: $id_hash{$id}<br />";
        }
    } else {
        print $q->h3("No Non Merchandise transaction records found.");
    }    
    StandardFooter();  


	
} elsif ($report eq "TE_SALES_RPT") {   
    print $q->h1("Tax Exempt Sales Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    my $tax_exempt_clause=" tudt.udefinedtxnid = 3 ";
	my $storenum=getStoreNum($dbh);
	if ($storenum==7037) {	
		$tax_exempt_clause=" tudt.udefinedtxnid in ('3','5') ";
	}
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of voided transactions 2014-07-24 I think that this is pretty accurate - kdg
 
    $query = dequote(<< "    ENDQUERY"); 
        select 
            tmt.refnum,
            tmt.transnum,
            tpt.regnum
        from
            txn_miscellaneous_trans tmt
            join txn_pos_transactions tpt on tmt.transnum = tpt.transnum and tmt.businessdate = tpt.businessdate
        where
			tpt.txndatetime > '$startdate'
		and
			tpt.txndatetime < '$enddate 23:59:59'  
        and
            tmt.misctype = 2
        
    ENDQUERY
    my %post_void_hash;
    my $sth = $dbh->prepare($query);        
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {              
        my $txnnum=$tmp[0];
        my $transnum=$tmp[1];
        my $regnum=$tmp[2];
        my $post_void="$transnum"."-"."$txnnum"."-"."$regnum";
        $post_void_hash{$post_void}=1;         
    }        
    
    
    #print $q->pre("$query");
 
    ###
    $query = dequote(<< "    ENDQUERY");     				
		select 
			tudt.transnum,
			tudt.txnnum,			
			tpt.regnum,
			tpt.cashiernum,
			tudt.itemdatetime,
            pc.lastname,
            pc.firstname
            
		from
			txn_user_defined_txn tudt
			left outer join txn_pos_transactions tpt on tpt.transnum=tudt.transnum and tpt.txnnum=tudt.txnnum   
            left outer join txn_customer_admin tca on tpt.transnum=tca.transnum and tpt.txnnum=tca.txnnum
            left outer join pa_customer pc on tca.custnum = pc.custnum
		where 
			tudt.itemdatetime > '$startdate'
		and
			tudt.itemdatetime < '$enddate 23:59:59'

		and
			tpt.txnvoidmod1 = 0            
		and
			$tax_exempt_clause 
		order by
			tudt.itemdatetime
    ENDQUERY
 
    #print $q->pre("$query");
    my @header=(
        "TranKey",
        "Txnnum",
        "Regnum",
        "SalesPerson",
        "Customer",
        "DateTime"
    );
    my @array_to_print=\@header;
    $sth = $dbh->prepare($query);        
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {
        my $Transnum=$tmp[0];
        my $Txnnum=$tmp[1];
        my $Regnum=$tmp[2];
        my $SalesPerson=$tmp[3];
        my $DateTime=$tmp[4];  
        my $lastname=$tmp[5];
        my $firstname=$tmp[6];
        my $Customer=$lastname;
        if ($firstname) {
            $Customer="$lastname".", "."$firstname";
        }
        my $key="$Transnum"."-"."$Txnnum"."-"."$Regnum";
        
        unless (defined($post_void_hash{$key})) {
            my @array=("$key","$Txnnum","$Regnum","$SalesPerson","$Customer","$DateTime");
            push(@array_to_print,\@array);
        }
    }
 
    if (recordCount(\@array_to_print) > 0) { 
        my @links = ( "$scriptname?report=TXN_REPORT2&selection=".'$_' );  
        printResult_arrayref2(\@array_to_print, \@links); 
 
    } else {
        print $q->h3("No Tax Exempt transaction records found.");
    }      
=pod    
    $tbl_ref = execQuery_arrayref($dbh, $query);
 
    if (recordCount($tbl_ref) > 1) {
	 
        printResult_arrayref2($tbl_ref, ''); 
 
    } else {
        print $q->h3("No Tax Exempt transaction records found.");
    }    
=cut    
    StandardFooter();  	
	
} elsif ($report eq "SU_SALES_RPT") {   
    print $q->h1("Suspended Sales Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");

	
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of suspended transactions
    # Suspended transactions are misc trans type 47.  Resumed are type 46.  Each transaction suspended is given an ID number which shows up
    # in the ref column for the txn_miscellaneous_trans table.  After it is resumed, the number is freed up to be re-used.  Each time a transaction
    # is suspended, it will take the first free ID number.  For example, if four transactions are suspended and they get ID's 1,2,3, and 4 but then
    # transaction #2 is resumed and completed, the next transaction suspended with get ID #2 rather than #5.
    # To determine if the transaction is ever resumed, look for txn_miscellaneous_transaction type 46.  The ref number shows the ID that was resumed.
    
    $query = dequote(<< "    ENDQUERY");     				
		select 
 			
			tpt.regnum,
            tpt.txnnum,
			tpt.cashiernum,
           
            convert(varchar(10), tpt.businessdate,21),
         
			tpt.txndatetime,
            tmt.misctype,
            tmt.refnum
		from
			Txn_POS_Transactions tpt
			join Txn_Miscellaneous_Trans tmt on tpt.transnum = tmt.transnum                            
										 
		where 
			tpt.txndatetime > '$startdate'
		and
			tpt.txndatetime < '$enddate 23:59:59'
 
		and
			(tmt.misctype = 47
        or
            tmt.misctype = 46)
		order by
			tpt.txndatetime
 
 
 
    ENDQUERY
 
    $tbl_ref = execQuery_arrayref($dbh, $query);
 
    my %suspend_hash=();
    
    if (recordCount($tbl_ref) > 1) {
        for my $datarows (1 .. $#$tbl_ref)  {
            my $thisrow = @$tbl_ref[$datarows]; 
            my $reg = $$thisrow[0];
            my $txn = $$thisrow[1];
            my $bdate = $$thisrow[3];
            my $id=$$thisrow[5];
            my $refnum=$$thisrow[6];
            my $key = "${reg}_${bdate}_${refnum}";
            
=pod            
            my @array = ("$reg","$txn","$id");
            my @old_array = ();
            if ($suspend_hash{$bdate}) {
                my $ref = $suspend_hash{$bdate};
                @old_array = @$ref;
            }
            push(@old_array,\@array);
            $suspend_hash{$bdate} = \@old_array;
=cut            
            if ($id == 46) {
                $$thisrow[5] = "46 - Resumed"; 
                $suspend_hash{$key} = 0;
            }
            if ($id == 47) {
                $$thisrow[5] = "47 - Suspended";  
                $suspend_hash{$key} = $txn;                
            }         
        }
        printResult_arrayref2($tbl_ref, ''); 
 
        # Iterate through the hash and determine which transactions were completed
 
        my @list = ();
        foreach my $key (sort keys(%suspend_hash)) {        
            if ($suspend_hash{$key}) {            
                my $txn = $suspend_hash{$key};
                my @tmp=split(/_/,$key);
                my $reg = $tmp[0];
                my $bdate = $tmp[1];
                my $msg = "Transaction: $txn Reg: $reg Date: $bdate  ";
                push(@list,$msg);
            }            
        }        
        if ($#list > -1) {
            my @slist = sort(@list);
            print "<br />The following transactions were not resumed:<br />";
            foreach my $s (@slist) {
                print "$s<br />";
            }
        }
 
    } else {
        print $q->h3("Suspended transaction records found.");
    }    
    StandardFooter();  	
	
} elsif ($report eq "NO_SALES_RPT") {   
    print $q->h1("No Sales Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    my $report_opt = trim(($q->param("report_opt"))[0]); 
    my $group_by='';
    my $select='';
   
    if ($report_opt == 0) {
        # Report by Vendor
        $select="v.vendorname";
        $group_by="group by ih.periodstart,v.vendorname";
        
    }
    if ($report_opt == 1) {
        # Report by Department
        $select="d.deptdescription";
        $group_by="group by ih.periodstart,d.deptdescription";
    }   
    my $order_by=$select;    
    print $q->p("From $startdate to $enddate");

    $query = dequote(<< "    ENDQUERY");     				

        select        
            $select,
            convert(varchar(10), ih.periodstart, 21) as Month,
            count(ih.plunum) as SkuCount            
        from 
            inventory_history ih
            join plu p on ih.plunum=p.plunum
            join department d on p.deptnum=d.deptnum
            join vendor v on p.vendorid=v.vendorid
        where
            (ih.periodstart > '$startdate'
            or
            ih.periodstart = '$startdate')
        and
            (ih.periodstart < '$enddate'
            or
            ih.periodstart = '$enddate')
        and
            ih.openqty > 0 
        and 
            ih.qtysold = 0 
        $group_by
        order by ih.periodstart,
        $order_by
 
    ENDQUERY
     
    $tbl_ref = execQuery_arrayref($dbh, $query);
    my @array_to_print;
    if (recordCount($tbl_ref) > 1) {
        # Add the headings;
        push(@array_to_print,@$tbl_ref[0]);
        my @format = (  
            "",
            "",
            {'align' => 'Left', 'num' => ''},             
        ); 
        my $last_date;
        for my $datarows (1 .. $#$tbl_ref)  {        
            my $thisrow = @$tbl_ref[$datarows];  
            
            my $date=$$thisrow[1];
            if ($last_date) {
                unless ($date eq $last_date) {
                    # We have shifted to a new date - Add a blank row
                    my @array=("","","");
                    push(@array_to_print,\@array);                    
                    $last_date=$date;
                }
            } else {
                $last_date=$date;
            }
            push(@array_to_print,$thisrow);
        }
        
        my @links = ( "$scriptname?report=NO_SALES_RPT_DTL&startdate=$startdate&enddate=$enddate&report_opt=".'$_' );          
        printResult_arrayref2(\@array_to_print, \@links, \@format);  
    } else {
        print $q->h3("No records found.");
    }    
    StandardFooter();  	
} elsif ($report eq "NO_SALES_RPT_DTL") {   
    print $q->h1("No Sales Reports"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    my $report_opt = trim(($q->param("report_opt"))[0]); 
 
   
    my $group_by='';
    my $select='';
    my $where='';
    my $label;
   
    if ($report_opt =~ /^\d/) {
        # Report by Department
        $select="p.plunum";
        $group_by="group by ih.periodstart,p.plunum";
        $where=" and d.deptdescription like '$report_opt' ";
        $label="Department";
    }
    if ($report_opt =~ /^\D/) {
        # Report by Vendor        
        $select="p.plunum";
        $group_by="group by ih.periodstart,p.plunum";
        $where=" and v.vendorname like '$report_opt' ";        
        $label="Vendor";
    }   
    print $q->h2("$label: $report_opt");     
    my $order_by=$select;    
    print $q->p("From $startdate to $enddate");

    $query = dequote(<< "    ENDQUERY");     				

        select        
            p.plunum,
            p.pludesc,
            convert(varchar(10), ih.periodstart, 21) as Month
                      
        from 
            inventory_history ih
            join plu p on ih.plunum=p.plunum
            join department d on p.deptnum=d.deptnum
            join vendor v on p.vendorid=v.vendorid
        where
            (ih.periodstart > '$startdate'
            or
            ih.periodstart = '$startdate')
        and
            (ih.periodstart < '$enddate'
            or
            ih.periodstart = '$enddate')
        and
            ih.openqty > 0 
        and 
            ih.qtysold = 0 
        $where

        order by ih.periodstart,
            $order_by
 
    ENDQUERY
    
    $tbl_ref = execQuery_arrayref($dbh, $query);
    my @array_to_print;
    if (recordCount($tbl_ref)) {
        # Add the headings;
        push(@array_to_print,@$tbl_ref[0]);
        my @format = (  
            "",
            "",
            "",
            {'align' => 'Left', 'num' => ''},             
        ); 
        my $last_date;
        for my $datarows (1 .. $#$tbl_ref)  {        
            my $thisrow = @$tbl_ref[$datarows];    
            my $sku=$$thisrow[0];
            # Show the SKU info in a pop-up window
            $$thisrow[0]=$q->a({-href=>"javascript:void(null)", -onClick=>"skudetail=window.open('/cgi-bin/i_products.pl?report=PRODINFO&SKU=$sku','skuinfo','width=500,height=390,scrollbars,resizable'); skudetail.focus();", -class=>"datalink"}, $sku);                      
            my $date=$$thisrow[2];
            if ($last_date) {
                unless ($date eq $last_date) {
                    # We have shifted to a new date - Add a blank row
                    my @array=("","","");
                    push(@array_to_print,\@array);                    
                    $last_date=$date;
                }
            } else {
                $last_date=$date;
            }
            push(@array_to_print,$thisrow);
        }
      
        my @links = ( "$scriptname?report=PHYINVRPT_DTL&pit=".'$_' );          
        printResult_arrayref2(\@array_to_print, \@links, \@format);  
    } else {
        print $q->h3("No records found.");
    }    
    StandardFooter();      
} elsif ($report eq "DATE_DISC_TXN_RPT") {   
    print $q->h1("Date Discrepancy Report"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    print $q->p("From $startdate to $enddate");
    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
    # Get a list of voided transactions
    $query = dequote(<< "    ENDQUERY");
        begin
            select
                mt.storenum, pt.transnum
            into #tmpPostVoids
            from
                Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions vpt on
                mt.storenum=vpt.storenum and
                mt.txnnum=vpt.txnnum and
                mt.transnum=vpt.transnum
            join Txn_POS_Transactions pt on
                mt.storenum = pt.storenum and
                pt.regnum = vpt.regnum and
                mt.refnum = pt.txnnum   and
                abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            where
                mt.misctype = 2;  
                
            select 
				convert(varchar(10),tpt.modtime,21) as "ServerDate",
				convert(varchar(10),tpt.txndatetime,21) as "RegDate",
                tpt.regnum,
				tpt.cashiernum,
				tpt.transnum,
				tpt.txnnum,
				tpt.businessdate,
				tpt.txndatetime,
				tpt.modtime
		 
 
            from
                 
                txn_pos_transactions tpt    
				left outer join txn_miscellaneous_trans tmt on tpt.transnum = tmt.transnum and tpt.txnnum = tmt.txnnum
                left outer join #tmpPostVoids v on tpt.storenum=v.storenum and tpt.transnum=v.transnum                
            where 
                tpt.modtime > '$startdate'
            and
                tpt.modtime < '$enddate 23:59:59'
			and
				(convert(varchar(10),tpt.modtime,21) not like
				convert(varchar(10),tpt.txndatetime,21))
            and
                tpt.txnvoidmod1 = 0            
            and
                v.transnum is null
			and
				(tmt.refnum = 0 or tmt.refnum is null)
            order by 
                tpt.modtime;
                
            drop table #tmpPostVoids;
        end
    ENDQUERY
 
    $tbl_ref = execQuery_arrayref($dbh, $query);
 
    if (recordCount($tbl_ref)) {
        printResult_arrayref2($tbl_ref); 
    } else {
        print $q->h3("No transactions with date discrepancies found.");
    }    
    StandardFooter();  
} elsif ($report eq "MGRCODE_RPT") {   
    print $q->h1("Manager Code Report"); 
    my $startdate = trim(($q->param("startdate"))[0]); 
    my $enddate = trim(($q->param("enddate"))[0]); 
    my $mgrcode = trim(($q->param("mgrcode"))[0]); 
	my $cb_no_reg0 =  trim(($q->param("cb_no_reg0"))[0]);
 
    print $q->p("From $startdate to $enddate - Manager Code: $mgrcode $manager_code_hash{$mgrcode}");
    
    my $datewhere="where pt.txndatetime > '$startdate' and pt.txndatetime < '$enddate 23:59:59'";
	my $regwhere;
	if ($cb_no_reg0) {
		$regwhere=" and tpt.regnum <> 0 ";
	}
    # Get a list of voided transactions
 
    # find any post-voided transactions for this business day
    # This is a different technique for eliminating post-voided transactions from the final result
    # Not certain that this is more accurate or necessrily easier - kdg
    $query = dequote(<< "    ENDQUERY"); 
        select
            refnum,
            businessdate
        from 
            txn_miscellaneous_trans
        where
            businessdate in 
                (select businessdate from txn_pos_transactions where 
                txndatetime > '$startdate' and txndatetime < '$enddate 23:59:59')
            and
                misctype = 2
    ENDQUERY
    
    my @post_voids;
    my $sth = $dbh->prepare($query);        
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {              
        my $txnnum=$tmp[0];
        my $businessdate=$tmp[1];
        push(@post_voids,"${txnnum}-${businessdate}");
    }    
    
    
    $query = dequote(<< "    ENDQUERY"); 
                
            select 
                tmt.transnum,
                tmt.txnnum,                
                tpt.regnum as Register,
                tpt.businessdate,
                tpt.txndatetime,
                tmt.cashiernum,                
                '' as Cashier,
                tmt.refnum
            from
                txn_miscellaneous_trans tmt  
                join txn_pos_transactions tpt on tmt.transnum = tpt.transnum and tmt.txnnum = tpt.txnnum
                
            where 
                tpt.txndatetime > '$startdate'
            and
                tpt.txndatetime < '$enddate 23:59:59'     
            and
                tpt.txnvoidmod1 = 0            
            and
                tmt.refnum = $mgrcode
			$regwhere				
            order by 
                tpt.txndatetime;                 
    ENDQUERY
 
    $tbl_ref = execQuery_arrayref($dbh, $query);
 
    my $tbl_ref2 = [];
    my $header=@$tbl_ref[0];
    push @$tbl_ref2, $header;
    
    for my $datarows (1 .. $#$tbl_ref)  {        
        my $thisrow = @$tbl_ref[$datarows];         
        my $transaction=$$thisrow[0];
        my $businessdate=$$thisrow[3];
        my $cashier=$$thisrow[5];		
        # if we have a cashier number - find the name
        if ($cashier) {
            my $q="select empfirstname,emplastname from employee where cashiernum = '$cashier'";        
            my $sth = $dbh->prepare($q);        
            $sth->execute();                
            while (my @t=$sth->fetchrow_array()) {              
                my $firstname=$t[0];
                my $lastname=$t[1];            
                $$thisrow[6]="$firstname $lastname";
            }
        } else {
            # Some transactions have the cashier number with the miscellaneous transaction and some are with the txn_pos_transaction
            my $q="select cashiernum from txn_pos_transactions where businessdate like '$businessdate' and transnum like '$transaction'";        
            my $sth = $dbh->prepare($q);        
            $sth->execute();                
            while (my @t=$sth->fetchrow_array()) { 
                $cashier=$t[0];
 
            }      
            $q="select empfirstname,emplastname from employee where cashiernum = '$cashier'";        
            $sth = $dbh->prepare($q);        
            $sth->execute();                
            while (my @t=$sth->fetchrow_array()) {              
                my $firstname=$t[0];
                my $lastname=$t[1];            
                $$thisrow[6]="$firstname $lastname";
				$$thisrow[5]=$cashier;					
            }            
        }
        if ($cashier == 0) {
           $$thisrow[6]="unknown";
        }
        
        my @transaction_info=split(/-/,$transaction);
        my $txnnum=$transaction_info[1];
        my $voided=0;
        foreach my $pvoid (@post_voids) {            
            my @tmp=split(/-/,$pvoid);
            my $pv_txnnum=$tmp[0];
            my $pv_businessdate=$tmp[1];  
            if (($txnnum == $pv_txnnum) && ($businessdate eq $pv_businessdate)){
                $voided=1;
            }
        }
        unless ($voided) {            
            push @$tbl_ref2, $thisrow;
            my $count=recordCount($tbl_ref2);            
        }
        
    }
            
    if (recordCount($tbl_ref2) ) {        
       # my @links = ( "$scriptname?report=TXN_REPORT2&selection=".'$_' );
        printResult_arrayref2($tbl_ref2); 
        #print $q->p("Click on the transaction to see the details of that transaction");
    } else {
        print $q->h3("No manager code $mgrcode transaction records found.");
    }    
    StandardFooter();         
} elsif ($report eq "PHYINVRPT") {   
    print $q->h1("Physical Inventory Reports");  
    # Get a list of the physical inventory records
    $query = "
    select 
        pitransnum,
        documentnum,
        txnstatus,
        origin,
        txndate,
        reason,
        invdate
    from 
        inventory_physical
    order by 
        pitransnum
    ";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @links = ( "$scriptname?report=PHYINVRPT_DTL&pit=".'$_' );
        printResult_arrayref2($tbl_ref, \@links); 
    } else {
        print $q->h3("No physical inventory records found.");
    }
    
     
    StandardFooter();    
} elsif ($report eq "PHYINVRPT_DTL") {   
    print $q->h1("Physical Inventory Reports: Detail");  
    
    my $pit=($q->param("pit"))[0];    
    # Get a list of the physical inventory records
    $query = "
    select 
        *
    from 
        inventory_physical_dtl
    where
        pitransnum = '$pit'
    ";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        
        printResult_arrayref2($tbl_ref); 
    } else {
        print $q->h3("No physical inventory records found.");
    }
    
     
    StandardFooter();      
} elsif ($report eq "VARRPT") {   
    print $q->h1("Current Variance.tsv");  
    my $varfile="c:/temp/inventory/variance.tsv";
    my @file_info;
    my @header;
    my @array_to_print;
    if (-e "$varfile") {
        open (FILE,"$varfile");
        @file_info=(<FILE>);
        close FILE;
        @header=split(/\t/,$file_info[0]);
        @array_to_print=\@header;
        for my $datarows (1 .. $#file_info) {
            my $thisrow = $file_info[$datarows];
            my @array=split(/\t/,$thisrow);
            push(@array_to_print,\@array);
        }
        if ($#array_to_print > -1) {  
            TTV::cgiutilities::printResult_arrayref2(\@array_to_print); 
        } else {
            print $q->h3("Found variance.tsv file but found nothing in it.");
        }
    } else {
        print $q->h3("No variance.tsv file found in c:/temp/inventory folder");
    }
    StandardFooter();  
} elsif ($report eq "VARTBLRPT") {   
    print $q->h1("Current Variance Table");  
	my %valuation_hash;
	my $sqlany_dbh = openODBCDriver($dsn);	
    # Is the variance table loaded?
    my $table="elderInvPhyVariance";
    my $table_found=0;
    $query = "select table_name,count from systable where creator = 1 and table_type = 'BASE' and table_name = '$table'";
    
    my $sth = $dbh->prepare($query);
    $sth->execute();            
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0] eq "$table") {                    
            $table_found=1;		                    
        }
    }    
    unless ($table_found) {
        print $q->h4("No variance table found in the database");
        $sqlany_dbh->disconnect;
        StandardFooter();  
        exit;
    }
    $query = "select ReportDate from $table group by ReportDate";  
    $sth = $dbh->prepare($query);
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {                                      
        $table_found=$tmp[0];		                                        
    }
    print $q->p("Physical Inventory variance table found dated $table_found");    
	# Get the totals:
	$query = "
	SELECT 
		sum(Onhand * Cost),
		sum(Onhand),
		sum(PI_Count * Cost),
		sum(PI_Count),
		sum(ReCount * Cost),
		sum(ReCount)
	from
		elderInvPhyVariance 

	";        
	
	$sth = $dbh->prepare($query);
	$sth->execute();                
	while (my @tmp=$sth->fetchrow_array()) {                                      

		my $onhandv=sprintf("%.2f",$tmp[0]) ;
		my $onhandq= sprintf("%d",$tmp[1]);
		my $PI_Countv=sprintf("%.2f",$tmp[2]) ;
		my $PI_Countq= sprintf("%d",$tmp[3]);   
		my $ReCountv=sprintf("%.2f",$tmp[4]) ;
		my $ReCountq= sprintf("%d",$tmp[5]);   
		my $Variancev=($ReCountv - $onhandv);
		my $Varianceq=($ReCountq - $onhandq);
		
		$valuation_hash{"Onhand"} = "$onhandq | $onhandv";
		$valuation_hash{"PI_Count"} = "$PI_Countq | $PI_Countv";
		$valuation_hash{"ReCount"} = "$ReCountq | $ReCountv";
		$valuation_hash{"Variance"} = "$Varianceq | $Variancev";

	}
    # Show the basic valuation 
    if (keys(%valuation_hash)) {
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->Tr(
               $q->td({-class=>'tablehead', -nowrap=>undef, -align=>'left'}, $q->b("Desc")),
               $q->td({-class=>'tablehead', -nowrap=>undef, -align=>'left'}, $q->b("Quantity")),  
               $q->td({-class=>'tablehead', -nowrap=>undef, -align=>'left'}, $q->b("Valuation")),               
              );
        foreach my $key (sort keys(%valuation_hash)) { 
            my $value=$valuation_hash{$key};                
            my @tmp=split(/\|/,$value);
            my $qty=formatNumeric($tmp[0]);                
            my $valuation=formatCurrency($tmp[1]);                
            print $q->Tr( 
               $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, $key),
               $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'right'}, $qty),               
               $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'right'}, $valuation),               
              );
        }              
        print $q->end_table(),"\n";  
        print $q->p();
    
    }  
    # Show valuation by vendor
    $query = dequote(<< "    ENDQUERY"); 
	SELECT 
        v.vendorname,
		
		sum(ipv.Onhand) as "On Hand Quantity",
        sum(ipv.Onhand * ipv.Cost) as "On Hand Valuation",
        sum(ipv.PI_Count) as "PI Quantity",
		sum(ipv.PI_Count * ipv.Cost) as "PI Valuation",
		sum(ipv.ReCount) as "Recount Quantity",
		sum(ipv.ReCount * ipv.Cost) as "Recount Valuation"
		
	from
		elderInvPhyVariance ipv
        left outer join plu p on ipv.sku = p.plunum
        left outer join vendor v on p.vendorid = v.vendorid
    group by
        p.vendorid,v.vendorname
    ENDQUERY
 
    #print $q->pre("$query");
    
    my $tbl_ref = execQuery_arrayref($dbh, $query); 
 
    if (recordCount($tbl_ref)) {              
        print $q->h3("Valuation By Vendor");
 
        my @fieldsum = ('Total:', 0,0,0,0,0,0);
        for my $datarows (1 .. $#$tbl_ref)  {
            my $thisrow = @$tbl_ref[$datarows]; 
     
            for my $field (1, 2, 3, 4, 5, 6)  {
                $fieldsum[$field] += trim($$thisrow[$field]);
            }        
        }       
        push @$tbl_ref, \@fieldsum;        
        my @format = (
            "",
            {'align' => 'Right', 'num' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'num' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'num' => ''},
            {'align' => 'Right', 'currency' => ''},                                      
        ); 
        printResult_arrayref2($tbl_ref, '', \@format); 
    }
    
	$sqlany_dbh->disconnect;	
    StandardFooter();  

###
} elsif ($report eq "IMPORT_SALES_QRY") {  
    my $verified   = trim(($q->param("verified"))[0]);
    my $uid   = trim(($q->param("uid"))[0]);
   
    my $pwd = trim(($q->param("pwd"))[0]);  
    if ($uid) {
        unless ($verified) {
            my $sqlany_dbh = openODBCDriver($dsn);	
            unless (doValidation($sqlany_dbh, $uid, $pwd)) {
                print $q->h2("ERROR: Failed validate user $uid");
                StandardFooter(); 
                $sqlany_dbh->disconnect;	
                exit;
            } else {
                $verified=1; 
                $sqlany_dbh->disconnect;	        
            }    
        }
    }
###
    unless ($verified) {
        print $q->p("Welcome to...");
        print $q->h2("&nbsp;" x 4 . " Offsite Sales Import Tool."); 

        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"IMPORT_SALES_QRY"});
        #print $q->input({-type=>"hidden", -name=>"verified", -value=>"1"});    
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("Enter User/Password")));
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "UserID:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"uid", -size=>"4", -value=>""}))
                  );
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Password:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'password', -name=>"pwd", -size=>"4", -value=>""}))
                  );
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        print $q->p();
### 
    } else {
        # Check if the UID is a cashier number or not.  If it is not, convert it to one.
        $uid=getCashier($uid);
        print $q->h1("Import Offsites Sales from CSV file: Select File");  
        print $q->p("Select the file to be uploaded.");
         
        print $q->start_multipart_form({-action=>"$scriptname", -method=>'post'}), "\n";     
        print $q->input({-type=>"hidden", -name=>"report", -value=>"IMPORT_SALES"});
        print $q->input({-type=>"hidden", -name=>"uid", -value=>"$uid"});  

        print $q->filefield
                    (-name      => 'source',
                    -size      => 40,
                    -maxlength => 80
                    );

        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "\&nbsp;"),
            );

        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({-accesskey=>'G', -value=>"   ENTER   "}))
            );
        print $q->end_form(), "\n";
    }
    StandardFooter(); 
###
} elsif ($report eq "IMPORT_SALES") {   
        print $q->h1("Import Offsites Sales from CSV file: Read File");  
        $CGI::POST_MAX = 1024 * 100;  # maximum upload filesize is 100K
        my $verified   = trim(($q->param("verified"))[0]);
        my $source   = trim(($q->param("source"))[0]);
        my $file   = trim(($q->param("file"))[0]);
        my $uid   = trim(($q->param("uid"))[0]);
        my $total_price   = trim(($q->param("total_price"))[0]);
        
		my $required_file="offsite_sales.txt";
        my $filename;
        my @file_contents;
        my @results;
        my $upload_dir="/temp";

        # Check that we can find various necessary components
        unless (-d $upload_dir) {
           print $q->p("Failed to find $upload_dir");
           StandardFooter();
           exit;
        }
 
 
        if ($verified) {
            # Now call the perl script to process the file just uploaded
            unless (-e $file) {
                print $q->p("ERROR: #2 - Failed to locate file ($file)");
                StandardFooter();
                exit;
            }
            my $result_code=process_offsite_sale_file($file, $uid, $total_price);
            #  I am using the result_code variable rather than reading what is returned because
            # the function may print various messages and those get scooped up in the return.
            if ($result_code) {
                LogMsg("$file processed successfully");
            } else {
                LogMsg("Error processing $file");
                StandardFooter();
                exit;
            }

        } else {
            # Upload the file
            if ($source) {
                $source=~s/\\/\//g;
            } else {
                print $q->p("ERROR: #1 - Failed to find source ($source)");
                StandardFooter();
                exit;
            }
            my $upload_filehandle = $q->upload('source');
            my $file=basename($source);
            # Only permit a specific file to be uploaded
            unless ($file =~ /$required_file/i) {
                print $q->p("Sorry, the file name must be $required_file.  ");
                print $q->p("$file is unacceptable");
                StandardFooter();
                exit;
            }
            $file="${upload_dir}/${file}";
            open ( UPLOADFILE, ">$file" ) or die "Error: ($file) $!";
            binmode UPLOADFILE;

            while ( <$upload_filehandle> ) {
                print UPLOADFILE;
                push(@file_contents,$_);
            }
            close UPLOADFILE;

            my $count=0;
            my $dbh = openODBCDriver($dsn);      
            my $total_price=0;
            foreach my $entry (@file_contents) {
				my @tmp=split(/\,/,$entry);
				my $sku=$tmp[0];
				my $qty=$tmp[1];
                # Get the price
                my $price;
                my $query=" select retailprice from plu where plunum = '$sku'";
                $sth = $dbh->prepare($query);
                $sth->execute();
                while (my @tmp=$sth->fetchrow_array()) {
                    if ($tmp[0]) {
                        $price=sprintf("%.2f",$tmp[0]);            
                    }
                }      
                my $extprice=sprintf("%.2f",($price * $qty));  
                $total_price+=$extprice;                
                $count++;
				print "Entry $count: SKU: $sku Qty: $qty Price: \$$price ExtPrice: \$$extprice<br />";
            }
            $dbh->disconnect; 
            print $q->p("$count entries found");
            #my $total_price=sprintf("%.2f",pre_process_offsite_sale_file($file,$uid));
            $total_price=sprintf("%.2f",$total_price);
            print $q->p("Total Price: \$$total_price");

            # Get verification

            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"IMPORT_SALES"});
            print $q->input({-type=>"hidden", -name=>"file", -value=>"$file"});
            print $q->input({-type=>"hidden", -name=>"verified", -value=>"yes"});
            print $q->input({-type=>"hidden", -name=>"uid", -value=>"$uid"});
            print $q->input({-type=>"hidden", -name=>"total_price", -value=>"$total_price"});

            print $q->submit({-accesskey=>'C', -value=>"   Process this Offsite Sale file   "});
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
        }

        print "<br>\n";	
	my $sqlany_dbh = openODBCDriver($dsn);
    $sqlany_dbh->disconnect;
 
	print "<br />";
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=IMPORT_SALES_QRY&uid=$uid&verified=yes"}, "Import another Sale");  
    print "<br />";    
    StandardFooter();      
} elsif ($tool =~ /^ECONF/) {
    print $q->h3("ElderConfig Tool.");
    my $ckey=($q->param("ckey"))[0];      
    if ($tool eq "ECONF") {
        my @reportlist = (	   
            ["View Elder Config Table", "/cgi-bin/i_toolshed.pl", "ECONFV",
                "View last current elderConfig table"              
            ],  
            ["Insert to Elder Config Table", "/cgi-bin/i_toolshed.pl", "ECONFI",
                "Add a new entry to the elderConfig table"              
            ], 

            );
        if (1) {
            foreach my $reportref (@reportlist) {
                my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
                print $q->p(
                $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
                $reportnote );
            }
        }    
    } elsif ($tool eq "ECONFV") {
        # Display the elderConfig table
        $query = "
        select 
            *
        from 
            elderconfig
        ";
        $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {  
            my @links = ( "$scriptname?report=ECONFA&ckey=".'$_' );
            printResult_arrayref2($tbl_ref, \@links); 
        } else {
            print $q->h3("No info found in elderconfig.");
        }
        $dbh->disconnect;
    } elsif ($tool eq "ECONFI") {
        print $q->h3("Insert Tool.");     
        # Get new key & value 
        print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});
        print $q->input({-type=>"hidden", -name=>"tool", -value=>"IECONF"});
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Key:"),
            $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->input({-type=>'text', -name=>"new_key_selection", -size=>"25" })));     
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Value:"),
            $q->td({-class=>'tabledatanb', -align=>'left'},
            $q->input({-type=>'text', -name=>"new_value_selection", -size=>"25" })));              
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({  -value=>"Submit"}))
                      );
        print $q->end_table(),"\n";
        print $q->end_form(), "\n";         
    } elsif ($tool eq "ECONFM") {
    } elsif ($tool =~ /^ECONFA/) {
        print $q->h3("Alter Tool."); 
        $query = "
        select 
            *
        from 
            elderconfig
        where
            ckey like '$ckey'
        ";      
        $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {              
            printResult_arrayref2($tbl_ref); 
            # Get new value for this key
            print $q->p("Change the value for this key");
            print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});
            print $q->input({-type=>"hidden", -name=>"tool", -value=>"MECONF"});
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Value:"),
                $q->td({-class=>'tabledatanb', -align=>'left'},
                $q->input({-type=>'text', -name=>"new_value_selection", -size=>"25" })));     
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                           $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                                  $q->submit({  -value=>"Submit"}))
                          );
            print $q->end_table(),"\n";
            print $q->end_form(), "\n"; 
            print "<br />";
            # Define new key for this value
            print $q->p("Rename the key for this value");
            print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});
            print $q->input({-type=>"hidden", -name=>"tool", -value=>"MECONF"});
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Key:"),
                $q->td({-class=>'tabledatanb', -align=>'left'},
                $q->input({-type=>'text', -name=>"new_key_selection", -size=>"25" })));     
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                           $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                                  $q->submit({  -value=>"Submit"}))
                          );
            print $q->end_table(),"\n";
            print $q->end_form(), "\n"; 
            print "<br />";			
            print $q->p("Delete this entry");
            print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});
            print $q->input({-type=>"hidden", -name=>"tool", -value=>"DECONF"});
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	
    
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                           $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                                  $q->submit({  -value=>"Delete"}))
                          );
            print $q->end_table(),"\n";
            print $q->end_form(), "\n";             
        } else {
            print $q->h3("No info found in elderconfig for key $ckey.");
        }
        
    }
    StandardFooter();  
} elsif ($tool eq "MECONF") {
    print $q->h3("ElderConfig Tool Modification.");
    my $ckey=($q->param("ckey"))[0]; 
    my $new_value_selection=($q->param("new_value_selection"))[0]; 
	my $new_key_selection=($q->param("new_key_selection"))[0]; 
    my $confirm=($q->param("confirm"))[0]; 
    if ($confirm) {
		if ($new_value_selection) {	
			# Change the value to the new value
			$query = "update elderconfig set cvalue = '$new_value_selection' where ckey = '$ckey'";
			if ($dbh->do($query)) {        
				ElderLog($logfile,"Updated elderconfig key $ckey to set the value to $new_value_selection");
				print "Updated value of $ckey to $new_value_selection<br />";
				$query = "
				select 
					*
				from 
					elderconfig
				where
					ckey like '$ckey'
				";      
				$tbl_ref = execQuery_arrayref($dbh, $query);
				if (recordCount($tbl_ref)) {              
					printResult_arrayref2($tbl_ref); 
				} else {
					print $q->h3("No info found in elderconfig for key $ckey.");
				}            
			} else {
				print "Database Update Error"."<br />\n";
				print "Driver=ODBC"."<br />\n";
				print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
				print "query=".$query."<br />\n";
				print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
				print "errstr=".$dbh->errstr ."<br />\n";            
			}  
		}
		if ($new_key_selection) {	
			# Change the key to the new key
			$query = "update elderconfig set ckey = '$new_key_selection' where ckey = '$ckey'";
			if ($dbh->do($query)) {        
				ElderLog($logfile,"Updated elderconfig key $ckey to $new_key_selection");
				print "Updated $ckey to $new_key_selection<br />";
				$query = "
				select 
					*
				from 
					elderconfig
				where
					ckey like '$new_key_selection'
				";      
				$tbl_ref = execQuery_arrayref($dbh, $query);
				if (recordCount($tbl_ref)) {              
					printResult_arrayref2($tbl_ref); 
				} else {
					print $q->h3("No info found in elderconfig for key $new_key_selection.");
				}            
			} else {
				print "Database Update Error"."<br />\n";
				print "Driver=ODBC"."<br />\n";
				print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
				print "query=".$query."<br />\n";
				print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
				print "errstr=".$dbh->errstr ."<br />\n";            
			}  
		}		
    } else {
		if ($new_value_selection) {
			print $q->p("Are you certain you wish to update the value of $ckey to $new_value_selection?");
			print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});
			print $q->input({-type=>"hidden", -name=>"new_value_selection", -value=>"$new_value_selection"});
			print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});
			print $q->input({-type=>"hidden", -name=>"tool", -value=>"MECONF"});
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	
		 
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
						   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
								  $q->submit({  -value=>"Confirm"}))
						  );
			print $q->end_table(),"\n";
			print $q->end_form(), "\n";  
		}
		if ($new_key_selection) {
			print $q->p("Are you certain you wish to update the key of $ckey to $new_key_selection?");
			print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});
			print $q->input({-type=>"hidden", -name=>"new_key_selection", -value=>"$new_key_selection"});
			print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});
			print $q->input({-type=>"hidden", -name=>"tool", -value=>"MECONF"});
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	
		 
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
						   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
								  $q->submit({  -value=>"Confirm"}))
						  );
			print $q->end_table(),"\n";
			print $q->end_form(), "\n";  
		}		
    }
    # Add a link to come back to this tool
    print "<br />";
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=ECONF"}, "Back to elderConfig tools");  

    StandardFooter();    
} elsif ($tool eq "IECONF") {
    print $q->h3("ElderConfig Tool Insert.");
    my $new_key_selection=($q->param("new_key_selection"))[0]; 
    my $new_value_selection=($q->param("new_value_selection"))[0]; 
    my $confirm=($q->param("confirm"))[0]; 
    if ($confirm) {
        # Change the value to the new value
        $query = "insert into elderconfig values ('$new_key_selection','$new_value_selection')";
        if ($dbh->do($query)) {        
            ElderLog($logfile,"Inserted $new_key_selection and value $new_value_selection into elderconfig");
            print "Inserted $new_key_selection and value $new_value_selection into elderconfig<br />";
            $query = "
            select 
                *
            from 
                elderconfig
            where
                ckey like '$new_key_selection'
            ";      
            $tbl_ref = execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref)) {              
                printResult_arrayref2($tbl_ref); 
            } else {
                print $q->h3("No info found in elderconfig for key $new_key_selection.");
            }            
        } else {
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";            
        }  
    } else {
        unless ($new_key_selection) {
            unless ($new_value_selection) {
                print $q->p("Error: You must provide a key and value to enter");
                StandardFooter();
                exit;
            } else {
                print $q->p("Error: You must provide a key for the value $new_value_selection");
                StandardFooter();
                exit;                
            }
        }
        unless ($new_value_selection) {
            print $q->p("Error: You must provide a value for the key $new_key_selection");
            StandardFooter();
            exit;   
        }
        
        print $q->p("Are you certain you wish to insert the following into elderconfig?");
        my @heading=("ckey","cvalue");
        my @array_to_print=\@heading;
        my @array=("$new_key_selection","$new_value_selection");
        push(@array_to_print,\@array);
        printResult_arrayref2(\@array_to_print); 
        print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"new_key_selection", -value=>"$new_key_selection"});
        print $q->input({-type=>"hidden", -name=>"new_value_selection", -value=>"$new_value_selection"});
        print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});
        print $q->input({-type=>"hidden", -name=>"tool", -value=>"IECONF"});
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	    
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({  -value=>"Confirm"}))
                      );
        print $q->end_table(),"\n";
        print $q->end_form(), "\n";         
    }
    # Add a link to come back to this tool
    print "<br />";
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=ECONF"}, "Back to elderConfig tools");  

    StandardFooter(); 
} elsif ($tool eq "DECONF") {
    print $q->h3("ElderConfig Tool Delete.");
    my $ckey=($q->param("ckey"))[0]; 
    
    my $confirm=($q->param("confirm"))[0]; 
    if ($confirm) {
        # Change the value to the new value
        $query = "delete from elderconfig where ckey = '$ckey'";
        if ($dbh->do($query)) {        
            ElderLog($logfile,"Deleted $ckey from elderconfig");
            print "Deleted $ckey from elderconfig<br />";        
        } else {
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";            
        }  
    } else {
        print $q->p("Are you certain you wish to delete the entry of $ckey?");
        print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"ckey", -value=>"$ckey"});        
        print $q->input({-type=>"hidden", -name=>"confirm", -value=>"1"});
        print $q->input({-type=>"hidden", -name=>"tool", -value=>"DECONF"});
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";	     
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({  -value=>"Confirm"}))
                      );
        print $q->end_table(),"\n";
        print $q->end_form(), "\n";         
    }
    # Add a link to come back to this tool
    print "<br />";
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=ECONF"}, "Back to elderConfig tools");  

    StandardFooter();   
} elsif ($tool eq "CLEAR_MISC_SKU_QUERY") { 
    print $q->p("Clear a MISC SKU");

    print $q->start_form({-action=>"/cgi-bin/i_toolshed.pl", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"CLEAR_MISC_SKU"});

    # use a database query to get the first and last day of the previous month.
    my ($enddate, $startdate);
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Report Criteria:")));

    $query = dequote(<< "    ENDQUERY");
    select
    convert(char(8), getdate(), 21)+ '01' as "FirstDate",
    convert(char(10), getdate(), 21) as "LastDate"
    ENDQUERY
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref) > 0)  {
        my $thisrow = @$tbl_ref[1];
        $startdate = trim($$thisrow[0]);
        $enddate   = trim($$thisrow[1]);
    }

    my $calcounter = 1;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"startdate", -size=>"12", -value=>"$startdate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].startdate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               ),
                         )
                  );
    $calcounter++;
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End date:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->script({-language=>"JavaScript"}, "var cal$calcounter = new CalendarPopup(\"testdiv1\");"),
                          $q->input({-type=>'text', -name=>"enddate", -size=>"12", -value=>"$enddate"}),
                          $q->a({-href=>"#", -NAME=>"anchor$calcounter", -ID=>"anchor$calcounter",
                                 -onclick=>"cal$calcounter.select(document.forms[0].enddate,'anchor$calcounter','yyyy-MM-dd'); return false;"},
                                $q->img({-src=>"/images/calendar.gif", -align=>"top", -border=>"0"})
                               )
                         )
                  );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
    print $q->end_table(),"\n";

    print $q->end_form(), "\n";

    print '<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>';

    $dbh->disconnect;    
    StandardFooter();   
} elsif ($tool eq "CLEAR_MISC_SKU") { 
    print $q->h1("List of Miscellaneous SKUS");

    my $sortfield = trim(($q->param("sort"))[0]);

    $sortfield = "DateTime" unless $sortfield;
    my $startdate =trim(($q->param("startdate"))[0]);
    my $enddate   =trim(($q->param("enddate"))[0]);

    my $whereclause = "";
    if ( ($startdate) && ($enddate) ) {
        if ( ($startdate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) || ($enddate !~ m/^\d{4}-\d{1,2}-\d{1,2}$/) ) {
            print "Startdate or Enddate format not currect";
            last SWITCH;
        }

        $whereclause = " and ppr.itemdatetime between '$startdate' and '$enddate 23:59:59' ";
    }

    $query = dequote(<< "      ENDQUERY");
      begin
         select
            mt.storenum, pt.transnum
         into #tmpPostVoids
         from
            Txn_Miscellaneous_Trans mt
            join Txn_POS_Transactions pt on
               mt.storenum = pt.storenum and
               mt.refnum   = pt.txnnum   and
               abs(datediff(day, mt.itemdatetime, pt.txndatetime)) < 5
            join txn_pos_transactions pt2 on mt.transnum = pt2.transnum 
                and pt.regnum = pt2.regnum                 
         where
            mt.misctype = 2;

         select
            ppr.txnnum as "TxnNum",
            convert(varchar(19),ppr.itemdatetime, 21) as "DateTime",
            ms.skunum as "SKU",
            ms.qty*(ms.extsellprice/abs(ms.extsellprice)) as "Qty",
            ms.stdunitprice*(ms.extsellprice/abs(ms.extsellprice)) as "Price",
            ppr.profileresponse1 as "Description",
            ppr.profileresponse2 as "Dept"
         from
            Txn_Profile_Prompt_Response ppr
            join Txn_Merchandise_Sale ms on
               ppr.storenum=ms.storenum and
               ppr.transnum=ms.transnum and
               (ppr.itemnum = ms.itemnum+1 or ppr.itemnum = ms.itemnum+2)
            left outer join #tmpPostVoids v on ppr.storenum=v.storenum and ppr.transnum=v.transnum
         where
            ppr.txnvoidmod = 0 and
            ppr.propromptid = 26 and
            ms.deptnum='9999' and
            v.transnum is null
            $whereclause
         order by $sortfield;

         drop table #tmpPostVoids;
      end
      ENDQUERY

    my $any_dbh = TTV::utilities::openODBCDriver($dsn);
    $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
    my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'currency' => ''},  '', '');
    my @links = ( "$scriptname?report=CLEAR_MISC_SKU_ITEM&whereclause=$whereclause&txnnum=".'$_' );
    my $newsortURL = "/cgi-bin/i_reports.pl?report=CLEAR_MISC_SKU&sort=\$_";
    my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
    if (recordCount($tbl_ref)) { printResult_arrayref2($tbl_ref, \@links, \@format, \@headerlinks); }
    else { print $q->b("no records found<br />\n"); }
    $any_dbh->disconnect;
 
    StandardFooter();   
} elsif ($tool eq "CLEAR_MISC_SKU_ITEM") { 
    print $q->h1("Clear Miscellaneous SKU entry"); 
    my $txnnum=trim(($q->param("txnnum"))[0]);    
    my $whereclause=trim(($q->param("whereclause"))[0]);  
    
    
    $query = dequote(<< "      ENDQUERY");
         select
            ppr.txnnum as "TxnNum",
            convert(varchar(19),ppr.itemdatetime, 21) as "DateTime",
            ms.skunum as "SKU",
            ms.qty*(ms.extsellprice/abs(ms.extsellprice)) as "Qty",
            ms.stdunitprice*(ms.extsellprice/abs(ms.extsellprice)) as "Price",
            ppr.profileresponse1 as "Description",
            ppr.profileresponse2 as "Dept",  
            ppr.transnum
         from
            Txn_Profile_Prompt_Response ppr
            join Txn_Merchandise_Sale ms on
               ppr.storenum=ms.storenum and
               ppr.transnum=ms.transnum and
               (ppr.itemnum = ms.itemnum+1 or ppr.itemnum = ms.itemnum+2)
            
         where
            ppr.txnnum = '$txnnum' and
            ppr.txnvoidmod = 0 and
            ppr.propromptid = 26 and
            ms.deptnum='9999'  
            $whereclause    
      ENDQUERY

    my $any_dbh = TTV::utilities::openODBCDriver($dsn);
    $tbl_ref = TTV::utilities::execQuery_arrayref($any_dbh, $query);
    my @format = ('', '', '', {'align' => 'Right', 'num' => '.0'},
            {'align' => 'Right', 'currency' => ''},  '', '');
    my @links = ( "$scriptname?report=CLEAR_MISC_SKU_ITEM&txnnum&whereclause=$whereclause=".'$_' );
    
     
    if (recordCount($tbl_ref)) { 
        printResult_arrayref2($tbl_ref, \@links, \@format); 
        # Get the transnum
        my $transnum = $$tbl_ref[1][7];
        
        $query = dequote(<< "        ENDQUERY");            
            delete from 
                txn_profile_prompt_response                
             where
                txnnum = '$txnnum' and
                transnum = '$transnum'
  
        ENDQUERY
          
        if ($dbh->do($query)) {        
            ElderLog($logfile,"Deleted txnnum $txnnum from Txn_Profile_Prompt_Response table");
            print $q->p("Deleted txnnum $txnnum from Txn_Profile_Prompt_Response table");
        } else {
            print "Database Update Error trying to delete txnnum $txnnum"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";            
        }  
        
    } else { 
        print $q->b("no records found<br />\n"); 
    }
    $any_dbh->disconnect;      
    StandardFooter();  
} elsif ($tool eq "POS") { 
    print $q->h1("Point of Sale"); 
    my $sku = ($q->param("skuid"))[0]; 
    my $txnnum = ($q->param("txnnum"))[0]; 
    my $transnum = ($q->param("transnum"))[0];     
    my $itemnum;
    my $elderSalesTransactionTable = "elderSalesTransaction";
    my $tbl_ref;
    my $dbh = TTV::utilities::openODBCDriver($dsn);
 
    if ($sku) {  
        # Trim off any spaces
        $sku=~ s/ //g;    
        # Is this a valid SKU?
        $query = dequote(<< "        ENDQUERY");
            select 
                plunum
            from
                plu
            where
                plunum = $sku
            
        ENDQUERY
        
        print $q->pre("$query");
        $tbl_ref = execQuery_arrayref($dbh, $query); 
        my $found = $$tbl_ref[1][0];
 
        unless ($found eq $sku) {
            print $q->h3("Error: Failed to recognize $sku");
            StandardFooter();
            exit;                        
        }
    }
 
    unless ($txnnum) {
  
        # This is a new transaction 
        # ################################################################
        # # create the sqlAnywhere BACKOFF table to store the Transaction information.
        # ################################################################

        $query = dequote(<< "        ENDQUERY");
        select * from dbo.sysobjects where id = object_id('$elderSalesTransactionTable') and type = 'U'
            
        ENDQUERY
        
        
        $tbl_ref = execQuery_arrayref($dbh, $query); 
        
        unless (recordCount($tbl_ref)) {  
            ElderLog($logfile, indent(1). "Creating my $elderSalesTransactionTable table in BACKOFF.");        
            $query = dequote(<< "            ENDQUERY");
                create table $elderSalesTransactionTable (                                 
                    txnnum int,                 
                    itemnum int,
                    TranDate DateTime,
                    SKU varchar(18)
             )
            ENDQUERY
            
            #$tbl_ref = execQuery_arrayref($dbh, $query);         
            unless ($dbh->do($query)) {
                ElderLog($logfile, $dbh->{Name});
                ElderLog($logfile, "query=".$query);
                ElderLog($logfile, "err=".$dbh->err."\tstate: ".$dbh->state);
                ElderLog($logfile, "errstr=".$dbh->errstr);
                print $q->p("Error - failed to create the $elderSalesTransactionTable.");            
                print $q->p("If the problem persists, please notify POS support in Akron.");
                StandardFooter();
                exit;
             
            }        
        }
    }
    # TODO
    # If there is no transaction yet, start one and generate a number
    # if there is one, we (should) have the number
    # If we have a number and a sku, add the sku to the transaction and then display all of the items on the transaction so far.
    
    # If we have no transaction - we need to start one
 
    unless ($txnnum) {
       
        $query = dequote(<< "        ENDQUERY");
            select 
                max(txnnum)
            from 
                $elderSalesTransactionTable 

        ENDQUERY
        
        $tbl_ref = execQuery_arrayref($dbh, $query); 
        if (recordCount($tbl_ref)) {         
            $txnnum = $$tbl_ref[1][0];
            # Increment the txnnum
            $txnnum++;
        } else {
            # Need to start one
            $txnnum = 1;
        }
        if ($txnnum > 9999) {
            $txnnum = 1;
        }
        
    }

 
    if ($sku) {
     
        # See if we have any other items on the transaction already
        $query = dequote(<< "        ENDQUERY");
            select 
                max(itemnum)
            from 
                $elderSalesTransactionTable 
            where
                txnnum = $txnnum
       
        ENDQUERY

        print $q->pre("$query");
        $tbl_ref = execQuery_arrayref($dbh, $query); 
        if (recordCount($tbl_ref)) {         
            $itemnum = $$tbl_ref[1][0];
            # Increment the txnnum
            $itemnum++;
        } else {
            # Need to start one
            $itemnum = 1;
        }
        
        # Add this SKU to the transaction table
        
        
        $query = dequote(<< "        ENDQUERY");
            insert into $elderSalesTransactionTable
            values ($txnnum,$itemnum,getdate(),'$sku')
 
            
        ENDQUERY
        
        print $q->pre("$query");
        
        unless ($dbh->do($query)) {
            ElderLog($logfile, $dbh->{Name});
            ElderLog($logfile, "query=".$query);
            ElderLog($logfile, "err=".$dbh->err."\tstate: ".$dbh->state);
            ElderLog($logfile, "errstr=".$dbh->errstr);
            print $q->p("Error - failed to insert SKU $sku into the $elderSalesTransactionTable.");            
            print $q->p("If the problem persists, please notify POS support in Akron.");
            StandardFooter();
            exit;
         
        } 
               
    }
   
    if ($txnnum) {
          
            # Get the list of items currently on this transaction
            $query = dequote(<< "            ENDQUERY");
                select 
                    *
                from 
                    $elderSalesTransactionTable 
                where
 
                    txnnum = $txnnum
            ENDQUERY
            
            $tbl_ref = execQuery_arrayref($dbh, $query); 
            if (recordCount($tbl_ref)) {              
                my @format = ('', '', '','',
                    {'align' => 'Right', 'currency' => ''},
                    '','','','',
                    {'align' => 'Right', 'currency' => ''}
                    );
                     
                TTV::cgiutilities::printResult_arrayref2($tbl_ref, '', \@format, ''); 
            }      
    }
    # Ask for a SKU
    print $q->start_form({-action=>"/cgi-bin/$scriptname", -method=>'get'}), "\n";  
    print $q->input({-type=>"hidden", -name=>"tool", -value=>"POS"});  
    print $q->input({-type=>"hidden", -name=>"txnnum", -value=>"$txnnum"});        
    print  $q->input({ -type=>'text', -name=>"skuid", -size=>"8", -value=>" "});        
    print $q->input({-type=>"submit", -name=>"submit", -value=>"Enter"});                 
    print $q->p("Enter SKU or bar code and click the 'Enter' button.");
    
    print $q->end_form(), "\n";  
   
    $dbh->disconnect;      
    StandardFooter(); 
} elsif ($tool eq "POS1") { 
    print $q->h1("Point of Sale"); 
    my $sku = ($q->param("skuid"))[0]; 
    
    StandardFooter();  
} elsif ($tool eq "GETDATE") { 
	# Show the date and time.  This can be used by registers to get the current
	# date and time if they have lost their clock.
	my @ltm = localtime();
	my $current_year=$ltm[5]+1900;
	my $current_month=$ltm[4]+1;
	my $current_day=$ltm[3];
	my $current_hour=$ltm[2];	
	my $current_minute=$ltm[1];	
	print "<br />year $current_year<br />";
	print "month $current_month<br />";
	print "day $current_day<br />";
	print "hour $current_hour<br />";
	print "minute $current_minute<br />";  	
} else {
  	print $q->h1("Unknown Tool Requested ($tool).");
  	StandardFooter();
}

# close the database handler.
$dbh->disconnect;

# close the html output
print $q->end_html;

exit;

# end of program.

#####################
# Functions
#####################

sub StandardFooter {
    print "<br /><br />";
    #print $q->a({-href=>"javascript:parent.history.go(0)"}, "refresh");
    #print $q->a({-href=>"javascript:parent.history.go(-1)"}, "go back");
	print "<br />";
    print $q->a({-href=>"/cgi-bin/i_toolshed.pl?report=DEFAULT"}, "Back to toolshed");  
    print "<br />";
    print $q->a({-href=>"/cgi-bin/i_stevetools.pl?report=LISTTOOLS&password=77580&.submit=GO"}, "Back to stevetools");                  
    print "<br />";
    print $q->a({-href=>"/index.html", -target=>'_top'}, "Back to The Elder");
}

sub update_userflag {
    my $sku=shift;
    my $flag=shift;
    my $expected_setting;
    my @tmp;        
    my $dbh = openODBCDriver($dsn);
    # Send the SKU, the flag to set  and this function will determine the setting and set it.
    # Do this by looking what the flag should be for that SKU and setting it in the plu table.
    $query = dequote(<< "    ENDQUERY"); 
    select 
        userflag$flag 
    from 
        elderVillagesSku            
    where 
        plunum = '$sku'
    ENDQUERY
        
    unless ($sth = $dbh->prepare($query)) {
        print $q->p("ERROR: Failed to prepare query ($query)");
        return;
    }
    $sth->execute();
    my $expected_flag="";
    while (@tmp = $sth->fetchrow_array) {
        if (defined($tmp[0])) {
            $expected_flag=$tmp[0];
        }
    }   
    
    if ($expected_flag eq "") {            
        # Don't have a value so set it NULL
        $query = dequote(<< "        ENDQUERY");         
        update 
            plu              
        set 
            userflagnum$flag = NULL
        where 
            plunum = '$sku'
        ENDQUERY
    } else {        
        # Cross-reference this and re-define the expected_flag with the codeid
        my $codeid=0;
        $query = dequote(<< "        ENDQUERY"); 
        select 
            codeid 
        from 
            user_flag_code
        where 
            userflagnum = $flag
            and codename like '$expected_flag'
        ENDQUERY
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (@tmp = $sth->fetchrow_array) {
            if (defined($tmp[0])) {
                $codeid=$tmp[0];
            }
        }       
        if ($codeid) {
        
            $query = dequote(<< "            ENDQUERY");         
            update 
                plu              
            set 
                userflagnum$flag = '$codeid'
            where 
                plunum = '$sku'
            ENDQUERY
            
        } else {
            print "<b>Error: Codename $expected_flag not found in user_flag_code table for flag $flag</b><br />";
             
            $query=0;
        }
    }

    # Now update the plu   
    if ($query) {
        if ($dbh->do($query)) {        
            ElderLog($logfile,"Updated flag $flag for SKU $sku to $expected_flag");
            print "Updated flag $flag for SKU $sku<br />";
        } else {
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";            
        }  
    }
    # close the database handler.
    $dbh->disconnect;    
}

sub CheckPassword {
   my($dbh, $password) = @_;

   my $rc = 0;
   my $key = calcPasskey($dbh) ;
   if ($password == ($key>>1)) {
      $rc = 1;
   }
   elsif ($password == (155160>>1)) {
      $rc = 1;
   }

   return $rc;
}

sub calcPasskey {
   my($dbh) = @_;
   my @today = localtime();
   my $date = sprintf("%02d%02d%4d", $today[3], $today[4]+1, $today[5]+1900 );
   $date += 0;

   my $storeId = '9999';
   $query = "select storenum from sys_table where sys_key=1";
   my $tbl_ref = execQuery_arrayref($dbh, $query);
   if (recordCount($tbl_ref) > 0) {
      $storeId = trim(@$tbl_ref[1]->[0]);
   }
   $storeId = sprintf("%04d%04d", $storeId, $storeId) + 0;

   my $key = $date ^ $storeId;

   return $key;
}


sub timestampfile {
   my $file = shift;
   my @now = localtime();
   my $timestamp = sprintf("%4d%02d%02d.%02d%02d%02d", $now[5]+1900, $now[4]+1, $now[3], $now[2], $now[1], $now[0]);
   my $newfile = "$file";
   substr($newfile,rindex($newfile, "."),0) = ".$timestamp";
   return $newfile;
}

sub doquery {
   my $query = shift;
   my $rc;
   my $dbh = openODBCDriver($dsn); 
   unless ($rc = $dbh->do($query)) {
      print "Database Update Error"."<br />\n";
      print "Driver=ODBC"."<br />\n";
      print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
      print "query=".$query."<br />\n";
      print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
      print "errstr=".$dbh->errstr ."<br />\n";
      $dbh->disconnect;
      return 0;
   }
   $dbh->disconnect;
   return 1;
}

sub run_n_print {
   my ($command) = @_;

   print $q->h1("$command");

   my @RC = `$command`;
   print "<pre>";
   foreach my $retline (@RC) { print $retline; }
   print "</pre><br />\n";

   return;
}

sub printTable {
	use CGI::Pretty qw/:standard :html3 *table/;
	$CGI::Pretty::INDENT = "  ";
	my $headref=shift;
	my $arrayref=shift;
    my @header=@$headref;
	my @arrayToPrint=@$arrayref;

	print "\n", start_table({-class=>'tableborder', -valign=>'top', -border=>'1'}), "\n";	
	print $q->Tr(td({-class=>'tablehead', -nowrap=>undef}, $headref));
	#print Tr(td({-class=>'tableborder'}, $arrayref) );    
 
	my($counter) = 0;	
	foreach my $a (@arrayToPrint) {
		next if ($a =~ /^$/);     
		$counter++;	
        #my @line=split(/ /,$a);
        my @line=@$a;
		my @row=();		
		for (my $i=0; $i<=$#line; $i++) {		
			my $field=$line[$i];		
			push(@row, $field);			
		}		
		print Tr(td({-class=>'tableborder'}, \@row) );
   }

   print end_table(),"\n";
   # print Summary Information (rows printed)
   print code("Rows returned=$counter"), br(), "\n";

}

sub check_sku_on_promo {
    my $sku=shift;
    my $vendorid;
    my $category;
    my $group;
    my $dept;
    my $uf1;
    my $uf2;
    my $uf3;
    my $uf4;
    my $uf5;
    my $uf6;    
    my $dbh = openODBCDriver($dsn);    
    $query="
    select 
        p.plunum,p.vendorid, c.catagorynum,d.groupnum,d.deptnum, 
        p.userflagnum1, p.userflagnum2, p.userflagnum3, p.userflagnum4, p.userflagnum5, p.userflagnum6
    from 
        plu p
        left outer join department d on p.deptnum = d.deptnum
        left outer join dept_group dg on d.groupnum = dg.groupnum
        left outer join category c on c.catagorynum = dg.catagorynum
    where
        p.plunum = '$sku'";
	$sth=$dbh->prepare($query);	
    $sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {      
        $sku="$tmp[0]";
        $vendorid="$tmp[1]";
        $category="$tmp[2]";
        $group="$tmp[3]";
        $dept="$tmp[4]";
        $uf1="$tmp[5]";
        $uf2="$tmp[6]";
        $uf3="$tmp[7]";
        $uf4="$tmp[8]";
        $uf5="$tmp[9]";
        $uf6="$tmp[10]";
    }
        
    # If the value is not set, set it to null
    unless ($sku) {
        return 2;
        #print $q->p("Error: SKU number undefined");
        #StandardFooter;
        #exit;
    }
    unless ($vendorid) {
        $vendorid="null";
    }    
    unless ($category) {
        $category="null";
    }    
    unless ($group) {
        $group="null";
    }    
    unless ($dept) {
        $dept="null";
    }    
    unless ($uf1) {
        $uf1="null";
    }    
    unless ($uf2) {
        $uf2="null";
    }    
    unless ($uf3) {
        $uf3="null";
    }        
    unless ($uf4) {
        $uf4="null";
    }   
    unless ($uf5) {
        $uf5="null";
    }   
    unless ($uf6) {
        $uf6="null";
    }       
    $query = "call dba.bosdb_hunt_promo(\@as_skuNum=$sku, \@as_vendor=$vendorid, \@ai_category=$category, ".
          "\@ai_group=$group, \@ai_deptNum=$dept, ".
          "\@as_userFlag1=$uf1, ".
          "\@as_userFlag2=$uf2, ".
          "\@as_userFlag3=$uf3, ".
          "\@as_userFlag4=$uf4, ".
          "\@as_userFlag5=$uf5, ".
          "\@as_userFlag6=$uf6, ".
          "); commit; ";

    ElderLog($logfile, indent(1).$query);

    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        return 1;
    } else {
        return 0;
    }
    $dbh->disconnect;
}

sub getStoreNum_orig {
    my $dbh = openODBCDriver($dsn);  
    my $storenum = '';
    $query = "select storenum from sys_table where sys_key=1";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref) > 0) {
        $storenum = trim(@$tbl_ref[1]->[0]);}
    return $storenum;
    $dbh->disconnect;

}

sub fix_userflag_desc {
    my $flag=shift;
    my $codename=shift;
    my $description=shift;
    my $dbh = openODBCDriver($dsn);
    
    # First make certain that this entry even exists;
    $query = dequote(<< "    ENDQUERY");
    select 
        codename 
    from 
        user_flag_code
    where
        userflagnum = '$flag'
    and
        codename like '$codename'        
    ENDQUERY
    
    my $tbl_ref = execQuery_arrayref($dbh, $query);         
    if (recordCount($tbl_ref)) {          
        # Update the entry        
        $query = dequote(<< "        ENDQUERY");
        update 
            user_flag_code 
        set 
            description = '$description'        
        where
            userflagnum = '$flag'
        and
            codename like '$codename'        
        ENDQUERY
        if ($dbh->do($query)) {
            print $q->p("Updated description in user_flag_code for user flag $flag codename $codename.");
            ElderLog($logfile,"Updated description in user_flag_code for user flag $flag codename $codename.");  
        } else {
            ElderLog($logfile,"Error trying to update description for user flag $flag codename $codename");
            print "Database Update Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;
        }     
    } else {
        # Insert the entry
        # Before we can do that, we need to get the last codeid used for this user flag
        my $codeid=0;        
        $query = dequote(<< "        ENDQUERY");
        select 
            max(codeid) 
        from 
            user_flag_code
        where
            userflagnum = '$flag'            
        ENDQUERY
        
        my $sth = $dbh->prepare($query);        
        $sth->execute();                
        while (my @tmp=$sth->fetchrow_array()) {              
            $codeid=$tmp[0];
        }
 
        # Increment the codeid to get what the next one should be and then insert it.
        $codeid++;        
        $query = dequote(<< "        ENDQUERY");
        insert into 
            user_flag_code 
        VALUES
            ($flag,$codeid,'$codename','$description')                    
        ENDQUERY
        
        if ($dbh->do($query)) {
            print $q->p("Inserted description in user_flag_code for user flag $flag codename $codename.");
            ElderLog($logfile,"Inserted description in user_flag_code for user flag $flag codename $codename.");  
        } else {
            ElderLog($logfile,"Error trying to Insert description for user flag $flag codename $codename");
            print "Database Insert Error"."<br />\n";
            print "Driver=ODBC"."<br />\n";
            print  $dbh->{Name}."<br />\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br />\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br />\n";
            print "errstr=".$dbh->errstr ."<br />\n";
            StandardFooter(); 
            exit;        
        }
    }    
    $dbh->disconnect;
}

sub saveComport {
    my $port=shift;
    # Save this port as the com port with the modem in elderConfig
    my $dbh = openODBCDriver($dsn);
    my $found_entry=0;
    my $key="modemPort";
    $query="
        select 
            cvalue 
        from 
            elderConfig
        where
            ckey like '$key'
    ";
    my $sth = $dbh->prepare($query);        
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {              
        $found_entry=$tmp[0];
    }    
    if ($found_entry) {
        if ($port eq "com1") {
            # This is the default and we don't bother saving this to the elderConfig
            $query="delete from elderConfig where ckey like '$key'";
            if ($dbh->do($query)) {
                ElderLog($logfile,"Removed modem port from elderConfig.");
                print $q->h4("Updated elderConfig with modem port.");
            } else {
                ElderLog($logfile,"Error trying to update elderConfig with modem port.");
                print $q->p("Error trying to update elderConfig with modem port.");
                print $q->p("ERROR: $dbh->errstr");
            }                
        }
        if ($found_entry eq "port") {
            # This entry is already there
            return 1;
        }
        # We have an entry, just need to update it
        $query="update elderConfig set cvalue = '$port' where ckey like '$key'";
        if ($dbh->do($query)) {
            print $q->h4("Updated elderConfig with modem port.");
            ElderLog($logfile,"Updated elderConfig with modem port.");
        } else {
            print $q->p("Error trying to update elderConfig with modem port.");
            ElderLog($logfile,"Error trying to update elderConfig with modem port.");
            print $q->p("ERROR: $dbh->errstr");
        }            
        
    } else {
        if ($port eq "com1") {
            # This is the default and we don't bother saving this to the elderConfig
            return;
        }
        # Need to insert a new entry
        $query="insert into elderConfig VALUES ('$key','$port')";
        if ($dbh->do($query)) {
            print $q->h4("Inserted modem port into elderConfig.");
            ElderLog($logfile,"Inserted modem port into elderConfig.");
        } else {
            print $q->p("Error trying to insert modem port into elderConfig.");
            ElderLog($logfile,"Error trying to insert modem port into elderConfig.");
            print $q->p("ERROR: $dbh->errstr");
        }          
    }
    $dbh->disconnect;        
}

sub getComport {
    # Return the comport that the elderConfig has for the modem
    my $dbh = openODBCDriver($dsn);
    my $found_entry=0;
    my $key="modemPort";
    $query="
        select 
            cvalue 
        from 
            elderConfig
        where
            ckey like '$key'
    ";
    my $sth = $dbh->prepare($query);        
    $sth->execute();                
    while (my @tmp=$sth->fetchrow_array()) {              
        $found_entry=$tmp[0];
    } 
    
    $dbh->disconnect; 
    return $found_entry;
}


sub parse_transnet_content {
    my $ref=shift;
    my @contents=@$ref;
    my %txn_count;
    my %card_count; 
    my $read=0;  
    my $termCount;
    my $foundTxn=0;
    my $foundSummary=0;
    my %txn_hash=();
    my $date="unknown";   
    my @txn_array=();
    my $merchantId;
    my $terminalId; 
    my %summary_hash;
    my @summary_array;
    my $current_txn=0;
    for (my $x=0; $x<=$#contents; $x++) {
        my $line=$contents[$x];
        my @tmp=();
        my @stmp=();
        my $y="";
        my $var="";
        my $txn;                    
        if ($line =~ /<TransactionNumber>.*<\/TransactionNumber>/) {                 
            my %var_hash;                 
            @tmp=split(/></,$line);
            for ($y=0; $y<=$#tmp; $y++) {
                if ($tmp[$y] =~ /TransactionNumber[>]/) {     
                    $var=$tmp[$y];                          
                    @stmp=split(/>/,$var);                                                      
                    $var=$stmp[1];                          
                    @stmp=split(/</,$var);                                                
                    $txn=$stmp[0];
                    $foundTxn=1;
                    $txn_count{$txn}=1;
                    $current_txn=$txn;
                }    
                if ($foundTxn) {                                
                    for my $term ("TransactionNumber","RequestAmount","Chain","Register","AccountType",
                    "Track2","InputMode","ApprovalCode","DisplayInfo",
                    "Function","Modifier","AdditionalModifier","StoreAndForward","Store",
                    "InputMode","CardPresent","AccountNumber","ExpiryDate","KeyToken","StoreAndForward","AccountNumber Encrypted=\"True\"") {
                        if ($tmp[$y] =~ /$term[>]/) {     
                            $var=$tmp[$y];                          
                            @stmp=split(/>/,$var);                                                      
                            $var=$stmp[1];                          
                            @stmp=split(/</,$var);                        
                            $var_hash{$term}=$stmp[0];  
                            if ($merchantId) {
                                $var_hash{'merchantId'}=$merchantId;  
                            }
                           
                            if ($merchantId) {                              
                                $var_hash{'terminalId'}=$terminalId;  
                            }     
                            if ($term eq "AccountNumber") {
                                if ($stmp[0]) {
                                    $card_count{$stmp[0]}=1;
                                }
                            }
                            if ($date) {
                                $var_hash{'TransactionDate'}=$date;
                            }
                        }                 
                    }  
                    # Getting the approved amount is a bit different
                    if ($tmp[$y] =~ /ApprovedAmount/) {     
                        my $var=$tmp[$y];             
                        my @tmp2=split(/"/,$var);
                        $var_hash{'ApprovedAmount'}="\$"."$tmp2[1]";                  
                    }       
                    if ($tmp[$y] =~ /RequestedAmount/) {     
                        my $var=$tmp[$y];             
                        my @tmp2=split(/"/,$var);
                        $var_hash{'RequestedAmount'}="\$"."$tmp2[1]";                  
                    }    
                }                    
            } 
            push(@txn_array,\%var_hash);
        }
        if ($line =~ /MerchantID:/) {
            my @tmp=split(/\:/,$line);
            $merchantId=$tmp[-1];                                
        }
        if ($line =~ /TerminalID:/) {
            my @tmp=split(/\:/,$line);
            $terminalId=$tmp[-1];                  
        }        
        if ($line =~ /Thread/) {
            my @tmp=split(/\s+/,$line);
            $date="$tmp[0] $tmp[1]";
            @tmp=split(/,/,$date);
            $date=$tmp[0]; 
            if ($foundSummary) {
                # This would indicate the end of the Message Summary
                $foundSummary=0;
                my @array=@summary_array;

                @summary_array=();
                $summary_hash{$current_txn}=\@array;
            }                
        }  
        if ($line =~ /Message summary:/) {
            $foundSummary=1;               
        }         
        if ($foundSummary) {
            if ($line =~ /\:/) {
                
                push(@summary_array,$line)                    
            }                         
        }           
    }
    my $tcount=keys(%txn_count);
    my $ccount=keys(%card_count);
    print "<br />Found $tcount transactions and $ccount cards<br /><br />";    
    foreach my $incident (@txn_array) {
        my %hash=%$incident;
        my $txn=$hash{'TransactionNumber'};
        print "-------------Reporting on transaction $txn---------------<br />";

        foreach my $key (sort keys(%hash)) {
            if ($key eq "DisplayInfo") {
                print $q->b({-class=>'redalert'},"$key - $hash{$key}<br />");
            } elsif (($key eq "ApprovalCode") || ($key =~ /amount/i)) {                
                print $q->b({-class=>'greenalert'},"$key - $hash{$key}<br />");
            } else {
                print "$key - $hash{$key}<br />";
            }
        }   
    }
    #my $tcount=keys(%txn_count);
    #my $ccount=keys(%card_count);
    #print "<br />Found $tcount transactions and $ccount cards<br />";
}
 

sub enableSelection {
    # In this function, we enable each SKU to participate in the Mix Match.  It is important to realize that a Mix Match number may be re-used
    # many times.  Therefore, it is important that we first remove any SKUs that are currently set to participate in this Mix Match number.  
    # The MerchandisingTools.pl script which created the MixMatch, is designed to use a new Mix Match id number only if there is not one
    # free to re-use.  Being "free" to re-use means that there are no Mix Match discounts currently using the ID.  Therefore, if there are
    # any SKUs using this Mix Match ID that we are currently enabling, that Mix Match discount is expected to be obsolete.
    my $dbh = openODBCDriver($dsn);
    my $selectionref = shift;
    my $MMID = shift;
    my @sku_selection=
    my @selection=@$selectionref;
    my $query;
    my $sku_count=0;
    my $total_sku_count=0;
    print$q->p("Enabling SKU selection for MixMatch ID $MMID");
     
    # Create a PLUTXN.ASC file for all of the SKUs selected
    my $plutxn="${xpsDir}/parm/plutxn.asc";  
    print $q->p(indent(1)."Creating $plutxn");
    unless (open (FILE, ">$plutxn")) {                 
        print $q->p("ERROR: Failed to open $plutxn.");
        print $q->p("Aborting here.");        
        exit;
    }    
    # A given SKU can be a part of at most 4 separate MixMatch discounts concurrently.  Before we
    # add a MixMatch id to this SKU, we must determine if this SKU is already a part of a current mixmatch
    # or one that will be concurrent with this MixMatch at some point in time.
    # First step, find all current Mix Match discounts other than this one.
    my @current_future_mixmatch_list;
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum
    from
        mix_match
    where 
        (startDate < CURRENT DATE    
        or
        startDate = CURRENT DATE)         
    and
        stopDate > CURRENT DATE
    
    ENDQUERY
    
    my $sth = $dbh->prepare($query);
    $sth->execute();        
    while (my @tmp=$sth->fetchrow_array()) {
        push(@current_future_mixmatch_list,$tmp[0]);         
    }
    # Second step, find all future Mix Match discounts other than this one.    
    $query = dequote(<< "    ENDQUERY");
    select 
        mixMatchNum
    from
        mix_match
    where 
        startDate > CURRENT DATE 
    
    ENDQUERY
    
    $sth = $dbh->prepare($query);
    $sth->execute();        
    while (my @tmp=$sth->fetchrow_array()) {
        push(@current_future_mixmatch_list,$tmp[0]);   
    }    
    # Third step, find all SKUs associated with these Mix Match discounts
    my %current_future_mm_hash;
    foreach my $current_future_mmid (@current_future_mixmatch_list) {
        $query = dequote(<< "        ENDQUERY");
        select 
            plunum,mmid1,mmid2,mmid3,mmid4
        from
            plu
        where 
            mmid1 = '$current_future_mmid'
        or
            mmid2 = '$current_future_mmid'
        or
            mmid3 = '$current_future_mmid'
        or
            mmid4 = '$current_future_mmid'
        
        ENDQUERY
        
        my $sth = $dbh->prepare($query);
        $sth->execute();        
        while (my @tmp=$sth->fetchrow_array()) {
            my @array=("$tmp[1]","$tmp[2]","$tmp[3]","$tmp[4]");
            $current_future_mm_hash{$tmp[0]}=\@array;              
        }       
    }
    # This hash maps the MMID field to the position in the ASC file
    my %mmid_mapping_hash=(
        '1' => '7',
        '2' => '34',
        '3' => '35',
        '4' => '36'
    );
    foreach my $sku (@selection) {
        # We want to associate this SKU with a MixMatch ID. First find out if the SKU is already associated
        # with a current or future MixMatch ID
        my $use_mmid_field=1;   # Default to 1
        if ($current_future_mm_hash{$sku}) {
            # This SKU is already associated with a MixMatch ID - which mmid field is used?
            my $ref=$current_future_mm_hash{$sku};
            my @array=@$ref;         
            if ($array[0]) {
                # MMID field 1 is already used
                if ($array[1]) {
                    # MMID field 2 is already used
                    if ($array[2]) {
                        # MMID field 3 is already used                        
                        if ($array[3]) {
                            # MMID field 4 is already used which means there are 
                            # no more fields free
                            $use_mmid_field=0;
                        } else {
                            $use_mmid_field=4;
                        }                           
                    } else {
                        $use_mmid_field=3;
                    }                    
                } else {
                    $use_mmid_field=2;
                }
            } else {
                $use_mmid_field=1;
            }            
        }  
       
        if ($use_mmid_field == 0) {
            print $q->p("WARNING: Sku $sku has no available MixMatch ID fields.");
            next;
        } else {
            for my $x (1 ... 36) {
                my $entry='';
                if ($x == 1) {
                    $entry="2";
                }
                if ($x == 2) {
                    $entry="$sku";
                }   
                if ($x == $mmid_mapping_hash{$use_mmid_field}) {
                    $entry="$MMID";
                }
                print FILE "$entry,";               
            }
            print FILE "\n";
            print $q->a("creating entry for $sku for mixmatch $MMID");
            print "<br />";
        }
        $sku_count++;
    }
    close FILE;

    print $q->p("Finished creating $plutxn for $sku_count SKUs");

    $total_sku_count+=$sku_count;
    $dbh->disconnect;
}

sub process_ASC {
    # Create the trigger file
    my $trigger="${xpsDir}/parm/newparm.trg";    
    if (open(TRG,">$trigger")) {
        print TRG "trigger";
        close TRG;
        my $trg_count=0;
        while (-e $trigger) {
            print $q->p("Waiting for $trigger to be processed...");
            sleep 10;
            $trg_count++;
            if ($trg_count > 4) {
                print $q->p("Error: $trigger has not been processed yet");
                last;
            }
        }
        print $q->p("The asc file has been processed");

    } else {
        print $q->p("Error: Could not create $trigger");

    }    

}

sub pre_process_offsite_sale_file {
    my $file=shift;
    my $uid=shift;
 
    if (-f $file) {
        print $q->p("Found $file");
     
    } else {
        print $q->p("Error: Failed to locate $file");
        return 0;
    }
    unless ($uid) {
        print $q->p("Error: No User Id");
        return 0;
    }
    # Open the file and determine the total price for the sale
    my $total_price;
    open(INPUT,"$file");
    my @input=(<INPUT>);
    close INPUT;    
    my $dbh = openODBCDriver($dsn);     
    foreach my $i (@input) {
        my @tmp=split(/,/,$i);
        my $sku=$tmp[0];
        my $qty=$tmp[1];
        my $price;
        my $query=" select retailprice from plu where plunum = '$sku'";
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            if ($tmp[0]) {
                $price="$tmp[0]";            
            }
        }  
        my $extprice=($price * $qty);
        $total_price+=$extprice;
    }
    
    $dbh->disconnect; 
    return $total_price;
    
}
sub process_offsite_sale_file {
    my $file=shift;
    my $uid=shift;
    my $total_amount=shift;
    
    if (-f $file) {
        print $q->p("Found $file");
     
    } else {
        print $q->p("Error: Failed to locate $file");
        return 0;
    }
    unless ($uid) {
        print $q->p("Error: No User Id");
        return 0;
    }
    
    # Define the import file
    my $import_file="offsite_sale_import.txt";
    my $temp_dir="c:/temp";
    my $working_file="${temp_dir}/${import_file}";
    if (-f $working_file) {
        unlink $working_file;
    }
  
    my $dbh = openODBCDriver($dsn); 
    # Determine the Offsite customer number
    my $custnum;
    $query = dequote(<< "    ENDQUERY");
        select custnum from pa_customer where lastname like 'OFFSITE'
    ENDQUERY
    
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            $custnum="$tmp[0]";            
        }
    }  
    # Trim the custnum
    #$custnum=substr($custnum,-4,4);
    if ($custnum) {
        print $q->p("Customer number: $custnum");
    } else {
        print $q->p("Error: Failed to determine the custnum for customer OFFSITE - Has it been created yet?");
        return 0;    
    }    
    # Determine the store number
    my $storenum=getStoreNum($dbh); 
    unless ($storenum) {
        print $q->p("ERROR: Failed to determine store number");
        return 0;
    }    
    # Determine the transnum
    my $transnum;
    my $txnnum;
    # Get a txnnum;
    $query = dequote(<< "    ENDQUERY");
        select max(transnum) from txn_pos_transactions
    ENDQUERY
    
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            $transnum="$tmp[0]";            
        }
    }  
    $query = dequote(<< "    ENDQUERY");
        select txnnum from txn_pos_transactions where transnum = $transnum
    ENDQUERY
    
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            $txnnum="$tmp[0]";            
        }
    }       
    
    # Clear the transnum - The next query will increment the transnum and give us the new one
    $transnum=0;
    # Insert a transaction 
 
    ###
    $query = dequote(<< "	ENDQUERY");
          select com_fun_insert_pos_transaction($storenum,
            255,
            $uid,
            0,0,0,0,0,
            $txnnum,
            getdate(),
            getdate())  
	ENDQUERY
    
    # Get that transnum 
    #$query="select max(transnum) from txn_pos_transactions";
    #$query="select max(transnum) from ptd_admin";
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            $transnum="$tmp[0]";            
        }
    }
    # Get the txnnum
    if ($transnum) {
        $query="select txnnum from txn_pos_transactions where transnum = $transnum";
        #$query="select max(transnum) from ptd_admin";
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            if ($tmp[0]) {
                $txnnum="$tmp[0]";            
            }
        }    
    }
    unless ($txnnum) {
        print $q->p("Error: Failed to determine the txnnum");
        return 0;    
    }
    if ($transnum) {
        $transnum++;
    } else {
        print $q->p("Error: Failed to determine a transnum");
        return 0;
    }
    print $q->p("Transnum: $transnum");
    # Determine a PTD Number
    my $ptdnum;
    $query="select max(ptdnum) from ptd_admin";
	$sth = $dbh->prepare($query);
	$sth->execute();
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            $ptdnum="$tmp[0]";            
        }
    }
    if ($ptdnum) {
        $ptdnum++;
    } else {
        # If we could not find one, we will just create one
        $ptdnum = 10;        
    }    
    print $q->p("Ptdnum: $ptdnum");    
    
    

    # Determine the date
    my @ltm = localtime();
    my $date_label = sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]); 
    my $date_time_label = sprintf("%04d-%02d-%02d %02d:%02d:%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3], $ltm[2],$ltm[1],$ltm[0]); 
   
    open(INPUT,"$file");
    my @input=(<INPUT>);
    close INPUT;
    open(FILE,">$working_file");
    print FILE "LH, $transnum\n";
    print FILE "LS, 0.00,$date_label\n";
    # In this next line, UID, DateTime, PT ID (4 = Offsite Sale), PT Number, Register Group Number, Register Number, Store Number - customer number
    print FILE "LA, $uid,$date_time_label,4,$ptdnum,0,1,$storenum, , , , , ,$custnum\n";
    # This next line asks for the schedule number but I don't know what that is.  Using 2 for now.
    # Third field is not supported and is 0.
    # Finally, the total of the sale including discounts but not tax.
    print FILE "LT,2,0,$total_amount\n";
    # Print a merchandise record for each item
    # 1) SKU
    # 2) ExtSellPrice
    # 3) Is it taxable for tax1?
    # 4) Is it taxable for tax2?
    # 5) Is it taxable for tax3?
    # 6) Is it taxable for tax4?
    # 7) Qty
    foreach my $i (@input) {
        my @tmp=split(/,/,$i);
        my $sku=$tmp[0];
        my $qty=$tmp[1];
        my $price;
        my $query=" select retailprice from plu where plunum = '$sku'";
        $sth = $dbh->prepare($query);
        $sth->execute();
        while (my @tmp=$sth->fetchrow_array()) {
            if ($tmp[0]) {
                $price="$tmp[0]";            
            }
        }  
        my $extprice=($price * $qty);
        print FILE "I,$sku,$extprice,0,0,0,0,$qty\n";
    }    
    close FILE;

    # Import the file
    my $batch="c:/program files/sap/retail systems/utilities/ptbatch.exe";
    if (-f $batch) {
        my $tool="ptbatch.exe";
        my $cmd="$tool $working_file";
   
        my @result=`$cmd`;
 
        foreach my $r (@result) {
            print "($r)<br />";
        }
    } else {
        print $q->p("ERROR: Failed to locate the ptbatch.exe file");
        return 0;
    }
    # Update the ptd_admin with the correct txnnum since it does not seem to do it correctly on its own
    $query = "
        select 
            txnnum
        from
            ptd_admin
        where
            transnum = $transnum
            and
            ptdnum = $ptdnum
    ";
    $sth = $dbh->prepare($query);
    $sth->execute();
    # 
    my $need_to_correct_txnnum=0;
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            unless ($tmp[0] eq $txnnum) {
                $need_to_correct_txnnum=1;
            }            
        }
    }      
    if ($need_to_correct_txnnum) {
        $query = "
            update
                ptd_admin
            set txnnum = $txnnum
            where
                transnum = $transnum
                and
                ptdnum = $ptdnum
        ";
        $sth = $dbh->prepare($query);
        $sth->execute();        
    }
    # Update the ptd_summary with the correct txnnum since it does not seem to do it correctly on its own
    $query = "
        select 
            updatetxnnum
        from
            ptd_summary
        where 
            ptdnum = $ptdnum
    ";
 
    #print $q->pre("$query");
    $sth = $dbh->prepare($query);
    $sth->execute();
    # 
    $need_to_correct_txnnum=0;
    while (my @tmp=$sth->fetchrow_array()) {
        if ($tmp[0]) {
            unless ($tmp[0] eq $txnnum) {
                $need_to_correct_txnnum=1;
            }            
        }
    }      
    if ($need_to_correct_txnnum) {
        $query = "
            update
                ptd_summary
            set updatetxnnum = $txnnum
            where
 
                ptdnum = $ptdnum
        ";
        
        #print $q->pre("$query");
        $sth = $dbh->prepare($query);
        $sth->execute();        
    }    
    # Set the customer name in ptd_admin if it is not there
    my $need_to_correct_custname=0;
     
    my $custname;
    my $custname_expected;    
    $query = "
        select 
            custnum,
            custname
        from
            ptd_admin
        where
            transnum = $transnum
            and
            ptdnum = $ptdnum
    ";
     
    #print $q->pre("$query");
    $sth = $dbh->prepare($query);
    $sth->execute();
    # 

    while (my @tmp=$sth->fetchrow_array()) {
        $custnum=$tmp[0];
        $custname=$tmp[1];
    }          
    if ($custnum) {
        # Get the custname for this custnum
        $query = "
            select 
                lastname,
                firstname
                
            from
                pa_customer
            where
                custnum = $custnum
 
        ";
        #print $q->pre("$query");
        $sth = $dbh->prepare($query);
        $sth->execute(); 
        while (my @tmp=$sth->fetchrow_array()) {
            my $lastname=$tmp[0];
            my $firstname=$tmp[1];
            $custname_expected="$lastname".", "."$firstname";
            unless ($custname_expected eq $custname) {
                $need_to_correct_custname=1;
            }
        }            
    }
    if ($need_to_correct_custname)  {
        if ($custname_expected) {
            $query = "
                update
                    ptd_admin
                set custname = '$custname_expected'
                where
                    transnum = $transnum
                    and
                    ptdnum = $ptdnum
            ";
            #print $q->pre("$query");
            $sth = $dbh->prepare($query);
            $sth->execute(); 
            # Do the ptd_summary as well
            $query = "
                update
                    ptd_summary
                set custname = '$custname_expected'
                where
                    custnum = $custnum
                    and
                    ptdnum = $ptdnum
            ";
            #print $q->pre("$query");
            $sth = $dbh->prepare($query);
            $sth->execute();             
            
        } else {
            print $q->p("WARNING: Tried to update custname in ptd_admin but could not determine what it should be.");
        }
    }        
    # Create a ptd_transaction_total entry
    my $itemnum;
    $query = "
        select 
            itemnum                        
        from
            ptd_total
        where
            transnum = $transnum

    ";
    #print $q->pre("$query");
    $sth = $dbh->prepare($query);
    $sth->execute(); 
    while (my @tmp=$sth->fetchrow_array()) {
        $itemnum=$tmp[0]; 
    }       
    if ($itemnum) {
        # Increment it and create an entry in the ptd_transaction_total table
        $itemnum++;
        $query = "
            insert into ptd_transaction_total
                VALUES ($storenum,$transnum,$itemnum,0,0,0,0,0,2,0)
        ";
        #print $q->pre("$query");
        $sth = $dbh->prepare($query);
        $sth->execute(); 
        ###
    }
    
    print $q->p("Offsite Sale Import Completed");
    $dbh->disconnect;
}

 sub doValidation {
   my ($dbh, $cid, $pwd) = @_;
   my $errorstr = '';
   validateUser($dbh, $cid, $pwd, \$errorstr);
   my $rc = 0;
   $rc = 1 unless ($errorstr);
   return $rc;
}

sub getCashier {    
    my $uid=shift;
    my $sqlany_dbh = openODBCDriver($dsn);
    my $query = "select empnum,cashiernum from employee where (empnum = $uid or cashiernum = $uid)";
    #print $q->pre("$query");
    $sth = $sqlany_dbh->prepare($query);
    $sth->execute();
    my $empnum;
    my $cashiernum;
    while (my @tmp=$sth->fetchrow_array()) {
        $empnum=$tmp[0];
        $cashiernum=$tmp[1];
    }      
    $sqlany_dbh->disconnect;  
    print $q->p("CashierNum: $cashiernum");
    return $cashiernum;
}

sub setMinMax {
    my $sku=shift;
    my $iso=shift;
    my $min=shift;
    my $max=shift;
    my $dbh = openODBCDriver($dsn);
    $query = dequote(<< "    ENDQUERY"); 
        update inventory_current
            set
                canorder = '$iso',
                minqty = $min,
                maxqty = $max
        where
            plunum = '$sku'
        
    ENDQUERY

    unless (my $rc = $dbh->do($query)) {
        ElderLog($logfile, indent(1)."setMinMax: failed to set ISO, Min, and Max for $sku");
        ElderLog($logfile, indent(2)."Database Update Error");
        ElderLog($logfile, indent(2)."Driver=ODBC");
        ElderLog($logfile, indent(2). $dbh->{Name});  # ie. "DSN=RBTest;"
        ElderLog($logfile, indent(2)."query=".$query);
        ElderLog($logfile, indent(2)."err=".$dbh->err." state: ".$dbh->state);
        ElderLog($logfile, indent(2)."errstr=".$dbh->errstr);
        
        print "Database Update Error"."<br>\n";
        print "Driver=ODBC"."<br>\n";
        print  $dbh->{Name}."<br>\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br>\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br>\n";
        print "errstr=".$dbh->errstr ."<br>\n";
        return 0;
    } else {
        ElderLog($logfile, indent(1)."set ISO Min and Max for $sku"); 
        return 1;
    }       
    
}

sub addToInvControl {
    my $sku=shift;
    my $dbh = openODBCDriver($dsn);
    my $return = 0;
    # Determine which of three tables needs to be updated
  
    my $update_ic=0;
    my $update_ih=0;
    
    $query = dequote(<< "    ENDQUERY");     
        select 
            p.plunum,
            ic.plunum,
            ih.plunum
        from
            plu p
            left outer join inventory_current ic on ic.plunum = p.plunum
            left outer join inventory_history ih on ih.plunum = p.plunum
        where
            p.plunum = '$sku'
    ENDQUERY

    #print $q->pre("$query");
    $sth = $dbh->prepare($query);
  
    $sth->execute();        
    while (my @tmp = $sth->fetchrow_array) { 
        my $psku=$tmp[0];                        
        my $pic=$tmp[1];                        
        my $pih=$tmp[2];

        unless ($pic) {
            $update_ic=1;
        }
        unless ($pih) {
            $update_ih=1;
        }            
    }         

    $query = dequote(<< "    ENDQUERY");      
        Update plu set invcontrolled = 'Y' where plunum='$sku'
    ENDQUERY
    
    unless (my $rc = $dbh->do($query)) {
        ElderLog($logfile, indent(1)."addToInvControl: failed to set $sku to inventory control in plu table");
        ElderLog($logfile, indent(2)."Database Update Error");
        ElderLog($logfile, indent(2)."Driver=ODBC");
        ElderLog($logfile, indent(2). $dbh->{Name});  # ie. "DSN=RBTest;"
        ElderLog($logfile, indent(2)."query=".$query);
        ElderLog($logfile, indent(2)."err=".$dbh->err." state: ".$dbh->state);
        ElderLog($logfile, indent(2)."errstr=".$dbh->errstr);
        
        print "Database Update Error"."<br>\n";
        print "Driver=ODBC"."<br>\n";
        print  $dbh->{Name}."<br>\n";  # ie. "DSN=RBTest;"
        print "query=".$query."<br>\n";
        print "err=".$dbh->err." state: ".$dbh->state."<br>\n";
        print "errstr=".$dbh->errstr ."<br>\n";
        return 0;
    } else {
        ElderLog($logfile, indent(1)."set $sku to inventory control in plu table");
    }       
    if ($update_ic) {
        $query = dequote(<< "        ENDQUERY");      
            insert into inventory_current(plunum,itemtype,canorder,minqty,maxqty,firstdate,
                qtypreissue,qtycurrent,lastordereddate,lastreceiveddate,lastvendorcost,
                lastvendor,lastsolddate,lastreturndate,startdate,invcontrolled,qtyonorder,
                storenum)
            values(
                '$sku',1,'N','0','0',
                (select dateadd(mm,0,cast (datepart(year,getdate()) as char(4)) + '-'+ cast(datepart(mm,getdate()) as
                char(2))+'-01')),
                0,0,null,null,0,'',null,null,
                (select dateadd(mm,0,cast (datepart(year,getdate()) as char(4)) + '-'+ cast (datepart(mm,getdate()) as
                char(2))+'-01')),
                'Y',0,(select storenum from sys_table))
        ENDQUERY
        
        unless (my $rc = $dbh->do($query)) {
            ElderLog($logfile, indent(1)."addToInvControl: failed to set $sku to inventory control in inventory_current table");
            ElderLog($logfile, indent(2)."Database Update Error");
            ElderLog($logfile, indent(2)."Driver=ODBC");
            ElderLog($logfile, indent(2). $dbh->{Name});  # ie. "DSN=RBTest;"
            ElderLog($logfile, indent(2)."query=".$query);
            ElderLog($logfile, indent(2)."err=".$dbh->err." state: ".$dbh->state);
            ElderLog($logfile, indent(2)."errstr=".$dbh->errstr);
            
            print "Database Update Error"."<br>\n";
            print "Driver=ODBC"."<br>\n";
            print  $dbh->{Name}."<br>\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br>\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr ."<br>\n";
            return 0;
        } else {
            ElderLog($logfile, indent(1)."set $sku to inventory control in inventory_current table");            
        }   
    } else {
        #print "SKU: $sku is already in inventory_current<br />"; 
    }

    if ($update_ih) {        
        $query = dequote(<< "        ENDQUERY");      
        insert into inventory_history(plunum,itemtype,periodstart,periodtype,openqty,
            closeqty,qtysold,qtyreturn,amtreturn,qtyreceived,costreceived,
            qtyordered,qtyadjusted,qtytransferedin,qtytransferedout,netsaletotal,
            retailtotal,periodend,lastvendorcost,storenum,minqtyonhand,maxqtyonhand)
        values(
            '$sku',1,
            (select dateadd(mm,0,cast (datepart(year,getdate()) as char(4)) + '-'+ cast (datepart(mm,getdate()) as
            char(2))+'-01')),
            3,0,0,0,0,0,0,0,0,0,0,0,0,0,
            (select dateadd (second, 59, dateadd(minute,-1,dateadd(mm,1,cast (datepart(year,getdate()) as char(4)) + '-'+
            cast(datepart(mm,getdate()) as char(2))+'-01')))),0,(select storenum from sys_table),0,0)


        ENDQUERY
        
        unless (my $rc = $dbh->do($query)) {
            ElderLog($logfile, indent(1)."addToInvControl: failed to set $sku to inventory control in inventory_history table");
            ElderLog($logfile, indent(2)."Database Update Error");
            ElderLog($logfile, indent(2)."Driver=ODBC");
            ElderLog($logfile, indent(2). $dbh->{Name});  # ie. "DSN=RBTest;"
            ElderLog($logfile, indent(2)."query=".$query);
            ElderLog($logfile, indent(2)."err=".$dbh->err." state: ".$dbh->state);
            ElderLog($logfile, indent(2)."errstr=".$dbh->errstr);
            
            print "Database Update Error"."<br>\n";
            print "Driver=ODBC"."<br>\n";
            print  $dbh->{Name}."<br>\n";  # ie. "DSN=RBTest;"
            print "query=".$query."<br>\n";
            print "err=".$dbh->err." state: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr ."<br>\n";
            return 0;
        } else {
            ElderLog($logfile, indent(1)."set $sku to inventory control in inventory_history table");               
        }              
    } else {
        #print "SKU: $sku is already in inventory_history<br />";        
    } 
    #print "<p>$sku placed on inventory control</p>";
    ElderLog($logfile, indent(1)."SKU $sku placed on inventory control");
     
    $dbh->disconnect;
    return 1;
}
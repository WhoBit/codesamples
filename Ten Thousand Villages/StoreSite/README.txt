Much of the web related code that I maintained runs in the store.  Each store has
a server running Apache to provide various reports and tools to the store staff
via a website (The Elder) that they can only access within the store.  The majority of this
code was written prior to my coming to Villages.  My job was to update and add
to it as needed.

This folder contains two scripts which I developed that also run on this store website.  
- i_reports_server.pl - Reports on the health of store critical systems
- i_toolshed.pl - A host of reports and tools for IT support.  Access to this
web code was hidden and restricted from the store staff.  

Also included is an example of a script which was developed prior to me but
updated by me through the years as needed.
- i_reports.pl - Provides a host of reports about their store to the store staff.
Typically these reports are pulling data out of the store database.  

2016-04-06 - kdg


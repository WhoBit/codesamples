Here are two scripts which I wrote for use in the Akron office.
- merchandisingTools.pl - This is a set of tools for the merchandising department to create
and publish promotions and MixMatch discounts.  
- postools.pl - This is a set of reports for the IT staff.  I wrote it to get information
that I wanted/needed to do my job.  

2016-04-06 - kdg



#!c:/perl/bin/perl

## posTools.pl
## A tool for POS support

## 2009-12-08  started - kdg
## 2010-03-09 - Significant revisions in selections - kdg
## 2010-03-10 - v1.0.2 - Merged in storeIssues.pl - kdg
## 2010-03-11 - v1.0.3 - Added cb_nozero.  Added default start and stop date.  Added Call Data Summary.  Enalbed secondary alias- kdg
## 2010-03-12 - v1.0.4 - Modification to printing summary of data collected.  Added STR_TO_DATE to order options - kdg
## 2010-03-12 - v1.0.5 - Fixed adding secondary alias.  Added sorting by alias - kdg
## 2010-03-15 - v1.0.6 - Implimented footer and standardfooter - kdg
## 2010-04-05 - v1.0.7 - Added Net Sales report - kdg
## 2010-04-06 - v1.0.8 - Revisions to Net Sales Report - kdg
## 2010-04-06 - v1.1.0 - Added ability to delete new issues - kdg
## 2010-04-16 - v1.2.0 - Started separating CDR/CDR-IT from CDR_REPORT - kdg
## 2010-04-21 - v1.2.1 - Added SQURPT - kdg
## 2010-04-22 - v1.2.2 - Added WIREORDR, Added options to SKU report - kdg
## 2010-04-27 - v1.2.3 - Added QTOOL - kdg
## 2010-04-29 - v1.2.4 - Revisions to phone report to corelate call and called alias,  Added RECRPT - kdg
## 2010-04-30 - v1.2.5 - Some corrections to purchase order report - kdg
## 2010-05-04 - v1.2.6 - Added mysql databases to query tool - kdg
## 2010-05-06 - v1.2.7 - Updated net sales to show tlog data even if we have no store data - kdg
## 2010-05-14 - v1.2.8 - Removed some errant code seen when updating issues  - kdg
## 2010-05-18 - v1.2.9 - Added querytext to query tool results - kdg
## 2010-06-08 - v1.2.10 - Expanded querytext a bit.  Allowed for multiple queries in the query tool.  Added check for duplicate records. - kdg
## 2010-06-22 - v1.2.11 - Added StoreInfo report - kdg
## 2010-06-30 - v1.2.12 - Added Store Discount Report - kdg
## 2010-07-26 - v1.2.13 - Added STOREINVRPT - kdg
## 2010-08-02 - v1.2.14 - Changed store reports to MISC store reports and added report selection - kdg
## 2010-08-23 - v1.2.15 - Added two reports utilizing the nspGetAkronSkuInfo.sql stored procedure - kdg
## 2010-08-26 - v1.3.0 - Created SKU reports sub menu - Added transfer reports - kdg
## 2010-09-10 - v1.3.1 - Added CurrentLocalCost to Navision SKU Report - kdg
## 2010-10-14 - v1.3.2 - Changed sort order on Store Transfers - kdg
## 2010-11-26 - v1.3.3 - Added Obsolete Items Info Report - kdg
## 2010-12-01 - v1.3.4 - Added Card Report - kdg
## 2010-12-03 - v1.3.5 - Adding Paymentech Discrepancy Report - kdg
## 2010-12-11 - v1.3.6 - Added Returns Report - kdg
## 2010-12-28 - v2.3.7 - Corrected a menu label - kdg
## 2010-12-28 - v2.3.8 - Added ptechDiscrepancyReport link - kdg
## 2010-12-30 - v2.3.9 - Disabled old ptech discrepancy reports.  Saving username as validated - kdg
## 2011-01-06 - v2.4.0 - Added links to other tools - kdg
## 2011-01-12 - v2.4.1 - Corrected setting base phone number in update_alias when there is a new entry added - kdg
## 2011-01-14 - v2.4.2 - Added a second criteria on the join in CDR_REPORT to account for multiple alias entries - kdg
## 2011-01-19 - v2.4.3 - Added tool to edit phone aliases.  Correction to report calls to alias when there are multiple alias
## 2011-01-28 - v2.4.4 - Added $uid when calling outside scripts such as merchandising.pl - kdg
## 2011-01-30 - v2.4.5 - Added Store Monitor Report to show stores by model or tender etc. - kdg
## 2011-02-03 - v2.5.0 - A bit of cleanup and re-arranging.  Revised handling of rcount_selection in query tools - kdg
## 2011-03-23 - v2.5.1 - Some revisions to store info stats - kdg
## 2011-03-25 - v2.5.2 - Some more revisions to reporting store info - kdg
## 2011-04-04 - v2.5.3 - Added STOREINFORPT - kdg
## 2011-04-08 - v2.5.4 - Updates to show throughput stats - kdg
## 2011-04-15 - v2.5.5 - Started Discounts Report - kdg
## 2011-04-19 - v2.6.0 - First working version of the Discount Report - kdg
## 2011-04-20 - v2.6.1 - Revision to summary of discount report and correction to item discounts - kdg
## 2011-04-26 - v2.6.2 - Removed cost from discount report - kdg
## 2011-04-29 - v2.6.3 - Started on version 2 of discount report - kdg
## 2011-05-16 - v2.6.4 - Added STOREPRICE_RPT - kdg
## 2011-05-17 - v2.6.5 - Filter STOREPRICE_RPT for SKUs w. Qty - kdg
## 2011-05-18 - v2.6.6 - Added checkbox to restrict skus in STOREPRICE_RPT - kdg
## 2011-05-26 - v2.6.7 - Added markdown options to Navision Sku Report - kdg
## 2011-05-31 - v2.6.8 - Added Company Only checkbox to STOREINFORPT - kdg
## 2011-06-01 - v2.6.9 - Modified STORERPT to use storeStats rather than connectInfo table - kdg
## 2011-06-09 - v2.6.10 - Added percentages to store info report III - kdg
## 2011-06-15 - v2.6.11 - Adjusted Net Sales Report to show either Tlog OR StoreData even if one is missing - kdg
## 2011-06-30 - v2.6.12 - Added SKU to Wire report by SKU - kdg
## 2011-07-07 - v2.6.13 - Added ability to limit net sales to Villages SKUs - kdg
## 2011-07-15 - v2.6.14 - Corrections to selection by new item date for Akron SKU Info report SKUQRY - kdg
## 2011-07-22 - v2.6.15 - Updated ordering of store issues display - kdg
## 2011-07-25 - v2.6.16 - Added STORESTATSRPT - kdg
## 2011-08-08 - v2.6.17 - Added STORESKUQRY - kdg
## 2011-08-16 - v2.6.18 - Revised STORESKUQRY to permit querying specific stores and to query history - kdg
## 2011-08-29 - v2.6.19 - Added Net Sales by month report - kdg
## 2011-09-27 - v2.7.0 - Started working on SALES_TXN report.  Still need to work on the multiple day query - kdg
## 2011-10-07 - v2.7.1 - Added reports to show dept and class on The Wire and in lookupdata.pm module - kdg
## 2011-10-12 - v2.7.2 - Changed the Tax report to a Sales Transaction report.  Added Tax and Discount info - kdg
## 2011-10-12 - v2.7.3 - Added Akron Lab to Net Sales - kdg
## 2011-10-13 - v2.7.4 - Worked on transaction report when date range is selected - kdg
## 2011-10-19 - v2.7.5 - Created SALES_TAX_CHECK - kdg
## 2011-10-21 - v2.7.6 - Added BULK_UPDATE_ISSUE - kdg
## 2011-11-07 - v2.7.7 - Correcting some issues with the discount report - kdg
## 2011-11-09 - v2.7.8 - Added traffic report - kdg
## 2011-11-10 - v2.7.9 - Added very basic Non Merchandise Report - kdg
## 2011-11-17 - v2.7.10 - Query issues by notes - kdg
## 2012-01-12 - v2.7.11 - Small formatting update - kdg
## 2012-02-07 - v2.7.12 - Revised the database to change date files from varchar to datetime format & so slight revisions to script - kdg
## 2012-02-10 - v2.7.13 - Using date_add to get correct end date for call detail report - kdg
## 2012-02-14 - v2.7.14 - Adding list of stores where sku is not found - kdg
## 2012-02-15 - v2.7.15 - Added Price Override Report - kdg
## 2012-02-21 - v2.8.0 - Added more sku reports to see more easily whether a SKU is on the wire.
## 2012-03-01 - v2.8.1 - Corrected call to wspGetItemData4Wire from nspGetItemData4Wire - kdg
## 2012-03-12 - v2.8.2 - Can now delete a specific issue in POS issues - kdg
## 2012-03-14 - v2.8.3 - Added Loss Prevention Report - kdg
## 2012-03-26 - v2.8.4 - Added utility to update stores table - kdg
## 2012-05-16 - v2.8.5 - Some work on the returns report to tally return type and reason - kdg
## 2012-05-23 - v2.8.6 - Added reason1 for returns to show where originally purchased - kdg
## 2012-05-25 - v2.8.7 - Added Tender Report - kdg
## 2012-06-01 - v2.8.8 - Checking for duplicate Txn in Net Sales - kdg
## 2012-06-21 - v2.9.0 - Added DEPT_CLASS_ASC to create DEPT.ASC from Navision entries - kdg
## 2012-08-17 - v2.9.1 - Finished create_vendor_asc which had been started some time ago - kdg
## 2012-09-25 - v2.9.2 - Don't show details of tender report if more than 500 lines.   - kdg
## 2012-09-25 - v2.9.3 - Added Gift Card report - kdg
## 2012-10-30 - v2.9.4 - Added link for MarketingTools - kdg
## 2012-11-09 - v2.9.5 - Correction to updating store table - kdg
## 2012-11-12 - v2.10.0 - Added ROUTERS report - kdg
## 2012-11-28 - v2.10.1 - Added Store Valuation Report - kdg
## 2012-12-14 - v2.10.2 - Some corrections to STORESTATSRPT - kdg
## 2013-01-24 - v2.10.3 - Added SKUSALE - kdg
## 2013-01-25 - v2.10.4 - Added description to SKUSALE and transaction report - kdg
## 2013-03-04 - v2.10.5 - Added 8324 to ITPOOL.  Adjusted a outer join in a query.  Added some links to get back to CDR menu - kdg
## 2013-03-12 - v2.10.6 - Moved to new server. Fixed Authentication & Style to work - saz
## 2013-04-15 - v2.10.7 - Added some formatting for ITEMS2WIRERPT - kdg
## 2013-05-30 - v2.10.8 - Updates/Corrections to DEPT_CLASS_ASC.  Adding comparison of departments in Navision with The Wire - kdg
## 2013-05-30 - v2.10.9 - Added WIRE_DEPT_CLASS_COUNT - kdg
## 2013-06-14 - v2.10.10 - Added PseudoLoyalty - kdg
## 2013-06-24 - v2.10.11 - Added link to TranId for Net Sales - kdg
## 2013-11-15 - v2.11.0 - Added Transaction Summary Report - kdg
## 2013-11-18 - v2.12.0 - Added STOREREGRPT - kdg
## 2013-12-30 - v2.12.1 - Order info_options by description - kdg
## 2014-01-04 - v2.13.0 - Added PROPROMPTRPT - kdg
## 2014-02-06 - v2.13.1 - Updated the Purchase Order report to show status of Sales and Shippment - kdg
## 2014-03-10 - v2.13.2 - Corrected tender & model number for store info - kdg
## 2014-04-04 - v2.13.3 - Added SYSTEMINVRPT - kdg
## 2014-04-25 - v2.14.0 - Added MCRPT - kdg
## 2014-05-22 - v2.14.1 - Added Ledger & Reservation Qty on SKU report from Navision - kdg
## 2014-06-02 - v2.14.2 - Added STORELIST - kdg
## 2014-07-22 - v2.14.3 - Using sum(ms.ExtNetPrice) as [Net] in NetSales2 rather than sum(ms.ExtSellPrice) as [Net] - kdg
## 2014-07-23 - v2.14.4 - Undid above change as I don't know when to use ExtNetPrice and when to use ExtSellPrice.  Corrected a formatting issue - kdg
## 2014-08-12 - v2.14.5 - Added link to salesTools.pl - kdg
## 2014-08-26 - v2.15.0 - Added TXNTOTALBYPERIOD - kdg
## 2014-09-03 - v2.16.0 - Added DISCOUNTCOUPON - Basic reporting of coupon codes used in discounts - kdg
## 2014-09-06 - v2.17.0 - Added Monthly POS Call Report - kdg
## 2014-09-10 - v2.18.0 - Added COGS Report - kdg
## 2014-09-17 - v2.19.0 - Added IN-MONTHLY-CDR-POS-RPT - kdg
## 2014-09-22 - v2.19.1 - Handling Post-Void in the CoGS report more similarly to how they are handled in The Elder. - kdg
## 2014-09-22 - v2.19.2 - Handling Layaway Pickup in the CoGS to report more similarly to how they are handled in The Elder - kdg
## 2014-10-20 - v2.19.3 - Using sum(ms.ExtNetPrice) as [Net] rather than sum(ms.ExtSellPrice) as [Net] in Net Sales Report - kdg
## 2014-10-21 - v2.19.4 - Added Country to NSKUQRY - kdg
## 2014-11-23 - v2.19.5 - Added summary option to SKUSALE - kdg
## 2014-12-12 - v2.20.0 - Added report and tool for current Artisans - kdg
## 2015-01-08 - v2.20.1 - Revised Returns report to account for changes in SAP POS 2.3 - kdg
## 2015-02-23 - v2.20 2 - Revised STORESTATRPT to si.infodata not like '' - kdg
## 2015-03-05 - v2.21.0 - Added STOREINFO - kdg
## 2015-03-18 - v2.21.1 - Added more to STOREINFO - kdg
## 2015-04-23 - v2.21.2 - Corrected store number for Akron SAP Lab from 99 to 98 - kdg
## 2015-05-06 - v2.21.3 - Corrections to inventory reports for Kitchen Kettle - kdg
## 2015-05-12 - v2.21.4 - Updating some info reports to show for all company or all contract - kdg
## 2015-05-20 - v2.21.5 - Added group for Wire Reports and added WIREGRPRPT - kdg
## 2015-05-28 - v2.21.6 - Added Tent Sale number to inventory report - kdg
## 2015-06-04 - v2.21.7 - Updated connection to The Wire - kdg
## 2015-06-13 - v2.21.8 - Revised DEPT_CLASS_ASC to create class_hash for lookupdata.pm - kdg
## 2015-06-15 - v2.21.9 - Corrections to DEPT_CLASS_ASC for dept 55 - kdg
## 2015-07-08 - v2.21.10 - Adjustment to SKU2WIRERPT to check web date for any department - kdg
## 2015-07-27 - v2.21.11 - Updated Store Info adding EFT Device driver version - kdg
## 2015-08-11 - v2.21.12 - Added an option to include inactive stores in Specific Store Info Report II - kdg
## 2015-09-11 - v2.21.13 - Fixed Kitchen Kettle in STORELIST2 - kdg
## 2015-10-09 - v2.22.0 - Added STOREVFSTAT - kdg
## 2015-10-12 - v2.22.1 - Added @fix_array - kdg
## 2015-10-15 - v2.22.2 - Some adjustments to STOREVFSTAT.  Fixes to GIFTCARD for all or company/contract selection - kdg
## 2015-10-22 - v2.22.3 - Updated STOREVFSTAT to show Register model used - kdg
## 2015-12-29 - v2.23.0 - Added LSINVRPT - kdg
## 2016-01-19 - v2.23.1 - Updated expected_term_hash for Cary & Archbold - kdg
## 2016-01-20 - v2.23.2 - Added 8204 to IT pool - kdg
## 2016-02-15 - v2.23.3 - Added register info to Tender Report - kdg
## 2016-02-25 - v2.23.4 - Revised register total count for Tender Report - kdg
## 2016-02-29 - v2.23.5 - Added EFTDeviceDriver version to VeriFone Status Report - kdg
## 2016-03-03 - v2.23.6 - Revised LSINVRPT - kdg
## 2016-03-22 - v2.23.7 - Cleaned up the stats on STOREVFSTAT - kdg

use constant SCRIPT_VERSION => "2.23.7";
use constant MINUTE => 60;
use constant HOUR => 3600;
use constant DAY  => 24 * HOUR;
use constant YEAR => 365 * DAY;
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Time::Duration qw(duration);
#use Win32::OLE;
use TTV::utilities qw(trim);
use TTV::cgiutilities qw(printResult_arrayref2 formatCurrency formatNumeric ElderLog);
use TTV::MlinkUtilities qw(openODBCDriver openFoxProDriver execQuery_arrayref
                      recordCount do_fox_insert dequote);
use DBI;
use Math::Round qw( round nearest );
use File::Basename;

my $q = new CGI;
my @source_info=();
my @issue_info=();
my @output_info=();
my $entry="";
my $current_status="";
my $update_date="";
my $new_status="";
my $note="";
my @ltm = localtime();
my $timeseconds=time();
my @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
my @previous_week_ltm=localtime($timeseconds - 604800); # The previous week
my @previous_30_ltm=localtime($timeseconds - (86400 * 30)); # 30 days ago
my $date_label = sprintf("%02d-%02d-%04d", $ltm[4]+1, $ltm[3], $ltm[5]+1900);
my $current_year=$ltm[5]+1900;
my $label_date = sprintf("%04d-%02d-%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
my $label_date_previous_day = sprintf("%04d-%02d-%02d", $previous_day_ltm[5]+1900, $previous_day_ltm[4]+1, $previous_day_ltm[3]);
my $label_date_previous_week = sprintf("%04d-%02d-%02d", $previous_week_ltm[5]+1900, $previous_week_ltm[4]+1, $previous_week_ltm[3]);
my $label_date_past = sprintf("%04d-%02d-%02d", $ltm[5]+1900, $ltm[4], $ltm[3]);
my $label_date_previous_30 = sprintf("%04d-%02d-%02d", $previous_30_ltm[5]+1900, $previous_30_ltm[4]+1, $previous_30_ltm[3]);
my $mysqld="192.168.21.42";
my $vcxdb="vcx_cdr";    # Call detail record of the VCX phone system
my $issuesdb="pos_support";
my @header=(
    "Issue No.",
    "Store No.",
    "Store Name",
    "Issue",
    "Status",
    "Notes",
    "Date Updated",
    "Date Found"
);
# Connecting to the wire
my $wirehost="192.168.37.27";
my $wireuser="rpt";
my $wireuserpw="thereporter";
my $wire_db="wire";


my %newfiles;
my $query;
# The scriptname defined like this allows the script to be named anything.  It will call itself as
# long as it is in the /p/ folder.
my $scriptname="/p/".basename($0);


my $dkcolor="ff9a00";
my $ltcolor="feeacc";
# print the HTML Header
my $style="BODY{font: normal 8pt times new roman,helvetica}\n".
          "FORM{font: normal 8pt times new roman,helvetica}\n".
          "TH{font: normal 8pt arial,helvetica}\n".
          "TD{font: normal 8pt arial,helvetica}\n".
          "table{font: bold normal 8pt arial,helvetica}\n".
          "input{font: normal 8pt times new roman,helvetica}\n".
          ".tableborder {border-color: LightBlue; border-style: solid; border-width: 1px;}\n".
          "";
$style="BODY{font: normal 11pt times new roman,helvetica}\n".
          "FORM{font: normal 11pt times new roman,helvetica}\n".
          "TH{font: bold 11pt arial,helvetica}\n".
          "TD{font: normal 11pt arial,helvetica}\n".
          "table{font: normal 11pt arial,helvetica}\n".
          "input{font: normal 11pt times new roman,helvetica}\n".
          ".tableborder {border-color: #EBEACC; border-style: solid; border-width: 1px;}\n".
          ".table {border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tablehead {background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px; color: #FFFFFF;}\n".
          "A.headerlink:link {color: #$ltcolor; text-decoration: none; }\n".
          "A.headerlink:active {color: #$ltcolor; text-decoration: none;}\n".
          "A.headerlink:visited {color: #$ltcolor; text-decoration: none;}\n".
          "A.datalink:link {color: #000000;}\n".
          "A.datalink:active {color: #000000;}\n".
          "A.datalink:visited {color: #000000;}\n".
          ".tabledata {background-color: #$ltcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tabledatanb {background-color: #$ltcolor; border-color: #$ltcolor; border-style: solid; border-width: 1px;}\n". # nb=no-border
          ".button1 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;
          ".button2 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #000000; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;
          "";
print $q->header( -type=>'text/html', -expires=>'-1');
print $q->start_html(-title=>'POS Tools', -author=>'kevin.glick@tenthousandvillages.com',-style=>{-code=>$style});

print $q->h1("POS Tools ".SCRIPT_VERSION);

my $report = ($q->param("report"))[0];

$report = "DEFAULT" unless $report;
if ($report eq "GETPARMS") {
    $report="DEFAULT";
}


#unless ($dbh) {
#    print $q->p("Error - Cannot connect to $mysqld");
#    exit;
#}

my $username   = $ENV{'REMOTE_USER'};

my $validstr;
my $errorstr;
#my @issue_info=();
#my @source_info=();
my $ITPOOL="8204,8111,8163,8108,8113,8114,8324,8198";
my $POSPOOL="8204, 8111,8324";


if ($report eq "DEFAULT") {
    # The default page.  Show the options here
    my $validstr;
    my $errorstr;
    my @issue_info=();
    my @source_info=();

=pod    I removed these redundant menu items - kdg
        ["Paymentech Reports", " /p/ptechDiscrepancyReport.pl", "DEFAULT",
        "The paymentech discrepancy reports"
        ],
        ["Call Data Report - To POS Support", "$scriptname", "CDR-IT",
        "View the call record for calls to POS Support"
        ],
=cut
    LogMsg(indent(1)."Authentication Succeeded for user $username");
    print $q->h2("POS Tools - Main Menu");
    my @reportlist = (
        ["Store Issues Report", "$scriptname", "ISSUES",
        "View the current issues for Triversity stores"
        ],
        ["Store Info Reports", "$scriptname", "STORERPTS",
        "View the store info reports"
        ],

        ["Call Data Reports Menu", "$scriptname", "PHONEMENUS",
        "More Phone report options"
        ],
        ["Akron Reports", "$scriptname", "AKRONMENUS",
        "Report pulling data from Akron databases."
        ],
        ["Wire Reports", "$scriptname", "WIREMENUS",
        "Report pulling data from The Wire databases."
        ],		
        ["SKU Reports", "$scriptname", "SKUMENUS",
        "Report looking at SKU info in Akron databases."
        ],
        ["ASC Tools", "$scriptname", "ASC_TOOLS",
        "Tools to create ASC files for Triversity stores. - BETA"
        ],
        ["Update Store Table", "$scriptname", "UPDATE_STORES",
        "Tools update the local store table"
        ],
		["Store Router List", "$scriptname", "ROUTERS",
        "Links to TTV Store Routers"
        ],

    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    print "---------------------------------------------------------------------------------------------------------<br>";
    @reportlist = (

        ["Paymentech Reports", " /p/ptechDiscrepancyReport.pl", "DEFAULT",
        "The paymentech discrepancy reports"
        ],
        ["Marketing Tools", " /p/marketingTools.pl", "MAIN",
        "Tool for processing maillist."
        ],
        ["Merchandising Tools", " /p/merchandisingTools.pl", "MAIN",
        "Tool for mixmatch discount, promotions, and transfers."
        ],
        ["Accounting Tools", " /p/accountingTools.pl", "MAIN",
        "Tool for markdowns."
        ],
        ["SKU Publishing Tool", " /p/createStoreStandardSkus.pl", "DEFAULT",
        "Tool for publish SKUs."
        ],
        ["Sales Tools", " /p/salesTools.pl", "MAIN",
        "Tool for sales managers"
        ],		
        ["List Tool", " /p/listTools.pl", "SIGNIN",
        "Tools for phpList."
        ],

    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
	standardfooter();
    exit;
} elsif ($report eq "SKUMENUS"){
    print $q->h2("SKU Reports Menu");
    my @reportlist = (
        ["Akron SKU Info Report ", "$scriptname", "SKUQRY",
        "Report showing details on all or selected SKUs from AkronSKUInfo Table."
        ],
        ["Navision SKU Info Report ", "$scriptname", "NSKUQRY",
        "Report showing details on all or selected SKUs from Navision Database."
        ],
        ["Navision Active SKU Report ", "$scriptname", "NACTSKURPT",
        "Report showing results of nspGetAkronSkuInfo procedure."
        ],
        ["Navision Obsolete SKU Report ", "$scriptname", "NOBSSKURPT",
        "Report showing obsolete SKUs."
        ],
        ["Navision SKU Report ", "$scriptname", "GETAKRONSKUQRY",
        "Report showing results of nspGetAkronSkuInfo procedure for specific SKU."
        ],
        ["Navision Non-Villages SKU Report ", "$scriptname", "NVACTSKURPT",
        "Report showing results of tspGetVendorStandardSKU procedure."
        ],
        ["Navision SKU Non-Villages Report ", "$scriptname", "GETVENDORSKUQRY",
        "Report showing results of tspGetVendorStandardSKU procedure for specific SKU."
        ],
        ["Current User Flags", "$scriptname", "UFRPT",
        "Show the current Seasonality Codes,VMCodes, etc."
        ],
        ["Current Artisans", "$scriptname", "ARTRPT",
        "Show the current Artisan Codes"
        ],		
        ["Current Departments", "$scriptname", "DEPT_ONLY",
        "Show the current Departments."
        ],
        ["Current Departments & Classes", "$scriptname", "DEPT_CLASS",
        "Show the current Departments and Classes.  (Option to create ASC and class_hash for lookupdata.pm)"
        ],
        ["Wire Departments & Classes", "$scriptname", "WIRE_DEPT_CLASS",
        "Show the Departments & Classes on The Wire."
        ],
        ["Wire Departments & Classes Count", "$scriptname", "WIRE_DEPT_CLASS_COUNT",
        "Show the number of items in the Departments & Classes on The Wire."
        ],		
        ["LookupData Departments & Classes", "$scriptname", "LOOKUPDATA_DEPT_CLASS",
        "Show the Departments & Classes in the lookupdata.pm module."
        ],
        ["Items to Wire Report - Part 1", "$scriptname", "ITEMS2WIRE",
        "Report showing the initial query of what items are getting put on The Wire via the catalog_sync."
        ],
        ["Items to Wire Report - Part 2", "$scriptname", "ITEMSNOT2WIRE",
        "Report showing what items NOT getting to The Wire via the catalog_sync."
        ],
        ["Items to Wire Report - Final", "$scriptname", "ITEMS2WIREQRY",
        "Report showing what final set items that are getting put on The Wire via the catalog_sync."
        ],
        ["Items to Wire Report - SKU", "$scriptname", "SKU2WIRE",
        "Report showing what info is being sent to The Wire for a specific SKU."
        ],
        ["Items to Wire Report - Estimation", "$scriptname", "SKU2WIREB",
        "Report showing whether a SKU is likely to be on The Wire and if not, why."
        ],
        ["Store Price Discrepancy", "$scriptname", "STOREPRICE_QRY",
        "Show the differences between Akron prices and the store prices."
        ],
        ["SKUs on The Wire", "$scriptname", "SKUWIRE_QRY",
        "Show the SKUs in the catalog on The Wire."
        ],
        ["SKU in The Stores", "$scriptname", "STORESKUQRY",
        "Search for a SKU in the Stores."
        ],
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    standardfooter();
} elsif ($report eq "AKRONMENUS"){
    print $q->h2("Akron Reports Menu");
    my @reportlist = (
        ["Sales, Discounts, & Transaction Reports ", "$scriptname", "SALES_AND_DISCOUNTS",
        "Report showing data from Akron's perspective."
        ],

        ["Purchase Order Report ", "$scriptname", "POQRY",
        "Report showing purchase orders for a store."
        ],
        ["Receiving Report ", "$scriptname", "RECQRY",
        "Report showing info a tblRecv.tsv's for a store."
        ],
        ["Transfer Report ", "$scriptname", "XFRQRY",
        "Report showing Company Store Transfers."
        ],

        ["Traffic Report ", "$scriptname", "TRAFFICQRY",
        "Report showing People Counts/Traffic in Stores."
        ],

        ["Akron Query Tool ", "$scriptname", "QTOOL",
        "Query one of the Akron databases."
        ],
        ["PseudoLoyalty", "$scriptname", "PLOYALTY",
        "A Pseudo Loyalty Program (Under Development)."
        ],	
        ["ProPrompt", "$scriptname", "PROPROMPTQRY",
        "Query the Profile Prompt Response for a store."
        ],	 		
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    standardfooter();
} elsif ($report eq "PHONEMENUS"){
    print $q->h2("Phone Reports Menu");
    my @reportlist = (
        ["Call Data Report - All Phones", "$scriptname", "CDR",
        "View the call record from the 3Com phone system"
        ],
        ["Call Data Report - To POS Support", "$scriptname", "CDR-IT",
        "View the call record for calls to POS Support"
        ],
        ["POS Support Monthly Report", "$scriptname", "MONTHLY-CDR-POS",
        "View the calls to and from POS Support by month."
        ],
        ["Incoming POS Support Monthly Report", "$scriptname", "IN-MONTHLY-CDR-POS",
        "View the calls to POS Support by month."
        ],		
        ["Call Data Summary Report ", "$scriptname", "CDR-SUM",
        "Report showing which days there are records for phone calls."
        ],
        ["Call Data Alias Edit Tool ", "$scriptname", "CDR_AE_TOOL",
        "Tool to edit existing aliases."
        ],
        ["Call Data Phone Number Tool ", "$scriptname", "CDR_PN_TOOL",
        "Tool to copy full numbers to 10 digit numbers."
        ],

    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    standardfooter();
} elsif ($report eq "STORERPTS"){
    print $q->h2("Store Reports Menu");
    my @reportlist = (
        ["Store Monitor Info Reports", "$scriptname", "STOREQRY",
        "Report on store info such as system model numbers, tenders, and traffic counter info."
        ],
 
        ["Store Report - Discount Group", "$scriptname", "STOREDISCGRP",
        "Shows the discount group for stores and partners etc."
        ],
        ["Store Inventory Report", "$scriptname", "STOREINVQRY",
        "Shows SKUs which reported QtyExtra"
        ],




        ["Returns Report", "$scriptname", "RETURNSQRY",
        "View returns report by store. - BETA - "
        ],
        ["Loss Prevention Report", "$scriptname", "LOSSPREVENTIONQRY",
        "View transaction stats report by store. - BETA - "
        ],
        ["Store List", "$scriptname", "STORELIST",
        "View List of Stores by Store Name or Store Number."
        ],	
        ["Store VeriFone Status", "$scriptname", "STOREVFSTAT",
        "Show if VeriFone is active in Stores. - BETA - "
        ],	
        ["LS Retail Store Inventory", "$scriptname", "LSINVQRY",
        "Show Store info requested by LS Retail. - BETA - "
        ],			

    );
    # Disabled paymentech discrepancy tools:
=pod
        ["Paymentech Discrepancy Report II", "$scriptname", "PMTDISCTOOL",
        "View and adjust Paymentech Transaction discrepancies."
        ],
        ["Card Discrepancy Report II", "$scriptname", "TRIDISCTOOL",
        "View and adjust Triversity Transaction discrepancies."
        ],
        ["Paymentech Discrepancy Report ", "$scriptname", "PMTDISCQRY",
        "Search for discrepancies between Paymentech and Triversity."
        ],
        ["Paymentech Match Report ", "$scriptname", "PMTMTCHQRY",
        "Show matches between Paymentech and Triversity Transactions."
        ],
        ["Card Search ", "$scriptname", "CRDQRY",
        "Search for transactions for a specific card."
        ],
        ["Paymentech Search ", "$scriptname", "PMTQRY",
        "Search for transactions for a specific card."
        ],
=cut
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    standardfooter();
} elsif ($report eq "WIREMENUS"){
    print $q->h2("Wire Reports Menu");
    my @reportlist = (
        ["Wire Order Report ", "$scriptname", "WIREORDRQRY",
        "Report showing orders placed on The Wire."
        ],
        ["Wire Query Tool ", "$scriptname", "WQTOOL",
        "Query the databases on The Wire."
        ],	 
        ["Wire Group Report ", "$scriptname", "WIREGRPQRY",
        "Show who is in what group on The Wire."
        ],			
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
    standardfooter();	
} elsif ($report eq "SKUWIRE_QRY"){
    print $q->h2("Query SKUs on The Wire Report");
    #my $host="192.168.37.27";
    #my $user="rpt";
    #my $pw="thereporter";
    #my $db_selection="wire";
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;	


    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }
    $query = "
    SELECT distinct(field_item_plinid_value)
    FROM content_type_item
    order by field_item_plinid_value
    ";
    my $vm_options= $q->option({-value=>""}, "");
    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias) {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $name = trim($$thisrow[0]);
        next unless ($name);
        $vm_options .= $q->option({-value=>$name}, "$name");
    }
    $query = "
    SELECT distinct(field_item_status_value)
    FROM content_type_item
    order by field_item_status_value
    ";
    my $status_options= $q->option({-value=>""}, "");
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias) {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $name = trim($$thisrow[0]);
        next unless ($name);
        $status_options .= $q->option({-value=>$name}, "$name");
    }
    $query = "
    SELECT distinct(field_item_supplier_name_value)
    FROM content_type_item
    order by field_item_supplier_name_value
    ";
    my $supplier_options= $q->option({-value=>""}, "");
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias) {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $name = trim($$thisrow[0]);
        next unless ($name);
        $supplier_options .= $q->option({-value=>$name}, "$name");
    }
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUWIRE_RPT"});

    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("SKUs on The Wire")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "VMCode:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"vm_selection", -size=>"1"}), $vm_options)
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Status:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"status_selection", -size=>"1"}), $status_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Supplier:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"supplier_selection", -size=>"1"}), $supplier_options)
              );
    ####

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Limit:"),
			   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({  -type=>'text', -name=>"limit_selection", -size=>"4" -value=>"100"}))
			  );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({-accesskey=>'E', -type=>"checkbox", -name=>"show_dollar", -checked}) .
        $q->font({-size=>-1}, "Show Dollar Signs")
        ));
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->p();
    standardfooter();
} elsif ($report eq "SKUWIRE_RPT"){
    print $q->h2("SKUs on The Wire");
    my $supplier_selection   = trim(($q->param("supplier_selection"))[0]);
    my $status_selection   = trim(($q->param("status_selection"))[0]);
    my $vm_selection   = trim(($q->param("vm_selection"))[0]);
    my $show_dollar   = trim(($q->param("show_dollar"))[0]);
    my $limit_selection   = trim(($q->param("limit_selection"))[0]);
    my $where_clause;
    my $limit;
    if ($supplier_selection) {
        $where_clause .= " and cti.field_item_supplier_name_value like '$supplier_selection'";
    }
    if ($status_selection) {
        $where_clause .= " and cti.field_item_status_value like '$status_selection'";
    }
    if ($vm_selection) {
        $where_clause .= " and cti.field_item_plinid_value like '$vm_selection'";
    }
    if ($limit_selection) {
        $limit = "limit $limit_selection";
    }
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;


    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }
    my $query = "
        select
            n.title as SKU,
            cti.field_item_item_name_value as Name,
            cti.field_item_merch_class_value as Category,
            cti.field_item_regcost_value as Cost,
            cti.field_item_price_value as Price,
            cti.field_item_weight_value as Weight,
            cti.field_item_ttvdesc_value as 'Desc'

        from
            node n
            join content_type_item cti on n.nid = cti.nid
        where
            n.type like 'item'
        $where_clause
        order by n.title
        $limit
    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        my @format;
        if ($show_dollar) {
            @format = ('', '','',
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},
            '','',);
        }
        printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->b("no records found<br>\n");
    }
    $dbh->disconnect;
    standardfooter();
} elsif ($report eq "STOREPRICE_QRY"){
    print $q->h2("Store Price Report");
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $store_options= $q->option({-value=>'V1'}, "All Company Stores");
    #$query = "select storeId,storeName from stores order by storeId";
    $query = "
    SELECT [StoreId]
      ,[StoreName]

    FROM [****Data].[dbo].[rtblStores]
    where EndDate is  NULL";

    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias) {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $number = trim($$thisrow[0]);
        my $name = trim($$thisrow[1]);
        $store_options .= $q->option({-value=>$number}, "$number - $name");
    }
    $dbh->disconnect;
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREPRICE_RPT"});

    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Price Reports")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options)
              );


    ####
    # Add a checkbox
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({-accesskey=>'E', -type=>"checkbox", -name=>"sku_restriction", -checked}) .
        $q->font({-size=>-1}, "Show only SKUs that are marked down")
        ));
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({-accesskey=>'E', -type=>"checkbox", -name=>"sku_qty_restriction", -checked}) .
        $q->font({-size=>-1}, "Show only SKUs w. Qty")
        ));
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->p();
    standardfooter();
} elsif ($report eq "STOREPRICE_RPT"){
    print $q->h2("Store Price Report");

    my $store_selection   = trim(($q->param("store_selection"))[0]);
    my $where_condition;
    my $restricted_label;
    my $qty_restricted_label = "(Showing SKUs with or without Qty.)";
    my $sku_qty_restriction   = trim(($q->param("sku_qty_restriction"))[0]);
    if ($sku_qty_restriction) {
        $qty_restricted_label="(Showing only SKUs w. Qty.)";
        $where_condition.=" and tss.Qty > 0";
    }

    my $sku_restriction   = trim(($q->param("sku_restriction"))[0]);
    if ($sku_restriction) {
        $restricted_label="(Showing only SKUs marked down.)";
        $where_condition.=" and vi.[Item Disc_ Group] like 'MARK%'";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my @format = ('', '', '',
    {'align' => 'Right', 'currency' => ''},
    {'align' => 'Right', 'currency' => ''},
    {'align' => 'Right', 'currency' => ''},
    {'align' => 'Right', 'num' => ''},
    );
    my @links;

    if ($store_selection eq "V1") {
        $query = "
        SELECT
            tss.StoreId,
            rs.[StoreName],
            count(tss.SKU) as Count

        FROM
            [****Data].[dbo].[tblStoreSkuInfo] tss
            join [****].[dbo].[Villages\$Item] vi on vi.[No_] COLLATE DATABASE_DEFAULT = tss.[SKU] COLLATE DATABASE_DEFAULT
            join [****Data].[dbo].[rtblStores] rs on tss.StoreId = rs.StoreId
            join [****Data].[dbo].[tblAkronSkuInfo] tasi on tasi.SKU = tss.SKU
        where
            rs.[StoreCode] = 'V1'
        and
            rs.EndDate is NULL
        and
            tss.[Price] <>  vi.[Unit Price]
        and
            tss.SKU not like '22%'
        and
            tss.SKU not like '99%'

        $where_condition
        group by tss.StoreId,rs.[StoreName]
        order by tss.[StoreId]
        ";
        @format = ('', '', '', '',
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        @links = ( "$scriptname?report=STOREPRICE_RPT&sku_qty_restriction=$sku_qty_restriction&sku_restriction=$sku_restriction&store_selection=".'$_' );

    } else {
        $query = "
        SELECT
            tss.StoreId,
            tss.SKU,
            tss.descrip,
            tss.price as 'StorePrice',
            vi.[Unit Price] as 'UnitPrice',
            tasi.price as 'AkronPrice',
            tss.Qty
        FROM
            [****Data].[dbo].[tblStoreSkuInfo] tss
            join [****].[dbo].[Villages\$Item] vi on vi.[No_] COLLATE DATABASE_DEFAULT = tss.[SKU] COLLATE DATABASE_DEFAULT
            join [****Data].[dbo].[tblAkronSkuInfo] tasi on tasi.SKU = tss.SKU
        where
            tss.[StoreId] = '$store_selection'
        and
            tss.[Price] <>  vi.[Unit Price]
        and
            tss.SKU not like '22%'
        and
            tss.SKU not like '99%'

        $where_condition
        ";
    };



    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        print $q->b("Store Price Discrepancies $restricted_label $qty_restricted_label<br>\n");

        printResult_arrayref2($tbl_ref, \@links,\@format);
    } else {
        print $q->b("No Store Price Discrepancies found. <br>\n");
    }
    print $q->p();
    $dbh->disconnect;
    standardfooter();

} elsif ($report eq "UFRPT"){
    print $q->h2("Current User Flag Code Report");
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    #UserFlag1
    $query = "
    SELECT
        [Code] ,[Description]
    FROM
        [****].[dbo].[Villages\$TTV System Rules]
    where
        [Ruleid] like 'SEASONAL'
        and [Status] < 2
    order by [Code]
    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        print $q->b("Seasonality Codes<br>\n");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no Seasonality Codes found<br>\n");
    }
    print $q->p();
    #UserFlag2
    $query = "
    SELECT
        [Code] ,[Description]
    FROM
        [****].[dbo].[Villages\$TTV System Rules]
    where
        [Ruleid] like 'VMCODE'
        and [Status] < 2
    order by [Code]
    ";
    $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        print $q->b("VMCodes<br>\n");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no VMCodes found<br>\n");
    }
	$dbh->disconnect;
    standardfooter();

} elsif ($report eq "ARTRPT") {
    print $q->h2("Current Artisan Report");
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
   
    $query = "
    SELECT
		distinct(ii.[Vendor No_]) as VendorNo, 
		v.[Short Name] as ShortName,
		v.[Name] as VendorName
    FROM
        [****].[dbo].[Villages\$Item] ii
		join [****].[dbo].[Villages\$Vendor] v on ii.[Vendor No_] = v.[No_]
 
    order by v.[Short Name]
    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        print $q->b("Artisan Codes<br>\n");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no Artisan Codes found<br>\n");
    }
    print $q->p();
 
	$dbh->disconnect;
    standardfooter();

} elsif (($report eq "DEPT_CLASS") || ($report eq "DEPT_ONLY")){
	my $where;
	my $class;
	my $class2;
	my $query;
	if ($report eq "DEPT_CLASS") {
		print $q->h2("Current Department & Class Report");
			$where = " and c.Class <> '00' ";
			$class = "c.Class,";
			$class2 = "'',";

			###
		$query = "
		select
			d.Dept,
			c.Class,
			c.[Description],
			c.[POS Description]
		from
			[Villages\$Dept_Class] d
			join [Villages\$Class] c on d.[Dept] = c.[Dept] and d.[List Type] = c.[List Type]
		where
			d.[List Type] = 0
			and c.Class <> '00'
		union
		select
			d.[Dept],
			'',
			d.[Description],
			d.[POS Description]
		from
			[Villages\$Dept_Class] d
		where
			d.[List Type] = 0

		order by 1,2
		";
			###

	} else {
		print $q->h2("Current Department Report");
=pod
		# Modify where to show only departments
		$where = " and c.Class = '00' ";
			###
		$query = "
		select
			d.Dept,
			c.Class,
			c.[Description],
			c.[POS Description]
		from
			[Villages\$Dept_Class] d
			join [Villages\$Class] c on d.[Dept] = c.[Dept] and d.[List Type] = c.[List Type]
		where
			d.[List Type] = 0
			and c.Class = '00'
		union
		select
			d.[Dept],
			'',
			d.[Description],
			d.[POS Description]
		from
			[Villages\$Dept_Class] d
		where
			d.[List Type] = 0
			--and c.Class > 0

		order by 1,2
		";
=cut
		# I am not certain what I was thinking with the above complex query.  To just show the departments, this seems
		# more appropriate now. 2012-11-19  kdg
		$query= "
		select
			d.[Dept],
			d.[Description],
			d.[POS Description]
		from
			[Villages\$Dept_Class] d
		where
			d.[List Type] = 0


		order by d.[Dept]
		";
			###
	}

    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');


    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {

        printResult_arrayref2($tbl_ref);
		if ($report eq "DEPT_CLASS") {
			# Give the option to print out an asc file based on this
			print $q->p(
			$q->a({-href=>"$scriptname?report=DEPT_CLASS_ASC", -class=>"button1"}, "DEPT").
			"Create a DEPT.ASC & GROUP.ASC file for these departments and a class_hash for the lookupdata.pm file" );
		}
    } else {
        print $q->b("no data found<br>\n");
    }
    $dbh->disconnect;
    standardfooter();
} elsif ($report eq "DEPT_CLASS_ASC") {
	$query = "
	select
		d.Dept,
		c.Class,
		c.[Description],
		c.[POS Description]
		
	from
		[Villages\$Dept_Class] d
		join [Villages\$Class] c on d.[Dept] = c.[Dept] and d.[List Type] = c.[List Type]
	where
		d.[List Type] = 0
		and c.Class <> '00'
	union
	select
		d.[Dept],
		'',
		d.[Description],
		d.[POS Description]
	from
		[Villages\$Dept_Class] d
	where
		d.[List Type] = 0

	order by 1,2
	";
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $sth=$dbh->prepare("$query");
    $sth->execute();

	my $fill;

	my $upload_dir = "/temp/POS";
	my $deptASCfile="${upload_dir}/DEPT.ASC";
	my $groupASCfile="${upload_dir}/GROUP.ASC";
	my $groupASCfileEphrata="${upload_dir}/GROUP_Ephrata.ASC";
	my $classPMfile="${upload_dir}/lookupdata.pm";
	if (open(OUTFILE,">$deptASCfile")) {
		if (open(OUTFILE2,">$groupASCfile")) {
			if (open(OUTFILE3,">$groupASCfileEphrata")) {
				if (open(OUTFILE4,">$classPMfile")) {
					my $count=0;
					my $class_hash_counter=0;
					my %group_hash;
					while (my @tmp = $sth->fetchrow_array()) {
						my $group_fill='';
						my $dept=$tmp[0];
						$group_hash{$dept}=1;
						if ($dept == 69) {
							# Department 69 uses a different fill that allows a price of $0.00
							$fill=",Y,N,Y,Y,N,N,Y,,,,,,Y,Y,,N,N,N,,,N,Y,,,N,,,,,,,,,,,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,Y,,,,,N,Y,N,N,N,N,N,N,N,N,N,N,N,N,N,N,,N,0,,,,,Y,N,2,";
						} else {
							$fill=",Y,N,Y,Y,N,N,Y,,,,,,Y,Y,,N,N,N,,,N,N,,,N,,,,,,,,,,,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,Y,,,,,N,Y,N,N,N,N,N,N,N,N,N,N,N,N,N,N,,N,0,,,,,Y,N,2,";
						}
						# Certain departments we never put in the store so filter them out here
						if (($dept > 70) && ($dept < 80)) {
							next;
						}
		
						my $class=$tmp[1];
						my $desc=$tmp[3];
						if ($dept == 55) {
							# ECommerce Only
							#next;
							# There is no POS Description for this dept so use description
							$desc=$tmp[2];
						}									
					
						# Strip any commas from the description
						$desc=~ s/\,//g;
						# Strip off leading spaces
						if ($desc =~ /^ /) {
							$desc =~ s/ //;
						}
						# Strip off trailing spaces
						if ($desc =~ / $/) {
							my $length=length($desc);
							$length--;
							my $new_desc=substr($desc,0,$length)
						}
						# Eliminate certain entries based on their description
						if ($desc eq "RETIRED") {
							#next;
						}
						
						
						# From the information we collected from the database query, build the fields for the asc file
						if ($class eq '') {
							$class="00";
							$group_fill="$dept"."-"."$desc".",,,,";
							print OUTFILE2 "$dept,1,$group_fill\n";
							# The Ephrata store is the only store using categories other than 1
							# We create a group asc file specifically for Ephrata here
							my $category=1;
							if ($dept eq 82) {
								$category=2;
							}
							if ($dept eq 83) {
								$category=3;
							}
							if ($dept eq 84) {
								$category=4;
							}
							print OUTFILE3 "$dept,$category,$group_fill\n";
						}

						my $dept_class="$dept"."$class";
						
						print OUTFILE4 "\'$dept_class\' => \'$desc\',";
						$class_hash_counter++;							
						if ($class_hash_counter > 5) {
							print OUTFILE4 "\n";
							$class_hash_counter=0;
						}
						
						# Modify the description
						$desc="$dept_class ".$desc;
						print OUTFILE "$dept_class,$dept,$desc,$fill\n";						
						$count++;

					}
					# At the end, we add one more entry which is not found in Navision
					print OUTFILE "9999,99,MISCELLANEOUS SKU,,N,N,Y,Y,N,N,Y,,,,,,Y,Y,,N,N,N,,,N,N,,,N,,,0,,1,26,,,,,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,Y,,,,,N,Y,N,N,N,N,N,N,N,N,N,N,N,N,N,N,0,N,0,,,,,Y,N,2,\n";
					print OUTFILE2 "99,1,MISC SKU,,,,\n";
					print OUTFILE3 "99,1,MISC SKU,,,,\n";

					close OUTFILE;
					close OUTFILE2;
					close OUTFILE3;
					close OUTFILE4;

					if (-f $deptASCfile) {
						print $q->p("Processed $count departments");
						print $q->p("Created new ASC file $deptASCfile");
					} else {
						print $q->p("ERROR:  Failed to create $deptASCfile");
					}
					if (-f $groupASCfile) {
						my $group_count=keys(%group_hash);
						print $q->p("Processed $group_count groups");
						print $q->p("Created new ASC file $groupASCfile");
					} else {
						print $q->p("ERROR:  Failed to create $groupASCfile");
					}
					if (-f $groupASCfileEphrata) {
						print $q->p("Created new ASC file $groupASCfileEphrata");
					} else {
						print $q->p("ERROR:  Failed to create $groupASCfileEphrata");
					}
					if (-f $classPMfile) {
						print $q->p("Created new PM file $classPMfile");
					} else {
						print $q->p("ERROR:  Failed to create $classPMfile");
					}					
				} else {
					print $q->p("ERROR:  Failed to create $classPMfile");
				}					
			} else {
				print $q->p("ERROR:  Failed to create $groupASCfileEphrata");
			}
		} else {
			print $q->p("ERROR:  Failed to create $groupASCfile");
		}
	} else {
		print $q->p("ERROR: Failed to open $deptASCfile");
	}
    $dbh->disconnect;
    standardfooter();
	
} elsif ($report eq "WIRE_DEPT_CLASS") {
	# First get the department & class info from Navision
	$query = "
	select
		d.Dept,
		c.Class,
		c.[Description],
		c.[POS Description]
	from
		[Villages\$Dept_Class] d
		join [Villages\$Class] c on d.[Dept] = c.[Dept] and d.[List Type] = c.[List Type]
	where
		d.[List Type] = 0
		and c.Class <> '00'
	union
	select
		d.[Dept],
		'',
		d.[Description],
		d.[POS Description]
	from
		[Villages\$Dept_Class] d
	where
		d.[List Type] = 0

	order by 1,2
	";
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $sth=$dbh->prepare("$query");
    $sth->execute();	
	my %department_hash=();
	my %department_name_hash=();	
	my %department_hash_of_names=();
	while (my @tmp = $sth->fetchrow_array()) {		 
		my $dept=$tmp[0]; 			
		my $class=$tmp[1];
		my $desc=$tmp[2];
		if ($dept) {
			if ($class) {
				my %class_hash;
				# If we have a department number AND a class number, this defines a class
				if ($department_hash{$dept}) {				
					my $ref=$department_hash{$dept};
					if ($ref == 1) {
						# We have not entered any classes yet so enter the first one
						$class_hash{$class}="$desc";
					} else {
						# We are adding classes to an existing hash
						%class_hash=%$ref;
						$class_hash{$class}="$desc";					
					}
					# Store the class has inside the department hash
					$department_hash{$dept}=\%class_hash;
				} else {
					LogMsg("Error: We encountered a class for dept $dept but have no entry for it in the department_hash");
				}
				
			} else {
				# If we have a department number but no class, then this defines the department
				# If we have not already defined a department for this number, do it now.
				unless ($department_hash{$dept}) {
					$department_hash{$dept}=1;
				}
				unless ($department_name_hash{$dept}) {
					$department_name_hash{$dept}=$desc;
					$department_hash_of_names{$desc}=$dept;
				}				
			}
		}		
	}	
	$dbh->disconnect;
	
	# Second get the department & class info from The Wire
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;


    $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }
	# Departments and classes are for either a product or a supply.  The way to determine whether the department or class is for
	# one or the other is rather cludgey.  I simply get a list of all of the terms used by supplies and then use that to
	# separate the departments/class into those for products and those for supplies.
	my @supply_terms;
	my %wire_supply_hash=();
    my $query = "
	SELECT
		DISTINCT(td.tid)
	FROM
		term_data td
		JOIN term_node tn ON td.tid = tn.tid
		JOIN node n ON tn.nid = n.nid
		JOIN content_type_item i ON (n.nid = i.nid AND field_item_type_value = 'supply')

    ";
    $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
		push(@supply_terms,$tmp[0]);
		
	}
	my @product_terms;
    $query = "
	SELECT
		DISTINCT(td.tid)
	FROM
		term_data td
		JOIN term_node tn ON td.tid = tn.tid
		JOIN node n ON tn.nid = n.nid
		JOIN content_type_item i ON (n.nid = i.nid AND field_item_type_value = 'product')

    ";
    $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
		push(@product_terms,$tmp[0]);
	}

	# Get the departments and classes from the wire
	my %wire_department_hash=();		# This hash will be used to compare against the department_hash created from Navision
	my %wire_department_name_hash=();	# This hash will be used to compare against the department_name_hash created from Navision
	
	my %dept_hash;
	my %class_hash;
	my %parent_hash;
	my @term_list;
    $query = "
	SELECT
		t.tid,
		t.name,
		h.parent
	FROM
		term_data t
		INNER JOIN term_hierarchy h ON t.tid = h.tid
	WHERE
		t.vid = 22
	order by
		t.tid

    ";

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $term_id = trim($$thisrow[0]);
			my $name = trim($$thisrow[1]);
			my $parent = trim($$thisrow[2]);
			push(@term_list,$term_id);
			if ($parent) {
				# Only classes have parents
				$class_hash{$term_id}=$name;
				$parent_hash{$term_id}=$parent;
			} else {
				$dept_hash{$term_id}=$name;
				$wire_department_name_hash{$name}=1;
			}
		}
		my @header=("Department","Class","TID");
		my @array_to_print=\@header;
		my @supply_header=("Department","TID");
		my @array_to_print_supply=\@supply_header;
		foreach my $term (@term_list) {
			# Determine if this term is one used for supplies
			my $is_supply_term=0;
			my $is_product_term=0;
			foreach my $s (@supply_terms) {
				if ($s == $term) {
					$is_supply_term=1;
					last;
				}
			}
			foreach my $s (@product_terms) {
				if ($s == $term) {
					$is_product_term=1;
					last;
				}
			}
			if ($dept_hash{$term}) {
				my @array;
				my @sarray;
				# This is a department
				my $wire_dept=$dept_hash{$term};
				unless ($wire_department_hash{$wire_dept}) {
					$wire_department_hash{$wire_dept}=1;
				}
				
				if ($is_supply_term) {
					@sarray=("$dept_hash{$term}","$term");
					push(@array_to_print_supply,\@sarray);					
					$wire_supply_hash{$dept_hash{$term}}=1;
				}
				if ($is_product_term) {
					@array=("$dept_hash{$term}","","$term");
					push(@array_to_print,\@array);
				}

				# Now look for any classes in this department

				foreach my $class_id (sort(keys(%parent_hash))) {
					my %tmp_hash;
					my @class_array;
					if ($parent_hash{$class_id} == $term) {
						# This is a class within this department
						$tmp_hash{$class_hash{$class_id}}=$class_id;
					}
					foreach my $class (sort(keys(%tmp_hash))) {
						my $tid=$tmp_hash{$class};
						@class_array=("","$class","$tid");
						# Supplies do not have classes - This is important to comment out because
						# otherwise, the bags which have classes in products but not in supplies
						# will end up getting the same classes as they do in products - kdg
						#if ($is_supply_term) {
						#	push(@array_to_print_supply,\@class_array);
						#	$wire_supply_hash{$class}=1;
						#}
						if ($is_product_term) {
							push(@array_to_print,\@class_array);
						}
						###
						my %wire_class_hash;
						# If we have a department number AND a class number, this defines a class
						if ($wire_department_hash{$wire_dept}) {				
							my $ref=$wire_department_hash{$wire_dept};
							if ($ref == 1) {
								# We have not entered any classes yet so enter the first one
								$wire_class_hash{$class}=1;
							} else {
								# We are adding classes to an existing hash
								%wire_class_hash=%$ref;
								$wire_class_hash{$class}=1;					
							}
							# Store the class has inside the department hash
							$wire_department_hash{$wire_dept}=\%wire_class_hash;
						} else {
							LogMsg("Error: We encountered a class for dept $wire_dept but have no entry for it in the wire_department_hash");
						}						
						###
					}
				}
			}
		}
		if (recordCount(\@array_to_print)) {
			print $q->h2("Current Departments & Classes on The Wire for Products");
			printResult_arrayref2(\@array_to_print);
		} else {
			print $q->p("Failed to determine departments and classes for Products");
		}
		if (recordCount(\@array_to_print_supply)) {
			print $q->h2("Current Departments on The Wire for Supplies");
			printResult_arrayref2(\@array_to_print_supply);
		} else {
			print $q->p("Failed to determine departments and classes for Supplies");
		}

    } else {
        print $q->b("no data found.");
    }

    $dbh->disconnect;
	
	my $discrepancy_count=0;
	# And finally, by carefully comparing the hashes that we have created, we should be able to determine if The Wire agrees with Navision
	foreach my $navision_dept (sort keys (%department_hash)) {
		my $navision_dept_name=$department_name_hash{$navision_dept};
		# Several departments are not sent to the wire - exclude them here
		next if ($navision_dept == 69);
		next if ($navision_dept_name =~ /RETIRED/);
		next if ($navision_dept > 70);

		#next if (($navision_dept > 70) && ($navision_dept < 79));
		if ($wire_department_hash{$navision_dept_name}) {	
			print "----------------------------------------------------<br />";
			print "Found Dept: $navision_dept_name on The Wire<br />";			
			print "----------------------------------------------------<br />";
			# Get the classes for this department in Navision
			my $ref=$department_hash{$navision_dept};
			my %navision_class_hash=%$ref;
			
			# Get the class from this wire department
			my $wire_ref=$wire_department_hash{$navision_dept_name};
			next if ($wire_ref == 1);
			my %wire_class_hash=%$wire_ref;
			
			foreach my $navision_class (sort keys(%navision_class_hash)) {
				my $navision_class_desc=$navision_class_hash{$navision_class};
				next if ($navision_class_desc eq "RETIRED");
				if ($wire_class_hash{$navision_class_desc}) {
					my $field = "&nbsp;";
					print "$field Found Class: $navision_class_desc on The Wire<br />";
				} else {
					
					my $match=0;
					if (1) {
						
						foreach my $test (sort keys(%wire_class_hash)) {
							#print "($test)<br />";
							my $ncd=lc($navision_class_desc);
							my $t=lc($test);
							$ncd=~s/ //g;
							$t=~s/ //g;
							if ($ncd eq $t) {
								$match=1;
								print "Matching ($navision_class_desc) to ($test)<br />";
							}
							
						}
					}
					unless ($match) {
						$discrepancy_count++;
						print "<b>--->>>Failed to find $navision_class_desc on The Wire<<<---</b><br />";
						if (1) {
							print "Known classes for this department on The Wire are:<br />";
							foreach my $test (sort keys(%wire_class_hash)) {
								print "($test)<br />";
							}
						}
					}
				}
 
			}		
		
 		
		} else {
			print "----------------------------------------------------<br />";		
			print "<b>---->>>Department ($navision_dept) $navision_dept_name is not found on The Wire</b><br />";
			print "----------------------------------------------------<br />";
			$discrepancy_count++;
		}
 	
	
	}
	if ($discrepancy_count) {
		print $q->h3("Found $discrepancy_count discrepancies between departments/classes in Navision and The Wire");
	} else {
		print $q->p("Found no discrepancies between departments/classes in Navision and The Wire");
	}
 	if (1) {	# This is not enabled because I cannot get it work work right yet - kdg 2013-05-30
		$discrepancy_count=0;
		print "<br /><br />-- Checking that Wire Departments are correct --<br />";
		# Check that all of the departments and classes on The Wire make sense
		
		#foreach my $wire_dept (sort keys (%wire_department_hash)) {
		foreach my $wire_dept (sort keys (%wire_department_name_hash)) {
			# Skip supplies
		 
			next if ($wire_supply_hash{$wire_dept});
			if ($department_hash_of_names{$wire_dept}) {
				print "Found wire dept $wire_dept in Navision<br />";
			} else {
				print ">>>>>  wire dept $wire_dept not found in Navision<<<<<<br />";
				$discrepancy_count++;
			}
		
				
		}
		if ($discrepancy_count) {
			print $q->p("Found $discrepancy_count discrepancies between departments/classes on The Wire and in Navision");
			print "(Note: This may be due to old entries that simply have not been cleaned up.  This is not considered a serious issue.)<br /><br />";
		} else {
			print $q->p("Found no discrepancies between departments/classes on The Wire and in Navision");
		}
		
	}
	standardfooter();
} elsif ($report eq "WIRE_DEPT_CLASS_COUNT") {	
###
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;


    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }

    $query = "
	SELECT
		td.tid,
		td.name,
		h.parent,
		count(n.title) as SKUs
	FROM
		term_data td
		INNER JOIN term_hierarchy h ON td.tid = h.tid
		JOIN term_node tn ON td.tid = tn.tid
		JOIN node n ON tn.nid = n.nid
	WHERE
		td.vid = 22
		and
		n.type = 'item'
	GROUP BY
		td.tid
	order by
		td.tid

    ";
    $query = "
	SELECT
		td.tid,
		td.name,
		count(n.title) as SKUs,
		
		sum(n.status) as 'Active SKUs',
		(count(n.title) - sum(n.status)) as 'Obsolete SKUs'
		 
 
	FROM
		term_data td
		LEFT OUTER JOIN term_node tn ON td.tid = tn.tid
		LEFT OUTER JOIN node n ON tn.nid = n.nid
		LEFT OUTER JOIN node nd ON tn.nid = nd.nid
	WHERE
		td.vid = 22
 	GROUP BY
		td.tid
 
 
	order by
		td.tid

    ";	
	my %term_hash=();
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
		printResult_arrayref2($tbl_ref);
=pod		
		my $item_count=0;
		my $active_item_count=0;
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $term_id = trim($$thisrow[0]);
			my $term_name = trim($$thisrow[1]);
			my $item = trim($$thisrow[2]);
			my $item_status = trim($$thisrow[3]);
			if ($item) {
				if ($item_status) {
					$active_item_count++;
				} else {
					
				}
			}
		}		 
=cut		
    } else {
        print $q->b("no data found.");
    }

    $dbh->disconnect;
	standardfooter();
###	
	
	
	
} elsif ($report eq "LOOKUPDATA_DEPT_CLASS") {
	print $q->h2("Current Departments & Classes in the lookupdata.pm");

	use TTV::lookupdata;
	my @header=("Department","Class");
	my @array_to_print=\@header;
	foreach my $key (sort(keys(%class_hash))) {
		my @array;
		my $tmp=substr($key,2);
		if (substr($key,2) eq "00") {
			# This is a department
			@array=("$class_hash{$key}","");
		} else {
			# This is a class
			@array=("","$class_hash{$key}");
		}
		push(@array_to_print,\@array);
	}
	if (recordCount(\@array_to_print)) {
		printResult_arrayref2(\@array_to_print);
	} else {
		print $q->p("Failed to compile data");
	}


	standardfooter();
} elsif ($report eq "COMPARE_DEPT_CLASS") {
	### THIS IS NOT FINISHED!!! ###
	# Part I - Get the info from ****
	my %dept_hash;
	my %class_hash;
	my %parent_hash;
	my %wire_dept_hash;
	my %wire_class_hash;
	my %wire_parent_hash;
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

    $query = "
	select
		d.Dept,
		c.Class,
		c.[Description],
		c.[POS Description]
	from
		[Villages\$Dept_Class] d
		join [Villages\$Class] c on d.[Dept] = c.[Dept] and d.[List Type] = c.[List Type]
	where
		d.[List Type] = 0
		and c.Class <> '00'
	union
	select
		d.[Dept],
		'',
		d.[Description],
		d.[POS Description]
	from
		[Villages\$Dept_Class] d
	where
		d.[List Type] = 0

	order by 1,2
    ";

    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $dept_id = trim($$thisrow[0]);
			my $class_id = trim($$thisrow[1]);
			my $desc = trim($$thisrow[2]);
			if ($class_id) {
				# This is a class
				##$class_hash{$class_id}=$desc;
				$class_hash{$desc}=$class_id;
				# The parent is the dept
				$parent_hash{$desc}=$dept_id;
				##$parent_hash{$dept_id}=$class_id;

			} elsif ($dept_id) {
				# If there is no class number but there is a department number,
				# this is a department designation
				$dept_hash{$desc}=$dept_id;
				##$dept_hash{$desc}=$dept_id;
			}
		}
       # printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no data found in Navision<br>\n");
    }
    $dbh->disconnect;
	#
	# Part II Get the data from The Wire
	#
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;


    $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }
	# Get the departments
    my $query = "
	SELECT
		distinct(t.tid),
		t.name

	FROM
		term_data t
		INNER JOIN  term_hierarchy h ON t.tid = h.tid
		JOIN term_node tn ON t.tid = tn.tid

	WHERE
		t.vid = 22
	and
		h.parent = 0
	and
		tn.nid in (select nid from content_type_item where field_item_type_value = 'product')
	ORDER BY
		weight, name
    ";

   $tbl_ref = execQuery_arrayref($dbh, $query);
	my %tmp_hash;
    if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $tid = trim($$thisrow[0]);
			my $desc = trim($$thisrow[1]);
			$wire_dept_hash{$desc} = $tid;
			##$wire_dept_hash{$desc} = $tid;
		}
	}
	# Get the classes
   $query = "
	SELECT
		distinct(t.tid),
		t.name,
		h.parent
	FROM
		term_data t
		INNER JOIN  term_hierarchy h ON t.tid = h.tid
		JOIN term_node tn ON t.tid = tn.tid

	WHERE
		t.vid = 22
	and
		h.parent <> 0
	and
		tn.nid in (select nid from content_type_item where field_item_type_value = 'product')
	ORDER BY
		weight, name
    ";


   $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $tid = trim($$thisrow[0]);
			my $desc = trim($$thisrow[1]);
			my $parent = trim($$thisrow[2]);
			$wire_class_hash{$desc} = $tid;
			##$wire_class_hash{$desc} = $tid;
		 	$wire_parent_hash{$desc}=$parent;
			##$wire_parent_hash{$parent}=$tid;
		}
	}
	#
	# Now compare the departments from Navision with The Wire
	#
	foreach my $key (sort(keys(%dept_hash))) {
		print "  dept ($key) value ($dept_hash{$key})<br />";

	}
	foreach my $key (sort(keys(%class_hash))) {

		next if ($parent_hash{$key} == 77);
		print "---------------------------<br />";
		print "  class ($key) value ($class_hash{$key})<br />";
		my $class_id=$class_hash{$key};
		print "  parent ($parent_hash{$key}) ($dept_hash{$parent_hash{$key}})   ";
		foreach my $p (sort(keys(%parent_hash))) {
			if ($key == $p) {
				print "Parent ($parent_hash{$p})<br />";
			}
		}
		#print "  parent ($parent_hash{$class_hash{$key}}) ($dept_hash{$parent_hash{$class_hash{$key}}})<br />";
	}
	#foreach my $key (sort(keys(%parent_hash))) {
	#	print "  parent key ($key) value ($parent_hash{$key})<br />";
	#}
	print "=====================================<br />";
	foreach my $key (sort(keys(%wire_dept_hash))) {
		print "  wire dept ($key) value ($wire_dept_hash{$key})<br />";
	}
	foreach my $key (sort(keys(%wire_class_hash))) {

		#next if ($parent_hash{$key} == 77);
		print "---------------------------<br />";
		print "  class ($key) value ($wire_class_hash{$key})<br />";
		print "  parent ($wire_parent_hash{$key}) ($wire_dept_hash{$wire_parent_hash{$key}})<br />";
	}
    standardfooter();

} elsif ($report eq "RETURNSQRY"){
    print $q->h2("Returns Report - BETA");

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"RETURNSRPT"});

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_week"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"enddate_selection", -size=>"10", -value=>"$label_date"}))
    );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
			  );
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "RETURNSRPT"){
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
    my $store_selection   = trim(($q->param("store_selection"))[0]);
    print $q->h2("Returns Report:  From $startdate_selection to $enddate_selection");

    my $startdate_where;
    my $enddate_where;
    my $storeid_where;
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
        #$enddate_where="and tm.[TranDateTime] < '$startdate_selection 23:59:59'";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
    if ($store_selection) {
        $storeid_where="and tm.[StoreId] = '$store_selection'";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    $query = "
    SELECT
        tm.[StoreId]
        ,tm.[CashierId]
		,ud.UDTranId
		,ud.UDTranType
		,ud.ReasonCode
        FROM
            [****TLog].[dbo].[tblTranMaster] tm
			join [****TLog].[dbo].[tblUserDefined] ud on tm.[TranId] = ud.[TranId]
        where
            tm.[isPostVoided] = 0
        and
            tm.[TranVoid] = 0
        and
            tm.[isReturn] =1

        $startdate_where
        $enddate_where
        order by tm.[StoreId]
    ";

    my %store_hash;

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        my $s=$tmp[0];  # Store id
		my $tid=$tmp[2];
		my $ttype=$tmp[3];
		my $rc=$tmp[4];

        my $c="$s"."-"."$tmp[1]";  # Cashier id - Combination of store id and cashier number
        my %cashier_hash;
		my %return_typeID_hash;
		my %return_type_hash;
		my %reason_hash;
        if (defined($store_hash{$s})) {
            my $ref=$store_hash{$s};
			my @array=@$ref;
			$ref=$array[0];
            %cashier_hash=%$ref;
			$ref=$array[1];
			%return_typeID_hash=%$ref;
			$ref=$array[2];
			%return_type_hash=%$ref;
			$ref=$array[3];
			%reason_hash=%$ref;
        }
        $cashier_hash{$c}++;  # Increment the returns for this cashier
		$return_typeID_hash{$tid}++;
		$return_type_hash{$ttype}++;
		$reason_hash{$rc}++;
		my @array_final;
        my $ref=\%cashier_hash;
		push(@array_final,$ref);
		$ref=\%return_typeID_hash;
		push(@array_final,$ref);
		$ref=\%return_type_hash;
		push(@array_final,$ref);
		$ref=\%reason_hash;
		push(@array_final,$ref);

		$ref=\@array_final;
        $store_hash{$s}=$ref;
        my $count=keys(%cashier_hash);
    }
    # Now prepare the presentation:  Storeid - Cashier Count - Return Count - Average Return Per Cashier - Max Return Per Cashier
    my @header = ("StoreId","Cashier Count","Avg. Return Per Cashier","Max Return Per Cashier","Return Type","Reason Code");
    my @array_to_print=\@header;
    foreach my $storeid (sort(keys(%store_hash))) {
        my @array=("$storeid");
        my $ref=$store_hash{$storeid};
		my @tmp=@$ref;
		# Process the cashier_hash
		$ref=$tmp[0];
        my %cashier_hash=%$ref;
        my $max_count=0;
        my $return_count=0;
        my $cashier_count=keys(%cashier_hash);
        push (@array,$cashier_count);
        # Now, iterate through the cashier hash and find the average return count per cashier and the maximum number for any one cashier
        foreach my $cashier (sort(keys(%cashier_hash))) {
            my $count=$cashier_hash{$cashier};
            if ($count > $max_count) {
                $max_count=$count;
            }
            $return_count+=$count;
        }
        my $avg_count=sprintf("%.1f",($cashier_count / $return_count));
        push(@array,$avg_count);
        push(@array,$max_count);

		# Process the return_typeID_hash
		$ref=$tmp[1];
        my %return_typeID_hash=%$ref;
		my $string;
		foreach my $id(sort(keys(%return_typeID_hash))) {
			my $count=$return_typeID_hash{$id};
			$string.="ID: $id Count: $count\n";
		}
		push(@array,$string);

		# Process the reason_hash
		$ref=$tmp[2];
        my %reason_hash=%$ref;

		foreach my $id(sort(keys(%reason_hash))) {
			my $count=$reason_hash{$id};
			$string.="Reason: $id Count: $count\n";
		}
		push(@array,$string);

		# Put this line on the display
		push(@array_to_print,\@array);
    }
    if (recordCount(\@array_to_print)) {
        my @links = ( "$scriptname?report=RETURNSRPT2
        &startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
        printResult_arrayref2(\@array_to_print, \@links);
    } else {
        print $q->b("no return records found<br>\n");
    }
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "RETURNSRPT2"){
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
    my $store_selection   = trim(($q->param("store_selection"))[0]);
    print $q->h2("Returns Report:  $store_selection - From $startdate_selection to $enddate_selection");

    my $startdate_where;
    my $enddate_where;
    my $storeid_where;
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
        #$enddate_where="and tm.[TranDateTime] < '$startdate_selection 23:59:59'";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
    if ($store_selection) {
        $storeid_where="and tm.[StoreId] = '$store_selection'";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    $query = "
    SELECT

        tm.[CashierId],
        tm.[TranNum],
        ms.[SKU],
        ms.[Quantity],
        ms.[ExtSellPrice],
        tm.[TranDateTime],
		ud.UDTranId as 'Return Type',
		ud.ReasonCode as 'Reason',
		pr.Response1 as 'Purchased From'
        FROM
            [****TLog].[dbo].[tblTranMaster] tm
            join [****TLog].[dbo].[tblMerchandiseSale] ms on tm.TranId = ms.TranId
			join [****TLog].[dbo].[tblUserDefined] ud on tm.[TranId] = ud.[TranId]
			join [****TLog].[dbo].[tblProfileResponse] pr on tm.[TranId] = pr.[TranId]
        where
            tm.[isPostVoided] = 0
        and
            tm.[TranVoid] = 0
        and
            tm.[isReturn] = 1
        and
            (ms.[ExtSellPrice] = 0 or ms.[ExtSellPrice] < 0)
        $startdate_where
        $enddate_where
        $storeid_where
        order by tm.[CashierId]
    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
		# Translate the TranID, TranType, and Reason

		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $tid = trim($$thisrow[6]);
			#my $ttype = trim($$thisrow[7]);
			my $reason = trim($$thisrow[7]);
			if ($tid == 12) {
				$$thisrow[6] = "$tid Return W/Out Receipt";
			}
			if ($tid == 13) {
				$$thisrow[6] = "$tid Return W. Receipt";
			}
			if ($tid == 14) {
				$$thisrow[6] = "$tid Return W. Gift Receipt";
			}
			if ($reason == 1) {
				$$thisrow[7] = "$reason - Changed Mind";
			}
			if ($reason == 2) {
				$$thisrow[7] = "$reason - Defective";
			}
			if ($reason == 3) {
				$$thisrow[7] = "$reason - Found Cheaper";
			}
			if ($reason == 4) {
				$$thisrow[7] = "$reason - Other";
			}
		}
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "DISCOUNTCOUPON"){
 
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
 
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
 
    my $store_selection   = trim(($q->param("store_selection"))[0]);
	my $coupon_selection   = trim(($q->param("coupon_selection"))[0]);
	my $any_coupon_selection   = trim(($q->param("any_coupon_selection"))[0]);
	unless (($coupon_selection) || ($any_coupon_selection)) {
		print $q->h3("Error: No coupon specified");
		localfooter("STORERPTS");
		standardfooter();
		exit;
	}
	my $coupon_where = "and ds.ReferenceNum = '$coupon_selection'";
	if ($any_coupon_selection) {
		$coupon_where = "and ds.ReferenceNum > 0";
	}	
    print $q->h2("Discount Coupon Report: Coupon: $coupon_selection From $startdate to $enddate");

    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');	
	my $storeid;
    my $storewhere;
	my @store_list;
	if ($store_selection ne "All") {
		if ($store_selection eq "Company") {		 
			$storewhere="StoreCode  like 'V1' ";
		} elsif ($store_selection eq "Contract") {
		 
			$storewhere="StoreCode  like 'V2' ";
		} else {			
			my @tmp=split(/\s+/,$store_selection);
			$storeid=$tmp[0];
			push(@store_list,$storeid);			
		}
	} else {
		$storewhere="StoreCode  like 'V1' or StoreCode  like 'V2'";

		my $all_store_query = dequote(<< "		ENDQUERY");
			select
				storeid,
				storename
			from
				rtblStores
			where
				($storewhere)
			and
				EndDate is null
			and
				StartDate < GETDATE()
			order by
				StoreId
		ENDQUERY


		my $tbl_ref2 = execQuery_arrayref($dbh, $all_store_query);
		my $counter=0;
		if (recordCount($tbl_ref2)) {
			for my $datarows (1 .. $#$tbl_ref2) {
				$counter++;
				my $thisrow = @$tbl_ref2[$datarows];
				my $storeid = $$thisrow[0];
				my $storename = $$thisrow[1];
				push(@store_list,$storeid);	 
			}
		}	
	}			
	$dbh->disconnect;
    $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
	$dbh = openODBCDriver($dsn, 'rpt', 'rpt'); 	
	my @header=("Store","Count");
	if ($any_coupon_selection) {
		@header=("Store","Coupon","Count");
	}	
	my @array_to_print = \@header;
		 	
	foreach $storeid (@store_list) {		
 		
		if ($any_coupon_selection) {
			my %coupon_hash;
			$query=dequote(<< "			ENDQUERY");
				select 
					ds.ReferenceNum
				from
					****Tlog..tblTranMaster tm
					join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId				 	 					
				where
					tm.TranDateTime >= '$startdate'
					and tm.TranDateTime < dateadd( d, 1, '$enddate')
					and tm.TranVoid = 0 and tm.IsPostVoided = 0
					and tm.TranModifier not in (2,6)
					and tm.StoreId in ('$storeid')	
					$coupon_where
				order by
					tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ds.Sequence

			ENDQUERY
						
			#print $q->pre("$query");
			my $sth=$dbh->prepare("$query");
			$sth->execute();

			while (my @tmp = $sth->fetchrow_array()) {
				my $coupon=$tmp[0];			 
				$coupon_hash{$coupon}++; 
			}		
			foreach my $coupon (sort keys(%coupon_hash)) {
				my $count=$coupon_hash{$coupon};			 
				my @array=("$storeid","$coupon","$count");
				push(@array_to_print,\@array);				
			}
		} else {
			$query=dequote(<< "			ENDQUERY");
				select 
					count(ds.ReferenceNum)
				from
					****Tlog..tblTranMaster tm
					join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
				where
					tm.TranDateTime >= '$startdate'
					and tm.TranDateTime < dateadd( d, 1, '$enddate')
					and tm.TranVoid = 0 and tm.IsPostVoided = 0
					and tm.TranModifier not in (2,6)
					and tm.StoreId in ('$storeid')	
					and ds.ReferenceNum = '$coupon_selection'
				 
			ENDQUERY
			
			
			#print $q->pre("$query");
			my $sth=$dbh->prepare("$query");
			$sth->execute();

			while (my @tmp = $sth->fetchrow_array()) {
				my $count=$tmp[0];
				my @array=("$storeid","$count");
				push(@array_to_print,\@array);
			}
		}
 		
	}

    if (recordCount(\@array_to_print)) { 
        printResult_arrayref2(\@array_to_print);
    } else {
        print $q->b("no records found<br>\n");
    }

 
	$dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();	
} elsif ($report eq "LSINVQRY"){
    print $q->h2("LS Retail Store Inventory Report - BETA");
 
	# Build a list of Stores
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $mssql_dbh = openODBCDriver($dsn, '****', '****');
    my $Active=1;
    my %store_hash;

    foreach my $status ("Company","Contract") {
        $query = 'exec ****Data..tspGetStoreInfo @Code=\'V1\''. ($Active ? ', @Active=1' : '') if ($status eq "Company");
        $query = 'exec ****Data..tspGetStoreInfo @Code=\'V2\''. ($Active ? ', @Active=1' : '') if ($status eq "Contract");
        my $tbl_ref = execQuery_arrayref($mssql_dbh, $query);
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $storeId = trim($$thisrow[0]);
            my $storeName = trim($$thisrow[1]);
            my @tmp=split(/-/,$storeName);
            $storeName=$tmp[1];
            $store_hash{$storeId}=$storeName;
        }
	}
	my $store_options=$q->option({-value=>"All"}, "All Stores");
	$store_options.=$q->option({-value=>"V1"}, "Company Stores");
	$store_options.=$q->option({-value=>"V2"}, "Contract Stores");	 
 
    foreach my $store (sort(keys(%store_hash))) {
        $store_options .= $q->option({-value=>"$store - $store_hash{$store}"}, "$store - $store_hash{$store}");
    }	
	
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"LSINVRPT"});
    # Store Number
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
 
                #$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                #$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_week"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"enddate_selection", -size=>"10", -value=>"$label_date"}))
    );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
			  );
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
	
    localfooter("STORERPTS");
    standardfooter();
	
	
} elsif ($report eq "LSINVRPT"){
 
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
    #my $store_selection   = trim(($q->param("store_selection"))[0]);
    print $q->h2("LS Retail Store Inventory Report:  From $startdate_selection to $enddate_selection");	

    my $startdate_where;
    my $enddate_where;
    my $storeid_where;
	my $startdate;
	my $enddate;
	my $storeid;
	my $where;
			
		#group by si.storeId
		
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
        #$enddate_where="and tm.[TranDateTime] < '$startdate_selection 23:59:59'";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
 
	# Get the number of registers from the POS Support database
 
    my $store_selection   = trim(($q->param("store_selection"))[0]);
 
	my $storeWhere='';
	my $storeWhere2='';
	my $dateWhere2="and [TranDateTime] > '$startdate_selection' and [TranDateTime] < '$enddate_selection 23:59:59'" ;
	my $codeDesc;
	my $label = '';
 
	# Determine the store selection
 
	if ($store_selection) {
		if ($store_selection eq "All") {
			print $q->h2 ("Data for All stores");
			$storeWhere2 = "WHERE StoreId in (select StoreId from [rtblStores] where EndDate is NULL)";	
		} elsif ($store_selection eq "V1") {		
			print $q->h2 ("Data for $store_selection");
			my @tmp=split(/\s+/,$store_selection);
			my $storeid = $tmp[0];
			$storeWhere = "and s.status = 'company'";
			$storeWhere2 = "WHERE StoreId in (select StoreId from [rtblStores] where StoreCode like 'V1' and EndDate is NULL)";	
		} elsif ($store_selection eq "V2") {		
			print $q->h2 ("Data for $store_selection");
			my @tmp=split(/\s+/,$store_selection);
			my $storeid = $tmp[0];
			$storeWhere = "and s.status = 'contract'";
			$storeWhere2 = "WHERE StoreId in (select StoreId from [rtblStores] where StoreCode like 'V2' and EndDate is NULL)";	
		} else {
			print $q->h2 ("Data for $store_selection");
			my @tmp=split(/\s+/,$store_selection);
			my $storeid = $tmp[0];
			$storeWhere = "and si.storeid = $storeid";		
			$storeWhere2 = "WHERE StoreId= $storeid";	
		}		 
	}
 
    my $dsn;
    my $dbh;
    my $query;
 
    my $tbl_ref;
    my $rpt_label;
	my %store_info_hash=();
 
	$dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

	unless ($dbh) {
		print $q->p("Error - Cannot connect to $mysqld ");
		standardfooter();
		exit;
	}
	my $codeDesc = "reg%Model%";

	
	####################
	# Create the temp table that will be used for the final report
	####################
	$query = "
	select
		si.storeid as StoreId,
		s.storename as StoreName,
		s.status as Status,
		SUBSTRING(c.codeDesc,1,4) as Reg
 
	from storeInfo si
		join stores s on si.storeid=s.storeid
		join codes c on si.codeid=c.codeid
	where
		c.codeDesc like '$codeDesc'
		and c.codeDesc not like '%regx%'
		$storeWhere
	order by 
		si.storeid
 
	";

	#print $q->pre($query);
 
	my $sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];
		my $StoreName=$tmp[1];
		my $Status=$tmp[2];
		my $RegNum=$tmp[3];
		my $key="$StoreId $RegNum";		
		$store_info_hash{$key} = ["$StoreName","$Status"];
	}
 
	$dbh->disconnect;
 
	# Connecting to ****Data
    $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
	
	my %store_day_hash=();
	my %store_reg_txn_hash=();
	my %store_reg_item_hash=();
	my %store_villages_SKU_hash=();	
	my %store_non_villages_SKU_hash=();		
 
	
	############################	
	# Get the number of Days
	############################	
	$query = "
		SELECT 
			[StoreId]
			,convert(Date, convert(varchar(10),TranDateTime,21)) as 'Date'			
		FROM 
			[****Data].[dbo].[tblStoreNetSales]
		$storeWhere2	
		$dateWhere2		
		GROUP BY
			StoreId, convert(Date, convert(varchar(10),TranDateTime,21)) 

	";	
 
	#print $q->pre($query);	
	
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	my $day_count=0;
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];				
		$store_day_hash{$StoreId}++;			
	}
 
	
	############################
	# Get the number of transactions 
	############################
	$query = "
		SELECT 
			[StoreId]
			,[RegNum]
			,count(distinct([TransNum]))as TranCount
 
		FROM 
			[****Data].[dbo].[tblStoreNetSales]
		$storeWhere2	
		$dateWhere2		
		GROUP BY 
			StoreId, RegNum

	";	
	#print $q->pre($query);	
	
	my $sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];
		my $RegNum="reg".$tmp[1];
		my $TranCount=$tmp[2];
		my $key="$StoreId $RegNum";
		# Collect the count of transaction per Store - Reg
		$store_reg_txn_hash{$key}+=$TranCount;				 
	}	
 
	############################
	# Get the Item info
	############################	
	$query = "
		SELECT 
			[StoreId]
			,[RegNum]
			,MAX([ItemNum]) as ItemCount
 
		FROM 
			[****Data].[dbo].[tblStoreNetSales]
		$storeWhere2	
		$dateWhere2		
		GROUP BY 
			StoreId, RegNum, TransNum

	";	
	#print $q->pre($query);	
 
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];
		my $RegNum="reg".$tmp[1];
		my $ItemCount=$tmp[2];
		my $key="$StoreId $RegNum";
		# Collect a count of Items per Store - Reg
		$store_reg_item_hash{$key}+=$ItemCount; 
	}	 	
=pod	
	############################
	# Get the SKU info
	############################	
	$query = "
		select 
			StoreId, 
			count(SKU)
		from 
			tblStoreSkuInfo
		where 
			lastupdate > '2015-12-20'
		and 
			StoreId > 1000
		group by StoreId


	";	
	#print $q->pre($query);	

	$sth=$dbh->prepare("$query");
	$sth->execute();
	
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];	 
		my $SKUCount=$tmp[1];		
		# Collect a count of SKUs per Store 
		$store_villages_SKU_hash{$StoreId}=$SKUCount; 
	}	
=cut	
	############################
	# Get the count of Villages SKU's
	############################	
	$query = "
		select 
			StoreId, 
			count(SKU)
		from 
			tblStoreSkuInfo
		where 
			StoreId > 1000
			and
			vendorid in ('01')
		group by StoreId
	";	
	#print $q->pre($query);	

	$sth=$dbh->prepare("$query");
	$sth->execute();
	
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];	 
		my $SKUCount=$tmp[1];		
		# Collect a count of SKUs per Store 
		$store_villages_SKU_hash{$StoreId}=$SKUCount; 
	}	
	############################
	# Get the count of Non Villages SKU's
	############################	
	$query = "
		select 
			StoreId, 
			count(SKU)
		from 
			tblStoreSkuInfo
		where 
			StoreId > 1000
			and
			vendorid not in ('01')
		group by StoreId
	";	
	#print $q->pre($query);	

	$sth=$dbh->prepare("$query");
	$sth->execute();
	
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];	 
		my $SKUCount=$tmp[1];		
		# Collect a count of SKUs per Store 
		$store_non_villages_SKU_hash{$StoreId}=$SKUCount; 
	}	
	
	############################	
	# Build the final report
	############################	
	my @header=("StoreId","StoreName","Status","Reg","TranCntAvg","ItemCntAvg","V_SKUCnt","NV_SKUCnt");
	my @array_to_print=\@header;
	
	foreach my $key (sort keys(%store_info_hash)) {
		my @tmp=split(/\s+/,$key);
		my $StoreId = $tmp[0];
		my $RegNum = $tmp[1];
		my $StoreName = $store_info_hash{$key} -> [0];
		my $Status = $store_info_hash{$key} -> [1];
		my $Days = $store_day_hash{$StoreId};
		my $TranCount = $store_reg_txn_hash{$key};
		my $ItemCount = $store_reg_item_hash{$key};
		my $SKUCount = '';
		my $nvSKUCount = '';
		if ($RegNum eq "reg1") {
			$SKUCount = $store_villages_SKU_hash{$StoreId};
			$nvSKUCount = $store_non_villages_SKU_hash{$StoreId};
			
		}

		# Do some calculations for the averages
		my $TranCntAvg = "NA";
		my $ItemCntAvg = "NA";
		if ($Days) {			
			$TranCntAvg = sprintf("%d",($TranCount / $Days));			
			if ($TranCntAvg) {
				$ItemCntAvg = sprintf("%d",($ItemCount / $TranCount));
			}
		
			# Build the array
			my @array=("$StoreId","$StoreName","$Status","$RegNum","$TranCntAvg","$ItemCntAvg","$SKUCount","$nvSKUCount");
			push(@array_to_print,\@array);			
		}
	}
	if (recordCount(\@array_to_print)) { 		
		printResult_arrayref2(\@array_to_print);  	 
	} else {
		print $q->b("no records found for LS Retail Report<br>");
	}	 	
	
	
	$dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();	
} elsif ($report eq "LOSSPREVENTIONQRY"){
    print $q->h2("Loss Prevention Report - BETA");

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"LOSSPREVENTIONRPT"});
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "(Optional) Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"store_selection", -size=>"12", -value=>""}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_week"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"enddate_selection", -size=>"10", -value=>"$label_date"}))
    );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
			  );
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
	
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "LOSSPREVENTIONRPT"){
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
    my $store_selection   = trim(($q->param("store_selection"))[0]);
    print $q->h2("Loss Prevention Report:  From $startdate_selection to $enddate_selection");

    my $startdate_where;
    my $enddate_where;
    my $storeid_where;
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
        #$enddate_where="and tm.[TranDateTime] < '$startdate_selection 23:59:59'";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
    if ($store_selection) {
        $storeid_where="and tm.[StoreId] = '$store_selection'";
    }
	# Step 1 - Get the number of transactions
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    $query = "
    SELECT
        (convert(VARCHAR,tm.[StoreId]) + '-' + CONVERT(VARCHAR,tm.[CashierId])) as StoreCashier,
		tm.[isReturn],
		tm.[isDiscount],
		tm.[isPriceOver],
		tm.[isTotaled],

		tm.[isPayment],
		tm.[isPostVoided],
		tm.[isZeroPayment],
		tmt.[MiscType],
		tmt.[RefNum]
        FROM
            [****TLog].[dbo].[tblTranMaster] tm
			left join [****TLog].[dbo].[tblMiscTran] tmt on tm.[TranId] = tmt.[TranId] and tmt.[MiscType] = 3 and tmt.[RefNum] = 201
		where
            tm.[TranVoid] = 0
		and
			tm.[CashierId] > 0
        and
			tm.[RegisterId] not like 'p%'
        $startdate_where
        $enddate_where
		$storeid_where
        order by tm.[StoreId]
    ";

    my %store_hash;
    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        my $store_cashier=$tmp[0];  # Store id & Cashier Number
		my $return=$tmp[1];
		my $discount=$tmp[2];
		my $priceover=$tmp[3];
		my $totaled=$tmp[4];
		my $payment=$tmp[5];
		my $postvoid=$tmp[6];
		my $zeropayment=$tmp[7];
		my $misctype=$tmp[8];
		my $refnum=$tmp[9];
		my $nosale=0;
		if (($misctype == 3) && ($refnum == 201)) {
			# MiscType 3 is a manager code.  Manager code 201 is a No Sale
			$nosale=1;
		}
        my %txn_hash;
        if (defined($store_hash{$store_cashier})) {
            my $ref=$store_hash{$store_cashier};
            %txn_hash=%$ref;
        }
        $txn_hash{'txn'}++;  # Increment the number of returns for this cashier

        $txn_hash{'return'}+=$return;  # Increment the number of returns for this cashier
		$txn_hash{'discount'}+=$discount;  # Increment the number of discount for this cashier
		$txn_hash{'priceover'}+=$priceover;  # Increment the number of priceover for this cashier
		$txn_hash{'totaled'}+=$totaled;  # Increment the number of totaled for this cashier
		$txn_hash{'payment'}+=$payment;  # Increment the number of payment for this cashier
		$txn_hash{'postvoid'}+=$postvoid;  # Increment the number of postvoid for this cashier
		$txn_hash{'zeropayment'}+=$zeropayment;  # Increment the number of zeropayment for this cashier
		$txn_hash{'nosale'}+=$nosale;  # Increment the number of nosale for this cashier
        my $ref=\%txn_hash;
        $store_hash{$store_cashier}=$ref;
    }
    # Now prepare the presentation:  Storeid - Cashier Count - Return Count - Average Return Per Cashier - Max Return Per Cashier
    my @header = ("StoreId-Cashier","Transactions","Returns","Discounts","Price Override","Totaled","Payment","No Sale","Post Void","Zero Payment");
    my @array_to_print=\@header;

	my @above_avg_returns;
	my @above_avg_discounts;
	my @above_avg_priceovers;
	my @above_avg_postvoids;
	my @above_avg_nosales;
	my %counts_hash = (
		'txn' => '0',
		'return' => '0',
		'discount' => '0',
		'priceover' => '0',
		'postvoid' => '0',
		'nosale' => '0',
	);
	my %averages_hash = (
		'txn' => '0',
		'return' => '0',
		'discount' => '0',
		'priceover' => '0',
		'postvoid' => '0',
		'nosale' => '0',
	);
    foreach my $store_cashier (sort(keys(%store_hash))) {
		my @tmp=split(/-/,$store_cashier);
		my $storeid=$tmp[0];
		my $cashier=$tmp[1];
        my $ref=$store_hash{$store_cashier};
        my %txn_hash=%$ref;
        # Now, iterate through the txn hash and find the stats for each cashier
		my $txn=$txn_hash{'txn'};
		my $return=$txn_hash{'return'};
		my $discount=$txn_hash{'discount'};
		my $priceover=$txn_hash{'priceover'};
		my $totaled=$txn_hash{'totaled'};
		my $payment=$txn_hash{'payment'};
		my $postvoid=$txn_hash{'postvoid'};
		my $zeropayment=$txn_hash{'zeropayment'};
		my $nosale=$txn_hash{'nosale'};
		$counts_hash{'txn'}+=$txn;
		$counts_hash{'return'}+=$return;
		$counts_hash{'discount'}+=$discount;
		$counts_hash{'priceover'}+=$priceover;
		$counts_hash{'nosale'}+=$nosale;

		# Calculate the average returns, discounts, priceover, postvoid, and nosale per transaction
		$averages_hash{'return'}=sprintf("%.2f",($counts_hash{'return'} / $counts_hash{'txn'}));
		$averages_hash{'discount'}=sprintf("%.2f",($counts_hash{'discount'} / $counts_hash{'txn'}));
		$averages_hash{'priceover'}=sprintf("%.2f",($counts_hash{'priceover'} / $counts_hash{'txn'}));
		$averages_hash{'nosale'}=sprintf("%.2f",($counts_hash{'nosale'} / $counts_hash{'txn'}));
    }
    foreach my $store_cashier (sort(keys(%store_hash))) {
		my @tmp=split(/-/,$store_cashier);
		my $storeid=$tmp[0];
		my $cashier=$tmp[1];
        my $ref=$store_hash{$store_cashier};
        my %txn_hash=%$ref;
        # Now, iterate through the txn hash and find the stats for each cashier
		my $txn=$txn_hash{'txn'};
		my $return=$txn_hash{'return'};
		my $discount=$txn_hash{'discount'};
		my $priceover=$txn_hash{'priceover'};
		my $totaled=$txn_hash{'totaled'};
		my $payment=$txn_hash{'payment'};
		my $postvoid=$txn_hash{'postvoid'};
		my $zeropayment=$txn_hash{'zeropayment'};
		my $nosale=$txn_hash{'nosale'};
		my @array=($store_cashier,$txn,$return,$discount,$priceover,$totaled,$payment,$nosale,$postvoid,$zeropayment);
		push(@array_to_print,\@array);
		# Calculate the average returns, , , , and  per transaction
		my $average=sprintf("%.2f",($return / $txn));
		if ($average > ($averages_hash{'return'})) {
			# There are above average returns here
			unless (recordCount(\@above_avg_returns)) {
				# Unless we already have data in this array, put the header on it.
				my @small_header = ("StoreId-Cashier","Transactions","Returns","Rate","Average Rate");
				@above_avg_returns = \@small_header;
			}
			my @small_array=($store_cashier,$txn,$return,$average,$averages_hash{'return'});
			push(@above_avg_returns,\@small_array);
		}
		# Calculate the average discounts,
		$average=sprintf("%.2f",($discount / $txn));
		if ($average > ($averages_hash{'discount'})) {
			# There are above average discounts here
			unless (recordCount(\@above_avg_discounts)) {
				# Unless we already have data in this array, put the header on it.
				my @small_header = ("StoreId-Cashier","Transactions","Discounts","Rate","Average Rate");
				@above_avg_discounts = \@small_header;
			}
			my @small_array=($store_cashier,$txn,$discount,$average,$averages_hash{'discount'});
			push(@above_avg_discounts,\@small_array);
		}
		# Calculate the average priceover,
		$average=sprintf("%.2f",($priceover / $txn));
		if ($average > ($averages_hash{'priceover'})) {
			# There are above average discounts here
			unless (recordCount(\@above_avg_priceovers)) {
				my @small_header = ("StoreId-Cashier","Transactions","PriceOvers","Rate","Average Rate");
				@above_avg_priceovers = \@small_header;
			}
			my @small_array=($store_cashier,$txn,$priceover,$average,$averages_hash{'priceover'});
			push(@above_avg_priceovers,\@small_array);
		}
		# Calculate the average postvoid,
		$average=sprintf("%.2f",($postvoid / $txn));
		if ($average > ($averages_hash{'postvoid'})) {
			# There are above average postvoid here
			unless (recordCount(\@above_avg_postvoids)) {
				my @small_header = ("StoreId-Cashier","Transactions","PostVoid","Rate","Average Rate");
				@above_avg_postvoids = \@small_header;
			}
			my @small_array=($store_cashier,$txn,$postvoid,$average,$averages_hash{'postvoid'});
			push(@above_avg_postvoids,\@small_array);
		}
		# Calculate the average nosale,
		$average=sprintf("%.2f",($nosale / $txn));
		if ($average > ($averages_hash{'nosale'})) {
			# There are above average nosale here
			unless (recordCount(\@above_avg_nosales)) {
				my @small_header = ("StoreId-Cashier","Transactions","NoSale","Rate","Average Rate");
				@above_avg_nosales = \@small_header;
			}
			my @small_array=($store_cashier,$txn,$nosale,$average,$averages_hash{'nosale'});
			push(@above_avg_nosales,\@small_array);
		}

    }
    if (recordCount(\@array_to_print)) {
        my @links = ( "$scriptname?report=LOSSPREVENTIONRPT2
        &startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
        printResult_arrayref2(\@array_to_print, \@links);

		if (recordCount(\@above_avg_returns)) {
			print $q->p("These are above average returns");
			my @links = ( "$scriptname?report=LOSSPREVENTIONRPT2
			&startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
			printResult_arrayref2(\@above_avg_returns, \@links);
		}
		if (recordCount(\@above_avg_discounts)) {
			print $q->p("These are above average discounts");
			my @links = ( "$scriptname?report=LOSSPREVENTIONRPT2
			&startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
			printResult_arrayref2(\@above_avg_discounts, \@links);
		}
		if (recordCount(\@above_avg_priceovers)) {
			print $q->p("These are above average priceovers");
			my @links = ( "$scriptname?report=LOSSPREVENTIONRPT2
			&startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
			printResult_arrayref2(\@above_avg_priceovers, \@links);
		}
		if (recordCount(\@above_avg_postvoids)) {
			print $q->p("These are above average postvoids");
			my @links = ( "$scriptname?report=LOSSPREVENTIONRPT2
			&startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
			printResult_arrayref2(\@above_avg_postvoids, \@links);
		}
		if (recordCount(\@above_avg_nosales)) {
			print $q->p("These are above average nosales");
			my @links = ( "$scriptname?report=LOSSPREVENTIONRPT2
			&startdate_selection=$startdate_selection&enddate_selection=$enddate_selection&store_selection=".'$_' );
			printResult_arrayref2(\@above_avg_nosales, \@links);
		}
    } else {
        print $q->b("no return records found<br>\n");
    }
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "LOSSPREVENTIONRPT2"){
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
    my $store_selection   = trim(($q->param("store_selection"))[0]);
	my @tmp=split(/-/,$store_selection);
	my $storeid=$tmp[0];
	my $cashier=$tmp[1];
    print $q->h2("Loss Prevention Report: Store: $storeid  Cashier: $cashier - From $startdate_selection to $enddate_selection");

    my $startdate_where;
    my $enddate_where;
    my $storeid_where;
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
    if ($store_selection) {
        $storeid_where="and tm.[StoreId] = '$storeid' and tm.[CashierId] = '$cashier' ";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    $query = "
    SELECT
		tm.TranId,
        tm.[CashierId],
		tm.[isReturn],
		tm.[isDiscount],
		tm.[isPriceOver],
		tm.[isTotaled],
		tm.[isPayment],
		case when tmt.[MiscType] = 3 and tmt.[RefNum] = 201 then 1 end as 'No Sale',
		tm.[isPostVoided],
		tm.[isZeroPayment],
        tm.[TranNum],
        ms.[SKU],
        ms.[Quantity],
        ms.[ExtSellPrice],
        tm.[TranDateTime]

        FROM
            [****TLog].[dbo].[tblTranMaster] tm
            left join [****TLog].[dbo].[tblMerchandiseSale] ms on tm.TranId = ms.TranId
			left join [****TLog].[dbo].[tblMiscTran] tmt on tm.[TranId] = tmt.[TranId] and tmt.[MiscType] = 3 and tmt.[RefNum] = 201
        where

            tm.[TranVoid] = 0
 		and
			tm.[CashierId] > 0
		and
			tm.[RegisterId] not like 'p%'
        $startdate_where
        $enddate_where
        $storeid_where
        order by tm.[CashierId]
    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {

		my @links = ("$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        printResult_arrayref2($tbl_ref, \@links);

    } else {
        print $q->b("no records found<br>\n");
    }
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "PMTDISCTOOL"){
    print $q->h2("Paymentech Discrepancy Tool");
    # Get a list of the Paymentech discrepancies
    my @known_issues=();
    my @current_issues=();
    my $user="ptechtools";
    my $pw="showmethetools";
    #my $mysqld="192.168.21.105";
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ISSUES: failed to connect to $mysqld<br>";
        localfooter("STORERPTS");
        standardfooter();
        exit;
    }
    # Find known issues
    my $query="select * from pmtAuthorization  order by storeId";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->h3("Paymentech Transactions Not Matched in Triversity");
        my @links = ( "$scriptname?report=PMTMATCHTOOL&disc_selection=".'$_' );
        printResult_arrayref2($tbl_ref, \@links);
    } else {
        print $q->b("no unmatched Paymentech records found<br>\n");
    }
    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "TRIDISCTOOL"){
    print $q->h2("Triversity Discrepancy Tool");
    # Get a list of the triversity discrepancies
    my @known_issues=();
    my @current_issues=();
    my $user="ptechtools";
    my $pw="showmethetools";
    #my $mysqld="192.168.21.105";
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ISSUES: failed to connect to $mysqld<br>";
        localfooter("STORERPTS");
        standardfooter();
        exit;
    }
    # Find known issues
    my $query="select * from triAuthorization  order by storeId";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->h3("Triversity Transactions Not Matched in Paymentech");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no unmatched Triversity records found<br>\n");
    }
    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "PMTMATCHTOOL"){
    print $q->h2("Paymentech Discrepancy Resolution Tool");
    my $disc_selection= trim(($q->param("disc_selection"))[0]);
    my $user="ptechtools";
    my $pw="showmethetools";
    my $storeid;
    #my $mysqld="192.168.21.105";
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ISSUES: failed to connect to $mysqld<br>";
        standardfooter();
        exit;
    }
    # Find known issues
    my $query="select * from pmtAuthorization  where pmtId = '$disc_selection'";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        # get the store number from this
        my @array=@$tbl_ref;
        my $header_ref=$array[0];
        my @header=@$header_ref;
        my $data_ref=$array[1];
        my @data=@$data_ref;

        for (my $x=0; $x<=$#header; $x++) {
            if ($header[$x] =~ /storeid/i) {
                $storeid=$data[$x];
            }
        }

        print $q->h3("Paymentech Transactions Not Matched in Triversity - Store: $storeid");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no unmatched Paymentech records found<br>\n");
    }
    $query="select * from triAuthorization where storeId = '$storeid'";
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->p();
        print $q->h3("Unmatched Triversity Transactions for store $storeid");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->p();
        print $q->b("no unmatched Triversity records found for store $storeid<br>\n");
    }

    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();
} elsif (($report eq "PMTDISCQRY") || ($report eq "PMTMTCHQRY")){
    my $label="Discrepancy";
    if ($report eq "PMTMTCHQRY") {
        $label="Match";
    }
    print $q->h2("Paymentech $label Query Menu");
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PMTDISCRPT"});
    print $q->input({-type=>"hidden", -name=>"label", -value=>"$label"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections:")));

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Card ending with:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"number_selection", -size=>"4", -value=>""}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Limit to store number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"storeid_selection", -size=>"4", -value=>""}))
    );
=pod
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_week"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"enddate_selection", -size=>"10", -value=>"$label_date"}))
    );
=cut

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_day"}))
    );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
			  );
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    standardfooter();

} elsif ($report eq "PMTDISCRPT") {

    my $number_selection   = trim(($q->param("number_selection"))[0]);
    my $name_selection   = trim(($q->param("name_selection"))[0]);
    my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
    my $label   = trim(($q->param("label"))[0]);
    print $q->h2("Paymentech $label Report: BETA");
    if ($storeid_selection == 1340) {
        print $q->p("Error: Store 1340 (Richmond) has a different Paymentech account number and we cannot see their info.");
        standardfooter();
        exit;
    }
    my $startdate_where;
    my $enddate_where;
    if ($startdate_selection) {
        # When we query the database, we need to look for transactions before and after the specified date since it may have gotten to Paymentech
        # on one day but entered Triversity on another
        $startdate_where="and ptd.[TranDateTime] > '$startdate_selection'";
        $enddate_where="and ptd.[TranDateTime] < '$startdate_selection 23:59:59'";

        $startdate_where="and (ptd.[TranDateTime] > DateAdd(day,-7,'$startdate_selection')
                    or ptd.[TranDateTime] < DateAdd(day,+14,'$startdate_selection'))";
    }
    if ($enddate_selection) {
        $enddate_where="and ptd.[TranDateTime] < '$startdate_selection 23:59:59'";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my @paymentech_list;
    my @triversity_list;
    # Collect the following info from Paymentech table:
    # Authorization Code
    # Store Id
    # TranDate
    # Card Type
    # Card Holder Number
    # Exp Date
    # Amount
    my $number_where;
    my $storeid_where;
    if ($number_selection) {
        $number_where=" and ptd.[CardholderNo] like '%$number_selection'";
    }
    if ($storeid_selection) {
        $storeid_where=" and pmi.[StoreId] like '$storeid_selection%'";
    }
    my $query = "
        SELECT
            pmi.[StoreId]
            ,ptd.[TranDateTime]
            ,ptd.[CardType]
            ,ptd.[CardholderNo]
            ,ptd.[ExpDate]
            ,ptd.[AuthCode]
            ,ptd.[Amt]
            ,ptd.[TrType]
        FROM
            [****Data].[dbo].[tblPaymentechTransactionDetailInfo]  ptd
           join [****Data].[dbo].[tblPaymentechMerchantInfo] pmi on ptd.[ReportingMerchNo] = pmi.[MerchantNo] and ptd.[TermId] = pmi.[TermId]
        where
            ptd.[Amt] <> 0
        $number_where
        $storeid_where
        $startdate_where
        $enddate_where
    ";

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        my $charge=sprintf("%.2f",$tmp[6]);
        my $storeid=$tmp[0];
        if ($storeid==0) {
            $storeid="Akron";
        }
        my $cardtype = $tmp[2];
        if ($cardtype eq "Stored Value") {
            # This is a gift card
            $cardtype="GiftCard";
        }
        my $expdate=$tmp[4];
        # The format in this table does not match the triversity format so change it
        my @etmp=split(/\//,$expdate);
        my $prefix=substr($current_year,0,2);
        my $year="$prefix"."$etmp[1]";
        my $mo=$etmp[0];
        $expdate="$year"."$mo";

        my %hash=(
            'storeid' => "$storeid",
            'date' => "$tmp[1]",
            'cardtype' => "$cardtype",
            'cardno' => "$tmp[3]",
            'expdate' => "$expdate",
            'authcode' => "$tmp[5]",
            'charge' => "\$$charge",
        );

        push(@paymentech_list,\%hash);

    }
    # Collect from the Triversity side:
    # Store Id
    # TranDate
    # Card Number
    # Exp Date
    # Amount
    # TranNum
    # Register
    # Card Name
    # Tender
    # Authorization Number
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
        $enddate_where="and tm.[TranDateTime] < '$startdate_selection 23:59:59'";
        $startdate_where="and (tm.[TranDateTime] > DateAdd(day,-7,'$startdate_selection')
                    or tm.[TranDateTime] < DateAdd(day,+14,'$startdate_selection'))";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$startdate_selection 23:59:59'";
    }
    $query = "
    SELECT
        tm.[StoreId]
        ,tm.[TranDateTime]
        ,case
            when convert(varchar(2), ta.[TenderId]) = '4' then 'MasterCard'
            when ta.[TenderId] = 5 then 'Visa'
            when ta.[TenderId] = 6 then 'Amex'
            when ta.[TenderId] = 7 then 'Discover'
            when ta.[TenderId] = 14 then 'GiftCard'
            else convert(varchar(2), ta.[TenderId])
        end as Tender
        ,ta.[CardNumber]
        ,ta.[ExpireDate]
        ,ta.[AuthorizationNum]
        ,ta.[AuthorizedAmount]

        ,ta.[CardName]
        ,tm.[TranNum]
        ,tm.[RegisterId]
        ,tm.[CashierId]

        FROM [****TLog].[dbo].[tblAuthorization] ta
        join  [****TLog].[dbo].[tblTranMaster] tm on tm.[TranId] = ta.[TranId]
        where
            tm.[StoreId] <> 1340
        and
            ta.[AuthorizedAmount] <> 0
        and
            tm.[isPostVoided] = 0
        and
            tm.[TranVoid] = 0
        and
            ta.[CardNumber] like '%$number_selection'
        and
            ta.[TranId] like '$storeid_selection%'
        $startdate_where
        $enddate_where
    ";
    $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        my $charge=sprintf("%.2f",$tmp[6]);
        my %hash=(
            'storeid' => "$tmp[0]",
            'date' => "$tmp[1]",
            'cardtype' => "$tmp[2]",
            'cardno' => "$tmp[3]",
            'expdate' => "$tmp[4]",
            'authcode' => "$tmp[5]",
            'charge' => "\$$charge",

            'cardName' => "$tmp[7]",
            'tranNum' => "$tmp[8]",
            'registerId' => "$tmp[9]",
            'cashierId' => "$tmp[10]",
        );

        push(@triversity_list,\%hash);

    }

    # Matched transactions
    my %matched_pmt_transactions;
    my %matched_tri_transactions;
    # Not matchup the transactions
    my @not_matched_paymentech_list;
    my @not_matched_triversity_list;
    my @required_key_list_gc = (
                'storeid',
                'cardtype',
                'authcode',
                'charge'
            ); # These keys must match
    my @required_key_list=(@required_key_list_gc,"cardno","expdate");
    # First, are all of the transactions seen from Paymentech also seen in Triversity?
    my $counter=0;
    foreach my $p (@paymentech_list) {
        $counter++;
        my %paymentech_hash=%$p;
        # Look only at the paymentech records for the specified date
        my $pdate=$paymentech_hash{'date'};
        my @tmp=split(/\s+/,$pdate);
        $pdate=$tmp[0];
        my $date=$startdate_selection;
        @tmp=split(/\s+/,$date);
        $date=$tmp[0];
        next unless ("$pdate" eq "$date");
        my $matched=0;
        foreach my $t (@triversity_list) {
            # Search through the entire triversity transaction list to find a matching transaction
            my %triversity_hash=%$t;
            my @req_list=@required_key_list;
            my $gc_flag=0;
            if ($paymentech_hash{'cardtype'} eq "GiftCard") {
                # Set the appropriate required list
                @req_list=@required_key_list_gc;
                $gc_flag=1;
            }
            my $key_match=1;
            foreach my $r (@req_list) {
                my $paymentech_value=$paymentech_hash{$r};
                my $triversity_value=$triversity_hash{$r};
                if (($gc_flag) && ($r eq "charge")){
                    # If we are looking at a gift card and are comparing the amount, we need to do some adjustments
                    my $tmp_value=$paymentech_value;
                    $tmp_value=~ s/\$//g;
                    if ($tmp_value < 0) {
                        $paymentech_value="\$".sprintf("%.2f",abs($tmp_value));
                    }
                }
                unless ($paymentech_value eq $triversity_value) {
                    # We found a required value which does not match so this is not the same record
                    $key_match=0;
                }
            }
            if ($key_match) {
                $matched=1;
                $matched_pmt_transactions{$counter}=$p;
                $matched_tri_transactions{$counter}=$t;
            }
        }
        unless ($matched) {
            # Add this to the list of not matched paymentech transactions
            push(@not_matched_paymentech_list,$p);
        }
    }
    # If we are looking for matches, show them now

    if ($label =~ /Match/i) {
        print $q->p("Paymentech Transactions matched to Triversity");
        my $counter=0;
        foreach my $match (keys(%matched_pmt_transactions)) {
            $counter++;
            my $ref=$matched_pmt_transactions{$match};
            my %phash=%$ref;
            $ref=$matched_tri_transactions{$match};
            my %thash=%$ref;
            my $sep;
            print "#".$counter." Paymentech: ";
            foreach my $key (sort(keys(%phash))) {
                print "$sep $key: $phash{$key}";
                $sep=" -";
            }
            print "<br>";
            $sep="";
            print "Triversity: ";
            foreach my $key (sort(keys(%thash))) {
                print "$sep $key: $thash{$key}";
                $sep=" -";
            }
            print "<br>";
            print "------------------------------------------<br>";
        }
        # Now clear these hashes so that we can reuse them
        undef %matched_pmt_transactions;
        undef %matched_tri_transactions;
    }
    # Now, check to see if all triversity transactions are at paymentech
    $counter=0;
    foreach my $t (@triversity_list) {
        $counter++;
        my %triversity_hash=%$t;
        # Look only at the triversity records for the specified date
        my $tdate=$triversity_hash{'date'};
        my @tmp=split(/\s+/,$tdate);
        $tdate=$tmp[0];
        my $date=$startdate_selection;
        @tmp=split(/\s+/,$date);
        $date=$tmp[0];
        next unless ("$tdate" eq "$date");
        my $matched=0;
        foreach my $p (@paymentech_list) {
            # Search through the entire triversity transaction list to find a matching transaction
            my %paymentech_hash=%$p;
            my @req_list=@required_key_list;
            my $gc_flag=0;
            if ($paymentech_hash{'cardtype'} eq "GiftCard") {
                # Set the appropriate required list
                @req_list=@required_key_list_gc;
                $gc_flag=1;
            }
            my $key_match=1;
            foreach my $r (@req_list) {
                my $paymentech_value=$paymentech_hash{$r};
                my $triversity_value=$triversity_hash{$r};
                if (($gc_flag) && ($r eq "charge")){
                    # If we are looking at a gift card and are comparing the amount, we need to do some adjustments
                    my $tmp_value=$paymentech_value;
                    $tmp_value=~ s/\$//g;
                    if ($tmp_value < 0) {
                        $paymentech_value="\$".sprintf("%.2f",abs($tmp_value));
                    }
                }
                unless ($paymentech_value eq $triversity_value) {
                    # We found a required value which does not match so this is not the same record
                    $key_match=0;
                }
            }
            if ($key_match) {
                $matched=1;
                $matched_pmt_transactions{$counter}=$p;
                $matched_tri_transactions{$counter}=$t;
            }
        }
        unless ($matched) {
            # Add this to the list of not matched paymentech transactions
            push(@not_matched_triversity_list,$t);
        }
    }
    if ($label =~ /Match/i) {
        print $q->p("Triversity Transactions matched to Paymentech");
        my $counter=0;
        foreach my $match (sort(keys(%matched_pmt_transactions))) {
            $counter++;
            my $ref=$matched_pmt_transactions{$match};
            my %phash=%$ref;
            $ref=$matched_tri_transactions{$match};
            my %thash=%$ref;
            my $sep;
            print "#".$counter." Triversity: ";
            foreach my $key (sort(keys(%thash))) {
                print "$sep $key: $thash{$key}";
                $sep=" -";
            }
            print "<br>";
            $sep="";
            print "Paymentech: ";
            foreach my $key (sort(keys(%phash))) {
                print "$sep $key: $phash{$key}";
                $sep=" -";
            }
            print "<br>";
            print "------------------------------------------<br>";
        }

        $dbh->disconnect;
        localfooter("STORERPTS");
        standardfooter();
        exit;
    }
    my @possible_paymentech_matches;
    if (($#not_matched_paymentech_list > -1) && (0)) {
        # The discrepancies may be because the matching transaction occured on a different date so look for a matching transaction which occured close to this time.
        foreach my $p (@not_matched_paymentech_list) {
            # Try to find a match in triversity
            my %hash=%$p;
            # Grab the required fields
            my $storeid = $hash{'storeid'};
            my $cardtype = $hash{'cardtype'};
            my $authcode = $hash{'authcode'};
            my $charge = $hash{'charge'};
            my $cardno = $hash{'cardno'};
            my $expdate = $hash{'expdate'};
            my $date = $hash{'date'};

            my $query = "
            SELECT
                tm.[StoreId]
                ,tm.[TranDateTime]
                ,case
                    when convert(varchar(2), ta.[TenderId]) = '4' then 'MasterCard'
                    when ta.[TenderId] = 5 then 'Visa'
                    when ta.[TenderId] = 6 then 'Amex'
                    when ta.[TenderId] = 7 then 'Discover'
                    when ta.[TenderId] = 14 then 'GiftCard'
                    else convert(varchar(2), ta.[TenderId])
                end as Tender
                ,ta.[CardNumber]
                ,ta.[ExpireDate]
                ,ta.[AuthorizationNum]
                ,ta.[AuthorizedAmount]

                ,ta.[CardName]
                ,tm.[TranNum]
                ,tm.[RegisterId]
                ,tm.[CashierId]

                FROM [****TLog].[dbo].[tblAuthorization] ta
                join  [****TLog].[dbo].[tblTranMaster] tm on tm.[TranId] = ta.[TranId]
                where
                    ta.[AuthorizedAmount] = $charge
                and
                    tm.[isPostVoided] = 0
                and
                    tm.[TranVoid] = 0
                and
                    ta.[CardNumber] like '%$cardno'
                and
                    ta.[TranId] like '$storeid%'
                and
                    (tm.[TranDateTime] > DateAdd(day,-7,'$date')
                    or
                    tm.[TranDateTime] < DateAdd(day,+14,'$date'))
            ";
            my $sth=$dbh->prepare("$query");
            $sth->execute();
            while (my @tmp = $sth->fetchrow_array()) {
                my $charge=sprintf("%.2f",$tmp[6]);
                my %hash=(
                    'storeid' => "$tmp[0]",
                    'date' => "$tmp[1]",
                    'cardtype' => "$tmp[2]",
                    'cardno' => "$tmp[3]",
                    'expdate' => "$tmp[4]",
                    'authcode' => "$tmp[5]",
                    'charge' => "\$$charge",

                     'cardName' => "$tmp[7]",
                     'tranNum' => "$tmp[8]",
                     'registerId' => "$tmp[9]",
                     'cashierId' => "$tmp[10]",
                );

                push(@possible_paymentech_matches,\%hash);

            }
        }
    }

    # Report the results
    ### PART I ###
    if ($#not_matched_paymentech_list > -1) {
        print $q->h3("PART I: The following transactions are found at paymentech but are not matched in Triversity");
        my @array_to_print;
        my @header;
        foreach my $p (@not_matched_paymentech_list) {
            my %hash=%$p;
            my @array;
            if ($#header < 0) {
                # If we have not gathered the header info yet, get it now
                foreach my $key (sort(keys(%hash))) {
                    push(@header,$key);
                }
                push(@array_to_print,\@header);
            }
            foreach my $key (sort(keys(%hash))) {
                push(@array,$hash{$key});
            }
            push(@array_to_print,\@array);
        }
        # Display results:
        if (1) {

            printResult_arrayref2(\@array_to_print);
            # Add a button

            ####
            print $q->p();
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"SRCHTRI"});
            my $ref=$array_to_print[0];
            my @array=@$ref;
            my $header=join(" ",@array);
            print $q->input({-type=>"hidden", -name=>"header", -value=>"$header"});
            for (my $x=1; $x<=$#array_to_print; $x++) {
                my $ref=$array_to_print[$x];
                my @array=@$ref;
                my $value=join(" ",@array);
                my $row="row"."$x";
                print $q->input({-type=>"hidden", -name=>"$row", -value=>"$value"});
            }

            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'S', -value=>"   Search   "}),"Search for matching Triversity Transactions")
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            print $q->p();

        }

        ### PART Ia ###
        if ($#possible_paymentech_matches > -1) {
            print $q->p("The following transactions are found in Triversity and are possible matches to the Paymentech transactions above:");
            my @array_to_print;
            my @header;
            foreach my $p (@possible_paymentech_matches) {
                my %hash=%$p;
                my @array;
                if ($#header < 0) {
                    # If we have not gathered the header info yet, get it now
                    foreach my $key (sort(keys(%hash))) {
                        push(@header,$key);
                    }
                    push(@array_to_print,\@header);
                }
                foreach my $key (sort(keys(%hash))) {
                    push(@array,$hash{$key});
                }
                push(@array_to_print,\@array);
            }
            if (1) {
                printResult_arrayref2(\@array_to_print);
            }

        }
    } else {
        print $q->p("No transactions are found at paymentech that are not matched in Triversity");
    }
    my @possible_triversity_matches;

    if (($#not_matched_triversity_list > -1) && (0)) {
        # Try to find possible matches for these in the Paymentech table which are close to this time

        foreach my $p (@not_matched_triversity_list) {
            # Try to find a match in triversity
            my %hash=%$p;
            # Grab the required fields
            my $storeid = $hash{'storeid'};
            my $cardtype = $hash{'cardtype'};
            my $authcode = $hash{'authcode'};
            my $charge = $hash{'charge'};
            my $cardno = $hash{'cardno'};
            my $expdate = $hash{'expdate'};
            my $date = $hash{'date'};
            if ($cardtype eq "GiftCard") {
                # This is a gift card so we need to look for the appropriate type in the paymentech table.
                $cardtype="Stored Value";
            }
            # Need to change the format of the expiration date to match the format in the paymentech table

            my $yr=substr($expdate,2,2);
            my $mo=substr($expdate,4,2);
            $expdate="$mo"."\/"."$yr";
            ####
            my $query = "
                SELECT
                    pmi.[StoreId]
                    ,ptd.[TranDateTime]
                    ,ptd.[CardType]
                    ,ptd.[CardholderNo]
                    ,ptd.[ExpDate]
                    ,ptd.[AuthCode]
                    ,ptd.[charge]
                    ,ptd.[TrType]
                FROM
                    [****Data].[dbo].[tblPaymentechTransactionDetailInfo]  ptd
                   join [****Data].[dbo].[tblPaymentechMerchantInfo] pmi on ptd.[ReportingMerchNo] = pmi.[MerchantNo] and ptd.[TermId] = pmi.[TermId]
                where
                    ptd.[Amt] = '$charge'
                and
                    pmi.[StoreId] = '$storeid'
                and
                    ptd.[CardType] = '$cardtype'
                and
                    ptd.[CardholderNo] = '$cardno'
                and
                    ptd.[ExpDate] = '$expdate'
                and
                    (ptd.[TranDateTime] > DateAdd(day,-7,'$date')
                    or
                    ptd.[TranDateTime] < DateAdd(day,+14,'$date'))
                order by ptd.[TranDateTime]
            ";

            my $sth=$dbh->prepare("$query");
            $sth->execute();

            while (my @tmp = $sth->fetchrow_array()) {
                my $charge=sprintf("%.2f",$tmp[6]);
                my $storeid=$tmp[0];
                if ($storeid==0) {
                    $storeid="Akron";
                }
                my $cardtype = $tmp[2];
                if ($cardtype eq "Stored Value") {
                    # This is a gift card
                    $cardtype="GiftCard";
                }
                my $expdate=$tmp[4];
                # The format in this table does not match the triversity format so change it
                my @etmp=split(/\//,$expdate);
                my $prefix=substr($current_year,0,2);
                my $year="$prefix"."$etmp[1]";
                my $mo=$etmp[0];
                $expdate="$year"."$mo";

                my %hash=(
                    'storeid' => "$storeid",
                    'date' => "$tmp[1]",
                    'cardtype' => "$cardtype",
                    'cardno' => "$tmp[3]",
                    'expdate' => "$expdate",
                    'authcode' => "$tmp[5]",
                    'charge' => "\$$charge",
                );

                push(@possible_triversity_matches,\%hash);

            }
            ####
        }
    }
    ### PART II ###
    if ($#not_matched_triversity_list > -1) {
        print $q->h3("PART II: The following transactions are found at Triversity but are not matched in Paymentech");
        my @array_to_print;
        my @header;
        foreach my $p (@not_matched_triversity_list) {
            my %hash=%$p;
            my @array;
            if ($#header < 0) {
                # If we have not gathered the header info yet, get it now
                foreach my $key (sort(keys(%hash))) {
                    push(@header,$key);
                }
                push(@array_to_print,\@header);
            }
            foreach my $key (sort(keys(%hash))) {
                push(@array,$hash{$key});
            }
            push(@array_to_print,\@array);
        }
        # Display results:
        printResult_arrayref2(\@array_to_print);
        # Add a button

        ####
        print $q->p();
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"SRCHPMT"});
        my $ref=$array_to_print[0];
        my @array=@$ref;
        my $header=join(" ",@array);
        print $q->input({-type=>"hidden", -name=>"header", -value=>"$header"});
        for (my $x=1; $x<=$#array_to_print; $x++) {
            my $ref=$array_to_print[$x];
            my @array=@$ref;
            my $value=join(" ",@array);
            my $row="row"."$x";
            print $q->input({-type=>"hidden", -name=>"$row", -value=>"$value"});
        }

        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'S', -value=>"   Search   "}),"Search for matching Paymentech Transactions")
                  );
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        print $q->p();


        ### PART IIa ###
        if ($#possible_triversity_matches > -1) {
            my @array_to_print;
            my @header;
            foreach my $p (@possible_triversity_matches) {
                my %hash=%$p;
                my @array;
                if ($#header < 0) {
                    # If we have not gathered the header info yet, get it now
                    foreach my $key (sort(keys(%hash))) {
                        push(@header,$key);
                    }
                    push(@array_to_print,\@header);
                }
                foreach my $key (sort(keys(%hash))) {
                    push(@array,$hash{$key});
                }
                push(@array_to_print,\@array);
            }
            # Display results:
            print $q->p("The following transactions are found at Paymentech and are possible matches to the Triversity transactions above:");
            printResult_arrayref2(\@array_to_print);

        }
    } else {
        print $q->p("No transactions are found in Triversity that are not matched at Paymentech");
    }
    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "SRCHPMT") {
    print $q->h2("Searching for matching Paymentech Transactions");
    # Rebuild the array from the parameters passed in
    my @header;
    my @array_to_print;
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    foreach my $name ($q->param) {
        my $value=trim(($q->param($name)));
        if ($name eq "header") {
            @header=split(/\s+/,$value);
            push(@array_to_print,\@header);
        } elsif ($name =~ "row") {
            # Because the expiration field is a date and time, it gets split above
            # This joins fields 4 and 5 into one datetime field
            my @tmp=split(/\s/,$value);
            my @array;
            my $date=$tmp[6];
            my $time=$tmp[7];
            for (my $x=0; $x<=$#tmp; $x++) {
                next if ($x == 7);
                if ($x == 6) {
                    push(@array,"$date $time");
                } else {
                    push(@array,$tmp[$x]);
                }

            }
            push(@array_to_print,\@array);
        }
    }
    if ($#array_to_print > -1) {
        print $q->p("The following transactions are found at paymentech but are not matched in Triversity");

        printResult_arrayref2(\@array_to_print);

        my %hash;
        my @possible_paymentech_matches;

        # For each record, try to find a match
        for ( my $x=1; $x<=$#array_to_print; $x++) {
            my $ref=$array_to_print[$x];
            my @record_array=@$ref;
            for (my $y=0; $y<=$#record_array; $y++) {
                $hash{$header[$y]}=$record_array[$y];
            }
            ####
            # Look for a possible match in the Triversity Database
            ####
            # Grab the required fields
            my $storeid = $hash{'storeid'};
            my $cardtype = $hash{'cardtype'};
            my $authcode = $hash{'authcode'};
            my $charge = $hash{'charge'};
            my $cardno = $hash{'cardno'};
            my $expdate = $hash{'expdate'};
            my $date = $hash{'date'};

            # Trim the dollar signs off of the amount
            $charge=~ s/\$//g;
            if ($cardtype eq "GiftCard") {
                # This is a gift card so we need to look for the appropriate type in the paymentech table.
                $cardtype="Stored Value";
            }
            # Need to change the format of the expiration date to match the format in the paymentech table

            my $yr=substr($expdate,2,2);
            my $mo=substr($expdate,4,2);
            $expdate="$mo"."\/"."$yr";
            ####
            my $query = "
                SELECT
                    pmi.[StoreId]
                    ,ptd.[TranDateTime]
                    ,ptd.[CardType]
                    ,ptd.[CardholderNo]
                    ,ptd.[ExpDate]
                    ,ptd.[AuthCode]
                    ,ptd.[amt]
                    ,ptd.[TrType]
                FROM
                    [****Data].[dbo].[tblPaymentechTransactionDetailInfo]  ptd
                   join [****Data].[dbo].[tblPaymentechMerchantInfo] pmi on ptd.[ReportingMerchNo] = pmi.[MerchantNo] and ptd.[TermId] = pmi.[TermId]pmi on ptd.[MerchantName] = pmi.[MerchantName] and ptd.[TermId] = pmi.[TermId]
                where
                    ptd.[Amt] = '$charge'
                and
                    pmi.[StoreId] = '$storeid'
                and
                    ptd.[CardType] = '$cardtype'
                and
                    ptd.[CardholderNo] = '$cardno'
                and
                    ptd.[ExpDate] = '$expdate'
                and
                    (ptd.[TranDateTime] > DateAdd(day,-7,'$date')
                    or
                    ptd.[TranDateTime] < DateAdd(day,+14,'$date'))
                order by ptd.[TranDateTime]
            ";


            my $sth=$dbh->prepare("$query");
            $sth->execute();
            while (my @tmp = $sth->fetchrow_array()) {
                my $charge=sprintf("%.2f",$tmp[6]);
                my %hash=(
                    'storeid' => "$tmp[0]",
                    'date' => "$tmp[1]",
                    'cardtype' => "$tmp[2]",
                    'cardno' => "$tmp[3]",
                    'expdate' => "$tmp[4]",
                    'authcode' => "$tmp[5]",
                    'charge' => "\$$charge",

                     #'cardName' => "$tmp[7]",
                     #'tranNum' => "$tmp[8]",
                     #'registerId' => "$tmp[9]",
                     #'cashierId' => "$tmp[10]",
                );
                push(@possible_paymentech_matches,\%hash);
            }
        }
        if ($#possible_paymentech_matches > -1) {
            # We have a list of possible matches so display them
            print $q->p("Below are possible matches in the Paymentech database");
            my @array_to_print;
            my @header;
            foreach my $p (@possible_paymentech_matches) {
                my %hash=%$p;
                my @array;
                if ($#header < 0) {
                    # If we have not gathered the header info yet, get it now
                    foreach my $key (sort(keys(%hash))) {
                        push(@header,$key);
                    }
                    push(@array_to_print,\@header);
                }
                foreach my $key (sort(keys(%hash))) {
                    push(@array,$hash{$key});
                }
                push(@array_to_print,\@array);
            }
            printResult_arrayref2(\@array_to_print);
        } else {
            print $q->p("Found no good possible matches in the Paymentech database");
            # Try a looser match
            my %hash;
            my @possible_paymentech_matches;
            # For each record, try to find a match
            for ( my $x=1; $x<=$#array_to_print; $x++) {
                my $ref=$array_to_print[$x];
                my @record_array=@$ref;
                for (my $y=0; $y<=$#record_array; $y++) {
                    $hash{$header[$y]}=$record_array[$y];
                }
                ####
                # Look for a possible match in the Triversity Database
                ####
                # Grab the required fields
                my $storeid = $hash{'storeid'};
                my $cardtype = $hash{'cardtype'};
                my $authcode = $hash{'authcode'};
                my $charge = $hash{'charge'};
                my $cardno = $hash{'cardno'};
                my $expdate = $hash{'expdate'};
                my $date = $hash{'date'};

                # Trim the dollar signs off of the amount
                $charge=~ s/\$//g;
                if ($cardtype eq "GiftCard") {
                    # This is a gift card so we need to look for the appropriate type in the paymentech table.
                    $cardtype="Stored Value";
                }
                # Need to change the format of the expiration date to match the format in the paymentech table

                my $yr=substr($expdate,2,2);
                my $mo=substr($expdate,4,2);
                $expdate="$mo"."\/"."$yr";

                $charge=abs($charge);
                my $query = "
                    SELECT
                        pmi.[StoreId]
                        ,ptd.[TranDateTime]
                        ,ptd.[CardType]
                        ,ptd.[CardholderNo]
                        ,ptd.[ExpDate]
                        ,ptd.[AuthCode]
                        ,ptd.[amt]
                        ,ptd.[TrType]
                    FROM
                        [****Data].[dbo].[tblPaymentechTransactionDetailInfo]  ptd
                        join [****Data].[dbo].[tblPaymentechMerchantInfo] pmi on ptd.[ReportingMerchNo] = pmi.[MerchantNo] and ptd.[TermId] = pmi.[TermId]
                    where
                        abs(ptd.[Amt]) = '$charge'
                    and
                        pmi.[StoreId] = '$storeid'
                    and
                        ptd.[CardType] = '$cardtype'
                    and
                        ptd.[CardholderNo] = '$cardno'
                    and
                        ptd.[ExpDate] = '$expdate'
                    and
                        (ptd.[TranDateTime] > DateAdd(day,-7,'$date')
                        or
                        ptd.[TranDateTime] < DateAdd(day,+14,'$date'))
                    order by ptd.[TranDateTime]
                ";
                my $sth=$dbh->prepare("$query");
                $sth->execute();
                while (my @tmp = $sth->fetchrow_array()) {
                    my $charge=sprintf("%.2f",$tmp[6]);
                    my %hash=(
                        'storeid' => "$tmp[0]",
                        'date' => "$tmp[1]",
                        'cardtype' => "$tmp[2]",
                        'cardno' => "$tmp[3]",
                        'expdate' => "$tmp[4]",
                        'authcode' => "$tmp[5]",
                        'charge' => "\$$charge",
                        'cardName' => "$tmp[7]",
                        'tranNum' => "$tmp[8]",
                        'registerId' => "$tmp[9]",
                        'cashierId' => "$tmp[10]",
                    );
                    push(@possible_paymentech_matches,\%hash);
                }
            }
            $dbh->disconnect;
            if ($#possible_paymentech_matches > -1) {
                # We have a list of possible matches so display them
                print $q->p("Below are possible matches in the Paymentech database");
                my @array_to_print;
                my @header;
                foreach my $p (@possible_paymentech_matches) {
                    my %hash=%$p;
                    my @array;
                    if ($#header < 0) {
                        # If we have not gathered the header info yet, get it now
                        foreach my $key (sort(keys(%hash))) {
                            push(@header,$key);
                        }
                        push(@array_to_print,\@header);
                    }
                    foreach my $key (sort(keys(%hash))) {
                        push(@array,$hash{$key});
                    }
                    push(@array_to_print,\@array);
                }
                printResult_arrayref2(\@array_to_print);
            } else {
                print $q->p("Found no good possible matches in the Paymentech database using less strict comparisons.");
            }
        }
    } else {
        print $q->p("Error: I failed to get the list of triversity transactions");
    }

    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();


} elsif ($report eq "SRCHTRI") {
    print $q->h2("Searching for matching Triversity Transactions");
    # Rebuild the array from the parameters passed in
    my @header;
    my @array_to_print;
    foreach my $name ($q->param) {
        my $value=trim(($q->param($name)));
        if ($name eq "header") {
            @header=split(/\s+/,$value);
            push(@array_to_print,\@header);
        } elsif ($name =~ "row") {
            # Because the expiration field is a date and time, it gets split above
            # This joins fields 4 and 5 into one datetime field
            my @tmp=split(/\s/,$value);
            my @array;
            my $date=$tmp[4];
            my $time=$tmp[5];
            for (my $x=0; $x<=$#tmp; $x++) {
                next if ($x == 5);
                if ($x == 4) {
                    push(@array,"$date $time");
                } else {
                    push(@array,$tmp[$x]);
                }

            }
            push(@array_to_print,\@array);
        }
    }
    if ($#array_to_print > -1) {
        print $q->p("The following transactions are found at paymentech but are not matched in Triversity");

        printResult_arrayref2(\@array_to_print);

        my %hash;
        my @possible_triversity_matches;
        # For each record, try to find a match
        for ( my $x=1; $x<=$#array_to_print; $x++) {
            my $ref=$array_to_print[$x];
            my @record_array=@$ref;
            for (my $y=0; $y<=$#record_array; $y++) {
                $hash{$header[$y]}=$record_array[$y];
            }
            # Look for a possible match in the Triversity Database
            ####
            # Grab the required fields
            my $storeid = $hash{'storeid'};
            my $cardtype = $hash{'cardtype'};
            my $authcode = $hash{'authcode'};
            my $charge = $hash{'charge'};
            my $cardno = $hash{'cardno'};
            my $expdate = $hash{'expdate'};
            my $date = $hash{'date'};

            # Trim the dollar signs off of the amount
            $charge=~ s/\$//g;
            my $query = "
            SELECT
                tm.[StoreId]
                ,tm.[TranDateTime]
                ,case
                    when convert(varchar(2), ta.[TenderId]) = '4' then 'MasterCard'
                    when ta.[TenderId] = 5 then 'Visa'
                    when ta.[TenderId] = 6 then 'Amex'
                    when ta.[TenderId] = 7 then 'Discover'
                    when ta.[TenderId] = 14 then 'GiftCard'
                    else convert(varchar(2), ta.[TenderId])
                end as Tender
                ,ta.[CardNumber]
                ,ta.[ExpireDate]
                ,ta.[AuthorizationNum]
                ,ta.[AuthorizedAmount]

                ,ta.[CardName]
                ,tm.[TranNum]
                ,tm.[RegisterId]
                ,tm.[CashierId]

                FROM [****TLog].[dbo].[tblAuthorization] ta
                join  [****TLog].[dbo].[tblTranMaster] tm on tm.[TranId] = ta.[TranId]
                where
                    ta.[AuthorizedAmount] like '$charge'
                and
                    tm.[isPostVoided] = 0
                and
                    tm.[TranVoid] = 0
                and
                    ta.[CardNumber] like '%$cardno'
                and
                    ta.[TranId] like '$storeid%'
                and
                    (tm.[TranDateTime] > DateAdd(day,-7,'$date')
                    or
                    tm.[TranDateTime] < DateAdd(day,+14,'$date'))
            ";
            my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
            my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

            my $sth=$dbh->prepare("$query");
            $sth->execute();
            while (my @tmp = $sth->fetchrow_array()) {
                my $charge=sprintf("%.2f",$tmp[6]);
                my %hash=(
                    'storeid' => "$tmp[0]",
                    'date' => "$tmp[1]",
                    'cardtype' => "$tmp[2]",
                    'cardno' => "$tmp[3]",
                    'expdate' => "$tmp[4]",
                    'authcode' => "$tmp[5]",
                    'charge' => "\$$charge",

                     'cardName' => "$tmp[7]",
                     'tranNum' => "$tmp[8]",
                     'registerId' => "$tmp[9]",
                     'cashierId' => "$tmp[10]",
                );

                push(@possible_triversity_matches,\%hash);

            }
            $dbh->disconnect;
           ####
        }
        if ($#possible_triversity_matches > -1) {
            # We have a list of possible matches so display them
            print $q->p("Below are possible matches in the Triversity database");
            my @array_to_print;
            my @header;
            foreach my $p (@possible_triversity_matches) {
                my %hash=%$p;
                my @array;
                if ($#header < 0) {
                    # If we have not gathered the header info yet, get it now
                    foreach my $key (sort(keys(%hash))) {
                        push(@header,$key);
                    }
                    push(@array_to_print,\@header);
                }
                foreach my $key (sort(keys(%hash))) {
                    push(@array,$hash{$key});
                }
                push(@array_to_print,\@array);
            }
            printResult_arrayref2(\@array_to_print);
        } else {
            print $q->p("Found no good possible matches in the Triversity database");
        }
    } else {
        print $q->p("Error: I failed to get the list of paymentech transactions");
    }


    localfooter("STORERPTS");
    standardfooter();




} elsif (($report eq "CRDQRY") || ($report eq "PMTQRY")){
    my $nextreport="CRDRPT";

    if ($report eq "PMTQRY") {
        print $q->h2("Paymentech Query Menu");
        $nextreport="PMTRPT";
    } else {
        print $q->h2("Card Query Menu");
    }

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"$nextreport"});

    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections:")));

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Card ending with:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"number_selection", -size=>"4", -value=>""}))
    );
        if ($report eq "CRDQRY") {
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Associated Name:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"name_selection", -size=>"20", -value=>""}))
        );

    }
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Charge Amt:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"charge_selection", -size=>"8", -value=>""}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Limit to store number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"storeid_selection", -size=>"4", -value=>""}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_week"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"enddate_selection", -size=>"10", -value=>"$label_date"}))
    );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
			  );
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    standardfooter();
} elsif ($report eq "CRDRPT") {
    print $q->h2("Card Report");
    my $number_selection   = trim(($q->param("number_selection"))[0]);
    my $name_selection   = trim(($q->param("name_selection"))[0]);
    my $charge_selection   = trim(($q->param("charge_selection"))[0]);
    my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);

    my $startdate_where;
    my $enddate_where;
    my $charge_where;
    if ($startdate_selection) {
        $startdate_where="and tm.[TranDateTime] > '$startdate_selection'";
    }
    if ($enddate_selection) {
        $enddate_where="and tm.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
    if ($charge_selection) {
        $charge_selection=abs($charge_selection);
        $charge_where="and  abs(ta.[AuthorizedAmount]) = '$charge_selection'";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $query = "
    SELECT
        tm.[StoreId]
        ,tm.[RegisterId]
        ,tm.[TranNum]
        ,tm.[TranDateTime]

        ,ta.[RequestAmount]
        ,ta.[AuthorizedAmount]
        ,ta.[CardNumber]
        ,ta.[ExpireDate]
        ,ta.[CardName]
        ,ta.[TenderId]
        ,case
            when convert(varchar(2), ta.[TenderId]) = '4' then 'MasterCard'
            when ta.[TenderId] = 5 then 'Visa'
            when ta.[TenderId] = 6 then 'Amex'
            when ta.[TenderId] = 7 then 'Discover'
            when ta.[TenderId] = 14 then 'GiftCard'
            else convert(varchar(2), ta.[TenderId])
        end as Tender
        ,ta.[CreditType]
        ,ta.[HowAuthorized]
        ,ta.[CardEntryType]
        ,tm.[CashierId]
        ,ta.[TranId]
        ,ta.[Sequence]
        ,ta.[TranSecs]

        ,ta.[AuthorizationNum]
        ,ta.[CreditTxnType]
        ,ta.[ExtCardNum]
        ,ta.[CashBackAmount]
        ,ta.[RetrievalNum]
        ,ta.[OrigCreditTranType]
        ,ta.[CorpCardType]
        ,ta.[NonMerchUPCCode]
        ,ta.[COMResultCode]
        ,ta.[Response1]
        ,ta.[Response2]

        ,ta.[BalanceAccount]
        ,ta.[ResponseCode]
        ,ta.[VoucherId]
        ,ta.[VoucherAgent]
        ,ta.[TranDate]
        ,ta.[TranTime]
        ,ta.[RefNum]
        ,ta.[ElapsedTime]
        ,ta.[TaxAmount]
        ,ta.[CustomerRefNum]
        ,ta.[procState]
        ,ta.[lastStateChange]
        ,ta.[uid]

        ,tm.[SaleType]
        ,tm.[TranModifier]
        ,tm.[TranVoid]
        ,tm.[BusinessDate]
        ,tm.[tlogfile]
        ,tm.[tlogline]
        ,tm.[isSale]
        ,tm.[isReturn]
        ,tm.[isDiscount]
        ,tm.[isPriceOver]
        ,tm.[isTotaled]
        ,tm.[isPayment]
        ,tm.[isPostVoided]
        ,tm.[isZeroPayment]
        ,tm.[numRecords]
        ,tm.[numUnitsSold]
        ,tm.[paymentTotal]
        FROM [****TLog].[dbo].[tblAuthorization] ta
        join  [****TLog].[dbo].[tblTranMaster] tm on tm.[TranId] = ta.[TranId]
        where
            tm.[isPostVoided] = 0
        and
            tm.[TranVoid] = 0
        and
            ta.[CardNumber] like '%$number_selection'
        and
            ta.[CardName] like '$name_selection%'
        and
            ta.[TranId] like '$storeid_selection%'
        $startdate_where
        $enddate_where
        $charge_where
    ";

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {

        my @format = (
        '', '','','',
        {'align' => 'Right', 'currency' => ''},{'align' => 'Right', 'currency' => ''},
        '','','','','','','','','','','','',
        '','','','','','','','','','','',
        {'align' => 'Right', 'currency' => ''},
        '','','','','','','',
        {'align' => 'Right', 'currency' => ''},
        '','','','','','','','','','','','',
        '','','','','','','','',
        {'align' => 'Right', 'currency' => ''},
        );
        printResult_arrayref2($tbl_ref, '',\@format);
    } else {
        print $q->b("no records found<br>\n");
    }
    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();
} elsif ($report eq "PMTRPT") {
    print $q->h2("Paymentech Report");
    my $number_selection   = trim(($q->param("number_selection"))[0]);
    my $name_selection   = trim(($q->param("name_selection"))[0]);
    my $charge_selection   = trim(($q->param("charge_selection"))[0]);
    my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);

    my $startdate_where;
    my $enddate_where;
    my $number_where;
    my $storeid_where;
    my $charge_where;
    if ($startdate_selection) {
        $startdate_where="and ptd.[TranDateTime] > '$startdate_selection'";
    }
    if ($enddate_selection) {
        $enddate_where="and ptd.[TranDateTime] < '$enddate_selection 23:59:59'";
    }
    if ($number_selection) {
        $number_where=" and ptd.[CardholderNo] like '%$number_selection'";
    }
    if ($storeid_selection) {
        $storeid_where=" and (pmi.[StoreId] like '$storeid_selection' or pmi.[StoreId] like 'Akron')";
    }
    if ($charge_selection) {
        $charge_selection=abs($charge_selection);
        $charge_where="and  abs(ptd.[Amt]) = '$charge_selection'";
    }
    my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $query = "
        SELECT
            ptd.[MerchantName]
            ,pmi.[StoreId]
            ,ptd.[TranDateTime]

            ,ptd.[SeqNo]
            ,ptd.[CardType]
            ,ptd.[CardholderNo]
            ,ptd.[ExpDate]
            ,ptd.[AuthCode]
            ,ptd.[EntryMode]
            ,ptd.[TermID]
            ,ptd.[TrType]
            ,ptd.[RecordType]
            ,ptd.[Amt]
            ,ptd.[ReportingMerchNo]
            ,ptd.[BatchNo]
            ,ptd.[BatchClose]
        FROM
            [****Data].[dbo].[tblPaymentechTransactionDetailInfo]  ptd
           join [****Data].[dbo].[tblPaymentechMerchantInfo] pmi on ptd.[ReportingMerchNo] = pmi.[MerchantNo] and ptd.[TermId] = pmi.[TermId]
        where
            1=1
        $storeid_where
        $number_where
        $startdate_where
        $enddate_where
        $charge_where
    ";

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {

        my @format = (
        '', '','','','','','',
        '','','','','',

        {'align' => 'Right', 'currency' => ''},
        );
        printResult_arrayref2($tbl_ref, '',\@format);
    } else {
        print $q->b("no records found<br>\n");
    }
    $dbh->disconnect;
    localfooter("STORERPTS");
    standardfooter();
	
	
} elsif ($report eq "MONTHLY-CDR-POS") {	

	print $q->h3("POS Support Monthly Call Report");
    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );	
    
    # Use yesterday as the default date    
    my $year=($previous_day_ltm[5]+1900);
    my $this_month=$previous_day_ltm[4];
    $this_month++;	
	my $month_options;
    my $year_options;	
	
    foreach my $month (sort {$a <=> $b} (keys(%month_hash))) {
        if ($month == $this_month) {
            $month_options .= $q->option({-value=>$month -selected=>'selected'}, "$month_hash{$month}");
        } else {
            $month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
        }
    }
    $year=$current_year;
    while ($year > 1999) {
        $year_options .= $q->option({-value=>$year}, "$year");
        $year--;
    }	
### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"MONTHLY-CDR-POS-RPT"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));


    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Month:"),
				$q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"month_selection", -size=>"1"}), $month_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"year_selection", -size=>"1"}), $year_options),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),						
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                						
              );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    localfooter("PHONEMENUS");
    standardfooter();	
} elsif ($report eq "MONTHLY-CDR-POS-RPT") {	
	print $q->h3("POS Support Monthly Call Report");	
    my $month_selection   = trim(($q->param("month_selection"))[0]);	
	my $year_selection   = trim(($q->param("year_selection"))[0]);
    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );	
	my $month=$month_hash{$month_selection};
	print $q->h4("$month $year_selection");
	my $date_selection="$year_selection"."-"."$month_selection"."-"."01";
 
	$query = dequote(<< "    ENDQUERY");
    select		
        cdr.CALLSTARTTIMEINGRESSGWACCESS as "Start",
	 
        cdr.CALLDURATIONCONNECTTODISCONN as "Duration"
    from
        call_detail_record cdr
    WHERE 
		(CALLEDPARTYE164ADDRESS in ($ITPOOL) or CALLINGPARTYE164ADDRESS in ($ITPOOL))
		and CALLSTARTTIMEINGRESSGWACCESS > '$date_selection'
		and CALLSTARTTIMEINGRESSGWACCESS < date_add('$date_selection', INTERVAL 1 MONTH)
		and CALLDURATIONCONNECTTODISCONN > 0
 
    order by
		cdr.CALLSTARTTIMEINGRESSGWACCESS        
   
    ENDQUERY
	
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
    unless ($dbh) {
        print "failed to connect to $mysqld<br>";
        exit;
    }	
	#print $q->pre("$query");
	my $sth=$dbh->prepare("$query");
	 
	$sth->execute();
	 
	my @header=("DoW","Date","Count","Duration");
	my @array_to_print=\@header;
	my %date_hash;
	my %date_call_count_hash;
	my $record_count=0;
	while (my @tmp = $sth->fetchrow_array()) {
		my $date = $tmp[0];
		my @tmp2=split(/\s+/,$date);
		$date=$tmp2[0];
		my $duration = $tmp[1];		
		$date_hash{$date}+=$duration;
		$date_call_count_hash{$date}++;
		$record_count++;
	}	
 
	# Get the day of the week for the dates	
	my @week=('','','','','','','');

	my @calendar_header=("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	my @calendar_to_print=\@calendar_header;
 
	#printResult_arrayref2(\@calendar_to_print); 
 
	foreach my $date (sort keys(%date_hash)) {
		my $duration = $date_hash{$date};
		my $count=$date_call_count_hash{$date};
		my $dow;
		$query="select DAYOFWEEK('$date')";
		#print $q->pre("$query");
		$sth=$dbh->prepare("$query");
		$sth->execute();
		
		while (my @tmp = $sth->fetchrow_array()) {		
			$dow=$tmp[0];
			$dow--;
		}
		#if (defined($week[$dow])) {
		if ($week[$dow]) {
			# This week is filled up so put it on the calendar
			my @array=@week;
			push(@calendar_to_print,\@array);
			# Clear the week array so we can start over
			@week=();
			#$week[$dow]="$duration";
			$week[$dow]="$date $duration";
		} else {
			$week[$dow]="$date $duration";
			#$week[$dow]="$duration";
		}
		my @array1=("$calendar_header[$dow]","$date","$count","$duration");
		push(@array_to_print,\@array1);			
		
	}
	# The calendar version is not working and is more confusing.
	# Push the last week on the calendar
 
	push(@calendar_to_print,\@week);
	#if (recordCount(\@calendar_to_print)) {				 		
		#printResult_arrayref2(\@calendar_to_print); 		
	#} else {
		#print $q->p("Found no data");
	#}	
	
 
	if (recordCount(\@array_to_print)) {				 		
		printResult_arrayref2(\@array_to_print); 		
	} else {
		print $q->p("Found no data");
	}	
	$dbh->disconnect;
    localfooter("PHONEMENUS");
    standardfooter();		
	
	
} elsif ($report eq "IN-MONTHLY-CDR-POS") {	
	
	print $q->h3("Incoming POS Support Monthly Call Report");
	print $q->h4("Calls Coming in to $POSPOOL");
    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );	
    
    # Use yesterday as the default date    
    my $year=($previous_day_ltm[5]+1900);
    my $this_month=$previous_day_ltm[4];
    $this_month++;	
	my $month_options;
    my $year_options;	
	
	foreach my $month (sort {$a <=> $b} (keys(%month_hash))) {
        if ($month == $this_month) {
            $month_options .= $q->option({-value=>$month -selected=>'selected'}, "$month_hash{$month}");
        } else {
            $month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
        }
    }
    $year=$current_year;
    while ($year > 1999) {
        $year_options .= $q->option({-value=>$year}, "$year");
        $year--;
    }	
### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"IN-MONTHLY-CDR-POS-RPT"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));


    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Month:"),
				$q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"month_selection", -size=>"1"}), $month_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"year_selection", -size=>"1"}), $year_options),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),						
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                						
              );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    localfooter("PHONEMENUS");
    standardfooter();	
} elsif ($report eq "IN-MONTHLY-CDR-POS-RPT") {	
	print $q->h3("Incoming POS Support Monthly Call Report");
	print $q->h4("Calls Coming in to $POSPOOL");
    my $month_selection   = trim(($q->param("month_selection"))[0]);	
	my $year_selection   = trim(($q->param("year_selection"))[0]);
    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );	
	my $month=$month_hash{$month_selection};
	print $q->h4("$month $year_selection");
	my $date_selection="$year_selection"."-"."$month_selection"."-"."01";

	$query = dequote(<< "    ENDQUERY");
    select		
        cdr.CALLSTARTTIMEINGRESSGWACCESS as "Start",	 
        cdr.CALLDURATIONCONNECTTODISCONN as "Duration",
		cdr.CALLEDPARTYE164ADDRESS
    from
        call_detail_record cdr
    WHERE 
		CALLEDPARTYE164ADDRESS in ($POSPOOL) 
		and CALLSTARTTIMEINGRESSGWACCESS > '$date_selection'
		and CALLSTARTTIMEINGRESSGWACCESS < date_add('$date_selection', INTERVAL 1 MONTH)
		and CALLDURATIONCONNECTTODISCONN > 0
 
    order by
		cdr.CALLSTARTTIMEINGRESSGWACCESS        
   
    ENDQUERY
	
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
    unless ($dbh) {
        print "failed to connect to $mysqld<br>";
        exit;
    }	
	#print $q->pre("$query");
	my $sth=$dbh->prepare("$query");
	 
	$sth->execute();
	 
	my @header=("DoW","Date","Extension","Count","Duration");
	my @array_to_print=\@header;
	my %date_hash;
	my %date_call_count_hash;
	my $record_count=0;
	while (my @tmp = $sth->fetchrow_array()) {
		my $date = $tmp[0];
		my @tmp2=split(/\s+/,$date);
		$date=$tmp2[0];
		my $duration = $tmp[1];		
		my $extension = $tmp[2];	
		$date_hash{"$date $extension"}+=$duration;
		$date_call_count_hash{"$date $extension"}++;
		$record_count++;
	}	
 
	# Get the day of the week for the dates	
	my @week=('','','','','','','');

	my @calendar_header=("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	my @calendar_to_print=\@calendar_header;
 
	#printResult_arrayref2(\@calendar_to_print); 
 
	foreach my $date_key (sort keys(%date_hash)) {
		my @tmp=split(/\s+/,$date_key);
		my $date = $tmp[0];
		my $extension = $tmp[1];
		my $duration = $date_hash{$date_key};
		my $count=$date_call_count_hash{$date_key};
		my $dow;
		$query="select DAYOFWEEK('$date')";
		#print $q->pre("$query");
		$sth=$dbh->prepare("$query");
		$sth->execute();
		
		while (my @tmp = $sth->fetchrow_array()) {		
			$dow=$tmp[0];
			$dow--;
		}

		my @array1=("$calendar_header[$dow]","$date","$extension","$count","$duration");
		push(@array_to_print,\@array1);			
		
	}

 

	
 
	if (recordCount(\@array_to_print)) {				 		
		printResult_arrayref2(\@array_to_print); 		
	} else {
		print $q->p("Found no data");
	}	
	$dbh->disconnect;
    localfooter("PHONEMENUS");
    standardfooter();		
	
} elsif (($report eq "CDR")||($report eq "CDR-IT")||($report eq "CDR-POS")) {
    # This is the main section for getting phone reports.
    # This section is entered if the phone reports are called but this section can also call itself.
    # The first time this section is entered, the select criteria is displayed at the top and a default report.
    # After select criteria is entered, this section calls itself using the select criteria.
    # This section may also be called from the update_alias function.  At that time, it brings a phone number
    # and the associated alias.  If that data is present, it is first added to the database before the standard
    # select criteria and report are displayed. - kdg
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld $DBI::errstr");
		standardfooter();
		exit;
	}
    my $itLimit=0;
    my $msg = "All Calls";
    $itLimit   = trim(($q->param("itLimit"))[0]);
    # If we call the report CDR but have itLimit set, change the report name
    if ($itLimit) {
        $report="CDR-IT";
        $msg = "POS Calls";
    }
    # If we call the report CDR-IT but have not set itLimit, set it.
    # itLimit is what differentiates CDR from CDR-IT
    if ($report eq "CDR-IT") {
        $itLimit=1;
        $msg = "POS Calls";
    }
    if ($report eq "CDR-POS") {
        $itLimit=2;
        $msg = "POS Calls II";
    }
	print $q->h2("Call Record Report: $msg");
    my $limit   = trim(($q->param("limit_selection"))[0]);
    if ($limit eq '') {
        # Default limit
        $limit=100;
    }
    my $order   = trim(($q->param("order_selection"))[0]);
    if ($order eq '') {
        # Default order
        $order="CALLSTARTTIMEINGRESSGWACCESS";
    }

    my $number_selection   = trim(($q->param("number_selection"))[0]);
    my $number_end_selection   = trim(($q->param("number_end_selection"))[0]);
    my $from_alias_number_selection   = trim(($q->param("from_alias_number_selection"))[0]);
    my $called_number_selection   = trim(($q->param("called_number_selection"))[0]);
    my $called_alias_selection   = trim(($q->param("called_alias_selection"))[0]);
    my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
    my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);
    my $nozero   = trim(($q->param("cb_nozero"))[0]);
    my $notfrompos = trim(($q->param("cb_notfrompos"))[0]);

    my $primary_ref   = trim(($q->param("primary_ref"))[0]);
    my $secondary_ref   = trim(($q->param("secondary_ref"))[0]);
    my $phoneNum = trim(($q->param("phoneNum"))[0]);

    # Set the default start selection
    unless ($start_date_selection) {
        $start_date_selection=$label_date_previous_day;
    }
    unless ($end_date_selection) {
        $end_date_selection=$label_date;
    }
    # If we have a phoneNumber to update, do that first
 
    if ($phoneNum) {
        if (($primary_ref)||($secondary_ref)) {
            if ($primary_ref) {
                if (update_alias("primary_ref","$primary_ref", "$phoneNum")) {
                    print $q->p("Set $primary_ref as alias for $phoneNum");
                } else {
                   print $q->p("Error setting $primary_ref as alias for $phoneNum!");
                }
            }
            if ($secondary_ref) {
                if (update_alias("secondary_ref","$secondary_ref", "$phoneNum")) {
                    print $q->p("Set $secondary_ref as secondary alias for $phoneNum");
                } else {
                   print $q->p("Error setting $secondary_ref as secondary alias for $phoneNum!");
                }
            }
        } else {
            print $q->p("ERROR: Failed to get an alias for $phoneNum");
            standardfooter();
            exit;
        }

    }
 
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"CDR_REPORT"});
    print $q->input({-type=>"hidden", -name=>"itLimit", -value=>"$itLimit"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));

     # Get the call from  options

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Call From Numbers Starting with:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"number_selection", -size=>"4", -value=>"$number_selection"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Call From Numbers Ending with:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"number_end_selection", -size=>"4", -value=>"$number_end_selection"}))
    );
 
     # Get the call from  Alias options
    $query = "select distinct primary_ref from phone_cross_ref order by primary_ref";
    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
 
    my $from_alias_number_options;
    if ($from_alias_number_selection) {
        $from_alias_number_options = $q->option({-value=>"$from_alias_number_selection"}, $from_alias_number_selection);
        $from_alias_number_options .= $q->option({-value=>'' -selected=>undef});
    } else {
        $from_alias_number_options = $q->option({-value=>'' -selected=>undef});
    }

    for my $datarows (1 .. $#$tbl_ref_alias)
    {
     my $thisrow = @$tbl_ref_alias[$datarows];
     my $number = trim($$thisrow[0]);

     $from_alias_number_options .= $q->option({-value=>$number}, "$number");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Call From Alias:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"from_alias_number_selection", -size=>"1"}), $from_alias_number_options)
              );



    # Get the call to  options
    if ($itLimit) {
        # Only get calls to POS support
        $query = "select distinct CALLEDPARTYE164ADDRESS from call_detail_record where CALLEDPARTYE164ADDRESS in ($ITPOOL) order by CALLEDPARTYE164ADDRESS";
    } else {
        $query = "select distinct CALLEDPARTYE164ADDRESS from call_detail_record order by CALLEDPARTYE164ADDRESS";
    }

    my $tbl_ref = execQuery_arrayref($dbh, $query);

    # The called number options
    my $called_number_options;
    if ($called_number_selection) {
        $called_number_options = $q->option({-value=>"$called_number_selection"}, $called_number_selection);
        $called_number_options .= $q->option({-value=>'' -selected=>undef});
    } else {
        $called_number_options = $q->option({-value=>'' -selected=>undef});
    }

    for my $datarows (1 .. $#$tbl_ref)
    {
     my $thisrow = @$tbl_ref[$datarows];
     my $number = trim($$thisrow[0]);

     $called_number_options .= $q->option({-value=>$number}, "$number");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Call Received By:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"called_number_selection", -size=>"1"}), $called_number_options)
              );

    # The called Alias options


    my $called_alias_options;
    if ($called_alias_selection) {
        $called_alias_options = $q->option({-value=>"$called_alias_selection"}, $called_alias_selection);
        $called_alias_options .= $q->option({-value=>'' -selected=>undef});
    } else {
        $called_alias_options = $q->option({-value=>'' -selected=>undef});
    }

    for my $datarows (1 .. $#$tbl_ref_alias)
    {
     my $thisrow = @$tbl_ref_alias[$datarows];
     my $number = trim($$thisrow[0]);

     $called_alias_options .= $q->option({-value=>$number}, "$number");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Call To Alias:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"called_alias_selection", -size=>"1"}), $called_alias_options)
              );
    # Get the start date options

    # Start Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"start_date_selection", -size=>"12", -value=>"$start_date_selection"}))
    );
    # End Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date(yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"end_date_selection", -size=>"12", -value=>"$end_date_selection"}))
    );
    # Order

    my $order_options;
    #$order_options.=  $q->option({-value=>"STR_TO_DATE(CALLSTARTTIMEINGRESSGWACCESS,'%Y-%m-%d') "}, "Start Time");
    $order_options.=  $q->option({-value=>"CALLSTARTTIMEINGRESSGWACCESS"}, "Start Time");
    #$order_options.=  $q->option({-value=>"STR_TO_DATE(CALLENDTIME,'%Y-%m-%d') "}, "End Time");
    $order_options.=  $q->option({-value=>"CALLENDTIME"}, "End Time");
    #$order_options.=  $q->option({-value=>"STR_TO_DATE(CALLSTARTTIMEANSWERED,'%Y-%m-%d') "}, "Time Answered");
    $order_options.=  $q->option({-value=>"CALLSTARTTIMEANSWERED"}, "Time Answered");
    $order_options.=  $q->option({-value=>'CALLINGPARTYE164ADDRESS'}, "Calling Party");
    $order_options.=  $q->option({-value=>'pcr.PRIMARY_REF'}, "Calling Alias");
    $order_options.=  $q->option({-value=>'pcr.SECONDARY_REF '}, "Secondary Alias");
    $order_options.=  $q->option({-value=>'CALLEDPARTYE164ADDRESS'}, "Called Party");
    $order_options.=  $q->option({-value=>'pcr2.PRIMARY_REF'}, "Called Alias");
    $order_options.=  $q->option({-value=>'pcr2.SECONDARY_REF'}, "Secondary Called Alias");
    $order_options.=  $q->option({-value=>'CALLDURATIONCONNECTTODISCONN'}, "Duration");

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Select Sort By:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({ -name=>"order_selection", -size=>"1"}), $order_options)
              );

    # Limit

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Report Length:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"limit_selection", -size=>"4", -value=>"$limit"}))
    );

    # Exclude 0 duration calls

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({-accesskey=>'E', -type=>"checkbox", -name=>"cb_nozero", -checked}) .
        $q->font({-size=>-1}, "Exclude 0 duration Calls ")
        ));

    if ($itLimit) {
        # Option to Exclude calls not originating from POS
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
            $q->input({-accesskey=>'F', -type=>"checkbox", -name=>"cb_notfrompos", -checked}) .
            $q->font({-size=>-1}, "Exclude calls not from POS ")
            ));

    }

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

	print $q->a({-href=>"$scriptname?report=PHONEMENUS", -target=>'_top'}, "Return to Phone Report Menu");
	print "<br />";
	standardfooter();
} elsif ($report eq "CDR_REPORT"){
###
    my $number_selection   = trim(($q->param("number_selection"))[0]);
    my $number_end_selection   = trim(($q->param("number_end_selection"))[0]);
    my $from_alias_number_selection   = trim(($q->param("from_alias_number_selection"))[0]);
    my $called_number_selection   = trim(($q->param("called_number_selection"))[0]);
    my $called_alias_selection   = trim(($q->param("called_alias_selection"))[0]);
    my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
    my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);
    my $itLimit   = trim(($q->param("itLimit"))[0]);
    my $nozero   = trim(($q->param("cb_nozero"))[0]);
    my $notfrompos = trim(($q->param("cb_notfrompos"))[0]);

    my $primary_ref   = trim(($q->param("primary_ref"))[0]);
    my $secondary_ref   = trim(($q->param("secondary_ref"))[0]);
    my $phoneNum = trim(($q->param("phoneNum"))[0]);

    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
    unless ($dbh) {
        print "failed to connect to $mysqld<br>";
        exit;
    }
    my $msg = "All Calls";
    if ($itLimit) {

        $msg = "POS Calls";
    }
 
	print $q->h2("Call Record Report: $msg");
    my $limit   = trim(($q->param("limit_selection"))[0]);
    if ($limit eq '') {
        # Default limit
        $limit=100;
    }
    my $order   = trim(($q->param("order_selection"))[0]);
    if ($order eq '') {
        # Default order
        $order="CALLSTARTTIMEINGRESSGWACCESS";
    }

    my @header=(
        "Calling Party",
        "Calling Alias",
        "Called Party",
        "Start",
        "Answered",
        "End",
        "Duration"

    );
    my $whereclause="Where 1 = 1 ";
    # The calling number starting with selection
    if ($number_selection) {
        $whereclause.="and CALLINGPARTYE164ADDRESS like '$number_selection%'";
    }
    # The calling number ending with selection
    if ($number_end_selection) {
        $whereclause.="and CALLINGPARTYE164ADDRESS like '%$number_end_selection'";
    }
    # The calling alias number selection
    if ($from_alias_number_selection) {
        $whereclause.="and pcr.primary_ref like '$from_alias_number_selection'";
    }
    # The called number selection
    if ($called_number_selection) {
        # Find only for the specified number
        $whereclause.="and CALLEDPARTYE164ADDRESS like '$called_number_selection'";
    }  elsif ($itLimit == 1) {
        # find only for ITPool numbers
        if ($notfrompos) {
            $whereclause.="and (CALLEDPARTYE164ADDRESS in ($ITPOOL) or CALLINGPARTYE164ADDRESS in ($ITPOOL))";
        } else {
            $whereclause.="and CALLEDPARTYE164ADDRESS in ($ITPOOL)";
        }

    }  elsif ($itLimit == 2) {
        # find only for ITPool numbers
        if ($notfrompos) {
            $whereclause.="and (CALLEDPARTYE164ADDRESS in ($POSPOOL) or CALLINGPARTYE164ADDRESS in ($ITPOOL))";
        } else {
            $whereclause.="and CALLEDPARTYE164ADDRESS in ($ITPOOL)";
        }

    }

    # The called alias selection
    if ($called_alias_selection) {
        my $phone_no_list;
    	my $query = dequote(<< "        ENDQUERY");
            select
                PHONE_NO
            from
                phone_cross_ref
            where
                PRIMARY_REF like '$called_alias_selection'
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my $sep;
        while (my @tmp = $sth->fetchrow_array()) {
            $phone_no_list.="$sep$tmp[0]";
            $sep=",";
        }
        if ($phone_no_list) {
            $whereclause.="and pcr2.PHONE_NO in ($phone_no_list)";
        } else {
            print $q->p("Error: Could not find a phone number for $called_alias_selection");

            standardfooter();
            exit;
        }

    }

    if ($start_date_selection) {
        if ($end_date_selection) {
           # $whereclause.="and STR_TO_DATE(CALLSTARTTIMEINGRESSGWACCESS,'%Y-%m-%d') >= '$start_date_selection'";
           # $whereclause.="and STR_TO_DATE(CALLSTARTTIMEINGRESSGWACCESS,'%Y-%m-%d') <= '$end_date_selection'";
			$whereclause.="and CALLSTARTTIMEINGRESSGWACCESS > '$start_date_selection'";
            #$whereclause.="and CALLSTARTTIMEINGRESSGWACCESS <= '$end_date_selection'";
			$whereclause.="and CALLSTARTTIMEINGRESSGWACCESS < date_add('$end_date_selection', INTERVAL 1 DAY)";

			my @tmp=split(/-/,$start_date_selection);
			my $year=$tmp[0];
			my $month=$tmp[1];
			my $day=$tmp[2];
			# Trim off leading 0's
			if ($month =~ /^0/) {
				$month=~ s/0//;
			}
			if ($day =~ /^0/) {
				$day=~ s/0//;
			}
			my $revised_date="${year}-${month}-${day}";
			#$whereclause.="and CALLSTARTTIMEINGRESSGWACCESS like '$revised_date %'";
		   #$whereclause.="and STR_TO_DATE(CALLSTARTTIMEINGRESSGWACCESS,'%Y-%m-%d') >= '$start_date_selection'";
        } else {
            print "ERROR: Start date specified but no end date\n";
            standardfooter();
            exit;

        }

    }

    if ($nozero) {
        $whereclause.="and CALLDURATIONCONNECTTODISCONN > 0";
    }
    # 
	my $query = dequote(<< "    ENDQUERY");
    select
        cdr.CALLINGPARTYE164ADDRESS as "Calling Party",
        pcr.PRIMARY_REF as "Calling Alias",
        pcr.SECONDARY_REF as "Secondary Alias",
        cdr.CALLEDPARTYE164ADDRESS as "Called Party",
        pcr2.PRIMARY_REF as "Called Alias",
        pcr2.SECONDARY_REF as "Called Secondary Alias",
        cdr.CALLSTARTTIMEINGRESSGWACCESS as "Start",
        cdr.CALLSTARTTIMEANSWERED as "Answered",
        cdr.CALLENDTIME as "End",
        cdr.CALLDURATIONCONNECTTODISCONN as "Duration"
    from
        call_detail_record cdr

        left outer join phone_cross_ref pcr on cdr.callingNo = pcr.BASE_PHONE_NO and cdr.CALLINGPARTYE164ADDRESS = pcr.PHONE_NO
        left outer join phone_cross_ref pcr2 on cdr.calledNo = pcr2.BASE_PHONE_NO and cdr.CALLINGPARTYE164ADDRESS = pcr.PHONE_NO
    $whereclause
    order by
        $order
    limit $limit
    ENDQUERY
	# Modified the second outer join in the query below - kdg 2013-03-04
	$query = dequote(<< "    ENDQUERY");
    select
        cdr.CALLINGPARTYE164ADDRESS as "Calling Party",
        pcr.PRIMARY_REF as "Calling Alias",
        pcr.SECONDARY_REF as "Secondary Alias",
        cdr.CALLEDPARTYE164ADDRESS as "Called Party",
        pcr2.PRIMARY_REF as "Called Alias",
        pcr2.SECONDARY_REF as "Called Secondary Alias",
        cdr.CALLSTARTTIMEINGRESSGWACCESS as "Start",
        cdr.CALLSTARTTIMEANSWERED as "Answered",
        cdr.CALLENDTIME as "End",
        cdr.CALLDURATIONCONNECTTODISCONN as "Duration"
    from
        call_detail_record cdr
        left outer join phone_cross_ref pcr on cdr.callingNo = pcr.BASE_PHONE_NO and cdr.CALLINGPARTYE164ADDRESS = pcr.PHONE_NO
        left outer join phone_cross_ref pcr2 on cdr.calledNo = pcr2.BASE_PHONE_NO
    $whereclause
    order by
        $order
    limit $limit
    ENDQUERY

	#print $q->pre("$query");
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    # Get the total duration for this report:
 
    my @fieldsum=();
    my $total_sec=0;
    my @arrayToPrint=@$tbl_ref;
	for (my $x=1; $x<=$#arrayToPrint; $x++) {
        my $a = $arrayToPrint[$x];
		my $time=$$a[-1];
		# Calculate the duration
		my $dur=duration($time);
		$$a[-1]=$dur;
		$arrayToPrint[$x]=$a;
		# Add time to the total time
        $total_sec+=$time;
    }
    my $total_time=duration($total_sec);
    @fieldsum = ('','', '', '', '', '', '', '','Total:', $total_time);
    push (@arrayToPrint,\@fieldsum);

    #
    if (recordCount($tbl_ref)) {
        #printArray(\@header,$tbl_ref);
        my @links = ( "$scriptname?report=UPDATE_ALIAS&referring=CDR&itLimit=$itLimit&number_selection=$number_selection&called_number_selection=$called_number_selection&start_date_selection=$start_date_selection&end_date_selection=$end_date_selection&phoneNum=".'$_' );
        printResult_arrayref2(\@arrayToPrint, \@links, '', '', '');

    } else {
        print $q->b("no records found<br>\n");
    }
    $dbh->disconnect;
    my $gotoreport="CDR";
    if ($itLimit) {
        $gotoreport="CDR-IT";
    }

	print $q->a({-href=>"$scriptname?report=PHONEMENUS", -target=>'_top'}, "Return to Phone Report Menu");
	print "<br />";
    print $q->a({-href=>"$scriptname?report=$gotoreport&number_selection=$number_selection&called_number_selection=$called_number_selection&start_date_selection=$start_date_selection&end_date_selection=$end_date_selection", -target=>'_top'}, "Return to Phone Report");
	standardfooter();

} elsif ($report eq "UPDATE_ALIAS") {
    my $phoneNum   = trim(($q->param("phoneNum"))[0]);
    my $report   = trim(($q->param("referring"))[0]);		# Get the referring report
    my $number_selection   = trim(($q->param("number_selection"))[0]);
    my $called_number_selection   = trim(($q->param("called_number_selection"))[0]);
    my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
    my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);
    my $itLimit   = trim(($q->param("itLimit"))[0]);

	print $q->h2("Update Alias Table:");
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
    unless ($dbh) {
        print "UPDATE_ALIAS: failed to connect to $mysqld<br>";
        exit;
    }
    # Get the current alias for the phone number
	my $query = dequote(<< "    ENDQUERY");
    select
        primary_ref as "Primary Alias",
        secondary_ref as "Secondary Alias"
    from
        phone_cross_ref
    where
        phone_no = '$phoneNum'

    ENDQUERY


    my $tbl_ref = execQuery_arrayref($dbh, $query);
    my $primary_ref;
    my $secondary_ref;
    if (recordCount($tbl_ref)) {
        $primary_ref=$$tbl_ref[1][0];
        $secondary_ref=$$tbl_ref[1][1];
        print $q->p("Present Aliases for phone number: $phoneNum");
        printResult_arrayref2($tbl_ref, '', '', '', '');
    } else {
        print $q->p("No existing Alias for phone number: $phoneNum");
    }

    #
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"phoneNum", -value=>"$phoneNum"}); # Include the phone number
    print $q->input({-type=>"hidden", -name=>"report", -value=>"$report"});
    print $q->input({-type=>"hidden", -name=>"itLimit", -value=>"$itLimit"}); # Go to the POS if set
    print $q->input({-type=>"hidden", -name=>"start_date_selection", -value=>"$start_date_selection"});
    print $q->input({-type=>"hidden", -name=>"end_date_selection", -value=>"$end_date_selection"});
    print $q->input({-type=>"hidden", -name=>"number_selection", -value=>"$number_selection"});
    print $q->input({-type=>"hidden", -name=>"called_number_selection", -value=>"$called_number_selection"});

    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Update Alias")));

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Primary Alias:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->input({-type=>'text', -name=>"primary_ref", -size=>"12", -value=>"$primary_ref"}),

                         )
                  );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Secondary Alias:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->input({-type=>'text', -name=>"secondary_ref", -size=>"12", -value=>"$secondary_ref"}),

                             )
                      );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    $dbh->disconnect;

    standardfooter();

} elsif ($report eq "ISSUES") {
	print $q->h2("Store Issues:");
    my @known_issues=();
    my @current_issues=();
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
    unless ($dbh) {
        print "ISSUES: failed to connect to $mysqld<br>";
        exit;
    }
    # Find known issues
    my $query="select * from storeIssues where status not like 'RESOLVED' order by status,store_id,issue_id";
    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@current_issues,\@tmp);
    }

	my $count=0;
	print "\n\n", $q->start_table({-valign=>'top', -cellspacing=>0, -cols=>2}), "\n";
	my $store_id="";
	my $storename="";
	my $issue="";
	my $status="";
	my $note="";
	my $enterdate="";
	my $updated="";
	my @issue_info=();

	printArray(\@header,\@current_issues);
    print $q->h3("NOTE: You may need to refresh your page to get the current listing!");

    $dbh->disconnect;
    footer();
    standardfooter();

} elsif ($report eq "EDITISSUE") {
	# Edit an issue
	my $errorstr;
	my $issueno   = trim(($q->param("issueno"))[0]);
	my @edit_issue=();
	# Decrement the issue number by one because arrays start counting at 0.
	#$issueno--;
	my $index=($issueno - 1);
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
    my $query="select * from storeIssues where issue_id = $issueno";
    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        @edit_issue=@tmp;
        #push(@current_issues,\@tmp);
    }

	print $q->h2("Selected Store Issue #$issueno:");

	my @row=();
	print "\n", start_table({-class=>'tableborder', -valign=>'top', -border=>'1'}), "\n";
	@row=("Store Number","$edit_issue[1]");
	print Tr(td({-class=>'tableborder', -nowrap=>undef}, \@row) );
	@row=("Store Name","$edit_issue[2]");
	print Tr(td({-class=>'tableborder', -nowrap=>undef}, \@row) );
	@row=("Issue","$edit_issue[3]");
	print Tr(td({-class=>'tableborder', -nowrap=>undef}, \@row) );
	@row=("Status","$edit_issue[4]");
	print Tr(td({-class=>'tableborder', -nowrap=>undef}, \@row) );
	@row=("Date Found","$edit_issue[7]");
	print Tr(td({-class=>'tableborder', -nowrap=>undef}, \@row) );
	# Notes may be long so let's put in a line wrap

	@row=("Notes","$edit_issue[5]");
	print Tr(td({-class=>'tableborder'}, \@row) );
	@row=("Date Updated","$edit_issue[6]");
	print Tr(td({-class=>'tableborder', -nowrap=>undef}, \@row) );
	print end_table(),"\n";
	print "<br>\n";

	print "\n", $q->start_form(-method=>'get', -action=>"$scriptname"), "\n\n";

	print $q->input({-type=>"hidden", -name=>"report", -value=>"UPDATE"});
	print $q->input({-type=>"hidden", -name=>"issueno", -value=>"$issueno"});

	print "Enter Note: ", $q->textfield(-name=>'notes', -value=>'',
				 -size=>'100'), "<br><br>\n";
	print "Select Status:", $q->radio_group(-name=>'status',
				  -values=>["PENDING","RESOLVED","NEW","DELETE"],
				  -default=>"PENDING",
				  -labels=>"STATUS"), "<br><br>\n";

	print $q->submit('Save');
	print $q->end_form, "\n\n";
    $dbh->disconnect;
    footer();
	standardfooter();

} elsif ("$report" eq "UPDATE") {

	# Update the file with the edits made
	my $errorstr="";
	my $issueno   = trim(($q->param("issueno"))[0]);
	my $index=($issueno - 1);
	my $notes   = trim(($q->param("notes"))[0]);
	my $status   = trim(($q->param("status"))[0]);

	my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
	my $query;
	my $sth;
	if ($status eq "DELETE") {
		$query = "delete from storeIssues where issue_id = '$issueno'";
		$sth=$dbh->prepare("$query");
		$sth->execute();
	} else {
		# Now that we have the updated issue, update the database
		$query="update storeIssues set notes = \'$notes\' where issue_id = $issueno";
		$sth=$dbh->prepare("$query");
		$sth->execute();
		$query="update storeIssues set status = \'$status\' where issue_id = $issueno";
		$sth=$dbh->prepare("$query");
		$sth->execute();
	}
    # Re-display the current issues table
    #$query="select * from storeIssues where status not like 'RESOLVED'";
    $query="select * from storeIssues where status not like 'RESOLVED' order by status,store_id,issue_id";
    $sth=$dbh->prepare("$query");
    $sth->execute();
    my @current_issues=();
    while (my @tmp = $sth->fetchrow_array()) {
        push(@current_issues,\@tmp);
    }
	printArray(\@header,\@current_issues);

    $dbh->disconnect;
    footer();
	standardfooter();
} elsif ("$report" eq "QUERY") {
    print $q->h2("Query from the Store Issues Records");
    #############
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"QUERY2"});

    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Filter by...")));

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Num:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"store_id", -size=>"12", -value=>""}))
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Name:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"storename", -size=>"12", -value=>""}))
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Status:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"status", -size=>"12", -value=>""}))
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Notes:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"notes", -size=>"12", -value=>""}))
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Description:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"description", -size=>"12", -value=>""}))
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Date:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"date", -size=>"12", -value=>""}))
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Updated:"),
               $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"update", -size=>"12", -value=>""}))
              );

    print $q->Tr(

        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Select All by Default:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},

        $q->input({ -type=>'checkbox', -name=>"cb_checked",   -value=>"1"  })),

    );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
              );
    print $q->end_table(),"\n";

    print $q->end_form(), "\n";


    ##############
    footer();
    standardfooter();

} elsif ("$report" eq "QUERY2") {
    print $q->h2("Query from the Store Issues Records");
	my $store_id   = trim(($q->param("store_id"))[0]);
	my $storename   = trim(($q->param("storename"))[0]);
	my $status   = trim(($q->param("status"))[0]);
	my $notes   = trim(($q->param("notes"))[0]);
	my $description   = trim(($q->param("description"))[0]);
    my $date   = trim(($q->param("date"))[0]);
    my $update   = trim(($q->param("update"))[0]);
    my $cb_checked   = trim(($q->param("cb_checked"))[0]);
    my $query="";
    my $sth="";
    my $where="";
    my $and="";
    my @query_results=();

    if ($store_id) {
        $where.=" store_id = \'$store_id\'";
        $and="and";
    }
    if (($storename) && (!$store_id)) {
        $where.=" store_name like \'$storename\'";
        $and="and";
    }
    if ($status) {
        $where.=" $and status like \'$status\'";
        $and="and";
    }
    if ($notes) {
        $where.=" $and notes like \'%$notes%\'";
        $and="and";
    }
    if ($description) {
        $where.=" $and description like \'%$description%\'";
        $and="and";
    }
    if ($date) {
        $where.=" $and date like \'$date%\'";
        $and="and";
    }
    if ($update) {
        $where.=" $and update like \'$update%\'";
        $and="and";
    }
    $query="select * from storeIssues where $where";

    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
    $sth=$dbh->prepare("$query");
    $sth->execute();
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    while (my @tmp = $sth->fetchrow_array()) {
        my $issueno=$tmp[0];
        my $status = $tmp[4];
        my $checked=0;
        if ($status =~ /NEW/i) {
            $checked=1;
        }
		if ($cb_checked) {
			$checked=1;
		}
        my $check;
        if ($checked) {
            $check = "<input type=checkbox name=\"cb_$issueno\", checked=$checked>";
        } else {
            $check = "<input type=checkbox name=\"cb_$issueno\">";
        }

        push(@tmp,$check);
        push(@query_results,\@tmp);
    }

    if ($#query_results > -1) {
        printArray(\@header,\@query_results);
		print $q->p("Select update option and click 'SUBMIT'. ");
		print $q->input({-type=>"hidden", -name=>"report", -value=>"BULK_UPDATE_ISSUE"});
		print $q->radio_group(-name=>'status',
				  -values=>["PENDING","RESOLVED","NEW","DELETE"],
				  -default=>"PENDING",
				  -labels=>"STATUS"), "<br><br>\n";
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'C', -value=>"   SUBMIT   "}))
                  );

        print $q->end_form(), "\n";
    } else {
        print $q->p("No results found");
    }

    $dbh->disconnect;
    footer();
    standardfooter();

} elsif ("$report" eq "DELISSUE") {
    print $q->h2("Delete Store Issues");
    # get a list of checked checkboxes
    my %issues;
    foreach my $name ($q->param) {
        next unless $name =~ m/^cb_(\-?\d+)$/;
        $issues{$1} = 1;
    }
    unless (scalar(keys(%issues))) {
        print "<p>No issues checked</p>\n";
        footer();
        standardfooter();
        exit;
    }
    foreach my $issue (sort(keys(%issues))) {
        my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

        $query = "delete from storeIssues where issue_id = '$issue'";

        if ($dbh->do($query)) {
            print "Successfully deleted store issue $issue<br>";
        } else {
            print "Failed to delete store issue $issue<br>";
            #print $dbh->{Name}."<br>\n";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
        }
        $dbh->disconnect;
    }

    footer();
    standardfooter();
} elsif ("$report" eq "BULK_UPDATE_ISSUE") {
	my $status   = trim(($q->param("status"))[0]);

    print $q->h2("Bulk Update Store Issues");
    # get a list of checked checkboxes
    my %issues;
    foreach my $name ($q->param) {
        next unless $name =~ m/^cb_(\-?\d+)$/;
        $issues{$1} = 1;
    }
    unless (scalar(keys(%issues))) {
        print "<p>No issues checked</p>\n";
        footer();
        standardfooter();
        exit;
    }
	my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

	foreach my $issue (sort(keys(%issues))) {
		my $note="updated";
		if ($status eq "DELETE") {
			$query = "delete from storeIssues where issue_id = '$issue'";
			$note="deleted"
		} else {
			$query="update storeIssues set status = '$status' where issue_id = $issue";
		}


		if ($dbh->do($query)) {
			print "Successfully $note store issue $issue<br>";
		} else {
			print "Failed to $note store issue $issue<br>";
			#print $dbh->{Name}."<br>\n";
			print "query=".$query."<br>\n";
			print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
			print "errstr=".$dbh->errstr."<br>\n";
		}

	}

	if (1) {
		# Re-display the current issues table
		#$query="select * from storeIssues where status not like 'RESOLVED'";
		$query="select * from storeIssues where status not like 'RESOLVED' order by status,store_id,issue_id";
		my $sth=$dbh->prepare("$query");
		$sth->execute();
		my @current_issues=();
		while (my @tmp = $sth->fetchrow_array()) {
			push(@current_issues,\@tmp);
		}
		printArray(\@header,\@current_issues);
	}
    $dbh->disconnect;
    footer();
    standardfooter();
} elsif ("$report" eq "CDR-SUM") {
    print $q->h2("Summary Call Data Report");
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");

	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld");
		standardfooter();
		exit;
	}
    #$query = "select distinct STR_TO_DATE(CALLSTARTTIMEINGRESSGWACCESS,'%Y-%m-%d') as Date from call_detail_record order by STR_TO_DATE(CALLSTARTTIMEINGRESSGWACCESS,'%Y-%m-%d')";
	$query = "select distinct(CALLSTARTTIMEINGRESSGWACCESS) as Date from call_detail_record order by CALLSTARTTIMEINGRESSGWACCESS";
	$query = "select distinct(DATE_FORMAT(CALLSTARTTIMEINGRESSGWACCESS, '%Y-%m-%d')) as Date from call_detail_record order by CALLSTARTTIMEINGRESSGWACCESS";
	#$query = "select distinct(DATE_FORMAT(CALLSTARTTIMEINGRESSGWACCESS, '%Y-%m-%d')) as Date from call_detail_record order by CALLSTARTTIMEINGRESSGWACCESS";
    #DATE_FORMAT(NOW(),'%m-%d-%Y')

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    # Find the breaks in the dates
    my @days_list;
    my $last_day;
    for my $datarows (1 .. $#$tbl_ref)
    {
        my $thisrow = @$tbl_ref[$datarows];
        my $date = $$thisrow[0];

        my @tmp=split(/-/,$date);
        my $day_number=$tmp[$#tmp];
        if ($last_day) {
            my $diff = ($day_number - $last_day);
            if ( $diff > 1) {

                $diff--;    # Decrement diff by one to show how many day's worth of data is missing.

                if ($diff > 2) {    # Assume 2 days is a weekend.  Special emphasis if it is more than a typical weekend.
                    print "---------------- $diff ---------------- <br>";
                } else {
                    print "---------------- $diff  <br>";
                }

               # push(@days_list,"-----------");
            }
        }
         print "$date<br>";
        $last_day=$day_number;
    }
    ##

    #printResult_arrayref2(\@days_list);#, \@links, '', '', '');
    $dbh->disconnect;
    standardfooter();

} elsif ("$report" eq "TRAFFICQRY"){

    @ltm=localtime();
    # Use yesterday as the default date
    @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
    my $year=($previous_day_ltm[5]+1900);
    my $this_month=$previous_day_ltm[4];
    $this_month++;
    my $this_day=$previous_day_ltm[3];

    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );
	# Build a list of Stores
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $mssql_dbh = openODBCDriver($dsn, '****', '****');
    my $Active=1;
    my %store_hash;

    foreach my $status ("Company","Contract") {
        $query = 'exec ****Data..tspGetStoreInfo @Code=\'V1\''. ($Active ? ', @Active=1' : '') if ($status eq "Company");
        $query = 'exec ****Data..tspGetStoreInfo @Code=\'V2\''. ($Active ? ', @Active=1' : '') if ($status eq "Contract");
        my $tbl_ref = execQuery_arrayref($mssql_dbh, $query);
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $storeId = trim($$thisrow[0]);
            my $storeName = trim($$thisrow[1]);
            my @tmp=split(/-/,$storeName);
            $storeName=$tmp[1];
            $store_hash{$storeId}=$storeName;
        }
	}

    my $month_options;
    my $day_options;
    my $year_options;
    my $data_options;
	my $store_options=$q->option({-value=>"99 - Akron Lab"}, "99 - Akron Lab");
	$store_options.=$q->option({-value=>"98 - Akron SAP Lab"}, "98 - Akron SAP Lab");
    foreach my $store (sort(keys(%store_hash))) {
        $store_options .= $q->option({-value=>"$store - $store_hash{$store}"}, "$store - $store_hash{$store}");
    }

    
	foreach my $month (sort {$a <=> $b} (keys(%month_hash))) {
        if ($month == $this_month) {
            $month_options .= $q->option({-value=>$month -selected=>'selected'}, "$month_hash{$month}");
        } else {
            $month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
        }

    }
    foreach my $day (1 ... 31) {
        if ($day == $this_day) {
            $day_options .= $q->option({-value=>$day -selected=>'selected'}, "$day");
        } else {
            $day_options .= $q->option({-value=>$day}, "$day");
        }

    }
    $year=$current_year;
    while ($year > 1999) {
        $year_options .= $q->option({-value=>$year}, "$year");
        $year--;
    }

    $data_options .= $q->option({-value=>0}, "Tlog data");
    $data_options .= $q->option({-value=>1}, "Storedata");
    # Get the report criteria:
	print $q->h2("Traffic Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TRAFFIC"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Data Source:"),
               #     $q->td({-class=>'tabledatanb', -align=>'left'},
               #       $q->Select({-name=>"data_selection", -size=>"1"}), $data_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    standardfooter();

} elsif ("$report" eq "TRAFFIC") {
    print $q->h2("Traffic Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
	my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

	$query=dequote(<< "	ENDQUERY");

	SELECT [StoreId]
		  ,[SensorId]
		  ,[StartTime]
		  ,[EndTime]
		  ,[CountIn]
		  ,[CountOut]
		  ,[OriginalCountIn]
		  ,[OriginalCountOut]
	FROM
		[****Data].[dbo].[tblStoreTraffic]
	where
		[StoreId] = '$storeid'
	and
		[StartTime] > '$startdate'
	and
		[EndTime] < '$enddate 23:59:59'
	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {

		printResult_arrayref2($tbl_ref );

	} else {
		print $q->p("Found no data in tblStoreTraffic for $storeid between $startdate and $enddate");
	}
    standardfooter();
} elsif ("$report" eq "SALES_AND_DISCOUNTS"){

    @ltm=localtime();
    # Use yesterday as the default date
    @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
    my $year=($previous_day_ltm[5]+1900);
    my $this_month=$previous_day_ltm[4];
    $this_month++;
    my $this_day=$previous_day_ltm[3];

    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );
	# Build a list of Stores
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $mssql_dbh = openODBCDriver($dsn, '****', '****');
    my $Active=1;
    my %store_hash;

    foreach my $status ("Company","Contract") {
        $query = 'exec ****Data..tspGetStoreInfo @Code=\'V1\''. ($Active ? ', @Active=1' : '') if ($status eq "Company");
        $query = 'exec ****Data..tspGetStoreInfo @Code=\'V2\''. ($Active ? ', @Active=1' : '') if ($status eq "Contract");
        my $tbl_ref = execQuery_arrayref($mssql_dbh, $query);
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $storeId = trim($$thisrow[0]);
            my $storeName = trim($$thisrow[1]);
            my @tmp=split(/-/,$storeName);
            $storeName=$tmp[1];
            $store_hash{$storeId}=$storeName;
        }
	}

    my $month_options;
    my $day_options;
	my $day_one_options;
    my $year_options;
    my $data_options;
	my $basic_store_options;
	my $store_options=$q->option({-value=>"99 - Akron Lab"}, "99 - Akron Lab");
	$store_options.=$q->option({-value=>"98 - Akron SAP Lab"}, "98 - Akron SAP Lab");
    foreach my $store (sort(keys(%store_hash))) {
        $store_options .= $q->option({-value=>"$store - $store_hash{$store}"}, "$store - $store_hash{$store}");
		$basic_store_options .= $q->option({-value=>"$store - $store_hash{$store}"}, "$store - $store_hash{$store}");
    }

	foreach my $month (sort {$a <=> $b} (keys(%month_hash))) {
        if ($month == $this_month) {
            $month_options .= $q->option({-value=>$month -selected=>'selected'}, "$month_hash{$month}");
        } else {
            $month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
        }
    }
    foreach my $day (1 ... 31) {
        if ($day == $this_day) {
            $day_options .= $q->option({-value=>$day -selected=>'selected'}, "$day");
        } else {
            $day_options .= $q->option({-value=>$day}, "$day");			 
        }
	}
    foreach my $day (1 ... 31) {
        if ($day == 1) {
            $day_one_options .= $q->option({-value=>$day -selected=>'selected'}, "$day");
        } else {
            $day_one_options .= $q->option({-value=>$day}, "$day");			 
        }		
    }
    $year=$current_year;
    while ($year > 1999) {
        $year_options .= $q->option({-value=>$year}, "$year");
        $year--;
    }

    $data_options .= $q->option({-value=>0}, "Tlog data");
    $data_options .= $q->option({-value=>1}, "Storedata");
    # Get the report criteria:

	print $q->h2("Discount Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNT"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Data Source:"),
               #     $q->td({-class=>'tabledatanb', -align=>'left'},
               #       $q->Select({-name=>"data_selection", -size=>"1"}), $data_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	
	
	print $q->h2("Net Sales Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"NETSALES2"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Data Source:"),
               #     $q->td({-class=>'tabledatanb', -align=>'left'},
               #       $q->Select({-name=>"data_selection", -size=>"1"}), $data_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Checkbox to split villages and non-villages totals
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Show Only Villages Sales:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},

        $q->input({ -type=>'checkbox', -name=>"company_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	
	
	print $q->h2("Sales and Taxes Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SALESTAXES"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
 
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );
    # Checkbox to allow for detailed reporting.
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Details:"),
        $q->td({-class=>'tabledatanb', -align=>'right'},

        $q->input({ -type=>'checkbox', -name=>"detail_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );
 

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";	
	
	print $q->h2("Sales and Taxes Report II");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SALESTAXESII"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
 
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

 
    # Checkbox to allow for detailed reporting.
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Transaction Details:"),
        $q->td({-class=>'tabledatanb', -align=>'right'},

        $q->input({ -type=>'checkbox', -name=>"detail_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";		

	print $q->h2("Cost of Goods Sold Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"COGS"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
 
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

 
 
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";		
	
    print $q->h2("Transaction Totals by Period Report");
	print $q->p("This report defaults to by month but then specific months can be selected.  Sales tax data is show.");
	print $q->p("Note that Transaction Totals are Net Sales plus any non-merchandise items so these totals may not agree with Net Sales.");
    ### Build the second table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TXNTOTALBYPERIOD"});
	print $q->input({-type=>"hidden", -name=>"period", -value=>"month"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>5}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),


              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );




    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),


              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->h2("Transaction Report");
    ### Build the third table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SALES_TXN"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

	# Add a box to specify a minimum transaction
	print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Minimum Transaction Amt (optional):"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'text', -name=>"tran_amt_selection", -size=>"10", -value=>""})),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),

            );	
	

    # Checkbox to show only transactions with a price override
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Only Price Override Txn:"),
        $q->td({-class=>'tabledatanb', -align=>'right'},

        $q->input({ -type=>'checkbox', -name=>"price_override_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->h2("Transaction Detail");
	### Form to view a specific transaction
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SALES_TXN_DTL"});

	# A text box to enter a specific TranId
	print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "TranId Selection:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'text', -name=>"tranid_selection", -size=>"10", -value=>""})),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),

            );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	print $q->h2("Non Merchandise Sales Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"NONMERCH"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->h2("Sales Tax Check  ");
    ### Build the Fifth table
	$store_options=$q->option({-value=>"All"}, "All Stores");
	$store_options.=$q->option({-value=>"V1"}, "Company Stores");
	$store_options.=$q->option({-value=>"V2"}, "Contract Stores");
	$store_options.=$q->option({-value=>"99 - Akron Lab"}, "99 - Akron Lab");
    foreach my $store (sort(keys(%store_hash))) {
        $store_options .= $q->option({-value=>"$store - $store_hash{$store}"}, "$store - $store_hash{$store}");
    }
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SALES_TAX_CHECK"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	

    print $q->h2("Transaction Summary Report");
    ### This allows for checking all stores, company, or contract.
	my $store_options_summary=$q->option({-value=>"All"}, "All Stores");
	$store_options_summary.=$q->option({-value=>"V1"}, "Company Stores");
	$store_options_summary.=$q->option({-value=>"V2"}, "Contract Stores");	
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SALES_TXN_SUMMARY"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Group:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options_summary),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

	# Add a box to specify a minimum transaction
	print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Minimum Transaction Amt (optional):"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'text', -name=>"tran_amt_selection", -size=>"10", -value=>""})),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),

            );	
	

    # Checkbox to show only basic summary
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Only Basic Summary:"),
        $q->td({-class=>'tabledatanb', -align=>'right'},

        $q->input({ -type=>'checkbox', -name=>"summary_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";	
	
	# Price Override
	print $q->h2("Price Override Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PROVERRIDE"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

	# Tender Report
	print $q->h2("Tender Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TENDERRPT"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

	print $q->h2("Gift Card Issue Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"GIFTCARD"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

	print $q->h2("Store Valuation Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREVALUATION"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

	print $q->h2("SKU Sales History Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKUSALE"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    # SKU
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"sku_selection", -size=>"12", -value=>""})),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

    # Checkbox to allow selecting summary only.
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Summary Only:"),
        $q->td({-class=>'tabledatanb', -align=>'right'},

        $q->input({ -type=>'checkbox', -name=>"summary_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );			  

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

	print $q->h2("Returns Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"RETURNS"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );


    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	

	print $q->h2("Discount Coupon Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNTCOUPON"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Data Source:"),
               #     $q->td({-class=>'tabledatanb', -align=>'left'},
               #       $q->Select({-name=>"data_selection", -size=>"1"}), $data_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );

	# A text box to enter a specific coupon
	print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Coupon Selection:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'text', -name=>"coupon_selection", -size=>"10", -value=>""})),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),

            );
    # Checkbox to allow selecting any coupon code.
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Any Coupon Code:"),
        $q->td({-class=>'tabledatanb', -align=>'right'},

        $q->input({ -type=>'checkbox', -name=>"any_coupon_selection",   -value=>"1"  })),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	
	
	
	print $q->h2("Manager Code Report");
	my %manager_code_hash=(
		'2' => 'Peripheral Setup',
		'4' => 'Configuration Summary',
		'6' => 'Define Terminal',
		'8' => 'Communication Diagnostics',
		'108' => 'Manual Tax %',
		'109' => 'Manual Tax $',
		'111' => 'Tax Modify for Tax1',
		'112' => 'Tax Modify for Tax2',
		'162' => 'Gift Receipt',
		'168' => 'View Current Transaction',    
		'170' => 'Clock In',
		'172' => 'Clock Out',
		'175' => 'Suspend/Resume',
		'180' => 'Price Override',
		'201' => 'No Sale',
		'202' => 'Item Void',
		'206' => 'Transaction Cancel',
		'210' => 'Return Sale',
		'211' => 'Return Sale W/O Receipt',
		'230' => 'Loans',
		'231' => 'Pick Up',
		'236' => 'Paid Out',
		'240' => 'Balance Terminal',    
		'244' => 'Open Store',
		'246' => 'Open Terminal',
		'249' => 'Close Terminal',
		'250' => 'End of Day',
		'260' => 'Manual Sale Authorization',
		'262' => 'Manual Return Authorization',
		'270' => 'Print Authorization Summary',
		'273' => 'Print Authorization Detail',
		'320' => 'Sales Report',
		'321' => 'Flash Sales Report',
		'322' => 'Tender Report',
		'330' => 'Sales Analysis',
		'402' => 'Start Training Mode',
		'403' => 'End Training Mode',
		'424' => 'Reset Business Date',				
	);
    my $mgrcode_options = $q->option({-value=>'0' -selected=>undef}," ");    
	foreach my $code (sort keys(%manager_code_hash)) {
		my $desc=$manager_code_hash{$code};
		 	
		$mgrcode_options .= $q->option({-value=>"$code|$desc"}, "$code - $desc");
	}   	
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"MCRPT"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $basic_store_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Manager Code:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"mcode_selection", -size=>"1"}), $mgrcode_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );			  

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_one_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";	
    standardfooter();
	
} elsif ("$report" eq "COGS") {	
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
    print $q->h2("Cost of Goods Sold Report"); 
    my @tmp=split(/-/,$store_selection);
    my $storeid=$tmp[0];
    my $storename=$tmp[1];
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	my $debug_table   = trim(($q->param("debug_table"))[0]);
 
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
	my $enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$enddate')";	
	my $cost_enddate_where = "AND dis.TranDate < dateadd( d, 1, '$enddate')";
	my $display_method="by_month";
	if (($start_month_selection == $end_month_selection) && ($start_year_selection == $end_year_selection)) {
		$display_method="by_day";
	}
 
	if ($date_selection) {	
		# A particular date has been specified
		my $year=substr($date_selection,0,4);
		my $month=substr($date_selection,5,2);
		my $day=substr($date_selection,8,2);
		if ($day) {	
			$display_method="by_txn";		
		} elsif ($month) {		
			$display_method="by_day";
			$day="01"		
		}
 
		$start_month_selection = $month;
		$start_year_selection = $year;
		$start_day_selection = $day;
		$startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
		print $q->h4("$store_selection - $start_year_selection-$start_month_selection ");
 
	} else {
		print $q->h4("$store_selection from $startdate to $enddate");	
	}

	if ($date_selection) {
		# If date_selection, then a particular month has been selected
		# so the end date is one month from the start date.
		if ($display_method eq "by_day") {
			$enddate_where = "and tm.TranDateTime < dateadd( m, 1, '$startdate')";
			$cost_enddate_where = "AND dis.TranDate < dateadd( m, 1, '$startdate')";		
		} 
		if ($display_method eq "by_txn") {
			$enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$startdate')";
			$cost_enddate_where = "AND dis.TranDate < dateadd( d, 1, '$startdate')";
			$cost_enddate_where='';
		} 		
	}
 
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
 
	
	$query = dequote(<< "	ENDQUERY");	 
		-- Get the Sales
		select 
			convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
			ms.[ExtSellPrice],
			case 
				when ms.[ExtSellPrice] < 0 then 
					(ms.[Quantity] * -1) 
				else
					ms.[Quantity]
			end as Qty,
			ms.[SKU],
			dis.[AvgCost] as Cost,
			tm.TranNum,
			tm.TranModifier,
			tm.IsPostVoided,
			tm.TranVoid,
			tm.TranId
			
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 
			left outer join [dbWarehouse].[dbo].[tblDailyInventorySkuSummary] dis on ms.SKU = dis.SKU
				and tm.StoreId = dis.[StoreId]
				and convert(Date, convert(varchar(10),tm.TranDateTime,21)) = convert(Date, convert(varchar(10),dis.TranDate,21))			
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and  
				tm.TranVoid = 0 and 
				--tm.IsPostVoided = 0 and
				--tm.TranModifier not in (2,6) and 
				tm.StoreId in ('$storeid')   
		ORDER BY
			ms.SKU
 		 
			;	 
  	
	ENDQUERY
			 
	#print $q->pre("$query");
 
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	if ($display_method eq "by_txn") {	
		print $q->p("Display $display_method");
		my @header = ("Date","SKU","Qty","Price","UnitCost","ExtCost","Margin");
		my @array_to_print = \@header;
				
		my @totals=("Total",0,0,0);
		my @totals_header = ("","Records","Qty","TotalPrice","TotalCost","Margin");
		my @totals_to_print = \@totals_header;		
		my $sales_count=0;
		my %cogs_hash;
		my @tmp_header = ("Txn","SKU","Qty","ExtPrice","ExtCost","Record");
		my @tmp_to_print = \@tmp_header;	
		my @tmp_totals = ("Total","","","","","");
		my $record_count=0;
		while (my @tmp = $sth->fetchrow_array()) {
			my $date = $tmp[0];
			my $price = $tmp[1];
			my $qty = sprintf("%d",$tmp[2]);
			my $sku = $tmp[3]; 		
			my $cost = sprintf("%.2f",$tmp[4]); 
			my $TranNum = $tmp[5];
			my $TranModifier = $tmp[6]; 
			my $IsPostVoided = $tmp[7]; 			
			my $TranVoid = $tmp[8]; 
			my $TranId = $tmp[9]; 
			# If the TranModifier is 2, this is a Layaway Sale
			# A TranModifier of 6 is a Layaway Cancel
			# Either of these means the price is zero'd out.  (This to match the CoGS report in The Village Elder
			if (($TranModifier == 2) || ($TranModifier == 6)) {
				$price = 0;
			}		
			if ($TranModifier == 4) {
				# This is a layaway pickup				 
				#getCostForLayaway($sku,$TranId,$date,$storeid);				 
				$cost = getCostForLayaway($sku,$TranId,$date,$storeid);				
			}
			if ($IsPostVoided) {
				$price = 0;
				$qty = 0;
			}			
			# If this is a rug SKU, the cost is calculated as .75 of the price
			if ($sku == 1199) {
				$cost = ($price * .75);
			}
			# If the cost is less than reasonable
			if ($cost < ($price * .2)) {
				print "Test Point 0: sku ($sku) price ($price) cost ($cost)<br />";
			}
			my $extcost = sprintf("%.2f",($qty * $cost)); 
			my $count = $tmp[5];
			my %sku_hash;
			if ($cogs_hash{$date}) {
				my $ref=$cogs_hash{$date};
				%sku_hash=%$ref;
			}
			$sku_hash{$sku} -> [0] += $price;	
			$sku_hash{$sku} -> [1] += $qty;
			$sku_hash{$sku} -> [2] = $cost;	
			$sku_hash{$sku} -> [3] ++;	
			$record_count++;	
			my $extcost=($cost * $qty);
			my $extprice=($price * $qty);
			my @array = ("$TranNum","$sku","$qty","$extprice","$extcost","$record_count");
			push(@tmp_to_print,\@array);
			$tmp_totals[2]+=$qty; 			
			$tmp_totals[3]+=$extprice;
			$tmp_totals[4]+=$extcost;			
			$cogs_hash{$date} = \%sku_hash;			
			$sales_count++;		 
		}	
		push(@tmp_to_print,\@tmp_totals);
		foreach my $date (sort keys(%cogs_hash)) {
			my $ref=$cogs_hash{$date};
			my %sku_hash=%$ref;		
			foreach my $sku (sort keys(%sku_hash)) {
				my $price = $sku_hash{$sku} -> [0];
				my $qty = $sku_hash{$sku} -> [1];
				my $cost = $sku_hash{$sku} -> [2];				
				my $records = $sku_hash{$sku} -> [3];
				my $extcost = ($qty * $cost);
				my $margin = sprintf("%.2f",($price - $extcost));
				# if Qty is negative, then it is a return.  The extcost calculated above
				# will be negative as the result of being multiplied by qty.
				# The cost will get changed to negative below.				
				if ($qty < 0) {
					$cost *= -1;
				}
				my @array=("$date","$sku","$qty","$price","$cost","$extcost","$margin");
				push(@array_to_print,\@array);	
				$totals[1]+=$records;
				$totals[2]+=$qty;
				$totals[3]+=$price;				
				$totals[4]+=$extcost;
				$totals[5]+=$margin;					
			}
		}		
		push(@totals_to_print,\@totals);
 
		if ($sales_count) {
			if ($debug_table) {
				my @format = (
					'','','',
					{'align' => 'Right', 'currency' => ''},
					{'align' => 'Right', 'currency' => ''},	 
 
				);				
				print $q->h4("Debug Table");				
				printResult_arrayref2(\@tmp_to_print, '', \@format, '', '');	
				print "<br />";
				print "<br \>";				
			}	
			my @format = (
				'','','',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	 
				{'align' => 'Right', 'currency' => ''},	 
				{'align' => 'Right', 'currency' => ''},	
			);	
			printResult_arrayref2(\@array_to_print, '', \@format, '', '');
			@format = (
				'', '', 
				{'align' => 'Right', 'currency' => ''},			 	 
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
			);	
			#printResult_arrayref2(\@totals_to_print, '', \@format, '', '');	
			print "<br />";
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Data Compilation Results:")));
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Records:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, $totals[1])
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("QTY:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, $totals[2])
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Ext Price:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, formatCurrency($totals[3]) )
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Ext Cost:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, formatCurrency($totals[4]) )
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Margin:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, formatCurrency($totals[5]) )
					  );
			print $q->end_table(),"\n";	
			print "<br />";
			print "<br \>";
			unless ($debug_table) {	
				print $q->a({-href=>"$scriptname?report=COGS&store_selection=$store_selection&date_selection=$date_selection&debug_table=1", -target=>'_top'}, "See Debug Table");			
				#print $q->a({-href=>"$scriptname?report=COGS&store_selection=$store_selection&start_month_selection=$start_month_selection&start_day_selection=$start_day_selection&start_year_selection=$start_year_selection&end_month_selection=$end_month_selection&end_day_selection=$end_day_selection&end_year_selection=$end_year_selection&debug_table=1", -target=>'_top'}, "See Debug Table");
				print "<br \>";	
				print "<br />";
			}			
		} else {
			print $q->h4("No Sales found");
		}	

	} else {
		print $q->p("Display $display_method");
		# By Day or By Month
		my @header = ("Date","Records","Qty","Price","ExtCost","Margin");
		my @array_to_print = \@header;	
		my @totals=("Total",0,0,0);
		my @totals_header = ("","Records","Qty","TotalPrice","TotalCost","Margin");
		my @totals_to_print = \@totals_header;		
		my $sales_count=0;
		my %cogs_hash;
		my %sku_hash;
		my @tmp_header = ("SKU","Price","Qty","Cost","ExtCost");
		my @tmp_to_print = \@tmp_header;
 
		while (my @tmp = $sth->fetchrow_array()) {		
			my $date = substr($tmp[0],0,7); 
			if ($display_method eq "by_day") {
				# Include the day in the date.
				$date = substr($tmp[0],0,10);
			}
			my $price = $tmp[1]; 		
			my $qty = sprintf("%d",$tmp[2]);
			my $sku = $tmp[3]; 		
			my $cost = sprintf("%.2f",$tmp[4]); 
			my $TranModifier = $tmp[6];
			my $IsPostVoided = $tmp[7]; 			
			my $TranVoid = $tmp[8]; 
			my $TranId = $tmp[9]; 			
			# If the TranModifier is 2, this is a Layaway Sale
			# A TranModifier of 6 is a Layaway Cancel
			# Either of these means the price is zero'd out.  (This to match the CoGS report in The Village Elder
			if (($TranModifier == 2) || ($TranModifier == 6)) {
				$price = 0;
			}
			#
			if ($TranModifier == 4) {
				# This is a layaway pickup				 
				#getCostForLayaway($sku,$TranId,$date,$storeid);				 
				$cost = getCostForLayaway($sku,$TranId,$date,$storeid);				
			}			
			if ($IsPostVoided) {
				$price = 0;
				$qty = 0;
			}
			# If this is a rug SKU, the cost is calculated as .75 of the price
			if ($sku == 1199) {
				$cost = ($price * .75);
			}			
			my $extcost = sprintf("%.2f",($qty * $cost));
			# if Qty is negative, then it is a return.  The extcost calculated above
			# will be negative as the result of being multiplied by qty.
			# The cost will get changed to negative below.
			if ($qty < 0) {
				$cost *= -1;		 
			}		
		 	
			$cogs_hash{$date} -> [0] += $price;	
			$cogs_hash{$date} -> [1] += $qty;
			$cogs_hash{$date} -> [2] += $extcost;	
			$cogs_hash{$date} -> [3] += 1; 

			$sku_hash{$sku} -> [0] += $price;	
			$sku_hash{$sku} -> [1] += $qty;
			$sku_hash{$sku} -> [2] = $cost;	
			
			
			#$cogs_hash{$date} -> [0] +=$extcost;			
			#$cogs_hash{$date}+=$extcost;
			 
			$sales_count++;		 
		}
		if ($debug_table) {
			foreach my $sku (sort keys(%sku_hash)) {
				my $price = $sku_hash{$sku} -> [0];
				my $qty = $sku_hash{$sku} -> [1];
				my $cost = $sku_hash{$sku} -> [2];
				my $extcost=($qty * $cost);
				my @array=("$sku","$price","$qty","$cost","$extcost");
				push(@tmp_to_print,\@array);
			}
		}
		foreach my $date (sort keys(%cogs_hash)) {
			my $price = $cogs_hash{$date} -> [0];
			my $qty = $cogs_hash{$date} -> [1];
			my $extcost = $cogs_hash{$date} -> [2];
			my $records = $cogs_hash{$date} -> [3];
			my $margin = sprintf("%.2f",($price - $extcost));
			#my $extcost = ($qty * $cost);
			# if Qty is negative, then it is a return.  The extcost calculated above
			# will be negative as the result of being multiplied by qty.
			# The cost will get changed to negative below.				
			if ($qty < 0) {
				#$cost *= -1;
			}
			my @array=("$date","$records","$qty","$price","$extcost","$margin");
			push(@array_to_print,\@array);	 				
			$totals[1]+=$records;
			$totals[2]+=$qty;
			$totals[3]+=$price;
			$totals[4]+=$extcost;
			$totals[5]+=$margin;			
			
			#my $extcost = $cogs_hash{$date};
			#$totals[1]+=$extcost;
			#my @array=("$date","$extcost");
			#push(@array_to_print,\@array);
		}
		push(@totals_to_print,\@totals);	  
	 
		if ($sales_count) {
			my @format = (
				'', '','',
				{'align' => 'Right', 'currency' => ''},				 	 
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},	
			);	
			my @links = ("$scriptname?report=COGS&store_selection=$store_selection&date_selection=".'$_' ); 
			printResult_arrayref2(\@array_to_print, \@links, \@format, '', '');	
			if ($debug_table) {
				print $q->h4("Debug Table");				
				printResult_arrayref2(\@tmp_to_print, '', '', '', '');	
				print "<br \>";	
				print "<br \>";	
			}
			print "<br />";
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Data Compilation Results:")));
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Records:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, $totals[1])
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("QTY:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, $totals[2])
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Ext Price:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, formatCurrency($totals[3]) )
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Ext Cost:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, formatCurrency($totals[4]) )
					  );
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'right'}, $q->b("Margin:")),
					   $q->td({-class=>'tabledata', -align=>'right'}, formatCurrency($totals[5]) )
					  );
			print $q->end_table(),"\n";	
			print "<br />";
			print "<br />";	
			unless ($debug_table) {
				print $q->a({-href=>"$scriptname?report=COGS&store_selection=$store_selection&start_month_selection=$start_month_selection&start_day_selection=$start_day_selection&start_year_selection=$start_year_selection&end_month_selection=$end_month_selection&end_day_selection=$end_day_selection&end_year_selection=$end_year_selection&debug_table=1", -target=>'_top'}, "See Debug Table");
				#print $q->a({-href=>"$scriptname?report=COGS&store_selection=$store_selection&date_selection=$date_selection&debug_table=1", -target=>'_top'}, "See Debug Table");
				print "<br \>";	
				print "<br />";
			}				
		} else {
			print $q->h4("No Sales found");
		}		
	}
 
	 
	$dbh->disconnect;	
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();	

} elsif ("$report" eq "SALESTAXES") {
		# In this report, I want to create a Sales Summary Report much like what is created by the Net Sales report
	# in The Elder.  This means we want the following:
	# Section #A - Gross Merchandise Sales
	# 1) Taxable Items:  			Count and Amount
	# 2) Nontaxable Items:  		Count and Amount
	# 3) Total Merchandise Sales: 	Count and Amount (The sum of #1 & #2 above)
	# Section #B - Sales Tax
	# 1) Tax#1:		Count and Amount
	# 2) Tax#2:		Count and Amount
	# 3) Tax#X:		Count and Amount
	# Section #C - Reductions to Gross Sales
	# Returns:
	# 1) Taxable Items:		Count and Amount
	# 2) NonTaxable Items: 	Count and Amount
	# 3) Total:				Count and Amount
	# Discounts:
	# 1) Taxable Items:		Count and Amount
	# 2) NonTaxable Items: 	Count and Amount
	# 3) Total:				Count and Amount	
	#    Total Reductions	Count and Amount
	# Net Sales
	#    Taxable Items:		Count and Amount
	#    NonTaxable Items:  Count and Amount
	#	 Total:				Count and Amount
	#
	
    print $q->h2("Sales and Taxes Report");
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
 
    my @tmp=split(/-/,$store_selection);
    my $storeid=$tmp[0];
    my $storename=$tmp[1];
	
	my $detail_selection   = trim(($q->param("detail_selection"))[0]);
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
 
	# Don't allow too large of a time span
	if ($end_year_selection != $start_year_selection) {
		print $q->h4("Please restrict your query to one year at a time.");
		standardfooter();
		exit;
		
	}
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	my $display_method = 0;
 
	my $primary_select="convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate";
	my $final_primary_select="tms.TranDate";
	my $enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$enddate')";
	my $group_by = "group by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $order_by = "order by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $final_order_by = "order by tms.TranDate";
	my $final_join = "left outer join #tmpNonMerchSales nms on tms.TranDate = nms.TranDate
			left outer join #tmpTaxTotals ttt on tms.TranDate = ttt.TranDate
			left outer join #tmpDiscount td on tms.TranDate = td.TranDate";
	if ($date_selection) {		
		my $year=substr($date_selection,0,4);
		my $month=substr($date_selection,5,2);
		my $day=substr($date_selection,8,2);
		if ($day) {
			$display_method="by_txn";	
			$startdate="$year"."-"."$month"."-"."$day";
			$enddate = $startdate;
			$enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$startdate')";	
			$primary_select="tm.TranId";
			$final_primary_select="tms.TranId";
			$group_by = "group by tm.TranId";
			$order_by = "order by tm.TranId";
			$final_order_by = "order by tms.TranId";
			$final_join = "left outer join #tmpNonMerchSales nms on tms.TranId = nms.TranId
			left outer join #tmpTaxTotals ttt on tms.TranId = ttt.TranId
			left outer join #tmpDiscount td on tms.TranId = td.TranId";			
		} elsif ($month) {
		
			$display_method="by_day"; 
			$startdate="$year"."-"."$month"."-"."01";
			$enddate_where = "and tm.TranDateTime < dateadd( m, 1, '$startdate')"; 
			$enddate = "End of Month";
 			
		}	 
	} else {		
		if ($start_month_selection eq $end_month_selection) {			
			if ($start_year_selection eq $end_year_selection) {
				$display_method="by_day";				
				 
			} else {
				$display_method="by_month";					
			}
		} else {			
			$display_method="by_month";								
		}
	}	
 
	print $q->h3("Store: $store_selection - $startdate to $enddate");
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
 	# Query #1 - Find the Net Sales
	$query=dequote(<< "	ENDQUERY");		 
		select 
			--ms.[Quantity] as QtySold,
			convert(int,sign(ms.ExtOrigPrice) * ms.Quantity) as QtySold,
			ms.[ExtSellPrice] as SellAmt,
			ms.[ExtRegPrice] as RegAmt, 		
			ms.[ExtOrigPrice] as OrigAmt,
			ms.[DiscountFlag] as Discounted,
			ms.TranId as TranId,
			tm.TranNum as Txn,
			ms.[PriceOverride] as OverRide
			--sum(ms.[ExtPrePromoPrice]) as SellAmt 
 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			--and
				--(ms.[ExtSellPrice] > 0
				--or
				--ms.[ExtSellPrice] = 0)
			and 
				(ms.Tax1 = '1'
				or
				ms.Tax2 = '1'
				or
				ms.Tax3 = '1'
				or
				ms.Tax4 = '1'
				or
				ms.Tax5 = '1')
 			 
			;
			
	ENDQUERY
	
	#print $q->pre("$query");
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	my @current_issues=();
	
	my %summary_hash;
	my %sales_tax_hash;
	my %transaction_hash;
	my %txn_hash;
	while (my @tmp = $sth->fetchrow_array()) {
			my $QtySold=$tmp[0];
			my $SellAmt=$tmp[1];
			my $RegAmt=$tmp[2];
			my $OrigAmt=$tmp[3];
			my $Discounted=$tmp[4]; 
			my $TranId=$tmp[5];
			my $Txn=$tmp[6];
			my $PriceOverride=$tmp[7];			
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'QtySold'}+=$QtySold;
			$sales_tax_hash{'SellAmt'}+=$SellAmt;
=pod			
			if ($Discounted) {			 
				$sales_tax_hash{'SellAmt'}+=$OrigAmt;
				$transaction_hash{$TranId} -> [1] += $OrigAmt;
			} else {
				$sales_tax_hash{'SellAmt'}+=$SellAmt;
				$transaction_hash{$TranId} -> [1] += $SellAmt;
			}
=cut		
=pod	
			if ($PriceOverride) {
				$sales_tax_hash{'SellAmt'}+=$SellAmt;
				$transaction_hash{$TranId} -> [1] += $SellAmt;			
			} else {
				# 2014-09-26 - Want the RegAmt for the Gross Amount
				$sales_tax_hash{'SellAmt'}+=$RegAmt;
				$transaction_hash{$TranId} -> [1] += $RegAmt;
			}
=cut			
			$sales_tax_hash{'RegAmt'}+=$RegAmt;
			$transaction_hash{$TranId} -> [0] += $QtySold;	
			$transaction_hash{$TranId} -> [1] += $SellAmt;				
			$transaction_hash{$TranId} -> [2] += $RegAmt;
			
			 
	}	
	# Query #2
	$query=dequote(<< "	ENDQUERY");		
		select		
 		
			ms.[Quantity] as QtyReturned,
			ms.[ExtRegPrice] as ReturnedAmt,
			ms.TranId as TranId,
			tm.TranNum as Txn	
	 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				ms.[ExtSellPrice] < 0				
			and 
				(ms.Tax1 = '1'
				or
				ms.Tax2 = '1'
				or
				ms.Tax3 = '1'
				or
				ms.Tax4 = '1'
				or
				ms.Tax5 = '1')		
 
			;
	ENDQUERY
	#print $q->pre("$query");	
	my $sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $QtyReturned=$tmp[0];
			my $ReturnedAmt=$tmp[1];
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];	
			$txn_hash{$TranId}=$Txn;			
			$sales_tax_hash{'QtyReturned'}+=$QtyReturned;
			$sales_tax_hash{'ReturnedAmt'}+=$ReturnedAmt;	 
			$transaction_hash{$TranId} -> [3] += $QtyReturned;
			$transaction_hash{$TranId} -> [4] += $ReturnedAmt;			
			 
	}	
	# Query #3	Non Taxable Net Sales
	$query=dequote(<< "	ENDQUERY");		
		select
			convert(int,sign(ms.ExtOrigPrice) * ms.Quantity) as QtySold,
			ms.[ExtSellPrice] as SellAmt,		
			--ms.[Quantity] as QtySoldNT,
			--ms.[ExtRegPrice] as SellAmtNT,
			ms.TranId as TranId,
			tm.TranNum as Txn 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			--and
				--(ms.[ExtSellPrice] > 0
				--or
				--ms.[ExtSellPrice] = 0)
			and 
				(ms.Tax1 = '0'
				and
				ms.Tax2 = '0'
				and
				ms.Tax3 = '0'
				and
				ms.Tax4 = '0'
				and
				ms.Tax5 = '0')
 
			;
	ENDQUERY
	#print $q->pre("$query");	
	my $sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $QtySoldNT=$tmp[0];
			my $SellAmtNT=$tmp[1];
			my $TranId=$tmp[2];
			my $Txn=$tmp[3];	
			$txn_hash{$TranId}=$Txn;			
			$sales_tax_hash{'QtySoldNT'}+=$QtySoldNT;
			$sales_tax_hash{'SellAmtNT'}+=$SellAmtNT;	 
			$transaction_hash{$TranId} -> [5] += $QtySoldNT;
			$transaction_hash{$TranId} -> [6] += $SellAmtNT;
		 			
	}		
	# Query #4
	$query=dequote(<< "	ENDQUERY");	
		select		
 	
			ms.[Quantity] as QtyReturnedNT,
			ms.[ExtRegPrice] as ReturnedAmtNT,
			ms.TranId as TranId,
			tm.TranNum as Txn			 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				ms.[ExtSellPrice] < 0				
			and 
				(ms.Tax1 = '0'
				and
				ms.Tax2 = '0'
				and
				ms.Tax3 = '0'
				and
				ms.Tax4 = '0'
				and
				ms.Tax5 = '0')		
 
			;
	ENDQUERY
	#print $q->pre("$query");	
	my $sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $QtyReturnedNT=$tmp[0];
			my $ReturnedAmtNT=$tmp[1];
			my $TranId=$tmp[2];
			my $Txn=$tmp[3];	
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'QtyReturnedNT'}+=$QtyReturnedNT;
			$sales_tax_hash{'ReturnedAmtNT'}+=$ReturnedAmtNT;	 
			$transaction_hash{$TranId} -> [7] += $QtyReturnedNT;
			$transaction_hash{$TranId} -> [8] += $ReturnedAmtNT;						
	}			
	# Query #5
	$query=dequote(<< "	ENDQUERY");	
		-- Collect taxes1
		select		
 
			tt.[Sequence] as TaxQty1,
			tt.[TaxAmount] as TaxAmt1,
			tt.[TaxableAmount] as TaxAbleAmt1,
			tm.TranId as TranId,
			tm.TranNum as Txn					 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 1
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	my $sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			#my $TaxQty1=$tmp[0];
			my $TaxQty1++;
			my $TaxAmt1=$tmp[1];
			my $TaxAbleAmt1=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty1'}+=$TaxQty1;
			$sales_tax_hash{'TaxAmt1'}+=$TaxAmt1;
			$sales_tax_hash{'TaxAbleAmt1'}+=$TaxAbleAmt1;				
			$transaction_hash{$TranId} -> [9] += $TaxQty1;
			$transaction_hash{$TranId} -> [10] += $TaxAmt1;
			$transaction_hash{$TranId} -> [11] += $TaxAbleAmt1;			
	}		
	# Query #6
	$query=dequote(<< "	ENDQUERY");			
		-- Collect taxes2
		select		
 	
			tt.[Sequence] as TaxQty2,
			tt.[TaxAmount] as TaxAmt2,
			tt.[TaxableAmount] as TaxAbleAmt2,
			tm.TranId as TranId,
			tm.TranNum as Txn				
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 2
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty2++;
			my $TaxAmt2=$tmp[1];
			my $TaxAbleAmt2=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty2'}+=$TaxQty2;
			$sales_tax_hash{'TaxAmt2'}+=$TaxAmt2;
			$sales_tax_hash{'TaxAbleAmt2'}+=$TaxAbleAmt2;				
			$transaction_hash{$TranId} -> [12] += $TaxQty2;
			$transaction_hash{$TranId} -> [13] += $TaxAmt2;
			$transaction_hash{$TranId} -> [14] += $TaxAbleAmt2;			
	}		
	# Query #7
	$query=dequote(<< "	ENDQUERY");		
		-- Collect taxes3
		select		
		 
			tt.[Sequence] as TaxQty3,
			tt.[TaxAmount] as TaxAmt3,
			tt.[TaxableAmount] as TaxAbleAmt3,
			tm.TranId as TranId,
			tm.TranNum as Txn			
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 3
 		 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty3++;
			my $TaxAmt3=$tmp[1];
			my $TaxAbleAmt3=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty3'}+=$TaxQty3;
			$sales_tax_hash{'TaxAmt3'}+=$TaxAmt3;
			$sales_tax_hash{'TaxAbleAmt3'}+=$TaxAbleAmt3;				
			$transaction_hash{$TranId} -> [15] += $TaxQty3;
			$transaction_hash{$TranId} -> [16] += $TaxAmt3;
			$transaction_hash{$TranId} -> [17] += $TaxAbleAmt3;			
	}		
	# Query #8
	$query=dequote(<< "	ENDQUERY");				

		-- Collect taxes4
		select		
 
			tt.[Sequence] as TaxQty4,
			tt.[TaxAmount] as TaxAmt4,
			tt.[TaxableAmount] as TaxAbleAmt4,
			tm.TranId as TranId,
			tm.TranNum as Txn					
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 4
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty4++;
			my $TaxAmt4=$tmp[1];
			my $TaxAbleAmt4=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty4'}+=$TaxQty4;
			$sales_tax_hash{'TaxAmt4'}+=$TaxAmt4;
			$sales_tax_hash{'TaxAbleAmt4'}+=$TaxAbleAmt4;				
			$transaction_hash{$TranId} -> [18] += $TaxQty4;
			$transaction_hash{$TranId} -> [19] += $TaxAmt4;
			$transaction_hash{$TranId} -> [20] += $TaxAbleAmt4;			
	}			
	# Query #9
	$query=dequote(<< "	ENDQUERY");				

		-- Collect taxes5
		select					
			tt.[Sequence] as TaxQty5,
			tt.[TaxAmount] as TaxAmt5,
			tt.[TaxableAmount] as TaxAbleAmt5,
			tm.TranId as TranId,
			tm.TranNum as Txn						 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 5
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty5++;
			my $TaxAmt5=$tmp[1];
			my $TaxAbleAmt5=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty5'}+=$TaxQty5;
			$sales_tax_hash{'TaxAmt5'}+=$TaxAmt5;
			$sales_tax_hash{'TaxAbleAmt5'}+=$TaxAbleAmt5;				
			$transaction_hash{$TranId} -> [21] += $TaxQty5;
			$transaction_hash{$TranId} -> [22] += $TaxAmt5;
			$transaction_hash{$TranId} -> [23] += $TaxAbleAmt5;			
	}			
	# Query #10
	if (0) {
	$query=dequote(<< "	ENDQUERY");				
	
		-- collect discounts
		select
 
			td.[DiscountQty] as DisQty,
			td.[ExtDiscountAmt] as DisAmt,
			tm.TranId as TranId,
			tm.TranNum as Txn			
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblDiscounts]td on tm.TranId=td.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and 
				(td.Tax1 = '1'
				or
				td.Tax2 = '1'
				or
				td.Tax3 = '1'
				or
				td.Tax4 = '1'
				or
				td.Tax5 = '1')	 
 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $DisQty=$tmp[0];
			my $DisAmt=$tmp[1]; 
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'DisQty'}+=$DisQty;
			$sales_tax_hash{'DisAmt'}+=$DisAmt;	
 
			$transaction_hash{$TranId} -> [24] += $DisQty;
			$transaction_hash{$TranId} -> [25] += $DisAmt;			 		
	}	
	}
	# Query #11
	$query=dequote(<< "	ENDQUERY");				
			
		-- Discount from Merchandise Sale table
		
		select
 
			ms.[Quantity] as DisQty,
			(ms.[ExtRegPrice] - ms.[ExtSellPrice]) as DisAmt,
			tm.TranId as TranId,
			tm.TranNum as Txn				
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
 
			and 
				(ms.Tax1 = '1'
				or
				ms.Tax2 = '1'
				or
				ms.Tax3 = '1'
				or
				ms.Tax4 = '1'
				or
				ms.Tax5 = '1')
			and
				ms.[DiscountFlag] = 1;
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $DisQty2=$tmp[0];
			my $DisAmt2=$tmp[1]; 
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'DisQty2'}+=$DisQty2;
			$sales_tax_hash{'DisAmt2'}+=$DisAmt2;
 		
			$transaction_hash{$TranId} -> [26] += $DisQty2;
			$transaction_hash{$TranId} -> [27] += $DisAmt2;			 		
	}		
	# Query #12
	$query=dequote(<< "	ENDQUERY");					 				
		-- Non Taxable Discounts from merchandise sale table
		
		select	 
			ms.[Quantity] as DisQtyNT,
			(ms.[ExtRegPrice] - ms.[ExtSellPrice]) as DisAmtNT, 
			tm.TranId as TranId,
			tm.TranNum as Txn	 					 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
 
			and 
				(ms.Tax1 = '0'
				and
				ms.Tax2 = '0'
				and
				ms.Tax3 = '0'
				and
				ms.Tax4 = '0'
				and
				ms.Tax5 = '0')
			and
				ms.[DiscountFlag] = 1				
 
			;		
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $DisQtyNT=$tmp[0];
			my $DisAmtNT=$tmp[1]; 
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'DisQtyNT'}+=$DisQtyNT;
			$sales_tax_hash{'DisAmtNT'}+=$DisAmtNT;		 				
			$transaction_hash{$TranId} -> [28] += $DisQtyNT;
			$transaction_hash{$TranId} -> [29] += $DisAmtNT;			 		
	}			
	# Query #13
	$query=dequote(<< "	ENDQUERY");				
		
		-- And Non Taxable discounts
		select	 
			td.[DiscountQty] as DisQtyNT,
			td.[ExtDiscountAmt] as DisAmtNT,
			tm.TranId as TranId,
			tm.TranNum as Txn				 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblDiscounts]td on tm.TranId=td.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and 
				(td.Tax1 = '0'
				and
				td.Tax2 = '0'
				and
				td.Tax3 = '0'
				and
				td.Tax4 = '0'
				and
				td.Tax5 = '0')	 
 
			;		
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $DisQtyNT2=$tmp[0];
			my $DisAmtNT2=$tmp[1]; 
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3]; 
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'DisQtyNT2'}+=$DisQtyNT2;
			$sales_tax_hash{'DisAmtNT2'}+=$DisAmtNT2;		 				
			$transaction_hash{$TranId} -> [30] += $DisQtyNT2;
			$transaction_hash{$TranId} -> [31] += $DisAmtNT2;			 		
	}			
 
	my @header = ("TranId","Txn","QtySold","SellAmt","QtySoldNT","SellAmtNT","TaxQty1","TaxAmt1","TaxQty2","TaxAmt2","TaxQty3","TaxAmt3","TaxQty4","TaxAmt4","TaxQty5","TaxAmt5");
	my @array_to_print=\@header;
	
	my @totals = ("Total",'','','','');
	my @reductions_header = ("TranId","Txn","QtyReturned","ReturnedAmt","QtyReturnedNT","ReturnedAmtNT","DisQty","DisAmt","DisQtyNT","DisAmtNT");
	my @reductions_array_to_print=\@reductions_header;
	my @reductions_totals = ("Total",'','','','','','');
	
	#my ($QtySold,$SellAmt,$QtySoldNT,$SellAmtNT,$TaxQty1,$TaxAmt1,$TaxQty2,$TaxAmt2,$TaxQty3,$TaxAmt3,$TaxQty4,$TaxAmt4,$TaxQty5,$TaxAmt5) = 0;
	#my ($QtyReturned,$ReturnedAmt,$QtyReturnedNT,$ReturnedAmtNT,$DisQty,$DisAmt,$DisQtyNT,$DisAmtNT)=0;
	my %sales_taxid_hash;
	my $taxable_count=0;
	my $nontaxable_count=0;
	foreach my $TranId (sort keys(%transaction_hash)) {
		# The Net Sale Qty and Amt For Taxable and NonTaxable
		my $QtySold = $transaction_hash{$TranId} -> [0];
		my $SellAmt = $transaction_hash{$TranId} -> [1];
		my $QtySoldNT = $transaction_hash{$TranId} -> [5];
		my $SellAmtNT = $transaction_hash{$TranId} -> [6];	
		# The Taxes
		my $TaxQty1 = $transaction_hash{$TranId} -> [9];
		my $TaxAmt1 = $transaction_hash{$TranId} -> [10];
		my $TaxQty2 = $transaction_hash{$TranId} -> [12];
		my $TaxAmt2 = $transaction_hash{$TranId} -> [13];
		my $TaxQty3 = $transaction_hash{$TranId} -> [15];
		my $TaxAmt3 = $transaction_hash{$TranId} -> [16];
		my $TaxQty4 = $transaction_hash{$TranId} -> [18];
		my $TaxAmt4 = $transaction_hash{$TranId} -> [19];
		my $TaxQty5 = $transaction_hash{$TranId} -> [21];
		my $TaxAmt5 = $transaction_hash{$TranId} -> [22];	
		if ($TaxQty1) {
			$sales_taxid_hash{'Tax1'} -> [0] += $TaxQty1;
			$sales_taxid_hash{'Tax1'} -> [1] += $TaxAmt1;
		}
 
		if ($TaxQty2) {
			$sales_taxid_hash{'Tax2'} -> [0] += $TaxQty2;
			$sales_taxid_hash{'Tax2'} -> [1] += $TaxAmt2;		
		}
 
		if ($TaxQty3) {
			$sales_taxid_hash{'Tax3'} -> [0] += $TaxQty3;
			$sales_taxid_hash{'Tax3'} -> [1] += $TaxAmt3;			
		}
 
		if ($TaxQty4) {
			$sales_taxid_hash{'Tax4'} -> [0] += $TaxQty4;
			$sales_taxid_hash{'Tax4'} -> [1] += $TaxAmt4;			
		}
 
		if ($TaxQty5) {
			$sales_taxid_hash{'Tax5'} -> [0] += $TaxQty5;
			$sales_taxid_hash{'Tax5'} -> [1] += $TaxAmt5;			
		}		
				

		$totals[2]+=$QtySold;
		$totals[3]+=$SellAmt;
		$totals[4]+=$QtySoldNT;
		$totals[5]+=$SellAmtNT;	
		$totals[6]+=$TaxQty1;				
		$totals[7]+=$TaxAmt1;
		$totals[8]+=$TaxQty2;				
		$totals[9]+=$TaxAmt2;
		$totals[0]+=$TaxQty3;				
		$totals[11]+=$TaxAmt3;
		$totals[12]+=$TaxQty4;				
		$totals[13]+=$TaxAmt4;
		$totals[14]+=$TaxQty5;				
		$totals[15]+=$TaxAmt5;	
		# The Returns & Discounts
		my $QtyReturned = $transaction_hash{$TranId} -> [3];
		my $ReturnedAmt = $transaction_hash{$TranId} -> [4];
		my $QtyReturnedNT = $transaction_hash{$TranId} -> [7];
		my $ReturnedAmtNT = $transaction_hash{$TranId} -> [8];		
		my $DisQty = $transaction_hash{$TranId} -> [26];
		my $DisAmt = $transaction_hash{$TranId} -> [27];
		my $DisQtyNT = $transaction_hash{$TranId} -> [28];
		my $DisAmtNT = $transaction_hash{$TranId} -> [29];		
		
		$reductions_totals[2]+=$QtyReturned;
		$reductions_totals[3]+=$ReturnedAmt;
		$reductions_totals[4]+=$QtyReturnedNT;
		$reductions_totals[5]+=$ReturnedAmtNT;
		$reductions_totals[6]+=$DisQty;
		$reductions_totals[7]+=$DisAmt;	
		$reductions_totals[8]+=$DisQtyNT;
		$reductions_totals[9]+=$DisAmtNT;
		# Calculate the Gross by adding the Discounts and Returns to the Net
		$QtySold += $QtyReturned;
		$sales_tax_hash{'QtySold'} += $QtyReturned;
		
		#$QtySold -= $QtyReturned;
		#$SellAmt += $ReturnedAmt;	# Return amount is negative remember.
		$SellAmt -= $ReturnedAmt;
		$sales_tax_hash{'SellAmt'} -= $ReturnedAmt;
		$SellAmt -= $DisAmt;
		$sales_tax_hash{'SellAmt'} += $DisAmt;	
		# Repeat for Non Taxable
		$QtySoldNT += $QtyReturnedNT;
		$sales_tax_hash{'QtySoldNT'} += $QtyReturnedNT;
		$SellAmtNT -= $ReturnedAmtNT;
		$sales_tax_hash{'SellAmtNT'} -= $ReturnedAmtNT;
		$SellAmtNT -= $DisAmtNT;
		$sales_tax_hash{'SellAmtNT'} += $DisAmtNT;
		
		# Collect the counts
 
		if (($QtySold != 0) || ($SellAmt != 0)) {
			$taxable_count++;
		}
		if (($QtySoldNT != 0) || ($SellAmtNT != 0)) {
			$nontaxable_count++;
		}		
		my $Txn=$txn_hash{$TranId};
		my @array=("$TranId","$Txn","$QtySold","$SellAmt","$QtySoldNT","$SellAmtNT","$TaxQty1","$TaxAmt1","$TaxQty2","$TaxAmt2","$TaxQty3","$TaxAmt3","$TaxQty4","$TaxAmt4","$TaxQty5","$TaxAmt5");
		push (@array_to_print,\@array);	
		my @reductions_array=("$TranId","$Txn","$QtyReturned","$ReturnedAmt","$QtyReturnedNT","$ReturnedAmtNT","$DisQty","$DisAmt","$DisQtyNT","$DisAmtNT");
		push (@reductions_array_to_print,\@reductions_array);		
	}
	push (@array_to_print,\@totals);
	push (@reductions_array_to_print,\@reductions_totals);	
	
	if ($detail_selection) {	# This can be used for some debugging.
		if (recordCount(\@array_to_print)) {	
			print $q->h3("Gross Merchandise Sales");
			printResult_arrayref2(\@array_to_print );
		} else {
			print $q->p("Found no data for Sales and Taxes for $storeid between $startdate and $enddate");
		}
		if (recordCount(\@reductions_array_to_print)) {	
			print $q->h3("Reductions to Gross Sales");
			printResult_arrayref2(\@reductions_array_to_print );
		} else {
			print $q->p("Found no data for Sales and Taxes for $storeid between $startdate and $enddate");
		}		
	}
 
	# Print out the results:
	my $TotalQtySold = ($sales_tax_hash{'QtySold'} + $sales_tax_hash{'QtySoldNT'});
	my $TotalSellAmt = ($sales_tax_hash{'SellAmt'} + $sales_tax_hash{'SellAmtNT'});	
	my $QtyTotalMerchSales=($sales_tax_hash{'QtySold'} + $sales_tax_hash{'QtySoldNT'});

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	# Part I - Gross Merch Sales
	print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Category")),
				$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Count")),
				$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Amount")),
			   );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Gross Merch Sales:")));

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtySold'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'SellAmt'})),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtySoldNT'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'SellAmtNT'})),
			   );
	my $gamt = ($sales_tax_hash{'SellAmt'} + $sales_tax_hash{'SellAmtNT'});
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Merch Sales:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $QtyTotalMerchSales),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($gamt)),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Sales Tax:")));   
	my $tax_amt_total=0;
	my $tax_cnt_total=0;
	foreach my $tax_id (sort keys(%sales_taxid_hash)) {
		my $ref=$sales_taxid_hash{$tax_id};
		my @array=@$ref;
		my $tax_count=$array[0];        
		my $tax_amt_sum=$array[1];
		$tax_amt_total+=$tax_amt_sum;
		$tax_cnt_total+=$tax_count;
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "$tax_id:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $tax_count),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($tax_amt_sum)),
			   );
	}       
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Taxes:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $tax_cnt_total),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($tax_amt_total)),
			   ); 
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "============="),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "================"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
			   );               		
	my $gross_sales=($gamt + $tax_amt_total) ; 
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Gross Sales:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($gross_sales)),
			   );    
	# Part II - Reductions to Gross Merch Sales			   
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Reductions to Gross Sales")));               
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Return:")));
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtyReturned'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'ReturnedAmt'})),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtyReturnedNT'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'ReturnedAmtNT'})),
			   );
	my $rcnt = ($sales_tax_hash{'QtyReturned'} + $sales_tax_hash{'QtyReturnedNT'});
	my $ramt = ($sales_tax_hash{'ReturnedAmt'} + $sales_tax_hash{'ReturnedAmtNT'});
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $rcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($ramt)),
			   );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Discounts:")));

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'DisQty2'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'DisAmt2'})),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'DisQtyNT'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'DisAmtNT'})),
			   );
			   
	my $dcnt = ($sales_tax_hash{'DisQty2'} + $sales_tax_hash{'DisQtyNT'});
	my $damt = ($sales_tax_hash{'DisAmt2'} + $sales_tax_hash{'DisAmtNT'});				   
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $dcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($damt)),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "============="),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "================"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
			   );                 
	my $total_reductions=($rcnt + $dcnt);
	my $total_reductionsamt=((abs $ramt) + $damt);
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Reductions:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $total_reductions),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($total_reductionsamt)),
			   );
	# Part III - Net Sales
	my $nTcnt = ($sales_tax_hash{'QtySold'} - $sales_tax_hash{'QtyReturned'});	
	my $nTamt = ($sales_tax_hash{'SellAmt'} - (abs $sales_tax_hash{'ReturnedAmt'}) - $sales_tax_hash{'DisAmt2'});
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Net Sales:")));
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $nTcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($nTamt)),
			   );
	my $nNTcnt = ($sales_tax_hash{'QtySoldNT'} - $sales_tax_hash{'QtyReturnedNT'});				   
	my $nNTamt = ($sales_tax_hash{'SellAmtNT'} - (abs $sales_tax_hash{'ReturnedAmtNT'}) - $sales_tax_hash{'DisAmtNT'});	
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "NonTaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $nNTcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($nNTamt)),
			   );
	my $ncnt = ($nTcnt + $nNTcnt);
	my $namt = ($nTamt + $nNTamt);
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $ncnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($namt)),
			   );

	print $q->end_table(),"\n";			
	
	
	$dbh->disconnect;
	standardfooter();
	exit;	
	# Debug - This is very close but does not yet match The Elder 100%
	# Alternative format
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	my @current_issues=();
	my %summary_hash;
	my %sales_tax_hash;
	while (my @tmp = $sth->fetchrow_array()) {
		my $QtySold = sprintf("%d", $tmp[0]);
		my $SellAmt = $tmp[1];
		my $QtyReturned  = sprintf("%d", $tmp[2]);
		my $ReturnedAmt  = $tmp[3];
		my $QtySoldNT  = sprintf("%d", $tmp[4]);
		my $SellAmtNT  = $tmp[5];
		my $QtyReturnedNT  = sprintf("%d", $tmp[6]);
		my $ReturnedAmtNT  = $tmp[7];
		my $TaxAmt1  = $tmp[8];
		my $TaxQty1  = $tmp[9];
		my $TaxAbleAmt1  = $tmp[10];
		if ($TaxQty1) {
			$sales_tax_hash{'Tax1'} -> [0] = $TaxQty1;
			$sales_tax_hash{'Tax1'} -> [1] = $TaxAmt1;
		}
		my $TaxAmt2  = $tmp[11];
		my $TaxQty2  = $tmp[12];
		my $TaxAbleAmt2  = $tmp[13];
		if ($TaxQty2) {
			$sales_tax_hash{'Tax2'} -> [0] = $TaxQty2;
			$sales_tax_hash{'Tax2'} -> [1] = $TaxAmt2;		
		}
		my $TaxAmt3  = $tmp[14];
		my $TaxQty3  = $tmp[15];
		my $TaxAbleAmt2  = $tmp[16];
		if ($TaxQty3) {
			$sales_tax_hash{'Tax3'} -> [0] = $TaxQty3;
			$sales_tax_hash{'Tax3'} -> [1] = $TaxAmt3;			
		}
		my $TaxAmt4  = $tmp[17];
		my $TaxQty4  = $tmp[18];
		my $TaxAbleAmt2  = $tmp[19];
		if ($TaxQty4) {
			$sales_tax_hash{'Tax4'} -> [0] = $TaxQty4;
			$sales_tax_hash{'Tax4'} -> [1] = $TaxAmt4;			
		}
		my $TaxAmt5  = $tmp[20];
		my $TaxQty5  = $tmp[21];
		my $TaxAbleAmt2  = $tmp[22];
		if ($TaxQty5) {
			$sales_tax_hash{'Tax5'} -> [0] = $TaxQty5;
			$sales_tax_hash{'Tax5'} -> [1] = $TaxAmt5;			
		}
		my $DisQty   = sprintf("%d", $tmp[23]);
		my $DisAmt   = $tmp[24];
		my $DisQtyNT   = sprintf("%d", $tmp[25]);
		my $DisAmtNT   = $tmp[26];		
		
		my $TaxAmt=($TaxAmt1 + $TaxAmt2 + $TaxAmt3 + $TaxAmt4 + $TaxAmt5);
		#my $TaxAbleAmt=($TaxAbleAmt1 + $TaxAbleAmt2 + $TaxAbleAmt3 + $TaxAbleAmt4 + $TaxAbleAmt5);
		my $QtyTotalMerchSales=($QtySold + $QtySoldNT);
		 
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";

		print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Category")),
					$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Count")),
					$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Amount")),
				   );

		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Gross Merch Sales:")));

		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $QtySold),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($SellAmt)),
				   );
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $QtySoldNT),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($SellAmtNT)),
				   );
		my $gamt = ($SellAmt + $SellAmtNT);
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Merch Sales:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $QtyTotalMerchSales),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($gamt)),
				   );
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Sales Tax:")));   
		my $tax_amt_total=0;
		my $tax_cnt_total=0;
		foreach my $tax_id (sort keys(%sales_tax_hash)) {
			my $ref=$sales_tax_hash{$tax_id};
			my @array=@$ref;
			my $tax_count=$array[0];        
			my $tax_amt_sum=$array[1];
			$tax_amt_total+=$tax_amt_sum;
			$tax_cnt_total+=$tax_count;
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "$tax_id:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $tax_count),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($tax_amt_sum)),
				   );
		}       
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Taxes:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $tax_cnt_total),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($tax_amt_total)),
				   ); 
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "============="),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "================"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
					$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
				   );               		
		my $gross_sales=($gamt + $tax_amt_total) ; 
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Gross Sales:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($gross_sales)),
				   );    
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Reductions to Gross Sales")));               
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Return:")));
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $QtyReturned),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($ReturnedAmt)),
				   );
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $QtyReturnedNT),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($ReturnedAmtNT)),
				   );
		my $rcnt = ($QtyReturned + $QtyReturnedNT);
		my $ramt = ($ReturnedAmt + $ReturnedAmtNT);
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $rcnt),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($ramt)),
				   );

		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Discounts:")));
 
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $DisQty),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($DisAmt)),
				   );
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $DisQtyNT),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($DisAmtNT)),
				   );
 				   
		my $dcnt = ($DisQty + $DisQtyNT);
		my $damt = ($DisAmt + $DisAmtNT);				   
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $dcnt),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($damt)),
				   );
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "============="),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "================"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
					$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
				   );                 
		my $total_reductions=($rcnt + $dcnt);
		my $total_reductionsamt=($ramt + $damt);
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Reductions:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $total_reductions),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($total_reductionsamt)),
				   );
		my $nTcnt = ($QtySold + $QtyReturned);
 
		my $nTamt = ($SellAmt - $ReturnedAmt - $DisAmt);
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Net Sales:")));
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $nTcnt),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($nTamt)),
				   );
		my $nNTcnt = ($QtySoldNT + $QtyReturnedNT);				   
		my $nNTamt = ($SellAmtNT - $ReturnedAmtNT - $DisAmtNT);	
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "NonTaxable Items:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $nNTcnt),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($nNTamt)),
				   );
		my $ncnt = ($nTcnt + $nNTcnt);
		my $namt = ($nTamt + $nNTamt);
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
					$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
					$q->td({-class=>'tabledatanb', -align=>'right'}, $ncnt),
					$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($namt)),
				   );

		print $q->end_table(),"\n";		
	}
	
	$dbh->disconnect;
	standardfooter();	
} elsif ("$report" eq "SALESTAXES_orig") {
    print $q->h2("Sales and Taxes Report");
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
 
    my @tmp=split(/-/,$store_selection);
    my $storeid=$tmp[0];
    my $storename=$tmp[1];
	
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
	
    #my $data_selection   = trim(($q->param("data_selection"))[0]);
    #my $company_selection   = trim(($q->param("company_selection"))[0]);
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	my $display_method = 0;
	# I would ike to eventually be able to display by month, by day, or show all transactions for a day.
	# this is not developed yet. - kdg
	# Determine how to display the info.  If the request spans several months, then show it by month.
	# If it is all in one month but spans several days, then show it by day.
	# If only one day is selected, then show all of the transactions for that day	
	# By Year - Show each year on a row
	# By Month - Show each month on a row
	# By Day - Show each day on a row
	# By Txn - Show each transaction on a row
	my $primary_select="convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate";
	my $final_primary_select="tms.TranDate";
	my $enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$enddate')";
	my $group_by = "group by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $order_by = "order by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $final_order_by = "order by tms.TranDate";
	my $final_join = "left outer join #tmpNonMerchSales nms on tms.TranDate = nms.TranDate
			left outer join #tmpTaxTotals ttt on tms.TranDate = ttt.TranDate
			left outer join #tmpDiscount td on tms.TranDate = td.TranDate";
	if ($date_selection) {		
		my $year=substr($date_selection,0,4);
		my $month=substr($date_selection,5,2);
		my $day=substr($date_selection,8,2);
		if ($day) {
			$display_method="by_txn";	
			$startdate="$year"."-"."$month"."-"."$day";
			$enddate = $startdate;
			$enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$startdate')";	
			$primary_select="tm.TranId";
			$final_primary_select="tms.TranId";
			$group_by = "group by tm.TranId";
			$order_by = "order by tm.TranId";
			$final_order_by = "order by tms.TranId";
			$final_join = "left outer join #tmpNonMerchSales nms on tms.TranId = nms.TranId
			left outer join #tmpTaxTotals ttt on tms.TranId = ttt.TranId
			left outer join #tmpDiscount td on tms.TranId = td.TranId";			
		} elsif ($month) {
		
			$display_method="by_day"; 
			$startdate="$year"."-"."$month"."-"."01";
			$enddate_where = "and tm.TranDateTime < dateadd( m, 1, '$startdate')"; 
			$enddate = "End of Month";
 			
		}	 
	} else {		
		if ($start_month_selection eq $end_month_selection) {			
			if ($start_year_selection eq $end_year_selection) {
				$display_method="by_day";				
				 
			} else {
				$display_method="by_month";					
			}
		} else {			
			$display_method="by_month";								
		}
	}	
 
	print $q->h3("Store: $store_selection - $startdate to $enddate");
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
	 
	$query=dequote(<< "	ENDQUERY");		 
		select				 			
			$primary_select,
			sum(ms.ExtSellPrice) as [MSNet]
			--sum(ms.ExtTaxableAmt) as [MSTaxableAmt]
			into #tmpMerchSales
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
		$group_by						 
		$order_by;
		 
		select				 			
			$primary_select,			
			sum(nms.SellAmount) as [NMSNet]
			--sum(nms.ExtTaxableAmount) as [NMSTaxableAmt]				
			into #tmpNonMerchSales
		from 
			****Tlog..tblTranMaster tm				
			join ****Tlog..tblNonMerchandiseSale nms on tm.TranId=nms.TranId   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')                 
		$group_by						
		$order_by;		
			
		select			 			
			$primary_select,								
			0 as [PaymentTotal],
			sum(tt.[TotalSale]) as [SaleTotal],
			sum(tt.[Tax1]) as [Tax1],		 
			sum(tt.[Tax2]) as [Tax2],		 
			sum(tt.[Tax3]) as [Tax3],			 
			sum(tt.[Tax4]) as [Tax4],			
			sum(tt.[Tax5]) as [Tax5],
			sum(tt.[ManualTaxTotals]) as [ManTax] 
			into #tmpTaxTotals
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTotal] tt on tm.TranId=tt.TranId
		where 
			tm.TranDateTime >= '$startdate' 			
			$enddate_where
			and
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and
				tm.StoreId in ('$storeid')
		$group_by						 
		$order_by;
		 		

		-- Get Discounts
        select			
			$primary_select,            
            sum(ds.ExtDiscountAmt) as [Discounts]
			into #tmpDiscount
        from ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
 
        where tm.TranDateTime >= '$startdate'
		$enddate_where        
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')       
		$group_by						 
		$order_by;
		
		-- Get the requested data
		select			 
			$final_primary_select,			 
			(ttt.SaleTotal + ttt.Tax1 + ttt.Tax2 + ttt.Tax3 + ttt.Tax4 + ttt.Tax5 + ttt.ManTax) as PaymentTotal,
			ttt.SaleTotal,				
			tms.MSNet,
			--tms.MSTaxableAmt,
			nms.NMSNet,
			--nms.NMSTaxableAmt,
			ttt.Tax1,
			ttt.Tax2,
			ttt.Tax3,
			ttt.Tax4,
			ttt.Tax5,
			ttt.ManTax,
			td.Discounts
			
		from
			#tmpMerchSales tms
			$final_join
			$final_order_by;
			
	drop table #tmpMerchSales;
	drop table #tmpNonMerchSales;
	drop table #tmpTaxTotals;
	drop table #tmpDiscount;
	
	ENDQUERY
	
	#print $q->pre("$query");

		#print $q->pre("$query");
    if ($display_method eq "by_month") {
		print $q->h3("By Month: query");
		#print $q->pre("$query");
		#$tbl_ref = execQuery_arrayref($dbh, $query);
	}	
    if ($display_method eq "by_day") {
		print $q->h3("By Day: query");
		#print $q->pre("$query");
		#$tbl_ref = execQuery_arrayref($dbh, $query);
	}		
    if ($display_method eq "by_txn") {
		print $q->h3("By Txn: query");
		#print $q->pre("$query2");
		#$tbl_ref = execQuery_arrayref($dbh, $query2);
	}		
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	# Process the query results differently depending on whether we are viewing by year, month, or day
    if ($display_method eq "by_month") {
		print $q->h3("By Month: display");
		# Need to group the results by month.
 
		# Total each day of the month
		my %month_hash;
		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];
		my @header=@$header_ref;
		my @array_to_print;
		my @totals_array_to_print;
		push(@array_to_print,$header_ref);
		$header[0]='';
		push(@header,"TotalTax");
		push(@totals_array_to_print,\@header);
 		
		if (recordCount($tbl_ref)) { 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				my $date=$$thisrow[0];
				my $month_date=substr($date,0,7);				
				$month_hash{$month_date} -> [0] = $month_date;
				$month_hash{$month_date} -> [1] += $$thisrow[1];		
				$month_hash{$month_date} -> [2] += $$thisrow[2];
				$month_hash{$month_date} -> [3] += $$thisrow[3];
				$month_hash{$month_date} -> [4] += $$thisrow[4];
				$month_hash{$month_date} -> [5] += $$thisrow[5];
				$month_hash{$month_date} -> [6] += $$thisrow[6];
				$month_hash{$month_date} -> [7] += $$thisrow[7];
				$month_hash{$month_date} -> [8] += $$thisrow[8];
				$month_hash{$month_date} -> [9] += $$thisrow[9];
				$month_hash{$month_date} -> [10] += $$thisrow[10];
				$month_hash{$month_date} -> [11] += $$thisrow[11];
				$month_hash{$month_date} -> [12] += $$thisrow[12];
				$month_hash{$month_date} -> [13] += $$thisrow[13];
			}	
		}	
 	
		foreach my $date (sort keys(%month_hash)) {
			my $v0 = $month_hash{$date} -> [0];
			my $v1 = $month_hash{$date} -> [1];
			my $v2 = $month_hash{$date} -> [2];
			my $v3 = $month_hash{$date} -> [3];
			my $v4 = $month_hash{$date} -> [4];
			my $v5 = $month_hash{$date} -> [5];
			my $v6 = $month_hash{$date} -> [6];
			my $v7 = $month_hash{$date} -> [7];
			my $v8 = $month_hash{$date} -> [8];
			my $v9 = $month_hash{$date} -> [9];
			my $v10 = $month_hash{$date} -> [10];
			my $v11 = $month_hash{$date} -> [11];
			my $v12 = $month_hash{$date} -> [12];
			my $v13 = $month_hash{$date} -> [13];
			 
			my @array=(
				
				"$v0",
				"$v1",
				"$v2",
				"$v3",
				"$v4",
				"$v5",
				"$v6",
				"$v7",				
				"$v8",	
				"$v9",	
				"$v10",	
				"$v11",		
				"$v12", 			
				"$v13", 
				);
			push(@array_to_print,\@array);
							 
			$fieldsum[1] += $month_hash{$date} -> [1];
			$fieldsum[2] += $month_hash{$date} -> [2];
			$fieldsum[3] += $month_hash{$date} -> [3];
			$fieldsum[4] += $month_hash{$date} -> [4];
			$fieldsum[5] += $month_hash{$date} -> [5];
			$fieldsum[6] += $month_hash{$date} -> [6];
			$fieldsum[7] += $month_hash{$date} -> [7];
			$fieldsum[8] += $month_hash{$date} -> [8]; 
			$fieldsum[9] += $month_hash{$date} -> [9]; 
			$fieldsum[10] += $month_hash{$date} -> [10]; 
			$fieldsum[11] += $month_hash{$date} -> [11]; 
			$fieldsum[12] += $month_hash{$date} -> [12]; 
			$fieldsum[13] += $month_hash{$date} -> [13]; 
 		}		
		# Get the total tax and add it to the end of the totals.
		
		my $total_tax;
		for my $field (7 .. 12) {
			$total_tax += $fieldsum[$field];
		}		
		push(@fieldsum,$total_tax);
		push(@totals_array_to_print,\@fieldsum);
 
		if (recordCount(\@array_to_print)) {				 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},					
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
			);
			my @links = ("$scriptname?report=SALESTAXES&store_selection=$store_selection&date_selection=".'$_' ); 			
			printResult_arrayref2(\@array_to_print, \@links, \@format );
			printResult_arrayref2(\@totals_array_to_print, '', \@format );

		} else {
			print $q->p("Found no data in tblStoreTraffic for $storeid between $startdate and $enddate");
		}	

	}

	if (($display_method eq "by_day") ||($display_method eq "by_txn"))  {		
 		#$tbl_ref = execQuery_arrayref($dbh, $query);
		print $q->h3("Display Method: $display_method");
		# Prepare to collect a separate table for the totals
		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];
		my @header=@$header_ref;
		my @array_to_print;
		my @totals_array_to_print;
		push(@array_to_print,$header_ref);
		$header[0]='';
		push(@header,"TotalTax");
		push(@totals_array_to_print,\@header);		
		if (recordCount($tbl_ref)) {
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				for my $field (1 .. $#$thisrow) {
					$fieldsum[$field] += $$thisrow[$field];
				}
			}
			# Get the total tax and add it to the end of the totals.
			
			my $total_tax;
			for my $field (7 .. 12) {
				$total_tax += $fieldsum[$field];
			}
			
			push(@fieldsum,$total_tax);
			push(@totals_array_to_print,\@fieldsum);
		}
		
		if (recordCount($tbl_ref)) {				
 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},					
				{'align' => 'Right', 'currency' => ''},	
			);
			my @links = ("$scriptname?report=SALESTAXES&store_selection=$store_selection&date_selection=".'$_' );
			if ($display_method eq "by_txn") {			
				@links = ("$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
			}
			
			printResult_arrayref2($tbl_ref, \@links, \@format );
			printResult_arrayref2(\@totals_array_to_print, '', \@format );

		} else {
			print $q->p("Found no data in tblStoreTraffic for $storeid between $startdate and $enddate");
		}
    }	
 	
	$dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
	standardfooter();

} elsif ("$report" eq "NETSALES2") {
    print $q->h2("Net Sales Report");	
	my $storeid   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $company_selection   = trim(($q->param("company_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    my $summary=0;
    my %storeData_hash;
    my %tlog_hash;
    my $where;
    my $twhere;
    my $tjoin;
    if ($company_selection) {
        print $q->p("Showing Villages Sales Only");
        $where = " and VendorID = '01' ";
        $tjoin = "left outer join [****Data].[dbo].[tblStoreSkuInfo] ssi on SSI.SKU COLLATE DATABASE_DEFAULT = ms.SKU and ssi.StoreId = '$storeid'";
        $twhere = " and ssi.vendorid = '01'";
    }

    # Get the storedata
     if ($enddate eq $startdate) {
        print $q->p("For $startdate");
		# On rare occasions, transactions get duplicated  Here is a check for that situation
        $query=dequote(<< "        ENDQUERY");
            select n.StoreId,
				 n.Transnum,
                 right('00' + convert(varchar(2), n.RegNum) ,2) as RegNum,
                 case when n.TxnNum >= 10000 then n.TxnNum-10000 else n.TxnNum end as TxnNum,
                 convert(DateTime, convert(varchar(10),n.TranDateTime,21)) as TranDate,
                 n.Qty,
                 n.NetAmt

            from ****Data..tblStoreNetSales n
            where TranDateTime >= '$startdate' and TranDateTime < dateadd( d, 1, '$enddate') and -- add 1 day for an exclusive upper bound
                    n.StoreId in ('$storeid') and
                    substring(n.TxnModifier,1,1) not in (2,6) -- Exclude layaway sales and cancels
                    $where

            order by n.TxnNum
        ENDQUERY

        my $sth=$dbh->prepare("$query");
        $sth->execute();
		my %txn_hash;
		my %transnum_hash;
        while (my @tmp = $sth->fetchrow_array()) {
            my $StoreId=$tmp[0];
			my $transnum=$tmp[1];
            my $RegNum=$tmp[2];
            my $txnNum=$tmp[3];
            my $TranDate=$tmp[4];
            my $Qty=$tmp[5];
            my $Net=$tmp[6];
			$transnum_hash{$transnum}=\@tmp;
			if ($txn_hash{$txnNum}) {
				if ($transnum != $txn_hash{$txnNum}) {
					print "<br />Warning: Txn $txnNum is associated with $transnum and $txn_hash{$txnNum}<br />";
					print "Store: $StoreId Transnum: $transnum Reg: $RegNum Txn: $txnNum Date: $TranDate Qty: $Qty Net: $Net<br />";
					my $ref=$transnum_hash{$txn_hash{$txnNum}};
					my @array=@$ref;
					my $StoreId=$array[0];
					my $transnum=$array[1];
					my $RegNum=$array[2];
					my $txnNum=$array[3];
					my $TranDate=$array[4];
					my $Qty=$array[5];
					my $Net=$array[6];
					print "Store: $StoreId <b>Transnum: $transnum</b> Reg: $RegNum Txn: $txnNum Date: $TranDate Qty: $Qty Net: $Net<br />";
				}
			}
			$txn_hash{$txnNum}=$transnum;

		}

        # Print detail if the request was only for one day.
        $query=dequote(<< "        ENDQUERY");
            select n.StoreId,
                 right('00' + convert(varchar(2), n.RegNum) ,2) as RegNum,
                 case when n.TxnNum >= 10000 then n.TxnNum-10000 else n.TxnNum end as TxnNum,
                 convert(DateTime, convert(varchar(10),n.TranDateTime,21)) as TranDate,
                 sum(n.Qty) as [Qty],
                 sum(n.NetAmt) as [Net]

            from ****Data..tblStoreNetSales n
            where TranDateTime >= '$startdate' and TranDateTime < dateadd( d, 1, '$enddate') and -- add 1 day for an exclusive upper bound
                    n.StoreId in ('$storeid') and
                    substring(n.TxnModifier,1,1) not in (2,6) -- Exclude layaway sales and cancels
                    $where
            group by n.StoreId, n.RegNum, n.TxnNum,
                        convert(DateTime, convert(varchar(10),n.TranDateTime,21))
            order by n.StoreId, n.RegNum,
                        convert(DateTime, convert(varchar(10),n.TranDateTime,21)), n.TxnNum
        ENDQUERY

		#print $q->pre($query);
        $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $StoreId=$tmp[0];
            my $RegNum=$tmp[1];
            my $txnNum=$tmp[2];
            my $TranDate=$tmp[3];
            my $Qty=$tmp[4];
            my $Net=$tmp[5];

            my @array=(
                $StoreId,
                $RegNum,
                $TranDate,
                $Qty,
                $Net
            );
            $storeData_hash{$txnNum}=\@array;
        }
    } else {
        print $q->p("For $startdate to $enddate");
        # If the request was for more than one day, print a summary only.
        $summary=1;
        $query=dequote(<< "        ENDQUERY");
            select
                convert(DateTime, convert(varchar(10),n.TranDateTime,21)) as TranDate,
                sum(n.NetAmt)
            from ****Data..tblStoreNetSales n
            where TranDateTime >= '$startdate' and TranDateTime < dateadd( d, 1, '$enddate') and -- add 1 day for an exclusive upper bound
                n.StoreId in ('$storeid') and
                substring(n.TxnModifier,1,1) not in (2,6)
                $where
            group by convert(DateTime, convert(varchar(10),n.TranDateTime,21))
            order by convert(DateTime, convert(varchar(10),n.TranDateTime,21))
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $TranDate=$tmp[0];
            my $Net=$tmp[1];
            my @array=(
                $Net
            );
            $storeData_hash{$TranDate}=\@array;
        }
    }
    my $tbl_ref1 = execQuery_arrayref($dbh, $query);
    # Get the tlog data
    if ($enddate eq $startdate) {
        # Print detail if the request was only for one day.
        $query=dequote(<< "        ENDQUERY");
            select tm.StoreId,
                 tm.RegisterId as RegNum,
                 case when tm.StoreId='2098' and tm.RegisterId = '02' and
                              tm.TranNum between 4013 and 4023 and
                              convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                        then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
                 convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
                 sum(convert(int,sign(ms.ExtOrigPrice) * ms.Quantity)) as [Qty],
                 sum(ms.ExtSellPrice) as [Net], 	-- 2014-10-20 - Changed again
				 --sum(ms.ExtNetPrice) as [Net],	-- 2014-07-23 - Not certain which is best to use - kdg
                 tm.tlogfile as [TlogFile],
				 tm.TranId as TranId
				 

            from ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                    $tjoin
            where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and  
                    tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                    tm.TranModifier not in (2,6) and  
                    tm.StoreId in ('$storeid')
                    $twhere

            group by tm.StoreId, tm.RegisterId, tm.TranNum,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)),
                        tm.tlogfile,tm.TranId
            order by tm.StoreId, tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
        ENDQUERY
 
		#print $q->pre("$query");
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $StoreId=$tmp[0];
            my $RegNum=$tmp[1];
            my $txnNum=$tmp[2];
            my $TranDate=$tmp[3];
            my $Qty=$tmp[4];
            my $Net=$tmp[5];
            my $TlogFile=$tmp[6];
			my $TranId=$tmp[7];
            my @array=(
                $StoreId,
                $RegNum,
                $TranDate,
                $Qty,
                $Net,
                $TlogFile,
				$TranId
            );			
            $tlog_hash{$txnNum}=\@array;
        }
    } else {
        # If the request was for more than one day, print a summary only.
        $summary=1;
        $query=dequote(<< "        ENDQUERY");
            select
                 convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
                 --sum(ms.ExtSellPrice) as [Net] -- 2014-10-20 - Changed - kdg
				 sum(ms.ExtNetPrice) as [Net]
            from ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                    $tjoin
            where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and -- add 1 day for an exclusive upper bound
                    tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                    tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                    tm.StoreId in ('$storeid')
                    $twhere
            group by
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21))
            order by
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21))
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $TranDate=$tmp[0];
            my $Net=$tmp[1];
            my @array=(
                $Net,
            );
            $tlog_hash{$TranDate}=\@array;
        }
    }
    my @report;
    my @totals=(
        "Total",
        '',
        '',
        '',
        '',
        0,
        0,
        0,
        0,
        '',
		''
    );
    my @format;
    if (keys(%tlog_hash)) {
        unless (keys(%storeData_hash)) {
            print $q->p("Warning: We have Tlog data but no store data for the requested period.");
        }
    }

    if ((keys(%storeData_hash)) || (keys(%tlog_hash)) ){
        # We have store data or tlog data
        unless (keys(%tlog_hash)) {
            print $q->b("NOTE: No Tlog data found!");
        }
        unless (keys(%storeData_hash)) {
            print $q->b("NOTE: No StoreData found!");
        }
        if ($summary) {
            my @header=(
                'Date',
                'TlogNet',
                'SdataNet',
                ''
            );
            @totals=(
                "Total",
                0,
                0,
                ''
            );
            push(@report,\@header);
            # Combine the two so we can show them both together.
            foreach my $date (sort(keys(%storeData_hash))) {
                my $tNet;
                if ($tlog_hash{$date}) {
                    my $tref=$tlog_hash{$date};
                    $tNet=$$tref[0];
                } else {
                    $tNet="unknown";
                }
                my $ref=$storeData_hash{$date};
                my $sNet=$$ref[0];
                # Trim the date
                my @tmp=split(/\s+/,$date);
                $date=$tmp[0];
                my $star;
                if ($tNet != $sNet){
                    $star='*';
                }
                my @array=(
                    $date,
                    $tNet,
                    $sNet,
                    $star
                );
                push(@report,\@array);
                $totals[1]+=$tNet;
                $totals[2]+=$sNet;
            }
        } else {
            # Create a hash of all of the txns either in the tlog or the store data
            my %txn_hash;
            foreach my $txn (sort(keys(%storeData_hash))) {
                $txn_hash{$txn}=1;
            }
            foreach my $txn (sort(keys(%tlog_hash))) {
                $txn_hash{$txn}=1;
            }
            my @header=(
				'TranId',
                'Tlog',
                'StoreId',
                'RegNum',
                'TxnNum',
                'Date',
                'TlogQty',
                'TlogNet',
                'SdataQty',
                'SdataNet',
                ''
            );
            push(@report,\@header);
            # We have tlog data
            # Combine the two so we can show them both together.
            foreach my $txn (sort(keys(%txn_hash))) {
                my $tStoreId;
                my $tRegNum;
                my $tTranDate;
                my $tQty;
                my $tNet;
                my $TlogFile;
				my $TranId;
                if ($tlog_hash{$txn}) {
                    my $tref=$tlog_hash{$txn};
                    $tStoreId=$$tref[0];
                    $tRegNum=$$tref[1];
                    $tTranDate=$$tref[2];
                    $tQty=$$tref[3];
                    $tNet=$$tref[4];
                    $TlogFile=$$tref[5];
					$TranId=$$tref[6];					
                } else {
                    $tQty="unknown";
                    $tNet="unknown";
                }

                my $sStoreId;
                my $sRegNum;
                my $TxnNum;
                my $sTranDate;
                my $sQty;
                my $sNet;
                if ($storeData_hash{$txn}) {
                    my $sref=$storeData_hash{$txn};
                    $sStoreId=$$sref[0];
                    $sRegNum=$$sref[1];
                    $sTranDate=$$sref[2];
                    $sQty=$$sref[3];
                    $sNet=$$sref[4];
                } else {
                    $sQty="unknown";
                    $sNet=0;
                }


                # $star is used to indicate when tlog data varies from store data
                my $star;
                if ($tNet != $sNet){
                    $star='*';
                }
                if ($tQty != $sQty){
                    $star='*';
                }
                # Trim the date
                my @tmp=split(/\s+/,$tTranDate);
                $tTranDate=$tmp[0];
                my @array=(
					$TranId,
                    $TlogFile,
                    $tStoreId,
                    $tRegNum,
                    $txn,
                    $tTranDate,
                    $tQty,
                    $tNet,
                    $sQty,
                    $sNet,

                    $star
                );
                push(@report,\@array);
                $totals[6]+=$tQty;
                $totals[7]+=$tNet;
                $totals[8]+=$sQty;
                $totals[9]+=$sNet;
            }
        }

        # Print the results

        push(@report,\@totals);
        my @format;
		my @links;
        if ($summary) {
            @format = ('', {'align' => 'Right', 'currency' => ''},{'align' => 'Right', 'currency' => ''});
        } else {
            @format = ('','', '',  '','','','',{'align' => 'Right', 'currency' => ''},'',{'align' => 'Right', 'currency' => ''});			
			@links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        }
        print $q->h3("$storeid - $storename");
        printResult_arrayref2(\@report, \@links, \@format);

    #} elsif (keys(%tlog_hash)) {
    #    print $q->p("Warning: We have Tlog data but no store data for the requested period.");
    } else {
        print $q->p("Warning: We have neither Tlog data nor store data for the requested period.");
        standardfooter();
        exit;
    }
    $dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();
} elsif ("$report" eq "DISCOUNT_V2") {
    print $q->h2("Discount Report - V2 - BETA");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $startdate   = trim(($q->param("startdate"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
	my @tranid_list;
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    unless ($startdate) {
        $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    }

    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    unless ($end_year_selection) {
        $enddate = $startdate;
    }

    print $q->h3("Store: #${storeid} $storename - $startdate");
    my $summary=0;
    my @date_array;

    if ($startdate eq $enddate) {
        #print $q->h3("Part I - Item and Transaction Discounts");
        for my $discount ("Item","Transaction","Promotion","MixMatch") {
            print $q->h4("$discount Discounts");
            my ($have_data,$tbl_ref,$format_ref);
            if ($discount eq "Item") {($have_data,$tbl_ref, $format_ref)=get_item_discounts($storeid,$storename,$startdate,$summary); }
            if ($discount eq "Transaction") {($have_data,$tbl_ref, $format_ref)=get_transaction_discounts($storeid,$storename,$startdate,$summary); }
            if ($discount eq "Promotion") {($have_data,$tbl_ref, $format_ref)=get_promotion_discounts($storeid,$storename,$startdate,$summary); }
            if ($discount eq "MixMatch") {($have_data,$tbl_ref, $format_ref)=get_mixmatch_discounts($storeid,$storename,$startdate,$summary); }
            if ($have_data) {
                print $q->p("$discount Discounts found for store number $storeid for $startdate");
                printResult_arrayref2($tbl_ref, '', $format_ref);
            } else {
                print $q->p("No $discount Discount data found for $storeid for $startdate");
            }
        }


    } else {
        $summary=1;
        # Get all of the dates where there are transactions between the start and the end date
        my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
        my $dbh = openODBCDriver($dsn, '****', '****');
        $query=dequote(<< "        ENDQUERY");
            select
                distinct(convert(DateTime, convert(varchar(10),tm.TranDateTime,21))) as TranDate

            from ****Tlog..tblTranMaster tm
                join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
            where
                tm.TranDateTime >= '$startdate'
            and
                tm.TranDateTime < dateadd( d, 1, '$enddate')
            and
                tm.TranVoid = 0 and tm.IsPostVoided = 0
            and
                tm.TranModifier not in (2,6)
            and
                tm.StoreId in ('$storeid')
            group by tm.StoreId, tm.RegisterId, tm.TranNum,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)),
                        tm.tlogfile
            order by
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21))
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $date=substr($tmp[0],0,10);
            push(@date_array,$date);
        }
        $dbh->disconnect;


    }
    if ($summary) {
        my @summary_totals=("Grand Total");
        for my $discount ("Item","Transaction","Promotion","MixMatch") {
            print $q->h4("$discount Discounts");
            my ($have_data,$header,$day_tbl_ref);
            my $tbl_ref=[];
            my $set_header=1;
            foreach my $date (@date_array) {

                if ($discount eq "Item") {($have_data,$header,$day_tbl_ref)=get_item_discounts($storeid,$storename,$date,$summary); }
                if ($discount eq "Transaction") {($have_data,$header,$day_tbl_ref)=get_transaction_discounts($storeid,$storename,$date,$summary); }
                if ($discount eq "Promotion") {($have_data,$header,$day_tbl_ref)=get_promotion_discounts($storeid,$storename,$date,$summary); }
                if ($discount eq "MixMatch") {($have_data,$header,$day_tbl_ref)=get_mixmatch_discounts($storeid,$storename,$date,$summary); }
                if ($have_data) {

                    if ($set_header) {
                        push(@$tbl_ref,$header);
                        $set_header=0;
                    }
                    push(@$tbl_ref,$day_tbl_ref);
                }
            }
            my $records=recordCount($tbl_ref);

            if ($records > 0) {
                print $q->p("$discount Discounts found for store number $storeid for $startdate to $enddate");
                my @fieldsum = ('<b>Total:</b>','');
                my @sum_columns = (1,2,3,4);
                for my $datarows (1 .. $#$tbl_ref){
                    my $thisrow = @$tbl_ref[$datarows];
                    foreach my $c (@sum_columns) {
                        $fieldsum[$c] += trim($$thisrow[$c]);
                        $summary_totals[$c] += trim($$thisrow[$c]);
                    }
                }
                push @$tbl_ref, \@fieldsum;


                my @summary_format = (
                '',
                {'align' => 'Right', 'num' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},

                );
                my @links = ( "$scriptname?report=DISCOUNT_V2&store_selection=$orig_storeid&startdate=".'$_' );
                printResult_arrayref2($tbl_ref, \@links, \@summary_format);
            } else {
                print $q->p("No $discount Discount data found for $storeid for $startdate to $enddate");
            }

        }
        my @header=(
            '',
            "Quantity",
            "OrigPrice",
            "Discount",
            "SellPrice"
        );
        my @summary_array=\@header;
        push(@summary_array,\@summary_totals);
        if (recordCount(\@summary_array)) {
            print $q->p("-----Summary Total-------");
            my @summary_format = (
            '',
            {'align' => 'Right', 'num' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},

            );
            printResult_arrayref2(\@summary_array, '', \@summary_format);
        }

    }


    standardfooter();
} elsif ("$report" eq "NONMERCH") {
    print $q->h2("Non Merchandise Sales Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $summary=0;
    unless ($startdate eq $enddate) {
        $summary=1;
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");
	my %non_merch_hash=(

	'1' => 'GIFT CERTIFICATE',
	'2' => 'SHIPPING FEE',
	'3' => 'DONATIONS',
	'4' => 'PAID IN',
	'5' => 'PAID OUT',
	'6' => 'OTHER',
	'14' => 'GIFT CARD',
	'15' => 'BAG TAX',

	);

 	my %voucherid_hash=(
		'0' => 'ISSUE GIFT CARD',
		'6' => 'BALANCE INQ'
	);
	my %profile_prompt_hash=(
		'1' => 'LOCAL CHARGE INFO',
		'2' => 'DELIVERY PROMPTS',
		'3' => 'STORE NUMBER',
		'4' => 'STORE USE',
		'5' => 'CHECK INFO',
		'6' => 'PAID IN/OUT',
		'7' => 'RETURN INFO',
	);
	$query=dequote(<< "	ENDQUERY");

	SELECT nms.[TranId]
      ,nms.[Sequence]
      ,nms.[VoucherId]
      ,nms.[NonMerchType]
	  --,pr.[Response1]
      ,nms.[SellAmount]
      ,nms.[OrigAmount]
      ,nms.[AddAmount]
      ,nms.[RefNum]
      ,nms.[DiscountFlag]
      ,nms.[Qty]
      ,nms.[EmployeeSaleFlag]
      ,nms.[ProfilePromptFlag]
      ,nms.[AffectNetSales]
      ,nms.[ReasonCode]
      ,nms.[Tax1]
      ,nms.[Tax2]
      ,nms.[Tax3]
      ,nms.[Tax4]
      ,nms.[Tax5]

      ,nms.[TaxModify1]
      ,nms.[TaxModify2]
      ,nms.[TaxModify3]
      ,nms.[TaxModify4]
      ,nms.[TaxModify5]


      ,nms.[ExtTaxableAmount]
      ,nms.[UPC]
      ,nms.[ChoiceType]
      ,nms.[Reason]
      ,nms.[ProfileID]
      ,nms.[DiscountModifiedFlag]
      ,nms.[ItemDiscountableFlag]
      ,nms.[TxnDiscountableFlag]
      ,nms.[EncryptionKeyIndex]
      ,nms.[ReturnIdentifier]
      ,nms.[OrderCategory]

      ,nms.[procState]
      ,nms.[lastStateChange]
      ,nms.[uid]
	FROM
		[****TLog].[dbo].[tblNonMerchandiseSale]  nms
		--left outer join [****TLog].[dbo].[tblProfileResponse] pr on nms.tranid = pr.tranid

	WHERE nms.[TranId] in
		(select
			TranId
		FROM
			[****TLog].[dbo].[tblTranMaster]
		where
			[StoreId] = '$storeid'
			and [TranDateTime] > '$startdate'
			and [TranDateTime] < '$enddate 23:59:59'
		)

	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		if (recordCount($tbl_ref) > 1000) {
			print $q->p("Query result very large! Please reduce the scope of your query.");
		} else {

			my @links = ( "$scriptname?report=PROPROMPT&store_selection=$orig_storeid&tranid=".'$_' );
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];

				# Substitute known values
				if ($$thisrow[3] == 14) {
					my $dtype=$$thisrow[2];
					$$thisrow[2]=$voucherid_hash{$dtype};
				} else {
				}
				my $dtype=$$thisrow[3];
				$$thisrow[3]=$non_merch_hash{$dtype};

			}
			printResult_arrayref2($tbl_ref, \@links );
		}
	} else {
		print $q->p("Found no data in tblNonMerchandiseSale for $storeid between $startdate and $enddate");
	}
    standardfooter();
} elsif ("$report" eq "STOREVALUATION") {
    print $q->h2("Store Valuation Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");

	my %action_summary;
	my $issue_total=0;
	$query=dequote(<< "	ENDQUERY");

	SELECT [StoreId]
		,[ldate]
		,[items]
		,[units]
		,[extlastcost]
		,[extavgcost]
		,[extprice]
	FROM [****Data].[dbo].[tblStoreValueSummary]
	where
		[StoreId] = '$storeid'
		and [ldate] > '$startdate'
		and [ldate] < '$enddate 23:59:59'

	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {

        my @format = (
        '','','',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
		);
		printResult_arrayref2($tbl_ref, '',\@format );
	} else {
		print $q->p("Found no data in tblStoreValueSummary for $storeid between $startdate and $enddate");
	}
    standardfooter();



} elsif ("$report" eq "SKUSALE") {
    print $q->h2("SKU Sale History Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $SKU   = trim(($q->param("sku_selection"))[0]);
	my $summary_selection = trim(($q->param("summary_selection"))[0]);

    my $orig_storeid=$storeid;
	my $where_store=" and tm.StoreId in ('$storeid')";
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
	my $where_store=" and tm.StoreId in ('$storeid')";
	my $where_sku=" and	ms.SKU = '$SKU' ";
	unless ($SKU) {
		$where_sku='';
	}
	my $join_store=" and ssi.storeid = '$storeid'";
	if ($storeid =~ /all/i) {
		$where_store='';
		$join_store='';
		unless ($SKU) {
			print $q->h4("Error: If no SKU is specified, then a store must be specified.");
			standardfooter();
			exit;
		}		
	}
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");

	my %action_summary;
	my %sku_summary;

	$query=dequote(<< "	ENDQUERY");
		select
			distinct(tm.tranid),
			tm.StoreId,
			convert(DateTime, convert(varchar(19),tm.TranDateTime,21)) as TranDate,
			ms.SKU,
			ssi.descrip,
			ms.Quantity,
			ms.ExtSellPrice
		from ****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
			join [****Data].[dbo].[tblStoreSkuInfo] ssi on ssi.SKU = ms.SKU $join_store
		where
			tm.TranDateTime >= '$startdate'
		and
			tm.TranDateTime <= '$enddate 23:59:59'
		and
			tm.TranVoid = 0 and tm.IsPostVoided = 0
		and
			tm.TranModifier not in (2,6)						
		$where_sku
		$where_store
 

	ENDQUERY

	#print $q->pre("$query");
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = @$tbl_ref[$datarows];
			my $sku=$$thisrow[3];
			my $qty=$$thisrow[5];
			$sku_summary{$sku}+=$qty;
		}
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        my @format = (
        '','','','','',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
		);
		if ($summary_selection) {
			my @header=("SKU","QtySold");
			my @array_to_print=\@header;
			foreach my $sku (sort keys(%sku_summary)) {
				my $qty=$sku_summary{$sku};
				my @array=("$sku","$qty");
				push(@array_to_print,\@array);
			}
			my @format=('',{'align' => 'Right', 'num' => ''},);
			printResult_arrayref2(\@array_to_print, '', \@format);
		} else {
			printResult_arrayref2($tbl_ref, \@links,\@format );
		}
	} else {
		print $q->p("Found no sales data for $SKU for $storeid between $startdate and $enddate");
	}
    standardfooter();

} elsif ("$report" eq "SALESTAXESII") {
	# In this report, I want to create a Sales Summary Report much like what is created by the Net Sales report
	# in The Elder.  This means we want the following:
	# Section #A - Gross Merchandise Sales
	# 1) Taxable Items:  			Count and Amount
	# 2) Nontaxable Items:  		Count and Amount
	# 3) Total Merchandise Sales: 	Count and Amount (The sum of #1 & #2 above)
	# Section #B - Sales Tax
	# 1) Tax#1:		Count and Amount
	# 2) Tax#2:		Count and Amount
	# 3) Tax#X:		Count and Amount
	# Section #C - Reductions to Gross Sales
	# Returns:
	# 1) Taxable Items:		Count and Amount
	# 2) NonTaxable Items: 	Count and Amount
	# 3) Total:				Count and Amount
	# Discounts:
	# 1) Taxable Items:		Count and Amount
	# 2) NonTaxable Items: 	Count and Amount
	# 3) Total:				Count and Amount	
	#    Total Reductions	Count and Amount
	# Net Sales
	#    Taxable Items:		Count and Amount
	#    NonTaxable Items:  Count and Amount
	#	 Total:				Count and Amount
	#
	
    print $q->h2("Sales and Taxes Report");
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
 
    my @tmp=split(/-/,$store_selection);
    my $storeid=$tmp[0];
    my $storename=$tmp[1];
	
	my $detail_selection   = trim(($q->param("detail_selection"))[0]);
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
 
	# Don't allow too large of a time span
	if ($end_year_selection != $start_year_selection) {
		print $q->h4("Please restrict your query to one year at a time.");
		standardfooter();
		exit;
		
	}
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	my $display_method = 0;
 
	my $primary_select="convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate";
	my $final_primary_select="tms.TranDate";
	my $enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$enddate')";
	my $group_by = "group by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $order_by = "order by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $final_order_by = "order by tms.TranDate";
	my $final_join = "left outer join #tmpNonMerchSales nms on tms.TranDate = nms.TranDate
			left outer join #tmpTaxTotals ttt on tms.TranDate = ttt.TranDate
			left outer join #tmpDiscount td on tms.TranDate = td.TranDate";
	if ($date_selection) {		
		my $year=substr($date_selection,0,4);
		my $month=substr($date_selection,5,2);
		my $day=substr($date_selection,8,2);
		if ($day) {
			$display_method="by_txn";	
			$startdate="$year"."-"."$month"."-"."$day";
			$enddate = $startdate;
			$enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$startdate')";	
			$primary_select="tm.TranId";
			$final_primary_select="tms.TranId";
			$group_by = "group by tm.TranId";
			$order_by = "order by tm.TranId";
			$final_order_by = "order by tms.TranId";
			$final_join = "left outer join #tmpNonMerchSales nms on tms.TranId = nms.TranId
			left outer join #tmpTaxTotals ttt on tms.TranId = ttt.TranId
			left outer join #tmpDiscount td on tms.TranId = td.TranId";			
		} elsif ($month) {
		
			$display_method="by_day"; 
			$startdate="$year"."-"."$month"."-"."01";
			$enddate_where = "and tm.TranDateTime < dateadd( m, 1, '$startdate')"; 
			$enddate = "End of Month";
 			
		}	 
	} else {		
		if ($start_month_selection eq $end_month_selection) {			
			if ($start_year_selection eq $end_year_selection) {
				$display_method="by_day";				
				 
			} else {
				$display_method="by_month";					
			}
		} else {			
			$display_method="by_month";								
		}
	}	
 
	print $q->h3("Store: $store_selection - $startdate to $enddate");
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
 	# Query #1 - Find the Net Sales
	$query=dequote(<< "	ENDQUERY");		 
		select 
			convert(int,sign(ms.ExtOrigPrice) * ms.Quantity) as QtySold,
			ms.[ExtSellPrice] as SellAmt,
			ms.[ExtRegPrice] as RegAmt, 			
			ms.TranId as TranId,
			tm.TranNum as Txn	
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and 
				(ms.Tax1 = '1'
				or
				ms.Tax2 = '1'
				or
				ms.Tax3 = '1'
				or
				ms.Tax4 = '1'
				or
				ms.Tax5 = '1') 			 
			;
			
	ENDQUERY
	
	#print $q->pre("$query");
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	my @current_issues=();
	
	my %summary_hash;
	my %sales_tax_hash;
	my %transaction_hash;
	my %txn_hash;
	while (my @tmp = $sth->fetchrow_array()) {
			my $QtySold=$tmp[0];
			my $SellAmt=$tmp[1];
			my $RegAmt=$tmp[2];	
			my $TranId=$tmp[3];
			my $Txn=$tmp[4];			
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'QtySold'}+=$QtySold;
			$sales_tax_hash{'SellAmt'}+=$SellAmt;		
			$transaction_hash{$TranId}{'QtySold'} += $QtySold;	
			$transaction_hash{$TranId}{'SellAmt'} += $SellAmt;													 
	}	
	# Query #2 - Find the Net Returned
	$query=dequote(<< "	ENDQUERY");		
		select		 		
			ms.[Quantity] as QtyReturned,
			ms.[ExtRegPrice] as ReturnedAmt,
			ms.TranId as TranId,
			tm.TranNum as Txn	
	 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				ms.[ExtSellPrice] < 0				
			and 
				(ms.Tax1 = '1'
				or
				ms.Tax2 = '1'
				or
				ms.Tax3 = '1'
				or
				ms.Tax4 = '1'
				or
				ms.Tax5 = '1')		
 
			;
	ENDQUERY
	#print $q->pre("$query");	
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $QtyReturned=$tmp[0];
			my $ReturnedAmt=$tmp[1];
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];	
			$txn_hash{$TranId}=$Txn;			
			$sales_tax_hash{'QtyReturned'}+=$QtyReturned;
			$sales_tax_hash{'ReturnedAmt'}+=$ReturnedAmt;	 
			$transaction_hash{$TranId}{'QtyReturned'} += $QtyReturned;
			$transaction_hash{$TranId}{'ReturnedAmt'} += $ReturnedAmt;							 
	}	
	# Query #3	Non Taxable Net Sales
	$query=dequote(<< "	ENDQUERY");		
		select
			convert(int,sign(ms.ExtOrigPrice) * ms.Quantity) as QtySold,
			ms.[ExtSellPrice] as SellAmt,		
			ms.TranId as TranId,
			tm.TranNum as Txn 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and 
				(ms.Tax1 = '0'
				and
				ms.Tax2 = '0'
				and
				ms.Tax3 = '0'
				and
				ms.Tax4 = '0'
				and
				ms.Tax5 = '0')
 
			;
	ENDQUERY
	#print $q->pre("$query");	
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $QtySoldNT=$tmp[0];
			my $SellAmtNT=$tmp[1];
			my $TranId=$tmp[2];
			my $Txn=$tmp[3];	
			$txn_hash{$TranId}=$Txn;			
			$sales_tax_hash{'QtySoldNT'}+=$QtySoldNT;
			$sales_tax_hash{'SellAmtNT'}+=$SellAmtNT;		
			$transaction_hash{$TranId}{'QtySoldNT'} += $QtySoldNT;	
			$transaction_hash{$TranId}{'SellAmtNT'} += $SellAmtNT;		
		 			
	}		
	# Query #4 - Find NonTaxed Quantity Returned
	$query=dequote(<< "	ENDQUERY");	
		select		
 	
			ms.[Quantity] as QtyReturnedNT,
			ms.[ExtRegPrice] as ReturnedAmtNT,
			ms.TranId as TranId,
			tm.TranNum as Txn			 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				ms.[ExtSellPrice] < 0				
			and 
				(ms.Tax1 = '0'
				and
				ms.Tax2 = '0'
				and
				ms.Tax3 = '0'
				and
				ms.Tax4 = '0'
				and
				ms.Tax5 = '0')		
 
			;
	ENDQUERY
	#print $q->pre("$query");	
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $QtyReturnedNT=$tmp[0];
			my $ReturnedAmtNT=$tmp[1];
			my $TranId=$tmp[2];
			my $Txn=$tmp[3];	
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'QtyReturnedNT'}+=$QtyReturnedNT;
			$sales_tax_hash{'ReturnedAmtNT'}+=$ReturnedAmtNT;	 
			$transaction_hash{$TranId}{'QtyReturnedNT'} += $QtyReturnedNT;
			$transaction_hash{$TranId}{'ReturnedAmtNT'} += $ReturnedAmtNT;							
	}			
	# Query #5 - Find Tax 1
	$query=dequote(<< "	ENDQUERY");	
		-- Collect taxes1
		select		 
			tt.[Sequence] as TaxQty1,
			tt.[TaxAmount] as TaxAmt1,
			tt.[TaxableAmount] as TaxAbleAmt1,
			tm.TranId as TranId,
			tm.TranNum as Txn					 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 1
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			#my $TaxQty1=$tmp[0];
			my $TaxQty1++;
			my $TaxAmt1=$tmp[1];
			my $TaxAbleAmt1=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty1'}+=$TaxQty1;
			$sales_tax_hash{'TaxAmt1'}+=$TaxAmt1;
			$sales_tax_hash{'TaxAbleAmt1'}+=$TaxAbleAmt1;				
			$transaction_hash{$TranId}{'TaxQty1'} += $TaxQty1;
			$transaction_hash{$TranId}{'TaxAmt1'} += $TaxAmt1;
			$transaction_hash{$TranId}{'TaxAbleAmt1'} += $TaxAbleAmt1;			
	}		
	# Query #6
	$query=dequote(<< "	ENDQUERY");			
		-- Collect taxes2
		select		
 	
			tt.[Sequence] as TaxQty2,
			tt.[TaxAmount] as TaxAmt2,
			tt.[TaxableAmount] as TaxAbleAmt2,
			tm.TranId as TranId,
			tm.TranNum as Txn				
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 2
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty2++;
			my $TaxAmt2=$tmp[1];
			my $TaxAbleAmt2=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty2'}+=$TaxQty2;
			$sales_tax_hash{'TaxAmt2'}+=$TaxAmt2;
			$sales_tax_hash{'TaxAbleAmt2'}+=$TaxAbleAmt2;				
			$transaction_hash{$TranId}{'TaxQty2'} += $TaxQty2;
			$transaction_hash{$TranId}{'TaxAmt2'} += $TaxAmt2;
			$transaction_hash{$TranId}{'TaxAbleAmt2'} += $TaxAbleAmt2;			
	}		
	# Query #7
	$query=dequote(<< "	ENDQUERY");		
		-- Collect taxes3
		select		
		 
			tt.[Sequence] as TaxQty3,
			tt.[TaxAmount] as TaxAmt3,
			tt.[TaxableAmount] as TaxAbleAmt3,
			tm.TranId as TranId,
			tm.TranNum as Txn			
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 3
 		 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty3++;
			my $TaxAmt3=$tmp[1];
			my $TaxAbleAmt3=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty3'}+=$TaxQty3;
			$sales_tax_hash{'TaxAmt3'}+=$TaxAmt3;
			$sales_tax_hash{'TaxAbleAmt3'}+=$TaxAbleAmt3;				
			$transaction_hash{$TranId}{'TaxQty3'} += $TaxQty3;
			$transaction_hash{$TranId}{'TaxAmt3'} += $TaxAmt3;
			$transaction_hash{$TranId}{'TaxAbleAmt3'} += $TaxAbleAmt3;			
	}		
	# Query #8
	$query=dequote(<< "	ENDQUERY");				

		-- Collect taxes4
		select		
 
			tt.[Sequence] as TaxQty4,
			tt.[TaxAmount] as TaxAmt4,
			tt.[TaxableAmount] as TaxAbleAmt4,
			tm.TranId as TranId,
			tm.TranNum as Txn					
		 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 4
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty4++;
			my $TaxAmt4=$tmp[1];
			my $TaxAbleAmt4=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty4'}+=$TaxQty4;
			$sales_tax_hash{'TaxAmt4'}+=$TaxAmt4;
			$sales_tax_hash{'TaxAbleAmt4'}+=$TaxAbleAmt4;				
			$transaction_hash{$TranId}{'TaxQty4'} += $TaxQty4;
			$transaction_hash{$TranId}{'TaxAmt4'} += $TaxAmt4;
			$transaction_hash{$TranId}{'TaxAbleAmt4'} += $TaxAbleAmt4;		
	}			
	# Query #9
	$query=dequote(<< "	ENDQUERY");				

		-- Collect taxes5
		select					
			tt.[Sequence] as TaxQty5,
			tt.[TaxAmount] as TaxAmt5,
			tt.[TaxableAmount] as TaxAbleAmt5,
			tm.TranId as TranId,
			tm.TranNum as Txn						 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTax] tt on tm.TranId=tt.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
			and
				tt.[TaxId] = 5
 			 
			;	
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $TaxQty5++;
			my $TaxAmt5=$tmp[1];
			my $TaxAbleAmt5=$tmp[2];
			my $TranId=$tmp[3]; 
			my $Txn=$tmp[4];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'TaxQty5'}+=$TaxQty5;
			$sales_tax_hash{'TaxAmt5'}+=$TaxAmt5;
			$sales_tax_hash{'TaxAbleAmt5'}+=$TaxAbleAmt5;				
			$transaction_hash{$TranId}{'TaxQty5'} += $TaxQty5;
			$transaction_hash{$TranId}{'TaxAmt5'} += $TaxAmt5;
			$transaction_hash{$TranId}{'TaxAbleAmt5'} += $TaxAbleAmt5;			
	}			

	# Query #10 Taxable Discount Data
	$query=dequote(<< "	ENDQUERY");				
			
		-- Discount from Merchandise Sale table
		
		select 
			ms.[Quantity] as DisQty,
			(ms.[ExtRegPrice] - ms.[ExtSellPrice]) as DisAmt,
			tm.TranId as TranId,
			tm.TranNum as Txn						 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')   
			and 
				(ms.Tax1 = '1'
				or
				ms.Tax2 = '1'
				or
				ms.Tax3 = '1'
				or
				ms.Tax4 = '1'
				or
				ms.Tax5 = '1')
			and
				ms.[DiscountFlag] = 1;
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $DisQty=$tmp[0];
			my $DisAmt=$tmp[1]; 
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'DisQty'}+=$DisQty;
			$sales_tax_hash{'DisAmt'}+=$DisAmt;			
			$transaction_hash{$TranId}{'DisQty'} += $DisQty;
			 
			$transaction_hash{$TranId}{'DisAmt'} += $DisAmt;			 		
	}		
	# Query #11 - Not Taxable Discounts
	$query=dequote(<< "	ENDQUERY");					 				
		-- Non Taxable Discounts from merchandise sale table
		
		select
	 
			ms.[Quantity] as DisQtyNT,
			(ms.[ExtRegPrice] - ms.[ExtSellPrice]) as DisAmtNT, 
			tm.TranId as TranId,
			tm.TranNum as Txn	 		
			 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
 
			and 
				(ms.Tax1 = '0'
				and
				ms.Tax2 = '0'
				and
				ms.Tax3 = '0'
				and
				ms.Tax4 = '0'
				and
				ms.Tax5 = '0')
			and
				ms.[DiscountFlag] = 1				
 
			;		
	ENDQUERY
	#print $q->pre("$query");
	$sth=$dbh->prepare("$query");
	$sth->execute();
 
	while (my @tmp = $sth->fetchrow_array()) { 			
			my $DisQtyNT=$tmp[0];
			my $DisAmtNT=$tmp[1]; 
			my $TranId=$tmp[2]; 
			my $Txn=$tmp[3];
			$txn_hash{$TranId}=$Txn;
			$sales_tax_hash{'DisQtyNT'}+=$DisQtyNT;
			$sales_tax_hash{'DisAmtNT'}+=$DisAmtNT;		 				
			$transaction_hash{$TranId}{'DisQtyNT'} += $DisQtyNT;
			$transaction_hash{$TranId}{'DisAmtNT'} += $DisAmtNT;			 		
	}			

 
	my @header = ("TranId","Txn","QtySold","SellAmt","QtySoldNT","SellAmtNT","TaxQty1","TaxAmt1","TaxQty2","TaxAmt2","TaxQty3","TaxAmt3","TaxQty4","TaxAmt4","TaxQty5","TaxAmt5");
	my @array_to_print=\@header;
	
	my @totals = ("Total",'','','','');
	my @reductions_header = ("TranId","Txn","QtyReturned","ReturnedAmt","QtyReturnedNT","ReturnedAmtNT","DisQty","DisAmt","DisQtyNT","DisAmtNT");
	my @reductions_array_to_print=\@reductions_header;
	my @reductions_totals = ("Total",'','','','','','');
	
	#my ($QtySold,$SellAmt,$QtySoldNT,$SellAmtNT,$TaxQty1,$TaxAmt1,$TaxQty2,$TaxAmt2,$TaxQty3,$TaxAmt3,$TaxQty4,$TaxAmt4,$TaxQty5,$TaxAmt5) = 0;
	#my ($QtyReturned,$ReturnedAmt,$QtyReturnedNT,$ReturnedAmtNT,$DisQty,$DisAmt,$DisQtyNT,$DisAmtNT)=0;
	my %sales_taxid_hash;
	my $taxable_count=0;
	my $nontaxable_count=0;
	foreach my $TranId (sort keys(%transaction_hash)) {
		# The Net Sale Qty and Amt For Taxable and NonTaxable
		my $QtySold = $transaction_hash{$TranId}{'QtySold'};
		my $SellAmt = $transaction_hash{$TranId}{'SellAmt'};
		my $QtySoldNT = $transaction_hash{$TranId}{'QtySoldNT'};
		my $SellAmtNT = $transaction_hash{$TranId}{'SellAmtNT'};	
		# The Taxes
		my $TaxQty1 = $transaction_hash{$TranId}{'TaxQty1'};
		my $TaxAmt1 = $transaction_hash{$TranId}{'TaxAmt1'};
		my $TaxQty2 = $transaction_hash{$TranId}{'TaxQty2'};
		my $TaxAmt2 = $transaction_hash{$TranId}{'TaxAmt2'};
		my $TaxQty3 = $transaction_hash{$TranId}{'TaxQty3'};
		my $TaxAmt3 = $transaction_hash{$TranId}{'TaxAmt3'};
		my $TaxQty4 = $transaction_hash{$TranId}{'TaxQty4'};
		my $TaxAmt4 = $transaction_hash{$TranId}{'TaxAmt4'};
		my $TaxQty5 = $transaction_hash{$TranId}{'TaxQty5'};
		my $TaxAmt5 = $transaction_hash{$TranId}{'TaxAmt5'};	
		if ($TaxQty1) {
			$sales_taxid_hash{'Tax1'} -> [0] += $TaxQty1;
			$sales_taxid_hash{'Tax1'} -> [1] += $TaxAmt1;
		} 
		if ($TaxQty2) {
			$sales_taxid_hash{'Tax2'} -> [0] += $TaxQty2;
			$sales_taxid_hash{'Tax2'} -> [1] += $TaxAmt2;		
		} 
		if ($TaxQty3) {
			$sales_taxid_hash{'Tax3'} -> [0] += $TaxQty3;
			$sales_taxid_hash{'Tax3'} -> [1] += $TaxAmt3;			
		} 
		if ($TaxQty4) {
			$sales_taxid_hash{'Tax4'} -> [0] += $TaxQty4;
			$sales_taxid_hash{'Tax4'} -> [1] += $TaxAmt4;			
		} 
		if ($TaxQty5) {
			$sales_taxid_hash{'Tax5'} -> [0] += $TaxQty5;
			$sales_taxid_hash{'Tax5'} -> [1] += $TaxAmt5;			
		}						
		$totals[2]+=$QtySold;
		$totals[3]+=$SellAmt;
		$totals[4]+=$QtySoldNT;
		$totals[5]+=$SellAmtNT;	
		$totals[6]+=$TaxQty1;				
		$totals[7]+=$TaxAmt1;
		$totals[8]+=$TaxQty2;				
		$totals[9]+=$TaxAmt2;
		$totals[0]+=$TaxQty3;				
		$totals[11]+=$TaxAmt3;
		$totals[12]+=$TaxQty4;				
		$totals[13]+=$TaxAmt4;
		$totals[14]+=$TaxQty5;				
		$totals[15]+=$TaxAmt5;	
		# The Returns & Discounts
		my $QtyReturned = $transaction_hash{$TranId}{'QtyReturned'} ;
		my $ReturnedAmt = $transaction_hash{$TranId}{'ReturnedAmt'};
		my $QtyReturnedNT = $transaction_hash{$TranId}{'QtyReturnedNT'};
		my $ReturnedAmtNT = $transaction_hash{$TranId}{'ReturnedAmtNT'};		
		my $DisQty = $transaction_hash{$TranId}{'DisQty'};
		my $DisAmt = $transaction_hash{$TranId}{'DisAmt'};
	 
		my $DisQtyNT = $transaction_hash{$TranId}{'DisQtyNT'};
		my $DisAmtNT = $transaction_hash{$TranId}{'DisAmtNT'};		
		
		$reductions_totals[2]+=$QtyReturned;
		$reductions_totals[3]+=$ReturnedAmt;
		$reductions_totals[4]+=$QtyReturnedNT;
		$reductions_totals[5]+=$ReturnedAmtNT;
		$reductions_totals[6]+=$DisQty;
		$reductions_totals[7]+=$DisAmt;	
		$reductions_totals[8]+=$DisQtyNT;
		$reductions_totals[9]+=$DisAmtNT;
		# Calculate the Gross by adding the Discounts and Returns to the Net
		$QtySold += $QtyReturned;
		$sales_tax_hash{'QtySold'} += $QtyReturned;		
		# Return amount is negative remember.
		$SellAmt -= $ReturnedAmt;
		$sales_tax_hash{'SellAmt'} -= $ReturnedAmt;
		$SellAmt -= $DisAmt;
		$sales_tax_hash{'SellAmt'} += $DisAmt;	
		# Repeat for Non Taxable
		$QtySoldNT += $QtyReturnedNT;
		$sales_tax_hash{'QtySoldNT'} += $QtyReturnedNT;
		$SellAmtNT -= $ReturnedAmtNT;
		$sales_tax_hash{'SellAmtNT'} -= $ReturnedAmtNT;
		$SellAmtNT -= $DisAmtNT;
		$sales_tax_hash{'SellAmtNT'} += $DisAmtNT;
		
		# Collect the counts
 
		if (($QtySold != 0) || ($SellAmt != 0)) {
			$taxable_count++;
		}
		if (($QtySoldNT != 0) || ($SellAmtNT != 0)) {
			$nontaxable_count++;
		}		
		my $Txn=$txn_hash{$TranId};
		my @array=("$TranId","$Txn","$QtySold","$SellAmt","$QtySoldNT","$SellAmtNT","$TaxQty1","$TaxAmt1","$TaxQty2","$TaxAmt2","$TaxQty3","$TaxAmt3","$TaxQty4","$TaxAmt4","$TaxQty5","$TaxAmt5");
		push (@array_to_print,\@array);	
		my @reductions_array=("$TranId","$Txn","$QtyReturned","$ReturnedAmt","$QtyReturnedNT","$ReturnedAmtNT","$DisQty","$DisAmt","$DisQtyNT","$DisAmtNT");
		push (@reductions_array_to_print,\@reductions_array);		
	}
	push (@array_to_print,\@totals);
	push (@reductions_array_to_print,\@reductions_totals);	
	
	if ($detail_selection) {	# This can be used for some debugging.
		if (recordCount(\@array_to_print)) {	
			print $q->h3("Gross Merchandise Sales");
			printResult_arrayref2(\@array_to_print );
		} else {
			print $q->p("Found no data for Sales and Taxes for $storeid between $startdate and $enddate");
		}
		if (recordCount(\@reductions_array_to_print)) {	
			print $q->h3("Reductions to Gross Sales");
			printResult_arrayref2(\@reductions_array_to_print );
		} else {
			print $q->p("Found no data for Sales and Taxes for $storeid between $startdate and $enddate");
		}		
	}
 
	# Print out the results:
	my $TotalQtySold = ($sales_tax_hash{'QtySold'} + $sales_tax_hash{'QtySoldNT'});
	my $TotalSellAmt = ($sales_tax_hash{'SellAmt'} + $sales_tax_hash{'SellAmtNT'});	
	my $QtyTotalMerchSales=($sales_tax_hash{'QtySold'} + $sales_tax_hash{'QtySoldNT'});

	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	# Part I - Gross Merch Sales
	print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Category")),
				$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Count")),
				$q->td({-class=>'tablehead', -nowrap=>undef, -align=>'center'}, $q->b("Amount")),
			   );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Gross Merch Sales:")));

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtySold'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'SellAmt'})),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtySoldNT'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'SellAmtNT'})),
			   );
	my $gamt = ($sales_tax_hash{'SellAmt'} + $sales_tax_hash{'SellAmtNT'});
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Merch Sales:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $QtyTotalMerchSales),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($gamt)),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Sales Tax:")));   
	my $tax_amt_total=0;
	my $tax_cnt_total=0;
	foreach my $tax_id (sort keys(%sales_taxid_hash)) {
		my $ref=$sales_taxid_hash{$tax_id};
		my @array=@$ref;
		my $tax_count=$array[0];        
		my $tax_amt_sum=$array[1];
		$tax_amt_total+=$tax_amt_sum;
		$tax_cnt_total+=$tax_count;
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "$tax_id:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $tax_count),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($tax_amt_sum)),
			   );
	}       
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Taxes:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $tax_cnt_total),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($tax_amt_total)),
			   ); 
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "============="),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "================"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
			   );               		
	my $gross_sales=($gamt + $tax_amt_total) ; 
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Gross Sales:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($gross_sales)),
			   );    
	# Part II - Reductions to Gross Merch Sales			   
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Reductions to Gross Sales")));               
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Return:")));
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtyReturned'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'ReturnedAmt'})),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'QtyReturnedNT'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'ReturnedAmtNT'})),
			   );
	my $rcnt = ($sales_tax_hash{'QtyReturned'} + $sales_tax_hash{'QtyReturnedNT'});
	my $ramt = ($sales_tax_hash{'ReturnedAmt'} + $sales_tax_hash{'ReturnedAmtNT'});
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $rcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($ramt)),
			   );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Discounts:")));

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'DisQty'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'DisAmt'})),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Nontaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $sales_tax_hash{'DisQtyNT'}),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($sales_tax_hash{'DisAmtNT'})),
			   );
			   
	my $dcnt = ($sales_tax_hash{'DisQty'} + $sales_tax_hash{'DisQtyNT'});
	my $damt = ($sales_tax_hash{'DisAmt'} + $sales_tax_hash{'DisAmtNT'});				   
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $dcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($damt)),
			   );
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "============="),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "================"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
				$q->td({-class=>'tabledatanb', -align=>'right'}, "======="),
			   );                 
	my $total_reductions=($rcnt + $dcnt);
	my $total_reductionsamt=((abs $ramt) + $damt);
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total Reductions:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $total_reductions),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($total_reductionsamt)),
			   );
	# Part III - Net Sales
	my $nTcnt = ($sales_tax_hash{'QtySold'} - $sales_tax_hash{'QtyReturned'});	
	my $nTamt = ($sales_tax_hash{'SellAmt'} - (abs $sales_tax_hash{'ReturnedAmt'}) - $sales_tax_hash{'DisAmt'});
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>4}, $q->b("Net Sales:")));
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Taxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $nTcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($nTamt)),
			   );
	my $nNTcnt = ($sales_tax_hash{'QtySoldNT'} - $sales_tax_hash{'QtyReturnedNT'});				   
	my $nNTamt = ($sales_tax_hash{'SellAmtNT'} - (abs $sales_tax_hash{'ReturnedAmtNT'}) - $sales_tax_hash{'DisAmtNT'});	
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "NonTaxable Items:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $nNTcnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($nNTamt)),
			   );
	my $ncnt = ($nTcnt + $nNTcnt);
	my $namt = ($nTamt + $nNTamt);
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right', -width=>50}, "&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Total:"),
				$q->td({-class=>'tabledatanb', -align=>'right'}, $ncnt),
				$q->td({-class=>'tabledatanb', -align=>'right'}, formatCurrency($namt)),
			   );

	print $q->end_table(),"\n";			
	
	
	$dbh->disconnect;
	standardfooter();

	# End of SALESTAXESII
} elsif ("$report" eq "SALESTAXES_orig") {
    print $q->h2("Sales and Taxes Report");
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
 
    my @tmp=split(/-/,$store_selection);
    my $storeid=$tmp[0];
    my $storename=$tmp[1];
	
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
	
    #my $data_selection   = trim(($q->param("data_selection"))[0]);
    #my $company_selection   = trim(($q->param("company_selection"))[0]);
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	my $display_method = 0;
	# I would ike to eventually be able to display by month, by day, or show all transactions for a day.
	# this is not developed yet. - kdg
	# Determine how to display the info.  If the request spans several months, then show it by month.
	# If it is all in one month but spans several days, then show it by day.
	# If only one day is selected, then show all of the transactions for that day	
	# By Year - Show each year on a row
	# By Month - Show each month on a row
	# By Day - Show each day on a row
	# By Txn - Show each transaction on a row
	my $primary_select="convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate";
	my $final_primary_select="tms.TranDate";
	my $enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$enddate')";
	my $group_by = "group by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $order_by = "order by convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $final_order_by = "order by tms.TranDate";
	my $final_join = "left outer join #tmpNonMerchSales nms on tms.TranDate = nms.TranDate
			left outer join #tmpTaxTotals ttt on tms.TranDate = ttt.TranDate
			left outer join #tmpDiscount td on tms.TranDate = td.TranDate";
	if ($date_selection) {		
		my $year=substr($date_selection,0,4);
		my $month=substr($date_selection,5,2);
		my $day=substr($date_selection,8,2);
		if ($day) {
			$display_method="by_txn";	
			$startdate="$year"."-"."$month"."-"."$day";
			$enddate = $startdate;
			$enddate_where = "and tm.TranDateTime < dateadd( d, 1, '$startdate')";	
			$primary_select="tm.TranId";
			$final_primary_select="tms.TranId";
			$group_by = "group by tm.TranId";
			$order_by = "order by tm.TranId";
			$final_order_by = "order by tms.TranId";
			$final_join = "left outer join #tmpNonMerchSales nms on tms.TranId = nms.TranId
			left outer join #tmpTaxTotals ttt on tms.TranId = ttt.TranId
			left outer join #tmpDiscount td on tms.TranId = td.TranId";			
		} elsif ($month) {
		
			$display_method="by_day"; 
			$startdate="$year"."-"."$month"."-"."01";
			$enddate_where = "and tm.TranDateTime < dateadd( m, 1, '$startdate')"; 
			$enddate = "End of Month";
 			
		}	 
	} else {		
		if ($start_month_selection eq $end_month_selection) {			
			if ($start_year_selection eq $end_year_selection) {
				$display_method="by_day";				
				 
			} else {
				$display_method="by_month";					
			}
		} else {			
			$display_method="by_month";								
		}
	}	
 
	print $q->h3("Store: $store_selection - $startdate to $enddate");
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
	 
	$query=dequote(<< "	ENDQUERY");		 
		select				 			
			$primary_select,
			sum(ms.ExtSellPrice) as [MSNet]
			--sum(ms.ExtTaxableAmt) as [MSTaxableAmt]
			into #tmpMerchSales
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId 		   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')  
		$group_by						 
		$order_by;
		 
		select				 			
			$primary_select,			
			sum(nms.SellAmount) as [NMSNet]
			--sum(nms.ExtTaxableAmount) as [NMSTaxableAmt]				
			into #tmpNonMerchSales
		from 
			****Tlog..tblTranMaster tm				
			join ****Tlog..tblNonMerchandiseSale nms on tm.TranId=nms.TranId   
		where 
			tm.TranDateTime >= '$startdate'			
			$enddate_where
			and -- add 1 day for an exclusive upper bound
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
				tm.StoreId in ('$storeid')                 
		$group_by						
		$order_by;		
			
		select			 			
			$primary_select,								
			0 as [PaymentTotal],
			sum(tt.[TotalSale]) as [SaleTotal],
			sum(tt.[Tax1]) as [Tax1],		 
			sum(tt.[Tax2]) as [Tax2],		 
			sum(tt.[Tax3]) as [Tax3],			 
			sum(tt.[Tax4]) as [Tax4],			
			sum(tt.[Tax5]) as [Tax5],
			sum(tt.[ManualTaxTotals]) as [ManTax] 
			into #tmpTaxTotals
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTotal] tt on tm.TranId=tt.TranId
		where 
			tm.TranDateTime >= '$startdate' 			
			$enddate_where
			and
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and
				tm.StoreId in ('$storeid')
		$group_by						 
		$order_by;
		 		

		-- Get Discounts
        select			
			$primary_select,            
            sum(ds.ExtDiscountAmt) as [Discounts]
			into #tmpDiscount
        from ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
 
        where tm.TranDateTime >= '$startdate'
		$enddate_where        
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')       
		$group_by						 
		$order_by;
		
		-- Get the requested data
		select			 
			$final_primary_select,			 
			(ttt.SaleTotal + ttt.Tax1 + ttt.Tax2 + ttt.Tax3 + ttt.Tax4 + ttt.Tax5 + ttt.ManTax) as PaymentTotal,
			ttt.SaleTotal,				
			tms.MSNet,
			--tms.MSTaxableAmt,
			nms.NMSNet,
			--nms.NMSTaxableAmt,
			ttt.Tax1,
			ttt.Tax2,
			ttt.Tax3,
			ttt.Tax4,
			ttt.Tax5,
			ttt.ManTax,
			td.Discounts
			
		from
			#tmpMerchSales tms
			$final_join
			$final_order_by;
			
	drop table #tmpMerchSales;
	drop table #tmpNonMerchSales;
	drop table #tmpTaxTotals;
	drop table #tmpDiscount;
	
	ENDQUERY
	
	#print $q->pre("$query");

		#print $q->pre("$query");
    if ($display_method eq "by_month") {
		print $q->h3("By Month: query");
		#print $q->pre("$query");
		#$tbl_ref = execQuery_arrayref($dbh, $query);
	}	
    if ($display_method eq "by_day") {
		print $q->h3("By Day: query");
		#print $q->pre("$query");
		#$tbl_ref = execQuery_arrayref($dbh, $query);
	}		
    if ($display_method eq "by_txn") {
		print $q->h3("By Txn: query");
		#print $q->pre("$query2");
		#$tbl_ref = execQuery_arrayref($dbh, $query2);
	}		
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	# Process the query results differently depending on whether we are viewing by year, month, or day
    if ($display_method eq "by_month") {
		print $q->h3("By Month: display");
		# Need to group the results by month.
 
		# Total each day of the month
		my %month_hash;
		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];
		my @header=@$header_ref;
		my @array_to_print;
		my @totals_array_to_print;
		push(@array_to_print,$header_ref);
		$header[0]='';
		push(@header,"TotalTax");
		push(@totals_array_to_print,\@header);
 		
		if (recordCount($tbl_ref)) { 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				my $date=$$thisrow[0];
				my $month_date=substr($date,0,7);				
				$month_hash{$month_date} -> [0] = $month_date;
				$month_hash{$month_date} -> [1] += $$thisrow[1];		
				$month_hash{$month_date} -> [2] += $$thisrow[2];
				$month_hash{$month_date} -> [3] += $$thisrow[3];
				$month_hash{$month_date} -> [4] += $$thisrow[4];
				$month_hash{$month_date} -> [5] += $$thisrow[5];
				$month_hash{$month_date} -> [6] += $$thisrow[6];
				$month_hash{$month_date} -> [7] += $$thisrow[7];
				$month_hash{$month_date} -> [8] += $$thisrow[8];
				$month_hash{$month_date} -> [9] += $$thisrow[9];
				$month_hash{$month_date} -> [10] += $$thisrow[10];
				$month_hash{$month_date} -> [11] += $$thisrow[11];
				$month_hash{$month_date} -> [12] += $$thisrow[12];
				$month_hash{$month_date} -> [13] += $$thisrow[13];
			}	
		}	
 	
		foreach my $date (sort keys(%month_hash)) {
			my $v0 = $month_hash{$date} -> [0];
			my $v1 = $month_hash{$date} -> [1];
			my $v2 = $month_hash{$date} -> [2];
			my $v3 = $month_hash{$date} -> [3];
			my $v4 = $month_hash{$date} -> [4];
			my $v5 = $month_hash{$date} -> [5];
			my $v6 = $month_hash{$date} -> [6];
			my $v7 = $month_hash{$date} -> [7];
			my $v8 = $month_hash{$date} -> [8];
			my $v9 = $month_hash{$date} -> [9];
			my $v10 = $month_hash{$date} -> [10];
			my $v11 = $month_hash{$date} -> [11];
			my $v12 = $month_hash{$date} -> [12];
			my $v13 = $month_hash{$date} -> [13];
			 
			my @array=(
				
				"$v0",
				"$v1",
				"$v2",
				"$v3",
				"$v4",
				"$v5",
				"$v6",
				"$v7",				
				"$v8",	
				"$v9",	
				"$v10",	
				"$v11",		
				"$v12", 			
				"$v13", 
				);
			push(@array_to_print,\@array);
							 
			$fieldsum[1] += $month_hash{$date} -> [1];
			$fieldsum[2] += $month_hash{$date} -> [2];
			$fieldsum[3] += $month_hash{$date} -> [3];
			$fieldsum[4] += $month_hash{$date} -> [4];
			$fieldsum[5] += $month_hash{$date} -> [5];
			$fieldsum[6] += $month_hash{$date} -> [6];
			$fieldsum[7] += $month_hash{$date} -> [7];
			$fieldsum[8] += $month_hash{$date} -> [8]; 
			$fieldsum[9] += $month_hash{$date} -> [9]; 
			$fieldsum[10] += $month_hash{$date} -> [10]; 
			$fieldsum[11] += $month_hash{$date} -> [11]; 
			$fieldsum[12] += $month_hash{$date} -> [12]; 
			$fieldsum[13] += $month_hash{$date} -> [13]; 
 		}		
		# Get the total tax and add it to the end of the totals.
		
		my $total_tax;
		for my $field (7 .. 12) {
			$total_tax += $fieldsum[$field];
		}		
		push(@fieldsum,$total_tax);
		push(@totals_array_to_print,\@fieldsum);
 
		if (recordCount(\@array_to_print)) {				 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},					
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
			);
			my @links = ("$scriptname?report=SALESTAXES&store_selection=$store_selection&date_selection=".'$_' ); 			
			printResult_arrayref2(\@array_to_print, \@links, \@format );
			printResult_arrayref2(\@totals_array_to_print, '', \@format );

		} else {
			print $q->p("Found no data in tblStoreTraffic for $storeid between $startdate and $enddate");
		}	

	}

	if (($display_method eq "by_day") ||($display_method eq "by_txn"))  {		
 		#$tbl_ref = execQuery_arrayref($dbh, $query);
		print $q->h3("Display Method: $display_method");
		# Prepare to collect a separate table for the totals
		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];
		my @header=@$header_ref;
		my @array_to_print;
		my @totals_array_to_print;
		push(@array_to_print,$header_ref);
		$header[0]='';
		push(@header,"TotalTax");
		push(@totals_array_to_print,\@header);		
		if (recordCount($tbl_ref)) {
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				for my $field (1 .. $#$thisrow) {
					$fieldsum[$field] += $$thisrow[$field];
				}
			}
			# Get the total tax and add it to the end of the totals.
			
			my $total_tax;
			for my $field (7 .. 12) {
				$total_tax += $fieldsum[$field];
			}
			
			push(@fieldsum,$total_tax);
			push(@totals_array_to_print,\@fieldsum);
		}
		
		if (recordCount($tbl_ref)) {				
 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},	
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},					
				{'align' => 'Right', 'currency' => ''},	
			);
			my @links = ("$scriptname?report=SALESTAXES&store_selection=$store_selection&date_selection=".'$_' );
			if ($display_method eq "by_txn") {			
				@links = ("$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
			}
			
			printResult_arrayref2($tbl_ref, \@links, \@format );
			printResult_arrayref2(\@totals_array_to_print, '', \@format );

		} else {
			print $q->p("Found no data in tblStoreTraffic for $storeid between $startdate and $enddate");
		}
    }	
 	
	$dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
	standardfooter();

} elsif ("$report" eq "NETSALES2_what") {
	# Not certain why this is still here.
    print $q->h2("Net Sales Report");	
	my $storeid   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $company_selection   = trim(($q->param("company_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    my $summary=0;
    my %storeData_hash;
    my %tlog_hash;
    my $where;
    my $twhere;
    my $tjoin;
    if ($company_selection) {
        print $q->p("Showing Villages Sales Only");
        $where = " and VendorID = '01' ";
        $tjoin = "left outer join [****Data].[dbo].[tblStoreSkuInfo] ssi on SSI.SKU COLLATE DATABASE_DEFAULT = ms.SKU and ssi.StoreId = '$storeid'";
        $twhere = " and ssi.vendorid = '01'";
    }

    # Get the storedata
     if ($enddate eq $startdate) {
        print $q->p("For $startdate");
		# On rare occasions, transactions get duplicated  Here is a check for that situation
        $query=dequote(<< "        ENDQUERY");
            select n.StoreId,
				 n.Transnum,
                 right('00' + convert(varchar(2), n.RegNum) ,2) as RegNum,
                 case when n.TxnNum >= 10000 then n.TxnNum-10000 else n.TxnNum end as TxnNum,
                 convert(DateTime, convert(varchar(10),n.TranDateTime,21)) as TranDate,
                 n.Qty,
                 n.NetAmt

            from ****Data..tblStoreNetSales n
            where TranDateTime >= '$startdate' and TranDateTime < dateadd( d, 1, '$enddate') and -- add 1 day for an exclusive upper bound
                    n.StoreId in ('$storeid') and
                    substring(n.TxnModifier,1,1) not in (2,6) -- Exclude layaway sales and cancels
                    $where

            order by n.TxnNum
        ENDQUERY

        my $sth=$dbh->prepare("$query");
        $sth->execute();
		my %txn_hash;
		my %transnum_hash;
        while (my @tmp = $sth->fetchrow_array()) {
            my $StoreId=$tmp[0];
			my $transnum=$tmp[1];
            my $RegNum=$tmp[2];
            my $txnNum=$tmp[3];
            my $TranDate=$tmp[4];
            my $Qty=$tmp[5];
            my $Net=$tmp[6];
			$transnum_hash{$transnum}=\@tmp;
			if ($txn_hash{$txnNum}) {
				if ($transnum != $txn_hash{$txnNum}) {
					print "<br />Warning: Txn $txnNum is associated with $transnum and $txn_hash{$txnNum}<br />";
					print "Store: $StoreId Transnum: $transnum Reg: $RegNum Txn: $txnNum Date: $TranDate Qty: $Qty Net: $Net<br />";
					my $ref=$transnum_hash{$txn_hash{$txnNum}};
					my @array=@$ref;
					my $StoreId=$array[0];
					my $transnum=$array[1];
					my $RegNum=$array[2];
					my $txnNum=$array[3];
					my $TranDate=$array[4];
					my $Qty=$array[5];
					my $Net=$array[6];
					print "Store: $StoreId <b>Transnum: $transnum</b> Reg: $RegNum Txn: $txnNum Date: $TranDate Qty: $Qty Net: $Net<br />";
				}
			}
			$txn_hash{$txnNum}=$transnum;

		}

        # Print detail if the request was only for one day.
        $query=dequote(<< "        ENDQUERY");
            select n.StoreId,
                 right('00' + convert(varchar(2), n.RegNum) ,2) as RegNum,
                 case when n.TxnNum >= 10000 then n.TxnNum-10000 else n.TxnNum end as TxnNum,
                 convert(DateTime, convert(varchar(10),n.TranDateTime,21)) as TranDate,
                 sum(n.Qty) as [Qty],
                 sum(n.NetAmt) as [Net]

            from ****Data..tblStoreNetSales n
            where TranDateTime >= '$startdate' and TranDateTime < dateadd( d, 1, '$enddate') and -- add 1 day for an exclusive upper bound
                    n.StoreId in ('$storeid') and
                    substring(n.TxnModifier,1,1) not in (2,6) -- Exclude layaway sales and cancels
                    $where
            group by n.StoreId, n.RegNum, n.TxnNum,
                        convert(DateTime, convert(varchar(10),n.TranDateTime,21))
            order by n.StoreId, n.RegNum,
                        convert(DateTime, convert(varchar(10),n.TranDateTime,21)), n.TxnNum
        ENDQUERY

		#print $q->pre($query);
        $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $StoreId=$tmp[0];
            my $RegNum=$tmp[1];
            my $txnNum=$tmp[2];
            my $TranDate=$tmp[3];
            my $Qty=$tmp[4];
            my $Net=$tmp[5];

            my @array=(
                $StoreId,
                $RegNum,
                $TranDate,
                $Qty,
                $Net
            );
            $storeData_hash{$txnNum}=\@array;
        }
    } else {
        print $q->p("For $startdate to $enddate");
        # If the request was for more than one day, print a summary only.
        $summary=1;
        $query=dequote(<< "        ENDQUERY");
            select
                convert(DateTime, convert(varchar(10),n.TranDateTime,21)) as TranDate,
                sum(n.NetAmt)
            from ****Data..tblStoreNetSales n
            where TranDateTime >= '$startdate' and TranDateTime < dateadd( d, 1, '$enddate') and -- add 1 day for an exclusive upper bound
                n.StoreId in ('$storeid') and
                substring(n.TxnModifier,1,1) not in (2,6)
                $where
            group by convert(DateTime, convert(varchar(10),n.TranDateTime,21))
            order by convert(DateTime, convert(varchar(10),n.TranDateTime,21))
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $TranDate=$tmp[0];
            my $Net=$tmp[1];
            my @array=(
                $Net
            );
            $storeData_hash{$TranDate}=\@array;
        }
    }
    my $tbl_ref1 = execQuery_arrayref($dbh, $query);
    # Get the tlog data
 
    if ($enddate eq $startdate) {
        # Print detail if the request was only for one day.
        $query=dequote(<< "        ENDQUERY");
            select tm.StoreId,
                 tm.RegisterId as RegNum,
                 case when tm.StoreId='2098' and tm.RegisterId = '02' and
                              tm.TranNum between 4013 and 4023 and
                              convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                        then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
                 convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
                 sum(convert(int,sign(ms.ExtOrigPrice) * ms.Quantity)) as [Qty],
                 sum(ms.ExtSellPrice) as [Net],
				 --sum(ms.ExtNetPrice) as [Net],	-- 2014-07-23 - Not certain which is best to use - kdg
                 tm.tlogfile as [TlogFile],
				 tm.TranId as TranId
				 

            from ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                    $tjoin
            where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and  
                    tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                    tm.TranModifier not in (2,6) and  
                    tm.StoreId in ('$storeid')
                    $twhere

            group by tm.StoreId, tm.RegisterId, tm.TranNum,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)),
                        tm.tlogfile,tm.TranId
            order by tm.StoreId, tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
        ENDQUERY
 
		#print $q->pre("$query");
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $StoreId=$tmp[0];
            my $RegNum=$tmp[1];
            my $txnNum=$tmp[2];
            my $TranDate=$tmp[3];
            my $Qty=$tmp[4];
            my $Net=$tmp[5];
            my $TlogFile=$tmp[6];
			my $TranId=$tmp[7];
            my @array=(
                $StoreId,
                $RegNum,
                $TranDate,
                $Qty,
                $Net,
                $TlogFile,
				$TranId
            );			
            $tlog_hash{$txnNum}=\@array;
        }
    } else {
        # If the request was for more than one day, print a summary only.
        $summary=1;
        $query=dequote(<< "        ENDQUERY");
            select
                 convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
                 --sum(ms.ExtSellPrice) as [Net] -- 2014-10-20 - Changed - kdg
				 sum(ms.ExtNetPrice) as [Net]
            from ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                    $tjoin
            where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and -- add 1 day for an exclusive upper bound
                    tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                    tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                    tm.StoreId in ('$storeid')
                    $twhere
            group by
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21))
            order by
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21))
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $TranDate=$tmp[0];
            my $Net=$tmp[1];
            my @array=(
                $Net,
            );
            $tlog_hash{$TranDate}=\@array;
        }
    }
    my @report;
    my @totals=(
        "Total",
        '',
        '',
        '',
        '',
        0,
        0,
        0,
        0,
        '',
		''
    );
    my @format;
    if (keys(%tlog_hash)) {
        unless (keys(%storeData_hash)) {
            print $q->p("Warning: We have Tlog data but no store data for the requested period.");
        }
    }

    if ((keys(%storeData_hash)) || (keys(%tlog_hash)) ){
        # We have store data or tlog data
        unless (keys(%tlog_hash)) {
            print $q->b("NOTE: No Tlog data found!");
        }
        unless (keys(%storeData_hash)) {
            print $q->b("NOTE: No StoreData found!");
        }
        if ($summary) {
            my @header=(
                'Date',
                'TlogNet',
                'SdataNet',
                ''
            );
            @totals=(
                "Total",
                0,
                0,
                ''
            );
            push(@report,\@header);
            # Combine the two so we can show them both together.
            foreach my $date (sort(keys(%storeData_hash))) {
                my $tNet;
                if ($tlog_hash{$date}) {
                    my $tref=$tlog_hash{$date};
                    $tNet=$$tref[0];
                } else {
                    $tNet="unknown";
                }
                my $ref=$storeData_hash{$date};
                my $sNet=$$ref[0];
                # Trim the date
                my @tmp=split(/\s+/,$date);
                $date=$tmp[0];
                my $star;
                if ($tNet != $sNet){
                    $star='*';
                }
                my @array=(
                    $date,
                    $tNet,
                    $sNet,
                    $star
                );
                push(@report,\@array);
                $totals[1]+=$tNet;
                $totals[2]+=$sNet;
            }
        } else {
            # Create a hash of all of the txns either in the tlog or the store data
            my %txn_hash;
            foreach my $txn (sort(keys(%storeData_hash))) {
                $txn_hash{$txn}=1;
            }
            foreach my $txn (sort(keys(%tlog_hash))) {
                $txn_hash{$txn}=1;
            }
            my @header=(
				'TranId',
                'Tlog',
                'StoreId',
                'RegNum',
                'TxnNum',
                'Date',
                'TlogQty',
                'TlogNet',
                'SdataQty',
                'SdataNet',
                ''
            );
            push(@report,\@header);
            # We have tlog data
            # Combine the two so we can show them both together.
            foreach my $txn (sort(keys(%txn_hash))) {
                my $tStoreId;
                my $tRegNum;
                my $tTranDate;
                my $tQty;
                my $tNet;
                my $TlogFile;
				my $TranId;
                if ($tlog_hash{$txn}) {
                    my $tref=$tlog_hash{$txn};
                    $tStoreId=$$tref[0];
                    $tRegNum=$$tref[1];
                    $tTranDate=$$tref[2];
                    $tQty=$$tref[3];
                    $tNet=$$tref[4];
                    $TlogFile=$$tref[5];
					$TranId=$$tref[6];					
                } else {
                    $tQty="unknown";
                    $tNet="unknown";
                }

                my $sStoreId;
                my $sRegNum;
                my $TxnNum;
                my $sTranDate;
                my $sQty;
                my $sNet;
                if ($storeData_hash{$txn}) {
                    my $sref=$storeData_hash{$txn};
                    $sStoreId=$$sref[0];
                    $sRegNum=$$sref[1];
                    $sTranDate=$$sref[2];
                    $sQty=$$sref[3];
                    $sNet=$$sref[4];
                } else {
                    $sQty="unknown";
                    $sNet=0;
                }


                # $star is used to indicate when tlog data varies from store data
                my $star;
                if ($tNet != $sNet){
                    $star='*';
                }
                if ($tQty != $sQty){
                    $star='*';
                }
                # Trim the date
                my @tmp=split(/\s+/,$tTranDate);
                $tTranDate=$tmp[0];
                my @array=(
					$TranId,
                    $TlogFile,
                    $tStoreId,
                    $tRegNum,
                    $txn,
                    $tTranDate,
                    $tQty,
                    $tNet,
                    $sQty,
                    $sNet,

                    $star
                );
                push(@report,\@array);
                $totals[6]+=$tQty;
                $totals[7]+=$tNet;
                $totals[8]+=$sQty;
                $totals[9]+=$sNet;
            }
        }

        # Print the results

        push(@report,\@totals);
        my @format;
		my @links;
        if ($summary) {
            @format = ('', {'align' => 'Right', 'currency' => ''},{'align' => 'Right', 'currency' => ''});
        } else {
            @format = ('','', '',  '','','','',{'align' => 'Right', 'currency' => ''},'',{'align' => 'Right', 'currency' => ''});			
			@links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        }
        print $q->h3("$storeid - $storename");
        printResult_arrayref2(\@report, \@links, \@format);

    #} elsif (keys(%tlog_hash)) {
    #    print $q->p("Warning: We have Tlog data but no store data for the requested period.");
    } else {
        print $q->p("Warning: We have neither Tlog data nor store data for the requested period.");
        standardfooter();
        exit;
    }
    $dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();
} elsif ("$report" eq "DISCOUNT_V2") {
    print $q->h2("Discount Report - V2 - BETA");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $startdate   = trim(($q->param("startdate"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
	my @tranid_list;
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    unless ($startdate) {
        $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    }

    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    unless ($end_year_selection) {
        $enddate = $startdate;
    }

    print $q->h3("Store: #${storeid} $storename - $startdate");
    my $summary=0;
    my @date_array;

    if ($startdate eq $enddate) {
        #print $q->h3("Part I - Item and Transaction Discounts");
        for my $discount ("Item","Transaction","Promotion","MixMatch") {
            print $q->h4("$discount Discounts");
            my ($have_data,$tbl_ref,$format_ref);
            if ($discount eq "Item") {($have_data,$tbl_ref, $format_ref)=get_item_discounts($storeid,$storename,$startdate,$summary); }
            if ($discount eq "Transaction") {($have_data,$tbl_ref, $format_ref)=get_transaction_discounts($storeid,$storename,$startdate,$summary); }
            if ($discount eq "Promotion") {($have_data,$tbl_ref, $format_ref)=get_promotion_discounts($storeid,$storename,$startdate,$summary); }
            if ($discount eq "MixMatch") {($have_data,$tbl_ref, $format_ref)=get_mixmatch_discounts($storeid,$storename,$startdate,$summary); }
            if ($have_data) {
                print $q->p("$discount Discounts found for store number $storeid for $startdate");
                printResult_arrayref2($tbl_ref, '', $format_ref);
            } else {
                print $q->p("No $discount Discount data found for $storeid for $startdate");
            }
        }


    } else {
        $summary=1;
        # Get all of the dates where there are transactions between the start and the end date
        my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
        my $dbh = openODBCDriver($dsn, '****', '****');
        $query=dequote(<< "        ENDQUERY");
            select
                distinct(convert(DateTime, convert(varchar(10),tm.TranDateTime,21))) as TranDate

            from ****Tlog..tblTranMaster tm
                join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
            where
                tm.TranDateTime >= '$startdate'
            and
                tm.TranDateTime < dateadd( d, 1, '$enddate')
            and
                tm.TranVoid = 0 and tm.IsPostVoided = 0
            and
                tm.TranModifier not in (2,6)
            and
                tm.StoreId in ('$storeid')
            group by tm.StoreId, tm.RegisterId, tm.TranNum,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)),
                        tm.tlogfile
            order by
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21))
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $date=substr($tmp[0],0,10);
            push(@date_array,$date);
        }
        $dbh->disconnect;


    }
    if ($summary) {
        my @summary_totals=("Grand Total");
        for my $discount ("Item","Transaction","Promotion","MixMatch") {
            print $q->h4("$discount Discounts");
            my ($have_data,$header,$day_tbl_ref);
            my $tbl_ref=[];
            my $set_header=1;
            foreach my $date (@date_array) {

                if ($discount eq "Item") {($have_data,$header,$day_tbl_ref)=get_item_discounts($storeid,$storename,$date,$summary); }
                if ($discount eq "Transaction") {($have_data,$header,$day_tbl_ref)=get_transaction_discounts($storeid,$storename,$date,$summary); }
                if ($discount eq "Promotion") {($have_data,$header,$day_tbl_ref)=get_promotion_discounts($storeid,$storename,$date,$summary); }
                if ($discount eq "MixMatch") {($have_data,$header,$day_tbl_ref)=get_mixmatch_discounts($storeid,$storename,$date,$summary); }
                if ($have_data) {

                    if ($set_header) {
                        push(@$tbl_ref,$header);
                        $set_header=0;
                    }
                    push(@$tbl_ref,$day_tbl_ref);
                }
            }
            my $records=recordCount($tbl_ref);

            if ($records > 0) {
                print $q->p("$discount Discounts found for store number $storeid for $startdate to $enddate");
                my @fieldsum = ('<b>Total:</b>','');
                my @sum_columns = (1,2,3,4);
                for my $datarows (1 .. $#$tbl_ref){
                    my $thisrow = @$tbl_ref[$datarows];
                    foreach my $c (@sum_columns) {
                        $fieldsum[$c] += trim($$thisrow[$c]);
                        $summary_totals[$c] += trim($$thisrow[$c]);
                    }
                }
                push @$tbl_ref, \@fieldsum;


                my @summary_format = (
                '',
                {'align' => 'Right', 'num' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},
                {'align' => 'Right', 'currency' => ''},

                );
                my @links = ( "$scriptname?report=DISCOUNT_V2&store_selection=$orig_storeid&startdate=".'$_' );
                printResult_arrayref2($tbl_ref, \@links, \@summary_format);
            } else {
                print $q->p("No $discount Discount data found for $storeid for $startdate to $enddate");
            }

        }
        my @header=(
            '',
            "Quantity",
            "OrigPrice",
            "Discount",
            "SellPrice"
        );
        my @summary_array=\@header;
        push(@summary_array,\@summary_totals);
        if (recordCount(\@summary_array)) {
            print $q->p("-----Summary Total-------");
            my @summary_format = (
            '',
            {'align' => 'Right', 'num' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},
            {'align' => 'Right', 'currency' => ''},

            );
            printResult_arrayref2(\@summary_array, '', \@summary_format);
        }

    }


    standardfooter();
} elsif ("$report" eq "NONMERCH") {
    print $q->h2("Non Merchandise Sales Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $summary=0;
    unless ($startdate eq $enddate) {
        $summary=1;
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");
	my %non_merch_hash=(

	'1' => 'GIFT CERTIFICATE',
	'2' => 'SHIPPING FEE',
	'3' => 'DONATIONS',
	'4' => 'PAID IN',
	'5' => 'PAID OUT',
	'6' => 'OTHER',
	'14' => 'GIFT CARD',
	'15' => 'BAG TAX',

	);

 	my %voucherid_hash=(
		'0' => 'ISSUE GIFT CARD',
		'6' => 'BALANCE INQ'
	);
	my %profile_prompt_hash=(
		'1' => 'LOCAL CHARGE INFO',
		'2' => 'DELIVERY PROMPTS',
		'3' => 'STORE NUMBER',
		'4' => 'STORE USE',
		'5' => 'CHECK INFO',
		'6' => 'PAID IN/OUT',
		'7' => 'RETURN INFO',
	);
	$query=dequote(<< "	ENDQUERY");

	SELECT nms.[TranId]
      ,nms.[Sequence]
      ,nms.[VoucherId]
      ,nms.[NonMerchType]
	  --,pr.[Response1]
      ,nms.[SellAmount]
      ,nms.[OrigAmount]
      ,nms.[AddAmount]
      ,nms.[RefNum]
      ,nms.[DiscountFlag]
      ,nms.[Qty]
      ,nms.[EmployeeSaleFlag]
      ,nms.[ProfilePromptFlag]
      ,nms.[AffectNetSales]
      ,nms.[ReasonCode]
      ,nms.[Tax1]
      ,nms.[Tax2]
      ,nms.[Tax3]
      ,nms.[Tax4]
      ,nms.[Tax5]

      ,nms.[TaxModify1]
      ,nms.[TaxModify2]
      ,nms.[TaxModify3]
      ,nms.[TaxModify4]
      ,nms.[TaxModify5]


      ,nms.[ExtTaxableAmount]
      ,nms.[UPC]
      ,nms.[ChoiceType]
      ,nms.[Reason]
      ,nms.[ProfileID]
      ,nms.[DiscountModifiedFlag]
      ,nms.[ItemDiscountableFlag]
      ,nms.[TxnDiscountableFlag]
      ,nms.[EncryptionKeyIndex]
      ,nms.[ReturnIdentifier]
      ,nms.[OrderCategory]

      ,nms.[procState]
      ,nms.[lastStateChange]
      ,nms.[uid]
	FROM
		[****TLog].[dbo].[tblNonMerchandiseSale]  nms
		--left outer join [****TLog].[dbo].[tblProfileResponse] pr on nms.tranid = pr.tranid

	WHERE nms.[TranId] in
		(select
			TranId
		FROM
			[****TLog].[dbo].[tblTranMaster]
		where
			[StoreId] = '$storeid'
			and [TranDateTime] > '$startdate'
			and [TranDateTime] < '$enddate 23:59:59'
		)

	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		if (recordCount($tbl_ref) > 1000) {
			print $q->p("Query result very large! Please reduce the scope of your query.");
		} else {

			my @links = ( "$scriptname?report=PROPROMPT&store_selection=$orig_storeid&tranid=".'$_' );
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];

				# Substitute known values
				if ($$thisrow[3] == 14) {
					my $dtype=$$thisrow[2];
					$$thisrow[2]=$voucherid_hash{$dtype};
				} else {
				}
				my $dtype=$$thisrow[3];
				$$thisrow[3]=$non_merch_hash{$dtype};

			}
			printResult_arrayref2($tbl_ref, \@links );
		}
	} else {
		print $q->p("Found no data in tblNonMerchandiseSale for $storeid between $startdate and $enddate");
	}
    standardfooter();
} elsif ("$report" eq "STOREVALUATION") {
    print $q->h2("Store Valuation Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");

	my %action_summary;
	my $issue_total=0;
	$query=dequote(<< "	ENDQUERY");

	SELECT [StoreId]
		,[ldate]
		,[items]
		,[units]
		,[extlastcost]
		,[extavgcost]
		,[extprice]
	FROM [****Data].[dbo].[tblStoreValueSummary]
	where
		[StoreId] = '$storeid'
		and [ldate] > '$startdate'
		and [ldate] < '$enddate 23:59:59'

	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {

        my @format = (
        '','','',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
		);
		printResult_arrayref2($tbl_ref, '',\@format );
	} else {
		print $q->p("Found no data in tblStoreValueSummary for $storeid between $startdate and $enddate");
	}
    standardfooter();



} elsif ("$report" eq "SKUSALE_second") {
    print $q->h2("SKU Sale History Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $SKU   = trim(($q->param("sku_selection"))[0]);
    my $orig_storeid=$storeid;
	my $where_store=" and tm.StoreId in ('$storeid')";
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
	$where_store=" and tm.StoreId in ('$storeid')";
	my $where_sku=" and	ms.SKU = '$SKU' ";
	my $join_store=" and ssi.storeid = '$storeid'";

	if ($storeid =~ /all/i) {
		$where_store='';
		$join_store='';
	
		unless ($SKU) {
			print $q->h4("Error: If no SKU is specified, then a store must be specified.");
			standardfooter();
			exit;
		}
	}
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");

	my %action_summary;

	$query=dequote(<< "	ENDQUERY");
		select
			distinct(tm.tranid),
			tm.StoreId,
			convert(DateTime, convert(varchar(19),tm.TranDateTime,21)) as TranDate,
			ms.SKU,
			ssi.descrip,
			ms.Quantity,
			ms.ExtSellPrice
		from ****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
			join [****Data].[dbo].[tblStoreSkuInfo] ssi on ssi.SKU = ms.SKU $join_store
		where
			tm.TranDateTime >= '$startdate'
		and
			tm.TranDateTime < '$enddate'
		and
			tm.TranVoid = 0 and tm.IsPostVoided = 0
		and
			tm.TranModifier not in (2,6)				
		$where_sku
		$where_store
 

	ENDQUERY

	#print $q->pre("$query");
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        my @format = (
        '','','','','',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
		);
		printResult_arrayref2($tbl_ref, \@links,\@format );
	} else {
		print $q->p("Found no sales data for $SKU for $storeid between $startdate and $enddate");
	}
    standardfooter();

} elsif ("$report" eq "RETURNS") {
    print $q->h2("Returns Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $SKU   = trim(($q->param("sku_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');


	# Determine the store selection
	my $storequery;
	my $store_label='';
	if ($storeid eq "V1")  {
		$store_label="Company Stores";
		# Company Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		";
	} elsif ($storeid eq "V2") {
		$store_label="Contract Stores";
		# Contract Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V2'
		";
	} elsif ($storeid eq "All") {
		$store_label="All Stores";
		# All stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		OR [StoreCode] like 'V2'
		";
	} else {
		$store_label=$storename;
		# A specific store
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreId] = '$storeid'
		";
	}
	print $q->h3("Store: $store_label - $startdate to $enddate");

	my %response_hash=();
	my %return_reason_hash=();
	my %return_amt_hash=();
	my %return_reason_amt_hash=();
	my %reason_hash=(
		"1" => "CHANGED MIND",
		"2" => "DEFECTIVE",
		"3" => "FOUND CHEAPER",
		"4" => "OTHER"
	);

	$query=dequote(<< "	ENDQUERY");
		select
			tm.tranid,
			tm.storeid,
			convert(DateTime, convert(varchar(19),tm.TranDateTime,21)) as TranDate,
			ms.SKU,

			ms.Quantity,
			ms.ExtSellPrice,
			tpr.[Response1],
			tmt.[ReasonCode],
            tpr.[Response2]
		from ****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
			join [****TLog]..[tblProfileResponse] tpr on tm.TranId = tpr.TranId
			join [****TLog]..[tblMiscTran] tmt on tm.TranId = tmt.TranId and tmt.sequence = (tpr.sequence + 1)
		where
			tm.TranDateTime >= '$startdate'
		and
			tm.TranDateTime < '$enddate'
		and
			tm.TranVoid = 0 and tm.IsPostVoided = 0
		and
			tm.TranModifier not in (2,6)
		and
			tm.StoreId in ($storequery)
		and
			tm.isReturn = 1
		and
			ms.ExtSellPrice < 0
		and
			tpr.ProfileId = 7

		order by
			tm.storeid


	ENDQUERY
 
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
=pod    
		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = $$tbl_ref[$datarows];

			my $response=$$thisrow[6];
 			$response_hash{$response}++;
			my $amt=$$thisrow[5];
			$return_amt_hash{$response}+=$amt;			
			my $reason=$reason_hash{$$thisrow[7]};
			$$thisrow[7]=$reason;
			$return_reason_hash{$reason}++;	
			$return_reason_amt_hash{$reason}+=$amt;				
						
		}
=cut        
		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = $$tbl_ref[$datarows];
            my $response2=$$thisrow[8];
            my $reason;
            my $original_purchase;
            if ($response2) {
                $reason=$$thisrow[6];
                $original_purchase=$$thisrow[8];
                $$thisrow[6] = $original_purchase;
                $$thisrow[7] = $reason;
                $$thisrow[8]='';
            } else {
                $original_purchase=$$thisrow[6];
                $reason=$reason_hash{$$thisrow[7]};
            }
			#my $response=$$thisrow[6];
 			$response_hash{$original_purchase}++;
			my $amt=$$thisrow[5];
			$return_amt_hash{$original_purchase}+=$amt;			
			#my $reason=$reason_hash{$$thisrow[7]};
			$$thisrow[7]=$reason;
			$return_reason_hash{$reason}++;	
			$return_reason_amt_hash{$reason}+=$amt;				
						
		}
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        my @format = (
        '','','','',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
 
		);
		printResult_arrayref2($tbl_ref, \@links,\@format );
		my $total_count;
		my $total_reason_count;

		foreach my $r (sort(keys(%response_hash))) {
			$total_count+=$response_hash{$r};
		}
		foreach my $r (sort(keys(%return_reason_hash))) {
			$total_reason_count+=$return_reason_hash{$r};
		}		
		foreach my $r (sort(keys(%response_hash))) {
			my $percent=sprintf("%.2f",(($response_hash{$r}/$total_count) * 100));
			my $amount=sprintf("%.2f",$return_amt_hash{$r});
			print $q->p("Source: $r  -  Count: $response_hash{$r} - ${percent}% - Amount: \$$amount"); 
		}
		foreach my $r (sort(keys(%return_reason_hash))) {
			my $reason_percent=sprintf("%.2f",(($return_reason_hash{$r}/$total_reason_count) * 100));
			my $reason_amount=sprintf("%.2f",$return_reason_amt_hash{$r});
			print $q->p("Reason: $r  -  Count: $return_reason_hash{$r} - ${reason_percent}% - Amount: \$$reason_amount"); 
		}		
		
		

	} else {
		print $q->p("Found no return sales data for $storeid between $startdate and $enddate");
	}
    standardfooter();
} elsif ("$report" eq "GIFTCARD") {
    print $q->h2("Gift Card Issue Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $summary=0;
    unless ($startdate eq $enddate) {
        $summary=1;
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");
	
	# Determine the store selection
	my $storequery;
	my $store_label = '';
	if ($storeid eq "V1")  {
		$store_label="Company Stores";
		# Company Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		";
	} elsif ($storeid eq "V2") {
		$store_label="Contract Stores";
		# Contract Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V2'
		";
	} elsif ($storeid eq "All") {
		$store_label="All Stores";
		# All stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		OR [StoreCode] like 'V2'
		";
	} else {
		$store_label=$storename;
		# A specific store
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreId] = '$storeid'
		";
	}		
	my %voucherid_hash=(
		'0' => 'ISSUE GIFT CARD',
		'6' => 'BALANCE INQ'
	);
	my %action_summary;
	my $issue_total=0;
	$query=dequote(<< "	ENDQUERY");

	SELECT [TranId]
      ,[Sequence]
      ,[TranSecs]
      ,[NonMerchType]
      ,[SellAmount]
      ,[OrigAmount]
      ,[AddAmount]
      ,[RefNum]
      ,[DiscountFlag]
      ,[Qty]


      ,[VoucherId] as 'Action'
      ,[ExtTaxableAmount]

      ,[lastStateChange]
      ,[uid]
	FROM
		[****TLog].[dbo].[tblNonMerchandiseSale]

	WHERE [TranId] in
		(select
			TranId
		FROM
			[****TLog].[dbo].[tblTranMaster]
		where
			--[StoreId] = '$storeid'
					
			StoreId in ($storequery)
			and [TranDateTime] > '$startdate'
			and [TranDateTime] < '$enddate 23:59:59'
		)
	and
		[NonMerchType] = 14

	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = $$tbl_ref[$datarows];
			# Substitute known values
			my $dtype=$$thisrow[10];
			$$thisrow[10]=$voucherid_hash{$dtype};
			$action_summary{$dtype}++;
			my $issued_amt=$$thisrow[4];
			$issue_total+=$issued_amt;

			###
		}
		printResult_arrayref2($tbl_ref, \@links );

		if ($summary) {
			my @header = ("Type","Count");
			my @array_to_print=\@header;

			foreach my $type (sort(keys(%action_summary))) {
				my @array;
				push(@array,$voucherid_hash{$type});
				push(@array,$action_summary{$type});
				push(@array_to_print,\@array);
			}
			print $q->p();
			printResult_arrayref2(\@array_to_print);
		}

	} else {
		print $q->p("Found no data in tblNonMerchandiseSale for $storeid between $startdate and $enddate");
	}
    standardfooter();

} elsif ("$report" eq "MCRPT") {
    print $q->h2("Manager Code Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $mcode_selection   = trim(($q->param("mcode_selection"))[0]);	
    my @tmp=split(/\|/,$mcode_selection);
    $mcode_selection=$tmp[0];	
    my $mcode_desc=$tmp[1];

    # The storeid contains the storeid and the store name.  Break them apart here:
    @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
	 
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);

    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";

 	my $dsn = "driver={SQL Server};Server=****;database=****TLog;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');


	# Determine the store selection
 
	my $store_label='';
	$store_label=$storename;
 
	print $q->h3("Store: $store_label - Manager Code: $mcode_desc - $startdate to $enddate");
 
 
	$query=dequote(<< "	ENDQUERY");
		select
			tm.tranid
			 
			,convert(DateTime, convert(varchar(19),tm.TranDateTime,21)) as TranDate
			
			,tm.[TranNum]
			,mt.[TranSecs]
			,mt.[MiscType]
			,mt.[RefNum]
			,mt.[Amount]
			,mt.[DrawerNo]
			,mt.[TranMode]
			,mt.[UserNum]
			,tm.[CashierId]
			,mt.[ProfilePromptFlag]
			,mt.[ReasonCode]
			,mt.[AddInfo]
			,mt.[StartStop]
			,mt.[Applied]
			,mt.[ChoiceTypeID]
			,mt.[procState]
			,mt.[lastStateChange]
			,mt.[uid]
		from ****Tlog..tblTranMaster tm
			join ****Tlog..[tblMiscTran] mt on tm.TranId=mt.TranId
 
		where
			tm.TranDateTime >= '$startdate'
		and
			tm.TranDateTime < '$enddate'
		and
			tm.TranVoid = 0 and tm.IsPostVoided = 0
		and
			tm.TranModifier not in (2,6)
		and
			tm.StoreId = $storeid
 
		and
			mt.[RefNum] = $mcode_selection
		order by
			tm.TranDateTime


	ENDQUERY
 
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = $$tbl_ref[$datarows];

 			
						
		}
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        my @format = (
        '','','','','',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
 
		);
		printResult_arrayref2($tbl_ref, \@links,\@format );
 	
		
		

	} else {
		print $q->p("Found  data for $storeid between $startdate and $enddate");
	}
    standardfooter();	
} elsif ("$report" eq "PROVERRIDE") {
    print $q->h2("Price Override Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $summary=0;
    unless ($startdate eq $enddate) {
        $summary=1;
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");


	$query=dequote(<< "	ENDQUERY");

	SELECT
		[TranId]

	FROM
		[****TLog].[dbo].[tblMiscTran]

	WHERE [TranId] in
		(select
			TranId
		FROM
			[****TLog].[dbo].[tblTranMaster]
		where
			[StoreId] = '$storeid'
			and [RefNum] = 180
			and [TranDateTime] > '$startdate'
			and [TranDateTime] < '$enddate 23:59:59'
		)

	ENDQUERY

	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
		printResult_arrayref2($tbl_ref, \@links );

	} else {
		print $q->p("Found no data in tblNonMerchandiseSale for $storeid between $startdate and $enddate");
	}
    standardfooter();
} elsif ("$report" eq "PROPROMPT") {
    print $q->h2("Profile Prompt Report");
	my $orig_storeid   = trim(($q->param("orig_storeid"))[0]);
	my $tranid   = trim(($q->param("tranid"))[0]);
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - TranId $tranid");
	# First get the info on the transaction
	$query=dequote(<< "	ENDQUERY");

	SELECT [TranId]
      ,[StoreId]
      ,[RegisterId]
      ,[TranDateTime]
      ,[TranNum]
      ,[CashierId]
      ,[SalespersonId]
      ,[SaleType]
      ,[TranModifier]
      ,[TranVoid]
      ,[BusinessDate]
      ,[tlogfile]
      ,[tlogline]
      ,[isSale]
      ,[isReturn]
      ,[isDiscount]
      ,[isPriceOver]
      ,[isTotaled]
      ,[isPayment]
      ,[isPostVoided]
      ,[isZeroPayment]
      ,[numRecords]
      ,[numUnitsSold]
      ,[paymentTotal]
	FROM [****TLog].[dbo].[tblTranMaster]
	WHERE [TranId] = '$tranid'
	ENDQUERY
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		print $q->h3("Transaction Info from tblTranMaster");
		printResult_arrayref2($tbl_ref );

	} else {
		print $q->p("Found no data in tblTranMaster for $orig_storeid TranId $tranid");
	}

	$query=dequote(<< "	ENDQUERY");

	SELECT [TranId]
		  ,[Sequence]
		  ,[TranSecs]
		  ,[ProfileId]
		  ,[PromptGroup]
		  ,[TotPromptGrp]
		  ,[Response1]
		  ,[Response2]
		  ,[Response3]
		  ,[Response4]
		  ,[PromptPrefix1]
		  ,[PromptPrefix2]
		  ,[PromptPrefix3]
		  ,[PromptPrefix4]
		  ,[procState]
		  ,[lastStateChange]
		  ,[uid]
	FROM [****TLog].[dbo].[tblProfileResponse]
	WHERE [TranId] = '$tranid'
	ENDQUERY

	$tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		print $q->h3("Info from tblProfileResponse");
		printResult_arrayref2($tbl_ref );

	} else {
		print $q->p("Found no data in tblProfileResponse for $orig_storeid TranId $tranid");
	}

	standardfooter();
} elsif ("$report" eq "TENDERRPT") {
    print $q->h2("Tender Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];

	my $join;
	my $where_store="tm.[StoreId] = '$storeid'";
	if (($storeid eq 'V1') || ($storeid eq 'V2')) {
		$join="join [****Data].[dbo].[rtblStores] rs on tm.StoreId = rs.StoreId";
		$where_store = "rs.[StoreCode] = '$storeid'";
	}
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $start   = trim(($q->param("start"))[0]);
	if ($start) {
		$startdate = $start;
	}
    my $end   = trim(($q->param("end"))[0]);
	if ($end) {
		$enddate = $end;
	}
    my $summary=0;
	my $show_register=1;
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");
	my %tender_hash=(
		'1' => 'Cash',
		'2' => 'Check',
		'3' => 'Travelers Check',
		'4' => 'MasterCard',
		'5' => 'Visa',
		'6' => 'AmericanExpress',
		'7' => 'Discover',
		'8' => 'Gift Certificate',
		'9' => 'Merchandise Credit',
		'10' => 'Offline Tender',
		'11' => 'Local Charge',
		'12' => 'Merchandise Credit Redeem',
		'13' => '$ Coupon Off',
		'14' => 'Gift Card',
		'18' => 'Gift Card',
		'21' => 'Debit Card',
		'22' => 'Debit Card',
	);

	$query=dequote(<< "	ENDQUERY");
	SELECT
		tm.[TranId],
		tp.[TenderId] as 'Tender',
		tp.[TenderAmount],
		tp.[ChangeDue],
		(tp.[TenderAmount] - tp.[ChangeDue]) as 'NetAmt',
		tm.[RegisterId]
	FROM
		[****TLog].[dbo].[tblPayment] tp
		join [****TLog].[dbo].[tblTranMaster] tm on tp.[TranId] = tm.[TranId]
	WHERE
		tm.[StoreId] = '$storeid'
		and tm.[TranDateTime] > '$startdate'
		and [TranDateTime] < '$enddate 23:59:59'
	ENDQUERY
	my $wherestore = "rs.[StoreCode] = '$storeid'";
	if ($storeid eq 'All'){
		$wherestore = "(rs.[StoreCode] = 'V1' or rs.[StoreCode] = 'V2')";
	}
	if (($storeid eq 'V1') || ($storeid eq 'V2') || ($storeid eq 'All')) {
		$show_register=0;
		# When we are looking at a group of stores, we are in "summary" mode because we are collecting
		# sums of some of the columns.  This affects how we gather counts later on.
		$summary=1;
		# We have a different query in this case
		$query=dequote(<< "		ENDQUERY");
		SELECT
			rs.[StoreId],
			tp.TenderId,
			count(tp.TenderId) as 'Count',
			sum(tp.[TenderAmount]) as 'TenderAmount',
			sum(tp.[ChangeDue]) as 'ChangeDue',
			sum((tp.[TenderAmount] - tp.[ChangeDue])) as 'NetAmt'
			
		FROM
			[****TLog].[dbo].[tblPayment] tp
			join [****TLog].[dbo].[tblTranMaster] tm on tp.[TranId] = tm.[TranId]
			join [****Data].[dbo].[rtblStores] rs on tm.StoreId = rs.StoreId
		WHERE
			$wherestore
			and tm.[TranDateTime] > '$startdate'
			and [TranDateTime] < '$enddate 23:59:59'
        group by rs.[StoreId],rs.[StoreName],tp.[TenderId]
        order by rs.[StoreId]
		ENDQUERY

	}
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {

        my @format = (
        '',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Center'},		
		);

		my @header=("Tender","Count","NetAmt");
		my @tender_totals_report=\@header;
		my @reg_header=("Register","Count");
		my @register_totals_report=\@reg_header;
		my %tender_totals_hash;
		my %tender_count_hash;
		my @fieldsum = ('<b>Total:</b>','');
		my @ceditfieldsum = ('<b>Credit Card Total:</b>','','');
		my @sum_columns = (2,3,4);
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
		my $netAmt_column=4;
		if (($storeid eq 'V1') || ($storeid eq 'V2') || ($storeid eq 'All')) {
			@links = ( "$scriptname?report=TENDERRPT&start=$startdate&end=$enddate&store_selection=".'$_' );
			@format = (
					'',
					'',
					{'align' => 'Right', 'num' => ''},
					{'align' => 'Right', 'currency' => ''},
					{'align' => 'Right', 'currency' => ''},
					{'align' => 'Right', 'currency' => ''},
					);
			@sum_columns = (2,3,4,5);
			$netAmt_column=5;
		}
		my %register_hash=();
		my %register_base_hash=();
		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = @$tbl_ref[$datarows];
			my $tender=$$thisrow[1];
			my $net=$$thisrow[$netAmt_column];
			my $tender_count=$$thisrow[2];
			my $registerId=$$thisrow[5];
			if (($tender_hash{$tender} =~ /Gift/i) || ($tender_hash{$tender} =~ /MasterCard/) || ($tender_hash{$tender} =~ /Visa/) || ($tender_hash{$tender} =~ /Discover/) || ($tender =~ /Ame/)) {
				$register_hash{"$registerId Card"}++;	
			} else {
				$register_hash{"$registerId Cash etc."}++;	
			}
			$register_hash{"$registerId Total ----"}++;	
			$register_base_hash{$registerId}++;
			if ($tender_hash{$tender}) {
				$$thisrow[1]=$tender_hash{$tender};
				$tender_totals_hash{$tender_hash{$tender}}+=$net;
				if ($summary) {
					# In summary mode, there is a column with tender counts
					$tender_count_hash{$tender_hash{$tender}}+=$tender_count;
				} else {
					# when looking at a specific store, there is no tender count column so we simply increment
					$tender_count_hash{$tender_hash{$tender}}++;
				}
				# Keep a separate total of the credit card amounts
				if (($tender > 3) && ($tender < 8)) {
					$ceditfieldsum[2] += $net;
					if ($summary) {
						$ceditfieldsum[1] += $tender_count;
					} else {
						$ceditfieldsum[1]++;
					}
				}
			} else {
				# In case the tender is not recognized by the tender hash
				$tender_totals_hash{$tender}+=$net;
				if ($summary) {
					$tender_count_hash{$tender}+=$tender_count;
				} else {
					$tender_count_hash{$tender}++;
				}
			}

			foreach my $c (@sum_columns) {
				$fieldsum[$c] += trim($$thisrow[$c]);
			}
			# Add the number of registers
			$fieldsum[5]=keys(%register_base_hash);
			
		}
		push @$tbl_ref, \@fieldsum;
		if (recordCount($tbl_ref) > 501) {
			print $q->p("Reduce the date range and/or reduce the number of selected stores to see the detail report");
		} else {
			printResult_arrayref2($tbl_ref, \@links, \@format );
		}

		# Now show the tender totals

		foreach my $t (sort(keys(%tender_totals_hash))) {
			my $count=$tender_count_hash{$t};
			my @array=("$t","$count","$tender_totals_hash{$t}");

			push(@tender_totals_report,\@array);
		}
		push(@tender_totals_report,\@ceditfieldsum);

        @format = (
        '',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''}
		);
		print "<br />";
		printResult_arrayref2(\@tender_totals_report, '', \@format );
		
		if ($show_register) {
			# Show the counts per register
			foreach my $t (sort(keys(%register_hash))) {
				my $count=$register_hash{$t};
				my @array=("$t","$count");

				push(@register_totals_report,\@array);
			}
	 
			@format = (
			'',
			{'align' => 'Right', 'num' => ''},
	 
			);
			print "<br />";
			printResult_arrayref2(\@register_totals_report, '', \@format );		
		}

	} else {
		print $q->p("Found no tender data for $storeid between $startdate and $enddate");
	}
    standardfooter();
} elsif ("$report" eq "DISCOUNT") {
    print $q->h2("Discount Report");
	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;

    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $summary=0;
    unless ($startdate eq $enddate) {
        $summary=1;
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    print $q->h3("Store: $orig_storeid - $startdate to $enddate");

    my %totals_hash;
    my $total_origPrice=0;
    my $total_Discounts=0;
    my $total_itemDiscounts=0;
    my $total_transDiscounts=0;
    my $total_promoDiscounts=0;
    my $total_MixMatchDiscounts=0;
    my $total_sellPrice=0;
    # Discounts are really of three basic sorts: 1) Discounts, 2) Promotions, and 3) MixMatch.  The #1 Discounts are basically separated into Item and Transaction Discounts
    # Because these three basic discounts are so different, I am going to report them separately.
    unless ($summary) {
        if ($txnNum) {
            print $q->p("View all transactions");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNT"});
            print $q->input({-type=>"hidden", -name=>"store_selection", -value=>"$orig_storeid"});
            print $q->input({-type=>"hidden", -name=>"start_month_selection", -value=>"$start_month_selection"});
            print $q->input({-type=>"hidden", -name=>"start_day_selection", -value=>"$start_day_selection"});
            print $q->input({-type=>"hidden", -name=>"start_year_selection", -value=>"$start_year_selection"});

            print $q->input({-type=>"hidden", -name=>"end_month_selection", -value=>"$end_month_selection"});
            print $q->input({-type=>"hidden", -name=>"end_day_selection", -value=>"$end_day_selection"});
            print $q->input({-type=>"hidden", -name=>"end_year_selection", -value=>"$end_year_selection"});

            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2} ));

            # Add the submit button
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'S', -value=>"   View All   "}))
                      );

            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            print $q->p();

        } else {

            print $q->p("View only one selected transaction");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNT"});
            print $q->input({-type=>"hidden", -name=>"store_selection", -value=>"$orig_storeid"});
            print $q->input({-type=>"hidden", -name=>"start_month_selection", -value=>"$start_month_selection"});
            print $q->input({-type=>"hidden", -name=>"start_day_selection", -value=>"$start_day_selection"});
            print $q->input({-type=>"hidden", -name=>"start_year_selection", -value=>"$start_year_selection"});
            print $q->input({-type=>"hidden", -name=>"end_month_selection", -value=>"$end_month_selection"});
            print $q->input({-type=>"hidden", -name=>"end_day_selection", -value=>"$end_day_selection"});
            print $q->input({-type=>"hidden", -name=>"end_year_selection", -value=>"$end_year_selection"});

            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2} ));

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Txn Selection:"),
                $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"txnNum", -size=>"5", -value=>""}))
            );

            # Add the submit button
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
                      );

            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            print $q->p();
        }
    }
	####################
    # PART I - Discounts
    ####################

    my %discountType_hash = (
    '0' => 'Undefined discount type',
    '1' => 'ITEM % OFF',
    '2' => 'ITEM $ OFF',
    '3' => 'PRICE MARKDOWN',
    '4' => 'TRANS % OFF',
    '5' => 'TRANS $ OFF',
    '6' => 'MFG $ COUPON',
    '7' => 'TRANS % OFF IN EMPLOYEE SALE',
    '8' => 'MIX MATCH ITEMS',
    '9' => 'MFG COUPON FOR MATCHED ITEM',
    '10' => 'SCAN MFG COUPON,NO LINK TO AN ITEM',
    '11' => 'ITEM % OFF IN EMPLOYEE SALE',
    '12' => 'Item % Surcharge',
    '13' => 'Item $ Surcharge',
    '14' => 'SURCHARGE MARKDOWN',
    '15' => 'Transaction % Surcharge',
    '16' => 'Transaction $ Surcharge',
    '17' => 'TRANS % AT TOTAL',
    '18' => 'TRANS $ AT TOTAL',
    '19' => 'TRANS % AT TENDER',
    '20' => 'TRANS $ AT TENDER',
    '25' => 'Item Best Price %',
    '26' => 'Item Best Price $',
    );

    my @item_discounts=(
        "1",
        "2",
        "3",

        "12",
        "13",
        "25",
        "26"
    );
    my @ms_array;
	my %coupon_hash;

    print $q->h3("Part I - Item and Transaction Discounts");
    print $q->h4("Item Discounts");
    ####
    # Step 1 - Find the Item Discounts

    $query=dequote(<< "    ENDQUERY");
        select
            tm.TranId,
            ds.Sequence,
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                      tm.TranNum between 4013 and 4023 and
                      convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                then tm.TranNum - 4000
                else tm.TranNum end as TxnNum,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.ExtDiscountAmt,
            ds.DiscountPercent,
            ds.DiscountType,
            ds.DiscountID,
            rds.Description,
            rds.DiscGroup,
            ds.Reason,
            case when LEN(ds.ReferenceNum) = 7 then ata.Description else null end as Publication,
            case when LEN(ds.ReferenceNum) = 7 then atb.Description else null end as Creative,
            case when LEN(ds.ReferenceNum) = 7 then atc.Description else null end as Year,
			ds.ReferenceNum
        from ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
            join [****].[dbo].[rtblDiscounts] rds on ds.DiscountID=rds.[DiscountId]

            left join ****..tblMarketingAdTrack ata on substring(ds.ReferenceNum,1,3) = ata.code
                and ata.CodeType='P'
            left join ****..tblMarketingAdTrack atb on substring(ds.ReferenceNum,4,2) = atb.code
                and left(atb.CodeType, 1) ='C' and right(atb.CodeType,2) = substring(ds.ReferenceNum,6,2)
            left join ****..tblMarketingAdTrack atc on substring(ds.ReferenceNum,6,2) = atc.code
                and atc.CodeType='Y'
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')

        $whereTxn

        order by tm.StoreId, tm.RegisterId,
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();
	#print $q->pre("$query");
    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    my @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
        "Discount %",
        "Discount ID",
        "Description",
        "DiscGroup",
        "Reason",
		"Coupon"
    );
    my @summary_header=(
        "",
        "Quantity",
        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
    );

    # Step 2 find the associated merchandise transaction.  We do this by stepping through all of the merchandise records of this tranid.
    # We match the  discount sequence to the merchandise record sequence by knowing that the discount sequence comes after
    # the merchandise sequence but before the next one.  Therefore, the merchandise sequence ($Sequence) should be less than the associated
    # discount sequence ($seq) and yet greater than the previous discount sequence ($previous_seq).
    my @array_to_print=\@header;
    my @summary_array_to_print=\@summary_header;

    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
		my $coupon=$array[16];
		
		if ($coupon) {
			my $key = "$tranid $TxnNum $seq";
			$key = "$tranid $TxnNum";
			$coupon_hash{$key} = "$coupon";			
		}
        # Throw out any discounts that are not item discounts
        unless (grep /$DiscountType/,@item_discounts){
            next;
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                ms.ExtRegPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                where
                    tm.TranId = '$tranid'
                order by
                    tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ms.Sequence desc
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my $sequence_not_used=1;
        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];

            my $extoprice=$tmp[3];
            my $extsprice=$tmp[4];
            my $Sequence=$tmp[5];

            # I don't think that any table really knows what the sell price was after this item discount was applied IF there was a subsequent transaction discount.  Therefore, it seems reasonable to me to calculate the
            # final selling price rather than try to find it anywhere. -kdg

            $extsprice=($extoprice + $ExtDiscountAmt);

            if (($Sequence < $seq) && ($sequence_not_used)) {
				 
                #  This is the first merchandise record in the transaction we are stepping through and this is the first discount record must apply to this merchandise record.
                my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$extoprice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                ,"$DiscountID","$Description","$DiscGroup","$Reason","$coupon");
                push(@array_to_print,\@array);
                $sequence_not_used=0;
            }
        }
    }

    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"ItemDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            print $q->p("Item Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            print $q->p("Item Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@array_to_print, '', \@format);
        }
    } else {
        print $q->p("No Item Discount data in tblMerchandiseSale found for $storeid between $startdate and $enddate");
    }

    ####

    print $q->h4("Transaction Discounts");
	###
 
	if (0) { # Option 1
 
    my @array_to_print=\@header;
    my @summary_array_to_print=\@summary_header;

    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
		my $coupon=$array[16];

        # Throw out any discounts that are item discounts
        if (grep /$DiscountType/,@item_discounts){
            next;
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                ms.ExtRegPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                where
                    tm.TranId = '$tranid'
                order by
                    tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ms.Sequence desc
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my $sequence_not_used=1;
        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];

            my $extoprice=$tmp[3];
            my $extsprice=$tmp[4];
            my $Sequence=$tmp[5];

            # I don't think that any table really knows what the sell price was after this item discount was applied IF there was a subsequent transaction discount.  Therefore, it seems reasonable to me to calculate the
            # final selling price rather than try to find it anywhere. -kdg

            $extsprice=($extoprice + $ExtDiscountAmt);

            if (($Sequence < $seq) && ($sequence_not_used)) {
                #  This is the first merchandise record in the transaction we are stepping through and this is the first discount record must apply to this merchandise record.
                my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$extoprice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                ,"$DiscountID","$Description","$DiscGroup","$Reason");
                push(@array_to_print,\@array);
                $sequence_not_used=0;
            }
        }
    }
 
    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"TransDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {		
            print $q->p("Transaction Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
			 
            print $q->p("Transaction Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@array_to_print, '', \@format);
        }
    } else {
        print $q->p("No Transaction Discount data in tblMerchandiseSale found for $storeid between $startdate and $enddate");
    }
	###
	} # End of option 1

    if (1) {	# Option 2
    # Step 1 find the prorated discounts

    @ms_array=();
    $query=dequote(<< "    ENDQUERY");
        select
            tm.TranId,
            ds.Sequence,
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                      tm.TranNum between 4013 and 4023 and
                      convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                then tm.TranNum - 4000
                else tm.TranNum end as TxnNum,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.ExtDiscountAmt,
            ds.DiscountPercent,
            ds.DiscountType,
            ds.DiscountID,
            rds.Description,
            rds.DiscGroup,
            cl.Description
        from
            ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscountsProrated ds on tm.TranId=ds.TranId
            join [****].[dbo].[rtblDiscounts] rds on ds.DiscountID=rds.[DiscountId]
            left join [****Data].[dbo].[tblStoreChoiceList] cl on tm.StoreId = cl.StoreId and ds.Reason = cl.ChoiceCode and cl.ChoiceType = 7
        where
            tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and tm.TranVoid = 0 and tm.IsPostVoided = 0
            and tm.TranModifier not in (2,6)
            and tm.StoreId in ('$storeid')
            $whereTxn
        order by
            tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ds.Sequence

    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    my @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
        "Discount %",
        "Discount ID",
        "Description",
        "DiscGroup",
        "Reason",
		"Coupon"
    );
    my @summary_header=(
        "",
        "Quantity",
        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
    );

    # Step 2 find the associated merchandise transaction.  We do this by stepping through all of the merchandise records of this tranid.
    # We match the prorated discount sequence to the merchandise record sequence by knowing that the discount sequence comes after
    # the merchandise sequence but before the next one.  Therefore, the merchandise sequence ($Sequence) should be less than the associated
    # discount sequence ($seq) and yet greater than the previous discount sequence ($previous_seq).
    my @array_to_print=\@header;
    my @summary_array_to_print=\@summary_header;
    my $ms_count=0;
    my $last_seq=0;
    my $previous_tranid=0;
    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
		my $coupon;		
		my $key = "$tranid $TxnNum $seq";
		$key = "$tranid $TxnNum";
		if ($coupon_hash{$key}) {
			$coupon=$coupon_hash{$key}; 
		}	
 
        my $previous_seq=0;
        if ($tranid == $previous_tranid) {
            # This is the next sequence in the same transaction.
            # Let's remember what the previous discount sequence was
            $previous_seq=$last_seq;
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                --ms.ExtOrigPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId

                where
                    tm.TranId = '$tranid'
                order by
                    tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ms.Sequence
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];
           # my $cost=$tmp[3];
            #my $extoprice=$tmp[4];
            my $extsprice=$tmp[3];
            my $Sequence=$tmp[4];

            if ($previous_seq) {
                # There was more than one item in this merchandise sale.  We have already found a previous sequence.
                # If this merchandise sequence ($Sequence) is less than the discount sequence ($seq) and yet still greater
                # than the previous discount sequence ($previous_seq) then this discount sequence and this merchandise sequence are associated.
                if (($Sequence < $seq) && ($Sequence > $previous_seq)) {
                    # This matches the sequence of the merchandise sale entry to the prorated discount entry
                    my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$OrigPrice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                   ,"$DiscountID","$Description","$DiscGroup","$Reason","$coupon");
                    push(@array_to_print,\@array);
                }
            } elsif ($Sequence < $seq) {
                #  This is the first merchandise record in the transaction we are stepping through and this is the first discount record must apply to this merchandise record.
                my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$OrigPrice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                ,"$DiscountID","$Description","$DiscGroup","$Reason","$coupon");
                push(@array_to_print,\@array);

            }
        }
        $last_seq=$seq;
        $previous_tranid=$tranid;
    }

    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"TransDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            print $q->p("Transaction Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {		
			 
            print $q->p("Transaction Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@array_to_print, '', \@format);
        }
    } else {
        print $q->p("No Transaction Discount data in tblMerchandiseSale found for $storeid between $startdate and $enddate");
    }
###
	}

    print $q->h3("Part II - Promotions");
    ####################
    # PART II - Promotions
    ####################
    # [tblMerchandiseSale] - Promotions
    $query=dequote(<< "    ENDQUERY");
        select
             tm.RegisterId as RegNum,
             case when tm.StoreId='2098' and tm.RegisterId = '02' and
                          tm.TranNum between 4013 and 4023 and
                          convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                    then tm.TranNum - 4000
                    else tm.TranNum end as TxnNum,
             ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
             tm.TranDateTime,
             ms.SKU,
             ms.Dept,
             ms.Quantity,
             --ms.Cost,
             ms.ExtOrigPrice,
             (ms.Quantity * ms.PromoPriceAdjustAmount) as PromoDiscount,
             --(ms.Quantity * (0 - ms.PromoPriceAdjustAmount)) as ExtPromoDisc,
             ms.ExtRegPrice,
             (ms.PromoPercentAdjustAmount * 100) as 'Promo%Amt',

             ms.PromoNum,
             tsp.[Description],
             ms.OrigPrice

        from ****Tlog..tblTranMaster tm
                join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                join [****Data].[dbo].[tblStorePromoHeader] tsp on ms.PromoNum = tsp.[PromoNum] and tm.StoreId = tsp.[StoreId]
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')
        and ms.PromoNum > 0
         $whereTxn
        order by tm.StoreId, tm.RegisterId,
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        #   get totals for the specified columns.
        undef($$tbl_ref[0][-1]);
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            my $OrigPrice=$$thisrow[-1];
            if ($OrigPrice > 0) {
                # If the OrigPrice is not 0, then it means that someone did not accept the default price.  As a result, the
                # PromoPriceAdjustAmount may be useless.
                # Set the promo discount to the difference between the orig price and the reg price
                $$thisrow[7] = ($$thisrow[6] - $$thisrow[8]);
                # An asterisk to mark this row.
                $$tbl_ref[$datarows][-1]="*";
            } else {
                undef($$tbl_ref[$datarows][-1]);
            }
            # If this item is being returned, then the ExtOrigPrice is going to be negative.  For some reason, the PromoPriceAdjustAmount is always
            # positive in the tblMerchandiseSale table but I want to show it as negative for sales and positive if is it is a return.
            if ($$thisrow[6] > 0) {
                $$thisrow[7] = (0 - $$thisrow[7]);
            }

            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        my @summary_header=('');

        foreach my $c (@sum_columns) {
            # Grab the headers for the fields we want.
            push(@summary_header,$$tbl_ref[0][$c]);
        }
        my @summary_array_to_print=\@summary_header;
        push @$tbl_ref, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"PromoDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];
        my @format = ('', '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',
        '',
        {'align' => 'Right', 'currency' => ''},

        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        print $q->p("Promotion Discounts found for store number $storeid between $startdate and $enddate");
        if ($summary) {
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            printResult_arrayref2($tbl_ref, '', \@format);
        }
    } else {
        print $q->p("No Promotion Discounts found for store number $storeid between $startdate and $enddate");
    }
    print $q->h3("Part III - MixMatch Discounts");
    ####################
    # PART III - MixMatch
    ####################
    ## Mix Match
    # Get the data from tblDiscounts table
    # First we find the discounts and then we associate them with the item.
    @ms_array=();   # Initialize this variable we are re-using;

    $query=dequote(<< "    ENDQUERY");
        select
            tm.[TranId],
            ds.[Sequence],
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                          tm.TranNum between 4013 and 4023 and
                          convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                    then tm.TranNum - 4000
                    else tm.TranNum end as TxnNum,
             ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.[ExtDiscountAmt]

        from ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')
        and ds.DiscountType = 8
        $whereTxn
        order by ds.[Sequence]
    ENDQUERY

    $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    # Now we loop through this hash and find the associated transaction
    @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "OrigPrice",
        "MixMatch Discount",
        "Sale Price",
        "MixMatchNumber"
    );
    @array_to_print=\@header;

    my %hash_to_print;
    my $previous_tranid=0;
    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $discount_seq=$array[1];
        my $reg=$array[2];
        my $txn=$array[3];
        my $date=$array[4];
        my $price=$array[5];
        my $discamt=$array[6];

        # Find the sequence that is just prior to this sequence in the [tblMerchandiseSale]
        $query=dequote(<< "        ENDQUERY");
            select
                Sequence
            from
                ****Tlog..tblMerchandiseSale
            where
                TranId = '$tranid'
            order by Sequence
        ENDQUERY

        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my @merch_sequences;
        while (my @tmp = $sth->fetchrow_array()) {
            push(@merch_sequences,$tmp[0]);
        }
        my $merchandise_sequence=0;
        for (my $x=0; $x<=$#merch_sequences; $x++) {
            my $ms=$merch_sequences[$x];
            my $next_ms=$merch_sequences[($x+1)];
            my $previous_ms=$merch_sequences[($x-1)];
            if ($discount_seq > $ms) {
                if (defined($next_ms)) {
                    if ($discount_seq < $next_ms) {
                        # The discount sequence fits in here.  It is less than this
                        # merchandise sequence but is more than the previous one
                        $merchandise_sequence=$ms;
                    }
                } else {
                    # There is no merchandise sequence higher
                    $merchandise_sequence=$ms;
                }
            }
        }
        # Find the merchandise record with this sequence number
        $query=dequote(<< "        ENDQUERY");
            select
                 tm.RegisterId as RegNum,
                 case when tm.StoreId='2098' and tm.RegisterId = '02' and
                              tm.TranNum between 4013 and 4023 and
                              convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                        then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
                 ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
                 tm.TranDateTime,
                 ms.SKU,
                 ms.Dept,
                 ms.Quantity,
                 ms.ExtOrigPrice,
                 --ms.Cost,
                 ms.MixMatchNumber,
                 ms.ExtRegPrice,
                 ms.Sequence
            from ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId

            where
                ms.TranId = '$tranid'
            and
                ms.Sequence = '$merchandise_sequence'
        ENDQUERY

        # Find the associated sequence
        $sth=$dbh->prepare("$query");
        $sth->execute();
        my $previous_seq=0;
        while (my @tmp = $sth->fetchrow_array()) {
            # We iterate through the merchandise records for this transaction by sequence number in descending order.
            my $RegNum=$tmp[0];
            my $TxnNum=$tmp[1];
            my $TranDateTime=$tmp[2];
            my $SKU=$tmp[3];
            my $Dept=$tmp[4];
            my $Quantity=$tmp[5];
            my $ExtOrigPrice=$tmp[6];
           # my $Cost=$tmp[7];
            my $MixMatchNumber=$tmp[7];
            my $ExtRegPrice=$tmp[8];
            my $item_seq=$tmp[9];
            #my $SalePrice=($ExtRegPrice + $discamt);
            my $SalePrice=($price + $discamt);
            my @seq_array=( "
            $RegNum",
            "$TxnNum",
            "$TranDateTime",
            "$SKU",
            "$Dept",
            "$Quantity",
            "$price",
            "$discamt",
            "$SalePrice",
            "$MixMatchNumber",
             );

            push(@array_to_print,\@seq_array);
            $hash_to_print{"$TxnNum"."."."$item_seq"}=\@seq_array;
        }
    }
    @array_to_print=\@header;
    # Put this array to print in order of txn
    foreach my $TxnNum (sort {$a <=> $b} keys(%hash_to_print)) {
        my $ref=$hash_to_print{$TxnNum};
        push(@array_to_print,$ref);
    }
    $tbl_ref=\@array_to_print;
    if (recordCount($tbl_ref)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        #   get totals for the specified columns.
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }

        }
        my @summary_header=('');
        foreach my $c (@sum_columns) {
            # Grab the headers for the fields we want.
            push(@summary_header,$$tbl_ref[0][$c]);
        }
        my @summary_array_to_print=\@summary_header;
        push @$tbl_ref, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;
        $total_origPrice+=$fieldsum[6];
        $total_MixMatchDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"MixMatchDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];
        my @format = ('', '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        '',
        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        print $q->p("MixMatch Discounts found for store number $storeid between $startdate and $enddate");
        if ($summary) {
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            printResult_arrayref2($tbl_ref, '', \@format);
        }
    } else {
        print $q->p("No MixMatch Discounts found for store number $storeid between $startdate and $enddate");
    }

    # Show the totals of all of the discounts
    @header=(
        "Total Quantity",
        "Total Original Price",
        "Total Item Discounts",
        "Total Transaction Discounts",
        "Total Promotion Discounts",
        "Total MixMatch Discounts",
        "Total Discounts",
        "Total Sell Price"
    );
	# Trimmed out OrigPrice and SellPrice - It is not just a matter of adding this up from all of the discounts.  If there
	# are ever two discounts on the same transaction, these sums become meaningless. - kdg
    @header=(
        "Total Quantity",

        "Total Item Discounts",
        "Total Transaction Discounts",
        "Total Promotion Discounts",
        "Total MixMatch Discounts",
        "Total Discounts",
		"Total Discounts & Promos"

    );

    @array_to_print=\@header;
    my @array=(
        $total_origPrice,
        $total_transDiscounts,
        $total_itemDiscounts,
        $total_promoDiscounts,
        $total_MixMatchDiscounts,
        $total_sellPrice
    );
	# Trimmed out OrigPrice and SellPrice
    @array=(

        $total_transDiscounts,
        $total_itemDiscounts,
        $total_promoDiscounts,
        $total_MixMatchDiscounts

    );
    $totals_hash{"Discounts"}=($totals_hash{"ItemDiscounts"} + $totals_hash{"TransDiscounts"} + $totals_hash{"PromoDiscounts"} + $totals_hash{"MixMatchDiscounts"});
    $totals_hash{"DiscountsLessPromo"}=($totals_hash{"ItemDiscounts"} + $totals_hash{"TransDiscounts"} + $totals_hash{"MixMatchDiscounts"});
    @array=(
        $totals_hash{"Quantity"},
        $totals_hash{"OrigPrice"},
        $totals_hash{"ItemDiscounts"},
        $totals_hash{"TransDiscounts"},
        $totals_hash{"PromoDiscounts"},
        $totals_hash{"MixMatchDiscounts"},
        $totals_hash{"Discounts"},
        $totals_hash{"SellPrice"},
    );

	# Trimmed out OrigPrice and SellPrice
    @array=(
        $totals_hash{"Quantity"},

        $totals_hash{"ItemDiscounts"},
        $totals_hash{"TransDiscounts"},
        $totals_hash{"PromoDiscounts"},
        $totals_hash{"MixMatchDiscounts"},

		$totals_hash{"DiscountsLessPromo"},
		$totals_hash{"Discounts"},

    );
    push(@array_to_print,\@array);
    if (recordCount(\@array_to_print)) {
        my @format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        );
        print $q->h3("Total Discounts");
        printResult_arrayref2(\@array_to_print, '', \@format);
        # Let's check that the totals make sense.  My expectation is that the total OrigPrice, less all of the discounts, should yield the total SellPrice.
        my $expected_sell=(sprintf("%.2f",$totals_hash{"OrigPrice"}) + sprintf("%.2f",$totals_hash{"ItemDiscounts"}) + sprintf("%.2f",$totals_hash{"TransDiscounts"})
        + sprintf("%.2f",$totals_hash{"PromoDiscounts"}) + sprintf("%.2f",$totals_hash{"MixMatchDiscounts"}));
        my $sell_price=sprintf("%.2f",$totals_hash{"SellPrice"});
        my $diff=sprintf("%.2f",($expected_sell - $sell_price));

        if ($diff > 0.00) {
            print $q->p("Note: There appears to be a discrepancy in the totals.");
            print "Expected Sell Price: \$$expected_sell<br>";
            print "Sell Price: \$$sell_price<br>";
            print "Difference: \$$diff<br>";
        }
    } else {
        print $q->p("No Totals data gathered");
    }
    $dbh->disconnect;

    standardfooter();

} elsif ("$report" eq "TXNTOTALBYPERIOD") {
	# Use this for monthly summaries

	my $store_selection   = trim(($q->param("store_selection"))[0]);
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$store_selection);
    my $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $year_month   = trim(($q->param("year_month"))[0]);	
	my $date   = trim(($q->param("date"))[0]);	
	my $period   = trim(($q->param("period"))[0]);	
	if ($year_month) {
		# If the year_month variable is set, then it is because the hyperlink under the year-month of the 
		# by month report has been selected.  Now, we report each day for this month.
		$start_year_selection=substr($year_month,0,4);
		$end_year_selection=$start_year_selection;
		$start_month_selection=substr($year_month,6,2);
		$end_month_selection=$start_month_selection;
		# Set the period to day
		$period = "day";
	}
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-1";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-1";
	my $wheredate="tm.TranDateTime < dateadd(month,1,'$enddate')";
	my $groupby="		group by
					convert(Date, convert(varchar(10),tm.TranDateTime,21))";
	my $select_date="convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate";
 
	my %tlog_hash;
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
	$query=dequote(<< "	ENDQUERY");
		select			 
			convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate,			
			sum(tm.[paymentTotal]) as [PaymentTotal],
			sum(tt.[TotalSale]) as [SaleTotal],
			sum(tt.[Tax1]) as [Tax1],		 
			sum(tt.[Tax2]) as [Tax2],		 
			sum(tt.[Tax3]) as [Tax3],			 
			sum(tt.[Tax4]) as [Tax4],			
			sum(tt.[Tax5]) as [Tax5],
			sum(tt.[ManualTaxTotals]) as [ManTax] 			 
		from 
			****Tlog..tblTranMaster tm
			join ****Tlog..[tblTotal] tt on tm.TranId=tt.TranId
		where 
			tm.TranDateTime >= '$startdate' 
			and 
			$wheredate
				--tm.TranDateTime < dateadd(month,1,'$enddate')
			and
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6) and
				tm.StoreId in ('$storeid')
			$groupby
			--group by
					--convert(Date, convert(varchar(10),tm.TranDateTime,21))					
			order by
					convert(Date, convert(varchar(10),tm.TranDateTime,21))					
	ENDQUERY
	if ($date) {
		# If the date variable is set, then it is because the hyperlink under a specific day has been selected.
		# We now report all sales for that day.
		my @tmp=split(/-/,$date);
		$start_year_selection=$tmp[0];
		$start_month_selection=$tmp[1];
		my $start_day_selection=$tmp[2];
		$startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
		$enddate="$startdate";		
		$wheredate="tm.TranDateTime < dateadd(day,1,'$enddate')";
		$groupby='';	# Clear the groupby to show each sale of the day
		$select_date="convert(varchar(10),tm.TranDateTime,21) as TranDate";
		# We need a differnet query to show all transactions for the day.
		$query=dequote(<< "		ENDQUERY");
			select			 
				tm.TranId,			
				tm.[paymentTotal] as [PaymentTotal],
				tt.[TotalSale] as [SaleTotal],
				tt.[Tax1] as [Tax1],		 
				tt.[Tax2] as [Tax2],		 
				tt.[Tax3] as [Tax3],			 
				tt.[Tax4] as [Tax4],			
				tt.[Tax5] as [Tax5],
				tt.[ManualTaxTotals] as [ManTax] 			 
			from 
				****Tlog..tblTranMaster tm
				join ****Tlog..[tblTotal] tt on tm.TranId=tt.TranId
			where 
				tm.TranDateTime >= '$startdate' 
				and 
				$wheredate
					--tm.TranDateTime < dateadd(month,1,'$enddate')
				and
					tm.TranVoid = 0 and tm.IsPostVoided = 0 and
					tm.TranModifier not in (2,6) and
					tm.StoreId in ('$storeid')
				$groupby
				--group by
						--convert(Date, convert(varchar(10),tm.TranDateTime,21))					
				order by
						--convert(Date, convert(varchar(10),tm.TranDateTime,21))					
						tm.TranNum
		ENDQUERY
	 
	}	 
	#print $q->pre("$query");	
	my $tbl_ref = execQuery_arrayref($dbh, $query); 
	if ($period eq "month") {
	    print $q->h2("Transaction Total Report by Month");
		print $q->h3("Store: $storename Storeid: $storeid");
		# Total each day of the month
		my %month_hash;
		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];
		my @header=@$header_ref;
		my @array_to_print;
		my @totals_array_to_print;
		push(@array_to_print,$header_ref);
		$header[0]='';
		push(@totals_array_to_print,\@header);
				
		if (recordCount($tbl_ref)) { 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				my $date=$$thisrow[0];
				my $payment=$$thisrow[1];
				my $sale=$$thisrow[2];
				my $tax1=$$thisrow[3];
				my $tax2=$$thisrow[4];
				my $tax3=$$thisrow[5];
				my $tax4=$$thisrow[6];
				my $tax5=$$thisrow[7];
				my $mantax=$$thisrow[8];
				my $month_date=substr($date,0,7);
				 
				$month_hash{$month_date} -> [0] += $payment;
				$month_hash{$month_date} -> [1] += $sale;
				$month_hash{$month_date} -> [2] += $tax1;
				$month_hash{$month_date} -> [3] += $tax2;
				$month_hash{$month_date} -> [4] += $tax3;
				$month_hash{$month_date} -> [5] += $tax4;
				$month_hash{$month_date} -> [6] += $tax5;
				$month_hash{$month_date} -> [7] += $mantax; 
			}	
		}	
		foreach my $date (sort keys(%month_hash)) {
			my $payment=$month_hash{$date} -> [0];
			my $sale=$month_hash{$date} -> [1];
			my $tax1=$month_hash{$date} -> [2];
			my $tax2=$month_hash{$date} -> [3];
			my $tax3=$month_hash{$date} -> [4];
			my $tax4=$month_hash{$date} -> [5];
			my $tax5=$month_hash{$date} -> [6];
			my $mantax=$month_hash{$date} -> [7];	
			my @array=("$date","$payment","$sale","$tax1","$tax2","$tax3","$tax4","$tax5","$mantax");
			push(@array_to_print,\@array);
							 
			$fieldsum[1] += $payment;
			$fieldsum[2] += $sale;
			$fieldsum[3] += $tax1;
			$fieldsum[4] += $tax2;
			$fieldsum[5] += $tax3;
			$fieldsum[6] += $tax4;
			$fieldsum[7] += $tax5;
			$fieldsum[8] += $mantax;
 
		}
		push(@totals_array_to_print,\@fieldsum);
		if (recordCount(\@array_to_print)) {	 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
			);
			my @links = ("$scriptname?report=TXNTOTALBYPERIOD&store_selection=$store_selection&year_month=".'$_' );			
			printResult_arrayref2(\@array_to_print, \@links, \@format);
			printResult_arrayref2(\@totals_array_to_print, '', \@format);
		} else {
			print $q->p("No data found");
		} 		
	} elsif ($period eq "day") {
	    print $q->h2("Transaction Total Report by Day");
		print $q->h3("Store: $storename Storeid: $storeid");
		# Total each sale of the day
		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];	# Get the header info (column labels)
		my @header=@$header_ref;		
		my @totals_array_to_print;		
		$header[0]='';					# Clear the first column label (date)
		push(@totals_array_to_print,\@header);
		
		if (recordCount($tbl_ref)) {	 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				foreach my $field (1 .. 8) {
					$fieldsum[$field] += $$thisrow[$field];
				} 
			}	
		}
		#push @$tbl_ref, \@fieldsum;
		push(@totals_array_to_print,\@fieldsum);
		if (recordCount($tbl_ref)) {	 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
			); 			
			my @links = ("$scriptname?report=TXNTOTALBYPERIOD&store_selection=$store_selection&date=".'$_' );				
			printResult_arrayref2($tbl_ref, \@links, \@format);
			printResult_arrayref2(\@totals_array_to_print, '', \@format);
		} else {
			print $q->p("No data found");
		} 
	} else {
		print $q->h2("Transaction Total Report for $date");
		print $q->h3("Store: $storename Storeid: $storeid");
		# Show each sale of the day

		my @fieldsum = ("Total");
		my $header_ref = @$tbl_ref[0];	# Get the header info (column labels)
		my @header=@$header_ref;		
		my @totals_array_to_print;		
		$header[0]='';					# Clear the first column label (date)
		push(@totals_array_to_print,\@header);
		
		if (recordCount($tbl_ref)) {	 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				foreach my $field (1 .. 8) {
					$fieldsum[$field] += $$thisrow[$field];
				} 
			}	
		}
		#push @$tbl_ref, \@fieldsum;
		push(@totals_array_to_print,\@fieldsum);
		if (recordCount($tbl_ref)) {	 
			my @format = (
				'',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
			); 			
			#http://****/p/postoolsbeta.pl?report=SALES_TXN_DTL&tranid_selection=203514413193
			my @links = ("$scriptname?report=SALES_TXN_DTL&&tranid_selection=".'$_' );				
			printResult_arrayref2($tbl_ref, \@links, \@format);
			printResult_arrayref2(\@totals_array_to_print, '', \@format);
		} else {
			print $q->p("No data found");
		} 		
	}
    $dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();
} elsif ("$report" eq "SALES_TAX_CHECK") {
	# For the stores selected, calculate the total taxable and then see if it matches what the tblTax shows for total taxable amount
 	my $storename   = trim(($q->param("storename"))[0]);
	my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $store_label;

	if ($storeid) {
		# The storeid contains the storeid and the store name.  Break them apart here:
		my @tmp=split(/-/,$storeid);

		$storeid=$tmp[0];
		$storeid=~s/ //g;
		$storename=$tmp[1];
		$storename=~s/ //;

	} else {
		$storeid = $storeid_selection;
	}


    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);


    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
	my $date_range=" $startdate to $enddate";
	if ($startdate eq $enddate) {
		$date_range=$startdate;
	}
	$enddate="$enddate 23:59:59";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
	# Determine the store selection
	my $storequery;
	if ($storeid eq "V1")  {
		$store_label="Company Stores";
		# Company Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		";
	} elsif ($storeid eq "V2") {
		$store_label="Contract Stores";
		# Contract Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V2'
		";
	} elsif ($storeid eq "All") {
		$store_label="All Stores";
		# All stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		OR [StoreCode] like 'V2'
		";
	} else {
		$store_label=$storename;
		# A specific store
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreId] = '$storeid'
		";
	}
	print $q->h2("Sales Tax Check for $store_label for $date_range");

	# find all of the transactions for the selected store in this date range
	my @tranId_list;

	my $query="
	select
		TranId
	from
		****Tlog..tblTranMaster
	where
		StoreId in ($storequery)
	and
		TranDateTime > '$startdate'
	and
		TranDateTime < '$enddate'
	and
		TranVoid = 0
	and
		IsPostVoided = 0
	and
		TranModifier not in (2,6)
	" ;

	my $sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		push(@tranId_list,$tmp[0]);

	}
	my $tran_count=0;
	my $error_count=0;
	foreach my $tranId (@tranId_list) {
		$tran_count++;
		# Get price override info & determine if there were returns and/or sales
		my $price_override=0;
		my $note1;
		my $has_sales=0;
		my $has_returns=0;
		my $total_net_price=0;
		my $total_abs_taxable_price=0;
		my $extTaxableAmt=0;
		my %taxable_amount_hash;
		my %taxable_amount_found_hash;
		my $tranDate;
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  ms.[PriceOverride],
			  ms.[ExtNetPrice],

			  case
				when ms.Tax1 = '1'
				or ms.Tax2 = '1'
				or ms.Tax3 = '1'
				or ms.Tax4 = '1'
				or ms.Tax5 = '1'
			  then
				ms.[ExtNetPrice]
			  end
				as [ExtTaxableAmt],
			  ms.Tax1,
			  ms.Tax2,
			  ms.Tax3,
			  ms.Tax4,
			  ms.Tax5,
			  tm.TranDateTime
		  FROM [****TLog].[dbo].[tblMerchandiseSale] ms
			join ****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]

			where
				ms.[TranId] in ('$tranId')
		ENDQUERY

		my $sth=$dbh->prepare("$query");
		$sth->execute();

		while (my @tmp = $sth->fetchrow_array()) {
			$price_override+=$tmp[0];
			$tranDate=$tmp[8];
			my $find_tax_id=1;
			if ($tmp[3]) {
				# This is tax 1
				$taxable_amount_hash{1}+=$tmp[1];
				#$find_tax_id=0;

			}
			if (($tmp[4]) && ($find_tax_id)) {
				# This is tax 2
				$taxable_amount_hash{2}+=$tmp[1];
				#$find_tax_id=0;

			}
			if (($tmp[5]) && ($find_tax_id)) {
				# This is tax 3
				$taxable_amount_hash{3}+=$tmp[1];
				#$find_tax_id=0;
			}
			if (($tmp[6]) && ($find_tax_id)) {
				# This is tax 4
				$taxable_amount_hash{4}+=$tmp[1];
				#$find_tax_id=0;
			}
			if (($tmp[7]) && ($find_tax_id)) {
				# This is tax 5
				$taxable_amount_hash{5}+=$tmp[1];
				#$find_tax_id=0;
			}
		}
		if ($price_override) {
			$note1="$price_override price overrides: $price_override";
		}

		my $TaxableAmount;

		# Show the tax info for this transaction
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  tt.[Sequence]
			  ,tt.[TaxId]
			  ,tt.[TaxAmount]
			  ,tt.[TaxableAmount]
			  ,tm.[paymentTotal]
			  ,tm.[TranNum]
			  ,tm.[TranDateTime]
			  ,tm.[isReturn]


		  FROM [****TLog].[dbo].[tblTax] tt
			join ****Tlog..tblTranMaster tm on tt.[TranId] = tm.[TranId]


			where
				tt.[TranId] in ('$tranId')
			order by
				tt.[Sequence]

		ENDQUERY

		my $tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {

			my $tranNum;

			my $isReturn=$$tbl_ref[1][7];
			###
			my $paymentTotal=$$tbl_ref[1][4];
			my @fieldsum = ('<b>Total:</b>','',0,0,0);
			my %sum_columns_hash = (2 => '1',3 => '1',4 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				# paymentTotal is returned at index 4 but is actually the transaction total
				# This value is saved above as the paymentTotal so here we actually calculate
				# the total for each item.  We then double-check at the end to verify that the
				# totals match.
				$$thisrow[4]=($$thisrow[2] + $$thisrow[3]);
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				$tranNum=$$thisrow[5];
				$tranDate=$$thisrow[6];
				my $taxId=$$thisrow[1];
				# From the tblTax, we collect that total taxable amount for each tax id.
				$taxable_amount_found_hash{$taxId}=$$thisrow[3];
			}
			###

		}
		#
			# Now, simply check the taxable amount calculated with the amount recorded in the tblTax

			foreach my $taxId (sort(keys(%taxable_amount_hash))) {
				my $calculated=sprintf("%.2f",abs($taxable_amount_hash{$taxId}));
				my $found=sprintf("%.2f",$taxable_amount_found_hash{$taxId});
				unless ($calculated eq $found) {
					my $diff=($calculated - $found);
					$error_count++;
					print $q->a({-href=>"$scriptname?report=SALES_TXN_DTL&tranid_selection=$tranId", -target=>'_top'}, "$tranId"),"WARNING:  TranNum: $tranId at $tranDate  -  Total taxable for tax $taxId was caculated to be \$$calculated but shows as  \$$found.  This is a difference of \$$diff. $note1<br />";
				}
			}
		#
	}
	print $q->p("Found $tran_count transactions - $error_count errors");
	$dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
	standardfooter();
} elsif ("$report" eq "SALES_TAX_CHECK_orig") {
 	my $storename   = trim(($q->param("storename"))[0]);
	my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $store_label;

	if ($storeid) {
		# The storeid contains the storeid and the store name.  Break them apart here:
		my @tmp=split(/-/,$storeid);

		$storeid=$tmp[0];
		$storeid=~s/ //g;
		$storename=$tmp[1];
		$storename=~s/ //;

	} else {
		$storeid = $storeid_selection;
	}


    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);


    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
	my $date_range=" $startdate to $enddate";
	if ($startdate eq $enddate) {
		$date_range=$startdate;
	}
	$enddate="$enddate 23:59:59";

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
	# Determine the store selection
	my $storequery;
	if ($storeid eq "V1")  {
		$store_label="Company Stores";
		# Company Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		";
	} elsif ($storeid eq "V2") {
		$store_label="Contract Stores";
		# Contract Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V2'
		";
	} elsif ($storeid eq "All") {
		$store_label="All Stores";
		# All stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		OR [StoreCode] like 'V2'
		";
	} else {
		$store_label=$storename;
		# A specific store
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreId] = '$storeid'
		";
	}
	print $q->h2("Sales Tax Check for $store_label for $date_range");

	# find all of the transactions for the selected store in this date range
	my @tranId_list;
	my $query="
	select
		TranId
	from
		****Tlog..tblTranMaster
	where
		StoreId in ($storequery)
	and
		TranDateTime > '$startdate'
	and
		TranDateTime < '$enddate'
	and
		TranVoid = 0
	and
		IsPostVoided = 0
	and
		TranModifier not in (2,6)
	" ;

	my $sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		push(@tranId_list,$tmp[0]);

	}
	my $tran_count=0;
	my $error_count=0;
	foreach my $tranId (@tranId_list) {
		# Get price override info & determine if there were returns and/or sales
		my $price_override=0;
		my $note1;
		my $has_sales=0;
		my $has_returns=0;
		my $total_net_price=0;
		my $total_abs_net_price=0;
		my $extTaxableAmt=0;
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  ms.[PriceOverride],
			  ms.[ExtNetPrice],
			  --convert(int,sign(ms.ExtOrigPrice) * ms.ExtTaxableAmt) as [ExtTaxableAmt]
			  (ms.[ExtNetPrice] * (ms.Tax1 + ms.Tax2 + ms.Tax3 + ms.Tax4 + ms.Tax5)) as [ExtTaxableAmt]
		  FROM [****TLog].[dbo].[tblMerchandiseSale] ms
			join ****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]

			where
				ms.[TranId] in ('$tranId')
		ENDQUERY

		my $sth=$dbh->prepare("$query");
		$sth->execute();

		while (my @tmp = $sth->fetchrow_array()) {
			$price_override+=$tmp[0];
			if ($tmp[1] > 0) {
				$has_sales=1;
			}
			if ($tmp[1] < 0) {
				$has_returns=1;
			}
			$total_net_price+=$tmp[1];
			$total_abs_net_price+=abs($tmp[1]);
			$extTaxableAmt+=$tmp[2];
		}
		if ($price_override) {
			$note1="$price_override price overrides: $price_override";
		}
		# Get the non-taxable totals for this transaction
		my $non_tax_total;

		$query=dequote(<< "		ENDQUERY");
		SELECT
			  ms.[ExtNetPrice]

		  FROM [****TLog].[dbo].[tblMerchandiseSale] ms
			join ****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]

			where
				ms.[TranId] in ('$tranId')
			and
				(ms.Tax1 = 0 and
				ms.Tax2 = 0 and
				ms.Tax3 = 0 and
				ms.Tax4 = 0 and
				ms.Tax5 = 0)

		ENDQUERY

		$sth=$dbh->prepare("$query");
		$sth->execute();

		while (my @tmp = $sth->fetchrow_array()) {
			$non_tax_total+=$tmp[0];
		}
		# Get the non-merchandise totals for this transaction
		my $non_merch_total;

		$query=dequote(<< "		ENDQUERY");
		SELECT
			  [SellAmount]
		FROM
			[****TLog].[dbo].[tblNonMerchandiseSale] ms
		join
			****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]

		where
			ms.[TranId] in ('$tranId')

		ENDQUERY

		$sth=$dbh->prepare("$query");
		$sth->execute();

		while (my @tmp = $sth->fetchrow_array()) {
			$non_merch_total+=$tmp[0];
		}
		# Get the tender total
		my $tender_total;
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  [TenderAmount]
		FROM
			[****TLog].[dbo].[tblPayment]


		where
			[TranId] in ('$tranId')


		ENDQUERY

		$sth=$dbh->prepare("$query");
		$sth->execute();

		while (my @tmp = $sth->fetchrow_array()) {
			$tender_total+=$tmp[0];
		}
		# Show the tax info for this transaction
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  tt.[Sequence]
			  ,tt.[TaxId]
			  ,tt.[TaxAmount]
			  ,tt.[TaxableAmount]
			  ,tm.[paymentTotal]
			  ,tm.[TranNum]
			  ,tm.[TranDateTime]
			  ,tm.[isReturn]


		  FROM [****TLog].[dbo].[tblTax] tt
			join ****Tlog..tblTranMaster tm on tt.[TranId] = tm.[TranId]


			where
				tt.[TranId] in ('$tranId')
			order by
				tt.[Sequence]

		ENDQUERY

		my $tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			$tran_count++;
			my $tranNum;
			my $tranDate;
			my $isReturn=$$tbl_ref[1][7];
			###
			my $paymentTotal=$$tbl_ref[1][4];
			my @fieldsum = ('<b>Total:</b>','',0,0,0);
			my %sum_columns_hash = (2 => '1',3 => '1',4 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				# paymentTotal is returned at index 4 but is actually the transaction total
				# This value is saved above as the paymentTotal so here we actually calculate
				# the total for each item.  We then double-check at the end to verify that the
				# totals match.
				$$thisrow[4]=($$thisrow[2] + $$thisrow[3]);
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				$tranNum=$$thisrow[5];
				$tranDate=$$thisrow[6];

			}
			###
			#print $q->h3("Tax info for this transaction:");
			my @format = ('', '',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},

			);
			push(@$tbl_ref,\@fieldsum);
			my $total_tax_amount=$fieldsum[2];
			$paymentTotal=sprintf("%.2f",$fieldsum[4]);

			if ($extTaxableAmt < 0) {
				# The total tax amount is a bit tricky.  It only shows the absolute value. So, I am using the extTaxableAmount to determine
				# what the sign of the tax amount should be
				$total_tax_amount=(0 - $total_tax_amount);
				$paymentTotal=sprintf("%.2f",(0 - $paymentTotal));
			}
			# Total tax amount + total abs price - total non taxed should equal paymentTotal
			my $calculatedPaymentTotal=sprintf("%.2f",($total_net_price + $total_tax_amount - $non_tax_total ));

			unless ($calculatedPaymentTotal eq $paymentTotal) {
				my $diff=($calculatedPaymentTotal - $paymentTotal);
				print $q->a({-href=>"$scriptname?report=SALES_TXN_DTL&tranid_selection=$tranId", -target=>'_top'}, "$tranId"),"WARNING:  TranNum: $tranNum at $tranDate  -  Total payment was caculated to be \$$calculatedPaymentTotal but shows as  \$$paymentTotal.  This is a difference of \$$diff. $note1<br />";
			}

		}
	}
	###
	print $q->p("Found $tran_count transactions - $error_count errors");
	$dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
	standardfooter();

} elsif ("$report" eq "SALES_TXN") {
 	my $storename   = trim(($q->param("storename"))[0]);
	my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $price_override_selection   = trim(($q->param("price_override_selection"))[0]);
	my $tran_amt_selection   = trim(($q->param("tran_amt_selection"))[0]);
	

	if ($storeid) {
		# The storeid contains the storeid and the store name.  Break them apart here:
		my @tmp=split(/-/,$storeid);

		$storeid=$tmp[0];
		$storeid=~s/ //g;
		$storename=$tmp[1];
		$storename=~s/ //;
	} else {
		$storeid = $storeid_selection;
	}
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $company_selection   = trim(($q->param("company_selection"))[0]);
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	print $q->h2("Transaction Report for $storename");
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    if ($date_selection) {
		$startdate=$date_selection;
		$enddate=$date_selection;
	}
	my $having_clause = '';
	if ($tran_amt_selection) {
		 
		$tran_amt_selection =~ s/\$//g;
		unless ($tran_amt_selection =~ /\D/) {
			$having_clause = " HAVING sum(ms.ExtSellPrice) > $tran_amt_selection "; 						
		}
	}
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    my $summary=0;
    my %storeData_hash;
    my %tlog_hash;


	my $tbl_ref;


    # Get the tlog data
    if ($enddate eq $startdate) {
		my $price_override_where;
		if ($price_override_selection) {
			print $q->h4("Transactions where a price override was used");
			$price_override_where=" and ms.[PriceOverride] > 0 ";
		}
        # Print detail if the request was only for one day.
		 print $q->h3("Date: $enddate");
        $query=dequote(<< "        ENDQUERY");
            select

				tm.TranId as TranId,
				case when tm.StoreId='2098' and tm.RegisterId = '02' and
					tm.TranNum between 4013 and 4023 and
					convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
						then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
				tm.RegisterId as RegNum,
				--convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
				sum(convert(int,sign(ms.ExtOrigPrice) * ms.Quantity)) as [Qty],
				sum(ms.ExtSellPrice) as [Net],
				tot.Tax1 as [TotalTax1],
				tot.ManualTax1 as [ManualTax1],
				tot.Tax2 as [TotalTax2],
				tot.ManualTax2 as [ManualTax2],
				tot.Tax3 as [TotalTax3],
				tot.ManualTax3 as [ManualTax3],
				tot.Tax4 as [TotalTax4],
				tot.ManualTax4 as [ManualTax4],
				tot.Tax5 as [TotalTax5],

				tm.PaymentTotal

            from
				****Tlog..tblTranMaster tm
				join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
				join ****Tlog..tblTotal tot on tm.TranId=tot.TranId

            where
				tm.TranDateTime >= '$startdate'
            and
				tm.TranDateTime < dateadd( d, 1, '$enddate')
			and
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6)
			and
				tm.StoreId in ('$storeid')
			 
			$price_override_where
            group by   tm.TranId, tm.TranNum,tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.StoreId,
						tot.Tax1, tot.ManualTax1, tot.Tax2, tot.ManualTax2, tot.Tax3, tot.ManualTax3, tot.Tax4, tot.ManualTax4, tot.Tax5,
						tm.PaymentTotal
			$having_clause	
            order by TxnNum, tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
								
        ENDQUERY

		#print $q->pre("$query");
		$tbl_ref = execQuery_arrayref($dbh, $query);
		 
		if (recordCount($tbl_ref)) {
			my @fieldsum = ('<b>Total:</b>','','',0,0,0,0,0,0,0,0,0,0,0,0);

			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				my $tranid=$$thisrow[0];
				my $txn=$$thisrow[1];
 	
				my %totals_hash;
				# Check the tax totals
				$query="
					select
						tot.Tax1,
						tot.ManualTax1,
						tot.Tax2,
						tot.ManualTax2,
						tot.TotalSale

					from
						****Tlog..tblTranMaster tm
						join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
					where
						tm.TranId in ('$tranid')
				";

				my $sth=$dbh->prepare("$query");
				$sth->execute();

				while (my @tmp = $sth->fetchrow_array()) {
					$totals_hash{"Tax1"}=$tmp[0];
					$totals_hash{"ManualTax1"}=$tmp[0];
					$totals_hash{"Tax2"}=$tmp[0];
					$totals_hash{"ManualTax2"}=$tmp[0];
					$totals_hash{"TotalSale"}=$tmp[0];
				}
				# See what SKUs were on this transaction and whether they were taxable
				$query="
					select
						Tax1,
						Tax2,
						ExtSellPrice
					from
						****Tlog..tblMerchandiseSale
					where
						TranId in ('$tranid')
				";
				#   get totals for the specified columns.
				$fieldsum[3] += trim($$thisrow[3]);
				$fieldsum[4] += trim($$thisrow[4]);
				$fieldsum[5] += trim($$thisrow[5]);
				$fieldsum[6] += trim($$thisrow[6]);
				$fieldsum[7] += trim($$thisrow[7]);
				$fieldsum[8] += trim($$thisrow[8]);
				$fieldsum[9] += trim($$thisrow[9]);
				$fieldsum[10] += trim($$thisrow[10]);
				$fieldsum[11] += trim($$thisrow[11]);
				$fieldsum[12] += trim($$thisrow[12]);
				$fieldsum[13] += trim($$thisrow[13]);
				$fieldsum[14] += trim($$thisrow[14]);
				#$fieldsum[15] += trim($$thisrow[15]);
			}
			push @$tbl_ref, \@fieldsum;
			my $tranid=$$tbl_ref[1][0];
			my $txn=$$tbl_ref[1][1];

			###
			my @links = ("$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
			my @format = ('', '','','',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
			);
			printResult_arrayref2($tbl_ref, \@links, \@format);
		} else {
			print $q->p("No data found");
		}
    } else {
		print $q->h3("Date: $startdate to $enddate ");
		my $price_override_where;
		if ($price_override_selection) {
			print $q->h4("Transactions where a price override was used");
			$price_override_where=" and ms.[PriceOverride] > 0 ";
		}
        # If the request was for more than one day, print a summary only.

	 
        $query=dequote(<< "        ENDQUERY");
            select
				--convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
				convert(Date, convert(varchar(10),tm.TranDateTime,23)) as TranDate,
				sum(ms.ExtSellPrice) as [Net],
				sum(tot.Tax1) as [TotalTax1],		 
				--sum(tot.ManualTax1) as [ManualTax1],
				sum(tot.Tax2) as [TotalTax2],
				--sum(tot.ManualTax2) as [ManualTax2],
				sum(tot.Tax3) as [TotalTax3],
				--sum(tot.ManualTax3) as [ManualTax3],
				sum(tot.Tax4) as [TotalTax4],
				--sum(tot.ManualTax4) as [ManualTax4],
				sum(tot.Tax5) as [TotalTax5],
				sum(tm.PaymentTotal) as [TotalPayment]
            from ****Tlog..tblTranMaster tm
				join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
				join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
            where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and
                    tm.TranVoid = 0 and tm.IsPostVoided = 0
				and
                    tm.TranModifier not in (2,6)
				and
                    tm.StoreId in ('$storeid')
			$price_override_where

            group by
                        convert(Date, convert(varchar(10),tm.TranDateTime,23))
						
			$having_clause	
            order by
                        convert(Date, convert(varchar(10),tm.TranDateTime,23))
        ENDQUERY
		# The query above was giving incorrect values for total taxes and total payment. Just show that for
		# the report by day. - 2014-08-25 - kdg
        $query=dequote(<< "        ENDQUERY");
            select			 
				convert(Date, convert(varchar(10),tm.TranDateTime,23)) as TranDate,
				sum(ms.ExtSellPrice) as [Net]				 								
            from ****Tlog..tblTranMaster tm
				join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
				join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
				 
            where 
				tm.TranDateTime >= '$startdate'
				and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and tm.TranVoid = 0 and tm.IsPostVoided = 0
				and tm.TranModifier not in (2,6)
				and tm.StoreId in ('$storeid')
				$price_override_where
            group by
                convert(Date, convert(varchar(10),tm.TranDateTime,23))						
				$having_clause	
            order by
                        convert(Date, convert(varchar(10),tm.TranDateTime,23))
        ENDQUERY
		
 
        $query=dequote(<< "        ENDQUERY");
            select			 
				convert(Date, convert(varchar(10),tm.TranDateTime,23)) as TranDate,
				ms.ExtSellPrice as [Net]				
 
				
            from ****Tlog..tblTranMaster tm
				join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
				
				 
            where 
				tm.TranDateTime >= '$startdate'
				and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and tm.TranVoid = 0 and tm.IsPostVoided = 0
				and tm.TranModifier not in (2,6)
				and tm.StoreId in ('$storeid')
				$price_override_where
            --group by
                --convert(Date, convert(varchar(10),tm.TranDateTime,23))						
				$having_clause	
            order by
                        convert(Date, convert(varchar(10),tm.TranDateTime,23))
        ENDQUERY
		
		#print $q->pre("$query");
		$tbl_ref = execQuery_arrayref($dbh, $query);
		my %net_sale_hash;
		if (recordCount($tbl_ref)) {
		 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				my $date = $$thisrow[0];
				my $net = $$thisrow[1];
 
				
				$net_sale_hash{$date} -> [0] += $net;
 			
			}			
		}
		
        $query=dequote(<< "        ENDQUERY");
            select	
				convert(Date, convert(varchar(10),tm.TranDateTime,23)) as TranDate,			
				tot.Tax1 as [TotalTax1],
				tot.Tax2 as [TotalTax2],
				tot.Tax3 as [TotalTax3],
				tot.Tax4 as [TotalTax4],
				tot.Tax5 as [TotalTax5],
				tm.PaymentTotal as [TotalPayment]				
            from ****Tlog..tblTranMaster tm
			 
				join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
				 
            where 
				tm.TranDateTime >= '$startdate'
				and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and tm.TranVoid = 0 and tm.IsPostVoided = 0
				and tm.TranModifier not in (2,6)
				and tm.StoreId in ('$storeid')
				$price_override_where
            --group by
                --convert(Date, convert(varchar(10),tm.TranDateTime,23))						
				$having_clause	
            order by
                        convert(Date, convert(varchar(10),tm.TranDateTime,23))
        ENDQUERY
		
		$tbl_ref = execQuery_arrayref($dbh, $query);
		 
		if (recordCount($tbl_ref)) {
		 
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				my $date = $$thisrow[0];
				my $TotalTax1 = $$thisrow[1];
				my $TotalTax2 = $$thisrow[2];
				my $TotalTax3 = $$thisrow[3];
				my $TotalTax4 = $$thisrow[4];
				my $TotalTax5 = $$thisrow[5];
				my $TotalPayment = $$thisrow[6];
 
				
				$net_sale_hash{$date} -> [1] += $TotalTax1;
				$net_sale_hash{$date} -> [2] += $TotalTax2;
				$net_sale_hash{$date} -> [3] += $TotalTax3;
				$net_sale_hash{$date} -> [4] += $TotalTax4;
				$net_sale_hash{$date} -> [5] += $TotalTax5;
				$net_sale_hash{$date} -> [6] += $TotalPayment;
 			
			}			
		}		
		my @header = ("Date","Net","Tax1","Tax2","Tax3","Tax4","Tax5","TotalPayment");	 
		 
		my @array_to_print = \@header;
		my @fieldsum=("Total",0,0,0,0,0,0,0);;
		foreach my $date (sort keys(%net_sale_hash)) {
			my $net=$net_sale_hash{$date} -> [0];
			my $TotalTax1=$net_sale_hash{$date} -> [1];
			my $TotalTax2=$net_sale_hash{$date} -> [2];
			my $TotalTax3=$net_sale_hash{$date} -> [3];
			my $TotalTax4=$net_sale_hash{$date} -> [4];
			my $TotalTax5=$net_sale_hash{$date} -> [5];
			my $TotalPayment=$net_sale_hash{$date} -> [6];
			$fieldsum[1]+=$net;
			$fieldsum[2]+=$TotalTax1;
			$fieldsum[3]+=$TotalTax2;
			$fieldsum[4]+=$TotalTax3;
			$fieldsum[5]+=$TotalTax4;
			$fieldsum[6]+=$TotalTax5;
			$fieldsum[7]+=$TotalPayment;
 
			my @array = ("$date","$net","$TotalTax1","$TotalTax2","$TotalTax3","$TotalTax4","$TotalTax5","$TotalPayment");
			push(@array_to_print,\@array);
		}
		 
		push(@array_to_print,\@fieldsum);
		
		if (recordCount(\@array_to_print)) {

			my @links = ("$scriptname?report=SALES_TXN&storeid_selection=$storeid&price_override_selection=$price_override_selection&storename=$storename&date_selection=".'$_','' );
			my @format = ('',
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			);
			printResult_arrayref2(\@array_to_print, \@links, \@format);


		} else {
			print $q->p("No data found");
		}

    }


    $dbh->disconnect;

	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();

} elsif ("$report" eq "SALES_TXN_SUMMARY") {
 	my $storename   = trim(($q->param("storename"))[0]);
	my $storeid_selection   = trim(($q->param("storeid_selection"))[0]);
	my $storeid   = trim(($q->param("store_selection"))[0]);
	my $summary_selection   = trim(($q->param("summary_selection"))[0]);
	my $tran_amt_selection   = trim(($q->param("tran_amt_selection"))[0]);
 

	if ($storeid) {
		# The storeid contains the storeid and the store name.  Break them apart here:
		my @tmp=split(/-/,$storeid);

		$storeid=$tmp[0];
		$storeid=~s/ //g;
		$storename=$tmp[1];
		$storename=~s/ //;
	} else {
		$storeid = $storeid_selection;
	}
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $company_selection   = trim(($q->param("company_selection"))[0]);
	my $date_selection   = trim(($q->param("date_selection"))[0]);
	print $q->h2("Transaction Report for $storename");
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    if ($date_selection) {
		$startdate=$date_selection;
		$enddate=$date_selection;
	}
	
	my $having_clause = '';
	if ($tran_amt_selection) {
		 
		$tran_amt_selection =~ s/\$//g;
		unless ($tran_amt_selection =~ /\D/) {
			$having_clause = " HAVING sum(ms.ExtSellPrice) > $tran_amt_selection "; 						
		}
	}
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
	
	# Determine the store selection
	my $storequery;
	my $store_label = '';
	if ($storeid eq "V1")  {
		$store_label="Company Stores";
		# Company Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		";
	} elsif ($storeid eq "V2") {
		$store_label="Contract Stores";
		# Contract Stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V2'
		";
	} elsif ($storeid eq "All") {
		$store_label="All Stores";
		# All stores
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreCode] like 'V1'
		OR [StoreCode] like 'V2'
		";
	} else {
		$store_label=$storename;
		# A specific store
		$storequery="
		SELECT [StoreId]
		FROM [****Data].[dbo].[rtblStores]
		WHERE [StoreId] = '$storeid'
		";
	}	
	
    my $summary=0;
    my %storeData_hash;
    my %tlog_hash;


	my $tbl_ref;


    # Get the tlog data

	print $q->h3("Date: $startdate to $enddate for $store_label");
 
	if ($tran_amt_selection) {
		$tran_amt_selection =~ s/\$//g;
		print $q->h4("Transactions more than \$$tran_amt_selection");
	}
 
=pod
	$query=dequote(<< "	ENDQUERY");
		select		
			tm.StoreId,
			convert(Date, convert(varchar(10),tm.TranDateTime,23)) as TranDate,
			sum(ms.ExtSellPrice) as [Net]
 
		from ****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
			join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
		where tm.TranDateTime >= '$startdate'
		and tm.TranDateTime < dateadd( d, 1, '$enddate')
			and
				tm.TranVoid = 0 and tm.IsPostVoided = 0
			and
				tm.TranModifier not in (2,6)
			and
				tm.StoreId in ($storequery)
		 

		group by  
					 convert(Date, convert(varchar(10),tm.TranDateTime,23))
		$having_clause	
		order by 
					tm.StoreId,convert(Date, convert(varchar(10),tm.TranDateTime,23))
	ENDQUERY
=cut	
	my %date_hash = ();
	my %store_hash = ();
	my %store_net_hash = ();
        $query=dequote(<< "        ENDQUERY");
            select
				tm.StoreId,
				 
				case when tm.StoreId='2098' and tm.RegisterId = '02' and
					tm.TranNum between 4013 and 4023 and
					convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
						then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
				tm.RegisterId as RegNum,
				convert(DateTime, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
				sum(convert(int,sign(ms.ExtOrigPrice) * ms.Quantity)) as [Qty],
				sum(ms.ExtSellPrice) as [Net],
				tot.Tax1 as [TotalTax1],
				tot.ManualTax1 as [ManualTax1],
				tot.Tax2 as [TotalTax2],
				tot.ManualTax2 as [ManualTax2],
				tot.Tax3 as [TotalTax3],
				tot.ManualTax3 as [ManualTax3],
				tot.Tax4 as [TotalTax4],
				tot.ManualTax4 as [ManualTax4],
				tot.Tax5 as [TotalTax5],

				tm.PaymentTotal

            from
				****Tlog..tblTranMaster tm
				join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
				join ****Tlog..tblTotal tot on tm.TranId=tot.TranId

            where
				tm.TranDateTime >= '$startdate'
            and
				tm.TranDateTime < dateadd( d, 1, '$enddate')
			and
				tm.TranVoid = 0 and tm.IsPostVoided = 0 and
				tm.TranModifier not in (2,6)
			and
				tm.StoreId in ($storequery)
 
            group by   tm.TranId, tm.TranNum,tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.StoreId,
						tot.Tax1, tot.ManualTax1, tot.Tax2, tot.ManualTax2, tot.Tax3, tot.ManualTax3, tot.Tax4, tot.ManualTax4, tot.Tax5,
						tm.PaymentTotal
			$having_clause	
            order by 
						tm.StoreId,TxnNum, tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
								
        ENDQUERY
		
	
	#print $q->pre("$query");
	$tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
 

		for my $datarows (1 .. $#$tbl_ref){
			my $thisrow = $$tbl_ref[$datarows];

			my $storeid = $$thisrow[0];
			my $net = $$thisrow[5];
			$store_hash{$storeid}++;
			$store_net_hash{$storeid}+= $net;
		}


		#my @links = ("$scriptname?report=SALES_TXN&storeid_selection=$storeid&price_override_selection=$price_override_selection&storename=$storename&date_selection=".'$_','' );
		my @format = ('','','','','',
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},		
		);
		unless ($summary_selection) {
			printResult_arrayref2($tbl_ref, '', \@format);
		}

		# Print the store hash results
		print $q->p();
		my @header = ("StoreId","TxnCount","Net");
		my @array_to_print = \@header;
		foreach my $storeid (sort(keys(%store_hash))) {
			my $count = $store_hash{$storeid};
			my $net = $store_net_hash{$storeid};
			my @array = ("$storeid","$count","$net");
			#print "Store: $storeid Count: $count Net: $net<br />";
			push(@array_to_print,\@array);
		}
		@format = ('','',
		{'align' => 'Right', 'currency' => ''});
		printResult_arrayref2(\@array_to_print, '', \@format);

	} else {
		print $q->p("No data found");
	}

 


    $dbh->disconnect;

	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();	
} elsif ("$report" eq "SALES_TXN_DTL") {
    my $tranid_selection = trim(($q->param("tranid_selection"))[0]);

 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
###
 	# Get Store Info
	my $storeid;
	my $storename;
	my $store_status;
	$query=dequote(<< "	ENDQUERY");
	SELECT
		  rs.[StoreName],
		  rs.[StoreCode],
		  rs.[StoreId]
	  FROM
		****Tlog..tblTranMaster tm
		join [****Data].[dbo].[rtblStores] rs on tm.[StoreId] = rs.[StoreId]
		where
			tm.[TranId] in ('$tranid_selection')


	ENDQUERY

	my $sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$storename=$tmp[0];
		if ($tmp[1] eq "V1") {
			$store_status="Company Store";
		}
		if ($tmp[1] eq "V2") {
			$store_status="Contract Store";
		}
		$storeid=$tmp[2];
	}
	my @tmp=split(/-/,$storename);
	$storename=$tmp[1];
	print $q->h2("Report for Transaction ID: $tranid_selection - Store: $storename - $store_status");

	# Get any non merch
	my $non_merch;

	# Get the non-taxable totals for this transaction
	my $non_tax_total;

	$query=dequote(<< "	ENDQUERY");
	SELECT
		  ms.[ExtNetPrice]

	  FROM [****TLog].[dbo].[tblMerchandiseSale] ms
		join ****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]
		where
			ms.[TranId] in ('$tranid_selection')
		and
			(ms.Tax1 = 0 and
			ms.Tax2 = 0 and
			ms.Tax3 = 0 and
			ms.Tax4 = 0 and
			ms.Tax5 = 0)

	ENDQUERY

	$sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$non_tax_total+=$tmp[0];
	}

	# Get any non merch
	my $non_merch_total;
	$query=dequote(<< "	ENDQUERY");
	SELECT
		  [SellAmount]
	FROM
		[****TLog].[dbo].[tblNonMerchandiseSale] ms
	join
		****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]

	where
		ms.[TranId] in ('$tranid_selection')


	ENDQUERY

	$sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$non_merch_total+=$tmp[0];
	}
	# Get the tender total
	my $tender_total;
	$query=dequote(<< "	ENDQUERY");
	SELECT
		  [TenderAmount]
	FROM
		[****TLog].[dbo].[tblPayment]


	where
		[TranId] in ('$tranid_selection')


	ENDQUERY

	$sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$tender_total+=$tmp[0];
	}
	# Get the transaction details
	my $has_sales=0;
	my $has_returns=0;
	my $total_net_price=0;
	my $total_abs_taxable_price=0;
	my $extTaxableAmt=0;
	my $extNet=0;

	$query=dequote(<< "	ENDQUERY");
		select
			tm.TranId as TranId,
			case when tm.StoreId='2098' and tm.RegisterId = '02' and
				tm.TranNum between 4013 and 4023 and
				convert(Date, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
					then tm.TranNum - 4000
					else tm.TranNum end as TxnNum,

			convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
			ms.Sequence as Seq,
			tm.RegisterId as Reg,
			ms.SKU,
			ssi.descrip,
			ms.Dept,
			convert(int,sign(ms.ExtOrigPrice) * ms.Quantity) as [Qty],
			ms.ExtSellPrice as [Net],

			case
				when ms.Tax1 = '1'
				or ms.Tax2 = '1'
				or ms.Tax3 = '1'
				or ms.Tax4 = '1'
				or ms.Tax5 = '1'
			then
				ms.[ExtNetPrice]
			end
				as [ExtTaxableAmt],
			ms.Tax1,
			ms.Tax2,
			ms.Tax3,
			ms.Tax4,
			ms.Tax5,
			ms.PromoPriceAdjustAmount as PromoPriceAdjAmt,
			ms.PromoPercentAdjustAmount as PromoPcntAdjAmt,
			ms.PriceOverride

		from
			****Tlog..tblTranMaster tm
			left join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
			join [****Data].[dbo].[tblStoreSkuInfo] ssi on ssi.SKU = ms.SKU and ssi.storeid = '$storeid'

		where
			tm.TranId in ('$tranid_selection')
		order by TxnNum, tm.RegisterId,
					convert(Date, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
	ENDQUERY


	my $tbl_ref = execQuery_arrayref($dbh, $query);

	if (recordCount($tbl_ref)) {
		# Get the totals
		my @fieldsum = ('<b>Total:</b>','','','','','','','',0,0,0,'','','','','','');
		my %sum_columns_hash = (8 => '1',9 => '1',10 => '1',16 => '1');
		my %taxable_amount_hash;
		my %taxable_amount_found_hash;
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = $$tbl_ref[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if ($sum_columns_hash{$column}) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                } else {
                    $fieldsum[$column] = '';
                }
            }
			if ($$thisrow[8] > 0) {
				$has_sales=1;
			}
			if ($$thisrow[8] < 0) {
				$has_returns=1;
			}

			$total_net_price+=$$thisrow[8];
			$total_abs_taxable_price+=abs($$thisrow[9]);
			my $find_tax_id=1;
			if ($$thisrow[10]) {
				# This is tax 1
				$taxable_amount_hash{1}+=$$thisrow[8];
				#$find_tax_id=0;
			}
			if (($$thisrow[11]) && ($find_tax_id)) {
				# This is tax 2
				$taxable_amount_hash{2}+=$$thisrow[8];
				#$find_tax_id=0;
			}
			if (($$thisrow[12]) && ($find_tax_id)) {
				# This is tax 3
				$taxable_amount_hash{3}+=$$thisrow[8];
				#$find_tax_id=0;
			}
			if (($$thisrow[13]) && ($find_tax_id)) {
				# This is tax 4
				$taxable_amount_hash{4}+=$$thisrow[8];
				#$find_tax_id=0;
			}
			if (($$thisrow[14]) && ($find_tax_id)) {
				# This is tax 5
				$taxable_amount_hash{5}+=$$thisrow[8];
				#$find_tax_id=0;
			}
        }
		my @format = ('', '','','','','','','','',
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			'','','','','',
			{'align' => 'Right', 'currency' => ''},
			'',''

		);
		$extNet=$fieldsum[8];
		$extTaxableAmt=sprintf("%.2f",$fieldsum[9]);
		my $TaxableAmount;
		push(@$tbl_ref,\@fieldsum);
		printResult_arrayref2($tbl_ref, '', \@format);
		# Show the tax info for this transaction
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  tt.[Sequence]
			  ,tt.[TaxId]
			  ,tt.[TaxAmount]
			  ,tt.[TaxableAmount]
			  ,tm.[paymentTotal]
			  ,tm.[isReturn]

		  FROM [****TLog].[dbo].[tblTax] tt
			join ****Tlog..tblTranMaster tm on tt.[TranId] = tm.[TranId]

			where
				tt.[TranId] in ('$tranid_selection')
			order by
				tt.[Sequence]

		ENDQUERY


		my $tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			my $paymentTotal=$$tbl_ref[1][4];
			my $isReturn=$$tbl_ref[1][5];
			@fieldsum = ('<b>Total:</b>','',0,0,0);
			my %sum_columns_hash = (2 => '1',3 => '1',4 => '1');

			for my $datarows (1 .. $#$tbl_ref){


				my $thisrow = $$tbl_ref[$datarows];

				# paymentTotal is returned at index 4 but is actually the transaction total
				# This value is saved above as the paymentTotal so here we actually calculate
				# the total for each item.  We then double-check at the end to verify that the
				# totals match.
				$$thisrow[4]=($$thisrow[2] + $$thisrow[3]);
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				my $taxId=$$thisrow[1];
				$taxable_amount_found_hash{$taxId}=$$thisrow[3];
			}
			###
			print $q->h3("Tax info for this transaction:");
			my @format = ('', '',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},

			);
			$TaxableAmount=sprintf("%.2f",$fieldsum[3]);
			push(@$tbl_ref,\@fieldsum);

			printResult_arrayref2($tbl_ref, '', \@format);

			# Now, simply check the taxable amount calculated with the amount recorded in the tblTax

			foreach my $taxId (sort(keys(%taxable_amount_hash))) {
				my $calculated=sprintf("%.2f",abs($taxable_amount_hash{$taxId}));
				my $found=sprintf("%.2f",$taxable_amount_found_hash{$taxId});
				unless ($calculated eq $found) {
					my $diff=($calculated - $found);
					print $q->p("WARNING:  Total taxable for tax $taxId was caculated to be \$$calculated but shows as  \$$found.  This is a difference of \$$diff. ");
				} else {
					print $q->p("TaxId $taxId Check Passed");
				}
			}

		} else {
			if ($total_abs_taxable_price) {
				print $q->h3("WARNING: No Tax data found");
			} else {
				print $q->p("No Tax data found");
			}

		}

		###
		# Show the Discount info
		###
		my %discount_type_hash=(
		'0' => 'Undefined discount type',
		'1' => 'ITEM % OFF',
		'2' => 'ITEM $ OFF',
		'3' => 'PRICE MARKDOWN',
		'4' => 'TRANS % OFF',
		'5' => 'TRANS $ OFF',
		'6' => 'MFG $ COUPON',
		'7' => 'TRANS % OFF IN EMPLOYEE SALE',
		'8' => 'MIX MATCH ITEMS',
		'9' => 'MFG COUPON FOR MATCHED ITEM',
		'10' => 'SCAN MFG COUPON,NO LINK TO AN ITEM',
		'11' => 'ITEM % OFF IN EMPLOYEE SALE',
		'12' => 'Item % Surcharge',
		'13' => 'Item $ Surcharge',
		'14' => 'SURCHARGE MARKDOWN',
		'15' => 'Transaction % Surcharge',
		'16' => 'Transaction $ Surcharge',
		'17' => 'TRANS % AT TOTAL',
		'18' => 'TRANS $ AT TOTAL',
		'19' => 'TRANS % AT TENDER',
		'20' => 'TRANS $ AT TENDER',
		'25' => 'Item Best Price %',
		'26' => 'Item Best Price $',
		);
		my %discount_id_hash=(
		'1' => 'DAMAGED 10%',
		'2' => 'DAMAGED 15%',
		'3' => 'DAMAGED 20%',
		'4' => 'DAMAGED 25%',
		'5' => 'DAMAGED 30%',
		'6' => 'DAMAGED 40%',
		'7' => 'DAMAGED 50%',
		'8' => 'DAMAGED 75%',
		'9' => 'DISCOUNT 10%',
		'10' => 'DISCOUNT 15%',
		'11' => 'DISCOUNT 20%',
		'12' => 'DISCOUNT 25%',
		'13' => 'DISCOUNT 30%',
		'14' => 'DISCOUNT 40%',
		'15' => 'DISCOUNT 50%',
		'16' => 'DISCOUNT 75%',
		'17' => 'DAMAGED 10% TRANS',
		'18' => 'DAMAGED 15% TRANS',
		'19' => 'DAMAGED 20% TRANS',
		'20' => 'DAMAGED 25% TRANS',
		'21' => 'DAMAGED 30% TRANS',
		'22' => 'DAMAGED 40% TRANS',
		'23' => 'DAMAGED 50% TRANS',
		'24' => 'DAMAGED 75% TRANS',
		'25' => 'DISCOUNT 10% TRANS',
		'26' => 'DISCOUNT 15% TRANS',
		'27' => 'DISCOUNT 20% TRANS',
		'28' => 'DISCOUNT 25% TRANS',
		'29' => 'DISCOUNT 30% TRANS',
		'30' => 'DISCOUNT 40% TRANS',
		'31' => 'DISCOUNT 50% TRANS',
		'32' => 'DISCOUNT 75% TRANS',
		'33' => 'EMPLOYEE DISC 20%',
		'34' => 'BIRTHDAY CLUB 15%',
		'35' => 'INTRASTORE DISC 25%',
		'36' => '$5 OFF COUPON',
		'37' => 'MAN $ OFF ITEM COUPON',
		'38' => 'EMPLOYEE DISC 10%',
		'39' => 'EMPLOYEE DISC 15%',
		'40' => 'EMPLOYEE DISC 25%',
		'41' => '$10 OFF COUPON',
		'42' => '$15 OFF COUPON',
		'43' => '$20 OFF COUPON',
		'44' => 'COFFEE CLUB DISC',
		'45' => 'PRICE MARKDOWN',
		'46' => 'PERM MARKDOWN 10%',
		'47' => 'PERM MARKDOWN 15%',
		'48' => 'PERM MARKDOWN 20%',
		'49' => 'PERM MARKDOWN 25%',
		'50' => 'PERM MARKDOWN 30%',
		'51' => 'PERM MARKDOWN 40%',
		'52' => 'PERM MARKDOWN 50%',
		'53' => 'PERM MARKDOWN 75%',
		'54' => 'PERMMKDN 10% TRAN',
		'55' => 'PERMMKDN 15% TRAN',
		'56' => 'PERMMKDN 20% TRAN',
		'57' => 'PERMMKDN 25% TRAN',
		'58' => 'PERMMKDN 30% TRAN',
		'59' => 'PERMMKDN 40% TRAN',
		'60' => 'PERMMKDN 50% TRAN',
		'61' => 'PERMMKDN 75% TRAN',
		'62' => 'New Cust 10% Off Coupon',
		'63' => '$ OFF MAGAZINE COUP',
		'64' => '% OFF MAGAZINE COUP',
		'65' => '% OFF SINGLE ITEM',
		'66' => '$ OFF SINGLE ITEM',
		'67' => '$ OFF COUPON',
		'68' => 'First Light coupon',
		'69' => 'BOGO',
		'70' => 'Bag Sale',
		'100' => 'E-Book Coupon',
		'101' => 'Tuesday Halfoff',
		'102' => 'EMPLOYEE DISC 50%',
		'103' => 'DINING CARD',
		'104' => 'APPRECIATION',
		);
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  [Sequence]
			  ,[DiscountType]
			  ,[Reason]
			  ,[DiscountPercent]
			  ,[ExtDiscountAmt]
			  ,[ReferenceNum]
			  ,[OrigPrice]
			  ,[DiscountID]
			  ,[AutoPresetDiscount]
			  ,[AffectNetSales]
			  ,[ProfilePrompt]
			  ,[RefNumScanned]

		  FROM [****TLog].[dbo].[tblDiscounts]

			where
				TranId in ('$tranid_selection')
			order by
				[Sequence]

		ENDQUERY


		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			@fieldsum = ('<b>Total:</b>','','',0,0);
			my %sum_columns_hash = (4 => '1',6 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				# Substitute known values
				my $dtype=$$thisrow[1];
				$$thisrow[1]=$discount_type_hash{$dtype};
				my $did=$$thisrow[7];
				$$thisrow[7]=$discount_id_hash{$did};
				###
			}

			print $q->h3("Discount info for this transaction:");
			my @format = ('', '','','',
				{'align' => 'Right', 'currency' => ''},
				'',
				{'align' => 'Right', 'currency' => ''},

			);
			push(@$tbl_ref,\@fieldsum);
			printResult_arrayref2($tbl_ref, '', \@format);
		} else {
			print $q->p("No Discount data found");
		}
		###
		# Determine Non-Merchandise info
		###
		my %non_merch_hash=(

		'1' => 'GIFT CERTIFICATE',
		'2' => 'SHIPPING FEE',
		'3' => 'DONATIONS',
		'4' => 'PAID IN',
		'5' => 'PAID OUT',
		'6' => 'OTHER',
		'14' => 'GIFT CARD',
		'15' => 'BAG TAX',

		);
		$query=dequote(<< "		ENDQUERY");
			SELECT [TranId]
			  ,[Sequence]
			  ,[TranSecs]
			  ,[NonMerchType]
			  ,[SellAmount]
			  ,[OrigAmount]
			  ,[AddAmount]
			  ,[RefNum]
			  ,[DiscountFlag]
			  ,[Qty]
			  ,[EmployeeSaleFlag]
			  ,[ProfilePromptFlag]
			  ,[AffectNetSales]
			  ,[ReasonCode]
			  ,[VoucherId]
			  ,[Tax1]
			  ,[Tax2]
			  ,[Tax3]
			  ,[Tax4]
			  ,[Tax5]

			  ,[TaxModify1]
			  ,[TaxModify2]
			  ,[TaxModify3]
			  ,[TaxModify4]
			  ,[TaxModify5]


			FROM [****TLog].[dbo].[tblNonMerchandiseSale]
			where [TranId]='$tranid_selection'

		ENDQUERY

		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			@fieldsum = ('<b>Total:</b>','','',0,0);
			my %sum_columns_hash = (4 => '1',6 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				# Substitute known values
				my $dtype=$$thisrow[3];
				$$thisrow[3]=$non_merch_hash{$dtype};
				my $did=$$thisrow[7];
				#$$thisrow[7]=$discount_id_hash{$did};
				###
			}

			print $q->h3("Non-Merchandise info for this transaction:");
			my @format = ('', '','','',
				{'align' => 'Right', 'currency' => ''},
				'',
				{'align' => 'Right', 'currency' => ''},

			);
			push(@$tbl_ref,\@fieldsum);
			printResult_arrayref2($tbl_ref, '', \@format);
		} else {
			print $q->p("No Non-Merchandise data found");
		}
		###
###
		# Determine Profile Prompt Response info
		###
		my %prompt_id_hash = (
			'1' => 'LOCAL CHARGE INFO',
			'2' => 'DELIVERY PROMPTS',
			'3' => 'STORE NUMBER',
			'4' => 'STORE USE',
			'5' => 'CHECK INFO',
			'6' => 'PAID IN/OUT',
			'7' => 'RETURN INFO',
			'8' => 'PURCHZIPCODE',
			'9' => 'COMPETITIVE PRICE',
			'10' => 'CUST INFO',
			'11' => 'NM-OTHER',
			'12' => 'NM-DONAT-OTHER',
			'13' => 'GI - ALL',
			'14' => 'GI - ZIPONLY',
			'15' => 'GI - ALLBUTSTATE',
			'16' => 'NM-DONAT',
			'17' => 'RETURN INFO.',
			'18' => 'NMD-LIVINGGIFT',
			'19' => 'CASH COUNT',
			'20' => 'TAX EXEMPT',
			'21' => 'ITEM INFORM',
			'22' => 'CUST NAME',
			'23' => 'PAID OUTS',
			'25' => 'ASSOCIATE INFO',
			'26' => 'SKU DESC',
			'27' => 'NMD-VILLAGES',
			'28' => 'NMD-MCC',
			'71' => 'URL FraudWatch'

		);
		$query=dequote(<< "		ENDQUERY");
			SELECT
			  [Sequence]
			  ,[ProfileId]
			  ,[PromptGroup]
			  ,[TotPromptGrp]
			  ,[Response1]
			  ,[Response2]
			  ,[Response3]
			  ,[Response4]
			  ,[PromptPrefix1]
			  ,[PromptPrefix2]
			  ,[PromptPrefix3]
			  ,[PromptPrefix4]

			FROM [****TLog].[dbo].[tblProfileResponse]
			where [TranId]='$tranid_selection'

		ENDQUERY

		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {

			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];

				# Substitute known values
				my $pid=$$thisrow[1];
				$$thisrow[1]=$prompt_id_hash{$pid};
				###
			}

			print $q->h3("Profile Prompt info for this transaction:");

			printResult_arrayref2($tbl_ref);
		} else {
			print $q->p("No Profile Prompt data found");
		}
		###
		# Determine Payment info
		###
		my %tender_hash = (
		'1' => 'CASH',
		'2' => 'CHECK',
		'3' => 'TRAVELER CHECK',
		'4' => 'MASTERCARD',
		'5' => 'VISA',
		'6' => 'AMEX',
		'7' => 'DISCOVER',
		'8' => 'GIFT CERT REDEEM',
		'9' => 'MERCHANDISE CREDIT',
		'10' => 'OFFLINE TENDER',
		'11' => 'LOCAL CHARGE',
		'12' => 'MERCH CREDIT REDEEM',
		'13' => '$ COUPON OFF',
		'14' => 'GIFT CARD'
		);
		$query=dequote(<< "		ENDQUERY");
			SELECT
				[Sequence]
				,[TenderId] as [Tender]

				,([TenderAmount] - [ChangeDue]) as [Amount Due]
				,[TenderAmount] as [Amount Paid]
				,[ChangeDue]
				,[ExchangeAmount]

				,[ReissueFlag]
				,[RefNum]

				,[DeclineOverride]
				,[ProfilePrompt]
				,[CurrencyCode]


		  FROM [****TLog].[dbo].[tblPayment]
			where [TranId]='$tranid_selection'

		ENDQUERY

		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			@fieldsum = ('<b>Total:</b>','','',0,0);
			my %sum_columns_hash = (2 => '1',3 => '1', 4 => '1', 5 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				# Substitute known values
				my $dtype=$$thisrow[1];
				$$thisrow[1]=$tender_hash{$dtype};
				my $did=$$thisrow[7];
				$$thisrow[7]=$tender_hash{$did};
				###
			}

			print $q->h3("Payment info for this transaction:");
			my @format = ('', '',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
			);
			push(@$tbl_ref,\@fieldsum);
			printResult_arrayref2($tbl_ref, '', \@format);
		} else {
			print $q->p("No Payment data found");
		}
		###

	} else {
		print $q->p("No Transaction data found");
	}
###

	$dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();
} elsif ("$report" eq "SALES_TXN_DTL_orig") {
    my $tranid_selection = trim(($q->param("tranid_selection"))[0]);
	print $q->h2("Report for Transaction ID: $tranid_selection");
 	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
###
=pod
	# Get the total tax paid on this transaction
	my %totals_hash;
	$query=dequote(<< "	ENDQUERY");
		select
			tot.Tax1,
			tot.ManualTax1,
			tot.Tax2,
			tot.ManualTax2,
			tot.TotalSale,
			tm.PaymentTotal

		from
			****Tlog..tblTranMaster tm
			join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
		where
			tm.TranId in ('$tranid_selection')

	ENDQUERY

	my $sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$totals_hash{Tax1}=$tmp[0];
		$totals_hash{ManTax1}=$tmp[1];
		$totals_hash{Tax2}=$tmp[2];
		$totals_hash{ManTax2}=$tmp[3];
		$totals_hash{TotalSale}=$tmp[4];
		$totals_hash{TotalPayment}=$tmp[5];
	}
=cut
	# Get any non merch
	my $non_merch;

	# Get the non-taxable totals for this transaction
	my $non_tax_total;
	$query=dequote(<< "	ENDQUERY");
	SELECT
		  ms.[ExtNetPrice]
	  FROM [****TLog].[dbo].[tblMerchandiseSale] ms
		join ****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]
		where
			ms.[TranId] in ('$tranid_selection')
		and
			(ms.Tax1 = 0 and
			ms.Tax2 = 0 and
			ms.Tax3 = 0 and
			ms.Tax4 = 0 and
			ms.Tax5 = 0)
	ENDQUERY

	my $sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$non_tax_total+=$tmp[0];
	}
	# Get any non merch
	my $non_merch_total;
	$query=dequote(<< "	ENDQUERY");
	SELECT
		  [SellAmount]
	FROM
		[****TLog].[dbo].[tblNonMerchandiseSale] ms
	join
		****Tlog..tblTranMaster tm on ms.[TranId] = tm.[TranId]
	where
		ms.[TranId] in ('$tranid_selection')
	ENDQUERY

	$sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$non_merch_total+=$tmp[0];
	}
	# Get the tender total
	my $tender_total;
	$query=dequote(<< "	ENDQUERY");
	SELECT
		  [TenderAmount]
	FROM
		[****TLog].[dbo].[tblPayment]
	where
		[TranId] in ('$tranid_selection')
	ENDQUERY

	$sth=$dbh->prepare("$query");
	$sth->execute();

	while (my @tmp = $sth->fetchrow_array()) {
		$tender_total+=$tmp[0];
	}
	# Get the transaction details
	my $has_sales=0;
	my $has_returns=0;
	my $total_net_price=0;
	my $total_abs_net_price=0;
	my $extTaxableAmt=0;
	my $extNet=0;
	$query=dequote(<< "	ENDQUERY");
		select
			tm.TranId as TranId,
			case when tm.StoreId='2098' and tm.RegisterId = '02' and
				tm.TranNum between 4013 and 4023 and
				convert(Date, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
					then tm.TranNum - 4000
					else tm.TranNum end as TxnNum,

			convert(Date, convert(varchar(10),tm.TranDateTime,21)) as TranDate,
			ms.Sequence,
			tm.RegisterId as RegNum,
			ms.SKU,
			ms.Dept,
			convert(int,sign(ms.ExtOrigPrice) * ms.Quantity) as [Qty],
			ms.ExtSellPrice as [Net],

			case
				when ms.Tax1 = '1'
				or ms.Tax2 = '1'
				or ms.Tax3 = '1'
				or ms.Tax4 = '1'
				or ms.Tax5 = '1'
			then
				ms.[ExtNetPrice]
			end
				as [ExtTaxableAmt],
			ms.Tax1,
			ms.Tax2,
			ms.Tax3,
			ms.Tax4,
			ms.PromoPriceAdjustAmount,
			ms.PromoPercentAdjustAmount,
			ms.PriceOverride

		from
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
		where
			tm.TranId in ('$tranid_selection')
		order by TxnNum, tm.RegisterId,
					convert(Date, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
	ENDQUERY


	my $tbl_ref = execQuery_arrayref($dbh, $query);

	if (recordCount($tbl_ref)) {
		# Get the totals
		my @fieldsum = ('<b>Total:</b>','','','','','','',0,0,0,'','','','',0);
		my %sum_columns_hash = (7 => '1',8 => '1',9 => '1',14 => '1');
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = $$tbl_ref[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if ($sum_columns_hash{$column}) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                } else {
                    $fieldsum[$column] = '';
                }
            }
			if ($$thisrow[8] > 0) {
				$has_sales=1;
			}
			if ($$thisrow[8] < 0) {
				$has_returns=1;
			}


			$total_net_price+=$$thisrow[8];
			$total_abs_net_price+=abs($$thisrow[8]);
        }
		my @format = ('', '','','','','','','',
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			'','','','',
			{'align' => 'Right', 'currency' => ''},
			'',''

		);
		$extNet=$fieldsum[8];
		$extTaxableAmt=$fieldsum[9];
		push(@$tbl_ref,\@fieldsum);
		printResult_arrayref2($tbl_ref, '', \@format);
		# Show the tax info for this transaction
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  tt.[Sequence]
			  ,tt.[TaxId]
			  ,tt.[TaxAmount]
			  ,tt.[TaxableAmount]
			  ,tm.[paymentTotal]
			  ,tm.[isReturn]

		  FROM [****TLog].[dbo].[tblTax] tt
			join ****Tlog..tblTranMaster tm on tt.[TranId] = tm.[TranId]

			where
				tt.[TranId] in ('$tranid_selection')
			order by
				tt.[Sequence]

		ENDQUERY


		my $tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			my $paymentTotal=$$tbl_ref[1][4];
			my $isReturn=$$tbl_ref[1][5];
			@fieldsum = ('<b>Total:</b>','',0,0,0);
			my %sum_columns_hash = (2 => '1',3 => '1',4 => '1');
			for my $datarows (1 .. $#$tbl_ref){

				my $thisrow = $$tbl_ref[$datarows];

				# paymentTotal is returned at index 4 but is actually the transaction total
				# This value is saved above as the paymentTotal so here we actually calculate
				# the total for each item.  We then double-check at the end to verify that the
				# totals match.
				$$thisrow[4]=($$thisrow[2] + $$thisrow[3]);
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
			}
			###
			print $q->h3("Tax info for this transaction:");
			my @format = ('', '',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},

			);
			push(@$tbl_ref,\@fieldsum);

			printResult_arrayref2($tbl_ref, '', \@format);

			my $total_tax_amount=$fieldsum[2];
			$paymentTotal=sprintf("%.2f",$fieldsum[4]);
			#$paymentTotal=sprintf("%.2f",($fieldsum[4] + $non_tax_total));

			if ($extTaxableAmt < 0) {
				# The total tax amount is a bit tricky.  It only shows the absolute value. So, I am using the extTaxableAmount to determine
				# what the sign of the tax amount should be
				$total_tax_amount=(0 - $total_tax_amount);

			}

			if ($extNet < 0) {
				$paymentTotal=sprintf("%.2f",(0 - $paymentTotal));
			}

			# Total tax amount + total abs price - total non taxed should equal paymentTotal
			#my $calculatedPaymentTotal=sprintf("%.2f",($total_abs_net_price + $total_tax_amount - $non_tax_total ));

			#my $calculatedPaymentTotal=sprintf("%.2f",($total_net_price + $total_tax_amount - $non_tax_total ));
			my $calculatedPaymentTotal=sprintf("%.2f",($total_net_price + $total_tax_amount));
			#$paymentTotal=sprintf("%.2f",($paymentTotal + $non_tax_total));
			unless ($calculatedPaymentTotal eq $paymentTotal) {
				my $diff=($calculatedPaymentTotal - $paymentTotal);
				print $q->h3("WARNING: Total payment was caculated to be \$$calculatedPaymentTotal but shows as  \$$paymentTotal.  This is a difference of \$$diff.");
			}

		} else {
			print $q->p("No Tax data found");
		}

		###
		# Show the Discount info
		###
		my %discount_type_hash=(
		'0' => 'Undefined discount type',
		'1' => 'ITEM % OFF',
		'2' => 'ITEM $ OFF',
		'3' => 'PRICE MARKDOWN',
		'4' => 'TRANS % OFF',
		'5' => 'TRANS $ OFF',
		'6' => 'MFG $ COUPON',
		'7' => 'TRANS % OFF IN EMPLOYEE SALE',
		'8' => 'MIX MATCH ITEMS',
		'9' => 'MFG COUPON FOR MATCHED ITEM',
		'10' => 'SCAN MFG COUPON,NO LINK TO AN ITEM',
		'11' => 'ITEM % OFF IN EMPLOYEE SALE',
		'12' => 'Item % Surcharge',
		'13' => 'Item $ Surcharge',
		'14' => 'SURCHARGE MARKDOWN',
		'15' => 'Transaction % Surcharge',
		'16' => 'Transaction $ Surcharge',
		'17' => 'TRANS % AT TOTAL',
		'18' => 'TRANS $ AT TOTAL',
		'19' => 'TRANS % AT TENDER',
		'20' => 'TRANS $ AT TENDER',
		'25' => 'Item Best Price %',
		'26' => 'Item Best Price $',
		);
		my %discount_id_hash=(
		'1' => 'DAMAGED 10%',
		'2' => 'DAMAGED 15%',
		'3' => 'DAMAGED 20%',
		'4' => 'DAMAGED 25%',
		'5' => 'DAMAGED 30%',
		'6' => 'DAMAGED 40%',
		'7' => 'DAMAGED 50%',
		'8' => 'DAMAGED 75%',
		'9' => 'DISCOUNT 10%',
		'10' => 'DISCOUNT 15%',
		'11' => 'DISCOUNT 20%',
		'12' => 'DISCOUNT 25%',
		'13' => 'DISCOUNT 30%',
		'14' => 'DISCOUNT 40%',
		'15' => 'DISCOUNT 50%',
		'16' => 'DISCOUNT 75%',
		'17' => 'DAMAGED 10% TRANS',
		'18' => 'DAMAGED 15% TRANS',
		'19' => 'DAMAGED 20% TRANS',
		'20' => 'DAMAGED 25% TRANS',
		'21' => 'DAMAGED 30% TRANS',
		'22' => 'DAMAGED 40% TRANS',
		'23' => 'DAMAGED 50% TRANS',
		'24' => 'DAMAGED 75% TRANS',
		'25' => 'DISCOUNT 10% TRANS',
		'26' => 'DISCOUNT 15% TRANS',
		'27' => 'DISCOUNT 20% TRANS',
		'28' => 'DISCOUNT 25% TRANS',
		'29' => 'DISCOUNT 30% TRANS',
		'30' => 'DISCOUNT 40% TRANS',
		'31' => 'DISCOUNT 50% TRANS',
		'32' => 'DISCOUNT 75% TRANS',
		'33' => 'EMPLOYEE DISC 20%',
		'34' => 'BIRTHDAY CLUB 15%',
		'35' => 'INTRASTORE DISC 25%',
		'36' => '$5 OFF COUPON',
		'37' => 'MAN $ OFF ITEM COUPON',
		'38' => 'EMPLOYEE DISC 10%',
		'39' => 'EMPLOYEE DISC 15%',
		'40' => 'EMPLOYEE DISC 25%',
		'41' => '$10 OFF COUPON',
		'42' => '$15 OFF COUPON',
		'43' => '$20 OFF COUPON',
		'44' => 'COFFEE CLUB DISC',
		'45' => 'PRICE MARKDOWN',
		'46' => 'PERM MARKDOWN 10%',
		'47' => 'PERM MARKDOWN 15%',
		'48' => 'PERM MARKDOWN 20%',
		'49' => 'PERM MARKDOWN 25%',
		'50' => 'PERM MARKDOWN 30%',
		'51' => 'PERM MARKDOWN 40%',
		'52' => 'PERM MARKDOWN 50%',
		'53' => 'PERM MARKDOWN 75%',
		'54' => 'PERMMKDN 10% TRAN',
		'55' => 'PERMMKDN 15% TRAN',
		'56' => 'PERMMKDN 20% TRAN',
		'57' => 'PERMMKDN 25% TRAN',
		'58' => 'PERMMKDN 30% TRAN',
		'59' => 'PERMMKDN 40% TRAN',
		'60' => 'PERMMKDN 50% TRAN',
		'61' => 'PERMMKDN 75% TRAN',
		'62' => 'New Cust 10% Off Coupon',
		'63' => '$ OFF MAGAZINE COUP',
		'64' => '% OFF MAGAZINE COUP',
		'65' => '% OFF SINGLE ITEM',
		'66' => '$ OFF SINGLE ITEM',
		'67' => '$ OFF COUPON',
		'68' => 'First Light coupon',
		'69' => 'BOGO',
		'70' => 'Bag Sale',
		'100' => 'E-Book Coupon',
		'101' => 'Tuesday Halfoff',
		'102' => 'EMPLOYEE DISC 50%',
		'103' => 'DINING CARD',
		'104' => 'APPRECIATION',
		);
		$query=dequote(<< "		ENDQUERY");
		SELECT
			  [Sequence]
			  ,[DiscountType]
			  ,[Reason]
			  ,[DiscountPercent]
			  ,[ExtDiscountAmt]
			  ,[ReferenceNum]
			  ,[OrigPrice]
			  ,[DiscountID]
			  ,[AutoPresetDiscount]
			  ,[AffectNetSales]
			  ,[ProfilePrompt]
			  ,[RefNumScanned]

		  FROM [****TLog].[dbo].[tblDiscounts]

			where
				TranId in ('$tranid_selection')
			order by
				[Sequence]

		ENDQUERY


		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			@fieldsum = ('<b>Total:</b>','','',0,0);
			my %sum_columns_hash = (4 => '1',6 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				# Substitute known values
				my $dtype=$$thisrow[1];
				$$thisrow[1]=$discount_type_hash{$dtype};
				my $did=$$thisrow[7];
				$$thisrow[7]=$discount_id_hash{$did};
				###
			}

			print $q->h3("Discount info for this transaction:");
			my @format = ('', '','','',
				{'align' => 'Right', 'currency' => ''},
				'',
				{'align' => 'Right', 'currency' => ''},

			);
			push(@$tbl_ref,\@fieldsum);
			printResult_arrayref2($tbl_ref, '', \@format);
		} else {
			print $q->p("No Discount data found");
		}
		###
		# Determine Non-Merchandise info
		###
		$query=dequote(<< "		ENDQUERY");
			SELECT [TranId]
			  ,[Sequence]
			  ,[TranSecs]
			  ,[NonMerchType]
			  ,[SellAmount]
			  ,[OrigAmount]
			  ,[AddAmount]
			  ,[RefNum]
			  ,[DiscountFlag]
			  ,[Qty]
			  ,[EmployeeSaleFlag]
			  ,[ProfilePromptFlag]
			  ,[AffectNetSales]
			  ,[ReasonCode]
			  ,[Tax1]
			  ,[Tax2]
			  ,[Tax3]
			  ,[Tax4]
			  ,[Tax5]

			  ,[TaxModify1]
			  ,[TaxModify2]
			  ,[TaxModify3]
			  ,[TaxModify4]
			  ,[TaxModify5]

			FROM [****TLog].[dbo].[tblNonMerchandiseSale]
			where [TranId]='$tranid_selection'

		ENDQUERY

		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			@fieldsum = ('<b>Total:</b>','','',0,0);
			my %sum_columns_hash = (4 => '1',6 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				# Substitute known values
				my $dtype=$$thisrow[1];
				$$thisrow[1]=$discount_type_hash{$dtype};
				my $did=$$thisrow[7];
				$$thisrow[7]=$discount_id_hash{$did};
				###
			}

			print $q->h3("Non-Merchandise info for this transaction:");
			my @format = ('', '','','',
				{'align' => 'Right', 'currency' => ''},
				'',
				{'align' => 'Right', 'currency' => ''},

			);
			push(@$tbl_ref,\@fieldsum);
			printResult_arrayref2($tbl_ref, '', \@format);
		} else {
			print $q->p("No Non-Merchandise data found");
		}
		###
###
		# Determine Profile Prompt Response info
		###
		my %prompt_id_hash = (
			'1' => 'LOCAL CHARGE INFO',
			'2' => 'DELIVERY PROMPTS',
			'3' => 'STORE NUMBER',
			'4' => 'STORE USE',
			'5' => 'CHECK INFO',
			'6' => 'PAID IN/OUT',
			'7' => 'RETURN INFO',
			'8' => 'PURCHZIPCODE',
			'9' => 'COMPETITIVE PRICE',
			'10' => 'CUST INFO',
			'11' => 'NM-OTHER',
			'12' => 'NM-DONAT-OTHER',
			'13' => 'GI - ALL',
			'14' => 'GI - ZIPONLY',
			'15' => 'GI - ALLBUTSTATE',
			'16' => 'NM-DONAT',
			'17' => 'RETURN INFO.',
			'18' => 'NMD-LIVINGGIFT',
			'19' => 'CASH COUNT',
			'20' => 'TAX EXEMPT',
			'21' => 'ITEM INFORM',
			'22' => 'CUST NAME',
			'23' => 'PAID OUTS',
			'25' => 'ASSOCIATE INFO',
			'26' => 'SKU DESC',
			'27' => 'NMD-VILLAGES',
			'28' => 'NMD-MCC',
			'71' => 'URL FraudWatch'

		);
		$query=dequote(<< "		ENDQUERY");
			SELECT
			  [Sequence]
			  ,[ProfileId]
			  ,[PromptGroup]
			  ,[TotPromptGrp]
			  ,[Response1]
			  ,[Response2]
			  ,[Response3]
			  ,[Response4]
			  ,[PromptPrefix1]
			  ,[PromptPrefix2]
			  ,[PromptPrefix3]
			  ,[PromptPrefix4]

			FROM [****TLog].[dbo].[tblProfileResponse]
			where [TranId]='$tranid_selection'

		ENDQUERY

		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {

			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];

				# Substitute known values
				my $pid=$$thisrow[1];
				$$thisrow[1]=$prompt_id_hash{$pid};
				###
			}

			print $q->h3("Profile Prompt info for this transaction:");

			printResult_arrayref2($tbl_ref);
		} else {
			print $q->p("No Profile Prompt data found");
		}
		###
		# Determine Payment info
		###
		my %tender_hash = (
		'1' => 'CASH',
		'2' => 'CHECK',
		'3' => 'TRAVELER CHECK',
		'4' => 'MASTERCARD',
		'5' => 'VISA',
		'6' => 'AMEX',
		'7' => 'DISCOVER',
		'8' => 'GIFT CERT REDEEM',
		'9' => 'MERCHANDISE CREDIT',
		'10' => 'OFFLINE TENDER',
		'11' => 'LOCAL CHARGE',
		'12' => 'MERCH CREDIT REDEEM',
		'13' => '$ COUPON OFF',
		'14' => 'GIFT CARD'
		);
		$query=dequote(<< "		ENDQUERY");
			SELECT
				[Sequence]
				,[TenderId] as [Tender]

				,([TenderAmount] - [ChangeDue]) as [Amount Due]
				,[TenderAmount] as [Amount Paid]
				,[ChangeDue]
				,[ExchangeAmount]

				,[ReissueFlag]
				,[RefNum]

				,[DeclineOverride]
				,[ProfilePrompt]
				,[CurrencyCode]


		  FROM [****TLog].[dbo].[tblPayment]
			where [TranId]='$tranid_selection'

		ENDQUERY

		$tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			###
			@fieldsum = ('<b>Total:</b>','','',0,0);
			my %sum_columns_hash = (2 => '1',3 => '1', 4 => '1', 5 => '1');
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				foreach my $column (1 ..$#$thisrow) {
					if ($sum_columns_hash{$column}) {
						$fieldsum[$column] += trim($$thisrow[$column]);
					} else {
						$fieldsum[$column] = '';
					}
				}
				# Substitute known values
				my $dtype=$$thisrow[1];
				$$thisrow[1]=$tender_hash{$dtype};
				my $did=$$thisrow[7];
				$$thisrow[7]=$tender_hash{$did};
				###
			}

			print $q->h3("Payment info for this transaction:");
			my @format = ('', '',
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
				{'align' => 'Right', 'currency' => ''},
			);
			push(@$tbl_ref,\@fieldsum);
			printResult_arrayref2($tbl_ref, '', \@format);
		} else {
			print $q->p("No Payment data found");
		}
		###

	} else {
		print $q->p("No Transaction data found");
	}
###

	$dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=SALES_AND_DISCOUNTS", -target=>'_top'}, "Net Sales Menu");
    standardfooter();
} elsif ("$report" eq "ITEMS2WIRE") {
    print $q->h2("Items to Wire Report - Part 1");
    my $CATALOG_SYNC_DB_SERVER='192.168.21.38:1433';
    my $CATALOG_SYNC_DB_USER='devseed';
    my $CATALOG_SYNC_DB_PASSWORD='erician';
    my $CATALOG_SYNC_DB_NAME='****';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC="Villages\$wspGetItemData4Wire";

    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_1='@obsonlist';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_2='@last_timestamp';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_3='@last_sku';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_4='@throttle';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_5='@get_timestamp_only';


 	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    if ($dbh) {
        print $q->p("Building query...");
    } else {
        print $q->p("Error.  Cannot connect to db on host ****");
        standardfooter();
        exit;
    }
    $query=dequote(<< "    ENDQUERY");
        exec
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_1 = '',
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_2 = 0,
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_3 = 0,
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_4 = 20000,
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_5 = 0

    ENDQUERY

    $query=dequote(<< "    ENDQUERY");
        select
            I.[Item No_],
            sum(I.[Quantity]) as Qty,
            max(case when I.[Entry Type] <> 1 then '1753-01-01' else [Posting Date] end) as [LastSale]  -- Most recent 'Sale'
        FROM
            [Villages\$Item Ledger Entry] I
        where
            I.[Store Inventory] = 0 and
            I.[Location Code] = 'B' and
            I.[Global Dimension 1 Code] in (select [AreaCode] from [dbo].[Villages\$GBPGAreaCodes]('COMPANY'))
        group by
            I.[Item No_]
        order by
            I.[Item No_]
    ENDQUERY


    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @format = ('', {'align' => 'Right', 'num' => '123'});
        printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;

    standardfooter();
} elsif ("$report" eq "ITEMSNOT2WIRE") {
    print $q->h2("Items not going to Wire Report");
    my $CATALOG_SYNC_DB_SERVER='192.168.21.38:1433';
    my $CATALOG_SYNC_DB_USER='devseed';
    my $CATALOG_SYNC_DB_PASSWORD='erician';
    my $CATALOG_SYNC_DB_NAME='****';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC="Villages\$wspGetItemData4Wire";

    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_1='@obsonlist';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_2='@last_timestamp';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_3='@last_sku';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_4='@throttle';
    my $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_5='@get_timestamp_only';


 	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    if ($dbh) {
        print $q->p("Building query...");
    } else {
        print $q->p("Error.  Cannot connect to db on host ****");
        standardfooter();
        exit;
    }
    $query=dequote(<< "    ENDQUERY");
        exec
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_1 = '',
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_2 = 0,
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_3 = 0,
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_4 = 20000,
            $CATALOG_SYNC_DB_DEFAULT_STOREDPROC_PARAM_5 = 0

    ENDQUERY
	my $jan1_lastyear = sprintf("%04d-%02d-%02d", ($ltm[5]+1900 - 1), 1, 1,);
    $query=dequote(<< "    ENDQUERY");
        select
            distinct(I.[Item No_]),
			vi.[Inventory Posting Group],
			vi.[Obsoleted Date],
			vi.[Item Department],
			vi.[Web Catalog Date],
			vi.[Available for Sale Date],
			vi.[Vlgs-Contract],
			vi.Partner,
			vi.[Ecomm Only]


        FROM
            [Villages\$Item Ledger Entry] I
			join [****].[dbo].[Villages\$Item] as vi on I.[Item No_] = vi.[No_]
        where
            I.[Store Inventory] = 0 and
            I.[Location Code] = 'B' and
            I.[Global Dimension 1 Code] in (select [AreaCode] from [dbo].[Villages\$GBPGAreaCodes]('COMPANY'))
			and
				(
					(vi.[Obsoleted Date]= '1753-01-01')
					or
					(vi.[Obsoleted Date]> '$jan1_lastyear')

				)
			and I.[Item No_] not in
			(
				select
				  i.[No_] as item
				FROM
				  [****].[dbo].[Villages\$Item] as i
				WHERE
				  i.[Inventory Posting Group] in ('PRODUCTS', 'SUPPLIES')

				and
					(
						(i.[Item Department] between '10' and '69'
					and
						i.[Web Catalog Date] BETWEEN '01-01-1946'
					and
						DATEADD(day, 1, GETUTCDATE())
					and
						i.[Available for Sale Date]>'01-01-1753'
						)
						OR
						(
							i.[Item Department] between '70' and '79'
							and
							(
								i.[Vlgs-Contract]=1
							or
								i.Partner=1)
							and
								i.[Web Catalog Date] BETWEEN '01-01-1946'
							and
								DATEADD(day, 1, GETUTCDATE())
						)
						OR
						(
							i.[Ecomm Only] = 1
							and
							i.No_ in (Select [Parent Item No_] from dbo.[Villages\$BOM Component])
						)
					)
			)

        order by
            I.[Item No_]
    ENDQUERY


    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        my @format = ('', {'align' => 'Right', 'num' => '123'});
        printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;

    standardfooter();
} elsif (("$report" eq "SKU2WIRE") || ("$report" eq "SKU2WIREB")) {
    # Query for the SKU
    print $q->h2("SKU to The Wire Query");
	my $estimation=0;
	if ("$report" eq "SKU2WIREB") {
		print $q->p("Enter a SKU below to see if it likely to be on The Wire.");
		$estimation=1;
	} else {
		print $q->p("Enter a SKU below to see what info is being sent to The Wire for it.");
	}

    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKU2WIRERPT"});
	print $q->input({-type=>"hidden", -name=>"estimation", -value=>"$estimation"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));

    # SKU Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"SKU_selection", -size=>"10", -value=>""}))
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "SKU2WIRERPT") {
    my $SKU_selection   = trim(($q->param("SKU_selection"))[0]);
	my $estimation   = trim(($q->param("estimation"))[0]);
    print $q->h2("Items to Wire Report - SKU - $SKU_selection");
    # First we need to determine what the SKU is prior to this SKU
 	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    if ($dbh) {
        print $q->p("Building query...");
    } else {
        print $q->p("Error.  Cannot connect to db on host ****");
        standardfooter();
        exit;
    }
	if ($estimation) {
		# Just do the brief report here trying to determine whether the SKU is likely to be on the Wire or not.
		###
		my $jan1_lastyear = sprintf("%04d-%02d-%02d", ($ltm[5]+1900 - 1), 1, 1,);
		$query=dequote(<< "		ENDQUERY");
			select
				distinct(I.[Item No_]),
				vi.[Inventory Posting Group],
				vi.[Obsoleted Date],
				vi.[Item Department],
				vi.[Web Catalog Date],
				vi.[Available for Sale Date],
				vi.[Vlgs-Contract],
				vi.Partner,
				vi.[Ecomm Only]
			FROM
				[Villages\$Item Ledger Entry] I
				join [****].[dbo].[Villages\$Item] as vi on I.[Item No_] = vi.[No_]
			where
				I.[Item No_] like '$SKU_selection%'
		ENDQUERY

		my $tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {
			my $sku;
			my $obsolete_date;
			my $web_date;
			my $contract;
			my $partner;
			my $ecomm;
			my $dept;
			my $avail_date;
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = $$tbl_ref[$datarows];
				$sku=$$thisrow[0];
				$obsolete_date=$$thisrow[2];
				$dept=$$thisrow[3];
				$web_date=$$thisrow[4];
				$avail_date=$$thisrow[5];
				$contract=$$thisrow[6];
				$partner=$$thisrow[7];
				$ecomm=$$thisrow[8];
				
				if (($dept > 69) && ($dept <= 80)) {
					unless (($contract) || ($partner)) {
						print "$$sku is not on The Wire because: This item is a supply but neither partner or contract are set.<br />";
					} else {
						if ($web_date eq '1753-01-01 00:00:00.000') {
							print "$sku is not on The Wire because: There is no web date specified.<br />";
						} else {
							print  "$sku is probably on The Wire.<br />";
						}
					}
				} elsif (($dept > 9) && ($dept < 70)) {
					if ($web_date eq '1753-01-01 00:00:00.000') {
						print "$sku is not on The Wire because: There is no web date specified.<br />";
					} else {
						print  "$sku is probably on The Wire.<br />";
					}
				} else {
					print  "$sku is probably on The Wire.<br />";
				}
			}
			###

			printResult_arrayref2($tbl_ref);


		} else {
			print $q->p("No data found for $SKU_selection");
		}
		###
	} else {
		my $starting_sku=0;

		$query=dequote(<< "		ENDQUERY");
			SELECT
				top 1 [No_]
			FROM
				[****].[dbo].[Villages\$Item]
			WHERE
				[No_] < '$SKU_selection'
			ORDER BY
				[No_] desc
		ENDQUERY

		my $sth=$dbh->prepare("$query");
		$sth->execute();
		my $count=0;
		while (my @tmp = $sth->fetchrow_array()) {
			$starting_sku=$tmp[0];
		}
		if ($starting_sku) {
			$query=dequote(<< "			ENDQUERY");
				exec
					Villages\$wspGetItemData4Wire  \@obsonweblist='',\@last_timestamp=0, \@last_sku=$starting_sku, \@throttle=1,\@get_timestamp_only=0

			ENDQUERY

			# Need to set this or the query will fail
			$dbh->{LongTruncOk}=1;
			my $tbl_ref = execQuery_arrayref($dbh, $query);
			my $rc=recordCount($tbl_ref);

			if (recordCount($tbl_ref)) {
				print $q->p("Info being sent to The Wire via the Catalog_Sync");
				printResult_arrayref2($tbl_ref, '' );
			} else {
				# If this SKU did now show any data, let's see if we can figure out why

				###
				my $jan1_lastyear = sprintf("%04d-%02d-%02d", ($ltm[5]+1900 - 1), 1, 1,);
				$query=dequote(<< "				ENDQUERY");
					select
						distinct(I.[Item No_]),
						vi.[Inventory Posting Group],
						vi.[Obsoleted Date],
						vi.[Item Department],
						vi.[Web Catalog Date],
						vi.[Available for Sale Date],
						vi.[Vlgs-Contract],
						vi.Partner,
						vi.[Ecomm Only]
					FROM
						[Villages\$Item Ledger Entry] I
						join [****].[dbo].[Villages\$Item] as vi on I.[Item No_] = vi.[No_]
					where
						I.[Item No_] = '$SKU_selection'
				ENDQUERY

				my $tbl_ref = execQuery_arrayref($dbh, $query);
				if (recordCount($tbl_ref)) {
					my $obsolete_date;
					my $web_date;
					my $contract;
					my $partner;
					my $ecomm;
					my $dept;
					my $avail_date;
					for my $datarows (1 .. $#$tbl_ref){
						my $thisrow = $$tbl_ref[$datarows];
						$obsolete_date=$$thisrow[2];
						$dept=$$thisrow[3];
						$web_date=$$thisrow[4];
						$avail_date=$$thisrow[5];
						$contract=$$thisrow[6];
						$partner=$$thisrow[7];
						$ecomm=$$thisrow[8];
					}
					###
					print $q->p("$SKU_selection is not on The Wire because of one of the following fields:");
					printResult_arrayref2($tbl_ref);

					if (($dept > 69) && ($dept <= 80)) {
						unless (($contract) || ($partner)) {
							print $q->h3("This item is a supply but neither partner or contract are set.");
						}
					} elsif (($dept > 9) && ($dept < 70)) {
						if ($web_date eq '1753-01-01 00:00:00.000') {
							print $q->h3("There is no web date specified.");
						}
					}
				} else {
					print $q->p("No data found for $SKU_selection");
				}
				###

			}
		} else {
			print $q->p("Error: Cannot find the starting SKU for $SKU_selection");
		}
	}
    $dbh->disconnect;

    standardfooter();
} elsif ("$report" eq "ITEMS2WIREQRY") {
    # Query for the SKU
    print $q->h2("SKUs to The Wire Query");

    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"ITEMS2WIRERPT"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));


    # Company Available Option
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Show Items Company-Available:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"company_selection",   -value=>"1", -checked=>"1"}))
    );
    # Contract Available Option
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Show Items Contract-Available:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"contract_selection",   -value=>"1", -checked=>"1"}))
    );
    # Partner Available Option
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Show Items Partner-Available:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"partner_selection",   -value=>"1", -checked=>"1"}))
    );
    # Festival Available Option
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Show Items Festival-Available:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"festival_selection",   -value=>"1", -checked=>"1"}))
    );
	# Products
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Item Type Products:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"product_selection",   -value=>"1", -checked=>"1"}))
    );
	# Supplies
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Item Type Supplies:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"supply_selection",   -value=>"1", -checked=>"1"}))
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "ITEMS2WIRERPT") {
    print $q->h2("Items to Wire Report - Final");
    my $company_selection   = trim(($q->param("company_selection"))[0]);
    my $contract_selection   = trim(($q->param("contract_selection"))[0]);
    my $partner_selection   = trim(($q->param("partner_selection"))[0]);
    my $festival_selection   = trim(($q->param("festival_selection"))[0]);
    my $product_selection   = trim(($q->param("product_selection"))[0]);
	my $supply_selection   = trim(($q->param("supply_selection"))[0]);
    unless (($company_selection) || ($contract_selection) || ($partner_selection) || ($festival_selection) || ($product_selection) || ($supply_selection)) {
        print $q->p("You must select at least one of the criteria options.");
        standardfooter();
        exit;
    }
    unless (($product_selection) || ($supply_selection)) {
        print $q->p("You must select at least one of the item type options.");
        standardfooter();
        exit;
    }
    print $q->p("Items displayed are available to at least one of the following:");
    if ($company_selection) {
        print "-->Company stores<br>";
    }
    if ($contract_selection) {
        print "-->Contract stores<br>";
    }
    if ($partner_selection) {
        print "-->Partner stores<br>";
    }
    if ($festival_selection) {
        print "-->Festival stores<br>";
    }
	print $q->p("Items types selected:");
    if ($product_selection) {
        print "-->Products<br>";
    }
    if ($supply_selection) {
        print "-->Supplies<br>";
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****;uid=rpt;pwd=rpt;";
	my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

    if ($dbh) {
        print $q->p("Building query...");
    } else {
        print $q->p("Error.  Cannot connect to db on host ****");
        standardfooter();
        exit;
    }


    $query=dequote(<< "    ENDQUERY");
        exec
            Villages\$wspGetItemData4Wire  \@obsonweblist='',\@last_timestamp=0, \@last_sku=0, \@throttle=10000

    ENDQUERY

	# Need to set this or the query will fail
	$dbh->{LongTruncOk}=1;
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    my $tbl_ref2=[];
    if (recordCount($tbl_ref)) {
        push @$tbl_ref2, @$tbl_ref[0];  # Get the column labels
		my $include_this;
		my $include_this_item;
        for my $datarows (1 .. $#$tbl_ref){
			$include_this=0;
			$include_this_item=0;
            my $thisrow = @$tbl_ref[$datarows];
            if ($company_selection) {
                if ($$thisrow[40]) {
					$include_this=1;
				}
            }
            if ($contract_selection) {
				if ($$thisrow[41]) {
					$include_this=1;
				}
            }
            if ($partner_selection) {
				if ($$thisrow[42]) {
					$include_this=1;
				}
            }
            if ($festival_selection) {
				if ($$thisrow[43]) {
					$include_this=1;
				}
            }
            if ($product_selection) {
                if ($$thisrow[0] eq "Product") {
					$include_this_item=1;
				}
            }
            if ($supply_selection) {
                if ($$thisrow[0] eq "Supply") {
					$include_this_item=1;
				}
            }
			if (($include_this) && ($include_this_item)) {
				push @$tbl_ref2, $thisrow;
			}
        }
 
        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},
		'',
		'',
		'',
		'',
		{'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''}, 
        '',
        {'align' => 'Right', 'num' => ''},
		'','','','','','','','','',
		{'align' => 'Right', 'num' => ''},
		{'align' => 'Right', 'num' => ''},
		{'align' => 'Right', 'num' => ''},
		'','','','','',
		{'align' => 'Right', 'currency' => ''}, 
		{'align' => 'Right', 'currency' => ''}, 
		'','','','','','','','','','','','','','',
		{'align' => 'Right', 'num' => ''},
        );				 
        printResult_arrayref2($tbl_ref2, '', \@format);
    } else {
        print $q->p("No data found");
    }



    $dbh->disconnect;

    standardfooter();
} elsif ("$report" eq "POQRY") {
    print $q->h2("Purchase Order Report Query");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PORPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
    # Start Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"start_date_selection", -size=>"12", -value=>""}))
    );
    # End Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date(yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"end_date_selection", -size=>"12", -value=>""}))
    );
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"storenum_selection", -size=>"12", -value=>""}))
    );
    # PO Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "PO Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"ponum_selection", -size=>"12", -value=>""}))
    );
    # Order Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Order Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"order_selection", -size=>"12", -value=>""}))
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "PORPT") {
    print $q->h2("Purchase Order Report");
	my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
	my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);
	my $storenum_selection   = trim(($q->param("storenum_selection"))[0]);
    my $ponum_selection   = trim(($q->param("ponum_selection"))[0]);
    my $order_selection   = trim(($q->param("order_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);

    my $where = "1=1";
    if ($start_date_selection) {
        $where.=" and obh.[Order Date] >= '$start_date_selection' ";
    }
    if ($end_date_selection) {
        $where.=" and obh.[Order Date] <= '$end_date_selection' ";
    }
    if ($order_selection) {
        $where.=" and obh.[Order No_] like '$order_selection%' ";
    }
    if ($ponum_selection) {
        $where.=" and obh.[External Doc_ No_] like '$ponum_selection%' ";
    }
    if ($storenum_selection) {
        $where.=" and obh.[Customer No_] = '$storenum_selection' ";
    }
    unless (($storenum_selection)||($order_selection)||($ponum_selection)) {
        print $q->p("Please specify at least the store number, order number, or po number");
        standardfooter();
        exit;
    }
	my %ordstat = ( -2 => "No status from Akron yet",
					-1 => "Unknown to Akron",
					0 => "Not Ready for WMS",
					1 => "Ready for WMS",
					2 => "Sent to WMS",
					3 => "Picked Ready to Ship",
					4 => "Shipped Ready to Invoice",
					5 => "Picked ON HOLD",
					6 => "Shipped ON HOLD",
					7 => "Shipped & Invoiced",
					'' => ''
				);	
    $query=dequote(<< "    ENDQUERY");
        select
            obh.[Entry No_],
            obh.[Order No_],
            obh.[Customer No_],
            obh.[External Doc_ No_],
            obh.[Status],
			sh.[WMS Status] as 'Sales Status',
			ssh.[WMS Status] as 'Shipment Status',
            obh.[Order Date],
            obh.[Date Time Entered],
            obh.[Date Time Processed],
            obh.[Comments],
            obh.[Requested Ship Date]
        from
            [Villages\$Order Buffer Header] obh
			left outer join [Villages\$Sales Header] sh on obh.[Order No_] = sh.[No_]
			left outer join [Villages\$Sales Shipment Header] ssh on obh.[Order No_] = ssh.[No_]
        where
            $where
        order by
            obh.[Order Date] desc
    ENDQUERY
    #print " query ($query)<br>";

    #my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

    my $tbl_ref = execQuery_arrayref($dbh, $query);
 
    if (recordCount($tbl_ref)) {
        for my $datarows (1 .. $#$tbl_ref){ 
			my $thisrow = @$tbl_ref[$datarows];
			my $order = $$thisrow[1];
			my $status = $$thisrow[4];
			$$thisrow[4]=$ordstat{$$thisrow[4]};
			$$thisrow[5]=$ordstat{$$thisrow[5]};
			$$thisrow[6]=$ordstat{$$thisrow[6]};			
		}
	
        my @links = ( '',"$scriptname?report=WIREORDRRPT2&report_selection=$report_selection&order_selection=".'$_' );
        my @format = ('', {'align' => 'Right', 'num' => '123'});
        printResult_arrayref2($tbl_ref, \@links, \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this report");
    standardfooter();
} elsif ("$report" eq "NOBSSKURPT") {
    print $q->h2("Navision Obsolete SKU Report");
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');
    my $query = "
    SELECT
        [No_]
        ,[Description]
        ,[New Item Date]
        ,[No Longer Purch Date]
        ,[Discontinued Date]
        ,[Obsoleted Date]
        ,[Length]
        ,[Width]
        ,[Height]
    FROM
        [****].[dbo].[Villages\$Item]
    where
        [Obsoleted Date] <> '1753-01-01'
    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    printResult_arrayref2($tbl_ref);
    $dbh->disconnect;
    standardfooter();

} elsif ("$report" eq "NACTSKURPT") {
    print $q->h2("Navision Active SKU Report");
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');
    #my $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
    #my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $query = "exec Abacus.****.[dbo].[Villages\$nspGetAkronSkuInfo] \@Active=1";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    printResult_arrayref2($tbl_ref);
    standardfooter();
 } elsif ("$report" eq "GETAKRONSKUQRY") {
    print $q->h2("Navision SKU Query");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";

    print $q->input({-type=>"hidden", -name=>"report", -value=>"GETAKRONSKURPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("SKU Selection")));
    # SKU
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"sku_selection", -size=>"12", -value=>""}))
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    standardfooter();

 } elsif ("$report" eq "GETAKRONSKURPT") {
	my $sku_selection   = trim(($q->param("sku_selection"))[0]);
    my $wqty;
    print $q->h2("Navision SKU Report: nspGetAkronSkuInfo");
    print "<br>These are the stages that the nspGetAkronSkuInfo.sql stored procedure goes through.<br>";
    print "This stored procedure is used to create the tblAkronSkuInfo in the ****Data database.<br><br>";
    $query = dequote(<< "    ENDQUERY");
        select ii.[No_]
        from [Villages\$Item] ii
        where
            ii.[Item Type] = 0 and
            ii.[No_] = '$sku_selection'
    ENDQUERY
    my $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    print "Stage 1: - Basic info in Villages\$Item table<br>";
    if (recordCount($tbl_ref)) {

        printResult_arrayref2($tbl_ref);
    } else {
        print $q->p("No data found");
    }

    $query = dequote(<< "    ENDQUERY");
       select
        I.[Item No_],
        sum(isnull(I.[Remaining Quantity],0) ) as "Remaining Qty"
       FROM
          [Villages\$Item Ledger Entry] I
       where
          I.[Item No_] = '$sku_selection' and
          I.[Store Inventory] = 0 and
          I.[Open] = 1 and
          I.[Location Code] = 'B' and
          I.[Global Dimension 1 Code] in (select [AreaCode] from [dbo].[Villages\$GBPGAreaCodes]('COMPANY'))
        group by I.[Item No_]
    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    print "Stage 2: Find Remaining Quantity<br>";
    if (recordCount($tbl_ref)) {
        $wqty=$$tbl_ref[1][1];
		my @format = (
		'',
		{'align' => 'Right', 'num' => ''},
		);
        printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->p("No data found");
    }

    $query = dequote(<< "    ENDQUERY");
       select
          R.[Item No_],
          sum(isnull(R.[Quantity (Base)],0) ) as RQty
       FROM
          [Villages\$Reservation Entry] R
       where
          R.[Item No_] = '$sku_selection' and
          R.[Location Code] = 'B' and
          R.[Global Dimension 1 Code] in (select [AreaCode] from [dbo].[Villages\$GBPGAreaCodes]('COMPANY')) and
          R.[Reservation Status] = 0 and  -- options: [0,1,2,3] = [Reservation,Tracking,Surplus,Prospect]
          R.[Source Type] = 37 and        -- table 37: Sales Line
          R.[Source Subtype] = 1          -- Document Type: Order (OptionString: Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order)
       group by R.[Item No_]
    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    print "Stage 3: Reservation Qty<br>";
    if (recordCount($tbl_ref)) {
        $wqty+=$$tbl_ref[1][1];

        printResult_arrayref2($tbl_ref);
    } else {
        print $q->p("No data found");
    }

    $query = dequote(<< "    ENDQUERY");
       select
          ii.[No_] as SKU,
          case when ii.[GiftCardID] > ''
             then substring(case when ii.[PLU Description] > '' then ii.[PLU Description] else ii.[Description] end, 1, 32) + ' GE'+ substring(ii.[GiftCardID], 3, 5)
             else case when ii.[PLU Description] > '' then ii.[PLU Description] else ii.[Description] end end
          as POSDesc,
          ii.[BarCode for Printing] as UPCCode,
          convert(decimal(38,2), isnull('$wqty',0)) as QOH_W,
          convert(numeric(8,2), ii.[Unit Price]) Price,
          ii.[Item Disc_ Group] as DiscGroup,
          convert(numeric(8,3), (select [Line Discount %] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'REG')) as DiscREG,
          convert(numeric(8,3), (select [Line Discount %] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'XTR')) as DiscXTR,
          convert(numeric(8,3), (select [Line Discount %] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'OOC')) as DiscOOC,
          convert(numeric(8,2), ii.[Unit Cost]) as AvgCost,
          ii.[Seasonality Code] as Seasonality,
          ii.[Visual Merchandising Code] as VMCode,
          ii.[Item Department] + ii.[Class] as Class,
          ii.[New Item Date] as NewItemDate,
          case when ii.[Discontinued Date] > '1753-01-01' and ii.[Discontinued Date] <= getdate() then 1 else 0 end as DiscontFlag,
          case when ii.[Obsoleted Date] > '1753-01-01' and ii.[Obsoleted Date] <= getdate() then 1 else 0 end as ObsoleteFlag,
          ii.[Web Rank] as WebRank,
          ii.[Vendor No_] as VendorNo,
          v.[Short Name] as VendorName,
          ii.[Vlgs-Contract], ii.Partner, ii.Festival, ii.[Vlgs-Company],
          pc.[Description] as ProductCare,
          convert(numeric(8,3), (select [Retail Price Discount] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'XTR')) as RetailDisc,
          case when [Inventory Posting Group] not in ('PRODUCTS', 'SUPPLIES') or
                    (ii.[Item Department] between '70' and '79') then 0 else 1 end as resell,
          [Inventory Posting Group] as InvPostGrp,
          convert(numeric(8,3), (select top 1 [Unit Price] from [Villages\$Sales Price]
                                 where [Sales Code] = 'FIXED' and [Item No_] = ii.[No_] and
                                 ([Starting Date] = '1753-01-01' or [Starting Date] < getdate()) and
                                 ([Ending Date] = '1753-01-01' or [Ending Date] >= getdate())
                                )) as FixedPrice
       from
          [Villages\$Item] ii
          left outer join [Villages\$Vendor] v on ii.[Vendor No_] = v.[No_]
          left outer join [Villages\$TTV System Rules] pc on ii.[Product Care Code] = pc.Code and pc.[Ruleid] = 'PRODCARE'
       where
          ii.[No_] = '$sku_selection' and
          ii.[Item Type] = 0 and

             ( (ii.[Obsoleted Date] = '1753-01-01') or
              (ii.[Obsoleted Date] > dateadd(year, -1, getdate()) and '$wqty' = 0)
             )
       order by
          ii.[No_]
    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);
    print "Stage 4: Info on the SKU<br>";
    if (recordCount($tbl_ref)) {

        printResult_arrayref2($tbl_ref);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;

    standardfooter();
 } elsif ("$report" eq "NVACTSKURPT") {
    print $q->h2("Navision Non-Villages SKU Report");


    my $DBName = '****';
    my $NavCompany = 'Villages';

    $query = "exec Abacus.[". $DBName ."].[dbo].[". $NavCompany .'$'."tspGetVendorStandardSKU]";

    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print "Information on all Non-Villages SKUs<br>";
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;

    standardfooter();
 } elsif ("$report" eq "GETVENDORSKUQRY") {
    print $q->h2("Navision Non-Villages SKU Query");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";

    print $q->input({-type=>"hidden", -name=>"report", -value=>"GETVENDORSKURPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("SKU Selection")));
    # SKU
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"sku_selection", -size=>"12", -value=>""}))
    );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    standardfooter();
 } elsif ("$report" eq "GETVENDORSKURPT") {
    print $q->h2("Navision Non-Villages SKU Report");
	my $sku_selection   = trim(($q->param("sku_selection"))[0]);
    my $DBName = '****';
    my $NavCompany = 'Villages';

    $query = "exec Abacus.[". $DBName ."].[dbo].[". $NavCompany .'$'."tspGetVendorStandardSKU] \@ItemNo='$sku_selection'";

    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print "Information on SKU $sku_selection<br>";
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "STORESKUQRY") {
	print $q->h2("Store SKU Report Query");

    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $store_options= $q->option({-value=>'All'}, "All Stores");
    $store_options.= $q->option({-value=>'Company'}, "Company Stores");
    $store_options.= $q->option({-value=>'Contract'}, "Contract Stores");
    $query = "
    SELECT [StoreId]
      ,[StoreName]

    FROM [****Data].[dbo].[rtblStores]
    where EndDate is  NULL
	and [StoreId] not like '99%'
	";

    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias) {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $number = trim($$thisrow[0]);
        my $name = trim($$thisrow[1]);
        $store_options .= $q->option({-value=>$number}, "$number - $name");
    }
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";

    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKURPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
    # SKU
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU:"),
        $q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'text', -name=>"sku_selection", -size=>"12", -value=>""}))
    );
	# Store Options
    print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->Select({-name=>"store_selection", -size=>"1"}), $store_options));
	# Check history
	print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "History:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'checkbox', -name=>"history_selection",   -value=>"1"}))
        );
	# With QOH
	print $q->Tr(
		$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "With QOH:"),
		$q->td({-class=>'tabledatanb', -align=>'left'},
		$q->input({ -type=>'checkbox', -name=>"QOH_selection",   -value=>"1"}))
        );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
	print $q->p("The query will find all SKUs that start like the specified SKU.");
	print $q->p("Checking the 'History' checkbox will query history rather than current inventory.");
	standardfooter();
} elsif (("$report" eq "SKUQRY") || ("$report" eq "NSKUQRY")) {

    if ("$report" eq "SKUQRY") {
        print $q->h2("Akron SKU Report Query");
    } elsif ("$report" eq "NSKUQRY") {
        print $q->h2("Navision SKU Report Query");
    }
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";

    print $q->input({-type=>"hidden", -name=>"report", -value=>"SKURPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
    # SKU
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "SKU:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"sku_selection", -size=>"12", -value=>""}))
    );
    # Vendor

    # The vendor code in this table is not the vendor as seen in the store - kdg
   # print $q->Tr(
   #     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Vendor:"),
   #     $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"vendor_selection", -size=>"12", -value=>""}))
   # );
    # Seasonality
    $query = dequote(<< "    ENDQUERY");
    select
        distinct Seasonality
    from
        tblAkronSkuInfo
    order
        by Seasonality
    ENDQUERY
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    $dbh->disconnect;

    my $seasonality_options;
    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];
        my $code = trim($$thisrow[0]);
        $seasonality_options .= $q->option({-value=>$code}, "$code");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Seasonality UF1:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"seasonality_selection", -size=>"1"}), $seasonality_options)
              );
    ##
   # print $q->Tr(
   #     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Seasonality UF1:"),
   #     $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"seasonality_selection", -size=>"12", -value=>""}))
   # );
    # VMCode
    $query = dequote(<< "    ENDQUERY");
    select
        distinct([Visual Merchandising Code])
    from
        [Villages\$Item]
    ENDQUERY
    $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
    $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    $tbl_ref = execQuery_arrayref($dbh, $query);
    $dbh->disconnect;

    my $vm_options;
    for my $datarows (1 .. $#$tbl_ref) {
        my $thisrow = @$tbl_ref[$datarows];
        my $code = trim($$thisrow[0]);
        $vm_options .= $q->option({-value=>$code}, "$code");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "VMCode UF2:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"vm_selection", -size=>"1"}), $vm_options)
              );
    if ("$report" eq "NSKUQRY") {
        # MarkDown
        $query = dequote(<< "        ENDQUERY");
        select
            distinct([Item Disc_ Group])
        from
            [Villages\$Item]
        order by
            [Item Disc_ Group]
        ENDQUERY
        my $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
        my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        $dbh->disconnect;

        my $markdown_options;
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $code = trim($$thisrow[0]);
            $markdown_options .= $q->option({-value=>$code}, "$code");
        }
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MarkDown:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"markdown_selection", -size=>"1"}), $markdown_options)
                  );
    }
    if ("$report" eq "SKUQRY") {
        $query = dequote(<< "        ENDQUERY");
        select
            distinct AkronStatus
        from
            tblAkronSkuInfo
        ENDQUERY
        my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
        my $dbh = openODBCDriver($dsn, '****', '****');
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        $dbh->disconnect;

        my $status_options= $q->option({-value=>'' -selected=>undef});

        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $code = trim($$thisrow[0]);
            $status_options .= $q->option({-value=>$code}, "$code");
        }
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Akron Status UF3:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"status_selection", -size=>"1"}), $status_options)
                  );
        # Akron Status
        #print $q->Tr(
        #    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Akron Status UF3:"),
        #    $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"status_selection", -size=>"12", -value=>""}))
        #);
    }
    # New Item Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Item Date UF5:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"nidate_selection", -size=>"12", -value=>""}))
    );
    # Current Local Cost
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Current Local Cost:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"clcost_selection", -size=>"12", -value=>""}))
    );
	# Supplies or Products
    ###
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"radio_option", -value=>"all", -checked=>"1"}) .
               $q->font({-size=>2}, "Show Products & Supplies")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"radio_option", -value=>"product",}) .
               $q->font({-size=>2}, "Show only Products")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"radio_option", -value=>"supply",}) .
               $q->font({-size=>2}, "Show only Supplies")));
    ###

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "SKURPT") {

	my $radio_option   = trim(($q->param("radio_option"))[0]);
	my $sku_selection   = trim(($q->param("sku_selection"))[0]);
	my $vendor_selection   = trim(($q->param("vendor_selection"))[0]);
	my $seasonality_selection   = trim(($q->param("seasonality_selection"))[0]);
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    my $vm_selection   = trim(($q->param("vm_selection"))[0]);
    my $markdown_selection   = trim(($q->param("markdown_selection"))[0]);
    my $status_selection   = trim(($q->param("status_selection"))[0]);
    my $nidate_selection   = trim(($q->param("nidate_selection"))[0]);
    my $clcost_selection = "NULL";
    $clcost_selection   = trim(($q->param("clcost_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);
    my $top;
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, '****', '****');
    my $orig_where = "1 = 1";
    my $where = $orig_where;
	my @format;
	my %store_not_found_hash;
    if ($report_selection eq "STORESKUQRY") {
	    my $history_selection   = trim(($q->param("history_selection"))[0]);
		my $QOH_selection   = trim(($q->param("QOH_selection"))[0]);
        print $q->h2("Store SKU Info Report");

		$where.= " and v.storeid > 100 and rs.storecode in ('V1','V2') and rs.enddate is NULL";
		my $storewhere;
        if ($sku_selection) {
            $where.=" and v.sku like '$sku_selection' ";
        }
        if ($vendor_selection) {
            $where.=" and VendorId = '$vendor_selection' ";
        }
        if ($seasonality_selection) {
            $where.=" and v.Seasonality  like '$seasonality_selection%' ";
        }
        if ($vm_selection) {
            $where.=" and v.vmcode  like '$vm_selection%' ";
        }
        if ($status_selection) {
            $where.=" and v.AkronStatus  like '$status_selection%' ";
        }
        if ($store_selection ne "All") {
			if ($store_selection eq "Company") {
				$where.=" and rs.StoreCode  like 'V1' ";
				$storewhere="StoreCode  like 'V1' ";
			} elsif ($store_selection eq "Contract") {
				$where.=" and rs.StoreCode  like 'V2' ";
				$storewhere="StoreCode  like 'V2' ";
			} else {
				$where.=" and v.storeid  like '$store_selection' ";
			}
        } else {
			$storewhere="StoreCode  like 'V1' or StoreCode  like 'V2'";
		}

        if ($QOH_selection) {
            $where.=" and v.qty > 0 ";
        }
        if ($nidate_selection) {
            if ($nidate_selection =~ /null/i) {
                $where.=" and v.dtnewit is NULL ";
            } else {
                my $date="${nidate_selection}-01";
                $where.=" and v.dtnewit  = '$date' ";
            }

        }
        if ("$where" eq "$orig_where") {
            # If we did not have any qualifications selected, just show the top 500
            $top=" top 500 ";
        }
		my %store_hash;

        $query = dequote(<< "        ENDQUERY");
        select
            $top
			v.storeid,
			rs.storename,
            v.sku,
            v.descrip,
			v.price,
			v.cost,
            v.qty,
			v.minqty,
			v.maxqty,
            v.storestatus,
            v.dept,
            v.lastupdate
        from
            tblStoreSkuInfo v
			left outer join rtblStores rs on rs.storeid=v.storeid
        where
            $where
        ENDQUERY

		@format = (
		'',
		'',
		'',
		'',
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'num' => ''},
		{'align' => 'Right', 'num' => ''},
		{'align' => 'Right', 'num' => ''},
		'',

		'',
		);
		if ($history_selection) {
			$query = dequote(<< "			ENDQUERY");
			select
				$top
				v.storeid,
				rs.storename,
				v.trandate,
				v.sku,
				v.price,
				v.cost,
				v.[QtyOnHand],
				v.minqty,
				v.maxqty,
				v.[VendorId]
			from
				tblStoreSkuHist v
				left outer join rtblStores rs on rs.storeid=v.storeid
			where
				$where
			order by
				v.trandate desc
			ENDQUERY
			@format = (
			'',
			'',
			'',
			'',
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'num' => ''},
			{'align' => 'Right', 'num' => ''},
			{'align' => 'Right', 'num' => ''},
			'',

			'',
			);
		}
###
		if ($storewhere) {

			my $tbl_ref = execQuery_arrayref($dbh, $query);
			if (recordCount($tbl_ref)) {
					for my $datarows (1 .. $#$tbl_ref) {
					my $thisrow = @$tbl_ref[$datarows];
					my $storeid = trim($$thisrow[0]);
					my $storename = trim($$thisrow[1]);
					# Build a hash with all of the stores where the sku is found
					$store_hash{$storeid}=$storename;

				}
			}
			my $count=keys(%store_hash);
			# Get a list of all of the stores so that we will know which stores the specified sku is NOT found in
			 #$storewhere.=" and v.sku like '$sku_selection' ";
			my $all_store_query = dequote(<< "			ENDQUERY");
				select
					storeid,
					storename
				from
					rtblStores
				where
					($storewhere)
				and
					EndDate is null
				and
					StartDate < GETDATE()
				order by
					StoreId
			ENDQUERY


			my $tbl_ref2 = execQuery_arrayref($dbh, $all_store_query);
			my $counter=0;
			if (recordCount($tbl_ref2)) {
				for my $datarows (1 .. $#$tbl_ref2) {
					$counter++;
					my $thisrow = @$tbl_ref2[$datarows];
					my $storeid = $$thisrow[0];
					my $storename = $$thisrow[1];
					unless ($store_hash{$storeid}) {
						# The SKU was NOT found in this store
						$store_not_found_hash{$storeid}=$storename;
					}
				}
			}
		}
###


    }
    if ($report_selection eq "SKUQRY") {
        print $q->h2("Akron SKU Info Report");
        if ($sku_selection) {
            $where.=" and v.sku like '$sku_selection%' ";
        }
        if ($vendor_selection) {
            $where.=" and VendorId = '$vendor_selection' ";
        }
        if ($seasonality_selection) {
            $where.=" and v.Seasonality  like '$seasonality_selection%' ";
        }
        if ($vm_selection) {
            $where.=" and v.vmcode  like '$vm_selection%' ";
        }
        if ($status_selection) {
            $where.=" and v.AkronStatus  like '$status_selection%' ";
        }
        if ($nidate_selection) {
            if ($nidate_selection =~ /null/i) {
                $where.=" and v.dtnewit is NULL ";
            } else {
                my $date="${nidate_selection}-01";
                $where.=" and v.dtnewit  = '$date' ";
            }

        }
		if ($radio_option eq "supply") {
			# Limit to supplies
			$where.=" and v.supply = 1 ";
		} elsif ($radio_option eq "product") {
			# Limit to products
			$where.=" and v.supply = 0 ";
		}
        if ("$where" eq "$orig_where") {
            # If we did not have any qualifications selected, just show the top 500
            $top=" top 500 ";
        }
        $query = dequote(<< "        ENDQUERY");
        select
            $top
            v.sku,
            v.descript,
            v.qty,
            v.price,
            v.avgcost,
            v.xtrcost,
            v.regcost,
            v.ooccost,
            v.Seasonality,
            v.vmcode,
            v.class,
            convert(char(7), v.dtnewit, 21) as "dtnewit",
            v.AkronStatus,
            v.AkronRank,
            v.upccode,
            pc.ProductCare,
            v.supply,
            v.vendno,
            v.vendname,
            v.lastufupdate,
            v.lastupdate
        from
            tblAkronSkuInfo v
            left outer join tblAkronProductCare pc on v.sku=pc.sku
        where
            $where
        ENDQUERY
    }

    if ($report_selection eq "NSKUQRY") {
        print $q->h2("Navision SKU Report");
        $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
        $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
        if ($sku_selection) {
            $where.=" and ii.[No_] like '$sku_selection%' ";
        }

        if ($vendor_selection) {
            $where.=" and ii.[Vendor No_] = '$vendor_selection' ";
        }
        if ($seasonality_selection) {
            $where.=" and ii.[Seasonality Code]  like '$seasonality_selection%' ";
        }
        if ($vm_selection) {
            $where.=" and ii.[Visual Merchandising Code]  like '$vm_selection%' ";
        }
        if ($markdown_selection) {
            $where.=" and ii.[Item Disc_ Group]  like '$markdown_selection%' ";
        }
        if ($status_selection) {
            $where.=" and v.AkronStatus  like '$status_selection%' ";
        }
        if ($nidate_selection) {
            $where.=" and ii.[New Item Date] like '$nidate_selection%' ";
        }
        if (($clcost_selection ne "NULL")&&($clcost_selection ne '')) {
            $where.=" and ii.[Current Local Cost] = '$clcost_selection' ";
        }
		if ($radio_option eq "supply") {
			# Limit to supplies
			$where.=" and [Inventory Posting Group] like 'SUPPLIES' ";
		} elsif ($radio_option eq "product") {
			# Limit to products
			$where.=" and [Inventory Posting Group] like 'PRODUCTS' ";
		}
        if ("$where" eq "$orig_where") {
            # If we did not have any qualifications selected, just show the top 500
            $top=" top 500 ";
        }

        $query = dequote(<< "        ENDQUERY");
       select
          $top
          ii.[No_] as SKU,
          case when ii.[GiftCardID] > ''
             then substring(case when ii.[PLU Description] > '' then ii.[PLU Description] else ii.[Description] end, 1, 32) + ' GE'+ substring(ii.[GiftCardID], 3, 5)
             else case when ii.[PLU Description] > '' then ii.[PLU Description] else ii.[Description] end end
          as POSDesc,
          ii.[BarCode for Printing] as UPCCode,

          convert(numeric(8,2), ii.[Unit Price]) as "UnitPrice",
          ii.[Item Disc_ Group] as DiscGroup,
          convert(numeric(8,3), (select top 1 [Line Discount %] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'REG')) as DiscREG,
          convert(numeric(8,3), (select top 1 [Line Discount %] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'XTR')) as DiscXTR,
          convert(numeric(8,2), ii.[Unit Cost]) as UnitCost,
          convert(numeric(8,2), ii.[Current Local Cost]) as CurrentLocalCost,
          ii.[Seasonality Code] as Seasonality,
          ii.[Visual Merchandising Code] as VMCode,
          ii.[Item Department] + ii.[Class] as Class,
          ii.[New Item Date] as NewItemDate,
		  ii.[Available for Sale Date] as AvailableDate,
          ii.[Discontinued Date] as DiscontinuedDate,
          ii.[Obsoleted Date] as ObsoletedDate,
          case when ii.[Discontinued Date] > '1753-01-01' and ii.[Discontinued Date] <= getdate() then 1 else 0 end as DiscontFlag,
          case when ii.[Obsoleted Date] > '1753-01-01' and ii.[Obsoleted Date] <= getdate() then 1 else 0 end as ObsoleteFlag,
          ii.[Web Rank] as WebRank,
          ii.[Vendor No_] as VendorNo,
          v.[Short Name] as VendorName,
          ii.[Vlgs-Contract], ii.Partner, ii.Festival, ii.[Vlgs-Company],
          pc.[Description] as ProductCare,
          convert(numeric(8,3), (select top 1[Retail Price Discount] from [Villages\$Sales Line Discount] where [Code] = ii.[Item Disc_ Group] and [Sales Code] = 'XTR')) as RetailDisc,
          case when [Inventory Posting Group] not in ('PRODUCTS', 'SUPPLIES') or
                    (ii.[Item Department] between '70' and '79') then 0 else 1 end as resell,
          [Inventory Posting Group] as InvPostGrp,
          convert(numeric(8,3), (select top 1 [Unit Price] from [Villages\$Sales Price]
                                 where [Sales Code] = 'FIXED' and [Item No_] = ii.[No_] and
                                 ([Starting Date] = '1753-01-01' or [Starting Date] < getdate()) and
                                 ([Ending Date] = '1753-01-01' or [Ending Date] >= getdate())
                                )) as FixedPrice,
		  co.[Name] as [Country],
		  ii.[Last Date Modified],
		  ii.[Last User Modified]
		 
			
       from
          [Villages\$Item] ii
          left outer join [Villages\$Vendor] v on ii.[Vendor No_] = v.[No_]
          left outer join [Villages\$TTV System Rules] pc on ii.[Product Care Code] = pc.Code and pc.[Ruleid] = 'PRODCARE'
		  left outer join [Villages\$Country] co on co.Code = v.[Country Code]
		   
       where
        $where
		 
       order by
          ii.[No_]

        ENDQUERY

    }

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {

        if ($top) {
            print $q->p("Showing $top SKUs");
        }
        printResult_arrayref2($tbl_ref, '', \@format);
		if ($report_selection eq "STORESKUQRY") {
			my $not_in=keys(%store_not_found_hash);
			if ($not_in) {
				print $q->p("Not found in these stores:");
				foreach my $storeid (sort(keys(%store_not_found_hash))) {
					my $storename=$store_not_found_hash{$storeid};
					print $q->b("$storeid $storename<br />");
				}
			} else {
				print $q->p("No stores where SKU was not found");
			}
		 

		}
		if ($report_selection eq "NSKUQRY") {
			print "<br />";
			my @format_qty = (
			'',
			{'align' => 'Right', 'num' => ''},
			{'align' => 'Right', 'num' => ''},
			'',
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'num' => ''},
			{'align' => 'Right', 'num' => ''},
			{'align' => 'Right', 'num' => ''},
			'',

			'',
			);
			for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
				my $plunum=$$thisrow[0];
	 
				# Get the inventory levels
				my $qty_query="
				select
					$plunum as SKU,
					sum(I.[Quantity]) as LedgerQty
				 
				from
					[Villages\$Item Ledger Entry] I
					 
				where
					I.[Item No_] = '$plunum' and
					I.[Store Inventory] = 0 and
					I.[Location Code] = 'B' and
					I.[Global Dimension 1 Code] in (select [AreaCode] from [dbo].[Villages\$GBPGAreaCodes]('COMPANY'))					
				";
				my $tbl_ref_qty = execQuery_arrayref($dbh, $qty_query);
				if (recordCount($tbl_ref_qty)) {
 
					printResult_arrayref2($tbl_ref_qty, '', \@format_qty);		
				}
				$qty_query="
				select
					$plunum as SKU,
					 
					sum(isnull(R.[Quantity (Base)],0) ) as ReservationQty
				from
					 
					[Villages\$Reservation Entry] R  
				where
					R.[Item No_] = '$plunum' and
					R.[Location Code] = 'B' and 
					R.[Global Dimension 1 Code] in 
						(select [AreaCode] from [dbo].[Villages\$GBPGAreaCodes]('COMPANY')) and 
					R.[Reservation Status] = 0 and   
					R.[Source Type] = 37 and        
					R.[Source Subtype] = 1 					
				";
				$tbl_ref_qty = execQuery_arrayref($dbh, $qty_query);
				if (recordCount($tbl_ref_qty)) {
 
					printResult_arrayref2($tbl_ref_qty, '', \@format_qty);		
				}				

            }	
			# Get the info about the last date modified
			
		}
    } else {
        print $q->p("No data found");
    }

    $dbh->disconnect;
    print "<br>";
	print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this report");
    standardfooter();
	

} elsif ("$report" eq "PROPROMPTQRY")  {

    print $q->h2("Profile Prompt Report Query");

   # print "Specify at least the store number or the order number.<br><br>";
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PROPROMPTRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));

    # Start Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"start_date_selection", -size=>"12", -value=>""}))
    );
    # End Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date(yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"end_date_selection", -size=>"12", -value=>""}))
    );
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"storenum_selection", -size=>"12", -value=>""}))
    );
    # Profile Prompt Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Profile Prompt:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"profile_prompt_selection", -size=>"12", -value=>""}))
    );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
	print $q->p("Profile Prompts:");
	print "Check Info - 5<br />";
	print "Paid In/Out - 6<br />";
	print "Return Info - 7<br />";
	print "Zip Code - 8<br />";
	print "Non Merchandise Other - 11<br />";
	print "Non Merchandise Donation Other - 12<br />";
	print "Non Merchandise Donation Other - 16<br />";
	print "Donation Living Gift - 18<br />";
	print "Customer Shopping Event - 30<br />";
	print "Show all Donations: donation<br />";

    standardfooter();	
} elsif ("$report" eq "PROPROMPTRPT") {
    print $q->h2("Profile Prompt Report");
   # print "Click on the order number to see detail for this order.<br><br>";
	my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
	my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);
	my $storenum_selection   = trim(($q->param("storenum_selection"))[0]);
    my $profile_prompt_selection   = trim(($q->param("profile_prompt_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);

 
    unless ($storenum_selection)  {
        print $q->p("Please specify a storenumber ");
        standardfooter();
        exit;
    }
	print $q->p("Store: $storenum_selection - $start_date_selection to $end_date_selection");
	my $start_date_where;
	my $end_date_where;
	my $prompt_where;
    if ($start_date_selection) {
        $start_date_where=" and [TranDateTime] >= '$start_date_selection' ";
    }
    if ($end_date_selection) {
        $end_date_where=" and [TranDateTime] <= '$end_date_selection' ";
    }
	if ($profile_prompt_selection) {
		if ($profile_prompt_selection =~ /donation/i) {
			$prompt_where=" and [ProfileId] in ('12','16','18') ";
		} else {
			$prompt_where=" and [ProfileId] in ('$profile_prompt_selection') ";
		}
	}
 

    my $query = dequote(<< "    ENDQUERY");
    select 
        tm.[TranDateTime],
		tm.RegisterId,
		tm.Trannum,
 
		tpr.ProfileId,
		tpr.Sequence,
		tpr.Response1,
		tpr.Response2,
		tpr.Response3,
		tpr.Response4
		 
	from 
		dbo.tblProfileResponse tpr
		join dbo.tblTranMaster tm on tpr.Tranid = tm.Tranid
    where 
        tm.[StoreId] like '$storenum_selection'              
        $start_date_where
		$end_date_where
		$prompt_where
       
    ENDQUERY
	
	#print $q->pre("$query");
	my $dsn = "driver={SQL Server};Server=****;database=****Tlog;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @links = ( '','','',"$scriptname?report=WIREORDRRPT2&report_selection=$report_selection&order_selection=".'$_' );
        my @format = (
			 
		'',
		{'align' => 'Center'},
		{'align' => 'Center'},
	 
		{'align' => 'Center'},
		);
        #printResult_arrayref2($tbl_ref, \@links, \@format);
		printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;
    print "<br>";
	#print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this report");

    standardfooter();	
} elsif ("$report" eq "WIREORDRQRY")  {

    print $q->h2("Wire Order Report Query");

    print "Specify at least the store number or the order number.<br><br>";
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"WIREORDRRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));

    # Start Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"start_date_selection", -size=>"12", -value=>""}))
    );
    # End Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date(yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"end_date_selection", -size=>"12", -value=>""}))
    );
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"storenum_selection", -size=>"12", -value=>""}))
    );
    # Order Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Wire Order Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"wire_order_selection", -size=>"12", -value=>""}))
    );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "WIREORDRRPT") {
    print $q->h2("Wire Order Report");
    print "Click on the order number to see detail for this order.<br><br>";
	my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
	my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);
	my $storenum_selection   = trim(($q->param("storenum_selection"))[0]);
    my $wire_order_selection   = trim(($q->param("wire_order_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);

    my $where = "1 = 1";
    unless (($storenum_selection)||($wire_order_selection)) {
        print $q->p("Please specify at least the storenumber or wire order number ");
        standardfooter();
        exit;
    }
    if ($storenum_selection) {
        $where.=" and [Customer No_] = '$storenum_selection' ";
    }
    if ($start_date_selection) {
        $where.=" and [Order Date] >= '$start_date_selection' ";
    }
    if ($end_date_selection) {
        $where.=" and [Order Date] <= '$end_date_selection' ";
    }
    if ($wire_order_selection) {
        $where.=" and [External Doc_ No_] like 'WIRE%$wire_order_selection%' ";
    }

    $query=dequote(<< "    ENDQUERY");
    SELECT
           [Entry No_]
          ,[Customer No_]
          ,[Document Type]
          ,[Order No_]
          ,[Order Date]
          ,[Requested Ship Date]
          ,[Ship-to Name]
          ,[External Doc_ No_]
          ,[Comments]
          ,[Location Code]
          ,[Festival Return]
          ,[Status]
          ,[Date Time Entered]
          ,[Date Time Processed]
          ,[On Hold]
          ,[Error Message]
          ,[Contact Email]
          ,[Contact No_]
          ,[WMS Pivot]
          ,[Add to Backorder Order]
          ,[No Backorders Allowed]
          ,[Reference]
          ,[Ship-to Address Type]
          ,[Entered By]
      FROM [****].[dbo].[Villages\$Order Buffer Header]
      where
        $where
    ENDQUERY

    my $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @links = ( '','','',"$scriptname?report=WIREORDRRPT2&report_selection=$report_selection&order_selection=".'$_' );
        my @format = ('', {'align' => 'Right', 'num' => '123'});
        printResult_arrayref2($tbl_ref, \@links, \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;
    print "<br>";
	print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this report");

    standardfooter();

} elsif ("$report" eq "WIREORDRRPT2") {

    my $order_selection   = trim(($q->param("order_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);

    if ($report_selection eq "WIREORDRQRY") {
        print $q->h2("Wire Order Report Detail")
    } else {
       print $q->h2("Order Report Detail")
    }
    my $where;
    unless ($order_selection) {
        print $q->p("Failed to get the order number to search for.");
        standardfooter();
        exit;
    } else {
        $where=" [Order No_] = '$order_selection' ";
    }


    $query=dequote(<< "    ENDQUERY");
    SELECT
          obd.[Master Entry No_]
          ,obd.[Entry No_]
          ,obd.[Customer No_]
          ,obd.[Document Type]
          ,obd.[Order No_]
          ,obd.[Line No_]
          ,obd.[Type]
          ,obd.[No_]
          ,cast(obd.[Quantity] as int) as Qty
          ,obd.[Location Code]
          ,convert(decimal(10,2),obd.[Unit Price] ) as "Unit Price"
          ,vi.[Description]
          ,cast(obd.[Reserved Qty_] as int) as "Reserved Qty"
          ,obd.[Processed]
          ,obd.[Error]
          ,obd.[Date Time Entered]
          ,obd.[Date Time Processed]

      FROM [****].[dbo].[Villages\$Order Buffer Detail] obd
      join [****].[dbo].[Villages\$Item] vi on obd.[No_] = vi.[No_]
      where
        $where
    ENDQUERY

    my $dsn = "driver={SQL Server};Server=ABACUS;database=****;uid=rpt;pwd=rpt;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
        #my @links = ( '','','',"$scriptname?report=WIREORDRRPT2&order_selection=".'$_' );
        my @format = ('', {'align' => 'Right', 'num' => '123'});
        printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;
	print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this report");
    standardfooter();

	
} elsif ("$report" eq "WIREGRPQRY")  {

    print $q->h2("Wire Group Report Query");

    print "Select a Group to show the members of.<br><br>";
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"WIREGRPRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
	###
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;

    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }	
	# Find the groups    
	$query = dequote(<< "	ENDQUERY");
	select
		nid,
		title				
	from
		node
	where
		type = 'group'
	order by
		type
	ENDQUERY
	     
	my $tbl_ref = execQuery_arrayref($dbh, $query);
	$dbh->disconnect;

	my $group_options= $q->option({-value=>'' -selected=>undef});

	for my $datarows (1 .. $#$tbl_ref) {
		my $thisrow = @$tbl_ref[$datarows];
		my $nid = trim($$thisrow[0]);
		
		my $description = trim($$thisrow[1]);
		
		$group_options .= $q->option({-value=>$nid}, "$description");
	}
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Group:"),
			   $q->td({-class=>'tabledatanb', -align=>'left'},
					  $q->Select({-name=>"group_selection", -size=>"1"}), $group_options)
			  );	
	###
 

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();	
} elsif ("$report" eq "WIREGRPRPT") {
    print $q->h2("Wire Group Report");
    my $group_selection   = trim(($q->param("group_selection"))[0]);
 
	# Get the group name again
 
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;

    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }	
	
	$query = dequote(<< "	ENDQUERY");
	select		
		title				
	from
		node
	where
		nid = $group_selection
	
	ENDQUERY
	     
	my $tbl_ref = execQuery_arrayref($dbh, $query);	
	my $group_name=$$tbl_ref[1][0];
	
	print $q->h3("Wire Group: $group_name");
	
    $query=dequote(<< "    ENDQUERY");
    SELECT
		u.name,
		u.status,	 
		FROM_UNIXTIME(u.created) as Created,				
		FROM_UNIXTIME(u.access) as LastAccess,		 
		FROM_UNIXTIME(u.login) as LastLogin		
	from
		users u
		join og_uid ou on ou.uid = u.uid
	where
		ou.nid = $group_selection
    ENDQUERY

    $tbl_ref = execQuery_arrayref($dbh, $query);

    if (recordCount($tbl_ref)) {
       
        my @format = ('',{'align' => 'center'});
        printResult_arrayref2($tbl_ref, '', \@format);
    } else {
        print $q->p("No data found");
    }
    $dbh->disconnect;
	
    standardfooter();
} elsif ("$report" eq "QTOOL") {
    print $q->h2("Query Tool");
    ### Don't Build the table
   # print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    # Select database
    my @db_list=(
        "****Data",
        "****",
        "****",
        "****TLog",
        "dbWarehouse",
        "pos_support",
        "vcx_cdr"

    );
    my $db_options; #= $q->option({-value=>"****Data" -selected=>1},"****Data");
    foreach my $db (@db_list) {
        $db_options .= $q->option({-value=>"$db"}, "$db");
    }
    # Build a form to browse tables
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"TBLBROWS"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->p("Browse database tables");
    print "Database: ",$q->Select({-name=>"db_selection", -size=>"1"}, $db_options);
    print "<br>";
    # Add the submit button
    print  $q->submit({-accesskey=>'S', -value=>"   Browse   "});

    print $q->end_form(), "\n";
    print "<br><br>";
    # Build a form to query
    print $q->p("...Or, build a query");
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"QTOOL2"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});



    print "Database: ",$q->Select({-name=>"db_selection", -size=>"1"}, $db_options);
    print " Row Count: ",$q->input({ -type=>'text', -name=>"rcount_selection", -size=>"5", -value=>"10"});
    print " Vertical: ",$q->input({ -type=>'checkbox', -name=>"vert_selection",  -value=>"10"});
    print "<br>";
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}),
        $q->textarea({-name=>"querytext", -cols=>'60', -rows=>'25', -value=>"select * from tblAkronSkuInfo "}),

        );
    print "<br>";
    # Add the submit button
    print  $q->submit({-accesskey=>'S', -value=>"   Submit   "});

    print $q->end_form(), "\n";

    print $q->p();
    standardfooter();
} elsif ("$report" eq "QTOOL2") {


    my $db_selection   = trim(($q->param("db_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);
    my $rcount_selection   = trim(($q->param("rcount_selection"))[0]);
    my $querytext = trim(($q->param("querytext"))[0]);
    print $q->h2("Query Tool Result: from $db_selection");
    my $dsn;
    my $dbh;
    my $dbtype="mysql";
    if (($db_selection eq "pos_support") || ($db_selection eq "vcx_cdr")) {

        $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$db_selection","pos","sop");
    } else {
        $dsn = "driver={SQL Server};Server=****;database=$db_selection;uid=****;pwd=****;";
        $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
        $dbtype="mssql";
    }
    my @query_list=split(/\;/,$querytext);
    foreach my $subquery (@query_list) {
        my $tbl_ref2 = [];
        unless ($rcount_selection eq "-1") {
            if ($subquery =~ /^select/i) {
                # If this is a select query and we have not specified we want to select all of the data...
                if ($dbtype eq "mssql") {
                    $subquery=~s/^select/select top $rcount_selection/;
                } else {
                    $subquery .= " limit $rcount_selection";
                }

            }
        }

        my $tbl_ref = execQuery_arrayref($dbh, $subquery);
=pod
        unless ($rcount_selection eq "-1") {
            for my $datarows (0 .. $rcount_selection) {
                my $thisrow = @$tbl_ref[$datarows];
                if ($#$thisrow > -1) {
                    # Only add this to final result if there is actually data there
                    push @$tbl_ref2, $thisrow;
                }

            }
            $tbl_ref=$tbl_ref2;
        }
=cut
        if (recordCount($tbl_ref)) {
            print $q->p("$subquery");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }
    }


    $dbh->disconnect;
    print "<br>";
    print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this tool");
    standardfooter();
} elsif ("$report" eq "TBLBROWS") {
    my $db_selection   = trim(($q->param("db_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);
    print $q->h2("Tables of the $db_selection database");
    $query = 'select table_name as "Table" from information_schema.Tables';

    if ($db_selection eq "****") {
        print "Sorry, this feature has not been implemented for **** yet.<br>";
        print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this tool");
        standardfooter();
        exit;
    }
    my $dsn;
    my $dbh;
    my $mysql=0;
    my $tbl_ref;
    if (($db_selection eq "pos_support") || ($db_selection eq "vcx_cdr")) {
        $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$db_selection","pos","sop");
        $query = 'show tables';
        $mysql=1;
            my @header=("Table Name");
            push (@$tbl_ref,\@header);
    } else {
        $dsn = "driver={SQL Server};Server=****;database=$db_selection;uid=****;pwd=****;";
        $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    }

     # = execQuery_arrayref($dbh, $query);
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    my $count=0;
    while (my @tmp = $sth->fetchrow_array()) {
        if ($mysql) {
            push (@$tbl_ref,\@tmp);
        } else {
            # Skip the certain tables
            if (($tmp[0] =~ /rtbl/)||($tmp[0] =~ /tbl/)) {
                push (@$tbl_ref,\@tmp);
            }
        }

    }

    my $def_table=$$tbl_ref[1][0];
    if (recordCount($tbl_ref)) {
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }

    $dbh->disconnect;

    # Build a form to query
    print $q->p("Build a query of the $db_selection database:");
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"QTOOL2"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->input({-type=>"hidden", -name=>"db_selection", -value=>"$db_selection"});

    print " Row Count: ",$q->input({ -type=>'text', -name=>"rcount_selection", -size=>"5", -value=>"10"});
    print " Vertical: ",$q->input({ -type=>'checkbox', -name=>"vert_selection",  -value=>"10"});
    print "<br>";
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}),
        $q->textarea({-name=>"querytext", -cols=>'60', -rows=>'15', -value=>"select * from $def_table "}),

        );
    print "<br>";
    # Add the submit button
    print  $q->submit({-accesskey=>'S', -value=>"   Submit   "});

    print $q->end_form(), "\n";
    print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this tool");
    standardfooter();
} elsif ("$report" eq "CDR_AE_TOOL") {
    print $q->p("Phone Alias Edit Tool:");

    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld");
		standardfooter();
		exit;
	}
    $query = dequote(<< "    ENDQUERY");
    select
        BASE_PHONE_NO as PhoneNo,
        PRIMARY_REF as PrimaryAlias,
        SECONDARY_REF as SecondaryAlias,
        PHONE_NO as FullPhoneNo
    from
        phone_cross_ref
    order by PrimaryAlias,SecondaryAlias,PhoneNo

    ENDQUERY

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @links = ( "$scriptname?report=CDR_AE_TOOL2&phone_selection=".'$_' );
        printResult_arrayref2($tbl_ref, \@links);
    } else {
        print $q->p("No Records Found");
    }
    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "CDR_AE_TOOL2") {
    print $q->h3("Phone Alias Edit Tool:");
    my $phone_selection   = trim(($q->param("phone_selection"))[0]);

    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld");
		standardfooter();
		exit;
	}	
    $query = dequote(<< "    ENDQUERY");
    select
        BASE_PHONE_NO as PhoneNo,
        PRIMARY_REF as PrimaryAlias,
        SECONDARY_REF as SecondaryAlias,
        PHONE_NO as FullPhoneNo
    from
        phone_cross_ref
    where
        BASE_PHONE_NO = '$phone_selection'

    ENDQUERY

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->p("Current Values");
        printResult_arrayref2($tbl_ref);
        print $q->p();
        # Give the options to enter new aliases or to delete the entry
        my $p_alias = $$tbl_ref[1][1];
        my $s_alias=$$tbl_ref[1][2];

###
        ### Build the table
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"CDR_AE_TOOL3"});
        print $q->input({-type=>"hidden", -name=>"phone_selection", -value=>"$phone_selection"});
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Edits")));
        # Primary Alias
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Primary Alias:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"primary_selection", -size=>"12", -value=>"$p_alias"}))
        );
        # Secondary Alias
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "New Secondary Alias:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"secondary_selection", -size=>"12", -value=>"$s_alias"}))
        );
        # Delete Option
        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Delete This Entry:"),
            $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'checkbox', -name=>"delete_selection",   -value=>""}))
        );

        # Add the submit button
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
                  );

        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        print $q->p("Enter edits and click 'Submit' or return to Alias edits with the link below.");
###
    } else {
        print $q->p("No Records Found");
    }
    $dbh->disconnect;
    localfooter("CDR_AE_TOOL");
    standardfooter();
} elsif ("$report" eq "CDR_AE_TOOL3") {
    print $q->h3("Phone Alias Edit Tool:");
    my $phone_selection   = trim(($q->param("phone_selection"))[0]);
    my $primary_selection   = trim(($q->param("primary_selection"))[0]);
    my $secondary_selection   = trim(($q->param("secondary_selection"))[0]);
    unless ($phone_selection) {
        print "Error: Failed to get the phone number.<br>";
        standardfooter();
        exit;
    }
    my $set_clause = "set PRIMARY_REF = '$primary_selection', SECONDARY_REF = '$secondary_selection'";
    my $delete_selection=0;
    foreach my $name ($q->param) {
        if ($name =~ /delete_selection/) {
            $delete_selection=1;
        }

    }


    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld");
		standardfooter();
		exit;
	}	
    if ($delete_selection) {
        # Delete
        $query = dequote(<< "        ENDQUERY");
        delete
        from
            phone_cross_ref
        where
            BASE_PHONE_NO = '$phone_selection'

        ENDQUERY

    } else {
        # Update
        $query = dequote(<< "        ENDQUERY");
        update
            phone_cross_ref
        $set_clause
        where
            BASE_PHONE_NO = '$phone_selection'

        ENDQUERY
    }

    if ($dbh->do($query)) {
        print $q->h4( "Successfully Updated phone_cross_ref.");
    } else {
        print $q->h3( "ERROR: Failed to update phone_cross_ref.");
        print "Query: $query \n";
    }
    $dbh->disconnect;
    localfooter("CDR_AE_TOOL");
    standardfooter();
} elsif ("$report" eq "CDR_PN_TOOL") {
    print $q->p("Phone Number Tool:");
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld");
		standardfooter();
		exit;
	}	

    ############################
    # Update the phone_cross_ref table
    ############################
    $query = dequote(<< "    ENDQUERY");
    update
        phone_cross_ref
    set
        BASE_PHONE_NO = if (length(phone_no) > 10,SUBSTRING(phone_no,-10,10),phone_no)
    where
        BASE_PHONE_NO is NULL
    ENDQUERY

    if (1) {
        if ($dbh->do($query)) {
            print "Successfully Updated phone_cross_ref.<br>";
        } else {
            print "ERROR: Failed to update phone_cross_ref.<br>";
            print "Query: $query \n";
        }
    }
    ###################################
    # Update the call_detail_record table - Called Number
    ###################################
    $query = dequote(<< "    ENDQUERY");
    update
        call_detail_record
    set
        calledNo = if (length(CALLEDPARTYE164ADDRESS) > 10,SUBSTRING(CALLEDPARTYE164ADDRESS,-10,10),CALLEDPARTYE164ADDRESS)
    where
        calledNo is NULL
    ENDQUERY

    if (1) {
        if ($dbh->do($query)) {
            print "Successfully Updated called numbers call_detail_record.<br>";
        } else {
            print "ERROR: Failed to update called numbers call_detail_record.<br>";
            print "Query: $query \n";
        }
    }
    ################################
    # Update the call_detail_record table - Calling Number
    ################################
    $query = dequote(<< "    ENDQUERY");
    update
        call_detail_record
    set
        callingNo = if (length(CALLINGPARTYE164ADDRESS) > 10,SUBSTRING(CALLINGPARTYE164ADDRESS,-10,10),CALLINGPARTYE164ADDRESS)
    where
        callingNo is NULL
    ENDQUERY

    if (1) {
        if ($dbh->do($query)) {
            print "Successfully Updated the calling numbers call_detail_record.<br>";
        } else {
            print "ERROR: Failed to update calling numbers call_detail_record.<br>";
            print "Query: $query \n";
        }
    }
    ################################
    # Purge records with no data
    ################################
    $query = dequote(<< "    ENDQUERY");
    delete
    from
        call_detail_record
    where
        CALLINGPARTYE164ADDRESS like ''
        and
        CALLEDPARTYE164ADDRESS like ''
        and
        CALLINGPARTYIPADDRESS like ''
        and
        CALLEDPARTYIPADDRESS like ''
        and
        CALLSTARTTIMEINGRESSGWACCESS like ''

    ENDQUERY

    if (1) {
        if ($dbh->do($query)) {
            print "Successfully deleted empty records from call_detail_record.<br>";
        } else {
            print "ERROR: Failed to delete empty records from call_detail_record.<br>";
            print "Query: $query \n";
        }
    }
    ################################
    # Check for Duplicates
    ################################
    my %duplicates_hash;
    $query = dequote(<< "    ENDQUERY");
    select
        TEXTCALLIDENTIFIER,count(TEXTCALLIDENTIFIER) as cnt
    from
        call_detail_record
    group by
        TEXTCALLIDENTIFIER
    order by
        cnt desc
    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();
    my $count=0;
    while (my @tmp = $sth->fetchrow_array()) {
        if ($tmp[1] > 1) {
            # Use the has to record rows with duplicate call indentifiers and how many duplicates there are
            $duplicates_hash{$tmp[0]}=$tmp[1];
        }

    }

    my $dup_count=keys(%duplicates_hash);
    if ($dup_count) {
        print "WARNING: Found $dup_count duplicate records<br>";
    } else {
        print "Found no duplicate records<br>";
    }



    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "XFRQRY") {
    print $q->h2("Transfer Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"XFRRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
    # Start Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"start_date_selection", -size=>"12", -value=>""}))
    );
    # End Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date(yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"end_date_selection", -size=>"12", -value=>""}))
    );
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"store_selection", -size=>"12", -value=>""}))
    );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->p();

    standardfooter();
} elsif ("$report" eq "XFRRPT") {
    print $q->h2("Transfer Report");
	my $store_selection   = trim(($q->param("store_selection"))[0]);
    my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
    my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);

    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    my $query = "exec rspICAcctMonthyXferSkuDetail \@StoreId='$store_selection', \@StartDate='$start_date_selection', \@EndDate='$end_date_selection'";

    my $tbl_ref = execQuery_arrayref($dbh, $query);

    my %docNum_hash;
    my $tbl_ref2=[];
    if (recordCount($tbl_ref)) {
        # Since the query is in a stored procedure, it is not easy to change the sort.  The following
        # code is going to change the sort order to be the document number
        # get the DocNum
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            $docNum_hash{$$thisrow[3]}=1;
        }
        # Put the headers in to the new table reference
        push @$tbl_ref2, @$tbl_ref[0];
        foreach my $key (sort(keys(%docNum_hash))) {

            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
                my $doc=$$thisrow[3];
                if ($doc eq $key) {
                    push @$tbl_ref2, $thisrow;
                }
            }

        }

        printResult_arrayref2($tbl_ref2);
    } else {
        print $q->p("No data found");
    }

    standardfooter();
} elsif ("$report" eq "RECQRY") {
    print $q->h2("tblRec.tsv Report");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"RECRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));

    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"storenum_selection", -size=>"12", -value=>""}))
    );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "RECRPT") {
    print $q->h2("tblRecv Report");
    print "-- this report is still in progress --<br><br>";
    print $q->p("Orders in ****Data");
	my $storenum_selection   = trim(($q->param("storenum_selection"))[0]);
    my $report_selection   = trim(($q->param("report_selection"))[0]);

    my $continue=1;
    my @po_list;

    unless ($storenum_selection) {
        print $q->p("Please specify the storenumber");
        standardfooter();
        exit;
    }
    $query=dequote(<< "    ENDQUERY");
        select
            *
        from
            tblStoreOpenRecvDocs
        where
            StoreId = '$storenum_selection' and
            ((txnStatus in ('P', 'L', ' ') and documentNum > '0' and len(documentNum) <= 10 ) or
            (txnStatus = 'I' and documentNum = '0' and origin='S' and VendorNum='01' ) )
    ENDQUERY

    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        # get the poNumber
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            push(@po_list,$$thisrow[3]);
        }
        #my @links = ( '',"$scriptname?report=WIREORDRRPT2&report_selection=$report_selection&order_selection=".'$_' );
        #my @format = ('', {'align' => 'Right', 'num' => '123'});
        printResult_arrayref2($tbl_ref);#, \@links, \@format);
    } else {
        print $q->p("No data found");
        $continue=0;
    }
    $dbh->disconnect;

    # Check Navision
    foreach my $po (@po_list) {
        print $q->p("Info from Navision for po $po");
        $query=dequote(<< "        ENDQUERY");
            select
                [Entry No_],
                [Order No_],
                [Customer No_],
                [External Doc_ No_],
                [Status],
                [Order Date],
                [Date Time Entered],
                [Date Time Processed],
                [Comments],
                [Requested Ship Date]
            from
                [Villages\$Order Buffer Header]
            where
                [Customer No_]='$storenum_selection' and
                [External Doc_ No_] like '$po%'
            order by
                [Order Date] desc
        ENDQUERY
        my $dsn = "driver={SQL Server};Server=Abacus;database=****;uid=rpt;pwd=rpt;";
        my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');

        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            my @links = ( '',"$scriptname?report=WIREORDRRPT2&report_selection=$report_selection&order_selection=".'$_' );
            my @format = ('', {'align' => 'Right', 'num' => '123'});
            printResult_arrayref2($tbl_ref, \@links, );#\@format);
        } else {
            print $q->p("No data found");
            $continue=0;
        }
    }


    $dbh->disconnect;


	print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this report");
    standardfooter();
} elsif ("$report" eq "WQTOOL") {
    print $q->h2("Wire Database Query Tool");

    my $db_selection = "wire";
    # Build a form to browse tables
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"WTBLBROWS"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->input({-type=>"hidden", -name=>"db_selection", -value=>"$db_selection"});
    print "<br>";
    # Add the submit button
    print  $q->submit({-accesskey=>'S', -value=>"   Browse   "});

    print $q->end_form(), "\n";
    #print "<br><br>";
    # Build a form to query
    print $q->p("...Or, build a query");
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"WQTOOL2"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});



    #print "Database: ",$q->Select({-name=>"db_selection", -size=>"1"}, $db_options);
    print " Row Count: ",$q->input({ -type=>'text', -name=>"rcount_selection", -size=>"5", -value=>"10"});
    print " Vertical: ",$q->input({ -type=>'checkbox', -name=>"vert_selection",  -value=>"10"});
    print "<br>";
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}),
        $q->textarea({-name=>"querytext", -cols=>'60', -rows=>'15', -value=>"select * from node "}),

        );
    print "<br>";
    # Add the submit button
    print  $q->submit({-accesskey=>'S', -value=>"   Submit   "});

    print $q->end_form(), "\n";

    print $q->p();
    standardfooter();
} elsif ("$report" eq "WTBLBROWS") {
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;

    my $report_selection   = trim(($q->param("report_selection"))[0]);
    print $q->h2("Tables of the $db_selection database");
    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }
    my $tbl_ref;
    my @header=("Table Name");
    push (@$tbl_ref,\@header);
    $query = 'show tables';

    my $sth=$dbh->prepare("$query");
    $sth->execute();
    my $count=0;
    while (my @tmp = $sth->fetchrow_array()) {
        push (@$tbl_ref,\@tmp);
    }

    my $def_table=$$tbl_ref[1][0];
    if (recordCount($tbl_ref)) {
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }

    $dbh->disconnect;

    # Build a form to query
    print $q->p("Build a query of the $db_selection database:");
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"WQTOOL2"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->input({-type=>"hidden", -name=>"db_selection", -value=>"$db_selection"});

    print " Row Count: ",$q->input({ -type=>'text', -name=>"rcount_selection", -size=>"5", -value=>"10"});
    print " Vertical: ",$q->input({ -type=>'checkbox', -name=>"vert_selection",  -value=>"10"});
    print "<br>";
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}),
        $q->textarea({-name=>"querytext", -cols=>'60', -rows=>'15', -value=>"select * from $def_table "}),

        );
    print "<br>";
    # Add the submit button
    print  $q->submit({-accesskey=>'S', -value=>"   Submit   "});

    print $q->end_form(), "\n";
    print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this tool");
    standardfooter();
} elsif ("$report" eq "WQTOOL2") {
    my $host=$wirehost;
    my $user=$wireuser;
    my $pw=$wireuserpw;
    my $db_selection=$wire_db;
    my $report_selection   = trim(($q->param("report_selection"))[0]);
    my $rcount_selection   = trim(($q->param("rcount_selection"))[0]);
    my $querytext = trim(($q->param("querytext"))[0]);

    my $dbh=DBI->connect("DBI:mysql:host=$host;database=$db_selection","$user","$pw");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $host - Still need to add a remote user?");
        standardfooter();
        exit;
    }
    ###
    my @query_list=split(/\;/,$querytext);
    foreach my $subquery (@query_list) {
        unless ($rcount_selection eq "-1") {
            if ($subquery =~ /^select/i) {
                # If this is a select query and we have not specified we want to select all of the data...
                $subquery .= " limit $rcount_selection";
            }
        }

        my $tbl_ref = execQuery_arrayref($dbh, $subquery);

        if (recordCount($tbl_ref)) {
            print $q->p("$subquery");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }
    }
    ###


=pod
    my $tbl_ref = execQuery_arrayref($dbh, $querytext);
    unless ($rcount_selection eq "-1") {

        for my $datarows (0 .. $rcount_selection) {
            my $thisrow = @$tbl_ref[$datarows];
            push @$tbl_ref2, $thisrow;
        }
        $tbl_ref=$tbl_ref2;
    }

    if (recordCount($tbl_ref)) {
        print $q->p("$querytext");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }
=cut
    $dbh->disconnect;
    print $q->a({-href=>"$scriptname?report=$report_selection", -target=>'_top'}, "Return to this tool");
    standardfooter();
} elsif ("$report" eq "STOREQRY") {
    print $q->h2("Store Reports");
    ### Get the options:

 
    # Store Number
    my $store_options= $q->option({-value=>''}, "");
    $store_options.= $q->option({-value=>'all'}, "All");
	
	my $store_options2=$q->option({-value=>"All"}, "All Stores");
	$store_options2.=$q->option({-value=>"V1"}, "Company Stores");
	$store_options2.=$q->option({-value=>"V2"}, "Contract Stores");	
=pod
    $query = "select storeId,storeName from stores order by storeId";
    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
		 my $thisrow = @$tbl_ref_alias[$datarows];
		 my $number = trim($$thisrow[0]);
		 my $name = trim($$thisrow[1]);

		$store_options .= $q->option({-value=>$number}, "$number - $name");

    }
=cut
	###
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    #my $store_options= $q->option({-value=>'All'}, "All Stores");

    $query = "
    SELECT [StoreId]
      ,[StoreName]

    FROM [****Data].[dbo].[rtblStores]
    where EndDate is  NULL
	and [StoreId] not like '99%'
	";

    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias) {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $number = trim($$thisrow[0]);
        my $name = trim($$thisrow[1]);
		$name=~s/Villages-//;
        $store_options .= $q->option({-value=>"$number $name"}, "$number - $name");
		$store_options2 .= $q->option({-value=>"$number $name"}, "$number - $name");
    }
	$dbh->disconnect;
    $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $mysqld ");
        standardfooter();
        exit;
    }
	###
    #  Machine number
    my $model_options= $q->option({-value=>''}, "");
    $query = "select distinct(infoData) from storeInfo where codeid = 5";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $model = trim($$thisrow[0]);
        $model_options .= $q->option({-value=>$model}, "$model");
    }

    #  Tender
    my $tender_options= $q->option({-value=>''}, "");
    $query = "select distinct(infoData) from storeInfo where codeid = 10";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        $tender_options .= $q->option({-value=>$option}, "$option");
    }
    # Report options
    my $report_options;
    $report_options .= $q->option({-value=>1}, "1 - Store Info (Monitor)");
    $report_options .= $q->option({-value=>2}, "2 - Store Info (****Data)");
    # Traffic Sensor options
    my $sensor_options= $q->option({-value=>''}, "");
    $query = "select distinct(infoData) from storeInfo where codeid = 12";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        $sensor_options .= $q->option({-value=>$option}, "$option");
    }
    # Info options
    my $info_options= $q->option({-value=>''}, "");
    $query = "select codeId,codeDesc from codes order by codeDesc";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        my $option_desc = trim($$thisrow[1]);
        $info_options .= $q->option({-value=>$option}, "$option_desc");
    }
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STORERPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Info Reports")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection1", -size=>"1"}), $store_options)
              );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number: (Alt)"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"store_selection2", -size=>"12", -value=>""}))
    );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Report Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"rpt_selection", -size=>"1"}), $report_options)
              );
    ####
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->p();
	###
	#	Register Reports
	###
    $report_options= $q->option({-value=>''}, "");
    $report_options .= $q->option({-value=>1}, "Model");
    $report_options .= $q->option({-value=>2}, "Serial Number");	
	$report_options .= $q->option({-value=>3}, "Memory");	
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREREGRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Register Info Reports")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options2)
              );
 
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Report Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"rpt_selection", -size=>"1"}), $report_options)
              );
    ####
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->p();	
	###The second menu
    ####
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREMONITORRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Specific Store Info Report")));

    # Model options
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "System Model Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"model_selection", -size=>"1"}), $model_options)
              );
    # Tender options
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Tender:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"tender_selection", -size=>"1"}), $tender_options)
              );
    # Traffic Sensor Version
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Traffic Sensor Version:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"sensor_selection", -size=>"1"}), $sensor_options)

              );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    print $q->p();
	###
	###The third menu
    ####
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREINFORPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Specific Store Info Report II")));


    # Other Values
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Select Specific Info:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"info_selection", -size=>"1"}), $info_options)

              );
    ###
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"cb_company", -value=>"0", -checked=>"1"}) .
               $q->font({-size=>2}, "Show Company & Contract Stores")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"cb_company", -value=>"1",}) .
               $q->font({-size=>2}, "Show only Company Stores")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
               $q->input({-type=>"radio", -name=>"cb_company", -value=>"2",}) .
               $q->font({-size=>2}, "Show only Contract Stores")));
 
    print $q->Tr(
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Include Inactive/Closed Stores:"),
				$q->td({-class=>'tabledatanb', -align=>'left'},
				$q->input({ -type=>'checkbox', -name=>"cb_inactive_checked",   -value=>"1"  })));   
    ###

    #print $q->Tr(
    #    $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
    #    $q->input({  -type=>"checkbox", -name=>"cb_company"}) .
    #    $q->font({-size=>-1}, "Company Stores Only ")
    #    ));
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
	###
    # The fourth menu
    $report_options= $q->option({-value=>''}, "");
    $query = "
        select
            distinct(ss.codeId),
            c.codeDesc
        from
            storeStats ss
        join
            codes c on ss.codeId = c.codeId
        order by
            ss.codeId
            ";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        my $stat_desc = $$thisrow[1];
        $report_options .= $q->option({-value=>$option}, "$option - $stat_desc");
    }
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STORESTATSRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Stats Reports")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection1", -size=>"1"}), $store_options)
              );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number: (Alt)"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"store_selection2", -size=>"12", -value=>""}))
    );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Report Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"rpt_selection", -size=>"1"}), $report_options)
              );
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );			  
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();			  
    ####
	###
    # The fifth menu - System Inventory
    $report_options= $q->option({-value=>''}, "");
    $query = "
        select
            distinct(ss.codeId),
            c.codeDesc
        from
            storeStats ss
        join
            codes c on ss.codeId = c.codeId
        order by
            ss.codeId
            ";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        my $stat_desc = $$thisrow[1];
        $report_options .= $q->option({-value=>$option}, "$option - $stat_desc");
    }
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    #print $q->input({-type=>"hidden", -name=>"report", -value=>"SYSTEMINVRPT"});
	print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREINFO"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("System Inventory Reports")));
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Selection:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection1", -size=>"1"}), $store_options)
              );
 
 
    ####	
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
	

    print $q->p();
    print $q->p();
    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "STOREMONITORQRY") {
    print $q->h2("Store Monitor Info Query");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREMONITORRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Selection")));

    ####
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $mysqld ");
        standardfooter();
        exit;
    }

    my $store_options= $q->option({-value=>''}, "");
    $query = "select storeId,storeName from stores order by storeId";
    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
     my $thisrow = @$tbl_ref_alias[$datarows];
     my $number = trim($$thisrow[0]);
     my $name = trim($$thisrow[1]);

    $store_options .= $q->option({-value=>$number}, "$number - $name");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection1", -size=>"1"}), $store_options)
              );
    ####
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number: (Alt)"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"store_selection2", -size=>"12", -value=>""}))
    );

    #  Machine number
    $store_options= $q->option({-value=>''}, "");
    $query = "select distinct(infoData) from storeInfo where codeid = 3";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $model = trim($$thisrow[0]);
        $store_options .= $q->option({-value=>$model}, "$model");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "System Model Number:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"model_selection", -size=>"1"}), $store_options)
              );
    #  Tender
    $store_options= $q->option({-value=>''}, "");
    $query = "select distinct(infoData) from storeInfo where codeid = 7";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        $store_options .= $q->option({-value=>$option}, "$option");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Tender:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"tender_selection", -size=>"1"}), $store_options)
              );

    # Traffic Sensor Version
    $store_options= $q->option({-value=>''}, "");
    $query = "select distinct(infoData) from storeInfo where codeid = 12";
    $tbl_ref_alias = execQuery_arrayref($dbh, $query);
    for my $datarows (1 .. $#$tbl_ref_alias)
    {
        my $thisrow = @$tbl_ref_alias[$datarows];
        my $option = trim($$thisrow[0]);
        $store_options .= $q->option({-value=>$option}, "$option");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Traffic Sensor Version:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"sensor_selection", -size=>"1"}), $store_options)
              );
    ####
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "STOREMONITORRPT") {
    print $q->h2("Store Monitor Info Report");
    my $store_selection1   = trim(($q->param("store_selection1"))[0]);
    my $store_selection2   = trim(($q->param("store_selection2"))[0]);

    my $store_where;
    my $store_selection;
    if ($store_selection1) {
        $store_selection=$store_selection1;
    } elsif ($store_selection2) {
       $store_selection=$store_selection2;
    }
    if ($store_selection) {
        $store_where=" and si.storeId = $store_selection";
    }
    my $model_selection   = trim(($q->param("model_selection"))[0]);
    my $model_where;
    my $model_join;
    if ($model_selection) {
        $model_where = " and c3.codeId = 3 and infoData like '%$model_selection'";
        $model_join = " left outer join codes c3 on c3.codeId=3 ";
    }
    my $tender_selection   = trim(($q->param("tender_selection"))[0]);
    my $tender_where;
    my $tender_join;
    if ($tender_selection) {
        $tender_where = " and c7.codeId = 7 and infoData like '$tender_selection'";
        $tender_join = " left outer join codes c7 on c7.codeId=7 ";
    }
    my $sensor_selection   = trim(($q->param("sensor_selection"))[0]);
    my $sensor_where;
    my $sensor_join;
    if ($sensor_selection) {
        $sensor_where = " and c12.codeId = 12 and infoData like '$sensor_selection'";
        $sensor_join = " left outer join codes c12 on c12.codeId=12 ";
    }



    # Todo - Probably want a substantially different report showing just store info.  You can then query by model, tender, etc.

    my $dsn;
    my $dbh;
    my $query;
    my $sth;
    my $tbl_ref;
    my $rpt_label;

    $rpt_label="Connectivity Stats";
    $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $mysqld ");
        standardfooter();
        exit;
    }
    $query = "select storeName from stores where storeId = $store_selection";
    $sth=$dbh->prepare("$query");
    $sth->execute();
    my $storename;
    while (my @tmp = $sth->fetchrow_array()) {
        $storename=$tmp[0];
    }
    $query = "
    select
        distinct(si.storeId),
        s.storeName,
        s.status
    from
        storeInfo si
        join stores s on s.storeId = si.storeId
        join codes c on c.codeId = si.codeId
        $model_join
        $tender_join
        $sensor_join
    where
        1 = 1
        $store_where
        $model_where
        $tender_where
        $sensor_where

        and c.codeId > 1
    order by
        si.storeId
    ";

    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        if ($store_selection) {
            print $q->h3("Store: $store_selection - $storename");
        }
        if ($model_selection) {
            print $q->h3("Model: $model_selection");
        }
        if ($tender_selection) {
            print $q->h3("Tender: $tender_selection");
        }
        if ($sensor_selection) {
            print $q->h3("Traffic Sensor Version: $sensor_selection");
        }
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }

    if ($store_selection) {
        $query = "
        select
            si.date,
            si.infoData as \"No Internet Counts\"
        from
            storeInfo si
            join codes c on c.codeId = si.codeId
        where
            si.storeId = $store_selection
            and c.codeId = 1
        order by si.date

        ";

        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            print $q->h3("$rpt_label");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }
    }
    $dbh->disconnect;

    standardfooter();

} elsif ("$report" eq "SYSTEMINVRPT") {
    print $q->h2("System Inventory Info Report");
    my $store_selection   = trim(($q->param("store_selection1"))[0]);
	if ($store_selection eq "all") {
		print $q->p("Sorry:  Showing all stores is not yet supported");
		standardfooter();
		exit;
	}
	#print $q->p("Store Selection: $store_selection");
	my $store_where;
	my $storename;
    if ($store_selection) {
		my @tmp=split(/\s+/,$store_selection);
		$store_selection=$tmp[0];
		$storename = $tmp[1];
        $store_where="si.storeId = $store_selection";
    }

    my $dsn;
    my $dbh;
    my $query;
    my $sth;
    my $tbl_ref;
    my $rpt_label;
 
    $rpt_label="System Inventory";
	# Connect to the database
    $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $mysqld ");
        standardfooter();
        exit;
    }
 
	# Determine the store number
    $query = "select storeName from stores where storeId = $store_selection";
	#print $q->pre($query);
    $sth=$dbh->prepare("$query");
    $sth->execute();
     
    while (my @tmp = $sth->fetchrow_array()) {
        $storename=$tmp[0];
    }
	 
	# Determine the Model, Serial Number, and Memory codes
    $query = "
		select 
			codeId, 
			codeDesc 
		from
			codes
		where 
			codeDesc like '%model%'
			or
			codeDesc like '%serialnumber%'
			or
			codeDesc like '%memory%'			
	";
	#print $q->pre($query);
    $sth=$dbh->prepare("$query");
 
    $sth->execute();
 
  
	my %edna_hash;
	my %reg_hash;
	my %workstation_hash;
	
    while (my @tmp = $sth->fetchrow_array()) {
		my $code=$tmp[0];
		my $desc=$tmp[1];
		if ($desc =~ /^reg/i) {
			$reg_hash{$desc}=$code;
		} elsif ($desc =~ /^workstation/i) {
			$workstation_hash{$desc}=$code;
		} elsif ($desc =~ /^UPS/i) {			
			# Ignore this one
		} else {
			$edna_hash{$desc}=$code;
		}      
    }	
	
	
	# Get all of the store info for this store
	my @header=(
		"System",
		"Model",
		"SerialNo",
		"Memory"
	);
	my @array_to_print=\@header;	
    my %info_hash;
    $query = "
    select
        codeId,        
		infoData
    from
        storeInfo 
 
    where
		storeId = $store_selection
 
 
    ";
	#print $q->pre("$query");
	
    $sth=$dbh->prepare("$query");
 
    $sth->execute();
 
	
    while (my @tmp = $sth->fetchrow_array()) {
		my $code=$tmp[0];
		my $info=$tmp[1];
		$info_hash{$code}=$info;
	}
	# Construct the Edna info
	my $system="Edna";
	my $model;
	my $serialno;
	my $memory;
	foreach my $desc (sort keys(%edna_hash)) {		
		if ($desc =~ /model/i) {			 
			$model=$info_hash{$edna_hash{$desc}};
		}
		if ($desc =~ /serialnumber/i) {			 
			$serialno=$info_hash{$edna_hash{$desc}};
		}
		if ($desc =~ /memory/i) {			 
			$memory=$info_hash{$edna_hash{$desc}};
		}		
	}
	my @array=("$system","$model","$serialno","$memory");
	push(@array_to_print,\@array);
	
	# Construct the Register info
	foreach my $reg ("reg1","reg2","reg3","reg4") {
		my $system="$reg";
		my $model;
		my $serialno;
		my $memory;
		foreach my $desc (sort keys(%reg_hash)) {		
			if ($desc =~ /$reg.*model/i) {			 
				$model=$info_hash{$reg_hash{$desc}};
			}
			if ($desc =~ /$reg.*serialnumber/i) {			 
				$serialno=$info_hash{$reg_hash{$desc}};
			}
			if ($desc =~ /$reg.*memory/i) {			 
				$memory=$info_hash{$reg_hash{$desc}};
			}		
		}
		my @array=("$system","$model","$serialno","$memory");
		if ($model) {
			push(@array_to_print,\@array);	
		}
	}
	# Construct the Workstation info
	 
	$system="Workstation";
 
 
	foreach my $desc (sort keys(%workstation_hash)) {
	 
		if ($desc =~ /workstation.*model/i) {			 
			$model=$info_hash{$workstation_hash{$desc}};		 
		}
		if ($desc =~ /workstation.*serialnumber/i) {			 
			$serialno=$info_hash{$workstation_hash{$desc}};
		}
		if ($desc =~ /workstation.*memory/i) {			 
			$memory=$info_hash{$workstation_hash{$desc}};
		}		
	}
	@array=("$system","$model","$serialno","$memory");
	if ($model) {
		push(@array_to_print,\@array);	
	}
 

 
    if (recordCount(\@array_to_print)) {
        if ($store_selection) {
            print $q->h3("Store: $store_selection - $storename");
        }
 
        printResult_arrayref2(\@array_to_print);
    } else {
        print $q->b("no records found<br>\n");
    }
 
    $dbh->disconnect;

    standardfooter();
} elsif ("$report" eq "STORESTATSRPT") {
    print $q->h2("Store Monitor Status Report");
    my $rpt_selection   = trim(($q->param("rpt_selection"))[0]);
    my $store_selection1   = trim(($q->param("store_selection1"))[0]);
    my $store_selection2   = trim(($q->param("store_selection2"))[0]);
    my $storeId;

    if ($store_selection1) {
        $storeId=$store_selection1;
    }
    if ($store_selection2) {
        $storeId=$store_selection2;
    }
	my $storewhere=" and si.storeId = '$storeId'";
	if ($store_selection1 eq "all") {
		$storewhere='';
	}
	unless ($rpt_selection) {
		print $q->p("Error: No report selected");
		standardfooter();
		exit;
	}

    my $dsn;
    my $dbh;
    my $query;
    my $sth;
    my $tbl_ref;
    my $rpt_label;
    my %info_hash;

    $rpt_label="Connectivity Stats";
    $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $mysqld ");
        standardfooter();
        exit;
    }

    $query = "
    select

        s.storeName,
		si.storeId,
        s.status,
        c.codeDesc,
        si.infodata,
        si.infodata2,
        si.date
    from
        storeStats si
        join stores s on s.storeId = si.storeId
        join codes c on c.codeId = si.codeId

    where
        1 = 1

	$storewhere
    and
        c.codeId = '$rpt_selection'
	and
		si.infodata not like ''

    order by
        s.storeName,si.date desc
    ";

	#print $q->pre("$query");
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }

    $dbh->disconnect;
    standardfooter();

} elsif ("$report" eq "STOREINFORPT") {
    print $q->h2("Store Monitor Info Report III");
    my $info_selection   = trim(($q->param("info_selection"))[0]);
    my $cb_company   = trim(($q->param("cb_company"))[0]);
    my $where;
    if ($cb_company == 1) {
        $where = "and s.status = 'Company'"
    }
    if ($cb_company == 2) {
        $where = "and s.status = 'Contract'"
    }
	my $cb_inactive_checked   = trim(($q->param("cb_inactive_checked"))[0]);
	
    my $dsn;
    my $dbh;
    my $query;
    my $sth;
    my $tbl_ref;
    my $rpt_label;
    my %info_hash;
	my %active_store_hash=();
 
	unless ($cb_inactive_checked) {
 
		# Get a list of the active stores		
		my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');		
 
		$query = "
		SELECT [StoreId]
		  ,[StoreName]

		FROM [****Data].[dbo].[rtblStores]
		where EndDate is  NULL
		and [StoreId] not like '99%'
		and [StoreId] not like '98%'
		";		
		$sth=$dbh->prepare("$query");
		$sth->execute();
		my $storename;
		while (my @tmp = $sth->fetchrow_array()) {
			my $StoreId=$tmp[0];
			$active_store_hash{$StoreId}=1;			
		}
			
		$dbh->disconnect;
	}

    $rpt_label="Connectivity Stats";
    $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error - Cannot connect to $mysqld ");
        standardfooter();
        exit;
    }

    $query = "
    select
        distinct(si.storeId),
        s.storeName,
        s.status,
        c.codeDesc,
        si.infodata,
        si.infodata2,
        si.date
    from
        storeInfo si
        join stores s on s.storeId = si.storeId
        join codes c on c.codeId = si.codeId

    where
        1 = 1
    $where

    and c.codeId = '$info_selection'
    group by si.storeId
    order by
        si.storeId
    ";

	#print $q->pre("$query");
    $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
		my $tbl_ref2 = [];
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
			my $storeId=$$thisrow[0];
			unless ($cb_inactive_checked) {				
				# Check that this is an active store
				# because that is the only ones we
				# want to display.
				if ($active_store_hash{$storeId}) {
					# This is an active store and we
					# want to display it
					push $tbl_ref2, $thisrow;					
					my $info=$$thisrow[4];
					$info_hash{$info}++;
				}
			} else {
				push $tbl_ref2, $thisrow;
				my $info=$$thisrow[4];
				$info_hash{$info}++;
			}

        }
        printResult_arrayref2($tbl_ref2);
        print $q->p();
        if (keys(%info_hash)) {
            print $q->h3("Results:");
            my $total=0;
            foreach my $info (sort(keys(%info_hash))) {
                $total+=$info_hash{$info};
            }
            foreach my $info (sort(keys(%info_hash))) {
                my $percent = ($total == 0) ? 0 : nearest(.01, $info_hash{$info} / $total * 100);
                print "$info - $info_hash{$info} - ${percent}%<br>";
            }
        }

    } else {
        print $q->b("no records found<br>\n");
    }


    $dbh->disconnect;

    standardfooter();
} elsif ("$report" eq "STORERPT") {
    print $q->h2("Store Info Report");
    my $store_selection1   = trim(($q->param("store_selection1"))[0]);
    my $store_selection2   = trim(($q->param("store_selection2"))[0]);
    my $sortfield = trim(($q->param("sort"))[0]);

    my $store_selection;
    if ($store_selection1) {
        $store_selection=$store_selection1;
    } elsif ($store_selection2) {
       $store_selection=$store_selection2;
    }

    my $model_selection   = trim(($q->param("model_selection"))[0]);
    my $model_where;
    # Todo - Probably want a substantially different report showing just store info.  You can then query by model, tender, etc.
    my $rpt_selection   = trim(($q->param("rpt_selection"))[0]);
    if ($store_selection =~ /all/i) {
        $rpt_selection = 3;
    }

	my $storename;
	my @tmp=split(/\s+/,$store_selection);
	$store_selection=$tmp[0];
	$storename=$tmp[1];

    my $dsn;
    my $dbh;
    my $query;
    my $sth;
    my $tbl_ref;
    my $rpt_label;
    if ($rpt_selection == 1) {
        $rpt_label="Connectivity Stats";
        $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

        unless ($dbh) {
            print $q->p("Error - Cannot connect to $mysqld ");
            standardfooter();
            exit;
        }
=pod
        $query = "select storeName from stores where storeId = $store_selection";
        $sth=$dbh->prepare("$query");
        $sth->execute();
        my $storename;
        while (my @tmp = $sth->fetchrow_array()) {
            $storename=$tmp[0];
        }
=cut
        $query = "
        select
            c.codeDesc,
            si.infoData
        from
            storeInfo si
            join codes c on c.codeId = si.codeId
        where
            si.storeId = $store_selection
            and c.codeId > 1

        ";
        $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {

            print $q->h3("Store: $store_selection - $storename");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }

        $query = "
        select
            si.date,
            si.infoData as \"No Internet Counts\"
        from
            connectInfo si
            join codes c on c.codeId = si.codeId
        where
            si.storeId = $store_selection
            and c.codeId = 1
        order by si.date desc
        limit 100

        ";
        $query = "
        select
            ss.date,
            ss.infoData as \"No Internet Counts\"
        from
            storeStats ss
            join codes c on c.codeId = ss.codeId
        where
            ss.storeId = $store_selection
            and c.codeId = 1
        order by ss.date desc
        limit 100

        ";
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            print $q->h3("$rpt_label");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }
        $query = "
        select
            ss.date,
            ss.infoData as \"Throughput\"
        from
            storeStats ss
            join codes c on c.codeId = ss.codeId
        where
            ss.storeId = $store_selection
            and c.codeDesc = 'Throughput'
        order by ss.date desc
        limit 100
        ";

        $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            print $q->h3("Throughput - bps");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }
    } elsif ($rpt_selection == 2) {
        $rpt_label="****data Store Info for $store_selection";
        $dsn = "driver={SQL Server};Server=****;database=****Data;uid=rpt;pwd=rpt;";
        $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
        my $versiondate;
        $query = "
           select
              max(VersionDate) as versiondate
           from
              rtblStoreVersionHist
           where
              versionDate < getdate()
           and
              StoreId = '$store_selection'
           group by
              StoreId

            ";

        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            $versiondate=$tmp[0];
        }

        my $version;
        $query="
           select
              a.Version
           from
              rtblStoreVersionHist a

          where
              a.StoreId = '$store_selection'
          and
              a.VersionDate = '$versiondate'
          ";

        $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            $version=$tmp[0];
        }

        $query="

           select
              s.StoreId, s.StoreName, s.StoreCode, s.State, s.PhoneVoice, '' as PollingNum, s.StoreHours,
              '$version' as XPSVer, s.PriVendor, s.TenderParms, s.StartDate, s.EndDate, s.email
           from
              rtblStores s
           where
              s.StoreId = '$store_selection'
        ";
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            print $q->h3("$rpt_label");
            printResult_arrayref2($tbl_ref);
        } else {
            print $q->b("no records found<br>\n");
        }
    } elsif ($rpt_selection == 3) {
        $sortfield = "s.storename" unless $sortfield;

        $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

        my @code_array;
        # Get the codes
        $query = "select codeid from codes where codeid > 1";
        $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            push(@code_array,$tmp[0]);
        }

        print $q->h3("Store: All Stores");
        foreach my $codeId (@code_array) {

            $query = "
            select
                si.storeid as StoreId,
                s.storename as StoreName,
                s.status as Status,
                c.codeDesc as Description,
                si.infoData as Data
            from storeInfo si
                join stores s on si.storeid=s.storeid
                join codes c on si.codeid=c.codeid
            where
                c.codeid = '$codeId'
            order by
                $sortfield
            ";

            $tbl_ref = execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref)) {
                print "<br>";
                print $q->b("records found for codeId $codeId<br>");
                my $newsortURL = "$scriptname?report=STORERPT&store_selection1=$store_selection1&store_selection2=$store_selection2&sort=\$_";
                my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
                printResult_arrayref2($tbl_ref, '', '', \@headerlinks);
            } else {
                print $q->b("no records found for codeId $codeId<br>");
            }
        }
        $dbh->disconnect;

        standardfooter();
        exit;
    }

    $dbh->disconnect;

    standardfooter();
	
	
	} elsif ("$report" eq "STOREREGRPT") {
    print $q->h2("Store Register Info Report");
    my $store_selection   = trim(($q->param("store_selection"))[0]);
    my $rpt_selection   = trim(($q->param("rpt_selection"))[0]);	 
    my $sortfield = trim(($q->param("sort"))[0]);
	unless ($sortfield) {
		$sortfield = "StoreId";
	}
	my $storeWhere='';
	my $codeDesc;
	my $label = '';
	
	if ($rpt_selection == 1) {
		# Model Number
		$codeDesc = "reg%Model%";
		$label = "Register Models";
	} elsif ($rpt_selection == 2) {
		# Serial Number
		$codeDesc = "reg%SerialNumber";
		$label = "Register Serial Numbers";
	} elsif ($rpt_selection == 3) {
		# Memory
		$codeDesc = "reg%TotalPhyMemory";
		$label = "Register Physical Memory";
	} else {
		print $q->p("Error - No Report Selected ");
		standardfooter();
		exit;	
	}
 
	# Determine the store selection
 
	if ($store_selection) {
		if ($store_selection eq "All") {
			print $q->h2 ("Data for All stores");
		} elsif ($store_selection eq "V1") {		
			print $q->h2 ("Data for $store_selection");
			my @tmp=split(/\s+/,$store_selection);
			my $storeid = $tmp[0];
			$storeWhere = "and s.status = 'company'";
		} elsif ($store_selection eq "V2") {		
			print $q->h2 ("Data for $store_selection");
			my @tmp=split(/\s+/,$store_selection);
			my $storeid = $tmp[0];
			$storeWhere = "and s.status = 'contract'";
		} else {
			print $q->h2 ("Data for $store_selection");
			my @tmp=split(/\s+/,$store_selection);
			my $storeid = $tmp[0];
			$storeWhere = "and si.storeid = $storeid";		
		}		 
	}
 
    my $dsn;
    my $dbh;
    my $query;
    my $sth;
    my $tbl_ref;
    my $rpt_label;
 
	$dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

	unless ($dbh) {
		print $q->p("Error - Cannot connect to $mysqld ");
		standardfooter();
		exit;
	}

	$query = "
	select
		si.storeid as StoreId,
		s.storename as StoreName,
		s.status as Status,
		c.codeDesc as Description,
		si.infoData as Data
	from storeInfo si
		join stores s on si.storeid=s.storeid
		join codes c on si.codeid=c.codeid
	where
		c.codeDesc like '$codeDesc'
		$storeWhere
	order by
		$sortfield
	";

	#print $q->pre($query);
	my %company_hash=();
	my %contract_hash=();
	my %stats_hash=();
	$tbl_ref = execQuery_arrayref($dbh, $query);
	if (recordCount($tbl_ref)) {
		print "<br>";
		print $q->b("records found for $label<br>");
		my $newsortURL = "$scriptname?report=STOREREGRPT&store_selection=$store_selection&sort=\$_";
		my @headerlinks = ($newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL);
		printResult_arrayref2($tbl_ref, '', '', \@headerlinks);
 
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $status = trim($$thisrow[2]);
			my $value = trim($$thisrow[4]);
			if ($value eq "2,038MB") {
				$value = "2,039MB";
			}
			if ($value eq "502MB") {
				$value = "503MB";
			}			
			$stats_hash{$value}++; 
		}		
		if (($store_selection eq "all") && ($rpt_selection != 2))  {
			print $q->h3("results summary");
			my $total=0;
			foreach my $key (sort keys(%stats_hash)) {
				$total += $stats_hash{$key};
			}
			foreach my $key (sort keys(%stats_hash)) {
				my $count = $stats_hash{$key};
				my $percent = sprintf("%.2f",(($count / $total) * 100));
				print "$key - Count: $count - $percent%<br />";
 
			}			
			
		}
	 
	} else {
		print $q->b("no records found for $label<br>");
	}

    $dbh->disconnect;

    standardfooter();
	
	
} elsif ("$report" eq "STORELIST") {
    print $q->h2("Store List");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STORELIST2"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
 
	print "Select List Type:", $q->radio_group(-name=>'list',
				  -values=>["STORENUM","STORENAME"],
				  -default=>"STORENUM",
				  -labels=>"LIST"), "<br><br>\n";

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({  -type=>"checkbox", -name=>"cb_detailed"}) .
        $q->font({-size=>-1}, "Detailed Report ")
        ));
    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
 
    print $q->p();

    standardfooter();	
} elsif ("$report" eq "STORELIST2") {
    print $q->h2("Store List Report");

    my $list_selection   = trim(($q->param("list"))[0]);	
	my $cb_detailed   = trim(($q->param("cb_detailed"))[0]);	
 
	print $q->p("List: $list_selection");
	
 
	# Get the list of stores from Navision
	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
	my %company_hash;
	my %contract_hash;
	
	my $order_by = "Order By 2";
	if ($list_selection eq "STORENAME") {
		$order_by = "Order By 1";
	}
	if ($cb_detailed) {
		$query = "
		Select distinct
		Name =
		 Case 
		   when u.[Name] = 'Ten Thousand Villages at'
			 then 'Ten Thousand Villages-Kitchen Kettle'		 
		   when u.[Name] = 'Ten Thousand Villages-Fredericksbur'
			 then 'Ten Thousand Villages-Fredericksburg'
		   when u.[Name] = 'Ten Thousand Villages-KingofPrussia'
			 then 'Ten Thousand Villages-King of Prussia'
		   when u.[Name] = 'Ten Thousand Villages-Perimeter Pl'
			 then 'Ten Thousand Villages-Perimeter Place'
		   when u.[Name] = 'Ten Thousand Villages-SaltLake City'
			 then 'Ten Thousand Villages-Salt Lake City'
		   when u.[Name] = 'Ten Thousand Villages-WHartford'
			 then 'Ten Thousand Villages-West Hartford'
		   when u.[Name] = 'Ten Thousand Villages-Charlottesvil'
			 then 'Ten Thousand Villages-Charlottesville'
		   when u.[Name] = 'Ten Thousand Villages-Richmond DWTN'
			 then 'Ten Thousand Villages-Richmond Downtown'
		   else u.[Name]
		 End,
		u.[No_], 
		Manager = c.[First Name] + ' ' + c.[Middle Name] + ' ' + c.[Surname],

		u.[Address],
		u.[Address 2],
		u.[City],
		State = u.[County],
		Telephone = u.[Phone No_],
		u.[Fax No_],
		c.[E-Mail], 
		c.[Organizational Level Code],
		o.[Description] as JobTitle,
		u.[POS Type],
		u.[Territory Code],
		u.[Salesperson Code],
		u.[Post Code],
		u.[Customer Posting Group],
		u.[Home Page],
		u.[Salesperson Code]

		FROM 
		[Villages\$Customer] u left outer join 
			[Villages\$Contact Business Relation] cbr on u.[No_]=cbr.[No_] left outer join
			[Villages\$Contact] c on cbr.[Contact No_]=c.[Company No_] left outer join 
		[Villages\$Organizational Level] o on c.[Organizational Level Code] = o.[Code]
		Where 

		c.Type=1 and  -- 0=company, 1=person
		c.[Company No_] <> 'ECOMMERCE' and 
		u.[Deactive Date] = '1753-01-01' and
		u.[Customer Posting Group] in ('COMPANY', 'CONTRACT') and 
		u.[Store Opening] < getdate() and 
		upper(u.[Name 2]) <> 'TENT SALE' and
		c.[Organizational Level Code] = 'MGR'
		$order_by
		";	
	} else {
		$query = "
		Select distinct
		Name =
		 Case 
		   when u.[Name] = 'Ten Thousand Villages at'
			 then 'Ten Thousand Villages-Kitchen Kettle'			 
		   when u.[Name] = 'Ten Thousand Villages-Fredericksbur'
			 then 'Ten Thousand Villages-Fredericksburg'
		   when u.[Name] = 'Ten Thousand Villages-KingofPrussia'
			 then 'Ten Thousand Villages-King of Prussia'
		   when u.[Name] = 'Ten Thousand Villages-Perimeter Pl'
			 then 'Ten Thousand Villages-Perimeter Place'
		   when u.[Name] = 'Ten Thousand Villages-SaltLake City'
			 then 'Ten Thousand Villages-Salt Lake City'
		   when u.[Name] = 'Ten Thousand Villages-WHartford'
			 then 'Ten Thousand Villages-West Hartford'
		   when u.[Name] = 'Ten Thousand Villages-Charlottesvil'
			 then 'Ten Thousand Villages-Charlottesville'
		   when u.[Name] = 'Ten Thousand Villages-Richmond DWTN'
			 then 'Ten Thousand Villages-Richmond Downtown'
		   else u.[Name]
		 End,
		u.[No_], 
		Manager = c.[First Name] + ' ' + c.[Middle Name] + ' ' + c.[Surname], 
		Telephone = u.[Phone No_],  
		u.[Salesperson Code] as 'RSM', 
		u.[Customer Posting Group] as 'Status'
 

		FROM 
		[Villages\$Customer] u left outer join 
			[Villages\$Contact Business Relation] cbr on u.[No_]=cbr.[No_] left outer join
			[Villages\$Contact] c on cbr.[Contact No_]=c.[Company No_] left outer join 
		[Villages\$Organizational Level] o on c.[Organizational Level Code] = o.[Code]
		Where 

		c.Type=1 and  -- 0=company, 1=person
		c.[Company No_] <> 'ECOMMERCE' and 
		u.[Deactive Date] = '1753-01-01' and
		u.[Customer Posting Group] in ('COMPANY', 'CONTRACT') and 
		u.[Store Opening] < getdate() and 
		upper(u.[Name 2]) <> 'TENT SALE' and
		c.[Organizational Level Code] = 'MGR'
		$order_by
		";	
	
	}
	
	#print $q->pre("$query");
 
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
		#my @header=$$tbl_ref[0];
		my @header;
		my @array_to_print;
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];			 
			my $name = trim($$thisrow[0]);
			# Trim off the "Villages-" from the name
			$name=~ s/Ten Thousand Villages-//;
			# Fix Kitchen Kettle
			my $storenum = trim($$thisrow[1]);
			if ($storenum == 5263) {
				if ($name eq "Ten Thousand Villages at") {
					$name="Kitchen Kettle";
				}
			}
			$$thisrow[0]=$name; 
			push(@array_to_print,$thisrow)
		 
		}
        print $q->h3("Store List: By $list_selection");
		
		if ($cb_detailed) {					 			
			printResult_arrayref2($tbl_ref);
		} else {
			my @links = ('',"$scriptname?report=STOREINFO&store_selection=".'$_' );
			printResult_arrayref_color($tbl_ref, \@links);
		}
    } else {
        print $q->b("no records found<br>\n");
    }
 	


    $dbh->disconnect;	
	standardfooter();
} elsif ("$report" eq "STOREINFO") {
    print $q->h2("Store Info");	
    my $store_selection   = trim(($q->param("store_selection"))[0]);
    my $store_selection1   = trim(($q->param("store_selection1"))[0]);	
    if ($store_selection1) {
		my @tmp=split(/\s+/,$store_selection1); 
		$store_selection=$tmp[0];
    }	
	
	# Store Selection is the store number
	     
	my ($ref,$ref2,$ref3, $ref4, $ref5, $ref6)  = collect_info_from_store($store_selection);
	# Store Info	
	my %store_info_hash = %$ref;	
	my %store_info_hash2 = %$ref2;		
	my %store_info_hash3 = %$ref3;	
	my %store_info_hash4 = %$ref4;	
	my %store_info_hash5 = %$ref5;
	my %store_info_hash6 = %$ref6;	# VeriFone info
	
	my @header = ("Name","StoreID","Manager","Telephone","RSM","Status");
	if (defined($store_info_hash{'TentSaleID'})) {
		@header = ("Name","StoreID","Manager","Telephone","RSM","Status","TentSaleID");
	}
	my @array_to_print = \@header;	
	my @array=("$store_info_hash{'name'}","$store_selection","$store_info_hash{'manager'}",
	"$store_info_hash{'telephone'}","$store_info_hash{'rsm'}","$store_info_hash{'status'}");
	if (defined($store_info_hash{'TentSaleID'})) {
		@header = ("Name","StoreID","Manager","Telephone","RSM","Status","TentSaleID");
		@array=("$store_info_hash{'name'}","$store_selection","$store_info_hash{'manager'}",
		"$store_info_hash{'telephone'}","$store_info_hash{'rsm'}","$store_info_hash{'status'}",
		"$store_info_hash{'TentSaleID'}");		
	}	
	push(@array_to_print,\@array);
	printResult_arrayref2(\@array_to_print);
	
	# Transnet / Paymentech Info
	@header=();	
	@array_to_print=();
	@array=();
	#print $q->h3("$store_info_hash{'name'} $store_selection Manager:$store_info_hash{'manager'}   $store_info_hash{'telephone'}   RSM: $store_info_hash{'rsm'}    $store_info_hash{'status'}");
	foreach my $name (sort keys(%store_info_hash3)) {		
		push(@header,$name);
		push(@array,$store_info_hash3{$name});
		@array_to_print = \@header;
		push(@array_to_print,\@array);
	}

	if ($#array_to_print > 0) {
		printResult_arrayref2(\@array_to_print);
	} else {
		print "No Transnet Paymentech Info found<br /><br />";
	}

	# Edna info
	@header=();	
	@array_to_print=();
	@array=();
	#print $q->h3("$store_info_hash{'name'} $store_selection Manager:$store_info_hash{'manager'}   $store_info_hash{'telephone'}   RSM: $store_info_hash{'rsm'}    $store_info_hash{'status'}");
	foreach my $name (sort keys(%store_info_hash2)) {
		unless (($name =~ /^reg/i) || ($name =~ /^WorkStation/)) {
			push(@header,$name);
			push(@array,$store_info_hash2{$name});
			#print $q->b("$name - $store_info_hash2{$name}");
		}
		@array_to_print = \@header;
		push(@array_to_print,\@array);
	}
	if ($#array_to_print > 0) {
		printResult_arrayref2(\@array_to_print);
	} else {
		print "No Edna Info found<br /><br />";
	}
	
	# Register Info
	my $register_info=0;
	for my $index (1...6) {		
		@header=();	
		@array_to_print=();
		@array=();	
		my $counter = 0;
		foreach my $name (sort keys(%store_info_hash2)) {
			if ($name =~ /^reg$index/i) {
				$counter++;
				push(@header,$name);
				push(@array,$store_info_hash2{$name});		
				#print $q->b("$name - $store_info_hash2{$name}");
			}
			@array_to_print = \@header;
			push(@array_to_print,\@array);								
		}
		
		if ($counter) {
		
			printResult_arrayref2(\@array_to_print);
			$register_info++;	
		}
	}
	unless ($register_info) {
		print "No Register Info found <br /><br />";	
	}
	
	# Workstation Info
	@header=();	
	@array_to_print=();
	@array=();
	my $counter=0;
	
	foreach my $name (sort keys(%store_info_hash2)) {
		if ($name =~ /^WorkStation/i) {
			push(@header,$name);
			push(@array,$store_info_hash2{$name});
			#print $q->b("$name - $store_info_hash2{$name}");
			$counter++;
		}
		@array_to_print = \@header;
		push(@array_to_print,\@array);
		
	}
	if ($counter) {
		printResult_arrayref2(\@array_to_print);
	} else {
		#print "No Workstation Info found <br /><br />";		
	}
	
	# Printer and System Info
	@header=();	
	@array_to_print=();
	@array=();
	$counter=0;
	foreach my $name (sort keys(%store_info_hash4)) {
		if ($name =~ /^System/i) {
			# The data coming back is name & IP so it needs to be split out
			push(@header,$name);
			my @tmp=split(/\s+/,$store_info_hash4{$name});
			my $data1 = $tmp[0];
			my $data2 = $tmp[1];
			push(@array,$data1);
			my $name2 = "$name IP";
			push(@header,$name2);
			push(@array,$data2);
			#print $q->b("$name - $store_info_hash4{$name}");
			$counter++;
		}
		if ($name =~ /^PRINTER/i) {
			# The data coming back is name & IP so it needs to be split out
			push(@header,$name);
			my @tmp=split(/\s+/,$store_info_hash4{$name});
			my $data1 = $tmp[0];
			my $data2 = $tmp[1];
			push(@array,$data1);
			my $name2 = "$name IP";
			push(@header,$name2);
			push(@array,$data2);
			#print $q->b("$name - $store_info_hash4{$name}");
			$counter++;
		}		
		@array_to_print = \@header;
		push(@array_to_print,\@array);
		
	}	
	if ($counter) {
		printResult_arrayref2(\@array_to_print);
	} else {
		print "No Printer Info found <br /><br />";		
	}	
	
	# IP to MAC info
	@header=("IP","MAC");	
	@array_to_print=(\@header);
	@array=();
	$counter=0;
	foreach my $name (sort keys(%store_info_hash5)) {
		my @array=("$name","$store_info_hash5{$name}"); 
		push(@array_to_print,\@array);
		$counter++;
	}	
	if ($counter) {
		printResult_arrayref2(\@array_to_print);
	}		
	
	# VeriFone info
	@header=("Label","Data");	
	@array_to_print=(\@header);
	
	@array=();
	$counter=0;
	foreach my $name (sort keys(%store_info_hash6)) {
		my @array=("$name","$store_info_hash6{$name}"); 
		push(@array_to_print,\@array);
		$counter++;
	}	
	if ($counter) {
		#print $q->h4("VeriFone Credit Card Terminals in use");
		printResult_arrayref2(\@array_to_print);
	} else {
		print "No VeriFone Info found <br /><br />";		
	}		

	standardfooter();
} elsif ("$report" eq "STOREINVQRY") {
    print $q->h2("Store Selection");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREINVRPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
    # Start Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"start_date_selection", -size=>"12", -value=>""}))
    );
    # End Date
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date(yyyy-mm-dd):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"end_date_selection", -size=>"12", -value=>""}))
    );
    # Store Number
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"store_selection", -size=>"12", -value=>""}))
    );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print "Leave the Store Number field blank to get report on all stores<br>";
    print $q->p();

    standardfooter();
} elsif ("$report" eq "STOREINVRPT") {
    print $q->h2("Store Inventory Report");

    my $store_selection   = trim(($q->param("store_selection"))[0]);
    my $start_date_selection   = trim(($q->param("start_date_selection"))[0]);
    my $end_date_selection   = trim(($q->param("end_date_selection"))[0]);

    my $wherestore;
    my $storename;
    if ($store_selection) {
        $wherestore=" and StoreId = $store_selection";
        # First get the store name
        my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
        unless ($dbh) {
            print $q->p("Error - Cannot connect to $mysqld ");
            standardfooter();
            exit;
        }
        my $query = "select storeName from stores where storeId = $store_selection";
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            $storename=$tmp[0];
        }
        $dbh->disconnect;
    } else {
        $storename = "All Stores";
    }



    # The main query here:
    my $dsn = "driver={SQL Server};Server=****;database=dbWarehouse;uid=rpt;pwd=rpt;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
    $query = "
    select
        StoreId,
        TranDate,
        SKU,
        QtyExtra
    from
        tblDailyInventorySkuSummary
    where
        QtyExtra <> 0
    and
        TranDate >= '$start_date_selection'
    and
        TranDate < dateadd( d, 1, '$end_date_selection')

    $wherestore
    order by TranDate,StoreId
    ";

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        print $q->h3("Store: $store_selection - $storename");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }


    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "STOREDISCGRP") {
    print $q->h2("Store Discount Group Report");
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');
    unless ($dbh) {
        print $q->p("Error - Cannot connect to **** ");
        standardfooter();
        exit;
    }

    $query = "
    select
        [No_],
        [Name],
        [City],
        [Customer Posting Group],
        [Customer Disc_ Group],
        [County]
    from
        [Villages\$Customer]
    order by
        [No_]

    ";
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        #print $q->h3("Store: $store_selection - $storename");
        printResult_arrayref2($tbl_ref);
    } else {
        print $q->b("no records found<br>\n");
    }


    $dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "UPDATE_STORES") {
    print $q->h2("Update Store Table");
	# Get the list of stores from Navision
	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
	my %company_hash;
	my %contract_hash;

	$query = "
	SELECT [StoreId]
	  ,[StoreName]
	  ,[StoreCode]

	FROM [****Data].[dbo].[rtblStores]
	where
		[EndDate] is  NULL
	and
		[StoreId] not like '99%'
	";

	my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
	for my $datarows (1 .. $#$tbl_ref_alias) {
		my $thisrow = @$tbl_ref_alias[$datarows];
		my $number = trim($$thisrow[0]);
		my $name = trim($$thisrow[1]);
		# Trim off the "Villages-" from the name
		$name=~ s/Villages-//;
		my $code = trim($$thisrow[2]);
		if ($code eq "V1") {
			$company_hash{$number}=$name;
		}
		if ($code eq "V2") {
			$contract_hash{$number}=$name;
		}
	}
    $dbh->disconnect;
    $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");

    unless ($dbh) {
        print $q->p("Error: Could not connect to $issuesdb database ");
        standardfooter();
        exit;
    }

	foreach my $storeid (sort(keys(%company_hash))) {
		my $storename=$company_hash{$storeid};
		my $status="Company";

		# Update the stores table

		$query="
			replace into stores (storeId,storeName,status) values ($storeid,'$storename','$status')
		";

		my $rc;
		if ($dbh->do($query)) {
			print $q->p("Successfully Updated store $storeid $storename.");
			$rc=1;
		} else {
			print $q->p("ERROR: Failed to update store number $storeid.");
			print $q->p("Query: $query");
			$rc=0;
		}

	}

	foreach my $storeid (sort(keys(%contract_hash))) {
		my $storename=$contract_hash{$storeid};
		my $status="Contract";
		# Update the stores table

		$query="
			replace into stores (storeId,storeName,status) values ($storeid,'$storename','$status')
		";

		my $rc;
		if ($dbh->do($query)) {
			print $q->p("Successfully Updated store $storeid $storename.");
			$rc=1;
		} else {
			print $q->p("ERROR: Failed to update store number $storeid.");
			print $q->p("Query: $query");
			$rc=0;
		}
	}

	$dbh->disconnect;
    standardfooter();
} elsif ("$report" eq "ROUTERS") {
	print $q->h2(" Store Router Links ");
	# Get the list of router IP addresses

	my @header=(
		"Store",
		"Store ID",
		"Address",
		"Updated"


	);
	my @array_to_publish=\@header;
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
    unless ($dbh) {
        print "ROUTERS: failed to connect to $mysqld ". $DBI::errstr . "<br>";
        exit;
    }
    # Find known issues
    my $query="
		select
			s.storeName,
			sia.storeId,
			sia.IP,
			sia.entryDate
		from store_ip_address sia
			join stores s on sia.storeId = s.storeId
		order by
			s.storeName

		";

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {

		my $IP=$tmp[2];
		my $link=$q->a({-href=>"https://$IP"}, "$IP");
		$tmp[2]=$link;
        push(@array_to_publish,\@tmp);
    }
	printResult_arrayref2(\@array_to_publish, );

    $dbh->disconnect;

    standardfooter();

} elsif ("$report" eq "ASC_TOOLS") {
    print $q->h2("ASC and lookupdata Creation Tools - Beta");
	print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
	print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
	print $q->input({-type=>"hidden", -name=>"report", -value=>"ASC_CREATE"});
	print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("ASC or Hash Selection:")));
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({  -type=>"checkbox", -name=>"cb_vendor"}) .
        $q->font({-size=>-1}, "Vendors ")
        ));
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
        $q->input({  -type=>"checkbox", -name=>"cb_artisan"}) .
        $q->font({-size=>-1}, "Artisans ")
        ));		
	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
			  );
	print $q->end_form(), "\n";
	print $q->end_table(),"\n";

    standardfooter();
} elsif ("$report" eq "ASC_CREATE") {
    print $q->h2("ASC Creation Tools");
    my %asc_hash;
    foreach my $name ($q->param) {
        next unless $name =~ /^cb_/;
        my @tmp=split(/_/,$name);
        $asc_hash{$tmp[1]} = 1;
    }
    unless (keys(%asc_hash)) {
        print $q->p("No selection made.");
        standardfooter();
        exit;
    }
    foreach my $key (sort(keys(%asc_hash))) {
        
        if ($key eq "vendor") {
            create_vendor_asc();
        } elsif ($key eq "artisan") {
            create_artisan_hash();			
        } else {
            print $q->p("Sorry, I don't know how to make an ASC file for $key.");
        }
    }
    standardfooter();
} elsif ("$report" eq "PLOYALTY") {
    print $q->h2("Pseudo Loyalty Program");
      
    my @reportlist = (
 
        ["Update", "$scriptname", "PLOYALTY_UPDATE_QRY",
        "Update the psuedo loyalty tables with the current data."
        ],
        ["Customer Query", "$scriptname", "PLOYALTY_RPT_QRY",
        "Pseudo Loyalty Customer Report."
        ],	 
        ["Report", "$scriptname", "PLOYALTY_BASIC_RPT",
        "Pseudo Loyalty Basic Report."
        ],			
    );
    foreach my $reportref (@reportlist) {
        my ($displayname, $scriptname, $reportname, $reportnote) = trim(@$reportref);
        print $q->p(
        $q->a({-href=>"$scriptname?report=$reportname", -class=>"button1"}, $displayname).
        $reportnote );
    }
 
    standardfooter();	
} elsif ("$report" eq "PLOYALTY_UPDATE_QRY") {
    print $q->h2("Pseudo Loyalty Update Query");
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PLOYALTY_UPDATE"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections:")));

    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"startdate_selection", -size=>"10", -value=>"$label_date_previous_30"}))
    );
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Date (YYYY-MM-DD):"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"enddate_selection", -size=>"10", -value=>"$label_date"}))
    );

	print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
			  );
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->p();
      
 
 
    standardfooter();	
} elsif ($report eq "PLOYALTY_UPDATE")   {
	my $user="ployalty";
	my $pw="pseudoloyalty";
    my $startdate_selection   = trim(($q->param("startdate_selection"))[0]);
    my $enddate_selection   = trim(($q->param("enddate_selection"))[0]);
	# Slight modification to the date format
	$startdate_selection=~s/-//g;
	$enddate_selection=~s/-//g;
	print $q->h2("Pseudo Loyalty Update from $startdate_selection to $enddate_selection");
 
	# Connection to **** to get the transaction info
    my $ttvdsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $ttvsql_dbh = openODBCDriver($ttvdsn, 'perl', '****');
 			   			 			   
	# Connection to pos_support to update the info
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ERROR: failed to connect to $mysqld<br>";
       
        standardfooter();
        exit;
    }	
 
	$query = "
	SELECT  
		[TranId]
		,[TenderId]
		,[CardNumber]
		,[CardName]
 
	FROM [****TLog].[dbo].[tblAuthorization]	
	where
		TranDate >= '$startdate_selection'
	and
		TranDate <= '$enddate_selection'
	and
		CardName is NOT NULL
 
		
	";
 
 
    my $tbl_ref = execQuery_arrayref($ttvsql_dbh, $query);

 
    if (recordCount($tbl_ref)) {
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $TranId= trim($$thisrow[0]);
			my $TenderId= trim($$thisrow[1]);
			my $CardNumber= trim($$thisrow[2]);
			my $CardName= trim($$thisrow[3]);
			
			# Need to re-arrange the name a bit
			my @tmp=split(/\//,$CardName);
			$CardName=$tmp[1].' '.$tmp[0];
			$CardName=~s/\'/\''/g; 
			# Grab the last 4 digits of the number
			my $l=length($CardNumber);
 
			my $start=($l - 4);
			$CardNumber=substr($CardNumber, $start);
 
			
			# First Update the customer table 
			my $update_query = dequote(<< "			ENDQUERY");
			insert into ployalty_customer
				(customerName,cardType,cardData)
			values
				('$CardName',$TenderId,$CardNumber)
			ENDQUERY
			
			unless ($dbh->do($update_query) > 0) {
				LogMsg(indent(2)."Database Error updating ployalty_customer");
				LogMsg(indent(2)."Driver=ODBC");
				LogMsg(indent(2).$dbh->{Name}); 
				LogMsg(indent(2)."query=".$query);
				LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
				LogMsg(indent(2)."errstr=".$dbh->errstr);   
 			
			} 
			
			# Second, determine the customer id for this customer
			$update_query = dequote(<< "			ENDQUERY");
			select
				customerId
			from
				ployalty_customer
			where
				customerName = '$CardName'
				and
				cardType = $TenderId
				and
				cardData = $CardNumber
 
			ENDQUERY
			
			my $sth=$dbh->prepare("$update_query");
			$sth->execute();
			my $customerId;
			while (my @tmp = $sth->fetchrow_array()) {   				
				$customerId=$tmp[0];				
			}  	
 
			if ($customerId) {
				# Third, update the transaction table
				$update_query = dequote(<< "				ENDQUERY");
				replace into ployalty_txn
					(customerId,TranId)
				values
					($customerId,$TranId)
				ENDQUERY
				
				unless ($dbh->do($update_query) > 0) {
					LogMsg(indent(2)."Database Error updating ployalty_txn");
					LogMsg(indent(2)."Driver=ODBC");
					LogMsg(indent(2).$dbh->{Name}); 
					LogMsg(indent(2)."query=".$query);
					LogMsg(indent(2)."err=".$dbh->err."\tstate: ".$dbh->state);
					LogMsg(indent(2)."errstr=".$dbh->errstr);   
			
				} 			
			} else {
				print "Error: Failed to determine customer ID for $CardName<br />";				 
			}
		}
	}
	$dbh->disconnect;
	$ttvsql_dbh->disconnect;

 

	standardfooter();	
} elsif ("$report" eq "PLOYALTY_RPT_QRY") {
    print $q->h2("Pseudo Loyalty Report Query");
	print $q->h2("Customer Query");
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"PLOYALTY_CUSTOMER_RPT"});
    print $q->input({-type=>"hidden", -name=>"report_selection", -value=>"$report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Selections")));
 
    # Customer Name
    print $q->Tr(
        $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Customer Name:"),
        $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"customer_name", -size=>"12", -value=>""}))
    );

    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({  -value=>"   Submit   "}))
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
   
    print $q->p(); 
 
    standardfooter();	
} elsif ("$report" eq "PLOYALTY_BASIC_RPT") {
    print $q->h2("Pseudo Loyalty Basic Report");
	# Connection to pos_support 
	my $user="ployalty";
	my $pw="pseudoloyalty";	
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ERROR: failed to connect to $mysqld<br>";
       
        standardfooter();
        exit;
    }		
	my %tender_hash=(
		'4' => 'MasterCard',
		'5' => 'Visa',
		'6' => 'Amex',
		'7' => 'Discover',
		'14' => 'Gift Card'
	);
	my $query = dequote(<< "	ENDQUERY");
	select
		customerId,
		customerName,
		cardType
	from
		ployalty_customer

	ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
		for my $datarows (1 .. $#$tbl_ref) {
			my $thisrow = @$tbl_ref[$datarows];
			my $CardType= trim($$thisrow[2]);
			$CardType=$tender_hash{$CardType};
			$$thisrow[2]=$CardType;
		}

		my @links = ( "$scriptname?report=PLOYALTY_TXN_RPT&customer_selection=".'$_' );
        printResult_arrayref2($tbl_ref, \@links);
    } else {
        print $q->b("no records found<br>\n");
    }	
 
	$dbh->disconnect;
    standardfooter();	
} elsif ("$report" eq "PLOYALTY_TXN_RPT") {
    print $q->h2("Pseudo Loyalty Transaction Report");
	my $customer_selection   = trim(($q->param("customer_selection"))[0]);
	# Connection to pos_support 
	my $user="ployalty";
	my $pw="pseudoloyalty";	
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ERROR: failed to connect to $mysqld<br>";
       
        standardfooter();
        exit;
    }		
 
	my $query = dequote(<< "	ENDQUERY");
	select
		TranId
	from
		ployalty_txn
	where
		customerId = $customer_selection
	ENDQUERY
 
 
	my $tranid_list="(";
	my $sep="";
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	my $customerId;
	while (my @tmp = $sth->fetchrow_array()) {   				
		my $TranId=$tmp[0];	
 
		$tranid_list.="$sep";
		$tranid_list.="\'$TranId\'";
		$sep=",";
	}  	
	$tranid_list.=")";	
 
	$dbh->disconnect;
	# Connect to Navision to get all of the transactions
    my $ttvdsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
    my $ttvsql_dbh = openODBCDriver($ttvdsn, 'perl', '****');
	
	
	$query=dequote(<< "	ENDQUERY");
		select
			top 10
			tm.TranId as TranId,
			case when tm.StoreId='2098' and tm.RegisterId = '02' and
				tm.TranNum between 4013 and 4023 and
				convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
					then tm.TranNum - 4000
					else tm.TranNum end as TxnNum,
			tm.RegisterId as RegNum,			
			sum(convert(int,sign(ms.ExtOrigPrice) * ms.Quantity)) as [Qty],
			sum(ms.ExtSellPrice) as [Net],
			tot.Tax1 as [TotalTax1],
			tot.ManualTax1 as [ManualTax1],
			tot.Tax2 as [TotalTax2],
			tot.ManualTax2 as [ManualTax2],
			tot.Tax3 as [TotalTax3],
			tot.ManualTax3 as [ManualTax3],
			tot.Tax4 as [TotalTax4],
			tot.ManualTax4 as [ManualTax4],
			tot.Tax5 as [TotalTax5],
			tm.PaymentTotal

		from
			****Tlog..tblTranMaster tm
			join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
			join ****Tlog..tblTotal tot on tm.TranId=tot.TranId
		where
			tm.TranId in $tranid_list
		and
			tm.TranVoid = 0 and tm.IsPostVoided = 0 and
			tm.TranModifier not in (2,6) 		 
		group by   tm.TranId, tm.TranNum,tm.RegisterId,
					convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.StoreId,
					tot.Tax1, tot.ManualTax1, tot.Tax2, tot.ManualTax2, tot.Tax3, tot.ManualTax3, tot.Tax4, tot.ManualTax4, tot.Tax5,
					tm.PaymentTotal

		order by TxnNum, tm.RegisterId,
					convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
	ENDQUERY
 
 
    my $tbl_ref = execQuery_arrayref($ttvsql_dbh, $query);
    if (recordCount($tbl_ref)) { 	 	
		my @links = ( "$scriptname?report=SALES_TXN_DTL&tranid_selection=".'$_' );
        printResult_arrayref2($tbl_ref, \@links);
    } else {
        print $q->b("no records found<br>\n");
    }	
	$ttvsql_dbh->disconnect;
    standardfooter();	

	
} elsif ("$report" eq "STOREVFSTAT") {
    print $q->h2("VeriFone Status Report");
	my %active_hash=();
	my %current_term_hash=();
	my %merchant_hash=();
	my %expected_term_hash=(
		'1134'=>'020',
		'1143'=>'005',
		'1245'=>'020',
		'1330'=>'020',
		'1340'=>'004',
		'1472'=>'003',
		'1626'=>'020',
		'1926'=>'020',
		'1931'=>'020',
		'2035'=>'006',
		'2048'=>'020',
		'2097'=>'003',
		'2098'=>'003',
		'2227'=>'002',
		'2262'=>'003',
		'2408'=>'005',
		'2422'=>'020',
		'2450'=>'003',
		'2607'=>'003',
		'2631'=>'020',
		'2760'=>'003',
		'2765'=>'003',
		'2793'=>'017',
		'2847'=>'020',
		'2918'=>'003',
		'2933'=>'003',
		'3069'=>'020',
		'3070'=>'002',
		'3074'=>'003',
		'3125'=>'003',
		'3187'=>'004',
		'3261'=>'020',
		'3409'=>'003',
		'3651'=>'005',
		'3667'=>'020',
		'3718'=>'020',
		'3785'=>'003',
		'3933'=>'003',
		'3964'=>'003',
		'3971'=>'004',
		'3979'=>'003',
		'3992'=>'005',
		'4177'=>'003',
		'4184'=>'003',
		'4639'=>'003',
		'4657'=>'003',
		'4673'=>'003',
		'4745'=>'004',
		'4862'=>'003',
		'4948'=>'003',
		'4981'=>'003',
		'5003'=>'004',
		'5086'=>'003',
		'5093'=>'003',
		'5120'=>'003',
		'5145'=>'020',
		'5147'=>'003',
		'5263'=>'003',
		'5290'=>'002',
		'7001'=>'002',
		'7021'=>'020',
		'7027'=>'006',
		'7037'=>'004',
		'7044'=>'020',
		'7053'=>'020',
		'7060'=>'003',
		'7071'=>'003',
		'7075'=>'003',
		'7096'=>'003',
		'7775'=>'020',
		'8202'=>'020',
		'9277'=>'003',	
	);
	# Get a list of the active stores		
	my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');		
	my %active_store_hash=();
	my $query = "
	SELECT [StoreId]
	  ,[StoreName]

	FROM [****Data].[dbo].[rtblStores]
	where EndDate is  NULL
	and [StoreId] not like '99%'
	and [StoreId] not like '98%'
	";		
	my $sth=$dbh->prepare("$query");
	$sth->execute();
	my $storename;
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];
		$active_store_hash{$StoreId}=1;			
	}
	
	# Get a list of the current terminal configuration
	$query = "
		SELECT 
			  
			  [StoreId],
			  [TermID],
			  [MerchantNo]
 
		FROM [****Data].[dbo].[tblPaymentechMerchantInfo]
		where 
			StoreId > 0
			and 
			DateAdd is not NULL
	";		
	$sth=$dbh->prepare("$query");
	$sth->execute();
	my $storename;
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];
		my $TermID=$tmp[1];	
		my $MerchantNo=$tmp[2];	
		$current_term_hash{$StoreId}=$TermID;
		$merchant_hash{$StoreId}=$MerchantNo;		
	}	
		
	$dbh->disconnect;	
	# Connection to pos_support 
 
	my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","pos","sop");
    unless ($dbh) {
        print "ERROR: failed to connect to $mysqld<br>";
       
        standardfooter();
        exit;
    }		
	# Get the Register Info
	$query = dequote(<< "	ENDQUERY");
 
		select
			distinct(si.storeId) as 'StoreId',
			s.storeName,
			s.status,
			c.codeDesc,
			si.infodata,
			si.infodata2 as 'Tid',
			si.date

		from
			storeInfo si
			join stores s on s.storeId = si.storeId
			join codes c on c.codeId = si.codeId

		where
			1 = 1
		
		and c.codeId = '116'
		group by si.storeId
		order by
			si.storeId
  
			
	ENDQUERY
	
	$sth=$dbh->prepare("$query");
	$sth->execute();
	my %reg1_hash;
	while (my @tmp = $sth->fetchrow_array()) {
		my $StoreId=$tmp[0];
		my $Reg=$tmp[4];
		if ($Reg =~ /HP/) {
			$Reg=substr($Reg,0,5);		 
		}
		$reg1_hash{$StoreId}=$Reg;	 	
	}	
	
	# Get the Verifone driver info for Reg1
	$query = dequote(<< "	ENDQUERY");
		select
			distinct(si.storeId) as 'StoreId',
			si.infodata
		from
			storeInfo si
			join stores s on s.storeId = si.storeId
			join codes c on c.codeId = si.codeId
		where			
			c.codeId = 434 
		group by si.storeId
		order by
			si.storeId  			
	ENDQUERY
	
	$sth=$dbh->prepare("$query");
	$sth->execute();
	my %reg1_eftDeviceDriver_hash;
	my %reg1_eftDeviceDriver_hash;
	while (my @tmp = $sth->fetchrow_array()) {	  	
		my $StoreId=$tmp[0];
		my $ver=$tmp[1];		
		$reg1_eftDeviceDriver_hash{$StoreId}=$ver;	 	 		
	}	

	# Get the Verifone driver info for Reg2	
	$query = dequote(<< "	ENDQUERY");
		select
			distinct(si.storeId) as 'StoreId',
			si.infodata
		from
			storeInfo si
			join stores s on s.storeId = si.storeId
			join codes c on c.codeId = si.codeId
		where			
			c.codeId = 438 
		group by si.storeId
		order by
			si.storeId  			
	ENDQUERY
	
	$sth=$dbh->prepare("$query");
	$sth->execute();
	my %reg2_eftDeviceDriver_hash;
	my %reg2_eftDeviceDriver_hash;
	while (my @tmp = $sth->fetchrow_array()) {	  	
		my $StoreId=$tmp[0];
		my $ver=$tmp[1];		
		$reg2_eftDeviceDriver_hash{$StoreId}=$ver;	 	 		
	}		

 
	$query = dequote(<< "	ENDQUERY");
 
		select
			distinct(si.storeId) as 'StoreId',
			s.storeName,
			s.status,
			c.codeDesc,
			si.infodata,
			si.infodata2 as 'Tid',
			'' as 'Register',
			'' as 'Reg1',
			'' as 'Reg2'

		from
			storeInfo si
			join stores s on s.storeId = si.storeId
			join codes c on c.codeId = si.codeId

		where
			1 = 1
		
		and c.codeId = '415'
		group by si.storeId
		order by
			si.storeId
  
			
	ENDQUERY
	 		
 
    my $tbl_ref = execQuery_arrayref($dbh, $query);
	my %info_hash;
    if (recordCount($tbl_ref)) {
		my $tbl_ref2 = [];
		$$tbl_ref2[0] = $$tbl_ref[0];	# Get the headers from the original
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
			my $storeId=$$thisrow[0];			
			if ($active_store_hash{$storeId}) {
				# This is an active store and we
				# want to display it
				push $tbl_ref2, $thisrow;					
				my $info=$$thisrow[4];
				$info_hash{$info}++;
				my $status=$$thisrow[4];
				if ($status eq "ACTIVE") {
					$active_hash{$storeId}=1;
				}				
				my $term=$current_term_hash{$storeId};
				$$thisrow[5] = $term;	
				# Stick in the Reg1 type 
				my $reg1=$reg1_hash{$storeId};
				$$thisrow[6] = $reg1;
				# Append the Reg1 EFTDeviceDriver Version
				my $reg1v=$reg1_eftDeviceDriver_hash{$storeId};
				
				unless ($reg1v) {
					$reg1v = "NA";
				}
				
				$$thisrow[7] = $reg1v;				
				# Append the Reg2 EFTDeviceDriver Version
				my $reg2v=$reg2_eftDeviceDriver_hash{$storeId};
				
				unless ($reg2v) {
					$reg2v = "NA";
				}	
				
				$$thisrow[8] = $reg2v;	
 		
			} else {
				# If the store is not active, remove it
				undef($reg1_eftDeviceDriver_hash{$storeId});
				undef($reg2_eftDeviceDriver_hash{$storeId});
			}

        }
        printResult_arrayref2($tbl_ref2);
        print $q->p();
        if (keys(%info_hash)) {
            print $q->h3("Results:");
            my $total=0;
            foreach my $info (sort(keys(%info_hash))) {
                $total+=$info_hash{$info};
            }
            foreach my $info (sort(keys(%info_hash))) {
                my $percent = ($total == 0) ? 0 : nearest(.01, $info_hash{$info} / $total * 100);
                print "$info - $info_hash{$info} - ${percent}%<br>";
            }
        }
		print $q->p("---------------------------------------------------");
		if (keys(%reg1_eftDeviceDriver_hash)) {
			print $q->h3("Reg1 Driver Versions:");
			my $total=0;
			my %version_hash;
			foreach my $store (sort(keys(%reg1_eftDeviceDriver_hash))) {				
				my $version=$reg1_eftDeviceDriver_hash{$store};
				if ($version) {
					$version_hash{$version}++;
					$total++;
				}				
			}
			foreach my $ver (sort(keys(%version_hash))) {
				
				my $count=$version_hash{$ver};
				my $percent = ($total == 0) ? 0 : nearest(.01, ($count / $total) * 100);
				print "Reg1 - ver: $ver - Count: $count - Percent: $percent<br />";				
			}
			
		}
		print $q->p("---------------------------------------------------");
		if (keys(%reg2_eftDeviceDriver_hash)) {
			print $q->h3("Reg2 Driver Versions:");
			my $total=0;
			my %version_hash;
			foreach my $store (sort(keys(%reg2_eftDeviceDriver_hash))) {				
				my $version=$reg2_eftDeviceDriver_hash{$store};				
				if ($version) {
					$version_hash{$version}++;
					$total++;
				}
			}
			foreach my $ver (sort(keys(%version_hash))) {
				my $count=$version_hash{$ver};
				my $percent = ($total == 0) ? 0 : nearest(.01, ($count / $total) * 100);
				print "Reg2 - ver: $ver - Count: $count - Percent: $percent<br />";				
			}
			
		}		
		print $q->p("---------------------------------------------------");
		# Check if the termid is correct
		my @fix_array=();
		foreach my $store (sort keys(%current_term_hash)) {
			my $current_tid = $current_term_hash{$store};
			if ($expected_term_hash{$store}) {
				my $expected_tid = $expected_term_hash{$store};
				unless ($expected_tid == $current_tid) {
					if ($active_hash{$store}) {
						print $q->b("WARNING: Expected Term ID for store $store is $expected_tid but found $current_tid.");
						print "<br />";
						my $fix="
							update [****Data].[dbo].[tblPaymentechMerchantInfo]
							set TermID = $expected_tid
							where  
							MerchantNo = '$merchant_hash{$store}';
						";
						#print $q->p("$fix");
						push(@fix_array,$fix);
					}
				}
			} else {
				if ($active_hash{$store}) {
					print $q->p("WARNING: Expected Term ID for store $store is unknown.");
				}
			}
		}
		# Check that we are not missing any stores
		my $missing_count=0;
		foreach my $store (sort keys(%active_store_hash)) {
			unless ($current_term_hash{$store}) {
				print $q->p("WARNING: Current Term ID for store $store is unknown.");
				$missing_count++;
			}
			unless ($expected_term_hash{$store}) {
				print $q->p("WARNING: Expected Term ID for store $store is unknown.");
				$missing_count++;
			}			
		}
		
		my $to_fix_count=0;
		foreach my $f (@fix_array) {
			print $q->p("$f");
			$to_fix_count++;
		}
		if ($to_fix_count) {
			print $q->p("There are $to_fix_count accounts to be fixed.");
		}
		print $q->h3("Note: The list of expected terminals is hard coded in this report.");
    } else {
        print $q->b("no records found<br>\n");
    }
 
 
	$dbh->disconnect;	
    standardfooter();	
} else {
    print $q->h2("$report is not currently supported");
    throwError("Sorry!");
    standardfooter();
}

# close the page.
print $q->end_html;

exit;

##############
# Functions
##############
sub create_vendor_asc {
    # Need to get the vendor info from navision
    my $DBName = '****';
    my $dsn = "driver={SQL Server};Server=****;database=$DBName;uid=****;pwd=****;";
    my $mssql_dbh = openODBCDriver($dsn, '****', '****');
	my $query = "
		SELECT
			[Triversity Vendor ID],
			Name,
			Address,
			City,
			[County],
			[Post Code],
			'',
			[Phone No_],
			[Fax No_],
			[Contact],
			'',
			[E-Mail],

			[Home Page]

		FROM
			[****].[dbo].[Villages\$Vendor]
		where
			[Triversity Vendor ID] is not null
		and
			[Triversity Vendor ID] not like ''
		order by
			[Triversity Vendor ID]
		";

	my $sth = $mssql_dbh->prepare($query);

	$sth->execute();
	my $download_dir = "//****/c/otrs/tmp";
	my $asc_file="${download_dir}/vendor.asc";
	my @header=(
		"Vendor Id",
		"Vendor Name",
		"Address",
		"City",
		"State/Prov.",
		"Zip/Postal",
		"Vendor Type",
		"Phone Number",
		"Fax Number",
		"Contact",
		"Account Number",
		"Email Address",
		"Web Site"

	);
	my @array_to_publish=\@header;
	# Because the vendor names in Navision are not exactly what is used in the store,
	# we will use a reference to get the name rather than getting it from Navision.
	my %vendor_name_hash=(
		'02' => 'Creedmoor Candles',
		'03' => 'Putumayo World Music',
		'04' => 'Music Design',
		'05' => 'The Enterprising Kitchen',
		'06' => 'MarketPlace: Handwork of India',
		'07' => 'Blue Hand',
		'08' => "Women's Bean Project",
		'09' => 'SERRV',
		'10' => 'ODT Maps Inc',
		'11' => 'Bunyaad',
		'12' => 'Equal Exchange',
		'13' => 'Level Ground Trading',
		'14' => 'Herald Press',
		'15' => 'Divine Chocolate',

	);
	open (ASC,">$asc_file");
	# The Villages Entry
	my @villages=("01","Ten Thousand Villages","704 Main Street","Akron","PA","17501","","7178598180","7178592622","Customer Service","","","",);
	push(@array_to_publish,\@villages);
	print ASC "1,01,Ten Thousand Villages,704 Main Street,Akron,PA,17501,,7178598180,7178592622,Customer Service,,\n";
	while (my @tmp=$sth->fetchrow_array()) {
		my $id=$tmp[0];
		my $name=$tmp[1];
		my $address=$tmp[2];
		my $city=$tmp[3];
		my $state=$tmp[4];
		my $zip=$tmp[5];
		my $phone=$tmp[6];
		my $fax=$tmp[7];
		my $contact=$tmp[8];
		my $email=$tmp[9];
		my $web=$tmp[10];
		$name=$vendor_name_hash{$id};	# If you wish to get the name from the vendor name hash rather than from Navision
		$tmp[1]=$name;					# Part II of using the vendor name hash.  Reset the name in the @tmp for display purposes
		print ASC "1,$id,$name,$address,$city,$state,$zip,$phone,$fax,$contact,$email,,$web\n";
		push(@array_to_publish,\@tmp);
	}
	close ASC;
	# Display the current vendors

	printResult_arrayref2(\@array_to_publish, );
	if (-f $asc_file) {
		print $q->p("Created $asc_file");
		print $q->a({-href=>"file:$asc_file", -target=>'_top'}, "Open/Download this file.");
		print "<br /><br />";
	} else {
		print $q->p("Error: Failed to create $asc_file");
	}

    $mssql_dbh->disconnect;
}

sub create_artisan_hash {
 
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
   
    my $query = "
    SELECT
		distinct(ii.[Vendor No_]) as VendorNo, 
		v.[Short Name] as ShortName,
		v.[Name] as VendorName
    FROM
        [****].[dbo].[Villages\$Item] ii
		join [****].[dbo].[Villages\$Vendor] v on ii.[Vendor No_] = v.[No_]
 
    order by v.[Short Name]
    ";
    my $sth=$dbh->prepare("$query");
    $sth->execute();	
	my %artisan_hash=();
	while (my @tmp = $sth->fetchrow_array()) {
		my $no=$tmp[0];
		my $sname=$tmp[1];
		my $name=$tmp[2];
		if ($sname) {
			# If there is a short name, use it.
			$name=$sname;
		}
		$artisan_hash{$no}=$name;
	}
	my $hash_to_print='%artisan_hash = ('."\n";
	foreach my $key (sort keys(%artisan_hash)) {
		my $name=$artisan_hash{$key};
		$name =~ s/\'/\\'/g;
		$hash_to_print.="\t\'$key\'\=\>\'$name\',\n";
	}
	$hash_to_print.=");";
	print $q->pre("$hash_to_print");
    print $q->p();
 
	$dbh->disconnect;
} 

sub update_alias {
    my $directive=shift;
    my $value=shift;
    my $phoneNum=shift;
    my $base_phoneNum=$phoneNum;
    if (length($phoneNum) > 10) {
        $base_phoneNum = substr($phoneNum,-10,10);
    }
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$vcxdb","pos","sop");
	unless ($dbh) {
		print $q->p("Error: Failed to connect to $vcxdb database on $mysqld");
		standardfooter();
		exit;
	}	
    # Is this phone number in the database already?
    $query = dequote(<< "    ENDQUERY");
    select
        phone_no
    from
        phone_cross_ref
    where
        phone_no = '$phoneNum'

    ENDQUERY

    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        # If the phone number exists, add "where" to the following query so that
        # we update the existing entry and don't add a new one.
        $query = dequote(<< "        ENDQUERY");
        update
            phone_cross_ref
        set
            $directive = '$value',
            base_phone_no = '$base_phoneNum'
        where
            phone_no = '$phoneNum'
        ENDQUERY

    } else {
        $query = dequote(<< "        ENDQUERY");
        replace into
            phone_cross_ref
        set
            $directive = '$value',
            phone_no = '$phoneNum',
            base_phone_no = '$base_phoneNum'
        ENDQUERY
    }

    my $rc;
    if ($dbh->do($query)) {
        LogMsg("Successfully Updated.");
        $rc=1;
    } else {
        LogMsg("ERROR: Failed to update alias for $phoneNum.");
        LogMsg("Query: $query");
        $rc=0;
    }
    $dbh->disconnect;
    return $rc;

}
sub printArray {
	use CGI::Pretty qw/:standard :html3 *table/;
	$CGI::Pretty::INDENT = "  ";
	my $headref=shift;
	my $arrayref=shift;
	my @arrayToPrint=@$arrayref;
	my $color="LightYellow";
	print "\n", start_table({-class=>'tableborder', -valign=>'top', -bgcolor=>$color, -border=>'1'}), "\n";
	print $q->Tr(td({-class=>'tablehead', -nowrap=>undef}, $headref));
	my($counter) = 0;
	foreach my $a (@arrayToPrint) {
		next if ($a =~ /^$/);
		$counter++;
		#my @line=split(/\|/,$a);
        my @line=@$a;
		my @row=();
        my $issueno=$line[0];
		my $href = "$scriptname?report=EDITISSUE&issueno=$issueno";
		push @row, a({href=>$href}, $line[0]);
		$color="LightYellow";
		for (my $i=1; $i<=$#line; $i++) {
			my $field=$line[$i];
			if ($field =~ /NEW/) {
				$color="Yellow";
			}

			push(@row, $field);

		}
        #my $check = "<input type=checkbox name=\"cb_$issueno\">", @$a;
        #push(@row, $check);
		print Tr(td({-class=>'tableborder', -bgcolor=>$color,}, \@row) );
   }

   print end_table(),"\n";
   # print Summary Information (rows printed)
   print code("Rows returned=$counter"), br(), "\n";

}
sub printStore {
	use CGI::Pretty qw/:standard :html3 *table/;
	$CGI::Pretty::INDENT = "  ";
	my $headref=shift;
	my $arrayref=shift;
	my @arrayToPrint=@$arrayref;
	my $color="Black";
	print "\n", start_table({-class=>'tableborder', -valign=>'top', -bgcolor=>$color, -border=>'1'}), "\n";
	print $q->Tr(td({-class=>'tablehead', -nowrap=>undef}, $headref));
	my($counter) = 0;
	foreach my $a (@arrayToPrint) {
		next if ($a =~ /^$/);
		$counter++;
	 
        my @line=@$a;
		my @row=();
        
	 
		
		$color="LightYellow";
		for (my $i=0; $i<=$#line; $i++) {
			my $field=$line[$i];
			my $status=$line[5];
			if ($status =~ /CONTRACT/) {
				$color="LightBlue";
			}

			push(@row, $field);

		}
 
		print Tr(td({-class=>'tableborder', -bgcolor=>$color,}, \@row) );
   }

   print end_table(),"\n";
   # print Summary Information (rows printed)
   print code("Rows returned=$counter"), br(), "\n";

}

sub printResult_arrayref_color {
 
	my ($tbl_ref,  $linksref, $formatref, $headerlinksref, $optionref, $footer_ref, $color_ref) = @_;
	# make a local copy of the links array.  I need to temporaily lower the
	# 'strict' pragma to perform this operation.
	no strict 'refs';
	my @links = @{ $linksref };
	my @format = @{ $formatref };
	my @headerlinks = @{ $headerlinksref };
	my %options = %{ $optionref };
	use strict 'refs';

	my $tableid = $options{'tableid'};

	use CGI::Pretty qw/:standard :html3 *table/;
	$CGI::Pretty::INDENT = "  ";

	my $headerrow = '';
	my $names = @$tbl_ref[0];

	my $cols = scalar(@$names);

	# , -id=>TableId;
	print "\n\n", start_table({-class=>'table', -valign=>'top', -cellspacing=>0, -cols=>$cols, -id=>$tableid}), "\n";

	for (my $i = 0; $i <= $#$names; $i++) {
		my $name = TTV::utilities::trim($$names[$i]);
		if (($name =~ m/^\s$/) || ($name eq "")) {
			$name = "&nbsp;"
		}
		if (($i <= $#headerlinks) && ($headerlinks[$i]) && ($name ne "&nbsp;") ) {
			my $href = $headerlinks[$i];
			$href =~ s/\$\_/$name/;
			$headerrow .= th({-class=>'tablehead', -nowrap=>undef}, a({-class=>'headerlink', -href=>$href}, $name));
		} else {
			$headerrow .= th({-class=>'tablehead', -nowrap=>undef}, $name);
		}
	}
	print Tr( $headerrow ) ;
	for my $datarows (1 .. $#$tbl_ref) {
		my $row = '';
		my $thisrow = @$tbl_ref[$datarows];

		for (my $i = 0;  $i <= $#$thisrow;  $i++) {
			my $field = TTV::utilities::trim($$thisrow[$i]);
			if (($field =~ m/^\s$/) || ($field eq "")) {
				$field = "&nbsp;"
			}

			my $align = "left";
			if (($i <= $#format) && ($format[$i]) && ($field ne "&nbsp;") ) {
				no strict 'refs';
				my %thisformat = %{ $format[$i] };
				use strict 'refs';

				if (exists $thisformat{"currency"}) {
				   $field = formatCurrency($field);				    
				}
				elsif ((exists$thisformat{"num"}) && ($field =~ m/^\-?\d+\.?\d*|\.\d+?$/)) {				   
				   my $numformat = $thisformat{"num"};
				   $numformat =~ m/^(\d*)\.(\d*)/;
				   $field = formatNumeric($field, $1, $2);				   
				}
				if (exists $thisformat{"align"}) {
				   $align = $thisformat{"align"};
				}
			}

			if (($i <= $#links) && ($links[$i]) && ($field ne "&nbsp;") ) {
				my $href = $links[$i];
				$href =~ s/\$\_/$field/;

				$row .= td({-class=>'tabledata', -nowrap=>undef, -align=>$align}, a({-class=>'datalink', -href=>$href}, $field));			
			} else {
				$row .= td({-class=>'tabledata', -nowrap=>undef, -align=>$align}, $field);
			}
		}
 
		print Tr($row);
	}
   # Print the footer here ##############################
   my $footer='';
   for (my $i = 0;  $i <= $#$footer_ref;  $i++)
   {
      my $field = @$footer_ref[$i];
         if (($field =~ m/^\s$/) || ($field eq ""))
         {
            $field = "&nbsp;"
         }
         my $align = "left";
         if (($i <= $#format) && ($format[$i]) && ($field ne "&nbsp;") ) {
            my %thisformat = %{ $format[$i] };
            if (exists $thisformat{"currency"}) {
               $field = formatCurrency($field);
            }
            elsif ((exists$thisformat{"num"}) && ($field =~ m/^\-?\d+\.?\d*|\.\d+?$/)) {
               my $numformat = $thisformat{"num"};
               $numformat =~ m/^(\d*)\.(\d*)/;
               $field = formatNumeric($field, $1, $2);
            }
            if (exists $thisformat{"align"}) {
               $align = $thisformat{"align"};
            }
         }
         $footer .= td({-class=>'tabledata', -nowrap=>undef, -align=>$align}, $field);
   }
   print Tr($footer);
   ######################################################
   print end_table(),"\n"; 
   print code("Rows Returned=". TTV::utilities::recordCount($tbl_ref) ), br(), "\n";
}
 



sub AuthenticateUser_simple {
	my $firstname = shift;
	my $lastname = shift;

	if (("$firstname" eq "pos") && ("$lastname" eq "sop")) {
		return "$firstname $lastname";
	}
	else {
		return "Authentication Failed";
	}
}

sub AuthenticateUser {
   my ($strUserID,$strUserPassword) = @_;

   return "Authentication Failed" unless ($strUserID);
   return "Authentication Failed" unless ($strUserPassword);

   my $ad = Authen::Simple::ActiveDirectory->new(
      host      => 'villages.com',
      principal => 'villages.com'
   );

   if ( $ad->authenticate( $strUserID, $strUserPassword) ) {
      return $strUserID;
   }

   return "Authentication Failed";
}

sub LogMsg {
	use Time::HiRes qw(gettimeofday);
   my ($logString) = @_;

   # prefix $logstring with "YYYYMMDD hhmmss : "
   my ($now, $usec) = gettimeofday();
   my @ltm = localtime($now);
   my $datetime = sprintf("%4d%02d%02d %02d:%02d:%02d.%03d", $ltm[5]+1900, $ltm[4]+1, $ltm[3], $ltm[2], $ltm[1], $ltm[0],($usec/1000));
   $logString = $datetime . " : " . $logString;

   my $logfile = $0;
   $logfile =~ s/\.pl$/\.log/i;
   open(LOGFILE, ">>$logfile") or return;
   printf LOGFILE "$logString\n";
   close(LOGFILE);
}

# returns 3* spaces, used for indenting log lines.
sub indent {
   my($n) = @_;
   return '   ' x $n;
}

sub throwError {
   my $errorstr  = shift;
	print $q->h2("Error:");
   print $q->h2("$errorstr");
	print "please ". $q->a({-href=>"javascript:parent.history.go(-1)"}, "go back").
	          ", correct this error and resubmit";
	exit;
}

sub footer {
    # Store Issues Footer
	print "<br \>";
	print $q->a({-href=>"$scriptname?report=ISSUES", -target=>'_top'}, "See Current Issues");
	print "<br \>";
    print $q->a({-href=>"$scriptname?report=QUERY", -target=>'_top'}, "Query Issues");
	#print $q->a({-href=>"/p/storeIssues.pl?report=SIGNOUT"}, "sign out");
}
sub localfooter {
    # Return to the same submenu
    my $report=shift;
	print "<br>\n";
	print $q->a({-href=>"$scriptname?report=$report", -target=>'_top'}, "Return to this same section");
}
sub standardfooter {
	print "<br>\n";
	print $q->a({-href=>"$scriptname?report=DEFAULT", -target=>'_top'}, "Main Menu");
	print "<br>\n";
    #print $q->a({-href=>"/p/postools.pl?report=QUERY", -target=>'_top'}, "Query Issues");
	#print $q->a({-href=>"$scriptname?report=SIGNOUT"}, "sign out");
}



sub get_item_discounts {
    my $storeid=shift;
    my $storename=shift;
    my $startdate=shift;
    my $enddate=$startdate;
    my $summary=shift;
    my $whereTxn=shift;
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    my %totals_hash;
    my $total_origPrice=0;
    my $total_Discounts=0;
    my $total_itemDiscounts=0;
    my $total_transDiscounts=0;
    my $total_promoDiscounts=0;
    my $total_MixMatchDiscounts=0;
    my $total_sellPrice=0;


    my %discountType_hash = (
    '0' => 'Undefined discount type',
    '1' => 'ITEM % OFF',
    '2' => 'ITEM $ OFF',
    '3' => 'PRICE MARKDOWN',
    '4' => 'TRANS % OFF',
    '5' => 'TRANS $ OFF',
    '6' => 'MFG $ COUPON',
    '7' => 'TRANS % OFF IN EMPLOYEE SALE',
    '8' => 'MIX MATCH ITEMS',
    '9' => 'MFG COUPON FOR MATCHED ITEM',
    '10' => 'SCAN MFG COUPON,NO LINK TO AN ITEM',
    '11' => 'ITEM % OFF IN EMPLOYEE SALE',
    '12' => 'Item % Surcharge',
    '13' => 'Item $ Surcharge',
    '14' => 'SURCHARGE MARKDOWN',
    '15' => 'Transaction % Surcharge',
    '16' => 'Transaction $ Surcharge',
    '17' => 'TRANS % AT TOTAL',
    '18' => 'TRANS $ AT TOTAL',
    '19' => 'TRANS % AT TENDER',
    '20' => 'TRANS $ AT TENDER',
    '25' => 'Item Best Price %',
    '26' => 'Item Best Price $',
    );

    my @item_discounts=(
        "1",
        "2",
        "3",
        "12",
        "13",
        "25",
        "26"
    );
    my @ms_array;


    ####
    # Step 1 - Find the Item Discounts

    $query=dequote(<< "    ENDQUERY");
        select
            tm.TranId,
            ds.Sequence,
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                      tm.TranNum between 4013 and 4023 and
                      convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                then tm.TranNum - 4000
                else tm.TranNum end as TxnNum,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.ExtDiscountAmt,
            ds.DiscountPercent,
            ds.DiscountType,
            ds.DiscountID,
            rds.Description,
            rds.DiscGroup,
            ds.Reason,
            case when LEN(ds.ReferenceNum) = 7 then ata.Description else null end as Publication,
            case when LEN(ds.ReferenceNum) = 7 then atb.Description else null end as Creative,
            case when LEN(ds.ReferenceNum) = 7 then atc.Description else null end as Year
        from ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
            join [****].[dbo].[rtblDiscounts] rds on ds.DiscountID=rds.[DiscountId]

            left join ****..tblMarketingAdTrack ata on substring(ds.ReferenceNum,1,3) = ata.code
                and ata.CodeType='P'
            left join ****..tblMarketingAdTrack atb on substring(ds.ReferenceNum,4,2) = atb.code
                and left(atb.CodeType, 1) ='C' and right(atb.CodeType,2) = substring(ds.ReferenceNum,6,2)
            left join ****..tblMarketingAdTrack atc on substring(ds.ReferenceNum,6,2) = atc.code
                and atc.CodeType='Y'
        where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and tm.TranVoid = 0
            and tm.IsPostVoided = 0
            and tm.TranModifier not in (2,6)
            and tm.StoreId in ('$storeid')
        $whereTxn
        order by tm.RegisterId, tm.TranNum

    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    my @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
        "Discount %",
        "Discount ID",
        "Description",
        "DiscGroup",
        "Reason"
    );
    my @summary_header=(
        "",
        "Quantity",
        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
    );

    # Step 2 find the associated merchandise transaction.  We do this by stepping through all of the merchandise records of this tranid.
    # We match the  discount sequence to the merchandise record sequence by knowing that the discount sequence comes after
    # the merchandise sequence but before the next one.  Therefore, the merchandise sequence ($Sequence) should be less than the associated
    # discount sequence ($seq) and yet greater than the previous discount sequence ($previous_seq).
    my @array_to_print=\@header;
    my @summary_array_to_print;

    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $discount_seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
        # Throw out any discounts that are not item discounts
        unless (grep /$DiscountType/,@item_discounts){
            next;
        }
        # Find the sequence that is just prior to this sequence in the [tblMerchandiseSale]
        $query=dequote(<< "        ENDQUERY");
            select
                Sequence
            from
                ****Tlog..tblMerchandiseSale
            where
                TranId = '$tranid'
            order by Sequence
        ENDQUERY

        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my @merch_sequences;
        while (my @tmp = $sth->fetchrow_array()) {
            push(@merch_sequences,$tmp[0]);
        }
        my $merchandise_sequence=0;
        for (my $x=0; $x<=$#merch_sequences; $x++) {
            my $ms=$merch_sequences[$x];

            my $next_ms=$merch_sequences[($x+1)];
            my $previous_ms=$merch_sequences[($x-1)];
            if ($discount_seq > $ms) {

                if (defined($next_ms)) {
                    if ($discount_seq < $next_ms) {
                        # The discount sequence fits in here.  It is less than this
                        # merchandise sequence but is more than the previous one
                        $merchandise_sequence=$ms;
                    }
                } else {
                    # There is no merchandise sequence higher
                    $merchandise_sequence=$ms;
                }
            }
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                ms.ExtRegPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                where
                    tm.TranId = '$tranid'
                and
                    ms.Sequence = '$merchandise_sequence'

        ENDQUERY
        $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];
            my $extoprice=$tmp[3];
            my $extsprice=$tmp[4];
            my $Sequence=$tmp[5];

            # I don't think that any table really knows what the sell price was after this item discount was applied IF there was a subsequent transaction discount.  Therefore, it seems reasonable to me to calculate the
            # final selling price rather than try to find it anywhere. -kdg
            $extsprice=($extoprice + $ExtDiscountAmt);

            my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$extoprice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
            ,"$DiscountID","$Description","$DiscGroup","$Reason");
            push(@array_to_print,\@array);
        }
    }

    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ("$startdate",'');

        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }

        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"ItemDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            return (1,\@summary_header,  \@summary_fieldsum);
        } else {
            return (1,\@array_to_print, \@format);
        }
    } else {
        return (0);
    }

    ####
    $dbh->disconnect;
}

sub get_transaction_discounts {
    my $storeid=shift;
    my $storename=shift;
    my $startdate=shift;
    my $enddate=$startdate;
    my $summary=shift;
    my $whereTxn=shift;
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    my %totals_hash;
    my $total_origPrice=0;
    my $total_Discounts=0;
    my $total_itemDiscounts=0;
    my $total_transDiscounts=0;
    my $total_promoDiscounts=0;
    my $total_MixMatchDiscounts=0;
    my $total_sellPrice=0;

    my @ms_array;

    $query=dequote(<< "    ENDQUERY");
        select
            tm.TranId,
            ds.Sequence,
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                      tm.TranNum between 4013 and 4023 and
                      convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                then tm.TranNum - 4000
                else tm.TranNum end as TxnNum,
            ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.ExtDiscountAmt,
            ds.DiscountPercent,
            ds.DiscountType,
            ds.DiscountID,
            rds.Description,
            rds.DiscGroup,
            --ds.Reason
            cl.Description
            --case when LEN(ds.ReferenceNum) = 7 then ata.Description else null end as Publication,
            --case when LEN(ds.ReferenceNum) = 7 then atb.Description else null end as Creative,
            --case when LEN(ds.ReferenceNum) = 7 then atc.Description else null end as Year

        from
            ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscountsProrated ds on tm.TranId=ds.TranId
            join [****].[dbo].[rtblDiscounts] rds on ds.DiscountID=rds.[DiscountId]

            left join ****..tblMarketingAdTrack ata on substring(ds.ReferenceNum,1,3) = ata.code
                and ata.CodeType='P'
            left join ****..tblMarketingAdTrack atb on substring(ds.ReferenceNum,4,2) = atb.code
                and left(atb.CodeType, 1) ='C' and right(atb.CodeType,2) = substring(ds.ReferenceNum,6,2)
            left join ****..tblMarketingAdTrack atc on substring(ds.ReferenceNum,6,2) = atc.code
                and atc.CodeType='Y'
            join [****Data].[dbo].[tblStoreChoiceList] cl on tm.StoreId = cl.StoreId and ds.Reason = cl.ChoiceCode and cl.ChoiceType = 7
        where
            tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and tm.TranVoid = 0 and tm.IsPostVoided = 0
            and tm.TranModifier not in (2,6)
            and tm.StoreId in ('$storeid')
            $whereTxn
        order by
            tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ds.Sequence

    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    my @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
        "Discount %",
        "Discount ID",
        "Description",
        "DiscGroup",
        "Reason"
    );
    my @summary_header=(
        "",
        "Quantity",
        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
    );

    # Step 2 find the associated merchandise transaction.  We do this by stepping through all of the merchandise records of this tranid.
    # We match the prorated discount sequence to the merchandise record sequence by knowing that the discount sequence comes after
    # the merchandise sequence but before the next one.  Therefore, the merchandise sequence ($Sequence) should be less than the associated
    # discount sequence ($seq) and yet greater than the previous discount sequence ($previous_seq).
    my @array_to_print=\@header;
    my @summary_array_to_print=\@summary_header;
    my $ms_count=0;
    my $last_seq=0;
    my $previous_tranid=0;
    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
        my $previous_seq=0;
        if ($tranid == $previous_tranid) {
            # This is the next sequence in the same transaction.
            # Let's remember what the previous discount sequence was
            $previous_seq=$last_seq;
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                --ms.ExtOrigPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId

                where
                    tm.TranId = '$tranid'
                order by
                    tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ms.Sequence
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];
           # my $cost=$tmp[3];
            #my $extoprice=$tmp[4];
            my $extsprice=$tmp[3];
            my $Sequence=$tmp[4];

            if ($previous_seq) {
                # There was more than one item in this merchandise sale.  We have already found a previous sequence.
                # If this merchandise sequence ($Sequence) is less than the discount sequence ($seq) and yet still greater
                # than the previous discount sequence ($previous_seq) then this discount sequence and this merchandise sequence are associated.
                if (($Sequence < $seq) && ($Sequence > $previous_seq)) {
                    # This matches the sequence of the merchandise sale entry to the prorated discount entry
                    my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$OrigPrice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                   ,"$DiscountID","$Description","$DiscGroup","$Reason");
                    push(@array_to_print,\@array);
                }
            } elsif ($Sequence < $seq) {
                #  This is the first merchandise record in the transaction we are stepping through and this is the first discount record must apply to this merchandise record.
                my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$OrigPrice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                ,"$DiscountID","$Description","$DiscGroup","$Reason");
                push(@array_to_print,\@array);

            }
        }
        $last_seq=$seq;
        $previous_tranid=$tranid;
    }

    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ("$startdate",'');
        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"TransDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            return (1,\@summary_header,  \@summary_fieldsum);
        } else {
            return (1,\@array_to_print, \@format);
        }
    } else {
        return (0);
    }
###
    $dbh->disconnect;
}

sub get_promotion_discounts {
    my $storeid=shift;
    my $storename=shift;
    my $startdate=shift;
    my $enddate=$startdate;
    my $summary=shift;
    my $whereTxn=shift;
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    my %totals_hash;
    my $total_origPrice=0;
    my $total_Discounts=0;
    my $total_itemDiscounts=0;
    my $total_transDiscounts=0;
    my $total_promoDiscounts=0;
    my $total_MixMatchDiscounts=0;
    my $total_sellPrice=0;

    my @ms_array;
    ####################
    # PART II - Promotions
    ####################
    # [tblMerchandiseSale] - Promotions
    $query=dequote(<< "    ENDQUERY");
        select
             tm.RegisterId as RegNum,
             case when tm.StoreId='2098' and tm.RegisterId = '02' and
                          tm.TranNum between 4013 and 4023 and
                          convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                    then tm.TranNum - 4000
                    else tm.TranNum end as TxnNum,
             ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
             tm.TranDateTime,
             ms.SKU,
             ms.Dept,
             ms.Quantity,
             --ms.Cost,
             ms.ExtOrigPrice,
             (ms.Quantity * ms.PromoPriceAdjustAmount) as PromoDiscount,
             --(ms.Quantity * (0 - ms.PromoPriceAdjustAmount)) as ExtPromoDisc,
             ms.ExtRegPrice,
             (ms.PromoPercentAdjustAmount * 100) as 'Promo%Amt',

             ms.PromoNum,
             tsp.[Description],
             ms.OrigPrice

        from ****Tlog..tblTranMaster tm
                join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId
                join [****Data].[dbo].[tblStorePromoHeader] tsp on ms.PromoNum = tsp.[PromoNum] and tm.StoreId = tsp.[StoreId]
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')
        and ms.PromoNum > 0
         $whereTxn
        order by tm.StoreId, tm.RegisterId,
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ("$startdate",'');
        my @sum_columns = (5,6,7,8);
        #   get totals for the specified columns.
        undef($$tbl_ref[0][-1]);
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            my $OrigPrice=$$thisrow[-1];
            if ($OrigPrice > 0) {
                # If the OrigPrice is not 0, then it means that someone did not accept the default price.  As a result, the
                # PromoPriceAdjustAmount may be useless.
                # Set the promo discount to the difference between the orig price and the reg price
                $$thisrow[7] = ($$thisrow[6] - $$thisrow[8]);
                # An asterisk to mark this row.
                $$tbl_ref[$datarows][-1]="*";
            } else {
                undef($$tbl_ref[$datarows][-1]);
            }
            # If this item is being returned, then the ExtOrigPrice is going to be negative.  For some reason, the PromoPriceAdjustAmount is always
            # positive in the tblMerchandiseSale table but I want to show it as negative for sales and positive if is it is a return.
            if ($$thisrow[6] > 0) {
                $$thisrow[7] = (0 - $$thisrow[7]);
            }

            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        my @summary_header=('');

        foreach my $c (@sum_columns) {
            # Grab the headers for the fields we want.
            push(@summary_header,$$tbl_ref[0][$c]);
        }
        my @summary_array_to_print=\@summary_header;
        push @$tbl_ref, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"PromoDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];
        my @format = ('', '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',
        '',
        {'align' => 'Right', 'currency' => ''},

        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            return (1,\@summary_header,  \@summary_fieldsum);
        } else {
            return (1,$tbl_ref, \@format);
        }
    } else {
        return (0);
    }

    $dbh->disconnect;
}

sub get_mixmatch_discounts {
    my $storeid=shift;
    my $storename=shift;
    my $startdate=shift;
    my $enddate=$startdate;
    my $summary=shift;
    my $whereTxn=shift;
    my $dsn = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '****', '****');

    my %totals_hash;
    my $total_origPrice=0;
    my $total_Discounts=0;
    my $total_itemDiscounts=0;
    my $total_transDiscounts=0;
    my $total_promoDiscounts=0;
    my $total_MixMatchDiscounts=0;
    my $total_sellPrice=0;

    my @ms_array;
    ####################
    # PART III - MixMatch
    ####################
    ## Mix Match
    # Get the data from tblDiscounts table
    # First we find the discounts and then we associate them with the item.
    @ms_array=();   # Initialize this variable we are re-using;

    $query=dequote(<< "    ENDQUERY");
        select
            tm.[TranId],
            ds.[Sequence],
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                          tm.TranNum between 4013 and 4023 and
                          convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                    then tm.TranNum - 4000
                    else tm.TranNum end as TxnNum,
             ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.[ExtDiscountAmt]

        from ****Tlog..tblTranMaster tm
            join ****Tlog..tblDiscounts ds on tm.TranId=ds.TranId
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')
        and ds.DiscountType = 8
        $whereTxn
        order by ds.[Sequence]
    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    # Now we loop through this hash and find the associated transaction
    my @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "OrigPrice",
        "MixMatch Discount",
        "Sale Price",
        "MixMatchNumber"
    );
    my @array_to_print;
    my $tbl_ref=\@header;
    my %hash_to_print;
    my $previous_tranid=0;
    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $discount_seq=$array[1];
        my $reg=$array[2];
        my $txn=$array[3];
        my $date=$array[4];
        my $price=$array[5];
        my $discamt=$array[6];

        # Find the sequence that is just prior to this sequence in the [tblMerchandiseSale]
        $query=dequote(<< "        ENDQUERY");
            select
                Sequence
            from
                ****Tlog..tblMerchandiseSale
            where
                TranId = '$tranid'
            order by Sequence
        ENDQUERY

        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my @merch_sequences;
        while (my @tmp = $sth->fetchrow_array()) {
            push(@merch_sequences,$tmp[0]);
        }
        my $merchandise_sequence=0;
        for (my $x=0; $x<=$#merch_sequences; $x++) {
            my $ms=$merch_sequences[$x];
            my $next_ms=$merch_sequences[($x+1)];
            my $previous_ms=$merch_sequences[($x-1)];
            if ($discount_seq > $ms) {
                if (defined($next_ms)) {
                    if ($discount_seq < $next_ms) {
                        # The discount sequence fits in here.  It is less than this
                        # merchandise sequence but is more than the previous one
                        $merchandise_sequence=$ms;
                    }
                } else {
                    # There is no merchandise sequence higher
                    $merchandise_sequence=$ms;
                }
            }
        }
        # Find the merchandise record with this sequence number
        $query=dequote(<< "        ENDQUERY");
            select
                 tm.RegisterId as RegNum,
                 case when tm.StoreId='2098' and tm.RegisterId = '02' and
                              tm.TranNum between 4013 and 4023 and
                              convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                        then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
                 ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
                 tm.TranDateTime,
                 ms.SKU,
                 ms.Dept,
                 ms.Quantity,
                 ms.ExtOrigPrice,
                 --ms.Cost,
                 ms.MixMatchNumber,
                 ms.ExtRegPrice,
                 ms.Sequence
            from ****Tlog..tblTranMaster tm
                    join ****Tlog..tblMerchandiseSale ms on tm.TranId=ms.TranId

            where
                ms.TranId = '$tranid'
            and
                ms.Sequence = '$merchandise_sequence'
        ENDQUERY

        # Find the associated sequence
        $sth=$dbh->prepare("$query");
        $sth->execute();
        my $previous_seq=0;
        while (my @tmp = $sth->fetchrow_array()) {
            # We iterate through the merchandise records for this transaction by sequence number in descending order.
            my $RegNum=$tmp[0];
            my $TxnNum=$tmp[1];
            my $TranDateTime=$tmp[2];
            my $SKU=$tmp[3];
            my $Dept=$tmp[4];
            my $Quantity=$tmp[5];
            my $ExtOrigPrice=$tmp[6];
           # my $Cost=$tmp[7];
            my $MixMatchNumber=$tmp[7];
            my $ExtRegPrice=$tmp[8];
            my $item_seq=$tmp[9];
            #my $SalePrice=($ExtRegPrice + $discamt);
            my $SalePrice=($price + $discamt);
            my @seq_array=( "
            $RegNum",
            "$TxnNum",
            "$TranDateTime",
            "$SKU",
            "$Dept",
            "$Quantity",
            "$price",
            "$discamt",
            "$SalePrice",
            "$MixMatchNumber",
             );

            push(@array_to_print,\@seq_array);
            $hash_to_print{"$TxnNum"."."."$item_seq"}=\@seq_array;
        }
    }
    @array_to_print=\@header;
    # Put this array to print in order of txn
    foreach my $TxnNum (sort {$a <=> $b} keys(%hash_to_print)) {
        my $ref=$hash_to_print{$TxnNum};
        push(@array_to_print,$ref);
    }
    #my $tbl_ref=\@array_to_print;
    if (recordCount(\@array_to_print)) {
        # Remember, at this point, tbl_ref only contains the header.  If we
        # have some records in the array_to_print, add them to the tbl_ref.
        push(@$tbl_ref,\@array_to_print);
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ("$startdate",'');
        my @sum_columns = (5,6,7,8);
        #   get totals for the specified columns.
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }

        }
        my @summary_header=('');
        foreach my $c (@sum_columns) {
            # Grab the headers for the fields we want.
            push(@summary_header,$$tbl_ref[0][$c]);
        }
        my @summary_array_to_print=\@summary_header;
        push @$tbl_ref, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;
        $total_origPrice+=$fieldsum[6];
        $total_MixMatchDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"MixMatchDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];
        my @format = ('', '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        '',
        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            return (1,\@summary_header,  \@summary_fieldsum);
        } else {
            return (1,\@array_to_print, \@format);
        }
    } else {
        return (0);
    }

    $dbh->disconnect;
}



sub getCostForLayaway {
	# This function will determine the cost of an item at the time the layaway was started.
	# The tranid passed it should be the tranid representing the Layaway pickup
	my $sku = shift;
	my $TranId = shift;
	my $date = shift;
	my $storeid = shift;
	my $PendingTranNum;
	my $original_date;
	my $cost;
	
	
 	my $dsn1 = "driver={SQL Server};Server=****;database=****Data;uid=****;pwd=****;";
	my $dbh1 = openODBCDriver($dsn1, 'perl', '****');
	
	# 
	$query = dequote(<< "	ENDQUERY");	 
		
		SELECT  			 			   
			ptp.[PendingTranNum]
		FROM 
			[****TLog].[dbo].[tblPendingTranPayment] ptp
			 
		where 
			ptp.TranId like '$TranId'
 		 
			;	 
  	
	ENDQUERY

	#print $q->pre("$query");
	my $sth=$dbh1->prepare("$query");
	$sth->execute();
	
	while (my @tmp = $sth->fetchrow_array()) {	
		$PendingTranNum=$tmp[0];		
	}
	if ($PendingTranNum) {
		$query = dequote(<< "		ENDQUERY");	 
			
			SELECT  			 			   
				ptp.[TranId],				
				convert(Date, convert(varchar(10),ptp.[lastStateChange],21)) as TranDate
			FROM 
				[****TLog].[dbo].[tblPendingTranPayment] ptp
				 
			where 
				ptp.[PendingTranNum] like '$PendingTranNum'
			order by
				ptp.[TranId]
				;	 
		
		ENDQUERY
		
		my $sth=$dbh1->prepare("$query");
		$sth->execute();
		
		while (my @tmp = $sth->fetchrow_array()) {	
			$original_date = $tmp[1];					
			last;
		}	

		# Now get the cost for this SKU at the time of the original purchase
		
		$query = dequote(<< "		ENDQUERY");	 			
			SELECT  			 			   
				dis.[AvgCost] as Cost								
			FROM 
				[dbWarehouse].[dbo].[tblDailyInventorySkuSummary] dis
				 
			where 
				dis.[StoreId] = $storeid
				and
				dis.SKU = '$sku'
				and
				convert(Date, convert(varchar(10),dis.TranDate,21)) like '$original_date'
 
				;	 
		
		ENDQUERY
		
		#print $q->pre("$query");
		$sth=$dbh1->prepare("$query");
		$sth->execute();
		
		while (my @tmp = $sth->fetchrow_array()) {	
			$cost = $tmp[0];										 
		}			
 		
		
	}
	return $cost;	
	$dbh1->disconnect;	
}

sub collect_info_from_store {
	# Gather info on this store
	my $storeid=shift;
	my %store_info_hash;
	my %store_info_hash2;
	my %store_info_hash3;	
	my %store_info_hash4;	
	my %store_info_hash5;		
	my %store_info_hash6;	# VeriFone Info	
	# Get Navision info
	# Get the list of stores from Navision
	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
	my %company_hash;
	my %contract_hash;

	my $query = "
		Select distinct
		Name =
		 Case 
		   when u.[Name] = 'Ten Thousand Villages-Fredericksbur'
			 then 'Ten Thousand Villages-Fredericksburg'
		   when u.[Name] = 'Ten Thousand Villages-KingofPrussia'
			 then 'Ten Thousand Villages-King of Prussia'
		   when u.[Name] = 'Ten Thousand Villages-Perimeter Pl'
			 then 'Ten Thousand Villages-Perimeter Place'
		   when u.[Name] = 'Ten Thousand Villages-SaltLake City'
			 then 'Ten Thousand Villages-Salt Lake City'
		   when u.[Name] = 'Ten Thousand Villages-WHartford'
			 then 'Ten Thousand Villages-West Hartford'
		   when u.[Name] = 'Ten Thousand Villages-Charlottesvil'
			 then 'Ten Thousand Villages-Charlottesville'
		   when u.[Name] = 'Ten Thousand Villages-Richmond DWTN'
			 then 'Ten Thousand Villages-Richmond Downtown'
		   else u.[Name]
		 End,

		Manager = c.[First Name] + ' ' + c.[Middle Name] + ' ' + c.[Surname], 
		Telephone = u.[Phone No_],  
		u.[Salesperson Code] as 'RSM', 
		u.[Customer Posting Group] as 'Status'
 

		FROM 
		[Villages\$Customer] u left outer join 
			[Villages\$Contact Business Relation] cbr on u.[No_]=cbr.[No_] left outer join
			[Villages\$Contact] c on cbr.[Contact No_]=c.[Company No_] left outer join 
		[Villages\$Organizational Level] o on c.[Organizational Level Code] = o.[Code]
		Where 

		c.Type=1 and  -- 0=company, 1=person
		c.[Company No_] <> 'ECOMMERCE' and 
		u.[Deactive Date] = '1753-01-01' and
		u.[Customer Posting Group] in ('COMPANY', 'CONTRACT') and 
		u.[Store Opening] < getdate() and 
		upper(u.[Name 2]) <> 'TENT SALE' and
		c.[Organizational Level Code] = 'MGR'
		and u.[No_] = $storeid
		";	

	my $sth=$dbh->prepare("$query");
	$sth->execute();
	
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];				
		my @ntmp=split(/-/,$name);
		$name  = $ntmp[1];
		# Fix Kitchen Kettle 
		if ($storeid == 5263) {			
			$name="Kitchen Kettle";
		} 		
		my $manager = $tmp[1];
		my $telephone = $tmp[2];
		my $rsm = $tmp[3];
		my $status = $tmp[4];	
		$store_info_hash{'name'} = $name;
		$store_info_hash{'manager'} = $manager;
		$store_info_hash{'telephone'} = $telephone;
		$store_info_hash{'rsm'} = $rsm;
		$store_info_hash{'status'} = $status;		
	}	
	# Now get the Tent Sale Info
	$query = "
		Select distinct
		Name =
		 Case 
		   when u.[Name] = 'Ten Thousand Villages-Fredericksbur'
			 then 'Ten Thousand Villages-Fredericksburg'
		   when u.[Name] = 'Ten Thousand Villages-KingofPrussia'
			 then 'Ten Thousand Villages-King of Prussia'
		   when u.[Name] = 'Ten Thousand Villages-Perimeter Pl'
			 then 'Ten Thousand Villages-Perimeter Place'
		   when u.[Name] = 'Ten Thousand Villages-SaltLake City'
			 then 'Ten Thousand Villages-Salt Lake City'
		   when u.[Name] = 'Ten Thousand Villages-WHartford'
			 then 'Ten Thousand Villages-West Hartford'
		   when u.[Name] = 'Ten Thousand Villages-Charlottesvil'
			 then 'Ten Thousand Villages-Charlottesville'
		   when u.[Name] = 'Ten Thousand Villages-Richmond DWTN'
			 then 'Ten Thousand Villages-Richmond Downtown'
		   else u.[Name]
		 End,

		No_
		
		FROM 
		[Villages\$Customer] u  
		Where  
		(upper(u.[Name 2]) = 'TENT SALE' 
		or
		upper(u.[Address]) = 'TENT SALE' )
		and
		[Name] like '%$store_info_hash{'name'}%'
		";		
	
	#print $q->pre("$query");

	$sth=$dbh->prepare("$query");
	$sth->execute();	
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];				
		my @ntmp=split(/-/,$name);
		$name  = $ntmp[1];
		# Fix Kitchen Kettle 
		if ($storeid == 5263) {			
			$name="Kitchen Kettle";
		} 		
		my $TentSaleID = $tmp[1];
 
		if ($store_info_hash{'name'} eq $name) {
			$store_info_hash{'TentSaleID'} = $TentSaleID;		 
		}		
	}	
 
	
	###
	$dbh->disconnect;

	# Connection to pos_support to get more info
    my $user="storeinfo";
    my $pw="storeinfo";	
    my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$issuesdb","$user","$pw");
    unless ($dbh) {
        print "ERROR: failed to connect to $mysqld<br>";
       
        standardfooter();
        exit;
    }		
	
	# Get Info about Edna, registers etc.
	$query = "
		select 			
			c.codeDesc,
			si.infoData
		from
			storeInfo si
			join codes c on si.codeId = c.codeId
		where
			(c.codeDesc in ('Brand','Model','SerialNumber','Total Memory','Tenders','TrafficCounter','MAC','ip-assignment')
			or
			c.codeDesc like 'Reg% Brand'
			or
			c.codeDesc like 'Reg% Model'	
			or
			c.codeDesc like 'Reg% TotalPhyMemory'				
			or
			c.codeDesc like 'Reg% SerialNumber'	
			or
			c.codeDesc like 'Reg% OS'
			or
			c.codeDesc like 'Reg% MAC'
			or
			c.codeDesc like 'WorkStation% Brand'			
			or
			c.codeDesc like 'WorkStation% Model'			
			or
			c.codeDesc like 'WorkStation% CPU %'				
			or
			c.codeDesc like 'WorkStation% Physical%'			
			or
			c.codeDesc like 'WorkStation% SerialNumber'				
			or
			c.codeDesc like 'WorkStation% OS'				
			or
			c.codeDesc like 'WorkStation% MAC'				
			or
			c.codeDesc like 'WorkStation% hostname'					
			)	

			
			and
			si.storeId = '$storeid'
	";
	$sth=$dbh->prepare("$query");
	
	#print $q->pre($query);
	$sth->execute();
	
	#print $q->p("err=".$dbh->err."\tstate: ".$dbh->state);
	#print $q->p("errstr=".$dbh->errstr);
					
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];	
		my $data = $tmp[1];
		$store_info_hash2{$name} = $data;
		
	}	
	# Collect Paymentech / Transnet info
	$query = "
		select 			
			c.codeDesc,
			si.infoData
		from
			storeInfo si
			join codes c on si.codeId = c.codeId
		where
			c.codeDesc in ('userName','xipassword')			
			and
			si.storeId = '$storeid'
	";
	$sth=$dbh->prepare("$query");
	
	#print $q->pre($query);
	$sth->execute();
	
	#print $q->p("err=".$dbh->err."\tstate: ".$dbh->state);
	#print $q->p("errstr=".$dbh->errstr);
					
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];	
		my $data = $tmp[1];
		$store_info_hash3{$name} = $data;
		
	}	
	# Collect Printer and System info
	$query = "
		select 			
			c.codeDesc,
			si.infoData,
			si.infoData2
		from
			storeInfo si
			join codes c on si.codeId = c.codeId
		where
			(c.codeDesc like 'PRINTER%'
			or
			c.codeDesc like 'System%')						
			and
			si.storeId = '$storeid'
	";
	$sth=$dbh->prepare("$query");
	
	#print $q->pre($query);
	$sth->execute();
	
	#print $q->p("err=".$dbh->err."\tstate: ".$dbh->state);
	#print $q->p("errstr=".$dbh->errstr);
					
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];	
		my $data = $tmp[1];
		my $data2 = $tmp[2];
		$store_info_hash4{$name} = "$data $data2";
		
	}		
	# Collect IP to MAC address info
	$query = "
		select 			
			c.codeDesc,
			si.infoData,
			si.infoData2
		from
			storeInfo si
			join codes c on si.codeId = c.codeId
		where
			c.codeDesc like 'IP-MAC'					
			and
			si.storeId = '$storeid'
	";
	$sth=$dbh->prepare("$query");
	
	#print $q->pre($query);
	$sth->execute();
	
	#print $q->p("err=".$dbh->err."\tstate: ".$dbh->state);
	#print $q->p("errstr=".$dbh->errstr);
					
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];	
		my $data = $tmp[1];
		my $data2 = $tmp[2];
		$store_info_hash5{$data} = "$data2";
		
	}	

	# Collect VeriFone info
	my $VeriFoneActive=0;
	$query = "
		select 			
			c.codeDesc,
			si.infoData,
			si.infoData2
		from
			storeInfo si
			join codes c on si.codeId = c.codeId
		where
			c.codeDesc like 'EFTDeviceDriver'					
			and
			si.storeId = '$storeid'
	";
	$sth=$dbh->prepare("$query");
	
	#print $q->pre($query);
	$sth->execute();
	
	#print $q->p("err=".$dbh->err."\tstate: ".$dbh->state);
	#print $q->p("errstr=".$dbh->errstr);
					
	while (my @tmp = $sth->fetchrow_array()) {	
		my $name = $tmp[0];	
		my $data = $tmp[1];
		my $data2 = $tmp[2];
		if ($data eq "ACTIVE") {
			$VeriFoneActive=1;		
		}
		$store_info_hash6{$name} = "$data";			
	}		
	if ($VeriFoneActive) {
		$query = "
			select 			
				c.codeDesc,
				si.infoData,
				si.infoData2
			from
				storeInfo si
				join codes c on si.codeId = c.codeId
			where
				(c.codeDesc like 'reg% IP-VeriFone'	
				or
				c.codeDesc like 'reg% TTVLib'				
				or
				c.codeDesc like 'reg% TTVEFT')					
				and
				si.storeId = '$storeid'
		";
		$sth=$dbh->prepare("$query");
		
		#print $q->pre($query);
		$sth->execute();
		
		#print $q->p("err=".$dbh->err."\tstate: ".$dbh->state);
		#print $q->p("errstr=".$dbh->errstr);
						
		while (my @tmp = $sth->fetchrow_array()) {	
			my $name = $tmp[0];	
			my $data = $tmp[1];
			my $data2 = $tmp[2];
			$store_info_hash6{$name} = "$data";							
		}		
	}
	$dbh->disconnect;	
	
			
	
	return (\%store_info_hash,\%store_info_hash2,\%store_info_hash3,\%store_info_hash4,\%store_info_hash5, \%store_info_hash6);
}
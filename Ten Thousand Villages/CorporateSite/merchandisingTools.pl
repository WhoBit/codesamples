#!c:/perl/bin/perl

## merchandisingTools.pl - Tools for tasks that Merchandising does periodically.

##  Originally created for publishing promotions and creating store transfers. - kdg

## 2009-10-19 - v1.0.0 Created - kdg
## 2009-10-27 - v1.0.1 - Brought all of the code in to this script - kdg
## 2009-10-29 - v1.0.2 - Formatting updates - kdg
## 2009-11-06 - v1.0.3 - Formatting updates for promotions - kdg
## 2009-11-09 - v1.0.4 - Added user_list - kdg
## 2009-11-12 - v1.0.5 - Changed extension of transfers from XFI/O to XFI/ODOC - kdg
## 2010-06-15 - v1.0.6 - Added print_method variable for promotions - kdg
## 2010-06-21 - v1.0.7 - Added scriptname variable.
## 2010-06-25 - v2.0.0 - Revised formatting, recording promotions, ability to add a 'delete' promotion - kdg
## 2010-06-30 - v2.0.1 - Added db entry for transfers - kdg
## 2010-11-03 - v2.0.2 - Added UPDATE_STORES and to_store_options and from _store_options - kdg
## 2010-12-21 - v2.1.0 - Changed extension of transfers back to XFI & XFO. (Don't remember why they were not this way)  - Send files to xps/dnload - kdg
## 2010-12-27 - v2.1.1 - Corrections to exlcuding stores for promotions.  Changed document number for transfers back to Akron - kdg
## 2010-12-29 - v2.1.2 - Changed document number for store to store transfers - kdg
## 2011-01-06 - v2.1.3 - Added some usage info around store selection - kdg
## 2011-04-06 - v2.2.0 - Added option to re-publish a promotion - kdg
## 2011-04-05 - v2.2.1 - Adding Bogo - MixMatch is currently not supported but is left in for possible future use - kdg
## 2011-04-11 - v2.2.2 - Added option to re-publish a promotion as a delete - kdg
## 2011-04-14 - v2.2.3 - Added option to delete a bogo - kdg
## 2011-04-29 - v2.3.0 - Added Discount Report - kdg
## 2011-05-02 - v2.3.1 - Minor cleanup and adjustments for BOGO - kdg
## 2011-07-25 - v2.3.2 - Disabled all mixmatch since I want to do that with Configurator.  Updated update_store.  Added promotion report by store. - kdg
## 2011-07-29 - v2.4.0 - Using mixmatch only to define the SKUs.  The mixmatch must be defined in the Configurator.  It is much easier to define there. - kdg
## 2011-09-20 - v2.4.1 - Added store labels in a few places - kdg
## 2012-01-31 - v2.4.2 - Added MixMatchId to labels for clarification.  Added support for vmcode. - kdg
## 2012-02-01 - v2.4.3 - Revised to permit promotions by dollar amount. - kdg
## 2012-02-28 - v2.4.4 - Corrections to permit re-creating a promotion to account for dollar off option - kdg
## 2012-04-10 - v2.5.0 - Added StorePriceReport - kdg
## 2012-05-03 - v2.5.1 - Corrected private variable problem with publish mix match definiton by SKU.  Added end form to multi-part-form - kdg
## 2012-08-28 - v2.5.2 - Exposed DefineMixMatch - kdg
## 2012-09-13 - v2.6.0 - Enabled Defining, Deleting, and Editing MixMatch Discounts.  Not exposed to Merchandising yet. - kdg
## 2012-10-02 - v2.6.1 - Put the MixMatch tools under MixMatch on the Menu - kdg
## 2012-10-31 - v2.7.0 - Added support for tblStoreMixMatch - kdg
## 2012-11-05 - v2.7.1 - Added report showing mixmatch discount history.  Added get_store_info. Added History Check utility. - kdg
## 2012-11-14 - v2.7.2 - Updated some text and formatting - kdg
## 2012-11-30 - v2.7.3 - Exposed MixMatch discount definition to Merchandising - kdg
## 2013-03-12 - v2.7.4 - Moved to new server. Fixed Authentication & Style to work - saz
## 2013-04-22 - v2.7.5 - Revised the upload_dir - kdg
## 2013-04-23 - v2.7.6 - Revised upload_dir again and using promoId rathr than return_code - kdg
## 2013-05-06 - v2.7.7 - Corrected finding DEPT.ASC - kdg
## 2013-05-07 - v2.7.8 - Changed a post to a get and removed lots of debug prints - kdg
## 2013-07-22 - v2.8.0 - Added library and upload function in an attempt to resolve issues with uploading a file.  Improved re-publishing - kdg
## 2013-08-15 - v2.8.1 - Modified to not quit if it hits a store it cannot create - kdg
## 2013-08-28 - v2.8.2 - Adding more logging for re-creating promotions - kdg
## 2013-09-24 - v2.8.3 - Added more logging regarding delete option - kdg
## 2013-11-06 - v2.8.4 - Added reports to show most recent mixmatch created and report to show which stores got a mixmatch - kdg
## 2013-11-21 - v2.8.5 - Added select all for selecting SKUs for MixMatch - kdg
## 2013-11-22 - v2.8.6 - Using replace into mixmatch_store  rather than insert into mixmatch_store (after creating a shared primary key) -kdg
## 2013-11-22 - v2.8.7 - Enabled MixMatch configuration by Seasonality - kdg
## 2013-11-25 - v2.9.0 - Added CURRENTSTOREPROMORPT - kdg
## 2013-12-06 - v2.9.1 - Exposed MixMatch History Utility for Sandra - kdg
## 2013-12-17 - v2.9.2 - Worked on code to deal with duplicates in tblStoreMixMatch - kdg
## 2013-12-17 - v2.9.3 - Modfied to save a copy of the mixmatch.asc file for the manifest - kdg
## 2014-01-06 - v2.9.4 - Modified check for duplicates in MMCHECK - kdg
## 2014-01-30 - v2.9.5 - Getting the departments dynamically from Navision for MixMatch configuration - kdg
## 2014-04-16 - v2.10.0 - Work on MixMatch Discounts to allow discounts on 2, 3, and 4 items purchased - kdg
## 2014-05-05 - v2.10.1 - Updated MMPUBCONFIRM to provide option to publish MixMatch discount to all contract stores - kdg
## 2014-07-08 - v2.10.2 - Added ability to sort fields in Promo Reports & Promo Details Report - kdg
## 2014-07-09 - v2.10.3 - Correction for option to publish to all Contract Stores - kdg
## 2014-08-21 - v2.10.4 - Added ApplyPrice variable and set it to 0. - kdg
## 2014-09-02 - v2.10.5 - Added support for selecting SKUs by vendor for a MixMatch discount - kdg
## 2014-09-04 - v2.11.0 - Added f3_allowGreater for MixMatch Discounts - kdg
## 2014-10-14 - v2.11.1 - Added options for 2+ and 3+ discounts - kdg
## 2014-10-28 - v2.11.2 - Added recalc_item_discount - kdg
## 2014-12-16 - v2.11.3 - Updated to allow editing the MixMatch Discount Name - kdg
## 2014-12-17 - v2.11.4 - Added option for 63% discount - kdg
## 2014-12-23 - v2.12.0 - Revised update_history_table to use eq to match mixmatchName rather than =~.  
##							Added updating mixmatch_pubrecord table.  Added report showing last publish of ASC file - kdg
## 2015-02-24 - v2.12.1 - Allowed the year for editing MixMatch to be up to next year.  Set Publishing options consistent for V1, V2, and all - kdg
## 2015-03-16 - v2.12.2 - Added 37.5% in MIXMATCH Discount Menu - kdg
## 2015-03-18 - v2.12.3 - Changed 37.5 to 38%.  Also some formatting changes. - kdg
## 2015-06-02 - v2.12.4 - Revised MMCHECK - kdg
## 2015-12-21 - v2.12.5 - Added rdr to user_list - kdg

use constant SCRIPT_VERSION => "2.12.5";
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
#use Win32::OLE;
#use Win32::Lanman;
use TTV::utilities qw(trim);
use TTV::library;
use TTV::lookupdata;
use TTV::cgiutilities qw(printResult_arrayref2 formatCurrency formatNumeric ElderLog);
use TTV::MlinkUtilities qw(indent openODBCDriver openFoxProDriver execQuery_arrayref recordCount do_fox_insert dequote processStoreOptions);
use DBI;
use Math::Round qw( round nearest );
use File::Basename;
use Time::Local;
use File::Copy;

my $q = new CGI;

my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
my $mssql_dbh = openODBCDriver($dsn, '***', '****');

my $newfilestxt = "newfiles.txt";
my %newfiles;
my $query;
# Database info
my $mysqld="****";
my $posdb="****";

my $result_code;
my $uid = $ENV{'REMOTE_USER'};
unless ($uid) {
	print $q->p("Error: Failed to determine User ID");
	exit;
}

my %storename_hash=();
my %store_status_hash=();

my $upload_dir="/temp/Merchandising";
my $test_dir = "/temp/POS";
my $dnload_dir  = "//****/MLINK/ToStore";
my $promotxnfile="${upload_dir}/${uid}promotxn.asc";
my $mixmatchASCfile="${upload_dir}/MIXMATCH.asc";
my $mixmatchSKUfile="${upload_dir}/MIXMATCHSKU.TSV";
my $createStoreStandardSku = "//****/MLINK/cron/createStoreStandardSkus.pl";

my $delete=0;
my $update=1;
my $testing_mode=0;
my $debug=0;
# The scriptname defined like this allows the script to be named anything.  It will call itself as
# long as it is in the /p/ folder.
my $scriptname="/p/".basename($0);
if ($scriptname =~ /beta/) {
    # Default to testing mode when using the beta version of the script.
    $testing_mode=1;
}
if ($testing_mode) {
    # - for testing, write here
    $dnload_dir = "$test_dir";
}
my %XferDocSKU;
# print the HTML Header

my $dkcolor="ff9a00";
my $ltcolor="feeacc";
my $style="BODY{font: normal 8pt times new roman,helvetica}\n".
          "FORM{font: normal 8pt times new roman,helvetica}\n".
          "TH{font: normal 8pt arial,helvetica}\n".
          "TD{font: normal 8pt arial,helvetica}\n".
          "table{font: bold normal 8pt arial,helvetica}\n".
          "input{font: normal 8pt times new roman,helvetica}\n".
          ".tableborder {border-color: LightBlue; border-style: solid; border-width: 1px;}\n".
          "";
$style="BODY{font: normal 11pt times new roman,helvetica}\n".
          "FORM{font: normal 11pt times new roman,helvetica}\n".
          "TH{font: bold 11pt arial,helvetica}\n".
          "TD{font: normal 11pt arial,helvetica}\n".
          "table{font: normal 11pt arial,helvetica}\n".
          "input{font: normal 11pt times new roman,helvetica}\n".
          ".tableborder {border-color: #EBEACC; border-style: solid; border-width: 1px;}\n".
          ".table {border-color: #$dkcolor; border-style: solid; border-width: 0px;}\n".
          ".tablehead {background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px; color: #FFFFFF;}\n".
          "A.headerlink:link {color: #$ltcolor; text-decoration: none; }\n".
          "A.headerlink:active {color: #$ltcolor; text-decoration: none;}\n".
          "A.headerlink:visited {color: #$ltcolor; text-decoration: none;}\n".
          "A.datalink:link {color: #000000;}\n".
          "A.datalink:active {color: #000000;}\n".
          "A.datalink:visited {color: #000000;}\n".
          ".tabledata {background-color: #$ltcolor; border-color: #$dkcolor; border-style: solid; border-width: 1px;}\n".
          ".tabledatanb {background-color: #$ltcolor; border-color: #$ltcolor; border-style: solid; border-width: 1px;}\n". # nb=no-border
          ".button1 { font: bold 11pt arial,helvetica; color: #FFFFFF; background-color: #$dkcolor; border-color: #$dkcolor; border-style: solid; border-width: 2px; padding: 0px 1ex 0px 1ex; text-decoration: none;}\n". #width: 100%;
          "";
my @user_list=(
    "kdg",
    "jpl",
    "byd",
    "jlw",
    "sdg",
    "saz",
	"rdr"
    );


###############
# MAIN
###############

print $q->header( -type=>'text/html', -expires=>'-1d');
print $q->start_html(-title=>'Merchandisingtools.pl', -author=>'kdg@tenthousandvillages.com',-style=>{-code=>$style});

print $q->h1("Merchandising Tools");
print $q->h6("Version ".SCRIPT_VERSION);
LogMsgQ("Merchandising Tools Version ".SCRIPT_VERSION);

my $report = ($q->param("report"))[0];
$report = "MAIN" unless $report;

get_store_info();


# Check that the user is on the user list
unless (grep /$uid/,@user_list) {
    print $q->h3( "Sorry.  You are not currently authorized to use this tool.");
    print $q->h3("Please send a note to kdg\@tenthousandvillages.com to request that your account be enabled.");
    exit;
}

SWITCH: {
   if ($report eq "MAIN") {
 
        LogMsgQ(indent(1)."User $uid at the main menu of Merchandising tools version: ".SCRIPT_VERSION);
        print $q->h3("Main Menu ");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print "<br>\n";
		###
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
          $q->start_form({-action=>"$scriptname", -method=>'post'}),
          $q->input({-type=>"hidden", -name=>"report", -value=>"XFER"}),
          $q->submit({ -value=>"   Store Transfer   "}),
          $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
          $q->start_form({-action=>"$scriptname", -method=>'post'}),
          $q->input({-type=>"hidden", -name=>"report", -value=>"PROMO"}),
          $q->submit({ -value=>"   Add Promotions   "}),
          $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
          $q->start_form({-action=>"$scriptname", -method=>'post'}),
          $q->input({-type=>"hidden", -name=>"report", -value=>"EXISTINGPROMO"}),
          $q->submit({ -value=>"   Publish Existing Promotions   "}),
          $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
          $q->start_form({-action=>"$scriptname", -method=>'post'}),
          $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH_MENU"}),
          $q->submit({ -value=>"   MixMatch Discounts   "}),
          $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
         $q->start_form({-action=>"$scriptname", -method=>'post'}),
         $q->input({-type=>"hidden", -name=>"report", -value=>"REPORTS"}),
         $q->submit({ -value=>"   Reports   "}),
         $q->end_form(),
		);
		print $q->end_table(),"\n";
		###
=pod
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"XFER"});
        print $q->submit({ -value=>"   Store Transfer   "});
        print $q->end_form(), "<br />";


        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMO"});
        print $q->submit({-accesskey=>'P', -value=>"   Add Promotions   "});
        print $q->end_form(), "\n";

        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"EXISTINGPROMO"});
        print $q->submit({-accesskey=>'P', -value=>"   Publish Existing Promotions   "});
        print $q->end_form(), "\n";

		print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
		print $q->input({-type=>"hidden", -name=>"report", -value=>"DEFINEMIXMATCH"});
		print $q->submit({-accesskey=>'U', -value=>"   Specify SKUs for a Mix Match Discount   "});
		print $q->end_form(), "\n";
        #print $q->end_table(),"\n";
=pod    # This feature has not been implimented yet - kdg
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMODEL"});
        print $q->submit({-accesskey=>'G', -value=>"   Delete Promotions   "});
        print $q->end_form(), "\n";
        #print $q->end_table(),"\n";
=cut
=pod
        if ($uid eq "kdg") {



            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH"});
            print $q->submit({-accesskey=>'U', -value=>"   Create MixMatch Discount   "});
            print $q->end_form(), "\n";

            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"EDITMIXMATCH"});
            print $q->submit({-accesskey=>'U', -value=>"   Edit MixMatch Discount   "});
            print $q->end_form(), "\n";

            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"DELMIXMATCH"});
            print $q->submit({-accesskey=>'U', -value=>"   Delete MixMatch Discount   "});
            print $q->end_form(), "\n";

			print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHASC"});
			print $q->submit({  -value=>"   Create MixMatch ASC   "});
			print $q->end_form(), "\n";

            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"UPDATE_STORES"});
            print $q->submit({-accesskey=>'U', -value=>"   Update Stores   "});
            print $q->end_form(), "\n";
        }
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"REPORTS"});
        print $q->submit({-accesskey=>'R', -value=>"   Reports   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
=cut
        StandardFooter();
        exit;
	} elsif ($report eq "MIXMATCH_MENU") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
          $q->start_form({-action=>"$scriptname", -method=>'post'}),
          $q->input({-type=>"hidden", -name=>"report", -value=>"DEFINEMIXMATCH"}),
          $q->submit({ -value=>"   Specify SKUs for a Mix Match Discount   "}),
          $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		#print $q->p("To recycle or modify an existing MixMatch Discount Definition:");
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
			$q->start_form({-action=>"$scriptname", -method=>'post'}),
			$q->input({-type=>"hidden", -name=>"report", -value=>"EDITMIXMATCH"}),
			$q->submit({ -value=>"   Edit MixMatch Discount   "}),
			$q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		#print $q->p("If you need to create a new MixMatch Discount Definition:");
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
		  $q->start_form({-action=>"$scriptname", -method=>'post'}),
		  $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH"}),
		  $q->submit({ -value=>"   Create MixMatch Discount   "}),
		  $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		#print $q->p("If you need to delete a MixMatch Discount Definition:");
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
		 $q->start_form({-action=>"$scriptname", -method=>'post'}),
		 $q->input({-type=>"hidden", -name=>"report", -value=>"DELMIXMATCH"}),
		 $q->submit({ -value=>"   Delete MixMatch Discount   "}),
		 $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
		 $q->start_form({-action=>"$scriptname", -method=>'get'}),
		 $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHASC"}),
		 $q->submit({  -value=>"   Create MixMatch ASC   "}),
		 $q->end_form(),
		);
		print $q->end_table(),"\n";
		print "<br>\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
		  $q->start_form({-action=>"$scriptname", -method=>'post'}),
		  $q->input({-type=>"hidden", -name=>"report", -value=>"MMCHECK"}),
		  $q->submit({ -value=>"   Check MixMatch History Utility  "}),
		  $q->end_form(),
		);
		print $q->end_table(),"\n";	
		print "<br>\n";		

		if ($uid eq "kdg") {



			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr(
			  $q->start_form({-action=>"$scriptname", -method=>'post'}),
			  $q->input({-type=>"hidden", -name=>"report", -value=>"MMUTILITY"}),
			  $q->submit({ -value=>"   Create MixMatch Discount Utility  "}),
			  $q->end_form(),
			);
			print $q->end_table(),"\n";


=pod
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->Tr(
             $q->start_form({-action=>"$scriptname", -method=>'post'}),
             $q->input({-type=>"hidden", -name=>"report", -value=>"UPDATE_STORES"}),
             $q->submit({ -value=>"   Update Stores   "}),
             $q->end_form(),
			);
=cut
		}
        StandardFooter();
        exit;
	} elsif ($report eq "MMCHECK") {
		# This utility will check for duplicate mixmatch discounts in the history table.  The history table must not
		# have duplicates since it serves as a reference table for discount reports.

        print $q->h3("MixMatch History Check Utility");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		LogMsgQ(indent(1)."User $uid at the MMCHECK menu");

		my $dbh;
		my $dsn;
		$dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		unless ( $dbh = openODBCDriver($dsn, '***', '****')) {
			LogMsgQ("update_history_table: Failed to connect to **** and **** database");
			print "update_history_table: Failed to connect to **** and **** database<br>";
			StandardFooter();
			exit;
		}
		my %mix_match_hash=();
	 
		# Query the mixmatch history table
		my $query="
			select
				mixmatchId,
				effectiveDate,
				expireDate,
				discountAmt,
				mixmatchName,
				description
			from
				[dbo].[tblStoreMixmatch]
		";

		my $sth=$dbh->prepare("$query");
		$sth->execute();
		while (my @tmp = $sth->fetchrow_array()) {
			my $id=$tmp[0];
			my $effective=$tmp[1];
			my $expire=$tmp[2];
			my $discountAmt=$tmp[3];
			my $mixmatchName=$tmp[4];			
			my $description=$tmp[5];
			###
			# I added some code here to try to better find MixMatch discounts that conflicted with each other.
			# What I am trying to find is two discounts with the same id which are different but which have over-lapping
			# effective dates.  I think that some of these comparing should be done in examining the results of the
			# second query and so I am not certain it should be here. - kdg June 2nd, '15
			###
			if (defined($mix_match_hash{$id})) { 
				my $skip=0;
				my $index=1;
				my $key = "$id $index";
				while (defined($mix_match_hash{$key})) {
					my $value=$mix_match_hash{$key};
					my @key_tmp=split(/\|/,$value);
					my $discountAmt_one=$key_tmp[2];
					my $mixmatchName_one=$key_tmp[3];
					my $description_one=$key_tmp[4];
					if (($discountAmt == $discountAmt_one) && ($mixmatchName eq $mixmatchName_one) && ($description eq $description_one)) {
						# If the two discounts have the same number, the same amount, the same name and description
						# then it does not matter if they have over-lapping effective dates.
						$skip=1;
					}
					$index++;
					$key = "$id $index";					
				}
				unless ($skip) {
					$mix_match_hash{$key}="$effective"."|"."$expire"."|"."$discountAmt"."|"."$mixmatchName"."|"."$description";
				}
			} else {
				$mix_match_hash{$id}="$effective"."|"."$expire"."|"."$discountAmt"."|"."$mixmatchName"."|"."$description";
			}
		}
		# Iterate through the list of mixmatches looking for inconsistencies
		my $duplicate_count=0;
		foreach my $key (sort(keys(%mix_match_hash))) {
			my @key_tmp=split(/\s+/,$key);
			my $mmid=$key_tmp[0];
			my $effective=$mix_match_hash{$key};
			my @tmp=split(/\|/,$effective);
			$effective=substr($tmp[0],0,19);
			my $expire=substr($tmp[1],0,19);
			
			$query="

				select
					[creationDate]
					,[mixmatchId]
					,[discountAmt]
					,[mixmatchName]
					,[description]
					
					,[effectiveDate]
					,[expireDate]
				from
					[dbo].[tblStoreMixmatch]
				where
					mixmatchId = $mmid
				and
					((((effectiveDate > '$effective'
					and
					effectiveDate < '$expire')
					or
					(expireDate > '$effective'
					and
					expireDate < '$expire')))
				or
					((effectiveDate = '$effective')
					and
					(expireDate = '$expire'))
				or
					(((effectiveDate < '$effective'
					and
					expireDate = '$expire')
					or
					(effectiveDate > '$effective'
					and
					expireDate = '$expire')))				
				
					)

			";
 
			#print $q->pre("$query");
			###
			my $tbl_ref = execQuery_arrayref($dbh, $query);

			if (recordCount($tbl_ref) > 1) {

				print $q->h3("MixMatch Definition History Duplicates");
		 
				
				my @links = ( "$scriptname?report=DELMIXMATCHRECORD&creationDate=".'$_' );
								
				printResult_arrayref2($tbl_ref, \@links);
				print $q->h3("Select the effective date of the entry you wish to delete.");
				$duplicate_count++;

			}
			###
		}
		unless ($duplicate_count) {
			print $q->h3("SUCCESS!");
			print $q->p("No duplicate records found in the history table");
		}
        $dbh->disconnect;
        StandardFooter();
        exit;
	} elsif ($report eq "MMUTILITY") {
        print $q->h3("MixMatch Utility");
		# Upload a MixMatch.ASC file and it will be used to update the tblStoreMixMatch in ****
        LogMsgQ(indent(1)."User $uid at the MMUTILITY menu");


		print $q->b("Upload the MixMatch.ASC file to update tblStoreMixMatch:");
		upload($scriptname,$q,"MMUTILITY2");
=pod		
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_multipart_form({-action=>"$scriptname", -method=>'post'}), "\n";
        #print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMUTILITY2"});

        print $q->filefield
                    (-name      => 'source',
                    -size      => 40,
                    -maxlength => 80);


        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "\&nbsp;"),
            );

        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({ -value=>"   ENTER   "}))
            );
        print $q->end_table();
        print $q->end_form();
=cut
        StandardFooter();
        exit;

    } elsif ($report eq "MMUTILITY2") {
        $CGI::POST_MAX = 1024 * 100;  # maximum upload filesize is 100K
        my $verified   = trim(($q->param("verified"))[0]);
        my $source   = trim(($q->param("source"))[0]);
        my $file   = trim(($q->param("file"))[0]);
		my $required_file="mixmatch.asc";
        my $filename;
        my @file_contents;
        my @results;

        # Check that we can find various necessary components
        unless (-d $upload_dir) {
           print $q->p("Failed to find $upload_dir");
           StandardFooter();
           exit;
        }

        # Present a summary of the action about to be executed
        print $q->h3("MixMatch Utility:");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        if ($verified) {
            # Now call the perl script to process the file just uploaded
            unless (-e $file) {
                print $q->p("ERROR: #2 - Failed to locate file ($file)");
                StandardFooter();
                exit;
            }
            my $result_code=process_mmasc($file,$uid);
            #  I am using the result_code variable rather than reading what is returned because
            # the function may print various messages and those get scooped up in the return.
            if ($result_code) {
                LogMsgQ("$file processed successfully");
            } else {
                LogMsgQ("Error processing $file");
                StandardFooter();
                exit;
            }

        } else {
            # Upload the file
            if ($source) {
                $source=~s/\\/\//g;
            } else {
                print $q->p("ERROR: #1 - Failed to find source ($source)");
                StandardFooter();
                exit;
            }
            my $upload_filehandle = $q->upload('source');
            my $file=basename($source);
            # Only permit a specific file to be uploaded
            unless ($file =~ /$required_file/i) {
                print $q->p("Sorry, the file name must be $required_file.  ");
                print $q->p("$file is unacceptable");
                StandardFooter();
                exit;
            }
            $file="${upload_dir}/${file}";
            open ( UPLOADFILE, ">$file" ) or die "Error: ($file) $!";
            binmode UPLOADFILE;

            while ( <$upload_filehandle> ) {
                print UPLOADFILE;
                push(@file_contents,$_);
            }
            close UPLOADFILE;

            my $count=0;

            foreach my $entry (@file_contents) {

				my @tmp=split(/\,/,$entry);
				my $mmid=$tmp[0];
				my $disc1=$tmp[65];
				my $disc2=$tmp[10];
				my $disc3=$tmp[21];
				my $disc4=$tmp[27];
				my $description=$tmp[6];
				my $name=$tmp[69];
				my $effective=$tmp[60];
				my $expire=$tmp[61];
		   ###

                $count++;
				print "Entry $count: MMID: $mmid Desc: $description Name: $name Effective: $effective Expire: $expire<br />";
            }
            print $q->p("$count entries found");


            # Get verification

            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MMUTILITY2"});
            print $q->input({-type=>"hidden", -name=>"file", -value=>"$file"});
            print $q->input({-type=>"hidden", -name=>"verified", -value=>"yes"});

            print $q->submit({-accesskey=>'C', -value=>"   Process this MixMatch.ASC file   "});
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
        }

        print "<br>\n";
        StandardFooter();
        exit;

    } elsif ($report eq "UPDATE_STORES") {
        print $q->h3("Updating Store table from Navision");
        update_stores();

        StandardFooter();
    } elsif ($report eq "UPDATE_STORES_OLD") {
        print $q->h3("Updating Store table from lookupdata module");

        my $dbh;
        my $query;
        my $update_count=0;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("UPDATE_STORES: Failed to connect to $mysqld and $posdb database");
            print "UPDATE_STORES: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
        # First see what we have in the stores table
        my %store_table_hash;
        $query = "select * from stores";
        my $sth=$dbh->prepare("$query");
        $sth->execute();
        while (my @tmp = $sth->fetchrow_array()) {
            my @array=("$tmp[1]","$tmp[2]");
            $store_table_hash{$tmp[0]}=\@array;
        }
        foreach my $storenumber (sort(keys(%storename_hash))) {
            unless(defined($store_table_hash{$storenumber}) && ($store_table_hash{$storenumber})) {
                my @array=("$storenumber","unknown");
                $store_table_hash{$storenumber}=\@array;
            }
            my $ref=$store_table_hash{$storenumber};
            my @store_info=@$ref;
            unless (($storename_hash{$storenumber} eq $store_info[0]) && ($store_status_hash{$storenumber} eq $store_info[1])) {
                # if the values we found to be in the store table do not match the values found in the lookupdata.pm, we need to update the stores table.
                $query = "
                replace into
                    stores
                set
                    storeId = '$storenumber',
                    storeName = '$storename_hash{$storenumber}',
                    status = '$store_status_hash{$storenumber}'
                ";

                if ($dbh->do($query)) {
                    print "Updated stores database with $storenumber $storename_hash{$storenumber} $store_status_hash{$storenumber}<br>";
                    $update_count++;
                } else {
                    print "Failed to update stores database with $storenumber $storename_hash{$storenumber} $store_status_hash{$storenumber}<br>";
                    print "query=".$query."<br>\n";
                    print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
                    print "errstr=".$dbh->errstr."<br>\n";
                    $result_code=0;

                }
            }
        }
        # Add the warehouse
        my $ref=$store_table_hash{0};
        my @store_info=@$ref;
        unless (("AkronWarehouse" eq $store_info[0]) && ("Company" eq $store_info[1])) {
            my $query = "
            replace into
                stores
            set
                storeId = '0000',
                storeName = 'AkronWarehouse',
                status = 'Company'
            ";

            if ($dbh->do($query)) {
                print "Updated stores database with 0000 AkronWarehouse Company<br>";
                $update_count++;
            } else {
                print "Failed to update stores database with 0000 AkronWarehouse Company<br>";
                print "query=".$query."<br>\n";
                print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
                print "errstr=".$dbh->errstr."<br>\n";
                $result_code=0;

            }
        }
        $dbh->disconnect;
        print "$update_count updates were made<br>";
        StandardFooter();
    }
   elsif ($report eq "XFER") {
        my $from   = trim(($q->param("from"))[0]);
        my $to   = trim(($q->param("to"))[0]);

        print $q->h3("Store Transfer Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print "<br>\n";
        # Build a list of Company Stores
        my @storeIds=("V1");
        my @storelist = processStoreOptions($mssql_dbh, \@storeIds);

        my $from_store_options = $q->option({-value=>'', -selected=>undef});
        if ($testing_mode) {
             $from_store_options .= $q->option({-value=>"0099"}, "0099 (Akron Lab)");
        }
        my $to_store_options = $q->option({-value=>'', -selected=>undef});
        if ($testing_mode) {
             $to_store_options .= $q->option({-value=>"0099"}, "0099 (Akron Lab)");
        }
        # Add the Akron warehouse
        $to_store_options .= $q->option({-value=>"0000"}, "0000 (Akron warehouse)");
        for my $store (@storelist) {
            my $storename=$storename_hash{$store};
            $to_store_options .= $q->option({-value=>$store}, "$store ($storename)");
            $from_store_options .= $q->option({-value=>$store}, "$store ($storename)");
        }


        #
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        LogMsgQ(indent(1)."User $uid at the Store Transfer menu");
        if (($from) && ($to)) {
            # If we have both from and to get file

            print $q->p("From store number: $from to store number: $to");
            print "<pre>You need to have a comma separated file with only two columns of data.
The first column is the SKU number.  The second column is the quantity of those SKUs to transfer.</pre>";

            print $q->p("Select the file to be uploaded.");
			#upload($scriptname,$q,"$to");
 		
            print $q->start_multipart_form({-action=>"$scriptname", -method=>'post'}), "\n";
            #print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"XFER2"});
            print $q->input({-type=>"hidden", -name=>"from", -value=>"$from"});
            print $q->input({-type=>"hidden", -name=>"to", -value=>"$to"});

            print $q->filefield
                        (-name      => 'source',
                        -size      => 40,
                        -maxlength => 80
                        );

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "\&nbsp;"),
                );

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                $q->submit({-accesskey=>'G', -value=>"   ENTER   "}))
                );
			print $q->end_form(), "\n";
 
        } else {
            # If we have nothing, start by getting the store to transfer from
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"XFER"});

           # print $q->Tr(
           #     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}),
           #     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "From Store Number:"),
           #     $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"from", -size=>"8", -value=>" "}))
           #     );

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "From Store Number:"),
                $q->td({-class=>'tabledatanb', -align=>'left'},
                $q->Select({-name=>"from", -size=>"1"}), $from_store_options)
                );
            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "To Store Number:"),
                $q->td({-class=>'tabledatanb', -align=>'left'},
                $q->Select({-name=>"to", -size=>"1"}), $to_store_options)
                );
           # print $q->Tr(
           #     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}),
           #     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "To Store Number:"),
           #     $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"to", -size=>"8", -value=>" "}))
           #     );
            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}),

                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                $q->submit({-accesskey=>'G', -value=>"   ENTER   "}))
                );
        }

        print $q->end_form(), "\n";

        print $q->end_table(),"\n";


        StandardFooter();
        exit;
   } elsif ($report eq "XFER2") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        $CGI::POST_MAX = 1024 * 100;  # maximum upload filesize is 100K
        my $verified   = trim(($q->param("verified"))[0]);
        my $from   = trim(($q->param("from"))[0]);
        my $to   = trim(($q->param("to"))[0]);
        my $source   = trim(($q->param("source"))[0]);
        my $file   = trim(($q->param("file"))[0]);
        my $filename;
        my @file_contents;
        #my $script='//****/mlink/cron/createXferDoc.pl';
        my @results;
        my $required_file="xferdoc.csv";

        # Check that we can find various necessary components
        unless (-d $upload_dir) {
           print $q->p("Failed to find $upload_dir");
           StandardFooter();
           exit;
        }

        #unless (-e $script) {
        #    print $q->p("ERROR: Could not find $script");
        #    StandardFooter();
        #    exit;
        #}

        # Present a summary of the action about to be executed
        my $fromname=$storename_hash{$from};
        unless ($fromname) {
            if ($to eq "0099") {
                $fromname="AkronLab";
            } else {
                $fromname="unknown";
            }
        }
        my $toname=$storename_hash{$to};
        unless ($toname) {
            if ($to eq "0000") {
                $toname="AkronWarehouse";
            } elsif ($to eq "0099") {
                $toname="AkronLab";
            } else {
                $toname="unknown";
            }
        }

        print $q->h3("Transfer summary:");
        print $q->p("From store number $from ($fromname) to store number $to ($toname)");

        if ($verified) {
            # Now call the perl script to process the file just uploaded
            unless (-e $file) {
                print $q->p("ERROR: #2 - Failed to locate $file");
                StandardFooter();
                exit;
            }
            my $basefile=basename($file);
            my @ltm = localtime();
            my $date_label=sprintf("%02d%02d%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);
            my $tQty=processXferInputFile($file);
            print $q->p("Found total qty of $tQty in $basefile");

            print $q->p("Creating transfer out for store $from");
            my $DocNo = createXferOutDoc($from,$to,$date_label,$tQty);
            print $q->h3("Created transfer out with document number: $DocNo");
            unless ($to eq "0000") {
                # If we are transfering to the warehouse, we do not need to worry about making certain that the SKUs
                # are published to the store.  Otherwise, this routine will publish the required SKUs.
                publishSkus($to);
            }
            print $q->p("NOTE: This process no longer creates the transfer in document. ");
            # When we get the new transfer - receive process going - remove these next two steps. - kdg
            if (0) {
                print $q->p("Creating transfer in for store $to");
                createXferInDoc($DocNo,$from,$to,$date_label,$tQty);
            }
            print $q->p("Deleting temporary files");
            unlink $file;
            print $q->p("Done");
        } else {
            # Upload the file
            if ($source) {
                $source=~s/\\/\//g;
            } else {
                print $q->p("ERROR: #1 - Failed to find $source");
                StandardFooter();
                exit;
            }
            my $upload_filehandle = $q->upload('source');
            my $file=basename($source);
            # For security reasons, only permit a specific file to be uploaded
            if (0) {
                unless ($file eq "$required_file") {
                     print $q->p("Sorry, the file name must be $required_file (case sensitive).  ");
                    print $q->p("$file is unacceptable");
                    StandardFooter();
                    exit;
                }
            }
            $file="${upload_dir}/${file}";
            open ( UPLOADFILE, ">$file" ) or die "Error: ($file) $!";
            binmode UPLOADFILE;

            while ( <$upload_filehandle> ) {
                print UPLOADFILE;
                push(@file_contents,$_);
            }
            close UPLOADFILE;
            my @header=(
            "SKU",
            "Qty"
            );
            my $counter=0;
            my $tQty=0;
            print "\n", $q->start_table({-class=>'tableborder', -valign=>'top',  -border=>'1'}), "\n";
            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef}, \@header));

            foreach my $f (@file_contents) {
                $counter++;
                my @row=split(/,/,$f);
                $tQty+=$row[1];
                print $q->Tr($q->td({-class=>'tableborder' }, \@row) );
            }
            print $q->end_table(),"\n";
            print "</br>\n";
            print $q->p("$counter SKUs - Total Qty: $tQty");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"XFER2"});
            print $q->input({-type=>"hidden", -name=>"from", -value=>"$from"});
            print $q->input({-type=>"hidden", -name=>"to", -value=>"$to"});
            print $q->input({-type=>"hidden", -name=>"file", -value=>"$file"});
            print $q->input({-type=>"hidden", -name=>"verified", -value=>"yes"});
            print $q->submit({-accesskey=>'G', -value=>"   Issue Transfer   "});
            print $q->end_form(), "\n";
        }


        print "<br>\n";


        StandardFooter();
        exit;
   }
   ##################
   # EXISTINGPROMO
   ##################
   elsif ($report eq "EXISTINGPROMO") {
        print $q->h3("Publish an Existing Promotion Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        LogMsgQ(indent(1)."User $uid at the Existing Promotions menu");
        print "<br>\n";
        print "<pre>
        This tool can be used to publish a promotion previously created with this tool.
        This is useful to re-publish a promotion to a store that did not initially get
        it or to send it again to a store that had problems when it got it the first time.
        </pre>\n";

        my @ltm = localtime();
        my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB
        # Create a form to permit selection of a promotion which has not yet been deleted and which is not yet expired
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("EXISTINGPROMO: Failed to connect to $mysqld and $posdb database");
            print "EXISTINGPROMO: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
        my $promo_options;
        my $promoId;
        my $query = "
			select 
				max(promoId), 
				promoNum, 
				promoTitle, 
				creationDate, 
				effectiveDate, 
				expireDate
			from 
				promotions 
			where 
				(status = 'pubd' 
				or
				status = 'crtd')
				and expireDate > current_date
				 
				
        group by promoNum
        ";
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            print $q->b("The following promotions can be re-published");
            print $q->p("Select the promotion to re-publish");

            my $tbl_ref2 = [];
            my $ref = ["promoId", "promoNum", "promoTitle", "creationDate", "effectiveDate", "expireDate",""];
            push @$tbl_ref2, $ref;
            for my $datarows (1 .. $#$tbl_ref) {
                my $thisrow = @$tbl_ref[$datarows];
                my $promoId = trim($$thisrow[0]);

                my $ref = [@$thisrow,"<input type=checkbox name=\"cb_$promoId\">"];
                push @$tbl_ref2, $ref;
            }
=pod
            # Check box to make this a delete rather than create
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-accesskey=>'D', -type=>"checkbox", -name=>"cb_promo_del"}) .
                   $q->font({-size=>-1}, "Create a \"delete\" promotion")
                  ));
=cut
            print "<form method='post' action=$scriptname>\n";
            print "<input type='hidden' name='report' value='REPUBPROMO'>\n";
            print $q->input({-type=>"checkbox", -name=>"cb_promo_del"},"Create a \"delete\" promo.");
            printResult_arrayref2($tbl_ref2);
            print "<input type='submit' value=' Publish '>\n";
            print "</form>\n";


        } else {
            print $q->b("no promotions eligible to be re-published<br>\n");
        }


        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";


        print $q->end_table();
        print $q->end_form();
        StandardFooter();
        exit;
}
   ##################
   # REPUBPROMO
   ##################
    elsif ($report eq "REPUBPROMO") {
        my $promo_del=0;

        foreach my $name ($q->param) {
            if ($name =~ /^cb_promo_del/) {
                $promo_del=1;
            }
        }

        my $promos;
        my $sep;
        my @promos;
        foreach my $name ($q->param) {
            next unless $name =~ m/^cb_(\-?\d+)$/;
            push(@promos,$1);
            $promos.="$sep"."$1";
            $sep=",";
        }
        print $q->h3("Re-Publish Promotions Menu");
        if ($testing_mode) {
        print "--testing mode--<br>";
        }

        LogMsgQ(indent(1)."User $uid at the Re Publish Promotions menu");
		if ($promo_del) {
			LogMsgQ(indent(2)."Option to delete selected promo IS SET");
		} else {
			LogMsgQ(indent(2)."Option to delete selected promo is not set");
		}
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("REPUBPROMO: Failed to connect to $mysqld and $posdb database");
            print "REPUBPROMO: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
        my @ltm = localtime();
        my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB
        my $promo_options;
        my $promoId;
        my $query = "
            select
                promoId,
                promoNum,
                promoTitle,
                creationDate,
                effectiveDate,
                expireDate
            from
                promotions
            where
                (status = 'pubd' or status = 'crtd')
            and
                expireDate > $date_label
            and
                promoId in ($promos)";
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
            my $rc=recreate_promos(\@promos,$promo_del);

            unless ($rc) {
                print $q->p("Error: Failed to create the ASC file ");
                StandardFooter();
                exit;
            }
            printResult_arrayref2($tbl_ref);
#kdg
###
            print $q->p("Select which stores you wish to publish these promotions to:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"PUBCONFIRM"});
            print $q->input({-type=>"hidden", -name=>"promo_del", -value=>"$promo_del"});
            #print $q->input({-type=>"hidden", -name=>"promoNum", -value=>"$promoNum"});
            #print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
           # print $q->input({-type=>"hidden", -name=>"promo_name", -value=>"$promo_name"});
            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("Enter Criteria")));
            print $q->Tr($q->td({-class  => 'tabledatanb',-nowrap => undef,-align  => 'right'},"Store Selection Group:"),
             $q->td({ -class => 'tabledatanb', -align => 'left' },$q->radio_group(
                   {
                        -name    => "selection",
                       
						-values  => [ 'Opt1: All Company', 'Opt2: All Contract', 'Opt3: All Company & Contract',],
                        -default => 'none',
                        -linebreak=>'true',
                   }),)
            );
			 #-values  => [ 'Opt1: All Company', 'Opt2: All Company except Ephrata', 'Opt3: All Company except Ephrata & Montreat'],
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Specific Store Exclusion:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"storeExclusion", -size=>"50"}))
                      );
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Specific Store Inclusion:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"storeInclusion", -size=>"50"}))
                      );

            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";

            print "<br>Select from Option 1, 2, or 3.  To further modify these options, you can exclude specific<br>";
            print "stores by putting their store number in the 'Exclusion' list.  Similarly, if you want to <br>";
            print "include more stores, put their store number in the 'Inclusion' list.  (If including or excluding<br>";
            print "more than one store, separate the store numbers with a comma.)<br>";
            print "Note: Selection from options 1, 2, or 3 is not required.  You can simply include stores by <br>";
            print "putting the store number in the 'Inclusion' box.<br>";
###
        } else {
            print $q->b("no promotions found");
        }
        StandardFooter();
        exit;
}
   ##################
   # PROMODEL
   ##################
   elsif ($report eq "PROMODEL") {
        print $q->h3("Delete Promotions Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        LogMsgQ(indent(1)."User $uid at the Delete Promotions menu");
        print "<br>\n";
        print "<pre>
        This tool can be used to delete a promotion created with this tool.
        It does not give you the option to delete it only in selected stores.  It will
        be deleted in all stores to which it was originally published.

        You can also add a 'delete' promotion.  To do this, go the the 'add promotion'
        button and check the 'delete' checkbox.</pre>\n";

        my @ltm = localtime();
        my $date_label=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB
        # Create a form to permit selection of a promotion which has not yet been deleted and which is not yet expired
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMODEL: Failed to connect to $mysqld and $posdb database");
            print "PROMODEL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
        my $promo_options;
        my $promoId;
        my $query = "select promoId, promoNum, promoTitle, creationDate, effectiveDate, expireDate  from promotions where status = 'pubd' and expireDate > $date_label";
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            print $q->b("The following promotions can be deleted");
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no promotions eligible to be deleted<br>\n");
        }


        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
###
=pod
        my $query = "select distinct primary_ref from phone_cross_ref order by primary_ref";
    my $tbl_ref_alias = execQuery_arrayref($dbh, $query);

    my $from_alias_number_options;
    if ($from_alias_number_selection) {
        $from_alias_number_options = $q->option({-value=>"$from_alias_number_selection"}, $from_alias_number_selection);
        $from_alias_number_options .= $q->option({-value=>'' -selected=>undef});
    } else {
        $from_alias_number_options = $q->option({-value=>'' -selected=>undef});
    }

    for my $datarows (1 .. $#$tbl_ref_alias)
    {
     my $thisrow = @$tbl_ref_alias[$datarows];
     my $number = trim($$thisrow[0]);

     $from_alias_number_options .= $q->option({-value=>$number}, "$number");
    }
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Call From Alias:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"from_alias_number_selection", -size=>"1"}), $from_alias_number_options)
              );
=cut
###

        print $q->end_table();
        print $q->end_form();
        StandardFooter();
        exit;
   }
   ##################
   # PROMO
   ##################
   elsif ($report eq "PROMO") {
        print $q->h3("Promotions Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        LogMsgQ(indent(1)."User $uid at the Promotions menu");
        print "<br>\n";
        print "<pre>Create a comma separated file named 'promo.csv' and upload below.
The first five rows of this file define the promotion criteria and must have the following format:
    The first row and first column should contain the keyword 'start'
    The first row and second column should contain the startdate for the promotion in the format of mm/dd/yyyy.

    The second row and first column should contain the keyword 'end'
    The second row and second column should contain the enddate for the promotion in the format of mm/dd/yyyy.

    The third row and first column should contain the keywords 'percent off' or 'dollar off'
    The third row and second column should contain the number representing the discount for the promotion.
    For example, if the keyword specifies a 'percent off' promotion, the number '25' would mean 25% off.
    If the keyword specifies a 'dollar off' promotion, the number '25' would mean $25 off.

    The fourth row and first column should contain the keywords 'promo name'
    The fourth row and second column should contain the name of the promotion.

    The fifth row and first column should contain the keywords 'promo number'
    The fifth row and second column should contain the number of the promotion.

    Starting on the sixth row, the first column should contain a SKU number to be included in the promotion.

    Each SKU on promotion must be on its own line.</pre>\n";

	print $q->b("Example defining a percent off promotion:");
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "start"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "01/01/2012"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "end"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "01/07/2012"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "percent off"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "25"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "promo name"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "25% off promotion"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "promo number"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "12"),
            );
        print $q->end_table();

		print "<br />";
	print $q->b("Example defining a dollar off promotion:");
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "start"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "01/01/2012"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "end"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "01/07/2012"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "dollar off"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "25"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "promo name"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "\$25 off promotion"),
            );
        print $q->Tr(
            $q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "promo number"),
			$q->td({-class=>'tabledata', -nowrap=>undef, -align=>'left'}, "12"),
            );
        print $q->end_table();

		print "<br />";
 
	print $q->b("Upload the promo.csv file here:");
		# Note:  I was having real problems getting this upload to work.  I have moved it to library.pm 
		# and so it is called with the function "upload" as shown below.  -kdg - 2013-07-22
		upload($scriptname,$q,"PROMO2");
=pod	
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_multipart_form({-action=>"$scriptname", -method=>'post'}), "\n";
        #print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMO2"});

        print $q->filefield
                    (-name      => 'source',
                    -size      => 40,
                    -maxlength => 80);


        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "\&nbsp;"),
            );

        print $q->Tr(
            $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
            $q->submit({-accesskey=>'G', -value=>"   ENTER   "}))
            );
        print $q->end_table();
        print $q->end_form();
=cut		
        StandardFooter();
        exit;
   }

    ##################
    # PROMO2
    ##################
    elsif ($report eq "PROMO2") {
        $CGI::POST_MAX = 1024 * 100;  # maximum upload filesize is 100K
        my $verified   = trim(($q->param("verified"))[0]);
        my $promo_del=0;
        foreach my $name ($q->param) {
            if ($name =~ /^cb_promo_del/) {
                $promo_del=1;
            }
        }

        my $source   = trim(($q->param("source"))[0]);
        my $file   = trim(($q->param("file"))[0]);
        my $promoNum   = trim(($q->param("promoNum"))[0]);
        my $promo_name   = trim(($q->param("promo_name"))[0]);
        my $filename;
        my @file_contents;
        my @results;
        my $required_file="promo.csv";

        # Check that we can find various necessary components
        unless (-d $upload_dir) {
           print $q->p("Failed to find $upload_dir");
           StandardFooter();
           exit;
        }

        # Present a summary of the action about to be executed
        print $q->h3("Create Promotion:");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        if ($verified) {
            # Now call the perl script to process the file just uploaded
            unless (-e $file) {
                print $q->p("ERROR: #2 - Failed to locate file ($file)");
                StandardFooter();
                exit;
            }
            my $promoId=process_inputfile($file,$uid,$promo_del);
			if ($promoId) {
				LogMsgQ("promoId: $promoId");
			}
            #  I am using the result_code variable rather than reading what is returned because
            # the function may print various messages and those get scooped up in the return.
            if ($promoId) {
                LogMsgQ("$file processed successfully");
                print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
                print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
                print $q->input({-type=>"hidden", -name=>"promoNum", -value=>"$promoNum"});
                print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
                print $q->input({-type=>"hidden", -name=>"promo_name", -value=>"$promo_name"});
                print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMO3"});
                print $q->submit({-accesskey=>'G', -value=>"   Publish this Promotion   "});
                print $q->end_form(), "\n";
            } else {
                LogMsgQ("Error processing $file");
                StandardFooter();
                exit;
            }

        } else {
            # Upload the file
            if ($source) {
                $source=~s/\\/\//g;
            } else {
                print $q->p("ERROR: #1 - Failed to find source ($source)");
                StandardFooter();
                exit;
            }
            my $upload_filehandle = $q->upload('source');
            my $file=basename($source);
            # For security reasons, only permit a specific file to be uploaded
            unless ($file eq "$required_file") {
                print $q->p("Sorry, the file name must be $required_file (case sensitive).  ");
                print $q->p("$file is unacceptable");
                StandardFooter();
                exit;
            }
            $file="${upload_dir}/${file}";
            open ( UPLOADFILE, ">$file" ) or die "Error: ($file) $!";
            binmode UPLOADFILE;

            while ( <$upload_filehandle> ) {
                print UPLOADFILE;
                push(@file_contents,$_);
            }
            close UPLOADFILE;
            my @header=(
            "SKU",
            "Qty"
            );
            my $start=$file_contents[0];
            my @tmp=split(/,/,$start);
            $start=$tmp[1];
            my $end=$file_contents[1];
            @tmp=split(/,/,$end);
            $end=$tmp[1];
            my $discount=$file_contents[2];
			my $discountType="Percent";	#default
            @tmp=split(/,/,$discount);
			if ($tmp[0] =~ /dollar/i) {
				$discountType="Dollar";
			}
            $discount=$tmp[1];
            my $promo_name=$file_contents[3];
            @tmp=split(/,/,$promo_name);
            $promo_name=$tmp[1];
            my $promo_num=$file_contents[4];
            @tmp=split(/,/,$promo_num);
            $promo_num=$tmp[1];
            print $q->p("Start date: $start");
            print $q->p("End date: $end");
            print $q->p("$discountType off: $discount");
            print $q->p("Promo name: $promo_name");
            print $q->p("Promo number: $promo_num");
            print $q->p("The following SKUs:");
            my $count=0;
            foreach (my $x=5; $x<=$#file_contents; $x++){
                my $sku=$file_contents[$x];
                $sku=~s/,//g;
                print "$sku</br>\n";
                $count++;
            }
            print $q->p("$count SKUs");
            # Get verification

            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMO2"});
            print $q->input({-type=>"hidden", -name=>"file", -value=>"$file"});
            print $q->input({-type=>"hidden", -name=>"verified", -value=>"yes"});
            print $q->input({-type=>"hidden", -name=>"promoNum", -value=>"$promoNum"});
            print $q->input({-type=>"hidden", -name=>"promo_name", -value=>"$promo_name"});

            #print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("Add A Promotion")));
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
                   $q->input({-accesskey=>'D', -type=>"checkbox", -name=>"cb_promo_del"}) .
                   $q->font({-size=>-1}, "Create a \"delete\" promotion")
                  ));

            print $q->submit({-accesskey=>'C', -value=>"   Create this Promotion   "});
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";

        }


        print "<br>\n";
        StandardFooter();
        exit;
    } elsif ($report eq "PROMO3") {

        # Report: PROMO3
        # This we select stores where the promotion will be published.
        my $promoNum   = trim(($q->param("promoNum"))[0]);
        my $promoId   = trim(($q->param("promoId"))[0]);
        my $promo_name   = trim(($q->param("promo_name"))[0]);

        print $q->h3("Store Selection Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        LogMsgQ(indent(1)."User $uid at the Store Selection menu");

        ##########
        print $q->p("Select which stores you wish to publish promotion #$promoNum $promo_name to:");
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PUBCONFIRM"});
        print $q->input({-type=>"hidden", -name=>"promoNum", -value=>"$promoNum"});
        print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
        print $q->input({-type=>"hidden", -name=>"promo_name", -value=>"$promo_name"});
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("Enter Criteria")));
        print $q->Tr($q->td({-class  => 'tabledatanb',-nowrap => undef,-align  => 'right'},"Store Selection Group:"),
         $q->td({ -class => 'tabledatanb', -align => 'left' },$q->radio_group(
               {
                    -name    => "selection",                    
					-values  => [ 'Opt1: All Company', 'Opt2: All Contract', 'Opt3: All Company & Contract',],
                    -default => 'none',
                    -linebreak=>'true',
               }),)
        );
		#-values  => [ 'Opt1: All Company', 'Opt2: All Company except Ephrata', 'Opt3: All Company except Ephrata & Montreat'],
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Specific Store Exclusion:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"storeExclusion", -size=>"50"}))
                  );
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Specific Store Inclusion:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"storeInclusion", -size=>"50"}))
                  );

        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        print "<br>Select from Option 1, 2, or 3.  To further modify these options, you can exclude specific<br>";
        print "stores by putting their store number in the 'Exclusion' list.  Similarly, if you want to <br>";
        print "include more stores, put their store number in the 'Inclusion' list.  (If including or excluding<br>";
        print "more than one store, separate the store numbers with a comma.)<br>";
        print "Note: Selection from options 1, 2, or 3 is not required.  You can simply include stores by <br>";
        print "putting the store number in the 'Inclusion' box.<br>";
        ########
        StandardFooter();
        exit;
    } elsif ($report eq "PUBCONFIRM") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->h3("Publish Confirmation");
        my $selection   = trim(($q->param("selection"))[0]);
        my $storeInclusion   = trim(($q->param("storeInclusion"))[0]);
        my $storeExclusion   = trim(($q->param("storeExclusion"))[0]);
        my $promoNum   = trim(($q->param("promoNum"))[0]);
        my $promoId   = trim(($q->param("promoId"))[0]);
        my $promo_name   = trim(($q->param("promo_name"))[0]);
        my $promo_del   = trim(($q->param("promo_del"))[0]);
        my @storeIds;
        my @exclusions=split(/,/,$storeExclusion);
        my @inclusions=split(/,/,$storeInclusion);

        if ($selection =~ /opt1/i) {
            @storeIds='V1';
        }
        if ($selection =~ /opt2/i) {
            @storeIds='V2';
            #@exclusions=("7037");
            #push(@exclusions,"7037");
        }
        if ($selection =~ /opt3/i) {
            @storeIds='all';
            #@exclusions=("7037","9277");
            #push(@exclusions,"7037");
            #push(@exclusions,"9277");
        }
        my @storelist = processStoreOptions($mssql_dbh, \@storeIds);
        my @tmp;
        # Exclude stores on the exclusion list
        STORE: foreach my $store (@storelist) {
            foreach my $e (@exclusions) {
                if ($store == $e) {
                    print "Excluding store $store<br>";
                    next STORE if ($e == $store);
                }
            }
            push(@tmp,$store);
        }
        @storelist=@tmp;
        # Include stores on the inclusion list
        foreach my $i (@inclusions) {
            print "Including store $i<br>";
            push(@storelist,$i);
        }
        if ($#storelist > -1) {
            print $q->p("Publish Promotion $promoNum $promo_name to the following stores:");
            foreach my $s (@storelist) {
                print "$s<br>";
            }
        } else {
            print $q->p("Error: No store specified");
            StandardFooter();
            exit;
        }
        print "<br>";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PUBPROMO"});
        print $q->input({-type=>"hidden", -name=>"promoNum", -value=>"$promoNum"});
        print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
        print $q->input({-type=>"hidden", -name=>"promo_name", -value=>"$promo_name"});
        print $q->input({-type=>"hidden", -name=>"promo_del", -value=>"$promo_del"});
        print $q->input({-type=>"hidden", -name=>"storeInclusion", -value=>"$storeInclusion"});
        print $q->input({-type=>"hidden", -name=>"storeExclusion", -value=>"$storeExclusion"});
        print $q->input({-type=>"hidden", -name=>"selection", -value=>"$selection"});
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Confirmed   "}))
                  );
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        StandardFooter();
        exit;
    } elsif ($report eq "PUBPROMO") {

        # Report: PUBPROMO - Actually publish the promotion

        my $selection   = trim(($q->param("selection"))[0]);
        my $storeInclusion   = trim(($q->param("storeInclusion"))[0]);
        my $storeExclusion   = trim(($q->param("storeExclusion"))[0]);
        my $promoNum   = trim(($q->param("promoNum"))[0]);
        my $promoId   = trim(($q->param("promoId"))[0]);
        my $promo_name   = trim(($q->param("promo_name"))[0]);
        my $promo_del   = trim(($q->param("promo_del"))[0]);

        print $q->h3("Publishing promotion $promoNum to selected stores");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my @storeIds;
        my @exclusions=split(/,/,$storeExclusion);
        my @inclusions=split(/,/,$storeInclusion);

        if ($selection =~ /opt1/i) {
            @storeIds='V1';
        }
        if ($selection =~ /opt2/i) {
            @storeIds='V2';
            #@exclusions=("7037");
            #push(@exclusions,"7037");
        }
        if ($selection =~ /opt3/i) {
            @storeIds='all';
            #@exclusions=("7037","9277");
            #push(@exclusions,"7037");
            #push(@exclusions,"9277");
        }
        my @storelist = processStoreOptions($mssql_dbh, \@storeIds);
        my @tmp;
        # Exclude stores on the exclusion list
        STORE: foreach my $store (@storelist) {
            foreach my $e (@exclusions) {
                if ($store == $e) {
                    # Exclude from the store list.
                    print "Excluding store $store<br>";
                    LogMsgQ("Excluding store $store");
                    next STORE if ($e == $store);
                }
            }
            push(@tmp,$store);
        }
        @storelist=@tmp;
        # Include stores on the inclusion list
        foreach my $i (@inclusions) {
            push(@storelist,$i);
            print "Including store $i<br>";
            LogMsgQ("Including store $i");
        }


        my $store_counter=0;
        # Connect to the database
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PUBPROMO: Failed to connect to $mysqld and $posdb database");
            print "PUBPROMO: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
		my %promoNum_hash;
        foreach my $store (@storelist) {

            # Publish the promo to each store
            my $targetfile = "$dnload_dir/$store/PROMOTXN.ASC";			
		 
            # Open ASC in append mode in case there is already a promotion queued
            if (open(ASC,">>$targetfile")) {
				print "Publishing to store $store $storename_hash{$store}...<br>";
				LogMsgQ("Publishing to store $store");			
                if (open(INPUT,"$promotxnfile")) {
                    while (<INPUT>) { 						
                        print ASC "$_";
						my $line=$_;
						my @tmp=split(/,/,$line);
						$promoNum=$tmp[18];
						$promoNum_hash{$promoNum}=1;
					 					
                    }
                } else {
                    print $q->p("Error:  Could not open $promotxnfile");
                    LogMsgQ("Error:  Could not open $promotxnfile");
                    StandardFooter();
                    exit;
                }
                close INPUT;
                close ASC;
				foreach my $promoNum (keys(%promoNum_hash)) {					
 
					# Get the promoId for this promoNum
					
					$query="select promoId from promotions where promoNum = $promoNum order by promoId desc limit 1";
					my $sth=$dbh->prepare("$query");
					if ($sth->execute()) {
						while (my @tmp = $sth->fetchrow_array()) {
							$promoId=$tmp[0];
						}
					} else {
						print "ERROR with db query: ($query)<br>";
						StandardFooter();
						exit;
					}					
					
					# Update the database
					my $query = "
					insert into promotion_store (promoId,storeId) values ('$promoId','$store')
					";
					unless ($dbh->do($query)) {
						print "Failed to record $store in promotion_store table<br>";
						print "query=".$query."<br>\n";
						print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
						print "errstr=".$dbh->errstr."<br>\n";
						StandardFooter();
						exit;
					} else {
						LogMsgQ("PUBPROMO: Updated promotion_store for promo $promoId and store $store.");
					}
					
					# update the status of the promotion
				 
					
					$query = "
					update
						promotions
					set status = 'pubd'
					where
						promoId = '$promoId'
					";
			 

					unless ($dbh->do($query)) {
						print "Failed to set promotion status in promotions table<br>";
						print "query=".$query."<br>\n";
						print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
						print "errstr=".$dbh->errstr."<br>\n";
					} else {
						LogMsgQ("PUBPROMO: Setting status of promo $promoId to pubd.");
					}	
				}
				open FILE, ">> $dnload_dir/$store/newfiles.txt";
				print FILE "PROMOTXN.ASC\tC:/XPS/PARM\n";
				close FILE;
				print "Successfully published to store $store $storename_hash{$store}.<br>";
				$store_counter++;				
            } else {
                print $q->p("Error:  Could not create $targetfile");
                LogMsgQ("Error:  Could not create $targetfile");
				print "Failed to publish to store $store $storename_hash{$store}!<br>";
                #StandardFooter();
                #exit;
            }

        }
        # If the publishing was successful, remove the promotxnfile
	 
		foreach my $promoNum (keys(%promoNum_hash)) {
			print $q->p("Successfully published promotion $promoNum to $store_counter store(s)");
		}
        unlink $promotxnfile;
        print $q->p("Done");
=pod		
        # update the status of the promotion
	 
		LogMsgQ("PUBPROMO: Setting status of promo $promoId to pubd.");
        my $query = "
        update
            promotions
        set status = 'pubd'
        where
            promoId = '$promoId'
        ";
        #my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop");

        unless ($dbh->do($query)) {
            print "Failed to set promotion status in promotions table<br>";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
        }
=cut		
        $dbh->disconnect;
        StandardFooter();
        exit;
	} elsif ($report eq "LIST_MIXMATCH_HISTORY") {

		if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
		my $query="
				select
					mixmatchId,
					discountAmt,
					description,
					mixmatchName,
					effectiveDate,
					expireDate,
					creationDate
				from
					tblStoreMixMatch
				order by
					mixmatchId,creationDate
				";


        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {

            print $q->h3("MixMatch Definition History");
			printResult_arrayref2($tbl_ref);

		} else {
			print $q->p("No MixMatch History found");
		}

        $dbh->disconnect;
		StandardFooter();
        exit;
	} elsif ($report eq "LIST_MIXMATCH_LAST") {

		if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
		my $query="
				select
					mixmatchId,
					discountAmt,
					description,
					mixmatchName,
					effectiveDate,
					expireDate,
					creationDate
				from
					tblStoreMixMatch
				where
					creationDate in (select max(creationDate) from tblStoreMixMatch)								
				";


        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {

            print $q->h3("Most Recent MixMatch Definition Created");
			printResult_arrayref2($tbl_ref);

		} else {
			print $q->p("No MixMatch History found");
		}

        $dbh->disconnect;
		StandardFooter();
        exit;		
		
	} elsif ($report eq "LIST_MIXMATCH_LAST_PUBLISH") {

		if ($testing_mode) {
            print "--testing mode--<br>";
        }
	 
 
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("EXISTINGPROMO: Failed to connect to $mysqld and $posdb database");
            print "EXISTINGPROMO: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }		
		my $query="
				SELECT
					* 
				FROM
					mixmatch_pubrecord
				ORDER BY
					publish_date desc
				LIMIT 
					5
				 
				";

		#print $q->pre("$query");
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {

            print $q->h3("When the MixMatch Definitions were last published.");
			printResult_arrayref2($tbl_ref);

		} else {
			print $q->p("No MixMatch Publish History found");
		}

        $dbh->disconnect;
		StandardFooter();
        exit;				
		
	} elsif ($report eq "DELMIXMATCHRECORD") {
		# Delete the MixMatch record from the tblStoreMixMatch table in ****
		my $creationDate   = trim(($q->param("creationDate"))[0]);
		my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
		my $confirmed   = trim(($q->param("confirmed"))[0]);
		
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
	 
		my $record_count = 0;
		 
		 
		# If we have only the creation date, we need to get the MixMatch id if there are more than one
		# records with the same creation date.
		if ($creationDate) {
			unless($mixmatchId)   {
				$query="
					select mixmatchId,creationDate 
					from 
						[dbo].[tblStoreMixmatch]
					WHERE
					CONVERT(DATETIME, [creationDate]) = CONVERT(DATETIME, '$creationDate')
				";
				#print $q->pre("$query");		
				my $sth=$dbh->prepare("$query");
				$sth->execute();
				while (my @tmp = $sth->fetchrow_array()) {
					$record_count++;			
					$mixmatchId=$tmp[0];			
				}		
			}
		} else {
			LogMsgQ("DELMIXMATCHRECORD: Failed to determine creation date");
			print "DELMIXMATCHRECORD: Failed to determine creation date<br>";		
			$dbh->disconnect;
			StandardFooter();
			exit;		
		}
		 
		if ($record_count > 1) {
			my $tbl_ref = execQuery_arrayref($dbh, $query);
			if (recordCount($tbl_ref)) {
				 
				my @links = ( "$scriptname?report=DELMIXMATCHRECORD&creationDate=$creationDate&mixmatchId=".'$_' );
				printResult_arrayref2($tbl_ref, \@links);
			} else {
				print $q->b("No MixMatch Found.");
			}			
		
		}  
		unless ($confirmed) {
				$query="
					select * 
					from 
						[dbo].[tblStoreMixmatch]
					WHERE
					CONVERT(DATETIME, [creationDate]) = CONVERT(DATETIME, '$creationDate')
					and
					mixmatchId = $mixmatchId
				";		
				my $tbl_ref = execQuery_arrayref($dbh, $query);
				if (recordCount($tbl_ref)) {
					 
					
					printResult_arrayref2($tbl_ref);
					 
					print $q->p("Please confirm this is the record you wish to delete.");
					#confirm
					print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
					print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
					print $q->input({-type=>"hidden", -name=>"report", -value=>"DELMIXMATCHRECORD"});
					print $q->input({-type=>"hidden", -name=>"creationDate", -value=>"$creationDate"});
					print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
					print $q->input({-type=>"hidden", -name=>"confirmed", -value=>"1"});
					print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
							   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
									  $q->submit({ -value=>"   Confirmed   "}))
							  );
					print $q->end_form(), "\n";
					print $q->end_table(),"\n";
					StandardFooter();
					exit;					
					###
				} else {
					print $q->b("No MixMatch Found.");
				}					
		} else {
		 
			 
			$query="
			exec pspDeleteMixMatch
				\@mixmatchId = $mixmatchId,
				\@creationDate = '$creationDate'
	 
			";	
			if ($dbh->do($query)) {
				print "Deleted entry for MixMatchId $mixmatchId created $creationDate from tblStoreMixmatch <br>";
			} else {
				LogMsgQ("DELMIXMATCHRECORD: Failed to delete entry for MixMatchId $mixmatchId created $creationDate from tblStoreMixmatch");
				print "Failed to delete entry for MixMatchId $mixmatchId created $creationDate from tblStoreMixmatch<br>";
				print "query=".$query."<br>\n";
				print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
				print "errstr=".$dbh->errstr."<br>\n";
				StandardFooter();
				exit;
			}
		 
 		
		}
		 
        $dbh->disconnect;
		StandardFooter();
        exit;			
		
		
	} elsif (($report eq "DELMIXMATCH") || ($report eq "LIST_MIXMATCH") || ($report eq "EDITMIXMATCH")) {
		if ($report eq "DELMIXMATCH") {
			print $q->h3("MIXMATCH Delete Menu");
		}
		if ($report eq "LIST_MIXMATCH") {
			print $q->h3("MIXMATCH Definitions List");
		}
		if ($report eq "EDITMIXMATCH") {
			print $q->h3("Edit MIXMATCH Definitions");
		}

        # Connect to the database
        my $dbh;
		my $dsn;
		my $table = "mixmatch";
		unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
			LogMsgQ("DELMIXMATCH: Failed to connect to $mysqld and $posdb database");
			print "DELMIXMATCH: Failed to connect to $mysqld and $posdb database<br>";
			StandardFooter();
			exit;
		}
        # Show all existing MIXMATCHs that have expired
        my $query="
        select
			mixmatchId,
			f2_alt1Qty as '1Qty',			
			f3_allowGreater as 'AllowGreater',
			f66_alt1Discount as '1Discnt',
			f8_alt2Qty as '2Qty',
			f11_alt2Discount as '2Discnt',
			f19_alt3Qty as '3Qty',
			f22_alt3Discount as '3Discnt',
			f25_alt4Qty as '4Qty',
			f28_alt4Discount as '4Discnt',
			f7_mixmatchName as MixMatchName,
			f70_instanceName as DiscountName,
			f61_effectiveDate as effectiveDate,
			f62_expireDate as expireDate,
			mixmatchCreator as 'By',
			creationDate,
            status
        from
            $table
		where
			f62_expireDate < CURRENT_TIMESTAMP
        ";

        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
			my @links;
			if ($report eq "DELMIXMATCH") {
				@links = ( "$scriptname?report=DELMIXMATCH2&mixmatchId=".'$_' );
			}
			if ($report eq "EDITMIXMATCH") {
				@links = ( "$scriptname?report=EDITMIXMATCH2&mixmatchId=".'$_' );
			}

			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				$$thisrow[12]=substr($$thisrow[12],0,19);
				$$thisrow[13]=substr($$thisrow[13],0,19);
				$$thisrow[15]=substr($$thisrow[15],0,19);
			}
			# Color code the definitions to show which ones are current and which have expired.
            print $q->h3("Expired MixMatch Definitions");
			printResult_arrayref2($tbl_ref, \@links);

			if ($report eq "EDITMIXMATCH") {
				print $q->p("You may select a MixMatch Definition to modify the effective dates.  This allows an expired MixMatch discount definition to be re-used. ");
			}

        } else {
            print $q->b("no expired MixMatch discounts found<br>\n");
        }
        # Show all existing MIXMATCHs that have NOT expired
        $query="
        select
			mixmatchId,
			f2_alt1Qty as '1Qty',
			f3_allowGreater as 'AllowGreater',
			f66_alt1Discount as '1Discnt',
			f8_alt2Qty as '2Qty',
			f11_alt2Discount as '2Discnt',
			f19_alt3Qty as '3Qty',
			f22_alt3Discount as '3Discnt',
			f25_alt4Qty as '4Qty',
			f28_alt4Discount as '4Discnt',
			f7_mixmatchName as MixMatchName,
			f70_instanceName as DiscountName,
			f61_effectiveDate as effectiveDate,
			f62_expireDate as expireDate,
			mixmatchCreator as 'By',
			creationDate,
            status
        from
            $table
		where
			f62_expireDate > CURRENT_TIMESTAMP
        ";

        $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
			my @links;
			if ($report eq "DELMIXMATCH") {
				@links = ( "$scriptname?report=DELMIXMATCH2&mixmatchId=".'$_' );
			}
			if ($report eq "EDITMIXMATCH") {
				@links = ( "$scriptname?report=EDITMIXMATCH2&mixmatchId=".'$_' );
			}
			for my $datarows (1 .. $#$tbl_ref){
				my $thisrow = @$tbl_ref[$datarows];
				$$thisrow[12]=substr($$thisrow[12],0,19);
				$$thisrow[13]=substr($$thisrow[13],0,19);
				$$thisrow[15]=substr($$thisrow[15],0,19);
			}
			# Color code the definitions to show which ones are current and which have expired.
			print $q->h3("Current/Future MixMatch Definitions");
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
			if ($report eq "EDITMIXMATCH") {
				print $q->p("You may select a MixMatch Definition to modify the effective dates.  This allows an expired MixMatch discount definition to be re-used. <br />
				(<b>NOTE</b>: Be aware that these discounts have NOT expired!  Changing them will affect current or future discounts.)");
			}

        } else {
            print $q->b("no current or future MixMatch discounts found <br>\n");
        }
        $dbh->disconnect;
        StandardFooter();
        exit;
	} elsif ($report eq "EDITMIXMATCH2") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $confirmed   = trim(($q->param("confirmed"))[0]);
		print $q->h3("Edit MIXMATCH ID $mixmatchId");
        # Connect to the database
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("EDITMIXMATCH2: Failed to connect to $mysqld and $posdb database");
            print "EDITMIXMATCH2: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
		# Load the MixMatch and display the settings.
		# The only fields that can be edited are the dates and the instanceName

		# Show the specified MIXMATCH
		my $query="
		select
			mixmatchId,
			f2_alt1Qty as 1Qty,
			f3_allowGreater as AllowGreater,
			f66_alt1Discount as 1Discount,
			f8_alt2Qty as 2Qty,
			f11_alt2Discount as 2Discount,
			f19_alt3Qty as 3Qty,
			f22_alt3Discount as 3Discount,
			f25_alt4Qty as 4Qty,
			f28_alt4Discount as 4Discount,
			f7_mixmatchName as MixMatchName,
			f70_instanceName as DiscountName,
			f61_effectiveDate as effectiveDate,
			f62_expireDate as expireDate,
			mixmatchCreator as 'By',
			creationDate,

			status
		from
			mixmatch
		where
			mixmatchId = '$mixmatchId'
		";

		my $tbl_ref = execQuery_arrayref($dbh, $query);
		if (recordCount($tbl_ref)) {

			printResult_arrayref2($tbl_ref );
			# Getting the current date settings to preset the forms
			my $current_start_date=$$tbl_ref[1][12];
			my $syear=substr($current_start_date,0,4);
			my $smonth=substr($current_start_date,5,2);
			my $sday=substr($current_start_date,8,2);
			my $current_end_date=$$tbl_ref[1][13];
			my $eyear=substr($current_end_date,0,4);
			my $emonth=substr($current_end_date,5,2);
			# timelocal starts counting months at 0 so we need to decrement this by one.
			$emonth--;
			my $eday=substr($current_end_date,8,2);

			my $current_instance_name=$$tbl_ref[1][11];

			### Get the current datetime
			my @ltm = localtime();
			my $year=$ltm[5]+1900;
			my $next_year = ($year + 1);
			my $month=$ltm[4];
			#$month++;
			my $day=$ltm[3];

			# Has this discount definition expired?
			my $deftime = timelocal(0,0,0,$eday,$emonth,$eyear);
			my $curtime = timelocal(0,0,0,$day,$month,$year);
			my $replace_definition = 0;
			if ($curtime < $deftime) {
				print $q->h2("WARNING: This MixMatch definition has not yet expired");
				# Because the old definition has not expired, we are replacing an existing definition
				$replace_definition="$mixmatchId"."_"."$current_start_date";
			}
			if ($curtime > $deftime) {
				print $q->b("<br />This MixMatch definition has expired and can be safely recycled.");
			}
	 
			# Now, increment the month so that January is 1 rather than 0
			$month++;
			$emonth++;
			### Build the table
			print "<br /><br />";
			print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
			print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
			print $q->input({-type=>"hidden", -name=>"report", -value=>"EDITMIXMATCH3"});
			print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
			print $q->input({-type=>"hidden", -name=>"replace_definition", -value=>"$replace_definition"});
			print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Edit MixMatch ID $mixmatchId")));
			if ($replace_definition) {	
				# Send the current effective date parameters - We do not permit these to be edited				 
				print $q->input({-type=>"hidden", -name=>"start_month_selection", -value=>"$smonth"});
				print $q->input({-type=>"hidden", -name=>"start_day_selection", -value=>"$sday"});
				print $q->input({-type=>"hidden", -name=>"start_year_selection", -value=>"$syear"});
			}
			# Set up the date options.
			my %month_hash=(
				'1'=>'Jan',
				'2'=>'Feb',
				'3'=>'Mar',
				'4'=>'Apr',
				'5'=>'May',
				'6'=>'Jun',
				'7'=>'Jul',
				'8'=>'Aug',
				'9'=>'Sep',
				'10'=>'Oct',
				'11'=>'Nov',
				'12'=>'Dec'
			);

			my $month_options = $q->option({-value=>$smonth}, "$month_hash{abs($smonth)}");
			foreach $month (sort(keys(%month_hash))) {
				$month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
			}
			my $day_options = $q->option({-value=>$sday}, "$sday");
			foreach $day (1 ... 31) {
				$day_options .= $q->option({-value=>$day}, "$day");
			}
			my $count=0;

			my $year_options;
			unless ($replace_definition) {			
				$syear--;
				$year_options = $q->option({-value=>$syear}, "$syear");
				$syear++;
				# Set the current year as the default
				$year_options .= $q->option({-value=>$syear -selected=>'selected'}, "$syear");
				 
				#while ($count < 3)  {
				 
				while ($syear <= $year)  {
					$syear++;
					$count++;
					$year_options .= $q->option({-value=>$syear}, "$syear");
				}
		
				# If editing a current definition - don't permit changing the start date.
				print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
					   $q->td({-class=>'tabledatanb', -align=>'left'},
							  $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
						$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
								$q->td({-class=>'tabledatanb', -align=>'left'},
								$q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
						$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
								$q->td({-class=>'tabledatanb', -align=>'left'},
								$q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
					  );

			}
			$month_options = $q->option({-value=>$emonth}, "$month_hash{abs($emonth)}");
			foreach $month (sort(keys(%month_hash))) {
				$month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
			}
			$day_options = $q->option({-value=>$eday}, "$eday");
			foreach $day (1 ... 31) {
				$day_options .= $q->option({-value=>$day}, "$day");
			}			
			$count=0;

			$eyear--;
			$year_options = $q->option({-value=>$eyear}, "$eyear");
			$eyear++;
			# Set the current year as the default
			$year_options .= $q->option({-value=>$eyear -selected=>'selected'}, "$eyear");
 
			#while ($count < 3) {
			while ($eyear <= $year)  {
				$eyear++;
				$count++;
				$year_options .= $q->option({-value=>$eyear}, "$eyear");
			}
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                            $q->td({-class=>'tabledatanb', -align=>'left'},
                            $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                            $q->td({-class=>'tabledatanb', -align=>'left'},
                            $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
                  );

 
			# Name for the MixMatch
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MIXMATCH Discount Name:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"mixmatch_name", -size=>"20", -value=>"$current_instance_name"})),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );
 
			# Add the submit button
			print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
					   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
							  $q->submit({-accesskey=>'S', -value=>"   Update   "})),
						$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
						$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
						$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
						$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
					  );

			print $q->end_form(), "\n";
			print $q->end_table(),"\n";
			print $q->p("<b>Note:</b> If you need to change the Discount Name, consider simply adding a new definition
			<br />unless you are certain you will never	wish to re-use the existing name.");
			###



		} else {
			print $q->b("no records found<br>\n");
		}
        $dbh->disconnect;
        StandardFooter();
        exit;
	} elsif ($report eq "EDITMIXMATCH3") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
		my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
		my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
		my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
		my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
		my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
		my $replace_definition   = trim(($q->param("replace_definition"))[0]);	# Not certain we need this here
		 
		my $mixmatch_name   = trim(($q->param("mixmatch_name"))[0]);
		# Escape single quote
		$mixmatch_name =~ s/'/''/g;
		my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
		my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
        # The end of the discount would be at midnight of the end date
        $enddate.=" 23:59:59";
		print $q->h3("Edited MIXMATCH ID $mixmatchId");
        # Connect to the database
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("EDITMIXMATCH3: Failed to connect to $mysqld and $posdb database");
            print "EDITMIXMATCH3: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
 
		# Update the table with the new dates - Set the status to Created in case it was already published.
		$query = "
			update mixmatch
				set f61_effectiveDate = '$startdate' ,
				f62_expireDate = '$enddate',
				creationDate=CURRENT_TIMESTAMP,
				status='Created',
				mixmatchCreator='$uid',
				f70_InstanceName='$mixmatch_name'
			where
				mixmatchId = '$mixmatchId'
		";
 
		#print $q->pre("$query");
        if ($dbh->do($query)) {
			# Show the new settings
			my $query="
			select
				mixmatchId,
				f2_alt1Qty as 1Qty,
				f3_allowGreater as AllowGreater,
				f66_alt1Discount as 1Discount,
				f8_alt2Qty as 2Qty,
				f11_alt2Discount as 2Discount,
				f19_alt3Qty as 3Qty,
				f22_alt3Discount as 3Discount,
				f25_alt4Qty as 4Qty,
				f28_alt4Discount as 4Discount,
				f7_mixmatchName as MixMatchName,
				f70_instanceName as DiscountName,
				f61_effectiveDate as effectiveDate,
				f62_expireDate as expireDate,
				mixmatchCreator as 'By',
				creationDate,

				status
			from
				mixmatch
			where
				mixmatchId = '$mixmatchId'
			";

			my $tbl_ref = execQuery_arrayref($dbh, $query);
			if (recordCount($tbl_ref)) {
				printResult_arrayref2($tbl_ref );
			} else {
				print $q->p("Error: Failed to read MixMatch ID $mixmatchId after updating it!");
			}
		} else {
            print "Failed to update effective and expire dates for MixMatch ID $mixmatchId.<br>";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
        }

		print "<br /><br />";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
			$q->start_form({-action=>"$scriptname", -method=>'post'}),
			$q->input({-type=>"hidden", -name=>"report", -value=>"EDITMIXMATCH"}),
			$q->submit({ -value=>"   Edit MixMatch Discount   "}),
			$q->end_form(),
		);
		print $q->end_table(),"\n";
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->Tr(
		 $q->start_form({-action=>"$scriptname", -method=>'get'}),
		 $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHASC"}),
		 $q->submit({  -value=>"   Create MixMatch ASC   "}),
		 $q->end_form(),
		);
		print $q->end_table(),"\n";
        $dbh->disconnect;
        StandardFooter();
        exit;
	} elsif ($report eq "DELMIXMATCH2") {

        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $confirmed   = trim(($q->param("confirmed"))[0]);

        # Connect to the database
        my $dbh;
		my $dsn;
		my $table = "mixmatch";
		unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
			LogMsgQ("DELMIXMATCH: Failed to connect to $mysqld and $posdb database");
			print "DELMIXMATCH: Failed to connect to $mysqld and $posdb database<br>";
			StandardFooter();
			exit;
		}
        if ($confirmed) {
			my $query="
			delete
			from
				mixmatch
			where
				mixmatchId = $mixmatchId
			";

			if ($dbh->do($query)) {
				print $q->p("MIXMATCH ID $mixmatchId has been deleted.");
			} else {
				print "Failed to set delete MIXMATCH id $mixmatchId.<br>";
				print "query=".$query."<br>\n";
				print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
				print "errstr=".$dbh->errstr."<br>\n";
				StandardFooter();
				exit;
			}
			###
			# Show all remaining MIXMATCHs

			$query="
			select
				mixmatchId,
				f2_alt1Qty as '1Qty',
				f3_allowGreater as 'AllowGreater',
				f66_alt1Discount as '1Discount',
				f8_alt2Qty as '2Qty',
				f11_alt2Discount as '2Discount',
				f19_alt3Qty as '3Qty',
				f22_alt3Discount as '3Discount',
				f25_alt4Qty as '4Qty',
				f28_alt4Discount as '4Discount',
				f7_mixmatchName as MixMatchName,
				f70_instanceName as DiscountName,
				f61_effectiveDate as effectiveDate,
				f62_expireDate as expireDate,
				mixmatchCreator as 'By',
				creationDate,

				status
			from
				$table
			";
			my $tbl_ref = execQuery_arrayref($dbh, $query);
			if (recordCount($tbl_ref)) {
				my @links = ( "$scriptname?report=DELMIXMATCH2&mixmatchId=".'$_' );
				printResult_arrayref2($tbl_ref, \@links, '', '', '');

			} else {
				print $q->b("no records found<br>\n");
			}
			###

        } else {
            # Show the specified MIXMATCH
            my $query="
			select
				mixmatchId,
				f2_alt1Qty as '1Qty',
				f3_allowGreater as 'AllowGreater',
				f66_alt1Discount as '1Discount',
				f8_alt2Qty as '2Qty',
				f11_alt2Discount as '2Discount',
				f19_alt3Qty as '3Qty',
				f22_alt3Discount as '3Discount',
				f25_alt4Qty as '4Qty',
				f28_alt4Discount as '4Discount',
				f7_mixmatchName as MixMatchName,
				f70_instanceName as DiscountName,
				f61_effectiveDate as effectiveDate,
				f62_expireDate as expireDate,
				mixmatchCreator as 'By',
				creationDate,
				status
			from
				$table
			where
				mixmatchId = '$mixmatchId'
            ";

            my $tbl_ref = execQuery_arrayref($dbh, $query);
            if (recordCount($tbl_ref)) {
                printResult_arrayref2($tbl_ref );

                print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
                print $q->input({-type=>"hidden", -name=>"report", -value=>"DELMIXMATCH2"});
                print $q->input({-type=>"hidden", -name=>"confirmed", -value=>"confirmed"});
                print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
                print $q->submit({-accesskey=>'U', -value=>"   Confirm   "});
                print $q->end_form(), "\n";
                print $q->p("Click 'Confirm' to delete this MIXMATCH definition.");
                print "(This does not delete a MIXMATCH in the stores.)<br>";

            } else {
                print $q->b("no records found<br>\n");
            }
        }
        $dbh->disconnect;
        StandardFooter();
        exit;
   } elsif ($report eq "DEFINEMIXMATCH") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        # Define what SKUs should be on a MixMatch Discount


        my %selection_hash=(
            'class'=>'Dept-Class',
            'dept'=>'Department',
            'seasonality'=>'Seasonality',
            'vmcode'=>'VMCode',
            'sku'=>'SKU',
            'status'=>'AkronStatus',
			'vendor'=>'Vendor',
        );


        ###
        # Now get the SKU selection
        ###
        # Set up the selection options
        my $selection_options;
        foreach my $key(sort(keys %selection_hash)) {
            $selection_options .= $q->option({-value=>$key}, "$selection_hash{$key}");
        }
        ### Build the table
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH3"});
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("MIXMATCH SKU Selection:")));



        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Select Items By:"),
                    $q->td({-class=>'tabledatanb', -align=>'left'},
                    $q->Select({-name=>"selection_selection", -size=>"1"}), $selection_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );

        # Specify the MixMatch ID - Previously created in the Configurator
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MIXMATCH Discount ID:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"mixmatchId", -size=>"4"})),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );
				   
		# Add a check box for select all
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "Select All: (Dept & Class)"),
			$q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_select_all", -value=>"1"})),
			$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			);		
        # Add the submit button
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );

        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        StandardFooter();
        exit;
   } elsif (($report eq "MIXMATCH") || ($report eq "MIXMATCHUTIL")) {
		my $util=0;
		if ($report eq "MIXMATCHUTIL") {
			# The util option is used to create the definition in a utility table
			# which does not affect the main configuration table.  This is used to
			# update the tblStoreMixMatch table in **** if necessary.
			$util=1;
		}
        # A MIXMATCH discount uses the mixmatch discount in Triversity to create a Buy One Get One discount.  Typically, it will give the
        # customer a discount off of the second item.  This tool only is for creating MIXMATCH discounts.  It does not provide all of the functionality
        # of the mix match tool in Triversity.

        print $q->h3("MIXMATCH Discount Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        LogMsgQ(indent(1)."User $uid at the $report menu");
		print $q->h3("Note: Please only create a new discount if there are no existing discounts (with the same name) you can recycle.");

        print $q->p("Enter your criteria");
 
        # In the future we may have more options
        my %bogo_hash=(

            '1'=>'Buy 1 Get 1 @ X% off',
            '2'=>'Buy 2 Get 1 @ X% off',
            '3'=>'Buy 3 Get 1 @ X% off',
			
			'5'=>'Buy 2 Get X% off',
			'6'=>'Buy 3 Get X% off',
			'7'=>'Buy 4 Get X% off',
			'99'=>'Additional X% off item',
        );
		# Revised 2014-09-04 - kdg
		%bogo_hash=(
            '1'=>'Buy 1 Get 1 @ X% off',
            '2'=>'Buy 2 Get 1 @ X% off',
            '3'=>'Buy 3 Get 1 @ X% off',			
			'5'=>'Buy 2 Get Discount',
			'6'=>'Buy 2+ Get Discount',
			'7'=>'Buy 3 Get Discount',
			'8'=>'Buy 3+ Get Discount',
			'9'=>'Buy 4 Get Discount',
			'10'=>'Buy 4+ Get Discount',
			'99'=>'Additional X% off item',
        );		
 
		# Note: This option is not presented above because it requires the f31_alt5Qty field in the database
		# and of this writing, it has not yet been created.  2014-04-16 - kdg
		# '4'=>'Buy 4 Get 1 @ X% off',

        my %month_hash=(
            '1'=>'Jan',
            '2'=>'Feb',
            '3'=>'Mar',
            '4'=>'Apr',
            '5'=>'May',
            '6'=>'Jun',
            '7'=>'Jul',
            '8'=>'Aug',
            '9'=>'Sep',
            '10'=>'Oct',
            '11'=>'Nov',
            '12'=>'Dec'
        );
        my %selection_hash=(
            '1'=>'Dept-Class',
            '2'=>'Department',
            '3'=>'Seasonality',
            '4'=>'VMCode',
            '5'=>'SKU',
			'6'=>'Vendor'
        );
        my $mixmatch_options;
        my $month_options;
        my $day_options;
        my $year_options;
        my $data_options;
        my $selection_options;
        my @ltm = localtime();
        my $year=$ltm[5]+1900;
        my $month=$ltm[4];
        my $day=$ltm[3];
        $month++;
        ### Build the table
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH2"});
		print $q->input({-type=>"hidden", -name=>"util", -value=>"$util"});
        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("MIXMATCH Criteria:")));

        # Set up the date options.
        $month_options = $q->option({-value=>$month}, "$month_hash{$month}");
        foreach $month (sort(keys(%month_hash))) {
            $month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
        }
        $day_options = $q->option({-value=>$day}, "$day");
        foreach $day (1 ... 31) {
            $day_options .= $q->option({-value=>$day}, "$day");
        }
        my $count=0;
		$year--;
		$year_options = $q->option({-value=>$year}, "$year");
		$year++;
		# Set the current year as the default
		$year_options .= $q->option({-value=>$year -selected=>'selected'}, "$year");
		while ($count < 3) {
			$year++;
			$count++;
			$year_options .= $q->option({-value=>$year}, "$year");
		}

        my $increment="5";
        my $base_discount="5";
        my $max_discount="100";
        my $discount_options .= $q->option({-value=>$base_discount}, "$base_discount");
        while ($base_discount < ($max_discount - $increment)) {
            $base_discount+=$increment;
            $discount_options .= $q->option({-value=>"$base_discount"}, "$base_discount");
			if ($base_discount == 60) {
				# Insert an option for 63% which is essentially 25% added to a 50% - kdg
				$discount_options .= $q->option({-value=>"63"}, "63");
			}
			if ($base_discount == 35) {
				# Insert an option for 38%  - kdg
				$discount_options .= $q->option({-value=>"38"}, "38");
			}			
        }

        $discount_options .= $q->option({-value=>$max_discount -selected=>'selected'}, "$max_discount");

        # Set up the basic MIXMATCH options
        my $mm_define_options;
        foreach my $key(sort(keys %bogo_hash)) {
            $mm_define_options .= $q->option({-value=>$key}, "$bogo_hash{$key}");
        }


        # Set up the selection options
        foreach my $key(sort(keys %selection_hash)) {
            $selection_options .= $q->option({-value=>$key}, "$selection_hash{$key}");
        }
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                            $q->td({-class=>'tabledatanb', -align=>'left'},
                            $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                            $q->td({-class=>'tabledatanb', -align=>'left'},
                            $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
                  );
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'},
                          $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                            $q->td({-class=>'tabledatanb', -align=>'left'},
                            $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                            $q->td({-class=>'tabledatanb', -align=>'left'},
                            $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
                  );

        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MIXMATCH Purchase Qty:"),
                    $q->td({-class=>'tabledatanb', -align=>'left'},
                    $q->Select({-name=>"mm_define_selection", -size=>"1"}), $mm_define_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MIXMATCH Discount %:"),
                    $q->td({-class=>'tabledatanb', -align=>'left'},
                    $q->Select({-name=>"discount_selection", -size=>"1"}), $discount_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );

        # Name for the MixMatch
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "MIXMATCH Discount Name:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"mixmatch_name", -size=>"20"})),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                     $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );
        # Add the submit button
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );

        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        StandardFooter();
        exit;
   } elsif ($report eq "MIXMATCHASC") {
        create_mixmatch_asc();
        StandardFooter();
        exit;
   } elsif ($report eq "MIXMATCH2") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		# Find an existing mixmatch configuration that we can use or create a new one if need be.
        my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
        my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
        my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);

		my $mm_define_selection   = trim(($q->param("mm_define_selection"))[0]);
        my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
        my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
        my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
        my $mixmatch_name   = trim(($q->param("mixmatch_name"))[0]);
		my $util   = trim(($q->param("util"))[0]);
        if ($testing_mode) {
            print "--Define Selection: $mm_define_selection--<br>";
        }
		my $table = "mixmatch";
		if ($util) {
			$table = "mixmatchUtil";
		}
		# Escape any single quotes in the name
		$mixmatch_name =~ s/'/''/g;
        my $mixmatch_selection   = trim(($q->param("mixmatch_selection"))[0]);
        my $discount_selection   = trim(($q->param("discount_selection"))[0]);
        # Build the start and end date variables:  The ASC file expects it as YY-MM-DD
        $start_month_selection=sprintf("%02d",$start_month_selection);
        $start_day_selection=sprintf("%02d",$start_day_selection);
        $end_month_selection=sprintf("%02d",$end_month_selection);
        $end_day_selection=sprintf("%02d",$end_day_selection);
        my $y=substr($start_year_selection,2,2);
        my $start_date="${start_year_selection}-${start_month_selection}-${start_day_selection}";
        $y=substr($end_year_selection,2,2);
        my $end_date="${end_year_selection}-${end_month_selection}-${end_day_selection}";

        # The end of the discount would be at midnight of the end date
        $end_date.=" 23:59:59";
        # The start of the discount would be at the very beginning of the date
        $start_date.=" 00:00:00";
        my %mm_hash;
        # Set these defaults:
        $mm_hash{'f2_alt1Qty'}='NULL';
        $mm_hash{'f8_alt2Qty'}='NULL';
        $mm_hash{'f19_alt3Qty'}='NULL';
        $mm_hash{'f25_alt4Qty'}='NULL';
        $mm_hash{'f63_applyDisc'}='NULL';
        $mm_hash{'f66_alt1Discount'}='NULL';
        $mm_hash{'f11_alt2Discount'}='NULL';
        $mm_hash{'f22_alt3Discount'}='NULL';
        $mm_hash{'f28_alt4Discount'}='NULL';
        $mm_hash{'f78_RecalcPromo'}="N";
        $mm_hash{'f79_AtTotal'}="Y";
        $mm_hash{'f80_DiscountOnLower'}="Y";
        $mm_hash{'f70_InstanceName'}=$mixmatch_name;
        $mm_hash{'mixmatchCreator'}=$uid;
        $mm_hash{'f61_effectiveDate'}=$start_date;
        $mm_hash{'f62_expireDate'}=$end_date;
        # From the MixMatch Selection, we will define some parameters for specific mix match types supported
        if ($mm_define_selection == 99) {
            $mm_hash{'f2_alt1Qty'}=1;
            $mm_hash{'f7_mixmatchName'}="Additional $discount_selection%";
            $mm_hash{'f63_applyDisc'}=10;
			$mm_hash{'f66_alt1Discount'}=$discount_selection;
        } elsif ($mm_define_selection == 1) {
            $mm_hash{'f2_alt1Qty'}=1;
            $mm_hash{'f8_alt2Qty'}=1;
            $mm_hash{'f11_alt2Discount'}=$discount_selection;
            $mm_hash{'f7_mixmatchName'}="Buy1Get1";
            $mm_hash{'f63_applyDisc'}=12;
        } elsif ($mm_define_selection == 2) {
            $mm_hash{'f2_alt1Qty'}=1;
            $mm_hash{'f8_alt2Qty'}=1;
            $mm_hash{'f19_alt3Qty'}=1;
            $mm_hash{'f22_alt3Discount'}=$discount_selection;
            $mm_hash{'f7_mixmatchName'}="Buy2Get1";
            $mm_hash{'f63_applyDisc'}=12;
        } elsif ($mm_define_selection == 3) {
            $mm_hash{'f2_alt1Qty'}=1;
            $mm_hash{'f8_alt2Qty'}=1;
            $mm_hash{'f19_alt3Qty'}=1;
            $mm_hash{'f25_alt4Qty'}=1;
            $mm_hash{'f28_alt4Discount'}=$discount_selection;
            $mm_hash{'f7_mixmatchName'}="Buy3Get1";
            $mm_hash{'f63_applyDisc'}=12;
        } elsif ($mm_define_selection == 4) {
            $mm_hash{'f2_alt1Qty'}=1;
            $mm_hash{'f8_alt2Qty'}=1;
            $mm_hash{'f19_alt3Qty'}=1;
            $mm_hash{'f25_alt4Qty'}=1;
			$mm_hash{'f31_alt5Qty'}=1;
            $mm_hash{'f28_alt4Discount'}=$discount_selection;
            $mm_hash{'f7_mixmatchName'}="Buy4Get1";
            $mm_hash{'f63_applyDisc'}=12;			
        } elsif ($mm_define_selection == 5) {
            $mm_hash{'f2_alt1Qty'}=2;
            $mm_hash{'f66_alt1Discount'}=$discount_selection;            
            $mm_hash{'f7_mixmatchName'}="Buy2GetDiscount";            
			$mm_hash{'f63_applyDisc'}=12;
        } elsif ($mm_define_selection == 6) {
            $mm_hash{'f2_alt1Qty'}=2;
			$mm_hash{'f3_allowGreater'}="Y";
            $mm_hash{'f66_alt1Discount'}=$discount_selection; 
            $mm_hash{'f7_mixmatchName'}="Buy2+GetDiscount"; 
			$mm_hash{'f63_applyDisc'}=12;			
        } elsif ($mm_define_selection == 7) {
            $mm_hash{'f2_alt1Qty'}=3;
            $mm_hash{'f66_alt1Discount'}=$discount_selection;
            $mm_hash{'f7_mixmatchName'}="Buy3GetDiscount";            
			$mm_hash{'f63_applyDisc'}=12;
        } elsif ($mm_define_selection == 8) {
            $mm_hash{'f2_alt1Qty'}=3;
			$mm_hash{'f3_allowGreater'}="Y";
            $mm_hash{'f66_alt1Discount'}=$discount_selection;
            $mm_hash{'f7_mixmatchName'}="Buy3+GetDiscount";            
			$mm_hash{'f63_applyDisc'}=12;			
        } elsif ($mm_define_selection == 9) {
            $mm_hash{'f2_alt1Qty'}=4;
            $mm_hash{'f66_alt1Discount'}=$discount_selection;            
            $mm_hash{'f7_mixmatchName'}="Buy4GetDiscount";            
			$mm_hash{'f63_applyDisc'}=12;
        } elsif ($mm_define_selection == 10) {
            $mm_hash{'f2_alt1Qty'}=4;
			$mm_hash{'f3_allowGreater'}="Y";
            $mm_hash{'f66_alt1Discount'}=$discount_selection;            
            $mm_hash{'f7_mixmatchName'}="Buy4+GetDiscount";            
			$mm_hash{'f63_applyDisc'}=12;			
        }  else {
            print $q->p("Error: Mix Match Selection # $mm_define_selection is not currently supported");
            StandardFooter();
            exit;
        }
        my @ltm = localtime();
        my $date_label2=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB 					
        my %mixmatch_hash=(
            '1'=>'Buy 1 Get 1 @ X% off',
            '2'=>'Buy 2 Get 1 @ X% off',
            '3'=>'Buy 3 Get 1 @ X% off',
			'4'=>'Buy 4 Get 1 @ X% off',
			'5'=>'Buy 2 Get Discount',
			'6'=>'Buy 2+ Get Discount',
			'7'=>'Buy 3 Get Discount',
			'8'=>'Buy 3+ Get Discount',
			'9'=>'Buy 4 Get Discount',
			'10'=>'Buy 4+ Get Discount',
			'99'=>'Additional X% off item',	
        );
        my %month_hash=(
            '1'=>'Jan',
            '2'=>'Feb',
            '3'=>'Mar',
            '4'=>'Apr',
            '5'=>'May',
            '6'=>'Jun',
            '7'=>'Jul',
            '8'=>'Aug',
            '9'=>'Sep',
            '10'=>'Oct',
            '11'=>'Nov',
            '12'=>'Dec'
        );
        my %selection_hash=(
            '1'=>'Dept-Class',
            '2'=>'Department',
            '3'=>'Seasonality',
            '4'=>'VMCode',
            '5'=>'SKU',
			'6'=>'Vendor'
        );

        # Connect to the database
        my $dbh;
		my $dsn;

		unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
			LogMsgQ("DELMIXMATCH: Failed to connect to $mysqld and $posdb database");
			print "DELMIXMATCH: Failed to connect to $mysqld and $posdb database<br>";
			StandardFooter();
			exit;
		}

		# Create a new mixmatch
		# First find the next mixmatch id number
		my $mixmatchId=0;

		my $query = "select max(mixmatchId) from $table";

		my $sth=$dbh->prepare("$query");
		$sth->execute();
		while (my @tmp = $sth->fetchrow_array()) {
			$mixmatchId=$tmp[0];
		}
		if (($mixmatchId) || ($mixmatchId == 0)) {
			# Increment the mixmatch id
			$mixmatchId++;
		} else {
			print $q->p("Error: Failed to create a new MIXMATCH");
			StandardFooter();
			exit;
		}
		print $q->p("Creating new mixmatch $mixmatch_name id $mixmatchId");
		my $status="Created";
		# Escape any single quotes in the name
		my $mixmatch_name2 = $mixmatch_name;
		$mixmatch_name2=~s/'/''/g;
		$query = "
			insert into $table
			VALUES (
				$mixmatchId,
				$mm_hash{'f2_alt1Qty'},
				'$mm_hash{'f3_allowGreater'}',
				$mm_hash{'f8_alt2Qty'},
				$mm_hash{'f19_alt3Qty'},
				$mm_hash{'f25_alt4Qty'},
				$mm_hash{'f66_alt1Discount'},
				$mm_hash{'f11_alt2Discount'},
				$mm_hash{'f22_alt3Discount'},
				$mm_hash{'f28_alt4Discount'},
				$mm_hash{'f63_applyDisc'},
				'$mm_hash{'f78_RecalcPromo'}',
				'$mm_hash{'f79_AtTotal'}',
				'$mm_hash{'f80_DiscountOnLower'}',
				'$mm_hash{'f7_mixmatchName'}',
				'$mm_hash{'f70_InstanceName'}',
				'$mm_hash{'mixmatchCreator'}',
				CURRENT_TIMESTAMP,
				'$mm_hash{'f61_effectiveDate'}',
				'$mm_hash{'f62_expireDate'}',
				'$status')
		";


		if ($dbh->do($query)) {
			print $q->p("Created mixmatch id $mixmatchId");
			#$found_mixmatch=$mixmatchId;
		} else {
			print "Failed to create mixmatch $mixmatchId<br>";
			print "query=".$query."<br>\n";
			print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
			print "errstr=".$dbh->errstr."<br>\n";
			StandardFooter();
			exit;
		}

        print "<pre>
        Finished creating mixmatch:

        > MIXMATCH Number: $mixmatchId
        > MIXMATCH Name: $mixmatch_name
        > MIXMATCH Start Date: $start_date
        > MIXMATCH End Date: $end_date
        > MIXMATCH Type: $mixmatch_hash{$mm_define_selection}
        > Discount: $discount_selection

        </pre>";
		#########################
		#	Create the ASC file
		#########################

		print $q->p();
		print $q->p("------------------------------------------------------------------------------------");
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
		print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHASC"});
		print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Create ASC for current MixMatch Definitions:")));
		# Add the submit button
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
					  $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
				$q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
			  );
		print $q->end_form(), "\n";
		print $q->end_table(),"\n";
		#print $q->p("Use this option if you only wish to create an ASC file containing all the MIXMATCH definitions. ");
=pod
        ###
        # Now get the SKU selection
        ###
        # Set up the selection options
        my $selection_options;
        foreach my $key(sort(keys %selection_hash)) {
            $selection_options .= $q->option({-value=>$key}, "$selection_hash{$key}");
        }
        ### Build the table
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH3"});
        print $q->input({-type=>"hidden", -name=>"mixmatch_selection", -value=>"$mixmatchId"});
        print $q->input({-type=>"hidden", -name=>"mixmatch_name", -value=>"$mixmatch_name"});
        print $q->input({-type=>"hidden", -name=>"start_date", -value=>"$start_date"});
        print $q->input({-type=>"hidden", -name=>"end_date", -value=>"$end_date"});
        print $q->input({-type=>"hidden", -name=>"discount_selection", -value=>"$discount_selection"});
        print $q->input({-type=>"hidden", -name=>"mixmatch_selection", -value=>"$mixmatch_selection"});

        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("MIXMATCH SKU Selection:")));



        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Select Items By:"),
                    $q->td({-class=>'tabledatanb', -align=>'left'},
                    $q->Select({-name=>"selection_selection", -size=>"1"}), $selection_options),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );

        # Add the submit button
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                    $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                  );

        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
=cut
        StandardFooter();
        exit;
	} elsif ($report eq "MIXMATCH3") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $dsn = "driver={SQL Server};Server=****;database=dbNavision;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');		
        # Determine what SKUs are going to be on the MIXMATCH.  Here you decide how you will select SKUs.
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $selection_selection   = trim(($q->param("selection_selection"))[0]);
		my $cb_select_all   = trim(($q->param("cb_select_all"))[0]);
		my $checked = '';
		if ($cb_select_all) {
			$checked = '-checked';
			if ($selection_selection eq "class") {
				print $q->p("Note: Not selecting certain classes");
			}
			if ($selection_selection eq "dept") {
				print $q->p("Note: Not selecting certain departments");
			}			
		}
        my $continue=1;
        unless ($mixmatchId) {
            print $q->p("Error: No MixMatchID specified");
            $continue=0;
        }
        unless ($selection_selection) {
            print $q->p("Error: No Selection specified");
            $continue=0;
        }
        unless ($continue) {
            StandardFooter();
            exit;
        }
        #######################
        if ($selection_selection eq "dept") {
            # Show checkboxes to select department - Remember, Akron calls departments what Triversity calls groups.

            # Build the group options
            my $group_asc_file="/temp/Merchandising/GROUP.ASC";
            my %group_hash;
            if (-e $group_asc_file) {
                open (FILE,"$group_asc_file");
                while (<FILE>) {
                    my @tmp=split(/,/);
                    my $group=$tmp[0];
                    my $grouplabel=$tmp[2];
                    $group_hash{$group}=$grouplabel;
                }
                close FILE;
            } else {
                print $q->p("Error: Cannot find $group_asc_file");
                StandardFooter();
                exit;
            }

            print $q->p("Select which departments you wish to include in mixmatch #${mixmatchId}:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            foreach my $key (sort(keys %group_hash)) {
				my $selected = $checked;
				if (($key =~ /^80/) || ($key =~ /^81/) || ($key =~ /^99/)) {
					# Certain departments are not selected by default
					$selected = '';
				}				 
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "$group_hash{$key}:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_group_selection_$key", -value=>"$key", $selected }))
                      );
            }
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            StandardFooter();
            exit;

        } elsif ($selection_selection eq "class") {
            # Show checkboxes to select dept-class
            # Build the department-class options  (Remember, Akron calls department-class what Triversity calls departments.          
			my $dept_asc_file="/temp/Merchandising/DEPT.ASC";
            my %dept_hash;
=pod
            if (-e $dept_asc_file) {
                open (FILE,"$dept_asc_file");
                while (<FILE>) {
                    my @tmp=split(/,/);
                    my $dept=$tmp[0];
                    my $deptlabel=$tmp[2];			 
                    $dept_hash{$dept}=$deptlabel;				
                }
                close FILE;
            } else {
                print $q->p("Error: Cannot find $dept_asc_file");
                StandardFooter();
                exit;
            }
=cut 	
			# Get the departments from Navision
			%dept_hash=();
 			
			$query = "
			select
				d.Dept,
				c.Class,
				c.[Description],
				c.[POS Description]
			from
				[Villages\$Dept_Class] d
				join [Villages\$Class] c on d.[Dept] = c.[Dept] and d.[List Type] = c.[List Type]
			where
				d.[List Type] = 0
				and c.Class <> '00'
			union
			select
				d.[Dept],
				'',
				d.[Description],
				d.[POS Description]
			from
				[Villages\$Dept_Class] d
			where
				d.[List Type] = 0

			order by 1,2
			";			
			my $sth=$dbh->prepare("$query");
			$sth->execute();
			while (my @tmp = $sth->fetchrow_array()) {
				 
				my $dept=$tmp[0];
				
				my $class=$tmp[1];
				if ($class eq '') {
					$class="00";
				}
				my $desc=$tmp[2];
				my $dept_class=$dept.$class;
				next if (($dept_class > 7099) && ($dept_class < 8000));
				my $label = "$dept_class $desc";
				$dept_hash{$dept_class}=$label;
				 
			}
		
			# Add Misc Sku
			$dept_hash{9999}="MISCELLANEOUS SKU";
			
			###
 		

            print $q->p("Select which Classes you wish to include in  mixmatch #${mixmatchId}:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            foreach my $key (sort(keys %dept_hash)) {
				my $selected = $checked;
				if (($key =~ /^80/) || ($key =~ /^81/) || ($key == 9999)) {
					# Certain Classes are not selected by default
					$selected = '';
				}				
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "$dept_hash{$key}:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_dept_selection_$key", -value=>"$key", $selected}))
                      );
            }
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            StandardFooter();
            exit;
        } elsif ($selection_selection eq "vmcode") {
            # Show checkboxes to select VMCode
            # Build the VMCode options


            print $q->p("Select which VMCode you wish to include in mixmatch #${mixmatchId}:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            foreach my $key (sort(keys %vmcode_hash)) {
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "$vmcode_hash{$key} - $key:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_vmcode_selection_$key", -value=>"$key"}))
                      );
            }
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({  -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            StandardFooter();
            exit;
        } elsif ($selection_selection eq "vendor") {
            # Show checkboxes to select Vendor
            # Build the Vendor options
			# Get the Vendors from Navision
			my %vendor_hash=(); 
			$query = "
				SELECT
					[Triversity Vendor ID],
					Name,
					Address,
					City,
					[County],
					[Post Code],
					'',
					[Phone No_],
					[Fax No_],
					[Contact],
					'',
					[E-Mail],

					[Home Page]

				FROM
					[dbNavision].[dbo].[Villages\$Vendor]
				where
					[Triversity Vendor ID] is not null
				and
					[Triversity Vendor ID] not like ''
				order by
					[Triversity Vendor ID]
	 
			";			
			my $sth=$dbh->prepare("$query");
			$sth->execute();
			while (my @tmp = $sth->fetchrow_array()) {				 
				my $Vendor_ID=$tmp[0];				
				my $Vendor_Name=$tmp[1];
 
				$vendor_hash{$Vendor_ID}=$Vendor_Name;
				 
			}				

            print $q->p("Select which Vendor you wish to include in mixmatch #${mixmatchId}:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            foreach my $key (sort(keys %vendor_hash)) {
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "$vendor_hash{$key} - $key:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_vendor_selection_$key", -value=>"$key"}))
                      );
            }
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({  -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            StandardFooter();
            exit;			
        } elsif ($selection_selection eq "seasonality") {
            # Show checkboxes to select Seasonality
            # Build the Seasonality options

			my %seasonality_hash = ();
 
			#UserFlag1
			$query = "
			SELECT
				[Code] ,[Description]
			FROM
				[dbNavision].[dbo].[Villages\$TTV System Rules]
			where
				[Ruleid] like 'SEASONAL'
				and [Status] < 2
			order by [Code]
			";

			my $sth=$dbh->prepare("$query");
			$sth->execute();

			while (my @tmp = $sth->fetchrow_array()) {
				my $code = $tmp[0];
				my $desc = $tmp[1];
				if (($code) && ($desc)) {
					$seasonality_hash{$code} = $desc;
				}
			}		
 
			##
            print $q->p("Select which Seasonality you wish to include in mixmatch #${mixmatchId}:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            foreach my $key (sort(keys %seasonality_hash)) {
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "$key - $seasonality_hash{$key}:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_seasonality_selection_$key", -value=>"$key"}))
                      );
            }
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({  -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            StandardFooter();
            exit;			
        } elsif ($selection_selection eq "sku") {
            # Show a text box to enter SKU numbers
            print $q->h3("Option 1:");
            print $q->p("Enter the SKUs you wish to include in mixmatch #${mixmatchId} separated by a coma:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});


            print $q->Tr($q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'text', -name=>"sku_selection", -size=>"35"})));
            print $q->Tr($q->td($q->submit({-accesskey=>'G', -value=>"   Go!   "})));
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
           # Provision for Uploading a file
            print $q->h3("Option 2:");
            print "<pre>You may upload a comma separated file with the SKUs to include in the MixMatch listed in the first column.</pre>";


            print $q->p("Select the file to be uploaded.");
			#upload($scriptname,$q,"MIXMATCHUPLOAD");
			
            print $q->start_multipart_form({-action=>"$scriptname", -method=>'post'}), "\n";
            #print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHUPLOAD"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            print $q->filefield
                        (-name      => 'source',
                        -size      => 40,
                        -maxlength => 80
                        );

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "\&nbsp;"),
                );

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                $q->submit({ -value=>"   ENTER   "}))
                );
			print $q->end_form(), "\n";
            StandardFooter();
            exit;
        } elsif ($selection_selection eq "status") {
            # Show checkboxes to select Akron Status
            # Build the Akron Status options
            my $dept_asc_file="DEPT.ASC";
            my %akron_status_hash=(
                'ACTIVE' => 'Active',
                'DISCONTD' => 'Discounted',
                'OBSOLETE' => 'Obsolete',
                'SALE' => 'Sale',
                'NOSELL' => 'NoSell'
            );

            print $q->p("Select which Status' you wish to include in  mixmatch #${mixmatchId}:");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            foreach my $key (sort(keys %akron_status_hash)) {
                print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'}, "$akron_status_hash{$key}:"),
                       $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-type=>'checkbox', -name=>"cb_akron_status_$key", -value=>"$key"}))
                      );
            }

            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                      );
            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            StandardFooter();
            exit;

        } else {
            print "Sorry, $selection_selection is currently not supported.<br>";
        }
        ###
		$dbh->disconnect;		
        StandardFooter();
        exit;
	} elsif ($report eq "MIXMATCHUPLOAD") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        $CGI::POST_MAX = 1024 * 100;  # maximum upload filesize is 100K
        my $verified   = trim(($q->param("verified"))[0]);
        my $selection_selection   = trim(($q->param("selection_selection"))[0]);
        my $mixmatchId= trim(($q->param("mixmatchId"))[0]);
        my $source   = trim(($q->param("source"))[0]);
        my $file   = trim(($q->param("file"))[0]);
        my $filename;
        my @file_contents;
        #my $script='//****/mlink/cron/createXferDoc.pl';
        my @results;
        print $q->h3("MixMatch SKU Define Upload for MixMatch Discount $mixmatchId");

        if ($verified) {
            # Now call the perl script to process the file just uploaded
            unless (-e $file) {
                print $q->p("ERROR: #2 - Failed to locate $file");
                StandardFooter();
                exit;
            }
            my $basefile=basename($file);
            processMixMatchInputFile($file,$mixmatchId);
            print $q->p("Deleting temporary files");
            unlink $file;
            print $q->p("Done");
            # - put a button here to publish
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCH4"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"upload"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

            print $q->submit({ -value=>"   Continue  "});
            print $q->end_form(), "\n";
        } else {
            # Upload the file
            if ($source) {
                $source=~s/\\/\//g;
            } else {
                print $q->p("ERROR: #1 - Failed to find $source");
                StandardFooter();
                exit;
            }
            my $upload_filehandle = $q->upload('source');
            my $file=basename($source);

            $file="${upload_dir}/${file}";
            open ( UPLOADFILE, ">$file" ) or die "Error: ($file) $!";
            binmode UPLOADFILE;

            while ( <$upload_filehandle> ) {
                print UPLOADFILE;
                push(@file_contents,$_);
            }
            close UPLOADFILE;
            my @header=(
                "SKU"
            );
            my $counter=0;

            print "\n", $q->start_table({-class=>'tableborder', -valign=>'top',  -border=>'1'}), "\n";
            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef}, \@header));

            foreach my $f (@file_contents) {
                $counter++;
                my @row=split(/,/,$f);

                print $q->Tr($q->td({-class=>'tableborder' }, \@row) );
            }
            print $q->end_table(),"\n";
            print "</br>\n";
            print $q->p("$counter SKUs ");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHUPLOAD"});
            print $q->input({-type=>"hidden", -name=>"selection_selection", -value=>"$selection_selection"});
            print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
            print $q->input({-type=>"hidden", -name=>"file", -value=>"$file"});
            print $q->input({-type=>"hidden", -name=>"verified", -value=>"yes"});
            print $q->submit({-accesskey=>'G', -value=>"   Process SKU Define File   "});
            print $q->end_form(), "\n";
        }
        print "<br>\n";
        StandardFooter();
        exit;

    } elsif ($report eq "MIXMATCH4") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
    # Here we create the tsv file that defines the SKU selection.  This TSV file is sent down to the store to activate the appropriate
    # SKUs in the Triversity database.

        # Get the specs for the mix match
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $selection_selection   = trim(($q->param("selection_selection"))[0]);
        my @selection_list;
        ## Second pass parameters: ##
        my $sku_selection   = trim(($q->param("sku_selection"))[0]);
        if ($sku_selection) {
            $selection_selection="sku";
            @selection_list=split(/\,/,$sku_selection);
        } else {
			foreach my $name ($q->param) {				
				my $add_choice=0;
				if ($name =~ /^cb_group/) {
					$selection_selection="group";
					$add_choice=1;
				} elsif ($name =~ /^cb_dept/) {
					$selection_selection="department";
					$add_choice=1;					
				} elsif ($name =~ /^cb_akron_status_/) {
					$selection_selection="status";
					$add_choice=1;
				} elsif ($name =~ /^cb_vmcode_/) {
					$selection_selection="vmcode";
					$add_choice=1;
				} elsif ($name =~ /^cb_vendor_/) {
					$selection_selection="vendor";
					$add_choice=1;					
				} elsif ($name =~ /^cb_seasonality_/) {
					$selection_selection="seasonality";
					$add_choice=1;
				}
				
				if ($add_choice) {
					my $choice   = trim(($q->param("$name"))[0]);
					push(@selection_list,$choice);
				}
			}
		}

=pod
        foreach my $name ($q->param) {
            next unless $name =~ /^cb_dept/;
            my $dept   = trim(($q->param("$name"))[0]);
            push(@selection_list,$dept);
            $selection_selection="department";
        }
        foreach my $name ($q->param) {
            next unless $name =~ /^cb_akron_status_/;
            my $status   = trim(($q->param("$name"))[0]);
            push(@selection_list,$status);
            $selection_selection="status";
        }
=cut

        unless ($selection_selection eq "upload") {
            # If we uploaded a file, we take care of all of this elsewhere
            my $resultCode;
            # If we have selected SKUs:  (This would be after having passed through this report once and made selections
            if ($testing_mode) {
                print "--testing mode--<br>";
            }
            if ($selection_selection) {
                $resultCode=create_mixmatch_by_sku($mixmatchId,$selection_selection,\@selection_list,$uid);
            } else {
                print $q->p("Error: Could not determine how to select SKUs");
                StandardFooter();
                exit;
            }


            if ($resultCode) {
                print $q->h4("Successfully created MixMatch SKU Selection TSV");
            } else {
                print $q->h3("Error attempting to create MixMatch SKU Selection TSV ");
                StandardFooter();
                exit;
            }
        }
        print $q->h3("MIXMATCH Discount Publish Menu");

        print $q->h3("Store Selection Menu");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        LogMsgQ(indent(1)."User $uid at the Store Selection menu");

        ##########
        print $q->p("Select which stores you wish to publish the SKU selection for MixMatch #${mixmatchId} to:");
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMPUBCONFIRM"});
        print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});

        print $q->input({-type=>"hidden", -name=>"pub_label", -value=>"SKU Selection for MixMatch #${mixmatchId}"});

        print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>3}, $q->b("Enter Criteria")));
        print $q->Tr($q->td({-class  => 'tabledatanb',-nowrap => undef,-align  => 'right'},"Store Selection Group:"),
         $q->td({ -class => 'tabledatanb', -align => 'left' },$q->radio_group(
               {
                    -name    => "selection",
                    -values  => [ 'Opt1: All Company', 'Opt2: All Contract', 'Opt3: All Company & Contract',],
                    -default => 'none',
                    -linebreak=>'true',
               }),)
        );
		#-values  => [ 'Opt1: All Company', 'Opt2: All Company except Ephrata', 'Opt3: All Company except Ephrata & Montreat', 'Opt4: All Contract'],		
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Specific Store Exclusion:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"storeExclusion", -size=>"50"}))
                  );
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Specific Store Inclusion:"),
                   $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({-accesskey=>'S', -type=>'text', -name=>"storeInclusion", -size=>"50"}))
                  );

        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Go!   "}))
                  );
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        print "<br>Select from Option 1, 2, 3, or 4.  To further modify these options, you can exclude specific<br>";
        print "stores by putting their store number in the 'Exclusion' list.  Similarly, if you want to <br>";
        print "include more stores, put their store number in the 'Inclusion' list.  (If including or excluding<br>";
        print "more than one store, separate the store numbers with a comma.)<br>";
        print "Note: Selection from options 1, 2, 3, or 4 is not required.  You can simply include stores by <br>";
        print "putting the store number in the 'Inclusion' box.<br>";
        ########
        StandardFooter();
        exit;

 } elsif ($report eq "MMPUBCONFIRM") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->h3("Publish Confirmation");
        my $selection   = trim(($q->param("selection"))[0]);
        my $storeInclusion   = trim(($q->param("storeInclusion"))[0]);
        my $storeExclusion   = trim(($q->param("storeExclusion"))[0]);
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);

        my $pub_label   = trim(($q->param("pub_label"))[0]);
		LogMsgQ("Publish Confirmation:  Selection: $selection");
		if ($storeInclusion) {
			LogMsgQ("Publish Confirmation:  Inclusion: $storeInclusion");
		}
		if ($storeExclusion) {
			LogMsgQ("Publish Confirmation:  Exclusion: $storeExclusion");
		}
		LogMsgQ("Publish Confirmation:  MixMatchID: $mixmatchId");
        my @storeIds;
        my @exclusions=split(/,/,$storeExclusion);
        my @inclusions=split(/,/,$storeInclusion);

        if ($selection =~ /opt1/i) {
            @storeIds='V1';
        }
        if ($selection =~ /opt2/i) {
            @storeIds='V2';
            #@exclusions=("7037");
            #push(@exclusions,"7037");
        }
        if ($selection =~ /opt3/i) {
            @storeIds='all';
            #@exclusions=("7037","9277");
            #push(@exclusions,"7037");
            #push(@exclusions,"9277");
        }
        #if ($selection =~ /opt4/i) {
        #    @storeIds='V2';
        #}		
		 
        my @storelist = processStoreOptions($mssql_dbh, \@storeIds);
        my @tmp;
        # Exclude stores on the exclusion list
        STORE: foreach my $store (@storelist) {
			# Trim off any blank spaces
			$store=~ s/ //g;
            foreach my $e (@exclusions) {
                if ($store == $e) {
                    print "Excluding store $store<br>";
                    next STORE if ($e == $store);
                }
            }
            push(@tmp,$store);
        }
        @storelist=@tmp;
        # Include stores on the inclusion list
        foreach my $i (@inclusions) {
			# Trim off any blank spaces
			$i=~ s/ //g;
            print "Including store $i<br>";
            push(@storelist,$i);
        }

        if ($#storelist > -1) {
            print $q->p("Publish SKU Definition for MixMatch #${mixmatchId} to the following stores:");
			my $counter=0;
            foreach my $s (@storelist) {
                print "$s $storename_hash{$s}<br>";
				$counter++;
            }
			LogMsgQ("Published SKU Definition for MixMatch #${mixmatchId} to $counter stores."); 
        } else {
            print $q->p("Error: No store specified");
			LogMsgQ("Error: No store specified");
            StandardFooter();
            exit;
        }
        print "<br>";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PUBMIXMATCH"});
        print $q->input({-type=>"hidden", -name=>"pub_label", -value=>"$pub_label"});
        print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
        print $q->input({-type=>"hidden", -name=>"storeInclusion", -value=>"$storeInclusion"});
        print $q->input({-type=>"hidden", -name=>"storeExclusion", -value=>"$storeExclusion"});
        print $q->input({-type=>"hidden", -name=>"selection", -value=>"$selection"});
        print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                          $q->submit({-accesskey=>'G', -value=>"   Confirmed   "}))
                  );
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        StandardFooter();
        exit;
    } elsif ($report eq "PUBMIXMATCH") {

        # Report: PUBMIXMATCH -  publish the mix match SKU selection TSV here

        my $selection   = trim(($q->param("selection"))[0]);
        my $storeInclusion   = trim(($q->param("storeInclusion"))[0]);
        my $storeExclusion   = trim(($q->param("storeExclusion"))[0]);
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $pub_label   = trim(($q->param("pub_label"))[0]);

        print $q->h3("Publishing $pub_label to selected stores");
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my @storeIds;
        my @exclusions=split(/,/,$storeExclusion);
        my @inclusions=split(/,/,$storeInclusion);

        if ($selection =~ /opt1/i) {
            @storeIds='V1';
        }
        if ($selection =~ /opt2/i) {
            @storeIds='V2';
            #@exclusions=("7037");
            #push(@exclusions,"7037");
        }
        if ($selection =~ /opt3/i) {
            @storeIds='all';
            #@exclusions=("7037","9277");
            #push(@exclusions,"7037");
            #push(@exclusions,"9277");
        }
        #if ($selection =~ /opt4/i) {
         #   @storeIds='V2';
        #}				
        my @storelist = processStoreOptions($mssql_dbh, \@storeIds);
        my @tmp;
        # Exclude stores on the exclusion list
        STORE: foreach my $store (@storelist) {
			# Trim off any blank spaces
			$store=~ s/ //g;
            foreach my $e (@exclusions) {
                if ($store == $e) {
                    # Exclude from the store list.
                    print "Excluding store $store<br>";
                    LogMsgQ("Excluding store $store");
                    next STORE if ($e == $store);
                }
            }
            push(@tmp,$store);
        }
        @storelist=@tmp;
        # Include stores on the inclusion list
        foreach my $i (@inclusions) {
			# Trim off any blank spaces
			$i=~ s/ //g;
            push(@storelist,$i);
            print "Including store $i<br>";
            LogMsgQ("Including store $i");
        }


        my $store_counter=0;
        # Connect to the database
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PUBMIXMATCH: Failed to connect to $mysqld and $posdb database");
            print "PUBMIXMATCH: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
        foreach my $store (@storelist) {
            print "Publishing to store $store $storename_hash{$store}...<br>";
            LogMsgQ("Publishing the MixMatch.TSV to store $store");
            my $targetfile = "$dnload_dir/$store/MIXMATCHSKU.TSV";
            # Open TSV in append mode in case there is already a mixmatch queued
            if (open(TSV,">>$targetfile")) {
                if (open(INPUT,"$mixmatchSKUfile")) {
                    while (<INPUT>) {
                        print TSV "$_";
                    }
                } else {
                    print $q->p("Error:  Could not open $mixmatchSKUfile");
                    LogMsgQ("Error:  Could not open $mixmatchSKUfile");
                    StandardFooter();
                    exit;
                }
                close INPUT;
                close TSV;
                #print $q->p("Created $targetfile");
                LogMsgQ("Created $targetfile");
				print "Successfully published to store $store $storename_hash{$store}<br>";
				# Append to the newfiles.txt file
				open FILE, ">> $dnload_dir/$store/newfiles.txt";
				print FILE "MIXMATCHSKU.TSV\tC:/MLINK/Akron\n";
				close FILE;

				# Update the counter
				$store_counter++;				
            } else {
                print $q->p("Error:  Could not create $targetfile");
                LogMsgQ("Error:  Could not create $targetfile");
				print "Failed to publish to store $store $storename_hash{$store}!<br>";
                #StandardFooter();
                #exit;
            }
			# Update the database
			my $query = "
			replace into mixmatch_store (mixmatchId,storeId) values ('$mixmatchId','$store')
			";
			unless ($dbh->do($query)) {
				print "Failed to record $store in mixmatch_store table<br>";
				print "query=".$query."<br>\n";
				print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
				print "errstr=".$dbh->errstr."<br>\n";
				StandardFooter();
				exit;
			} else {
				LogMsgQ("PUBMIXMATCH: Updated mixmatch_store for MixMatch $mixmatchId and store $store.");
			}			

        }

        print $q->p("Successfully published MixMatch SKU configuration for $mixmatchId to $store_counter store(s)");
        unlink $promotxnfile;
        print $q->p("Done");
        # update the status of the bogo/mixmatch
        if ($pub_label =~ /bogo/i) {
            my $query = "
            update
                bogo
            set status = 'pubd'
            where
                bogoId = '$mixmatchId'
            ";


            unless ($dbh->do($query)) {
                print "Failed to set mixmatch status in mixmatch table<br>";
                print "query=".$query."<br>\n";
                print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
                print "errstr=".$dbh->errstr."<br>\n";
			} else {
				LogMsgQ("PUBPROMO: Setting status of bogo $mixmatchId to pubd.");
			}
        } else {
            my $query = "
            update
                mixmatch
            set status = 'pubd'
            where
                mixmatchId = '$mixmatchId'
            ";

            unless ($dbh->do($query)) {
                print "Failed to set mixmatch status in mixmatch table<br>";
                print "query=".$query."<br>\n";
                print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
                print "errstr=".$dbh->errstr."<br>\n";            
			} else {
				LogMsgQ("PUBPROMO: Setting status of mixmatch $mixmatchId to pubd.");
			}
        }
        $dbh->disconnect;
        StandardFooter();
        exit;
} elsif ($report eq "PUBMIXMATCHASC") {

        # Report: PUBMIXMATCHASC -  publish the mix match ASC file

        my $selection   = trim(($q->param("selection"))[0]);
        my $storeInclusion   = trim(($q->param("storeInclusion"))[0]);
        my $storeExclusion   = trim(($q->param("storeExclusion"))[0]);
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        my $pub_label   = trim(($q->param("pub_label"))[0]);

        print $q->h3("Publishing MixMatch Definition file");

        my @storeIds='all';

        my @storelist = processStoreOptions($mssql_dbh, \@storeIds);
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $store_counter=0;
        # Connect to the database
        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PUBMIXMATCHASC: Failed to connect to $mysqld and $posdb database");
            print "PUBMIXMATCHASC: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
		if ($testing_mode) {
			# Publish only to store 99
			@storelist=("0099");
		}
 	
		foreach my $store (@storelist) {
			if ($store eq "99") {
				$store="0099";
			}
			print "Publishing to store $store $storename_hash{$store}<br>";
			LogMsgQ("Publishing the MixMatch.ASC to store $store");
			my $targetfile = "$dnload_dir/$store/mixmatch.ASC";

			# Append to the newfiles.txt file
			if (open FILE, ">> $dnload_dir/$store/newfiles.txt") {
				print FILE "mixmatch.ASC\tC:/XPS/parm\n";
				close FILE;
				if (copy($selection,$targetfile)) {
					# Update the counter
					$store_counter++;
				} else {
					print "Error: Failed to open copy $selection to $targetfile!<br />";
				}
			} else {
				print "Error: Failed to open $dnload_dir/$store/newfiles.txt for store $store<br />";
			}
		}
 		
		# Save a copy of the MixMatch.ASC file in the source safe		 
		#my $dnload_dir  = "//****/MLINK/ToStore";
		my $targetfile = "$dnload_dir/newstore/last_version/ASC/production/mixmatch.ASC";
		 
		if (copy($selection,$targetfile)) {
			# Update the counter
			$store_counter++;
		} else {
			print "Error: Failed to open copy $selection to $targetfile!<br />";
		}		
		
        print $q->p("Successfully published MixMatch definition to $store_counter store(s)");

        print $q->p("Done");
		unless ($testing_mode) {
			if ($store_counter) {
				# update the status of the  mixmatch

				my $query = "
				update
					mixmatch
				set status = 'pubd'
				where
					status NOT LIKE 'pubd'

				";
				unless ($dbh->do($query)) {
					print "Failed to set mixmatch status in mixmatch table<br>";
					print "query=".$query."<br>\n";
					print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
					print "errstr=".$dbh->errstr."<br>\n";
				} else {
					LogMsgQ("PUBPROMO: Setting status of mixmatch to pubd.");
				}
				
				# Record the publishing in the pubrecord table;
				my $query = "
				insert into mixmatch_pubrecord
					VALUES (
						CURRENT_TIMESTAMP,
						'$uid')
				";
				unless ($dbh->do($query)) {
					print "Failed to update mixmatch_pubrecord table<br>";
					print "query=".$query."<br>\n";
					print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
					print "errstr=".$dbh->errstr."<br>\n";
				} else {
					LogMsgQ("PUBPROMO: User $uid Recorded publishing of MixMatch ASC.");
				}				
			}
		}
        $dbh->disconnect;
        StandardFooter();
        exit;

    } elsif ($report eq "REPORTS") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Reports");
        LogMsgQ(indent(1)."User $uid at the Reports menu");
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMORPT"});
        print $q->submit({-accesskey=>'G', -value=>"   PromoReports - By Promotion   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREPROMORPT"});
        print $q->submit({-accesskey=>'G', -value=>"   PromoReports - By Store   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"CURRENTPROMORPT"});
        print $q->submit({-accesskey=>'G', -value=>"   PromoReports - Currently in Stores  - By Promotion "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";				
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"CURRENTSTOREPROMORPT"});
        print $q->submit({-accesskey=>'G', -value=>"   PromoReports - Currently in Stores  - By Store "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";		
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"XFRRPT"});
        print $q->submit({-accesskey=>'G', -value=>"   TransferReports   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
        #print $q->end_table(),"\n";
=pod # Currently disabled
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMRPT"});
        print $q->submit({-accesskey=>'G', -value=>"   MixMatchReports   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MIXMATCHRPT"});
        print $q->submit({-accesskey=>'G', -value=>"   BogoReports   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
=cut
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNT_QRY"});
        print $q->submit({-accesskey=>'G', -value=>"   DiscountReports   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREPRICE_QRY"});
        print $q->submit({-accesskey=>'G', -value=>"   StorePriceReports   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"LIST_MIXMATCH"});
        print $q->submit({-accesskey=>'G', -value=>"   Show Current MixMatch Definitions   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";

        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"LIST_MIXMATCH_HISTORY"});
        print $q->submit({-accesskey=>'G', -value=>"   Show MixMatch Definitions History   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
		
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"LIST_MIXMATCH_LAST"});
        print $q->submit({ -value=>"   Show the most Recent MixMatch Definition Created   "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";
		
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"LIST_MIXMATCH_LAST_PUBLISH"});
        print $q->submit({ -value=>"   Show when the MixMatch discounts were last published    "});
        print $q->end_form(), "\n";
		print "<br>\n";
        #print $q->end_table(),"\n";		
		
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREMIXMATCHRPT"});
        print $q->submit({-accesskey=>'G', -value=>"   MixMatch Report - By Store   "});
        print $q->end_form(), "\n";
        #print $q->end_table(),"\n";		
		
		
		
        StandardFooter();
		
    } elsif ($report eq "CURRENTPROMORPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Promo Reports - Promotions Currently In Stores");
        LogMsgQ(indent(1)."User $uid at the Store Current Promo Reports menu");
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
		 
        my $query = "
		SELECT  
			  spd.[PromoNum]
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]			   			  
			  ,
			   convert(varchar(10),spd.[DateCreated],120) as DateCreated
			 
		FROM [****].[dbo].[tblStorePromoDetail] spd
			join [****].[dbo].[rtblStores] s on spd.StoreId = s.StoreId
		WHERE
			([PromoExpire] > getdate()
			or
			[PromoExpire] = getdate())
			and
			spd.StoreId > 1000
			and
			spd.[DateRemoved] is NULL
		GROUP BY
			  spd.[PromoNum]
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]			   			  
			  ,DateCreated
			  ,spd.[DateRemoved]
		ORDER BY
			  spd.[PromoNum]
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]			   			  
			  ,DateCreated
			  ,spd.[DateRemoved]
			
		
			
			
        ";		  						
		#print $q->pre("$query");
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=CURRENTPROMORPT_DTL&promonum=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
            print $q->p("Select the promo to see which stores have it currently configured.");

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();	
    } elsif ($report eq "CURRENTPROMORPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $promonum   = trim(($q->param("promonum"))[0]);
        print $q->p("Promo Reports - Stores with Promotion #$promonum");
        LogMsgQ(indent(1)."User $uid at the Store Current Promo Reports menu");
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
		 
        my $query = "
		SELECT spd.[StoreId]
			  ,s.[StoreName]
			  ,case 
				when s.[StoreCode] = 'V1' then 'COMPANY'
				when s.[StoreCode] = 'V2' then '- contract -'
			  end as StoreCode

 
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]
 
			  ,spd.[DateCreated]
			 
		  FROM [****].[dbo].[tblStorePromoDetail] spd
			join [****].[dbo].[rtblStores] s on spd.StoreId = s.StoreId
		  WHERE
			([PromoExpire] > getdate()
			or
			[PromoExpire] = getdate())
			and
			spd.StoreId > 1000
			and
			spd.[PromoNum] = '$promonum'
			and
			spd.[DateRemoved] is NULL
		  GROUP BY
			spd.[StoreId]
			  ,s.[StoreName]
			  , StoreCode			     
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire] 
			  ,spd.[DateCreated]
			   	  									
        ";
 
		#print $q->pre("$query");
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=CURRENTSTOREPROMORPT_DTL2&promonum=$promonum&storeId=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
            print $q->p("Select the store to see details of promotion #$promonum currently configured.");

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();			
    } elsif ($report eq "CURRENTSTOREPROMORPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Promo Reports - Promotions Currently In Stores");
        LogMsgQ(indent(1)."User $uid at the Store Current Promo Reports menu");
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
		 
        my $query = "
		SELECT spd.[StoreId]
			  ,s.[StoreName]
			  ,case 
				when s.[StoreCode] = 'V1' then 'COMPANY'
				when s.[StoreCode] = 'V2' then '- contract -'
			  end as StoreCode

			  ,spd.[PromoNum]
			  ,spd.[ItemCnt]
			  ,spd.[PromoDetailId]
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]
			  ,spd.[PromoMethod]
			  ,spd.[PluNum]
			  ,spd.[Description]
			  ,spd.[CategoryNum]
			  ,spd.[GroupNum]
			  ,spd.[DeptNum]
			  ,spd.[VendorNum]
			  ,spd.[UserFlag1]
			  ,spd.[UserFlag2]
			  ,spd.[UserFlag3]
			  ,spd.[UserFlag4]
			  ,spd.[UserFlag5]
			  ,spd.[UserFlag6]
			  ,spd.[amt1]
			  ,spd.[amt2]
			  ,spd.[ExceptionFlag]
			  ,spd.[ExceptionNumber]
			  ,spd.[TypePromoPrice]
			  ,spd.[ChangeAmt]
			  ,spd.[Accumulative]
			  ,spd.[ApplyPrice]
			  ,spd.[PrintPriceMethod]
			  ,spd.[PermanentMarkdown]
			  ,spd.[DateCreated]
			  ,spd.[DateRemoved]
		  FROM [****].[dbo].[tblStorePromoDetail] spd
			join [****].[dbo].[rtblStores] s on spd.StoreId = s.StoreId
		  WHERE
			([PromoExpire] > getdate()
			or
			[PromoExpire] = getdate())
			and
			spd.StoreId > 1000
			
			
        ";
         my $query = "
			SELECT spd.[StoreId]
			  ,s.[StoreName]
			  ,case 
				when s.[StoreCode] = 'V1' then 'COMPANY'
				when s.[StoreCode] = 'V2' then '- contract -'
			  end as StoreCode

 
			FROM [****].[dbo].[tblStorePromoDetail] spd
				join [****].[dbo].[rtblStores] s on spd.StoreId = s.StoreId
			WHERE
				([PromoExpire] > getdate()
				or
				[PromoExpire] = getdate())
				and
				spd.StoreId > 1000
			GROUP BY spd.StoreId,s.StoreName,StoreCode
			ORDER BY
				spd.StoreId
		  
			
			
        ";
		#print $q->pre("$query");
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=CURRENTSTOREPROMORPT_DTL&storeId=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
            print $q->p("Select the store to see which promotions currently configured.");

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();	
    } elsif ($report eq "CURRENTSTOREPROMORPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $storeId   = trim(($q->param("storeId"))[0]);		
		 
        print $q->p("Promo Reports - Promotions Currently In Store #$storeId");
        LogMsgQ(indent(1)."User $uid at the Store Current Promo Reports menu");
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
	 
        my $query = "
		SELECT  
			  spd.[PromoNum]
			   			  
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]
			  ,spd.[DateCreated]			  
 
		  FROM [****].[dbo].[tblStorePromoDetail] spd
			join [****].[dbo].[rtblStores] s on spd.StoreId = s.StoreId
		  WHERE
			([PromoExpire] > getdate()
			or
			[PromoExpire] = getdate())
			and
			spd.StoreId = '$storeId'
		  
		  Group By spd.PromoNum, spd.EffectiveDate,spd.PromoExpire,spd.DateCreated
		  Order by spd.PromoNum
			
			
        ";
 
 
		#print $q->pre("$query");
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=CURRENTSTOREPROMORPT_DTL2&storeId=$storeId&promonum=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
			
             

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();	
    } elsif ($report eq "CURRENTSTOREPROMORPT_DTL2") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $storeId   = trim(($q->param("storeId"))[0]);	
		my $promonum   = trim(($q->param("promonum"))[0]);			
		 
        print $q->p("Promo Reports - Promotion #$promonum Currently In Store #$storeId");
        LogMsgQ(indent(1)."User $uid at the Store Current Promo Reports menu");
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, '***', '****');
		 
        my $query = "
		SELECT  

			  spd.[PluNum]
			  ,spd.[ItemCnt]
			  ,spd.[PromoDetailId]
			  ,spd.[EffectiveDate]
			  ,spd.[PromoExpire]
			  ,spd.[PromoMethod]
			 
			  ,spd.[Description]
			  ,spd.[CategoryNum]
			  ,spd.[GroupNum]
			  ,spd.[DeptNum]
			  ,spd.[VendorNum]
			  ,spd.[UserFlag1]
			  ,spd.[UserFlag2]
			  ,spd.[UserFlag3]
			  ,spd.[UserFlag4]
			  ,spd.[UserFlag5]
			  ,spd.[UserFlag6]
			  ,spd.[amt1]
			  ,spd.[amt2]
			  ,spd.[ExceptionFlag]
			  ,spd.[ExceptionNumber]
			  ,spd.[TypePromoPrice]
			  ,spd.[ChangeAmt]
			  ,spd.[Accumulative]
			  ,spd.[ApplyPrice]
			  ,spd.[PrintPriceMethod]
			  ,spd.[PermanentMarkdown]
			  ,spd.[DateCreated]
			  ,spd.[DateRemoved]		  
 
		  FROM [****].[dbo].[tblStorePromoDetail] spd
			join [****].[dbo].[rtblStores] s on spd.StoreId = s.StoreId
		  WHERE
			([PromoExpire] > getdate()
			or
			[PromoExpire] = getdate())
			and
			spd.StoreId = '$storeId'
			and
			spd.PromoNum = '$promonum'					
			
        "; 
 
		#print $q->pre("$query");
        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
           # my @links = ( "$scriptname?report=CURRENTSTOREPROMORPT_DTL2&storeId=$storeId&promonum".'$_' );
           # printResult_arrayref2($tbl_ref, \@links, '', '', '');
		    printResult_arrayref2($tbl_ref);			             
        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();			
    } elsif ($report eq "STOREPROMORPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Promo Reports");
        LogMsgQ(indent(1)."User $uid at the Store Promo Reports menu");
        #Here you could create a report to show what promotions went to what stores when.

        #Another idea for a report is to show what promotions are in effect at any given store at any given time.
        my $query = "
        select distinct(ps.storeId),s.storeName from
            promotion_store ps
            join stores s on ps.storeId = s.storeId
        order by ps.storeId
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("STOREPROMORPT: Failed to connect to $mysqld and $posdb database");
            print "STOREPROMORPT: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=STOREPROMORPT_DTL&storeId=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
            print $q->p("Select the store to see which promotions were sent to it.");

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();
    } elsif ($report eq "STOREPROMORPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $storeId   = trim(($q->param("storeId"))[0]);
        my $sortfield = trim(($q->param("sort"))[0]);      
        unless ($sortfield) {
            $sortfield="promoId";
        }		
        ## adding an order field to be ASC or DESC -kdg ###
        my $order = trim(($q->param("order"))[0]);           
        if (($order eq "") || ($order eq "ASC")) {
            ## Toggle the order
			$order = "DESC";
        } else {
			$order = "ASC";            
        }    		
        print $q->p("Promo Details Reports: Promotions at store $storeId");
		print $q->p("Sorted by $sortfield $order");
        my $query = "
        select * from
            promotions
        where
            promoId in (select promoId from promotion_store where storeId = $storeId)
        order by $sortfield $order 
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("STOREPROMORPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "STOREPROMORPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);
		###
        my $newsortURL = "$scriptname?report=STOREPROMORPT_DTL&storeId=$storeId&order=$order&sort=\$_";
        my @headerlinks = ($newsortURL, $newsortURL,$newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL); 
		###
        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=PROMORPT_DTL&promoId=".'$_' );
 
            printResult_arrayref2($tbl_ref, \@links, '', \@headerlinks, '');
            print $q->p("Select a promotion to see more info about that promotion.");
        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
         print "<br>";

        StandardFooter();
    } elsif ($report eq "PROMORPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $sortfield = trim(($q->param("sort"))[0]);      
        unless ($sortfield) {
            $sortfield="promoId";
        }		
        ## adding an order field to be ASC or DESC -kdg ###
        my $order = trim(($q->param("order"))[0]);           
        if (($order eq "") || ($order eq "ASC")) {
            ## Toggle the order
			$order = "DESC";
        } else {
			$order = "ASC";            
        }  		
        print $q->p("Promo Reports");
		print $q->p("Sorted by $sortfield $order");
        LogMsgQ(indent(1)."User $uid at the Promo Reports menu");
        #Here you could create a report to show what promotions went to what stores when.

        #Another idea for a report is to show what promotions are in effect at any given store at any given time.
        my $query = "
        select * from
            promotions
        order by $sortfield $order 
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);
		###
        my $newsortURL = "$scriptname?report=PROMORPT&order=$order&sort=\$_";
        my @headerlinks = ($newsortURL, $newsortURL,$newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL, $newsortURL); 
		###		
 
        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=PROMORPT_DTL&promoId=".'$_' );            
			printResult_arrayref2($tbl_ref, \@links, '', \@headerlinks, '');
        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();

    } elsif ($report eq "PROMORPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $promoId   = trim(($q->param("promoId"))[0]);
		print $q->p("Promo Reports for PromoId: $promoId");
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMORPT_DTL1"});
        print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
        print $q->submit({-accesskey=>'S', -value=>"   SKUs in the promotion   "});
        print $q->end_form(), "\n";
       # print $q->end_table(),"\n";
       # print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMORPT_DTL2"});
        print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
        print $q->submit({-accesskey=>'S', -value=>"   Stores getting this promotion   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        StandardFooter();
    } elsif ($report eq "PROMORPT_DTL1") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $promoId   = trim(($q->param("promoId"))[0]);
        print $q->p("Promo Details Reports: SKUs in promotion #$promoId ");
        my $query = "
        select
            pd.skuNum
        from
            promotion_detail pd

        where
            pd.promoId = '$promoId'
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
         print "<br>";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMORPT_DTL2"});
        print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
        print $q->submit({-accesskey=>'S', -value=>"   Stores getting this promotion   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        StandardFooter();
    } elsif ($report eq "PROMORPT_DTL2") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $promoId   = trim(($q->param("promoId"))[0]);
        print $q->p("Promo Details Reports: Stores getting promo #$promoId");
        my $query = "
        select
            distinct(ps.storeId), s.storeName
        from
            promotion_detail pd
            join promotion_store ps on pd.promoId = ps.promoId
            join stores s on s.storeId = ps.storeId
        where
            pd.promoId = '$promoId'
        order by
            ps.storeId
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }
        print "<br>";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"PROMORPT_DTL1"});
        print $q->input({-type=>"hidden", -name=>"promoId", -value=>"$promoId"});
        print $q->submit({-accesskey=>'S', -value=>"   SKUs in the promotion   "});
        print $q->end_form(), "\n";
        $dbh->disconnect;
        StandardFooter();
    } elsif ($report eq "STOREMIXMATCHRPT") {
		my $where_clause = "where mms.storeId not like '99'";
        if ($testing_mode) {
            print "--testing mode--<br>";
			$where_clause = '';
        }
        print $q->p("MixMatch Reports");
        LogMsgQ(indent(1)."User $uid at the Store MixMaatch Reports menu");
        #Here you could create a report to show what promotions went to what stores when.

        #Another idea for a report is to show what promotions are in effect at any given store at any given time.
        my $query = "
        select distinct(mms.storeId),s.storeName from
            mixmatch_store mms
            join stores s on s.storeId = mms.storeId
			$where_clause
        order by mms.storeId
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=STOREMIXMATCHRPT_DTL&storeId=".'$_' );
			
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
            print $q->p("Select the store to see which mixmatch was sent to it.");

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();	
 
    } elsif ($report eq "STOREMIXMATCHRPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $storeId   = trim(($q->param("storeId"))[0]);
        print $q->p("MixMatch Details Reports: MixMatch Discounts at store $storeId");
        my $query = "
        select 
			mixmatchId,
			f7_mixmatchName as Description,
			f70_InstanceName as MixMatchName,
			mixmatchCreator as Creator,
			creationDate as CreationDate,
			f61_effectiveDate as StartDate,
			f62_expireDate as EndDate,
			status
		from
            mixmatch
        where
            mixmatchId in (select mixmatchId from mixmatch_store where storeId = $storeId)
        order by mixmatchId
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("STOREMIXMATCHRPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "STOREMIXMATCHRPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=MMRPT_DTL&mixmatchId=".'$_' );
			@links = ();	# Currently disabled since we are not recording this detail - 2013-11-06 - kdg
            printResult_arrayref2($tbl_ref, \@links, '', '', '');
            #print $q->p("Select a MixMatch to see more info about that discount.");
        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
         print "<br>";

        StandardFooter();
} elsif ($report eq "MMRPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
		
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMRPT_DTL1"});
        print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
        print $q->submit({-accesskey=>'S', -value=>"   SKUs in the discount   "});
        print $q->end_form(), "\n";
       # print $q->end_table(),"\n";
       # print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMRPT_DTL2"});
        print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
        print $q->submit({-accesskey=>'S', -value=>"   Stores getting this discount   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";

        StandardFooter();
    } elsif ($report eq "MMRPT_DTL1") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        print $q->p("MixMatch Details Reports: SKUs in the discount: $mixmatchId  ");
        my $query = "
        select
            *
        from
            mixmatch_detail mmd

        where
            mmd.mixmatchId = '$mixmatchId'
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("MMRPT_DTL1: Failed to connect to $mysqld and $posdb database");
            print "MMRPT_DTL1: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
         print "<br>";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMRPT_DTL2"});
        print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
        print $q->submit({-accesskey=>'S', -value=>"   Stores getting this discount   "});
        print $q->end_form(), "\n";
        print $q->end_table(),"\n";
        StandardFooter();
    } elsif ($report eq "MMRPT_DTL2") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        print $q->p("MixMatch Details Reports: Stores getting this discount  ");
        my $query = "
        select
            distinct(mms.storeId), s.storeName
        from
            mixmatch_detail mmd
            join mixmatch_store mms on mmd.mixmatchId = mms.mixmatchId
            join stores s on s.storeId = mms.storeId
        where
            mmd.mixmatchId = '$mixmatchId'
        order by
            mms.storeId
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }
        print "<br>";
        print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
        print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
        print $q->input({-type=>"hidden", -name=>"report", -value=>"MMRPT_DTL1"});
        print $q->input({-type=>"hidden", -name=>"mixmatchId", -value=>"$mixmatchId"});
        print $q->submit({-accesskey=>'S', -value=>"   SKUs in the discount   "});
        print $q->end_form(), "\n";
        $dbh->disconnect;
        StandardFooter();    	
    } elsif ($report eq "XFRRPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Transfer Reports");

        my $query = "
        select
            transferId,
            creationDate,
            transferCreator,
            fromStore,
            toStore
        from
            transfers
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=XFRRPT_DTL&transferId=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();
    } elsif ($report eq "XFRRPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $transferId   = trim(($q->param("transferId"))[0]);
        print $q->p("Transfer Details Reports ");
        my $query = "
        select
            t.fromStore,
            fs.storeName,
            t.toStore,
            ts.storeName,
            td.skuNum

        from
            transfer_detail td
            join transfers t on td.transferId = t.transferId
            left join stores ts on ts.storeId = t.toStore
            join stores fs on fs.storeId = t.fromStore

        where
            td.transferId = '$transferId'
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }
        $dbh->disconnect;
        StandardFooter();
    } elsif ($report eq "MMRPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Mix Match Reports");

        my $query = "
        select
            mixmatchId,
            mixmatchNum,
            mixmatchTitle,
            mixmatchCreator,
            creationDate,
            effectiveDate,
            expireDate,
            status

        from
            mixmatch
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("MMRPT: Failed to connect to $mysqld and $posdb database");
            print "MMRPT: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=MMRPT_DTL&mixmatchId=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();
    } elsif ($report eq "MMRPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $mixmatchId   = trim(($q->param("mixmatchId"))[0]);
        print $q->p("Transfer Details Reports ");
        my $query = "


        select
            case when mmd.selector = 1 then 'Department' else 'Dept-Class' end as Selector,
            mmd.selection,
            mmd.purchaseQty,
            mmd.discount,
            ms.storeId,
            ms.storeName

        from
            mixmatch_detail mmd
            join mixmatch_store mms on mmd.mixmatchId = mms.mixmatchId
            left join stores ms on ms.storeId = mms.storeId


        where
            mmd.mixmatchId = '$mixmatchId'
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("PROMORPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "PROMORPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }
        $dbh->disconnect;
        StandardFooter();

    } elsif ($report eq "MIXMATCHRPT") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        print $q->p("Bogo Reports");

        my $query = "
        select
            bogoId,
            bogoQty,
            bogoDiscount,
            bogoName,
            bogoCreator,
            creationDate,
            effectiveDate,
            expireDate,
            status

        from
            bogo
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("MMRPT: Failed to connect to $mysqld and $posdb database");
            print "MMRPT: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            my @links = ( "$scriptname?report=MIXMATCHRPT_DTL&bogoId=".'$_' );
            printResult_arrayref2($tbl_ref, \@links, '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }

        $dbh->disconnect;
        StandardFooter();
    } elsif ($report eq "MIXMATCHRPT_DTL") {
        if ($testing_mode) {
            print "--testing mode--<br>";
        }
        my $bogoId   = trim(($q->param("bogoId"))[0]);
        print $q->p("Bogo Details Reports ");
        my $query = "


        select
            bd.bogoId,
            bd.selector,
            bd.selection,
            s.storeId,
            s.storeName

        from
            bogo_detail bd
            join bogo_store bs on bd.bogoId = bs.bogoId
            left join stores s on s.storeId = bs.storeId


        where
            bd.bogoId = '$bogoId'
        ";

        my $dbh;
        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("MIXMATCHRPT_DTL: Failed to connect to $mysqld and $posdb database");
            print "MIXMATCHRPT_DTL: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }

        my $tbl_ref = execQuery_arrayref($dbh, $query);

        if (recordCount($tbl_ref)) {
            printResult_arrayref2($tbl_ref, '', '', '', '');

        } else {
            print $q->b("no records found<br>\n");
        }
        $dbh->disconnect;
        StandardFooter();

} elsif (("$report" eq "NETSALES") || ($report eq "DISCOUNT_QRY") || ($report eq "DISCOUNT_QRY2")) {
    my $next_report;
    my $label;
    if ($report eq "NETSALES") {
        $next_report="NETSALES2";
        $label="Net Sales";
    } elsif ($report eq "DISCOUNT_QRY") {
        $next_report="DISCOUNT";
        $label="Discount";

    } elsif ($report eq "DISCOUNT_QRY2") {
        $next_report="DISCOUNT_V2";
        $label="Discount";
    }
    print $q->h2("$label Report");
	if ($testing_mode) {
		print "--testing mode--<br>";
	}
    # Get the report criteria:
    ### Build the table
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"$next_report"});
    print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>6}, $q->b("Report Criteria:")));

    my @ltm=localtime();
    my $timeseconds=time();
    my $current_year=$ltm[5]+1900;
    # Use yesterday as the default date
    my @previous_day_ltm=localtime($timeseconds - 86400); # The previous day
    my $year=($previous_day_ltm[5]+1900);
    my $this_month=$previous_day_ltm[4];
    $this_month++;
    my $this_day=$previous_day_ltm[3];

    my %month_hash=(
        '1'=>'Jan',
        '2'=>'Feb',
        '3'=>'Mar',
        '4'=>'Apr',
        '5'=>'May',
        '6'=>'Jun',
        '7'=>'Jul',
        '8'=>'Aug',
        '9'=>'Sep',
        '10'=>'Oct',
        '11'=>'Nov',
        '12'=>'Dec'
    );
    my $store_options;
    my $month_options;
    my $day_options;
    my $year_options;
    my $data_options;
    #use TTV::lookupdata;

    foreach my $store (sort(keys(%storename_hash))) {
        $store_options .= $q->option({-value=>"$store - $storename_hash{$store}"}, "$store - $storename_hash{$store}");
    }

    foreach my $month (sort(keys(%month_hash))) {
        if ($month == $this_month) {
            $month_options .= $q->option({-value=>$month -selected=>'selected'}, "$month_hash{$month}");
        } else {
            $month_options .= $q->option({-value=>$month}, "$month_hash{$month}");
        }

    }
    foreach my $day (1 ... 31) {
        if ($day == $this_day) {
            $day_options .= $q->option({-value=>$day -selected=>'selected'}, "$day");
        } else {
            $day_options .= $q->option({-value=>$day}, "$day");
        }

    }
    $year=$current_year;
    while ($year > 1999) {
        $year_options .= $q->option({-value=>$year}, "$year");
        $year--;
    }

    $data_options .= $q->option({-value=>0}, "Tlog data");
    $data_options .= $q->option({-value=>1}, "Storedata");
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"store_selection", -size=>"1"}), $store_options),

               # $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Data Source:"),
               #     $q->td({-class=>'tabledatanb', -align=>'left'},
               #       $q->Select({-name=>"data_selection", -size=>"1"}), $data_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Start Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"start_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"start_year_selection", -size=>"1"}), $year_options)
              );
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "End Month:"),
               $q->td({-class=>'tabledatanb', -align=>'left'},
                      $q->Select({-name=>"end_month_selection", -size=>"1"}), $month_options),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Day:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_day_selection", -size=>"1"}), $day_options) ,
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Year:"),
                        $q->td({-class=>'tabledatanb', -align=>'left'},
                        $q->Select({-name=>"end_year_selection", -size=>"1"}), $year_options)
              );


    # Add the submit button
    print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
               $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                      $q->submit({-accesskey=>'S', -value=>"   Submit   "})),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
              );

    print $q->end_form(), "\n";
    print $q->end_table(),"\n";

    StandardFooter();
} elsif ("$report" eq "DISCOUNT") {
    print $q->h2("Discount Report");

	my $storeid   = trim(($q->param("store_selection"))[0]);
    my $orig_storeid=$storeid;
    # The storeid contains the storeid and the store name.  Break them apart here:
    my @tmp=split(/\s+/,$storeid);
    $storeid=$tmp[0];
    my $storename=$tmp[-1];
    my $start_month_selection   = trim(($q->param("start_month_selection"))[0]);
    my $start_day_selection   = trim(($q->param("start_day_selection"))[0]);
    my $start_year_selection   = trim(($q->param("start_year_selection"))[0]);
    my $end_month_selection   = trim(($q->param("end_month_selection"))[0]);
    my $end_day_selection   = trim(($q->param("end_day_selection"))[0]);
    my $end_year_selection   = trim(($q->param("end_year_selection"))[0]);
    my $data_selection   = trim(($q->param("data_selection"))[0]);
    my $txnNum   = trim(($q->param("txnNum"))[0]);
    my $whereTxn;
    if ($txnNum) {
        $whereTxn = " and tm.TranNum = $txnNum";
    }
    my $startdate="$start_year_selection"."-"."$start_month_selection"."-"."$start_day_selection";
    my $enddate="$end_year_selection"."-"."$end_month_selection"."-"."$end_day_selection";
    my $summary=0;
    unless ($startdate eq $enddate) {
        $summary=1;
    }
 	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, '***', '****');
    print $q->h3("Store: #${storeid} $storename - $startdate to $enddate");

    my %totals_hash;
    my $total_origPrice=0;
    my $total_Discounts=0;
    my $total_itemDiscounts=0;
    my $total_transDiscounts=0;
    my $total_promoDiscounts=0;
    my $total_bogoDiscounts=0;
    my $total_sellPrice=0;
    # Discounts are really of three basic sorts: 1) Discounts, 2) Promotions, and 3) MixMatch.  The #1 Discounts are basically separated into Item and Transaction Discounts
    # Because these three basic discounts are so different, I am going to report them separately.
    unless ($summary) {
        if ($txnNum) {
            print $q->p("View all transactions");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNT"});
            print $q->input({-type=>"hidden", -name=>"store_selection", -value=>"$orig_storeid"});
            print $q->input({-type=>"hidden", -name=>"start_month_selection", -value=>"$start_month_selection"});
            print $q->input({-type=>"hidden", -name=>"start_day_selection", -value=>"$start_day_selection"});
            print $q->input({-type=>"hidden", -name=>"start_year_selection", -value=>"$start_year_selection"});

            print $q->input({-type=>"hidden", -name=>"end_month_selection", -value=>"$end_month_selection"});
            print $q->input({-type=>"hidden", -name=>"end_day_selection", -value=>"$end_day_selection"});
            print $q->input({-type=>"hidden", -name=>"end_year_selection", -value=>"$end_year_selection"});

            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2} ));

            # Add the submit button
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'S', -value=>"   View All   "}))
                      );

            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            print $q->p();

        } else {

            print $q->p("View only one selected transaction");
            print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
            print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
            print $q->input({-type=>"hidden", -name=>"report", -value=>"DISCOUNT"});
            print $q->input({-type=>"hidden", -name=>"store_selection", -value=>"$orig_storeid"});
            print $q->input({-type=>"hidden", -name=>"start_month_selection", -value=>"$start_month_selection"});
            print $q->input({-type=>"hidden", -name=>"start_day_selection", -value=>"$start_day_selection"});
            print $q->input({-type=>"hidden", -name=>"start_year_selection", -value=>"$start_year_selection"});
            print $q->input({-type=>"hidden", -name=>"end_month_selection", -value=>"$end_month_selection"});
            print $q->input({-type=>"hidden", -name=>"end_day_selection", -value=>"$end_day_selection"});
            print $q->input({-type=>"hidden", -name=>"end_year_selection", -value=>"$end_year_selection"});

            print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2} ));

            print $q->Tr(
                $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Txn Selection:"),
                $q->td({-class=>'tabledatanb', -align=>'left'}, $q->input({ -type=>'text', -name=>"txnNum", -size=>"5", -value=>""}))
            );

            # Add the submit button
            print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
                       $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
                              $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
                      );

            print $q->end_form(), "\n";
            print $q->end_table(),"\n";
            print $q->p();
        }
    }
####################
    # PART I - Discounts
    ####################

    my %discountType_hash = (
    '0' => 'Undefined discount type',
    '1' => 'ITEM % OFF',
    '2' => 'ITEM $ OFF',
    '3' => 'PRICE MARKDOWN',
    '4' => 'TRANS % OFF',
    '5' => 'TRANS $ OFF',
    '6' => 'MFG $ COUPON',
    '7' => 'TRANS % OFF IN EMPLOYEE SALE',
    '8' => 'MIX MATCH ITEMS',
    '9' => 'MFG COUPON FOR MATCHED ITEM',
    '10' => 'SCAN MFG COUPON,NO LINK TO AN ITEM',
    '11' => 'ITEM % OFF IN EMPLOYEE SALE',
    '12' => 'Item % Surcharge',
    '13' => 'Item $ Surcharge',
    '14' => 'SURCHARGE MARKDOWN',
    '15' => 'Transaction % Surcharge',
    '16' => 'Transaction $ Surcharge',
    '17' => 'TRANS % AT TOTAL',
    '18' => 'TRANS $ AT TOTAL',
    '19' => 'TRANS % AT TENDER',
    '20' => 'TRANS $ AT TENDER',
    '25' => 'Item Best Price %',
    '26' => 'Item Best Price $',
    );

    my @item_discounts=(
        "1",
        "2",
        "3",
        "12",
        "13",
        "25",
        "26"
    );
    my @ms_array;

    print $q->h3("Part I - Item and Transaction Discounts");
    print $q->h4("Item Discounts");
    ####
    # Step 1 - Find the Item Discounts

    $query=dequote(<< "    ENDQUERY");
        select
            tm.TranId,
            ds.Sequence,
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                      tm.TranNum between 4013 and 4023 and
                      convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                then tm.TranNum - 4000
                else tm.TranNum end as TxnNum,
            ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.ExtDiscountAmt,
            ds.DiscountPercent,
            ds.DiscountType,
            ds.DiscountID,
            rds.Description,
            rds.DiscGroup,
            ds.Reason,
            case when LEN(ds.ReferenceNum) = 7 then ata.Description else null end as Publication,
            case when LEN(ds.ReferenceNum) = 7 then atb.Description else null end as Creative,
            case when LEN(ds.ReferenceNum) = 7 then atc.Description else null end as Year
        from ****..tblTranMaster tm
            join ****..tblDiscounts ds on tm.TranId=ds.TranId
            join [****].[dbo].[rtblDiscounts] rds on ds.DiscountID=rds.[DiscountId]

            left join ****..tblMarketingAdTrack ata on substring(ds.ReferenceNum,1,3) = ata.code
                and ata.CodeType='P'
            left join ****..tblMarketingAdTrack atb on substring(ds.ReferenceNum,4,2) = atb.code
                and left(atb.CodeType, 1) ='C' and right(atb.CodeType,2) = substring(ds.ReferenceNum,6,2)
            left join ****..tblMarketingAdTrack atc on substring(ds.ReferenceNum,6,2) = atc.code
                and atc.CodeType='Y'
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')

        $whereTxn

        order by tm.StoreId, tm.RegisterId,
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
    ENDQUERY

    my $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    my @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
        "Discount %",
        "Discount ID",
        "Description",
        "DiscGroup",
        "Reason"
    );
    my @summary_header=(
        "",
        "Quantity",
        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
    );

    # Step 2 find the associated merchandise transaction.  We do this by stepping through all of the merchandise records of this tranid.
    # We match the  discount sequence to the merchandise record sequence by knowing that the discount sequence comes after
    # the merchandise sequence but before the next one.  Therefore, the merchandise sequence ($Sequence) should be less than the associated
    # discount sequence ($seq) and yet greater than the previous discount sequence ($previous_seq).
    my @array_to_print=\@header;
    my @summary_array_to_print=\@summary_header;

    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
        # Throw out any discounts that are not item discounts
        unless (grep /$DiscountType/,@item_discounts){
            next;
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                ms.ExtRegPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****..tblTranMaster tm
                    join ****..tblMerchandiseSale ms on tm.TranId=ms.TranId
                where
                    tm.TranId = '$tranid'
                order by
                    tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ms.Sequence desc
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my $sequence_not_used=1;
        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];

            my $extoprice=$tmp[3];
            my $extsprice=$tmp[4];
            my $Sequence=$tmp[5];

            # I don't think that any table really knows what the sell price was after this item discount was applied IF there was a subsequent transaction discount.  Therefore, it seems reasonable to me to calculate the
            # final selling price rather than try to find it anywhere. -kdg

            $extsprice=($extoprice + $ExtDiscountAmt);

            if (($Sequence < $seq) && ($sequence_not_used)) {
                #  This is the first merchandise record in the transaction we are stepping through and this is the first discount record must apply to this merchandise record.
                my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$extoprice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                ,"$DiscountID","$Description","$DiscGroup","$Reason");
                push(@array_to_print,\@array);
                $sequence_not_used=0;
            }
        }
    }

    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"ItemDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            print $q->p("Item Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            print $q->p("Item Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@array_to_print, '', \@format);
        }
    } else {
        print $q->p("No Item Discount data in tblMerchandiseSale found for $storeid between $startdate and $enddate");
    }

    ####

    print $q->h4("Transaction Discounts");

    if (0) {
        print $q->h4("Prorated Discounts");
        # tblDiscountProrated  - these are Transaction Discounts
        $query=dequote(<< "        ENDQUERY");
            select
                 tm.RegisterId as RegNum,
                 case when tm.StoreId='2098' and tm.RegisterId = '02' and
                              tm.TranNum between 4013 and 4023 and
                              convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                        then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
                 ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
                 tm.TranDateTime,
                 ds.DiscountType,
                 ds.Reason,
                 ds.DiscountPercent,
                 ds.ExtDiscountAmt,
                 ds.OrigPrice,
                 ds.DiscountID
                 --tm.tlogfile as [TlogFile]

            from ****..tblTranMaster tm
                    join ****..tblDiscountsProrated ds on tm.TranId=ds.TranId
            where tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
                and -- add 1 day for an exclusive upper bound
                    tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                    tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                    tm.StoreId in ('$storeid')
             $whereTxn
            order by tm.StoreId, tm.RegisterId,
                        convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
        ENDQUERY
        my $tbl_ref = execQuery_arrayref($dbh, $query);
        if (recordCount($tbl_ref)) {
                my @format = ('', '',
                '',
                '',
                {'align' => 'Right', 'currency' => ''},
                );
            print $q->p("Data in tblDiscountsProrated found for $storeid between $startdate and $enddate");
            printResult_arrayref2($tbl_ref, '', \@format);

        } else {
            print $q->p("No data in tblDiscountsProrated found for $storeid between $startdate and $enddate");
        }
    }
    # Step 1 find the prorated discounts

    @ms_array=();
    $query=dequote(<< "    ENDQUERY");
        select
            tm.TranId,
            ds.Sequence,
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                      tm.TranNum between 4013 and 4023 and
                      convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                then tm.TranNum - 4000
                else tm.TranNum end as TxnNum,
            ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.ExtDiscountAmt,
            ds.DiscountPercent,
            ds.DiscountType,
            ds.DiscountID,
            rds.Description,
            rds.DiscGroup,
            --ds.Reason
            cl.Description
            --case when LEN(ds.ReferenceNum) = 7 then ata.Description else null end as Publication,
            --case when LEN(ds.ReferenceNum) = 7 then atb.Description else null end as Creative,
            --case when LEN(ds.ReferenceNum) = 7 then atc.Description else null end as Year

        from
            ****..tblTranMaster tm
            join ****..tblDiscountsProrated ds on tm.TranId=ds.TranId
            join [****].[dbo].[rtblDiscounts] rds on ds.DiscountID=rds.[DiscountId]

            left join ****..tblMarketingAdTrack ata on substring(ds.ReferenceNum,1,3) = ata.code
                and ata.CodeType='P'
            left join ****..tblMarketingAdTrack atb on substring(ds.ReferenceNum,4,2) = atb.code
                and left(atb.CodeType, 1) ='C' and right(atb.CodeType,2) = substring(ds.ReferenceNum,6,2)
            left join ****..tblMarketingAdTrack atc on substring(ds.ReferenceNum,6,2) = atc.code
                and atc.CodeType='Y'
            join [****].[dbo].[tblStoreChoiceList] cl on tm.StoreId = cl.StoreId and ds.Reason = cl.ChoiceCode and cl.ChoiceType = 7
        where
            tm.TranDateTime >= '$startdate'
            and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and tm.TranVoid = 0 and tm.IsPostVoided = 0
            and tm.TranModifier not in (2,6)
            and tm.StoreId in ('$storeid')
            $whereTxn
        order by
            tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ds.Sequence

    ENDQUERY

    $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
        "Discount %",
        "Discount ID",
        "Description",
        "DiscGroup",
        "Reason"
    );
    @summary_header=(
        "",
        "Quantity",
        "ExtOrigPrice",
        "Discount",
        "ExtSellPrice",
    );

    # Step 2 find the associated merchandise transaction.  We do this by stepping through all of the merchandise records of this tranid.
    # We match the prorated discount sequence to the merchandise record sequence by knowing that the discount sequence comes after
    # the merchandise sequence but before the next one.  Therefore, the merchandise sequence ($Sequence) should be less than the associated
    # discount sequence ($seq) and yet greater than the previous discount sequence ($previous_seq).
    @array_to_print=\@header;
    @summary_array_to_print=\@summary_header;
    my $ms_count=0;
    my $last_seq=0;
    my $previous_tranid=0;
    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $seq=$array[1];
        my $RegNum=$array[2];
        my $TxnNum=$array[3];
        my $TranDateTime=$array[4];
        my $OrigPrice=$array[5];
        my $ExtDiscountAmt=$array[6];
        my $DiscountPercent=$array[7];
        my $DiscountType=$array[8];
        my $DiscountID=$array[9];
        my $Description=$array[10];
        my $DiscGroup=$array[11];
        my $Reason=$array[12];
        my $Publication=$array[13];
        my $Creative=$array[14];
        my $Year=$array[15];
        my $previous_seq=0;
        if ($tranid == $previous_tranid) {
            # This is the next sequence in the same transaction.
            # Let's remember what the previous discount sequence was
            $previous_seq=$last_seq;
        }

        $query=dequote(<< "        ENDQUERY");
            select
                ms.[SKU],
                ms.[Dept],
                ms.[Quantity],
                --ms.Cost,
                --ms.ExtOrigPrice,
                ms.ExtSellPrice,
                ms.Sequence
                from
                    ****..tblTranMaster tm
                    join ****..tblMerchandiseSale ms on tm.TranId=ms.TranId

                where
                    tm.TranId = '$tranid'
                order by
                    tm.StoreId, tm.RegisterId, convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum, ms.Sequence
        ENDQUERY
        my $sth=$dbh->prepare("$query");
        $sth->execute();

        while (my @tmp = $sth->fetchrow_array()) {
            my $sku=$tmp[0];
            my $dept=$tmp[1];
            my $qty=$tmp[2];
           # my $cost=$tmp[3];
            #my $extoprice=$tmp[4];
            my $extsprice=$tmp[3];
            my $Sequence=$tmp[4];

            if ($previous_seq) {
                # There was more than one item in this merchandise sale.  We have already found a previous sequence.
                # If this merchandise sequence ($Sequence) is less than the discount sequence ($seq) and yet still greater
                # than the previous discount sequence ($previous_seq) then this discount sequence and this merchandise sequence are associated.
                if (($Sequence < $seq) && ($Sequence > $previous_seq)) {
                    # This matches the sequence of the merchandise sale entry to the prorated discount entry
                    my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$OrigPrice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                   ,"$DiscountID","$Description","$DiscGroup","$Reason");
                    push(@array_to_print,\@array);
                }
            } elsif ($Sequence < $seq) {
                #  This is the first merchandise record in the transaction we are stepping through and this is the first discount record must apply to this merchandise record.
                my @array=( "$RegNum","$TxnNum","$TranDateTime","$sku","$dept","$qty", "$OrigPrice","$ExtDiscountAmt","$extsprice","$DiscountPercent",
                ,"$DiscountID","$Description","$DiscGroup","$Reason");
                push(@array_to_print,\@array);

            }
        }
        $last_seq=$seq;
        $previous_tranid=$tranid;
    }

    if (recordCount(\@array_to_print)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        for my $datarows (1 .. $#array_to_print){
            my $thisrow = $array_to_print[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        push @array_to_print, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"TransDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];

        my @format = (
        '',
        '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',

        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        if ($summary) {
            print $q->p("Transaction Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            print $q->p("Transaction Discounts found for store number $storeid between $startdate and $enddate");
            printResult_arrayref2(\@array_to_print, '', \@format);
        }
    } else {
        print $q->p("No Transaction Discount data in tblMerchandiseSale found for $storeid between $startdate and $enddate");
    }
###

    print $q->h3("Part II - Promotions");
    ####################
    # PART II - Promotions
    ####################
    # [tblMerchandiseSale] - Promotions
    $query=dequote(<< "    ENDQUERY");
        select
             tm.RegisterId as RegNum,
             case when tm.StoreId='2098' and tm.RegisterId = '02' and
                          tm.TranNum between 4013 and 4023 and
                          convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                    then tm.TranNum - 4000
                    else tm.TranNum end as TxnNum,
             ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
             tm.TranDateTime,
             ms.SKU,
             ms.Dept,
             ms.Quantity,
             --ms.Cost,
             ms.ExtOrigPrice,
             (ms.Quantity * ms.PromoPriceAdjustAmount) as PromoDiscount,
             --(ms.Quantity * (0 - ms.PromoPriceAdjustAmount)) as ExtPromoDisc,
             ms.ExtRegPrice,
             (ms.PromoPercentAdjustAmount * 100) as 'Promo%Amt',

             ms.PromoNum,
             tsp.[Description],
             ms.OrigPrice

        from ****..tblTranMaster tm
                join ****..tblMerchandiseSale ms on tm.TranId=ms.TranId
                join [****].[dbo].[tblStorePromoHeader] tsp on ms.PromoNum = tsp.[PromoNum] and tm.StoreId = tsp.[StoreId]
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')
        and ms.PromoNum > 0
         $whereTxn
        order by tm.StoreId, tm.RegisterId,
                    convert(DateTime, convert(varchar(10),tm.TranDateTime,21)), tm.TranNum
    ENDQUERY
    my $tbl_ref = execQuery_arrayref($dbh, $query);
    if (recordCount($tbl_ref)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        #   get totals for the specified columns.
        undef($$tbl_ref[0][-1]);
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            my $OrigPrice=$$thisrow[-1];
            if ($OrigPrice > 0) {
                # If the OrigPrice is not 0, then it means that someone did not accept the default price.  As a result, the
                # PromoPriceAdjustAmount may be useless.
                # Set the promo discount to the difference between the orig price and the reg price
                $$thisrow[7] = ($$thisrow[6] - $$thisrow[8]);
                # An asterisk to mark this row.
                $$tbl_ref[$datarows][-1]="*";
            } else {
                undef($$tbl_ref[$datarows][-1]);
            }
            # If this item is being returned, then the ExtOrigPrice is going to be negative.  For some reason, the PromoPriceAdjustAmount is always
            # positive in the tblMerchandiseSale table but I want to show it as negative for sales and positive if is it is a return.
            if ($$thisrow[6] > 0) {
                $$thisrow[7] = (0 - $$thisrow[7]);
            }

            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }
        }
        my @summary_header=('');

        foreach my $c (@sum_columns) {
            # Grab the headers for the fields we want.
            push(@summary_header,$$tbl_ref[0][$c]);
        }
        my @summary_array_to_print=\@summary_header;
        push @$tbl_ref, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;

        $total_origPrice+=$fieldsum[6];
        $total_promoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"PromoDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];
        my @format = ('', '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},

        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'num' => ''},
        '',
        '',
        {'align' => 'Right', 'currency' => ''},

        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        print $q->p("Promotion Discounts found for store number $storeid between $startdate and $enddate");
        if ($summary) {
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            printResult_arrayref2($tbl_ref, '', \@format);
        }
    } else {
        print $q->p("No Promotion Discounts found for store number $storeid between $startdate and $enddate");
    }
    print $q->h3("Part III - MixMatch Discounts");
    ####################
    # PART III - MixMatch
    ####################
    ## Mix Match
    # Get the data from tblDiscounts table
    # First we find the discounts and then we associate them with the item.
    @ms_array=();   # Initialize this variable we are re-using;

    $query=dequote(<< "    ENDQUERY");
        select
            tm.[TranId],
            ds.[Sequence],
            tm.RegisterId as RegNum,
            case when tm.StoreId='2098' and tm.RegisterId = '02' and
                          tm.TranNum between 4013 and 4023 and
                          convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                    then tm.TranNum - 4000
                    else tm.TranNum end as TxnNum,
             ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
            tm.TranDateTime,
            ds.OrigPrice,
            ds.[ExtDiscountAmt]

        from ****..tblTranMaster tm
            join ****..tblDiscounts ds on tm.TranId=ds.TranId
        where tm.TranDateTime >= '$startdate'
        and tm.TranDateTime < dateadd( d, 1, '$enddate')
            and -- add 1 day for an exclusive upper bound
                tm.TranVoid = 0 and tm.IsPostVoided = 0 and
                tm.TranModifier not in (2,6) and -- skip if 2=Layaway sale or 6=Layaway cancel
                tm.StoreId in ('$storeid')
        and ds.DiscountType = 8
        $whereTxn
        order by ds.[Sequence]
    ENDQUERY

    $sth=$dbh->prepare("$query");
    $sth->execute();

    while (my @tmp = $sth->fetchrow_array()) {
        push(@ms_array,\@tmp);
    }

    # Now we loop through this hash and find the associated transaction
    @header=(
        "RegNum",
        "TxnNum",
        "TranDateTime",
        "SKU",
        "Dept",
        "Quantity",

        "OrigPrice",
        "MIXMATCH Discount",
        "Sale Price",
        "MixMatchNumber"
    );
    @array_to_print=\@header;

    my %hash_to_print;
    $previous_tranid=0;
    foreach my $ms_ref (@ms_array) {
        my @array=@$ms_ref;
        my $tranid=$array[0];
        my $discount_seq=$array[1];
        my $reg=$array[2];
        my $txn=$array[3];
        my $date=$array[4];
        my $price=$array[5];
        my $discamt=$array[6];

        # Find the sequence that is just prior to this sequence in the [tblMerchandiseSale]
        $query=dequote(<< "        ENDQUERY");
            select
                Sequence
            from
                ****..tblMerchandiseSale
            where
                TranId = '$tranid'
            order by Sequence
        ENDQUERY

        my $sth=$dbh->prepare("$query");
        $sth->execute();
        my @merch_sequences;
        while (my @tmp = $sth->fetchrow_array()) {
            push(@merch_sequences,$tmp[0]);
        }
        my $merchandise_sequence=0;
        for (my $x=0; $x<=$#merch_sequences; $x++) {
            my $ms=$merch_sequences[$x];
            my $next_ms=$merch_sequences[($x+1)];
            my $previous_ms=$merch_sequences[($x-1)];
            if ($discount_seq > $ms) {
                if (defined($next_ms)) {
                    if ($discount_seq < $next_ms) {
                        # The discount sequence fits in here.  It is less than this
                        # merchandise sequence but is more than the previous one
                        $merchandise_sequence=$ms;
                    }
                } else {
                    # There is no merchandise sequence higher
                    $merchandise_sequence=$ms;
                }
            }
        }
        # Find the merchandise record with this sequence number
        $query=dequote(<< "        ENDQUERY");
            select
                 tm.RegisterId as RegNum,
                 case when tm.StoreId='2098' and tm.RegisterId = '02' and
                              tm.TranNum between 4013 and 4023 and
                              convert(DateTime, convert(varchar(10),tm.TranDateTime,21))= '2009-03-11'
                        then tm.TranNum - 4000
                        else tm.TranNum end as TxnNum,
                 ---convert(DateTime, convert(varchar(10),tm.TranDateTime,111)) as TranDate,
                 tm.TranDateTime,
                 ms.SKU,
                 ms.Dept,
                 ms.Quantity,
                 ms.ExtOrigPrice,
                 --ms.Cost,
                 ms.MixMatchNumber,
                 ms.ExtRegPrice,
                 ms.Sequence
            from ****..tblTranMaster tm
                    join ****..tblMerchandiseSale ms on tm.TranId=ms.TranId

            where
                ms.TranId = '$tranid'
            and
                ms.Sequence = '$merchandise_sequence'
        ENDQUERY

        # Find the associated sequence
        $sth=$dbh->prepare("$query");
        $sth->execute();
        my $previous_seq=0;
        while (my @tmp = $sth->fetchrow_array()) {
            # We iterate through the merchandise records for this transaction by sequence number in descending order.
            my $RegNum=$tmp[0];
            my $TxnNum=$tmp[1];
            my $TranDateTime=$tmp[2];
            my $SKU=$tmp[3];
            my $Dept=$tmp[4];
            my $Quantity=$tmp[5];
            my $ExtOrigPrice=$tmp[6];
           # my $Cost=$tmp[7];
            my $MixMatchNumber=$tmp[7];
            my $ExtRegPrice=$tmp[8];
            my $item_seq=$tmp[9];
            #my $SalePrice=($ExtRegPrice + $discamt);
            my $SalePrice=($price + $discamt);
            my @seq_array=( "
            $RegNum",
            "$TxnNum",
            "$TranDateTime",
            "$SKU",
            "$Dept",
            "$Quantity",
            "$price",
            "$discamt",
            "$SalePrice",
            "$MixMatchNumber",
             );

            push(@array_to_print,\@seq_array);
            $hash_to_print{"$TxnNum"."."."$item_seq"}=\@seq_array;
        }
    }
    @array_to_print=\@header;
    # Put this array to print in order of txn
    foreach my $TxnNum (sort {$a <=> $b} (keys(%hash_to_print))) {
        my $ref=$hash_to_print{$TxnNum};
        push(@array_to_print,$ref);
    }
    $tbl_ref=\@array_to_print;
    if (recordCount($tbl_ref)) {
        my @fieldsum = ('<b>Total:</b>','');
        my @summary_fieldsum = ('<b>Total:</b>','');
        my @sum_columns = (5,6,7,8);
        #   get totals for the specified columns.
        for my $datarows (1 .. $#$tbl_ref){
            my $thisrow = @$tbl_ref[$datarows];
            foreach my $column (1 ..$#$thisrow) {
                if (grep(/$column/,@sum_columns)) {
                    $fieldsum[$column] += trim($$thisrow[$column]);
                    for (my $x=0; $x<=$#sum_columns; $x++) {
                        if ($sum_columns[$x] == $column) {
                            $summary_fieldsum[($x +1)] += trim($$thisrow[$column]);
                        }
                    }
                } else {
                    $fieldsum[$column] = '';
                }
            }

        }
        my @summary_header=('');
        foreach my $c (@sum_columns) {
            # Grab the headers for the fields we want.
            push(@summary_header,$$tbl_ref[0][$c]);
        }
        my @summary_array_to_print=\@summary_header;
        push @$tbl_ref, \@fieldsum;
        push @summary_array_to_print, \@summary_fieldsum;
        $total_origPrice+=$fieldsum[6];
        $total_bogoDiscounts=$fieldsum[7];
        $total_sellPrice+=$fieldsum[8];
        $totals_hash{"Quantity"}+=$fieldsum[5];
        $totals_hash{"OrigPrice"}+=$fieldsum[6];
        $totals_hash{"MIXMATCHDiscounts"}=$fieldsum[7];
        $totals_hash{"SellPrice"}+=$fieldsum[8];
        my @format = ('', '',
        '',
        '',
        '',
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        '',
        '',
        );
        my @summary_format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},

        );
        print $q->p("MixMatch Discounts found for store number $storeid between $startdate and $enddate");
        if ($summary) {
            printResult_arrayref2(\@summary_array_to_print, '', \@summary_format);
        } else {
            printResult_arrayref2($tbl_ref, '', \@format);
        }
    } else {
        print $q->p("No MixMatch Discounts found for store number $storeid between $startdate and $enddate");
    }

    # Show the totals of all of the discounts
    @header=(
        "Total Quantity",
        "Total Original Price",

        "Total Item Discounts",
        "Total Transaction Discounts",
        "Total Promotion Discounts",
        "Total MIXMATCH Discounts",
        "Total Discounts",
        "Total Sell Price"
    );

    @array_to_print=\@header;
    my @array=(
        $total_origPrice,
        $total_transDiscounts,
        $total_itemDiscounts,
        $total_promoDiscounts,
        $total_bogoDiscounts,
        $total_sellPrice
    );
    $totals_hash{"Discounts"}=($totals_hash{"ItemDiscounts"} + $totals_hash{"TransDiscounts"} + $totals_hash{"PromoDiscounts"} + $totals_hash{"MIXMATCHDiscounts"});
    @array=(
        $totals_hash{"Quantity"},
        $totals_hash{"OrigPrice"},
        $totals_hash{"ItemDiscounts"},
        $totals_hash{"TransDiscounts"},
        $totals_hash{"PromoDiscounts"},
        $totals_hash{"MIXMATCHDiscounts"},
        $totals_hash{"Discounts"},
        $totals_hash{"SellPrice"},
    );
    push(@array_to_print,\@array);
    if (recordCount(\@array_to_print)) {
        my @format = (
        {'align' => 'Right', 'num' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        {'align' => 'Right', 'currency' => ''},
        );
        print $q->h3("Total Discounts");
        printResult_arrayref2(\@array_to_print, '', \@format);
        # Let's check that the totals make sense.  My expectation is that the total OrigPrice, less all of the discounts, should yield the total SellPrice.
        my $expected_sell=(sprintf("%.2f",$totals_hash{"OrigPrice"}) + sprintf("%.2f",$totals_hash{"ItemDiscounts"}) + sprintf("%.2f",$totals_hash{"TransDiscounts"})
        + sprintf("%.2f",$totals_hash{"PromoDiscounts"}) + sprintf("%.2f",$totals_hash{"MIXMATCHDiscounts"}));
        my $sell_price=sprintf("%.2f",$totals_hash{"SellPrice"});
        my $diff=sprintf("%.2f",($expected_sell - $sell_price));

        if ($diff > 0.00) {
            print $q->p("Note: There appears to be a discrepancy in the totals.");
            print "Expected Sell Price: \$$expected_sell<br>";
            print "Sell Price: \$$sell_price<br>";
            print "Difference: \$$diff<br>";
        }
    } else {
        print $q->p("No Totals data gathered");
    }
    $dbh->disconnect;

    StandardFooter();
	###

	} elsif ($report eq "STOREPRICE_QRY"){
		print $q->h2("Store Price Report");
		if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
		my $store_options= $q->option({-value=>'V1'}, "All Company Stores");
		#$query = "select storeId,storeName from stores order by storeId";
		$query = "
		SELECT [StoreId]
		  ,[StoreName]

		FROM [****].[dbo].[rtblStores]
		where EndDate is  NULL";

		my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
		for my $datarows (1 .. $#$tbl_ref_alias) {
			my $thisrow = @$tbl_ref_alias[$datarows];
			my $number = trim($$thisrow[0]);
			my $name = trim($$thisrow[1]);
			$store_options .= $q->option({-value=>$number}, "$number - $name");
		}
		$dbh->disconnect;
		### Build the table
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->start_form({-action=>"$scriptname", -method=>'get'}), "\n";
		print $q->input({-type=>"hidden", -name=>"report", -value=>"STOREPRICE_RPT"});
		print $q->Tr($q->td({-class=>'tablehead', -nowrap=>undef, -colspan=>2}, $q->b("Store Price Reports")));
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "Store Number:"),
				   $q->td({-class=>'tabledatanb', -align=>'left'},
						  $q->Select({-name=>"store_selection", -size=>"1"}), $store_options)
				  );


		####
		# Add a checkbox
		print $q->Tr(
			$q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
			$q->input({-accesskey=>'E', -type=>"checkbox", -name=>"sku_restriction", -checked}) .
			$q->font({-size=>-1}, "Show only SKUs that are marked down")
			));
		print $q->Tr(
			$q->td({-class=>'tabledatanb', -nowrap=>undef, -colspan=>3},
			$q->input({-accesskey=>'E', -type=>"checkbox", -name=>"sku_qty_restriction", -checked}) .
			$q->font({-size=>-1}, "Show only SKUs w. Qty")
			));
		# Add the submit button
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
				   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
						  $q->submit({-accesskey=>'S', -value=>"   Submit   "}))
				  );

		print $q->end_form(), "\n";
		print $q->end_table(),"\n";

		print $q->p();
		StandardFooter();
	} elsif ($report eq "STOREPRICE_RPT"){
		print $q->h2("Store Price Report");
		if ($testing_mode) {
            print "--testing mode--<br>";
        }
		my $store_selection   = trim(($q->param("store_selection"))[0]);
		my $where_condition;
		my $restricted_label;
		my $qty_restricted_label = "(Showing SKUs with or without Qty.)";
		my $sku_qty_restriction   = trim(($q->param("sku_qty_restriction"))[0]);
		if ($sku_qty_restriction) {
			$qty_restricted_label="(Showing only SKUs w. Qty.)";
			$where_condition.=" and tss.Qty > 0";
		}

		my $sku_restriction   = trim(($q->param("sku_restriction"))[0]);
		if ($sku_restriction) {
			$restricted_label="(Showing only SKUs marked down.)";
			$where_condition.=" and vi.[Item Disc_ Group] like 'MARK%'";
		}
		my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
		my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
		my @format = ('', '', '',
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'currency' => ''},
		{'align' => 'Right', 'num' => ''},
		);
		my @links;

		if ($store_selection eq "V1") {
			$query = "
			SELECT
				tss.StoreId,
				rs.[StoreName],
				count(tss.SKU) as Count

			FROM
				[****].[dbo].[tblStoreSkuInfo] tss
				join [dbNavision].[dbo].[Villages\$Item] vi on vi.[No_] COLLATE DATABASE_DEFAULT = tss.[SKU] COLLATE DATABASE_DEFAULT
				join [****].[dbo].[rtblStores] rs on tss.StoreId = rs.StoreId
				join [****].[dbo].[tblAkronSkuInfo] tasi on tasi.SKU = tss.SKU
			where
				rs.[StoreCode] = 'V1'
			and
				rs.EndDate is NULL
			and
				tss.[Price] <>  vi.[Unit Price]
			and
				tss.SKU not like '22%'
			and
				tss.SKU not like '99%'

			$where_condition
			group by tss.StoreId,rs.[StoreName]
			order by tss.[StoreId]
			";
			@format = ('', '', '', '',
			{'align' => 'Right', 'currency' => ''},
			{'align' => 'Right', 'currency' => ''},

			);
			@links = ( "$scriptname?report=STOREPRICE_RPT&sku_qty_restriction=$sku_qty_restriction&sku_restriction=$sku_restriction&store_selection=".'$_' );

		} else {
			$query = "
			SELECT
				tss.StoreId,
				tss.SKU,
				tss.descrip,
				tss.price as 'StorePrice',
				vi.[Unit Price] as 'UnitPrice',
				tasi.price as 'AkronPrice',
				tss.Qty
			FROM
				[****].[dbo].[tblStoreSkuInfo] tss
				join [dbNavision].[dbo].[Villages\$Item] vi on vi.[No_] COLLATE DATABASE_DEFAULT = tss.[SKU] COLLATE DATABASE_DEFAULT
				join [****].[dbo].[tblAkronSkuInfo] tasi on tasi.SKU = tss.SKU
			where
				tss.[StoreId] = '$store_selection'
			and
				tss.[Price] <>  vi.[Unit Price]
			and
				tss.SKU not like '22%'
			and
				tss.SKU not like '99%'

			$where_condition
			";
		};



		my $tbl_ref = execQuery_arrayref($dbh, $query);

		if (recordCount($tbl_ref)) {
			print $q->b("Store Price Discrepancies $restricted_label $qty_restricted_label<br>\n");

			printResult_arrayref2($tbl_ref, \@links,\@format);
		} else {
			print $q->b("No Store Price Discrepancies found. <br>\n");
		}
		print $q->p();
		$dbh->disconnect;
		StandardFooter();

		###

	} else {
        print $q->h3("ERROR: $report not found");
    }
}
#print $q->a({-href=>"javascript:parent.history.go(-1)"}, "go back");

# close the page.
print $q->end_html;

exit;

################
# FUNCTIONS
################

sub AuthenticateUser {
   my ($strUserID,$strUserPassword) = @_;

   return "Authentication Failed" unless ($strUserID);
   return "Authentication Failed" unless ($strUserPassword);

   my $ad = Authen::Simple::ActiveDirectory->new(
      host      => 'villages.com',
      principal => 'villages.com'
   );

   if ( $ad->authenticate( $strUserID, $strUserPassword) ) {
      return $strUserID;
   }

   return "Authentication Failed";
}

sub LogMsgQ {
	# Quiet LogMsgQ - Does not print to screen
	use Time::HiRes qw(gettimeofday);
	my ($logString) = @_;

	# prefix $logstring with "YYYYMMDD hhmmss : "
	my ($now, $usec) = gettimeofday();
	my @ltm = localtime($now);
	my $datetime = sprintf("%4d%02d%02d %02d:%02d:%02d.%03d", $ltm[5]+1900, $ltm[4]+1, $ltm[3], $ltm[2], $ltm[1], $ltm[0],($usec/1000));
	$logString = $datetime . " : " . $logString;

	my $logfile = $0;
	$logfile =~ s/\.pl$/\.log/i;
	open(LOGFILE, ">>$logfile") or return;
	printf LOGFILE "$logString\n";
	close(LOGFILE);
}

# returns 3* spaces, used for indenting log lines.
sub indented {
   my($n) = @_;
   return '   ' x $n;
}




sub throwError {
   my $errorstr  = shift;
	print $q->h2("Error:");
   print $q->h2("$errorstr");
	#print "please ". $q->a({-href=>"javascript:parent.history.go(-1)"}, "go back").
	#          ", correct this error and resubmit";
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"DEFAULT"});
    print $q->submit({-accesskey=>'G', -value=>"   Start Over   "});
    print $q->end_form(), "\n";
	exit;
}
sub processMixMatchInputFile {
    my $inputfile=shift;
    my $mixmatchId=shift;
    unless (-e $inputfile) {
        print $q->p("Error: Failed to find $inputfile");
        StandardFooter();
        exit;
    }
    unless ($mixmatchId) {
        print $q->p("Failed to determine $mixmatchId");
        StandardFooter();
        exit;
    }
    print $q->p("Reading in SKUs list");
    # ############################################################
    # Read in the list of SKUs being marked down and their price
    # ############################################################
    open(INPUT,"$inputfile");
    my @inputContent=(<INPUT>);
    close INPUT;
    print $q->p("Open $mixmatchSKUfile");
    unless (open(MMSKU,">$mixmatchSKUfile")) {
        print $q->p("Error opening $mixmatchSKUfile");
        LogMsgQ("Error opening $mixmatchSKUfile in processMixMatchInputFile function");
        StandardFooter();
        exit;
    }
    my $counter=0;
    print MMSKU "MIXMATCH\tSKU\n";
    foreach my $i (@inputContent) {
        # Each line of the inputfile represents a SKU with various info about its costs
        chomp($i);
        my @tmp=split(/,/,$i);
        my $sku=$tmp[0];

        my @data=();

        # Make certain we are looking at a line with a SKU on it
        unless ($sku =~ /^[0-9]/){
            LogMsgQ("Skipping line ($i)");
            next;
        }
        $counter++;
        # create the MIXMATCHSKU.TSV
        print MMSKU "$mixmatchId\t$sku\n";
        if ($debug) {
            LogMsgQ(indent(1)."SKU: $sku  ");
        }
    }
    if ($counter == 0) {
        LogMsgQ(indent(1)."Found no SKUs defined");
        print $q->p("Found no SKUs defined");
        StandardFooter();
        exit;
    }
    close MMSKU;
    LogMsgQ(indent(1)."Read in $counter SKUs for MixMatch definition");
    print $q->p("Processed $counter SKUs for MixMatch $mixmatchId");
    return $counter;
}
sub processXferInputFile {
    my $inputfile=shift;
    unless (-e $inputfile) {
        LogMsgQ("Failed to find $inputfile");
        LogMsgQ("$0 ending");
        exit;
    }
    LogMsgQ("Reading in SKUs list");
    # ############################################################
    # Read in the list of SKUs being marked down and their price
    # ############################################################
    open(INPUT,"$inputfile");
    my @inputContent=(<INPUT>);
    close INPUT;

    my $tQty;
    my $counter=0;

    foreach my $i (@inputContent) {
        # Each line of the inputfile represents a SKU with various info about its costs
        chomp($i);
        my @tmp=split(/,/,$i);
        my $sku=$tmp[0];
        my $qty=$tmp[1];
        my @data=();

        # Make certain we are looking at a line with a SKU on it
        unless ($sku =~ /^[0-9]/){
            LogMsgQ("Skipping line ($i)");
            next;
        }
        $counter++;

        if ($debug) {
            LogMsgQ(indent(1)."SKU: $sku Qty: $qty");
        }
        $XferDocSKU{$sku}+=$qty;
        $tQty+=$qty;
    }
    if ($counter == 0) {
        LogMsgQ(indent(1)."Found no SKUs to Receive");
        LogMsgQ("$0 ending");
        exit;
    }
    LogMsgQ(indent(1)."Read in $counter SKUs to receive");
    return $tQty;
}

sub createXferOutDoc {
    # ############################################################
    # create a Transfer Out Document
    # ############################################################
    my $fromStore=shift;
    my $toStore=shift;
    my $date_label=shift;
    my $tQty=shift;
    $fromStore = substr('0000'. $fromStore,-4) if (length($fromStore) <4);
    $toStore = substr('0000'. $toStore,-4) if (length($toStore) <4);
    LogMsgQ("Creating Transfer Out document for $fromStore...");
    unless ((-e "${dnload_dir}/${fromStore}") && (-d "${dnload_dir}/${fromStore}")) {
        print $q->p("Error: I cannot locate ${dnload_dir}/${fromStore}");
        LogMsgQ("Error: I cannot locate ${dnload_dir}/${fromStore}");
        StandardFooter();
        exit;
    }
    my $ponum = "Xfer";
    my $targetpath="c:/mlink/akron"; # Where the file is to go when polled down to the store

    my $DocNo;
    $DocNo=getDocNo("$fromStore");
    my $filename="${DocNo}.XFO";
    my $outputFile="${dnload_dir}/${fromStore}/${filename}";

    LogMsgQ(indent(1)."Processing Order number: $DocNo");
    ###
    # Create the entry in the transfer table in the database
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
        LogMsgQ("createXferOutDoc: Failed to connect to $mysqld and $posdb database");
        print "createXferOutDoc: Failed to connect to $mysqld and $posdb database<br>";
        StandardFooter();
        exit;
    }
    my @ltm = localtime();
    my $date_label2=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB
    my $query = "
    insert into
        transfers
    (transferCreator,creationDate,toStore,fromStore)
    values ('$uid',CURRENT_TIMESTAMP,'$toStore','$fromStore')
    ";

    unless ($dbh->do($query)) {
        print "Failed to add entry to transfers table<br>";
        print "query=".$query."<br>\n";
        print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
        print "errstr=".$dbh->errstr."<br>\n";
        StandardFooter();
        exit;
    }
    # Get the transferId for the new transfer just inserted
    my $transferId;
    $query="select
        transferId
    from
        transfers
    where
        transferCreator like '$uid' and
        creationDate like '$date_label2%'
    order by
        transferId desc
    limit 1";
    my $sth=$dbh->prepare("$query");
    if ($sth->execute()) {
        while (my @tmp = $sth->fetchrow_array()) {
            $transferId=$tmp[0];
        }
    } else {
        print "ERROR with db query: ($query)<br>";
        StandardFooter();
        exit;
    }

    if ($transferId) {
        LogMsgQ("obtained transferId");
    } else {
        LogMsgQ("Error: Failed to determine transferId");
        print "ERROR: Failed to determine transferIde<br>";
        $result_code=0;
        return;
    }
    ###
    my $dnLoadFile="${dnload_dir}/${filename}";
    open OUTFILE, ">$outputFile";

    # Create the Transfer Document
    LogMsgQ(indent(1)."Creating Transfer Out Document ");
    LogMsgQ(indent(1)."-> order $DocNo/$ponum in $filename");
    # Create the Header
    my @header = ('H', $DocNo, '', '', $toStore, '', $date_label, '',$tQty,'');
    my $output = pack "A1A20A1A2A4A2A8A2A4A9", @header;
    print OUTFILE "$output\n";

    # Create the Data portion
    foreach my $sku (sort(keys(%XferDocSKU))) {
        my $qty=$XferDocSKU{$sku};
        my $qty2=($qty * 1000);
        my @detail = ('D', $sku, $qty2, '', '');
        $output = pack "A1A18A6A4A2", @detail;
        print OUTFILE "$output\n";

        # Enter this data into the database as well
        my $query = "
        insert into
            transfer_detail
            (transferId,skuNum)
        values
            ('$transferId','$sku')
        ";

        unless ($dbh->do($query)) {
            print "Failed to add sku $sku to transfer_detail table<br>";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
            $result_code=0;
            return;
        }
    }

    close OUTFILE;

    # Update newfiles
    open FILE, ">> $dnload_dir\\$fromStore\\$newfilestxt";
    print FILE "$filename\t$targetpath\n";
    close FILE;
    # update the status of the transfer
    $query = "
    update
        transfers
    set status = 'crtd'
    where
        transferId = '$transferId'
    ";

    if ($dbh->do($query)) {
        LogMsgQ("Set transfer $transferId status to created");
    } else {
        print "Failed to set transfer status in transfer table<br>";
        print "query=".$query."<br>\n";
        print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
        print "errstr=".$dbh->errstr."<br>\n";
        $result_code=0;
        return;
    }
    $dbh->disconnect;
    LogMsgQ(indent(1)."Transfer Out Document has been created - $outputFile.");
    print $q->h3("Created transfer out with PO number: $ponum");
    return $DocNo;
}


sub publishSkus {
    # ############################################################
    # Publish SKUs being transfered to the receiving store if they are not already in their system
    # ############################################################

    my $toStore=shift;

    my $pub_count=0;
    my $existing=0;
    foreach my $sku (sort(keys(%XferDocSKU))) {
        # Check if the sku is already at the store
        my $query = "select sku from tblStoreSkuInfo where StoreId='$toStore' and sku = '$sku'";
        my $tbl_ref2 = execQuery_arrayref($mssql_dbh, $query);
        # Skip this sku if it is already at the store
        if (recordCount($tbl_ref2) >0) {
            $existing++;
            next;
        }
        # Otherwise, call the script to create the SKU for that store.
        print "Creating SKU $sku in store $toStore<br>\n";
        my @result=`$createStoreStandardSku --storeId=$toStore --item=$sku --onlynew`;

        foreach my $r (@result) {
            chomp($r);
            next if ($r =~ /****/i);
            my @tmp=split(/ : /,$r);
            chomp(my $response=$tmp[1]);
            if ($response =~ /error/) {
                print "$response</br>";
                LogMsgQ(indent(1)."$response");
            } elsif ($response =~ /Processed 1/i) {
                $pub_count++;
                print "--- Published $sku<br>\n";
                LogMsgQ(indent(1)."--- Published $sku");
            }
        }
    }
    if ($pub_count) {
        print $q->p("Published $pub_count SKUs to $toStore");
        LogMsgQ("Published $pub_count SKUs to $toStore");
    }
    if ($existing) {
        print $q->p("$existing SKUs already in $toStore");
        LogMsgQ("$existing SKUs already in $toStore");
    }
}

sub createXferInDoc {
    # ############################################################
    # create a Transfer In Document
    # ############################################################
    my $DocNo = shift;
    my $fromStore=shift;
    my $toStore=shift;
    my $date_label=shift;
    my $tQty=shift;
    $fromStore = substr('0000'. $fromStore,-4) if (length($fromStore) <4);
    $toStore = substr('0000'. $toStore,-4) if (length($toStore) <4);
    LogMsgQ("Creating Transfer In document for $toStore...");
    unless ((-e "${dnload_dir}/${fromStore}") && (-d "${dnload_dir}/${toStore}")) {
        LogMsgQ ("Error: I cannot locate ${dnload_dir}/${toStore}");
        exit;
    }
    my $ponum = "Xfer";
    my $targetpath="c:/mlink/akron"; # Where the file is to go when polled down to the store

    my $filename="${DocNo}.XFI";
    my $outputFile="${dnload_dir}/${toStore}/${filename}";


    LogMsgQ(indent(1)."Processing Order number: $DocNo");
    my $dnLoadFile="${dnload_dir}/${filename}";
    open OUTFILE, ">$outputFile";

    # Create the Transfer Document
    LogMsgQ(indent(1)."Creating Transfer In Document ");
    LogMsgQ(indent(1)."-> order $DocNo/$ponum in $filename");
    # Create the Header
    my @header = ('H', $DocNo, '', '', $fromStore, '', $date_label, '',$tQty,'');
    my $output = pack "A1A20A1A2A4A2A8A2A4A9", @header;
    print OUTFILE "$output\n";

    # Create the Data portion
    foreach my $sku (sort(keys(%XferDocSKU))) {
        my $qty=$XferDocSKU{$sku};
        my $qty2=($qty * 1000);
        my @detail = ('D', $sku, $qty2, '', '');
        $output = pack "A1A18A6A4A2", @detail;
        print OUTFILE "$output\n";
    }

    close OUTFILE;

    # Update newfiles
    open FILE, ">> $dnload_dir\\$toStore\\$newfilestxt";
    print FILE "$filename\t$targetpath\n";
    close FILE;

    LogMsgQ(indent(1)."Transfer In Document has been created - $outputFile.");
}

sub getDocNo {
    # Come up with a unique document name
    my $store=shift;
    my @ltm = localtime();
    my $date = sprintf("%04d%02d%02d", $ltm[5]+1900, $ltm[4]+1, $ltm[3]);
    my $time = sprintf("%02d%02d%02d", $ltm[2], $ltm[1], $ltm[0]);
    my $name;
    if ($store eq "0000") {
        # If the store is specified, generate a document number a bit differently
        # Here I am using the date to come up with a unique number.
        my $sum=($ltm[5] + $ltm[4]+1 + $ltm[3] +$ltm[2] + $ltm[1] + $ltm[0]);
        $name="$store"."Merch"."$sum"
    } else {
        my $sum=($ltm[5] + $ltm[4]+1 + $ltm[3] +$ltm[2] + $ltm[1] + $ltm[0]);
        $name="$store"."XFR"."$sum"
    }
    return $name;
}

sub process_mmasc {
    my $inputfile=shift;
    my $uid=shift;
	# Process the ASC file into the tblStoreMixMatch
	if (-f $inputfile) {
		open(INPUTFILE,"$inputfile");
		my @file_contents=(<INPUTFILE>);
		close INPUTFILE;
		my $count=0;
		foreach my $entry (@file_contents) {
			my @tmp=split(/\,/,$entry);
			my $mixmatchId=$tmp[0];
			my $disc1=$tmp[65];
			my $disc2=$tmp[10];
			my $disc3=$tmp[21];
			my $disc4=$tmp[27];
			my $description=$tmp[6];
			my $name=$tmp[69];
			my $effectiveDate=$tmp[60];
			my $expireDate=$tmp[61];

			my $discountAmt=0;
			if ($disc1) {
				$discountAmt=$disc1;
			}
			if ($disc2) {
				$discountAmt=$disc2;
			}
			if ($disc3) {
				$discountAmt=$disc3;
			}
			if ($disc4) {
				$discountAmt=$disc4;
			}
			# Convert the date formats
			my @tmp_date=split(/\//,$effectiveDate);
			my $Y=$tmp_date[0];
			my $M=$tmp_date[1];
			my $D=$tmp_date[2];
			if (length($Y) == 2) {
				$Y="20".$Y;
			}
			$effectiveDate="$Y"."-"."$M"."-"."$D 00:00:00.000";
	   ###	# We cannot know the creation date from the ASC file so just set it to the effective date
			my $creationDate=$effectiveDate;

			@tmp_date=split(/\//,$expireDate);
			$Y=$tmp_date[0];
			$M=$tmp_date[1];
			$D=$tmp_date[2];
			if (length($Y) == 2) {
				$Y="20".$Y;
			}
			$expireDate="$Y"."-"."$M"."-"."$D 23:59:59.000";

			$count++;
			 
			#print " update_history_table  MMID: $mixmatchId Discount: $discountAmt Name: $name Description: $description Created:  $creationDate Effective: $effectiveDate Expire: $expireDate<br />";
			update_history_table("$mixmatchId","$discountAmt","$name","$description","$creationDate","$effectiveDate","$expireDate","utility");
		}
	} else {
		print $q->p("ERROR: Failed to access $inputfile");
		exit;
	}
}

sub process_inputfile {
    my $inputfile=shift;
    my $uid=shift;
    my $delete_flag=shift;
	my @filedata=();
    my @sku_list=();
	my $line="";
	my @lineinfo=();
	my $sku="";
	my $price="";
    my $start_date;
    my $end_date;
    my $percent_off;
	my $dollar_off;
    my $promo_name;
    my $promoNum;
    my $sku_count=0;
    unless (open(INPUT,"$inputfile")) {
        # Failed to open input file so return a failed result code
        LogMsgQ("Failed to open $inputfile in process_inputfile function");
        $result_code=0;
        return 0;
    }
    print $q->p("Reading in SKU list...");
	@filedata=(<INPUT>);
	close INPUT;
	foreach $line (@filedata) {
		chomp($line);
		@lineinfo=split(/,/,$line);

		if ($lineinfo[0] =~ /start/i) {
			my @tmp=split(/\//,$lineinfo[1]);
			my $mm=$tmp[0];
			my $dd=$tmp[1];
			my $yy=$tmp[2];
			@tmp=split(//,$yy);
			$yy="${tmp[$#tmp-1]}${tmp[$#tmp]}";
			$start_date="${yy}/${mm}/${dd}";
			next;
		}
		if ($lineinfo[0] =~ /end/i) {
			my @tmp=split(/\//,$lineinfo[1]);
			my $mm=$tmp[0];
			my $dd=$tmp[1];
			my $yy=$tmp[2];
			@tmp=split(//,$yy);
			$yy="${tmp[$#tmp-1]}${tmp[$#tmp]}";
			$end_date="${yy}/${mm}/${dd}";
			next;
		}
		if ($lineinfo[0] =~ /percent/i) {
			$percent_off=$lineinfo[1];
			next;
		}
		if ($lineinfo[0] =~ /dollar/i) {
			$dollar_off=$lineinfo[1];
			next;
		}

		if ($lineinfo[0] =~ /name/i) {
			$promo_name=$lineinfo[1];
			next;
		}

		if ($lineinfo[0] =~ /number/i) {
			$promoNum=$lineinfo[1];
			next;
		}
		my $sku=$lineinfo[0];		# The sku is the first item
		my $desc=$lineinfo[1];
		if ($sku =~ /[0-9]/) { 			# If we have a numeric value
			next if ($sku =~ /[a-z]/);	# If the value has no letters
			next if ($sku =~ /[A-Z]/);
			#print " -> sku: ($sku)";
            #if ($desc) {
            #    print "- desc: ($desc)";
            #}
            #print "\n";
			push(@sku_list,$sku);
            $sku_count++;
		}
	}
	unless ($start_date) {
        print "Error! No start date determined</br>\n";
        LogMsgQ("Error! No start date determined in process_inputfile function");
        $result_code=0;
        return 0;
	}
	unless ($end_date) {
        print "Error! No end date determined</br>\n";
        LogMsgQ("Error! No end date determined in process_inputfile function");
        $result_code=0;
        return 0;
	}
	unless (($percent_off) || ($dollar_off)) {
        print "Error! No percent_off or dollar_off determined</br>\n";
        LogMsgQ("Error! No percent_off or dollar_off determined in process_inputfile function");
        $result_code=0;
        return 0;
	}
	unless ($promoNum) {
		print "Error! No promo number determined</br>\n";
        LogMsgQ("Error! No promo number determined in process_inputfile function");
        $result_code=0;
        return 0;
	}
	unless ($promo_name) {
		print "Error! No promo name determined</br>\n";
        LogMsgQ("Error! No promo number determined in process_inputfile function");
        $result_code=0;
        return 0;
	}
    unless ($sku_count) {
        print $q->p("Error: Found $sku_count SKUs");
        StandardFooter();
        exit;
    }

    print $q->p("Found $sku_count SKUs on promotion $promo_name");
    my $promoId=create_ascfiles(\@sku_list,$start_date,$end_date,$percent_off,$dollar_off,$promoNum,$promo_name,$uid,$delete_flag);
    if ($result_code){
        print $q->p("Cleaning up...");
        unlink $inputfile;
        print $q->p("Done");
    } else {
        print $q->p("Error trying to create ASC file");
        $result_code=0;
        return 0;
    }
    $result_code=1;
    return $promoId;
}

sub recreate_promos {
    my $promoref=shift;
    my $delete_flag=shift;
    my @promos=@$promoref;
    my $recreate_flag=1;    # This tells the create_asc function that we are re-creating a promo rather than creating a new one
    my $append_flag=0;      # This controls whether we are appending to the ASC file or not
    my $dbh;
    unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
        LogMsgQ("CREATE_ASCFILES: Failed to connect to $mysqld and $posdb database");
        print "CREATE_ASCFILES: Failed to connect to $mysqld and $posdb database<br>";
        StandardFooter();
        exit;
    }
	LogMsgQ(indent(1)."User $uid at the recreate_promos function");	
    foreach my $p (@promos) {
        # Get the necessary promo info
        my $start_date;
        my $end_date;
        my $percent_off;
		my $dollar_off;
        my $promoNum;
        my $promo_name;
		my $discountType;
        my $query = "
		select
			promoNum,
			promoTitle,
			promoDiscount,
			discountType,
			DATE_FORMAT(effectiveDate, '%y/%m/%d'),
			DATE_FORMAT(expireDate, '%y/%m/%d')
		from
			promotions
		where
			promoId = $p";
        my $sth=$dbh->prepare("$query");
        if ($sth->execute()) {
            while (my @tmp = $sth->fetchrow_array()) {
                $promoNum=$tmp[0];
                $promo_name=$tmp[1];
				$discountType=$tmp[3];
                $start_date=$tmp[4];
                $end_date=$tmp[5];
				if ($discountType eq "dollar") {
					$dollar_off=$tmp[2];
				} else {
					$percent_off=$tmp[2];
					# The first promotions did not have the percent off recorded  This is a work-around for those
					unless ($percent_off) {
						# Get the percent off from the promo name
						my @tmp=split(/\s+/,$promo_name);
						$percent_off=$tmp[-1];
						$percent_off=~s/%//;
					}
				}
            }
        } else {
            print "ERROR with db query: ($query)<br>";
            StandardFooter();
            exit;
        }

        my @sku_list;
        $query="select skuNum from promotion_detail where promoId = $p";
        $sth=$dbh->prepare("$query");
        if ($sth->execute()) {
            while (my @tmp = $sth->fetchrow_array()) {
                push(@sku_list,$tmp[0]);
            }
        } else {
            print "ERROR with db query: ($query)<br>";
            StandardFooter();
            exit;
        }

        # Create the promotxn asc file
		if ($testing_mode) {
			print "Calling create_ascfiles promotxn start ($start_date) end ($end_date) % ($percent_off) $ ($dollar_off) num ($promoNum) name ($promo_name) uid ($uid) d ($delete_flag) r ($recreate_flag) a ($append_flag)<br />";
        }
		LogMsgQ(indent(1)."User $uid creating ascfiles for promo number: $promoNum name: $promo_name");	
		my $rc=create_ascfiles(\@sku_list,$start_date,$end_date,$percent_off,$dollar_off,$promoNum,$promo_name,$uid,$delete_flag,$recreate_flag,$append_flag);
        $append_flag=1;
    }
    $dbh->disconnect;
}
sub create_ascfiles {
    my ($array_ref,$start_date,$end_date,$percent_off,$dollar_off,$promoNum,$promo_name,$uid,$delete_flag,$recreate_flag,$append_flag) = @_;
    my @sku_list=@$array_ref;
	my $promoDiscount;
	my $discountType;
	if ($percent_off) {
		$promoDiscount=$percent_off;
		$discountType="percent";
	} elsif ($dollar_off) {
		$promoDiscount=$dollar_off;
		$discountType="dollar";
	} else {
		print $q->p("Error: Failed to determine discount amount.");
		LogMsgQ("Error: Failed to determine discount amount in create_ascfiles function.");
		StandardFooter();
		exit;
	}
    my $detail_number=0;
    my @ltm = localtime();
    my $date_label=sprintf("%02d%02d%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);   # For the ASC file
    my $date_label2=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB
	# Create the promotxn asc files
    my $type="add";
    if ($delete_flag) {
		LogMsgQ("Creating ASC files - delete flag IS SET");
        $update=3;
        $type="del";
    } else {
		LogMsgQ("Creating ASC files - delete flag is not set");
	}
    my $dbh;
    my $print_method=1; # Whether it shows the % off on the receipt or not
    my $promoId;    # This is the id generated when we insert the promoNum into the promotions database
 
    #$promotxnfile="${upload_dir}/${uid}promotxn.asc";
   
    if ($append_flag) {
		LogMsgQ(indent(1)."Open $promotxnfile - appending");
        # We append to an existing file
        unless (open(PROMOTXN,">>$promotxnfile")) {
            print $q->p("Error opening $promotxnfile");
            LogMsgQ("Error opening $promotxnfile in create_ascfiles function");
            StandardFooter();
            exit;
        }
    } else {
		LogMsgQ(indent(1)."Open $promotxnfile - starting new");
        unless (open(PROMOTXN,">$promotxnfile")) {

            print $q->p("Error opening $promotxnfile");
            LogMsgQ("Error opening $promotxnfile in create_ascfiles function");
            StandardFooter();
            exit;
        }

    }

    # If we are re-publishing an existing promotion, we do not need or want to enter it into the database.
    unless ($recreate_flag) {
        print $q->p("Creating promotion $promo_name");
        # Connect to the database
        #my $dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop");

        unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
            LogMsgQ("CREATE_ASCFILES: Failed to connect to $mysqld and $posdb database");
            print "CREATE_ASCFILES: Failed to connect to $mysqld and $posdb database<br>";
            StandardFooter();
            exit;
        }
        # Create the entry in the promotions table
        my $query = "
        insert into
            promotions
        (promoNum,promoTitle,promoDiscount,discountType,promoCreator,creationDate,effectiveDate,expireDate,type)
        values ('$promoNum','$promo_name','$promoDiscount','$discountType','$uid',CURRENT_TIMESTAMP,'$start_date','$end_date','$type')
        ";

        unless ($dbh->do($query)) {
            print "Failed to add promotion $promoNum to promotions table<br>";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
            StandardFooter();
            exit;
        }

        # Get the promoId for the new promotion just inserted
        $query="select promoId from promotions where promoNum = $promoNum and promoCreator like '$uid' and creationDate like '$date_label2%'
        order by promoId desc
        limit 1";
        my $sth=$dbh->prepare("$query");
        if ($sth->execute()) {
            while (my @tmp = $sth->fetchrow_array()) {
                $promoId=$tmp[0];
            }
        } else {
            print "ERROR with db query: ($query)<br>";
            StandardFooter();
            exit;
        }

        if ($promoId) {
            LogMsgQ("Obtained promoId $promoId");
        } else {
            LogMsgQ("Error: Failed to obtain promoId");
            print "ERROR: Failed to obtain promoId<br>";
            $result_code=0;
            return;
        }
    }
	foreach my $sku (@sku_list) {

		# Fields:
		# 1 Update Type
		# 2 Start Date yy/mm/dd
		# 3 Stop Date yy/mm/dd
		# 4 Start Time hh:mm
		# 5 Stop Time hh:mm
		# 6 PLU/SKU
		# 7 NA
		# 8 User Flag 1
		# 9 User Flag 2
		# 10 DEPT
		# 11 NA
		# 12 GROUP
		# 13 VENDOR
		# 14 From PRICE POINT
		# 15 To PRICE POINT
		# 16 Percent Off
		# 17 $ Markdown
		# 18 New Price
		# 19 Promotion Number
		# 20 Category
		# 21 Accumulate
		# 22 Exception
		# 23 Exception Number
		# 24 NA
		# 25 NA
		# 26 NA
		# 27 NA
		# 28 NA
		# 29 NA
		# 30 NA
		# 31 Description
		# 32 Apply Price
		# 33 User Flage 3
		# 34 User Flage 4
		# 35 User Flage 5
		# 36 User Flage 6
		# 37 Detail Number
		# 38 Print Method
	 
		# 2014-08-21 - 	Adding the ApplyPrice variable.  This field had previously been hard coded to "1" but this setting
		# 				was not working so well in SAP POS 2.3 when multiple concurrent promotions were configured. I determined
		#				that the "Use Original Feature" setting worked better.  This is equivalent to "0". - kdg
		my $ApplyPrice = 0;	# Default setting for the Promo Method Applied To field - Use Original Feature
		
		# sample 1,08/07/03,08/09/30,00:01,23:59,"5400910",,,,,,,,0,0,90,,,2,,N,,,,,,,,,,"World Caravan Old II",1,,,,,1259,1
		$detail_number++;

		print PROMOTXN "$update,$start_date,$end_date,00:01,23:59,$sku,,,,,,,,0,0,$percent_off,$dollar_off,,$promoNum,,N,,,,,,,,,,$promo_name,$ApplyPrice,,,,,$detail_number,$print_method\n";
        # Enter this data into the database as well
        my $query = "
        insert into
            promotion_detail
        (promoId,skuNum) values ('$promoId','$sku')
        ";
        # If we are re-publishing an existing promotion, we do not need or want to enter it into the database.
        unless ($recreate_flag) {
            unless ($dbh->do($query)) {
                print "Failed to add sku $sku to promotion_detail table<br>";
                print "query=".$query."<br>\n";
                print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
                print "errstr=".$dbh->errstr."<br>\n";
                $result_code=0;
                return;
            }
        }
	}
	LogMsgQ("$detail_number items on $promo_name");
	LogMsgQ("Close $promotxnfile");
	close PROMOTXN;
    # If we are re-publishing an existing promotion, we do not need or want to enter it into the database.
    unless ($recreate_flag) {
        # update the status of the promotion
        my $query = "
        update
            promotions
        set status = 'crtd'
        where
            promoId = '$promoId'
        ";

        if ($dbh->do($query)) {
            LogMsgQ("Set promo $promoNum (id $promoId) status to created");
        } else {
            print "Failed to set promotion status in promotions table<br>";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
            $result_code=0;
            $dbh->disconnect;
            return;
        }
        $dbh->disconnect;
    }
	my $label="creating";
    if ($recreate_flag) {
		$label="re-creating";
        print $q->b("Finished re-creating promotion:");
    }
	$discountType=ucfirst($discountType);
	print "<pre>
	Finished creating promotion:
		> Internal Promo ID: $promoId
		> Promotion Number: $promoNum
		> Promotion Name: $promo_name
		> SKU count: $detail_number
		> Discount Type: $discountType
		> $discountType Off: $promoDiscount
		> Promo type: $type</pre>";

    $result_code=1;
    return $promoId;
}

sub create_mixmatch_by_sku {
    # Creating a bogo discount by SKU.  Here we create the  TSV file containing the SKUs to enable the MixMatch/BOGO for the selected SKUs
    my ($mixmatchId,$selection_selection,$my_selection,$uid) = @_;
	
    my @selection_specified;
    my $group_selection;
    my $department_selection;
    my @sku_list;
    my @ltm = localtime();
    my $date_label=sprintf("%02d%02d%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);   # For the ASC file
    my $date_label2=sprintf("%04d-%02d-%02d",  $ltm[5]+1900,$ltm[4]+1, $ltm[3]);    # for the DB
	# Create the mix match asc files
    my $type="add";

    LogMsgQ("Open $mixmatchSKUfile");
    unless (open(MMSKU,">$mixmatchSKUfile")) {
        print $q->p("Error opening $mixmatchSKUfile");
        LogMsgQ("Error opening $mixmatchSKUfile in create_mixmatch_by_sku function");
        StandardFooter();
        exit;
    }
	if ($testing_mode) {
		print "--Inside create_mixmatch_by_sku --<br />";
		print "MixMatch ID: $mixmatchId<br />";
		print "Selection: $selection_selection<br />";
		@selection_specified=@$my_selection;
		foreach my $s (@selection_specified) {
			if ($s) {
				print "Selected: $s<br />";
			}
		}
	}
    # Here we create a tsv file to send down to the store.  This TSV file will contain info on how the SKUs are being selected.
    if ($selection_selection eq "sku") {

        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tSKU\n";
        foreach my $sku (@selection_specified) {
            print MMSKU "$mixmatchId\t$sku\n";
        }

    } elsif ($selection_selection eq "group") {
        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tGROUP\n";
        foreach my $group (@selection_specified) {
            print MMSKU "$mixmatchId\t$group\n";
        }
    } elsif ($selection_selection eq "department") {
        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tDEPT\n";
        foreach my $dept (@selection_specified) {
            print MMSKU "$mixmatchId\t$dept\n";
        }
    } elsif ($selection_selection eq "status") {
        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tAKRONSTATUS\n";
        foreach my $status (@selection_specified) {
            print MMSKU "$mixmatchId\t$status\n";
        }
    } elsif ($selection_selection eq "vmcode") {
        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tVMCODE\n";
        foreach my $status (@selection_specified) {
            print MMSKU "$mixmatchId\t$status\n";
        }
    } elsif ($selection_selection eq "vendor") {
        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tVENDOR\n";
        foreach my $status (@selection_specified) {
            print MMSKU "$mixmatchId\t$status\n";
        }		
    } elsif ($selection_selection eq "seasonality") {
        @selection_specified=@$my_selection;
        print MMSKU "MIXMATCH\tSEASONALITY\n";
        foreach my $status (@selection_specified) {
            print MMSKU "$mixmatchId\t$status\n";
        }		
    } else {
        print $q->p("Error: $selection_selection is not supported");
        StandardFooter();
        exit;
    }
	LogMsgQ("Close $mixmatchSKUfile");
	close MMSKU;

    $result_code=1;
    return $result_code;
}

sub create_mixmatch_asc {
    # Create the MixMatch ASC file here:
    unless (open(MMASC,">$mixmatchASCfile")) {
        print $q->p("Error opening $mixmatchASCfile");
        LogMsgQ("Error opening $mixmatchASCfile in create_mixmatch_asc function");
        StandardFooter();
        exit;
    }
    print $q->p("Building ASC file for MIXMATCH");
	if ($testing_mode) {
		print "--testing mode--<br>";
	}
    # The MixMatch ASC file format:
    # 1 - id
    # 2 - qty
    # 7 - Description
    # 61 - start date
    # 62 - stop date
    # 63 - apply discount to Alt# (Essentially, use 2 if you do not wish to apply to items on promotion and 12 if you do)
    # 65 - Must complete Mix Match criteria
    # 70 - Discount Description

	# Connect to the database
	my $dbh;
	my $dsn;
	my $table = "mixmatch";
	unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
		LogMsgQ("DELMIXMATCH: Failed to connect to $mysqld and $posdb database");
		print "DELMIXMATCH: Failed to connect to $mysqld and $posdb database<br>";
		StandardFooter();
		exit;
	}
    # We need to add all of the bogo definitions to the ASC file since it will erase any from the Triversity database if we leave one out.
    $query="
        select
            mixmatchId,
            f2_alt1Qty as 1Qty,
			f3_allowGreater as AllowGreater,
			f66_alt1Discount as 1Discount,
			f8_alt2Qty as 2Qty,
			f11_alt2Discount as 2Discount,
			f19_alt3Qty as 3Qty,
			f22_alt3Discount as 3Discount,
			f25_alt4Qty as 4Qty,
            f28_alt4Discount as 4Discount,
            f7_mixmatchName as MixMatchName,
			f70_instanceName as DiscountName,
            f61_effectiveDate as effectiveDate,
            f62_expireDate as expireDate,
			f63_applyDisc,
			f78_RecalcPromo,
			f80_DiscountOnLower,
			creationDate

        from
            $table
		";

    my $sth=$dbh->prepare("$query");
    if ($sth->execute()) {
        while (my @tmp = $sth->fetchrow_array()) {
            my $mixmatchId=$tmp[0];
            my $f2_alt1Qty=$tmp[1];
			my $f3_allowGreater=$tmp[2];
			if ($f3_allowGreater eq "Y") {
				$f3_allowGreater = '%';
			}
			my $f66_alt1Discount=$tmp[3];
			my $f8_alt2Qty=$tmp[4];
			my $f11_alt2Discount=$tmp[5];
			my $f19_alt3Qty=$tmp[6];
			my $f22_alt3Discount=$tmp[7];
			my $f25_alt4Qty=$tmp[8];
			my $f28_alt4Discount=$tmp[9];
			my $f7_mixmatchName=$tmp[10];
			my $f70_instanceName=$tmp[11];

			my $effectiveDate=$tmp[12];
			my $tmp_date=$tmp[12];
			my @splitter=split(/\s+/,$tmp_date);
			$tmp_date=$splitter[0];
			@splitter=split(/\-/,$tmp_date);
			my $Y=substr($splitter[0],2,2);
			my $M=$splitter[1];
			my $D=$splitter[2];
			my $f61_effectiveDate="$Y"."/"."$M"."/"."$D";

			my $expireDate=$tmp[13];
			$tmp_date=$tmp[13];
			@splitter=split(/\s+/,$tmp_date);
			$tmp_date=$splitter[0];
			@splitter=split(/\-/,$tmp_date);
			$Y=substr($splitter[0],2,2);
			$M=$splitter[1];
			$D=$splitter[2];
			my $f62_expireDate="$Y"."/"."$M"."/"."$D";

			my $f63_applyDisc=$tmp[14];
			my $f78_RecalcPromo=$tmp[15];
			my $f80_DiscountOnLower=$tmp[16];
			my $creationDate=$tmp[17];

			my $f71_ReportGroupQty=1;
            my $apply_setting=12;    # Default of 12 is the "as defined" setting in the Configurator for the "Apply Disc to Alt." setting.  This is what we want in most cases.
            my $recalc_item_discount="Y"; # This was set to "Y" on 2014-10-27 as Item Discounts were causing problems.  Promos are not affected as this only item discounts - kdg
			my $recalc_promo_discount="N";  # I think that we want this set to N.  The result is that if an item is on promo, it will consider the promo price when determining the bogo discount.
			
            # The following code is not correct but is being left in case I re-use it later - kdg
            #if ($combineDiscount) {
            #    $apply_setting=12;
            #}
			print "Adding mixmatch: $mixmatchId  name: $f70_instanceName desc: $f7_mixmatchName effective: $effectiveDate expire: $expireDate<br />";
            print MMASC "$mixmatchId,$f2_alt1Qty,$f3_allowGreater,,,,$f7_mixmatchName,$f8_alt2Qty,,,$f11_alt2Discount,,,N,N,N,N,N,$f19_alt3Qty,,,$f22_alt3Discount,,,$f25_alt4Qty,,,$f28_alt4Discount,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,$f61_effectiveDate,$f62_expireDate,$f63_applyDisc,N,Y,$f66_alt1Discount,9999,99,,$f70_instanceName,$f71_ReportGroupQty,,,,0,0,,$f78_RecalcPromo,Y,$f80_DiscountOnLower,N,,,,,1,,,,,,,,,,,,,$recalc_item_discount,0\n";

			# Determine the discount amount and then update the history table which keeps a record of
			# the MixMatch discounts that have been defined.  This table is necessary in case a MixMatchId
			# ever gets re-used.
			my $discountAmt=0;
			if ($f66_alt1Discount) {
				$discountAmt=$f66_alt1Discount;
			}
			if ($f11_alt2Discount) {
				$discountAmt=$f11_alt2Discount;
			}
			if ($f22_alt3Discount) {
				$discountAmt=$f22_alt3Discount;
			}
			if ($f28_alt4Discount) {
				$discountAmt=$f28_alt4Discount;
			}
		 
			update_history_table("$mixmatchId","$discountAmt","$f70_instanceName","$f7_mixmatchName","$creationDate","$effectiveDate","$expireDate");
			 
        }
 
        print $q->p
    } else {
        print "ERROR with db query: create_mixmatch_by_sku ($query)<br>";
        StandardFooter();
        exit;
    }
	LogMsgQ("Close $mixmatchASCfile");

	close MMASC;
    print $q->p("Finished Building ASC file for MIXMATCH - $mixmatchASCfile");
	# Check for duplicates and show a button if there are any
	if (check_mixmatch_duplicates()) {
		print $q->h3("Error: Duplicate entry found in tblStoreMixMatch");
 
	} else {
	 
		# Build option to publish the ASC file
		print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
		print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
		print $q->input({-type=>"hidden", -name=>"report", -value=>"PUBMIXMATCHASC"});
		print $q->input({-type=>"hidden", -name=>"selection", -value=>"$mixmatchASCfile"});
		print $q->Tr($q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'right'}, "\&nbsp;"),
				   $q->td({-class=>'tabledatanb', -nowrap=>undef, -align=>'left'},
						  $q->submit({  -value=>"   Publish MixMatch Definition    "}))
				  );
		print $q->end_form(), "\n";
		print $q->end_table(),"\n";
		StandardFooter();
		exit;
	}
}

sub update_history_table {
	my ($mixmatchId,$discountAmt,$mixmatchName,$description,$creationDate,$effectiveDate,$expireDate,$utility) =@_;
	# Beware the possible confusion of the mixmatch name and the description.  The name is what would appear
	# on the receipt.  The description describes the discount such as Buy1Get1 or Buy2Get1.

	# Check to see if this definition is in the history table.  If it is not, add it.
 
	my $dbh;
	my $dsn;
	$dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	unless ( $dbh = openODBCDriver($dsn, '***', '****')) {
		LogMsgQ("update_history_table: Failed to connect to **** and **** database");
		print "update_history_table: Failed to connect to **** and **** database<br>";
		StandardFooter();
		exit;
	}

	# Query the mixmatch history table
	my $query="
		select
			mixmatchId,
			discountAmt,
			mixmatchName,
			description,
			creationDate,
			effectiveDate,
			expireDate
		from
			[dbo].[tblStoreMixmatch]
	";

	#print $q->pre("$query");
	my $entry_present=0;
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (my @tmp = $sth->fetchrow_array()) {
		my $id=$tmp[0];
		my $name=$tmp[2];
		my $effective=substr($tmp[5],0,19);
		my $expire=substr($tmp[6],0,19);
		 
		if ($utility) {
			# If this is being called from the utility function, then the check ignores the description & creationdate		 
			if (($id == $mixmatchId) &&
				($name eq "$mixmatchName") &&
				($effective eq $effectiveDate) &&
				($expire eq $expireDate)) {
				$entry_present=1;
			}
			if ($tmp[0] == $mixmatchId) {

			}
		} else { 
 
			if (($id == $mixmatchId) &&
				($name eq "$mixmatchName") &&
				($effective eq $effectiveDate) &&
				($expire eq $expireDate)) {
				$entry_present=1;
			}			
		}
	 
    } 		
	
	unless ($entry_present) {
 
		# Escape any single quotes
		$mixmatchName =~ s/'/''/g;
		$query="
		exec pspInsertMixMatch
			\@mixmatchId = $mixmatchId,
			\@discountAmt = $discountAmt,
			\@mixmatchName = '$mixmatchName',
			\@description = '$description',
			\@creationDate = '$creationDate',
			\@effectiveDate = '$effectiveDate',
			\@expireDate = '$expireDate'
		";
		if ($dbh->do($query)) {
			print "Updated tblStoreMixmatch table with mixmatchId $mixmatchId effectiveDate $effectiveDate <br>";
		} else {
			print "Failed to update tblStoreMixmatch table with mixmatchId $mixmatchId effectiveDate $effectiveDate<br>";
			print "query=".$query."<br>\n";
			print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
			print "errstr=".$dbh->errstr."<br>\n";
			StandardFooter();
			exit;
		} 
	}
}

sub update_stores {
    my $dbh;
    my $query;
    my $update_count=0;
    unless ($dbh=DBI->connect("DBI:mysql:host=$mysqld;database=$posdb","pos","sop")) {
        LogMsgQ("UPDATE_STORES: Failed to connect to $mysqld and $posdb database");
        print "UPDATE_STORES: Failed to connect to $mysqld and $posdb database<br>";
        StandardFooter();
        exit;
    }
    # First see what we have in the stores table
    my %store_table_hash;
    $query = "select * from stores";
    my $sth=$dbh->prepare("$query");
    $sth->execute();
    while (my @tmp = $sth->fetchrow_array()) {
        my @array=("$tmp[1]","$tmp[2]");
        $store_table_hash{$tmp[0]}=\@array;
    }

    # Second, see what we have in Navision
    my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
    my $mssql_dbh = openODBCDriver($dsn, '***', '****');

    unless ($mssql_dbh) {
        LogMsgQ("UPDATE_STORES: Failed to connect to **** and **** database");
        print "UPDATE_STORES: Failed to connect to **** and **** database<br>";
        StandardFooter();
        exit;
    }

    # Update Company and Contract
    my $Active=1;
    my %store_hash;
    foreach my $status ("Company","Contract") {
        $query = 'exec ****..tspGetStoreInfo @Code=\'V1\''. ($Active ? ', @Active=1' : '') if ($status eq "Company");
        $query = 'exec ****..tspGetStoreInfo @Code=\'V2\''. ($Active ? ', @Active=1' : '') if ($status eq "Contract");
        my $tbl_ref = execQuery_arrayref($mssql_dbh, $query);
        for my $datarows (1 .. $#$tbl_ref) {
            my $thisrow = @$tbl_ref[$datarows];
            my $storeId = trim($$thisrow[0]);
            my $storeName = trim($$thisrow[1]);
            my @tmp=split(/-/,$storeName);
            $storeName=$tmp[1];
            $store_hash{$storeId}=$storeName;
        }

        foreach my $storenumber (sort(keys(%store_hash))) {
            unless(defined($store_table_hash{$storenumber})  ) {
                # We have a store not currently in our stores table;

                #   we need to update the stores table.
                $query = "
                replace into
                    stores
                set
                    storeId = '$storenumber',
                    storeName = '$store_hash{$storenumber}',
                    status = '$status'
                ";

                if ($dbh->do($query)) {
                    print "Updated stores database with $storenumber $store_hash{$storenumber} $status<br>";
                    $update_count++;
                } else {
                    print "Failed to update stores database with $storenumber $store_hash{$storenumber} $status<br>";
                    print "query=".$query."<br>\n";
                    print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
                    print "errstr=".$dbh->errstr."<br>\n";
                    $result_code=0;

                }
            }
        }
    }
    $mssql_dbh->disconnect;
    print "Add warehouse<br>";
    # Add the warehouse
    my $ref=$store_table_hash{0};
    my @store_info=@$ref;
    unless (("AkronWarehouse" eq $store_info[0]) && ("Company" eq $store_info[1])) {
        my $query = "
        replace into
            stores
        set
            storeId = '0000',
            storeName = 'AkronWarehouse',
            status = 'Company'
        ";

        if ($dbh->do($query)) {
            print "Updated stores database with 0000 AkronWarehouse Company<br>";
            $update_count++;
        } else {
            print "Failed to update stores database with 0000 AkronWarehouse Company<br>";
            print "query=".$query."<br>\n";
            print "err=".$dbh->err."<br>\nstate: ".$dbh->state."<br>\n";
            print "errstr=".$dbh->errstr."<br>\n";
            $result_code=0;

        }
    }
    $dbh->disconnect;
    print "$update_count updates were made<br>";

}
sub StandardFooter {
    print "<br>\n";
    print "<br>\n";
    print $q->p("---------------------------------------------------------------------------");
    print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    # print $q->start_table({-class=>'table', -valign=>'top', -cellspacing=>0}), "\n";
    print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"MAIN"});
    print $q->submit({-accesskey=>'G', -value=>"   Main Menu   "});
    print $q->end_form(), "\n";
    #print $q->end_table(),"\n";

=pod
    print $q->start_form({-action=>"$scriptname", -method=>'post'}), "\n";
    print $q->input({-type=>"hidden", -name=>"report", -value=>"DEFAULT"});
    print $q->submit({ -value=>"   Sign Out   "});
    print $q->end_form(), "\n";
    print $q->end_table(),"\n";
=cut	
   # print $q->a({-href=>"$scriptname", -target=>'_top'}, "Sign Out");
}

sub get_store_info {
	# Get the store names

	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh = openODBCDriver($dsn, 'rpt', 'rpt');
	my $store_options= $q->option({-value=>'V1'}, "All Company Stores");
	#$query = "select storeId,storeName from stores order by storeId";
	$query = "
	SELECT [StoreId]
	  ,[StoreName]
	  ,[StoreCode]

	FROM [****].[dbo].[rtblStores]
	where EndDate is  NULL";

	my $tbl_ref_alias = execQuery_arrayref($dbh, $query);
	for my $datarows (1 .. $#$tbl_ref_alias) {
		my $thisrow = @$tbl_ref_alias[$datarows];
		my $number = trim($$thisrow[0]);
		my $name = trim($$thisrow[1]);
		my $code = trim($$thisrow[2]);

		if ($code eq "V1") {
			$store_status_hash{$number}="Company";
			$storename_hash{$number}=$name;
		}
		if ($code eq "V2") {
			$store_status_hash{$number}="Contract";
			$storename_hash{$number}=$name;
		}
	}
	$dbh->disconnect;

}

sub check_mixmatch_duplicates {
	my $dsn = "driver={SQL Server};Server=****;database=****;uid=****;pwd=****;";
	my $dbh;
	unless ( $dbh = openODBCDriver($dsn, '***', '****')) {
		LogMsgQ("check_mixmatch_duplicates: Failed to connect to **** and **** database");
		print "check_mixmatch_duplicates: Failed to connect to **** and **** database<br>";
		StandardFooter();
		exit;
	}
	my %mix_match_hash=();
	# Query the mixmatch history table
	my $query="
		select
			mixmatchId,
			effectiveDate,
			expireDate
		from
			[dbo].[tblStoreMixmatch]
	";

	my $sth=$dbh->prepare("$query");
	$sth->execute();
	while (my @tmp = $sth->fetchrow_array()) {
		my $id=$tmp[0];
		my $effective=$tmp[1];
		my $expire=$tmp[2];
		$mix_match_hash{$id}="$effective"."|"."$expire";
	}
	# Iterate through the list of mixmatches looking for inconsistencies
	my $duplicate_count=0;
	foreach my $mmid (sort(keys(%mix_match_hash))) {

		my $effective=$mix_match_hash{$mmid};
		my @tmp=split(/\|/,$effective);
		$effective=substr($tmp[0],0,19);
		my $expire=substr($tmp[1],0,19);
		$query="

			select
				[creationDate]
				,[mixmatchId]
				,[discountAmt]
				,[mixmatchName]
				,[description]
				
				,[effectiveDate]
				,[expireDate]
			from
				[dbo].[tblStoreMixmatch]
			where
				mixmatchId = $mmid
			and
				((((effectiveDate > '$effective'
				and
				effectiveDate < '$expire')
				or
				(expireDate > '$effective'
				and
				expireDate < '$expire')))
			or
				((effectiveDate = '$effective')
				and
				(expireDate = '$expire')))

		";
		###
		my $tbl_ref = execQuery_arrayref($dbh, $query);

		if (recordCount($tbl_ref) > 1) {

			print $q->h3("MixMatch Definition History Duplicates");
	 
			
			my @links = ( "$scriptname?report=DELMIXMATCHRECORD&creationDate=".'$_' );
							
			printResult_arrayref2($tbl_ref, \@links);
			print $q->h3("Select the effective date of the entry you wish to delete.");
			$duplicate_count++;

		}
		###
	}
	$dbh->disconnect;	
	return $duplicate_count;
}
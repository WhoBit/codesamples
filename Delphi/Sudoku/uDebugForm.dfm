object frmDebug: TfrmDebug
  Left = 0
  Top = 0
  Caption = 'Debug'
  ClientHeight = 806
  ClientWidth = 785
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object memoDebug: TMemo
    Left = 8
    Top = 8
    Width = 373
    Height = 790
    TabOrder = 0
  end
  object btnReturn: TButton
    Left = 440
    Top = 432
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 1
    OnClick = btnReturnClick
  end
  object btnDebugPossibleValues: TButton
    Left = 440
    Top = 362
    Width = 121
    Height = 25
    Caption = 'Show PossibleValues'
    TabOrder = 2
    OnClick = btnDebugPossibleValuesClick
  end
  object rgDebug: TRadioGroup
    Left = 440
    Top = 32
    Width = 225
    Height = 265
    Caption = 'Debug Options'
    Items.Strings = (
      'Reset Possible Values'
      'Recalculate By Value By Row'
      'Recalculate By Value By Column'
      'Recalculate By Value By Sector'
      'Recalculate By Count By Row'
      'Recalculate By Count By Column'
      'Recalculate By Count By Sector'
      'Recalculate By Two Possible By Row'
      'Recalculate By Two Possible By Column'
      'Recalculate By Two Possible By Sector')
    TabOrder = 3
  end
  object btnDebugExecute: TButton
    Left = 680
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Execute'
    TabOrder = 4
    OnClick = btnDebugExecuteClick
  end
  object btnClear: TButton
    Left = 440
    Top = 393
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 5
    OnClick = btnClearClick
  end
  object rgShowPossibleOptions: TRadioGroup
    Left = 576
    Top = 320
    Width = 185
    Height = 67
    Caption = 'Show Possible Values Options'
    ItemIndex = 0
    Items.Strings = (
      'By Row'
      'By Sector')
    TabOrder = 6
  end
end

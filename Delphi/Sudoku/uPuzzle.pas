unit uPuzzle;

interface

uses System.SysUtils, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
      Vcl.Menus, Vcl.ExtCtrls,
      Winapi.Windows, Winapi.Messages,  System.Variants, System.Classes, Vcl.Graphics,
      uTypes, uCell, uSolvePuzzle, uRecalculate, uPuzzleInterface, uSolveInterface,
      uRecalculateInterface, uLibrary, uConstants;

type
  TPuzzle = class(TInterfacedObject, IPuzzle)
    fButtonGrid : TButtonGrid;
    fRowArray : TArrayOfCells;
    fBackupRowArray : TArrayOfStr;
    fColumnArray : TArrayOfCells;
    fSectorArray : TArrayOfCells;
    fGuessCell : TSudokuCell;
    fGuessPV : TValuesSet;
    fGuessCounter : Integer;

    procedure SolvePuzzle;

    function Solve : Integer;

    function Recalculate(aOption : T_RCS = A) : Integer;

    function GetButtonGrid : TButtonGrid;
    procedure SetButtonGrid(aButtonGrid : TButtonGrid);

    function GetRowArray : TArrayOfCells;
    procedure SetRowArray(anArray : TArrayOfCells);

    function GetColumnArray : TArrayOfCells;
    procedure SetColumnArray(anArray : TArrayOfCells);

    function GetSectorArray : TArrayOfCells;
    procedure SetSectorArray(anArray : TArrayOfCells);

    function CheckPuzzle : Boolean;
    procedure ClearPuzzle;
    procedure ResetPuzzle;

    procedure ResetCaptions(aPuzzle : TPuzzle);

    function FindGuessCell(aPuzzle : TPuzzle) : TSudokuCell;
    function SearchForGuessCandidate(anArray : TArrayOfCells) : TSudokuCell;

    procedure TryGuess;

    function CopyArray(anArray : TArrayOfCells) : TArrayOfStr;
    procedure RestoreFromArray(aRArray : TArrayOfStr);

  public
    constructor Create;
    property ButtonGrid : TButtonGrid read fButtonGrid write fButtonGrid;
    property RowArray : TArrayOfCells read fRowArray write fRowArray;
    property ColumnArray : TArrayOfCells read fColumnArray write fColumnArray;
    property SectorArray : TArrayOfCells read fSectorArray write fSectorArray;
    procedure ResetPossibleValues;
  end;

implementation

uses
  uMainForm;

constructor TPuzzle.Create;

begin
  fGuessCounter := 0;

end;

// Solve the complete puzzle

procedure TPuzzle.SolvePuzzle;
var
  MyRecalculateResult : Integer;
  MySolvePuzzleResult : Integer;
  MyResultCount : Integer;
  MyCounter,MyLimit : Integer;
  ContinueLoop : Boolean;

  myPause : Boolean;
  MySolveLevel : Integer;
begin
  MySolveLevel := frmMain.rgSolveOptions.ItemIndex;
  ContinueLoop := True;
  MyCounter := 0;
  MyLimit := 81;

  myPause := False;

  while ContinueLoop do
  begin
    // Recalculate
    MyRecalculateResult := Recalculate;
    // Solve
    MySolvePuzzleResult := Solve;

    if (MySolvePuzzleResult < 0) then
    begin
      Show('Stop Loop - Puzzle is wrong');
      Show(Format('MyGuessCounter: %d',[Self.fGuessCounter]));
    end ;
    if ((MySolvePuzzleResult < 1) AND (MyRecalculateResult = 0)) then
    begin
      if (Self.fGuessCounter = 1) then
      begin
        Inc(Self.fGuessCounter);
        Show('Restoring Button Grid...');
        ClearPuzzle;
        RestoreFromArray(Self.fBackupRowArray);
        ResetCaptions(Self);
        ResetPossibleValues;
        MyRecalculateResult := Recalculate;
        TryGuess;
        MySolvePuzzleResult := 1;
        MyCounter := 0;
      end;
    end;
    Inc(MyCounter);
    if MyCounter > MyLimit then
    begin
      ContinueLoop := False;
      Show(Format('Stop Loop - Counter: %d',[MyCounter]));
    end;

    if MyRecalculateResult + MySolvePuzzleResult = 0 then
    begin
      Inc(Self.fGuessCounter);
      if Self.fGuessCounter > 1 then
      begin
        ContinueLoop := False;
        Show(Format('Stop Loop - MyRecalculateResult: %d',[MyRecalculateResult]));
        Show(Format('Stop Loop - MySolvePuzzleResult: %d',[MySolvePuzzleResult]));
      end else
      begin
        if Self.fGuessCounter = 1 then
        begin
          { It is time to "guess"...}
//          ShowMessage('Time to Guess');
          Self.fGuessCell := FindGuessCell(Self);
          Self.fGuessPV := Self.fGuessCell.fPossibleValues;
          Self.fBackupRowArray := CopyArray(RowArray);
          TryGuess;
          Inc(MySolvePuzzleResult);
          myPause := True;
        end;
      end;
    end;
    Show(Format('Guess Count: %d ',[Self.fGuessCounter]));
    if MySolveLevel = 0 then
    begin
      // Only solve one cell and then stop
      ContinueLoop := False;
      Break;
    end;

  end;

  if MyRecalculateResult + MySolvePuzzleResult = 0 then
  begin
    if (CheckPuzzle) then
    begin
      ShowMessage('Puzzle has been solved');
    end else
    begin
      ShowMessage('Puzzle has not been solved');
    end;
  end;
end;

function TPuzzle.Recalculate(aOption : T_RCS = A) : Integer;
var
  MyRecalculate : IRecalculate;
  MyRecalculateResult : Integer;
  MyCounter : Integer;
  MyResultCount : Integer;
begin
  MyRecalculateResult := 1;
  MyCounter := 0;
  MyResultCount := 0;
  // Create the object
  MyRecalculate := TRecalculate.Create;
  // Assign the fields
  MyRecalculate.RowArray := GetRowArray;
  MyRecalculate.ColumnArray := GetColumnArray;
  MyRecalculate.SectorArray := GetSectorArray;

  while (MyRecalculateResult > 0) AND (MyCounter < 5) do
  begin
    MyRecalculateResult := MyRecalculate.ReCalculatePossibleValues(aOption);
    Inc(MyCounter);
    Show(Format('Recalculation Result: %d - MyCounter: %d',[MyRecalculateResult,MyCounter]));
    Inc(MyResultCount,MyRecalculateResult);
  end;

  // Refresh the display of possible values if necessary
  if DisplayPV then
  begin
    frmMain.DisplayPossibleValues(1);
  end;

  Result := MyResultCount;

end;

// Solve

function TPuzzle.Solve : Integer;
var
  MySolvePuzzle : ISolvePuzzle;
  MySolvePuzzleResult : Integer;
  MySolveLevel : Integer;
  MyCounter : Integer;
  MyResultCount : Integer;
begin
  MySolveLevel := frmMain.rgSolveOptions.ItemIndex;
  MySolvePuzzleResult := 1;
  MyCounter := 0;
  MyResultCount := 0;

  // Create the object
  MySolvePuzzle := TSolvePuzzle.Create;
  MySolvePuzzleResult := MySolvePuzzle.SolveOneCell(RowArray,ButtonGrid);
  // Check that the puzzle is still correct
  if not (MySolvePuzzle.CheckPuzzleCorrect(RowArray)) then
  begin
    ShowMessage('ERROR: Puzzle is not correct!');
    Result := -1;
    exit;
  end;
  Inc(MyCounter);
  Show(Format('SolvePuzzle Result: %d - MyCounter: %d',[MySolvePuzzleResult,MyCounter]));
  Inc(MyResultCount,MySolvePuzzleResult);
  Result := MyResultCount;
end;

function TPuzzle.CheckPuzzle: Boolean;
var
  MySolvePuzzle : ISolvePuzzle;
begin
  // Create the object
  MySolvePuzzle := TSolvePuzzle.Create;
  Result := (MySolvePuzzle.CheckPuzzleCorrect(RowArray) AND
  MySolvePuzzle.CheckPuzzleCorrect(ColumnArray) AND
  MySolvePuzzle.CheckPuzzleCorrect(SectorArray) AND
  MySolvePuzzle.CheckPuzzleComplete(RowArray)
  );
end;

function TPuzzle.GetButtonGrid;
begin
  Result := fButtonGrid;
end;

procedure TPuzzle.SetButtonGrid(aButtonGrid: TButtonGrid);
begin
  fButtonGrid := aButtonGrid;
end;

function TPuzzle.GetRowArray;
begin
  Result := fRowArray;
end;

procedure TPuzzle.SetRowArray(anArray: TArrayOfCells);
begin
  fRowArray := anArray;
end;

function TPuzzle.GetColumnArray;
begin
  Result := fColumnArray;
end;

procedure TPuzzle.SetColumnArray(anArray: TArrayOfCells);
begin
  fColumnArray := anArray;
end;

function TPuzzle.GetSectorArray;
begin
  Result := fSectorArray;
end;

procedure TPuzzle.SetSectorArray(anArray: TArrayOfCells);
begin
  fSectorArray := anArray;
end;

function TPuzzle.FindGuessCell(aPuzzle : TPuzzle) : TSudokuCell;
{ Some hard puzzles cannot be solved by the method of reducing possible
  values.  If the puzzle has a row/column/sector where only two cells
  remain and each share the same pair of two possible values, this
  function tries one of those.  If the puzzle is not solved, it restores
  the puzzle to that point of guessing and tries the other possible value.
  This function finds a cell suitable for guessing a solution for and returns it. }
var
  SolutionsFound : Integer;

  GuessCell : TSudokuCell;
begin
  SolutionsFound := 0;
  GuessCell := nil;
  Show('Searching for Guess Candidate by Row');
  GuessCell := SearchForGuessCandidate(aPuzzle.fRowArray);
  if (GuessCell = nil) then
  begin
    Show('Searching for Guess Candidate by Column');
    GuessCell := SearchForGuessCandidate(aPuzzle.fColumnArray);
  end;
  if (GuessCell = nil) then
  begin
    Show('Searching for Guess Candidate by Sector');
    GuessCell := SearchForGuessCandidate(aPuzzle.fSectorArray);
  end;
  if (GuessCell = nil) then
  begin
    Show('Failed to find a suitable cell with two possible to guess with.');
  end;

  Result := GuessCell;
end;

function TPuzzle.SearchForGuessCandidate(anArray : TArrayOfCells) : TSudokuCell;
var
  I,J : Integer;
  PV,PossibleValues : TValuesSet;
  PVCount : Integer;
  GuessCell : TSudokuCell;
  GuessPV : TValuesSet;

begin
  GuessCell := nil;
  { Save the current state of the puzzle to a global variable. }

  for I := sStart to sFinish do    // Outer Loop
  begin
    PV := [];
    PVCount := 0;
    for J := sStart to sFinish do    // Inner Loop
    begin
      { Search for a cell with only two possible values }
      PossibleValues := anArray[I,J].fPossibleValues;
      if (CountInSet(PossibleValues) = 2) then
      begin
        { Assume this will be a suitable Guess Cell. }
        GuessCell := anArray[I,J];
        GuessPV := PossibleValues;
        if (PV = []) then
        begin
          PV := PossibleValues;
        end
        else if (PossibleValues <> PV) then
        begin
          { If we find another cell with only two possible values but
            they are different possible values, we give up on this
            inner loop. }
          GuessCell := nil;
          GuessPV := [];
          { Should break out to the next outer loop. }
          break;
        end;
        Show('Guess Cell''s PV: ' + PVSetToString(PV));
      end;    // End of checking PV = 2
    end;   // End of inner loop
    { If we do have a Guess Cell, we can return. }
    if (GuessCell <> nil) then
    begin
      break;
    end;
  end;  // End of outer loop
  Result := GuessCell;

end;


procedure TPuzzle.TryGuess;
var
  PV : TValuesSet;
  P : AnsiChar;
  SolutionsFound : Integer;
  PVCounter : Integer;

begin
  SolutionsFound := 0;
  PVCounter := 0;

  { Get the current possible values of the guess cell. }
  PV := Self.fGuessPV;

  for P in PV do
  begin
    Inc(PVCounter);
    if (PVCounter = Self.fGuessCounter) then
    begin
      ButtonGrid[Self.fGuessCell.fRow.ToInteger,Self.fGuessCell.fColumn.ToInteger].Font.Size:=DefaultFontSize;
      ButtonGrid[Self.fGuessCell.fRow.ToInteger,Self.fGuessCell.fColumn.ToInteger].Caption := '*' + P + '*';
      MyRowArray[Self.fGuessCell.fRow.ToInteger,Self.fGuessCell.fColumn.ToInteger].fValue := P;
      MyRowArray[Self.fGuessCell.fRow.ToInteger,Self.fGuessCell.fColumn.ToInteger].fNumOfPossibleValues := 0;
      MyRowArray[Self.fGuessCell.fRow.ToInteger,Self.fGuessCell.fColumn.ToInteger].fPossibleValues := [];
      break;
    end;
  end;
end;

function TPuzzle.CopyArray(anArray : TArrayOfCells) : TArrayOfStr;
var
  I,J : Integer;
  MyNewArray : TArrayOfStr;
begin
  for I := sStart to sFinish do
  begin
    for J := sStart to sFinish do
    begin
      MyNewArray[I,J] := (anArray[I,J].GetValue);
    end;
  end;
  Result := MyNewArray;
end;

procedure TPuzzle.RestoreFromArray(aRArray : TArrayOfStr);
var
  I,J : Integer;
begin
  for I := sStart to sFinish do
  begin
    for J := sStart to sFinish do
    begin
      RowArray[I,J].SetValue(aRArray[I,J]);
    end;
  end;
end;

procedure TPuzzle.ClearPuzzle;
var
  I,J : Integer;
begin
  { Clear the existing puzzle and create a new one }
  for I:= sStart to sFinish do  //Row
  begin
    for J:=sStart to sFinish do //Column
    begin
      RowArray[I,J].ClearCell;
      ButtonGrid[I,J].Caption := '';
    end;
  end;
end;

procedure TPuzzle.ResetPuzzle;
  { Reset some fields in the puzzle object.  This is necessary so that another
    puzzle can be loaded and solved. }
begin
  fGuessCell := nil;
  fGuessPV := [];
  fGuessCounter := 0;
end;

procedure TPuzzle.ResetCaptions(aPuzzle : TPuzzle);
var
  I,J : Integer;
begin
  Show('Resetting Captions...');
  for I := sStart to sFinish do
  begin
    for J := sStart to sFinish do
    begin
      aPuzzle.fButtonGrid[I,J].Caption := aPuzzle.fRowArray[I,J].fValue;
    end;
  end;

end;


procedure TPuzzle.ResetPossibleValues;
var
  I,J : Integer;
begin
  { For all buttons/cells with no value yet, set the possible values back to default }
  for I:= sStart to sFinish do  //Row
  begin
    for J:=sStart to sFinish do //Column
    begin
      if RowArray[I,J].GetValue = '' then
      begin
        RowArray[I,J].SetPossibleValues(OriginalPossibleValues);
      end;
    end;
  end;
end;

end.

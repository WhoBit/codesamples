unit uMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uCell, uValueEntryForm, System.UITypes,
  uTypes, Vcl.Menus, Vcl.ExtCtrls, uSolvePuzzle, uTypesHighLevel, uPuzzle,
  uPuzzleInterface,  uLibrary, uConstants;

type
  TfrmMain = class(TForm)
    btnClose: TButton;
    MainMenu: TMainMenu;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    File1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    btnGo: TButton;
    rgSolveOptions: TRadioGroup;
    btnShowDebug: TButton;
    btnClearPuzzle: TButton;
    cbDisplayPossibleValues: TCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OpenClick(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure SaveAsClick(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure btnShowDebugClick(Sender: TObject);

    procedure btnClearPuzzleClick(Sender: TObject);
//    procedure DisplayPossibleValues;
    procedure DisplayPossibleValues(anOption: Integer);
    procedure cbDisplayPossibleValuesClick(Sender: TObject);

  private
    { Private declarations }
    procedure NameCell;
  public
    { Public declarations }
    procedure btnOneClick(Sender: TObject);
    procedure ShowPossibleValues(anOption : Integer);

  end;

var
  frmMain: TfrmMain;
  MyButtonGrid: TButtonGrid;
  MySudokuPuzzle : IPuzzle;
  MyBackupPuzzle : IPuzzle;

  MyRowArray : TArrayOfCells;
  MyColumnArray : TArrayOfCells;
  MySectorArray : TArrayOfCells;
  DisplayPV : Boolean;

implementation

{$R *.dfm}

uses
  uDebugForm ;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
   I,J: Integer;
begin
  for I:= sStart to sFinish do  //Row
    for J:=sStart to sFinish do //Column
      MyRowArray[I,J].Free;

//  MySudokuPuzzle.Free;
end;


procedure TfrmMain.FormCreate(Sender: TObject);
var
   I,J: Integer;
   iStr, jStr: String;
   ButtonSize,RowOffset,ColumnOffset,XtraSpace : Integer;
begin
  // Set some default values
  DisplayPV := False;
  // First we create a Puzzle object
  MySudokuPuzzle := TPuzzle.Create;

  // Second, we create a grid of TButtons

  // Set some variables
  ButtonSize := 50;
  RowOffset := 50;
  XtraSpace := 4;

  // Initialize the buttons that make up the puzzle itself
  for I:= sStart to sFinish do  //Row
  begin
    if (I = 4) OR (I = 7) then
      Inc(RowOffset,XtraSpace);
    for J:=sStart to sFinish do //Column
    begin
      iStr := IntToStr(I);
      jStr := IntToStr(J);
      ColumnOffset := 50;
      if (J > 3)  then
        ColumnOffset := ColumnOffset + XtraSpace;
      if (J > 6)  then
        ColumnOffset := ColumnOffset + XtraSpace;

      // Create the button.  The button is the visual representation
      // of the cell but they are two different arrays.
      MyButtonGrid[I,J]:=TButton.Create(Self);

      // Set the properties of the cell (TButton)
      With MyButtonGrid[I,J] do
      begin
        Left := (J * ButtonSize) + ColumnOffset ;
        Top := (I * ButtonSize) + RowOffset;
        Caption:=' ';
        Height:=ButtonSize;
        Width:=ButtonSize;
        Visible:=true;
        Parent:=self;
        Font.Size:=DefaultFontSize;
        Font.Style:=Font.Style+[fsBold];
        Name:= 'Cell' + iStr + jStr;
        Hint:= 'Click cell to assign a value.';
        ShowHint:= True;
        OnClick := btnOneClick;
      end;

      // Create the cell object array.  The second array is just a copy
      // of the first.  It allows use to look at the array of cells by column.
      MyRowArray[I,J] := TSudokuCell.Create(iStr,jStr);
      MyColumnArray[J,I] := MyRowArray[I,J];
    end;
  end;

  // And now we update our puzzle object with some of these things
  // we have just built up.
  MySudokuPuzzle.ButtonGrid := MyButtonGrid;
  MySudokuPuzzle.RowArray := MyRowArray;
  MySudokuPuzzle.ColumnArray := MyColumnArray;

  // We need to run the Name function.  This is where the SectorArray
  // is built and that is critical.
  NameCell;
end;



procedure TfrmMain.btnGoClick(Sender: TObject);
begin
  MySudokuPuzzle.SolvePuzzle;
end;

procedure TfrmMain.btnOneClick(Sender: TObject);
var
  CellValue : String;
  CellName : String;
  CurrentValue : String;

  MyPossibleValues : TValuesSet;
  ValidValue : Boolean;
  I : Integer;
  J : Integer;
begin
  ValidValue := False;
  // Get the cell name from the button
  CellName := (Sender as TButton).Name;
  // From the cell name determine the position in the RowArray and thus
  // the cell object.  Once we know the object, get some field info.
  I := StrToInt(CellName[5]);
  J := StrToInt(CellName[6]);

  MyPossibleValues :=  MyRowArray[I,J].fPossibleValues;
  CurrentValue := MyRowArray[I,J].fValue;

  // Ask the user for the value using the Value Entry form

  if dlgCellValue.Execute then
  begin
    // Get the entered value
    CellValue := dlgCellValue.edtCellValue.Text ;
  end;

  // Evaluate the CellValue returned

  // CellValue is a String.  We take the value at index 1 and that is a Char
  // which allows us to use the CharInSet to see if it is in the set of
  // possible values.

  if (CellValue <> '') AND (CharInSet(CellValue[1], MyPossibleValues)) then
    ValidValue := True;

  { A Value of 0 can be entered to clear the current value }
  if (CellValue = '0') then
  begin
    ValidValue := True;
    CellValue := '';
  end;

  if (ValidValue) then
    begin
      { First set the caption of the button }
      (Sender as TButton).Font.Size:=DefaultFontSize;
      (Sender as TButton).Caption := CellValue;
      { Then set the Value in the cell object }
      MyRowArray[I,J].fValue := CellValue;
    end

  else
    if (CellValue <> '') then
    begin
      ShowMessage('Sorry but ' + CellValue + '  cannot be used here.');
    end else
    begin
      ShowMessage('Please enter a number from 0 to 9.');
    end;


end;

  procedure TfrmMain.btnShowDebugClick(Sender: TObject);
begin
  frmDebug.Show;
end;

procedure TfrmMain.cbDisplayPossibleValuesClick(Sender: TObject);
begin
  if cbDisplayPossibleValues.Checked then
  begin
    DisplayPV := True;  // Display Possible Values for each Cell
    DisplayPossibleValues(1);
  end else
  begin
    DisplayPV := False;  // Display Possible Values for each Cell
    DisplayPossibleValues(0);
  end;

end;

procedure TfrmMain.DisplayPossibleValues(anOption: Integer);
var
  I,J : Integer;
  PV : TValuesSet;
  MyCaption : String;
  P : AnsiChar;
  V : Integer;
  Vstr : String;
begin
  { Display the possible values for each button/cell that does not yet have
    a value. }
  for I := sStart to sFinish do    // For each Row
  begin
    for J := sStart to sFinish do   // For each Column
    begin

      if (MyRowArray[I,J].GetValue = '') then
      begin
        MyCaption := '';
        if (anOption = 1) then
        begin
          PV := MyRowArray[I,J].GetPossibleValues;
          for P in PV do
          begin
            V := Integer(P);
            Vstr := Chr(V);
            MyCaption := MyCaption + Vstr;
          end;
          MyButtonGrid[I,J].Font.Size := SmallFontSize;
        end;
        MyButtonGrid[I,J].Caption := MyCaption;
      end;
    end;
  end;

end;

// Utility Functions
procedure TfrmMain.NameCell;
var
  I,J : Integer;
  S,N : String;
  SCounter,SectNum : Integer;

begin
  SCounter := 0;
  for SectNum := sStart to sFinish do // Iterate through Sectors
  begin
    for I := sStart to sFinish do     // Iterate through Rows
    begin
      for J := sStart to sFinish do   // Iterate through Columns
      begin
        S := MyRowArray[I,J].fSector;
        if (S.ToInteger = SectNum) then
        begin
          Inc(SCounter);
          if (SCounter > 9) then
          begin
            SCounter :=1; // When the counter reaches 9, we reset it.
                          // There will always be 9 cells in each sector.
          end;
          N := 'S' + S + SCounter.ToString;
          MyRowArray[I,J].Name := N;
          MySectorArray[S.ToInteger,Scounter] := MyRowArray[I,J];
        end;

        if (S = '') then
        begin
          ShowMessage(Format('Error naming cell in row %d and column %d.',[I,J]));
        end;
      end;
    end;
  end;
  MySudokuPuzzle.SectorArray := MySectorArray;
end;

  // Main Menu
procedure TfrmMain.OpenClick(Sender: TObject);
var
  FileStrings: TStringList;
  FileLine : String;
  I,J,N : Integer;
  CellValue,CellName : String;

begin
//  uLibrary.Show('Open file procedure.');
  { execute an open file dialog }
  if OpenDialog.Execute then
//    uLibrary.Show('Check if file exists...');
    { first check if file exists }
    if FileExists(OpenDialog.FileName) then
    begin
      { if it exists, load the data into the memo box }
//        uLibrary.Show('Open file create FileStrings...');
      FileStrings := TStringList.Create;
      try
        FileStrings.LoadFromFile(OpenDialog.FileName);
//          uLibrary.Show('Iterate through filestring...');
        for N := 0 to FileStrings.Count - 1 do
        begin
          if (FileStrings[N].Length > 9) then
          begin
//            uLibrary.Show(Format('Index: %d',[N]));
            FileLine := FileStrings[N];
            CellName := Copy(FileLine, 1, 6);
            I := StrToInt(FileLine[5]);
            J := StrToInt(FileLine[6]);
            CellValue := Copy(FileLine, 10, 1);
            // This is the number we see
            MyButtonGrid[I,J].Caption := CellValue;
            frmDebug.memoDebug.Lines.Add(Format('Button Row: %d - Column %d - Value %s',[I,J,CellValue]));
            // But this is the number the program uses
            MyRowArray[I,J].fValue := CellValue;
            // "Set" the value of fSector for the cell object.
            MyRowArray[I,J].DetermineSector(I.ToString, J.ToString);
          end;
        end;
      finally
//        uLibrary.Show('Free the filestring');
        FileStrings.Free;
      end
    end
    else
      { otherwise raise an exception }
      raise Exception.Create('File does not exist.');
end;

procedure TfrmMain.SaveClick(Sender: TObject);
var
  I,J : Integer;
  CellValue,CellName : String;
  FileLine : String;
begin
  { execute a save file dialog }
  if SaveDialog.Execute then
    { first check if file exists }
    if FileExists(SaveDialog.FileName) then
      { if it exists, raise an exception }
      raise Exception.Create('File already exists. Cannot overwrite.')
    else
    begin
      { otherwise, save the puzzle into the file }
      for I:= sStart to sFinish do
        for J:= sStart to sFinish do
          begin
            CellName := MyButtonGrid[I,J].Name;
            CellValue := MyRowArray[I,J].fValue;
            //WriteLn(MyFile, CellName + ' = ' + CellValue);
            FileLine := CellName + ' = ' + CellValue;
           // FileLine.SaveToFile(SaveDialog.FileName);
          end;
    end;
end;

procedure TfrmMain.SaveAsClick(Sender: TObject);
var
  I,J : Integer;
  CellValue,CellName : String;
  FileLine : String;
  FileStrings: TStringList;
begin
  SaveDialog.Title := 'Save As';
  if SaveDialog.Execute then
  begin
    FileStrings := TStringList.Create;
    { save the puzzle into the file }
    for I:= sStart to sFinish do
      for J:= sStart to sFinish do
        begin
          CellName := MyButtonGrid[I,J].Name;
          CellValue := MyRowArray[I,J].fValue;
          //WriteLn(MyFile, CellName + ' = ' + CellValue);
          FileLine := CellName + ' = ' + CellValue;
          { Add to the StringList }
          FileStrings.Add(FileLine);
//          FileLine.SaveToFile(SaveDialog.FileName);
        end;

    FileStrings.SaveToFile(SaveDialog.FileName);
    FileStrings.Free;
  end;
end;

procedure TfrmMain.btnClearPuzzleClick(Sender: TObject);
begin
  MySudokuPuzzle.ClearPuzzle;
  MySudokuPuzzle.ResetPuzzle;
end;



procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  Close;
end;



procedure TfrmMain.ShowPossibleValues(anOption : Integer);
var
  I,J : Integer;
  PV : String;
  anArray : TArrayOfCells;
  Label_1, Label_2 : String;
begin
  Label_1 := 'Row';
  Label_2 := 'Column';
  case anOption of
    0 : anArray := MyRowArray;
    1 : begin
          anArray := MyColumnArray;
          Label_1 := 'Column';
          Label_2 := 'Row';
        end;
  end;
  for I:= sStart to sFinish do
    for J:= sStart to sFinish do
      begin
        if CountInSet(anArray[I,J].GetPossibleValues) > 0 then
        begin
          PV := PVSetToString(anArray[I,J].GetPossibleValues);
          frmDebug.memoDebug.Lines.Add(Format('Button %s: %d - %s %d - Possible Values %s',[Label_1,I,Label_2,J,PV]));
        end;
      end;
end;

end.

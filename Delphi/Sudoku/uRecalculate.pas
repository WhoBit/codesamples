unit uRecalculate;

interface

uses
    System.SysUtils,
// System.Variants, System.Classes,
    Vcl.Dialogs,
    uRecalculateInterface, uCell, uTypes, uLibrary;


type
  TRecalculate = class(TInterfacedObject, IRecalculate)
    fRowArray : TArrayOfCells;
    fColumnArray : TArrayOfCells;
    fSectorArray : TArrayOfCells;
    function ReCalculatePossibleValues(aOption : T_RCS = A) : Integer;
    function ReCalculateByValues(aOption : T_RCS = A) : Integer;
    function ReCalculateByCount(aOption : T_RCS = A) : Integer;
    function ReCalculateByTwoPossible(aOption : T_RCS = A) : Integer;
    function ReCalcByValues(anArray : TArrayOfCells) : Integer;
    function ReCalcByCount(anArray : TArrayOfCells) : Integer;
    function ReCalcByTwoPossible(anArray : TArrayOfCells) : Integer;
    function FindIdenticalTwo(anArray : TValuesSetArray; aSet : TValuesSet) : TArrayOfTwo;
    function AddToSet(aValue : Integer; aSet : TValuesSet) : TValuesSet;

    procedure SetRowArray(anArray : TArrayOfCells);
    procedure SetColumnArray(anArray : TArrayOfCells);
    procedure SetSectorArray(anArray : TArrayOfCells);

    function GetRowArray : TArrayOfCells;
    function GetColumnArray : TArrayOfCells;
    function GetSectorArray : TArrayOfCells;

    function FindGuessCell : TSudokuCell;
    function SearchForGuessCandidate(anArray : TArrayOfCells) : TSudokuCell;


    property RowArray : TArrayOfCells read GetRowArray write SetRowArray;
    property ColumnArray : TArrayOfCells read GetColumnArray write SetColumnArray;
    property SectorArray : TArrayOfCells read GetSectorArray write SetSectorArray;

  end;


implementation

procedure TRecalculate.SetRowArray(anArray: TArrayOfCells);
begin
  fRowArray := anArray;
end;

procedure TRecalculate.SetColumnArray(anArray: TArrayOfCells);
begin
  fColumnArray := anArray;
end;

procedure TRecalculate.SetSectorArray(anArray: TArrayOfCells);
begin
  fSectorArray := anArray;
end;

function TRecalculate.GetRowArray;
begin
  Result := fRowArray;
end;


function TRecalculate.GetColumnArray;
begin
  Result := fColumnArray;
end;


function TRecalculate.GetSectorArray;
begin
  Result := fSectorArray;
end;

function TRecalculate.ReCalculatePossibleValues(aOption : T_RCS = A) : Integer;
var
  MyRecalculateResults : Integer;
begin
  Show('RecalculatePossibleValues');
  MyRecalculateResults := 0;

  // Recalculate by Values
  Inc(MyRecalculateResults,ReCalculateByValues(aOption));
  // Recalculate by Count
   Inc(MyRecalculateResults,ReCalculateByCount(aOption));
  // Recalculate by Two Possible
  Inc(MyRecalculateResults,ReCalculateByTwoPossible(aOption));

  Result := MyRecalculateResults;
end;

function TRecalculate.ReCalculateByValues(aOption : T_RCS = A) : Integer;
var
  MyRecalculateResults : Integer;

begin
  MyRecalculateResults := 0;
  Show('RecalculatePossibleValues - ByValues');

  if (aOption = A) OR (aOption = VR) then
  begin
    // Recalculate by row
    Show('ReCalcByValues - RowArray');
    Inc(MyRecalculateResults,ReCalcByValues(RowArray));
  end;
  if (aOption = A) OR (aOption = VC) then
  begin
    // Recalculate by column
    Show('ReCalcByValues - ColumnArray');
    Inc(MyRecalculateResults,ReCalcByValues(ColumnArray));
  end;
  if (aOption = A) OR (aOption = VS) then
  begin
    // Recalculate by sector
    Show('ReCalcByValues - SectorArray');
    Inc(MyRecalculateResults,ReCalcByValues(SectorArray));
  end;

  Result := MyRecalculateResults;
end;

function TRecalculate.ReCalculateByCount(aOption : T_RCS = A) : Integer;
var
  MyRecalculateResults : Integer;
begin
  MyRecalculateResults := 0;
  Show('RecalculatePossibleValues - ByCount');
  if (aOption = A) OR (aOption = CR) then
  begin
    // Recalculate by row
    Show('ReCalcByCount - RowArray');
    Inc(MyRecalculateResults,ReCalcByCount(RowArray));
  end;
  if (aOption = A) OR (aOption = CC) then
  begin
    // Recalculate by column
    Show('ReCalcByCount - ColumnArray');
    Inc(MyRecalculateResults,ReCalcByCount(ColumnArray));
  end;
  if (aOption = A) OR (aOption = CS) then
  begin
    // Recalculate by sector
    Show('ReCalcByCount - SectorArray');
    Inc(MyRecalculateResults,ReCalcByCount(SectorArray));
  end;

  Result := MyRecalculateResults;
end;

function TRecalculate.ReCalculateByTwoPossible(aOption : T_RCS = A) : Integer;
var
  MyRecalculateResults : Integer;
begin
  MyRecalculateResults := 0;
  Show('RecalculatePossibleValues - ByTwoPossible');
  if (aOption = A) OR (aOption = TR) then
  begin
    // Recalculate by row
    Show('ReCalcByTwoPossible - RowArray');
    Inc(MyRecalculateResults,ReCalcByTwoPossible(RowArray));
  end;
  if (aOption = A) OR (aOption = TC) then
  begin
    // Recalculate by column
    Show('ReCalcByTwoPossible - SectorArray');
    Inc(MyRecalculateResults,ReCalcByTwoPossible(ColumnArray));
  end;
  if (aOption = A) OR (aOption = TS) then
  begin
    // Recalculate by sector
    Show('ReCalcByTwoPossible - ColumnArray');
    Inc(MyRecalculateResults,ReCalcByTwoPossible(SectorArray));
  end;

  Result := MyRecalculateResults;
end;


function TRecalculate.ReCalcByValues(anArray : TArrayOfCells) : Integer;
  { This procedure will recalculate the possible values for each cell
    by row, column, or sector. }
var
  I,J : Integer;
  TotalValues,PV,NewPV  : TValuesSet;  // All of the values in this R/C/S

  V : String;
  ReCalcCount : Integer;
begin
  ReCalcCount := 0;
  for I := sStart to sFinish do    // For each R/C/S
  begin
    TotalValues := [];
    for J := sStart to sFinish do   // For each cell
    begin
      V := anArray[I,J].GetValue;
      if (V <> '') then
      begin
        TotalValues := AddToSet(V.ToInteger,TotalValues);
      end;
    end;
    { We now have all of the values for this R/C/S.
      Now iterate through all of the cells in this R/C/S
      and remove these values from each cell's set of Possible Values. }
    for J := sStart to sFinish do   // For each cell
    begin
      V := anArray[I,J].GetValue;
      if (V <> '') then
      begin
        { This cell has a value set. }
        NewPV := [];  // It has no possible values
      end
      else
      begin
        { Determine what the Possible Values should be for this cell }

        PV := anArray[I,J].GetPossibleValues;
        NewPV := PV - TotalValues;
        if (NewPV <> PV) then
        begin
          Inc(ReCalcCount);
        end;

      end;
      { Update the Possible Values and the Number of Possible Values. }
      anArray[I,J].SetPossibleValues(NewPV);
      anArray[I,J].SetNumOfPossibleValues(CountInSet(NewPV));
    end;
  end;

 // Show(Format('Finished ReCalculating Possible Values by Value: %d',[ReCalcCount]));
  Result := ReCalcCount;

end;

function TRecalculate.ReCalcByCount(anArray : TArrayOfCells) : Integer;
  { Each row/column/sector must have one of every possible value.  If only one cell in
    the r/c/s is the only cell where a given number is possible, then the
    value for that cell must be that number. }

var
  I,J,V,Value : Integer;
  PossibleValues,PV1,PV2   : TValuesSet;
  P: AnsiChar;
  S : String;
  ValueCountArray : array [sStart..sFinish] of Integer;
  ReCalcCount : Integer;
begin
  ReCalcCount := 0;
  FillChar(ValueCountArray,SizeOf(ValueCountArray),0);

  for I := sStart to sFinish do   // R/C/S
  begin
    FillChar(ValueCountArray,SizeOf(ValueCountArray),0);
    for J := sStart to sFinish do   // Interate through each Cell in the R/C/S
    begin
      PossibleValues := anArray[I,J].GetPossibleValues;
      for P in PossibleValues do
      begin
        S := P;
        V := S.ToInteger;
        Inc(ValueCountArray[V]);
      end;
    end;
    { Were there any values that are possible in only one cell in this R/C/S? }
    for V := sStart to sFinish do
    begin
      if (ValueCountArray[V] = 1) then
      begin
        { This value is found only once in this R/C/S.  Where ever that is,
          this is really the only possible value for that cell. }
        for J:= sStart to sFinish do    // Each Column in this row
        begin
          PossibleValues := anArray[I,J].GetPossibleValues;
          for P in PossibleValues do
          begin
            S := P;
            Value := S.ToInteger;
            if (V = Value) then
            begin
              { We should now be at the cell that is the only one that has
                this value.  Remove all other possible values from this cell. }
              PV1 := anArray[I,J].GetPossibleValues;
              anArray[I,J].SetPossibleValues([P]);
              anArray[I,J].SetNumOfPossibleValues(1);
              PV2 := anArray[I,J].GetPossibleValues;
              if (PV1 <> PV2) then
              begin
                Inc(ReCalcCount);
              end;
            end;
          end;
        end;
      end;
    end;
  end;

  Result := ReCalcCount;
end;

function TRecalculate.ReCalcByTwoPossible(anArray : TArrayOfCells) : Integer;
  { If there are two and only two cells in any row that have exactly the
    same set of two possible values, then these two cells are going to be
    the only two cells that can have these values.  This means that no other
    cells in the same row can have either of these two possible values in
    their set of possible values.  Here we try to find them and to adjust
    the set of possible values in the row accordingly. }

var
  I,J,K,L,N : Integer;
  PossibleValues,PV,PVi,NewPV   : TValuesSet;
  CellsAndPVArray : TValuesSetArray;
  KCountArray : array [sStart..sFinish] of Integer;
  ArrayOfSame : TArrayOfTwo;
  PVSetArray : TArrayOfPV; // Array of the different combinations of two PV's
  ReCalcCount : Integer;

  iCounter,PVSetCounter : Integer;

begin
  ReCalcCount := 0;

  FillChar(ArrayOfSame,SizeOf(ArrayOfSame),0);
  K := 0;
  L := 0;
  PVSetCounter := 0;
  PVSetArray[1] := [];
  PVSetArray[2] := [];

  { FIRST STEP - Interate through all cells in a row and find those with
    only two possible values.  }

  for I := sStart to sFinish do   // Row
  begin
    { Initialize this array }
    for N := sStart to sFinish do
    begin
      CellsAndPVArray[N] := [];
      KCountArray[N] := 0;
    end;
    iCounter := 0;
    PVSetArray[1] := [];
    PVSetArray[2] := [];
    PVSetCounter := 0;

    { STEP ONE }
    for J := sStart to sFinish do   // Interate through each Column in the row
    begin
      PV := anArray[I,J].GetPossibleValues;
      if (CountInSet(PV) = 2) then
      begin
        // using this array to keep track of columns with two PV
        CellsAndPVArray[J] := PV;
        Inc(iCounter);
        // If this is a new set of 2 PV, add it to the array
        if (PVSetCounter = 0) then
        begin
          { This is the first set }
          Inc(PVSetCounter);
          PVSetArray[PVSetCounter] := PV;
        end
        else
        begin
          { This is NOT the first set }
          if (PVSetArray[PVSetCounter] <> PV) then
          begin
            { This is a new set of two PV's }
            Inc(PVSetCounter);
            PVSetArray[PVSetCounter] := PV;
          end;
        end;
      end;
    end;
    { Interation completed through all of the cells in this row }
    { STEP TWO }
    { Iterate through the elements of this array and see if any two are identical }
    if (iCounter < 2) then
    begin
      continue;
    end;

    for N := 1 to 2 do
    begin
      PVi := PVSetArray[N];
      if (PVi = []) then
      begin
        { Proceed only if the instance holds a possible values set }
        continue;
      end;

      J := 9;
      iCounter := 0; // Reset this so we can reuse it
      FillChar(ArrayOfSame,SizeOf(ArrayOfSame),0);

      { ArrayOfSame is a single dimension array of integers which the value
        is a column number that has a possible value set that matchs the
        possible values set of the column that matches the index number.
        Perhaps confusing but simply if you find an index with a nonzero
        value, the number of the index and the number of the value represent
        two columns in the puzzle with the same 2 possible values. }
      ArrayOfSame := FindIdenticalTwo(CellsAndPVArray,PVi);

      { STEP THREE }

       K := ArrayOfSame[1];
       L := ArrayOfSame[2];
       if (K = 0) OR (L = 0) then
       begin
         continue;
       end;
      { We have two cells with the same two possible values }
      PossibleValues := anArray[I,K].GetPossibleValues;
      PV := anArray[I,L].GetPossibleValues;
      if (PossibleValues <> PV) then
      begin
        ShowMessage('Error 1028:  Failed to accurately find pair of possible values');
        continue;
      end;
      { Iterate through all of the other cells in this row and remove
        these values from the set of possible values for those cells. }
      for J := sStart to sFinish do // Iterate through the cells
      begin
        if (J <> K) AND (J <> L) then
        begin
          PV := anArray[I,J].GetPossibleValues;
          if (PV <> PossibleValues) AND (CountInSet(PV) > 0) then
          begin
            NewPV := PV - PossibleValues;
            if (NewPV <> PV) then
            begin
              Inc(ReCalcCount);
              { Update the Possible Values for this cell. }
              anArray[I,J].SetPossibleValues(NewPV);
              PV := anArray[I,J].GetPossibleValues;
              { Update the number of possible values for this cell. }
              anArray[I,J].SetNumOfPossibleValues(CountInSet(PV));
            end;
          end;
        end;
      end;
    end;
  end;
  Show(Format('Finished ReCalculating Possible Values Two Possible: %d',[ReCalcCount]));
  Result := ReCalcCount;
end;

function TRecalculate.FindIdenticalTwo(anArray : TValuesSetArray; aSet : TValuesSet) : TArrayOfTwo;
var
  I,J : Integer;
  ArrayOfSame : TArrayOfTwo;
  nCounter : Integer;
begin
  nCounter := 0;
  { Initialize this array }
  FillChar(ArrayOfSame,SizeOf(ArrayOfSame),0);

  for I := sStart to sFinish do
  begin
    for J := sStart to sFinish do
    begin
      { Lots of conditions to check here:
      1) The PV set in the first iteration (I) matches the set in the second (J)
      2) The PV set is not empty
      3) The instance of the first iteration (I) is not the same as the instance
          of the second (J).
      4) Double check that the PV set in the first iteration is indeed 2
      5) The PV set matches the set (aSet) that we are supposed to be looking for }
      if ((anArray[I] = anArray[J]) AND (anArray[I] <> []) AND (I <> J)
        AND (CountInSet(anArray[I]) = 2 ) AND (anArray[I] = aSet)) then
      begin
        if (nCounter = 1) then
        begin
          { Check that the set of two is the same as the previous set of two we found }
          if not (anArray[I] = anArray[ArrayOfSame[nCounter]]) then
          begin
            continue;
          end;
        end;
        Inc(nCounter);
        if (nCounter >2) then
        begin
          Show('ERROR 1708 Found more than two cells with identical Possible Values.');
        end;
        ArrayOfSame[nCounter] := J;
      end;
    end;
  end;


  Result := ArrayOfSame;
end;

function TRecalculate.AddToSet(aValue : Integer; aSet : TValuesSet) : TValuesSet;
begin
    { Is there a better way to add a new member to a set?  This seems to be
      very cumbersome but it seems to work. }
    case aValue of
      1 : Include(aSet, '1');
      2 : Include(aSet, '2');
      3 : Include(aSet, '3');
      4 : Include(aSet, '4');
      5 : Include(aSet, '5');
      6 : Include(aSet, '6');
      7 : Include(aSet, '7');
      8 : Include(aSet, '8');
      9 : Include(aSet, '9');
    end;
    Result := aSet;
end;


function TRecalculate.FindGuessCell : TSudokuCell;
{ Some hard puzzles cannot be solved by the method of reducing possible
  values.  If the puzzle has a row/column/sector where only two cells
  remain and each share the same pair of two possible values, this
  function tries one of those.  If the puzzle is not solved, it restores
  the puzzle to that point of guessing and tries the other possible value.
  This function finds a cell suitable for guessing a solution for and returns it. }
var
  I,J : Integer;

  SolutionsFound : Integer;
  NumPV : Integer;  // Number of Possible Values
  PossibleValues :TValuesSet;
  P : Char;

  GuessPairVector1 : Integer;
  GuessPairVector2 : Integer;
  RCS : String;  // Is the Guess Pair in Row, Column, or Sector?

  GuessCell : TSudokuCell;
begin
  SolutionsFound := 0;
  GuessCell := nil;
  Show('Searching for Guess Candidate by Row');
  GuessCell := SearchForGuessCandidate(RowArray);
  if (GuessCell = nil) then
  begin
    Show('Searching for Guess Candidate by Column');
    GuessCell := SearchForGuessCandidate(ColumnArray);
  end;
  if (GuessCell = nil) then
  begin
    Show('Searching for Guess Candidate by Sector');
    GuessCell := SearchForGuessCandidate(SectorArray);
  end;
  if (GuessCell = nil) then
  begin
    Show('Failed to find a suitable cell with two possible to guess with.');
  end;
  Result := GuessCell;
end;

function TRecalculate.SearchForGuessCandidate(anArray : TArrayOfCells) : TSudokuCell;
var
  I,J : Integer;
  PV,PossibleValues : TValuesSet;
  PVCount : Integer;
  GuessCell : TSudokuCell;
  GuessPV : TValuesSet;
begin
  GuessCell := nil;
  { Save the current state of the puzzle to a global variable. }
  // SaveState;           // TODO - SaveState and RestoreState method still not working 2017-04-13
//  SaveFile;
  for I := sStart to sFinish do    // Outer Loop
  begin
    PV := [];
    PVCount := 0;
    for J := sStart to sFinish do    // Inner Loop
    begin
      { Search for a cell with only two possible values }
      PossibleValues := anArray[I,J].fPossibleValues;
      if (CountInSet(PossibleValues) = 2) then
      begin
        { Assume this will be a suitable Guess Cell. }
        GuessCell := anArray[I,J];
        GuessPV := PossibleValues;
        if (PV = []) then
        begin
          PV := PossibleValues;
        end
        else if (PossibleValues <> PV) then
        begin
          { If we find another cell with only two possible values but
            they are different possible values, we give up on this
            inner loop. }
          GuessCell := nil;
          GuessPV := [];
          { Should break out to the next outer loop. }
          break;
        end;
//        ShowPVSet('Guess Cell''s PV: ',PV);
      end;    // End of checking PV = 2
    end;   // End of inner loop
    { If we do have a Guess Cell, we can return. }
    if (GuessCell <> nil) then
    begin
      break;
    end;
  end;  // End of outer loop
  Result := GuessCell;

end;

end.


object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Sudoku Puzzle - Generation 2'
  ClientHeight = 601
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnClose: TButton
    Left = 504
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Close'
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    OnClick = btnCloseClick
  end
  object btnGo: TButton
    Left = 352
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 1
    OnClick = btnGoClick
  end
  object rgSolveOptions: TRadioGroup
    Left = 168
    Top = 8
    Width = 153
    Height = 73
    Caption = 'Solve Options'
    ItemIndex = 0
    Items.Strings = (
      'Solve One Cell'
      'Solve Entire Puzzle')
    TabOrder = 2
  end
  object btnShowDebug: TButton
    Left = 544
    Top = 568
    Width = 75
    Height = 25
    Caption = 'Show Debug'
    TabOrder = 3
    OnClick = btnShowDebugClick
  end
  object btnClearPuzzle: TButton
    Left = 32
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Clear Puzzle'
    TabOrder = 4
    OnClick = btnClearPuzzleClick
  end
  object cbDisplayPossibleValues: TCheckBox
    Left = 352
    Top = 55
    Width = 139
    Height = 17
    Caption = 'Display Possible Values'
    TabOrder = 5
    OnClick = cbDisplayPossibleValuesClick
  end
  object MainMenu: TMainMenu
    Left = 32
    Top = 96
    object File1: TMenuItem
      Caption = '&File'
      object Open1: TMenuItem
        Caption = '&Open'
        OnClick = OpenClick
      end
      object Save1: TMenuItem
        Caption = '&Save'
        OnClick = SaveClick
      end
      object SaveAs1: TMenuItem
        Caption = 'Save &As'
        OnClick = SaveAsClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = btnCloseClick
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 32
    Top = 152
  end
  object SaveDialog: TSaveDialog
    Left = 32
    Top = 200
  end
end

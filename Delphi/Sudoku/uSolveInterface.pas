unit uSolveInterface;

interface

uses uTypes, uCell;

type

  ISolvePuzzle = interface
    function SolveOneCell(anArray : TArrayOfCells; aGrid : TButtonGrid) : Integer;
    function CheckPuzzleCorrect(anArray : TArrayOfCells) : Boolean;
    function CheckPuzzleComplete(anArray : TArrayOfCells) : Boolean;
  end;

implementation

end.

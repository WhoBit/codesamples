unit uValueEntryForm;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TdlgCellValue = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    edtCellValue: TEdit;
    lblCellValue1: TLabel;
    lblCellValue2: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    SelectedValue: string;
    function Execute: Boolean;
  end;

var
  dlgCellValue: TdlgCellValue;

implementation

{$R *.dfm}

function TdlgCellValue.Execute: Boolean;
begin
  edtCellValue.Text := '';
  Result := (ShowModal = mrOK);
  SelectedValue :=  edtCellValue.Text;
end;

end.

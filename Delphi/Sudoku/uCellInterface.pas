unit uCellInterface;

interface

uses uTypes;

type



  ISudokuCell = interface
    function DetermineSector(aRow, aColumn : String) : String;
    function GetPossibleValues : TValuesSet;
    procedure SetPossibleValues(aValuesSet : TValuesSet);

    function GetNumOfPossibleValues : Integer;
    procedure SetNumOfPossibleValues(aValue : Integer);

    function GetValue : String;
    procedure SetValue(aValue : String);
    function GetRow : String;
    procedure SetRow(aRow : String);
    function GetColumn : String;
    procedure SetColumn(aColumn : String);

    property PossibleValues : TValuesSet read GetPossibleValues write SetPossibleValues;
    property NumOfPossibleValues : Integer read GetNumOfPossibleValues write SetNumOfPossibleValues;
    property Value : String read GetValue write SetValue;
    property Row : String read GetRow write SetRow;
    property Column : String read GetColumn write SetColumn;


  end;



implementation

end.

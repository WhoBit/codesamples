unit uTypes;

interface

uses
   Vcl.StdCtrls, uConstants;

const
  sStart = 1;
  sFinish = 9;


type
  TSudokuNums =   '1'..'9';
  TValuesSet = set of TSudokuNums;
  TValuesSetArray = array [sStart..sFinish] of TValuesSet;
  TButtonGrid = array[sStart..sFinish,sStart..sFinish] of TButton;
  TArrayOfTwo = array [1..2] of Integer;
  TArrayOfPV = array [1..2] of TValuesSet;
  TArrayOfInt = array [sStart..sFinish] of Integer;
  TArrayOfStr = array[sStart..sFinish,sStart..sFinish] of String;
  { Recalculation types: Value, Count, and Two Possible}
  T_RCS = (A,VR,VC,VS,CR,CC,CS,TR,TC,TS); // Row, Column, Sector

implementation

end.

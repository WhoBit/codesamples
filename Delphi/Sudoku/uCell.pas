unit uCell;

interface

uses
  System.SysUtils, Vcl.Dialogs, uTypes, uCellInterface, uConstants;

type
  TSudokuCell = class(TInterfacedObject, ISudokuCell)
    fPossibleValues : TValuesSet;
    fNumOfPossibleValues : Integer;
    fRow : string;
    fColumn : string;
    fSector : string;
    fValue : string;
    fName : string;
  private



  public
    constructor Create(aRow, aColumn : String);
    function DetermineSector(aRow, aColumn : String) : String;

    function GetPossibleValues : TValuesSet;
    procedure SetPossibleValues(aValuesSet : TValuesSet);

    function GetNumOfPossibleValues : Integer;
    procedure SetNumOfPossibleValues(aValue : Integer);

    function GetValue : String;
    procedure SetValue(aValue : String);

    function GetRow : String;
    procedure SetRow(aRow : String);
    function GetColumn : String;
    procedure SetColumn(aColumn : String);

    procedure ClearCell;

    property PossibleValues : TValuesSet read GetPossibleValues write SetPossibleValues;
    property NumOfPossibleValues : Integer read GetNumOfPossibleValues write SetNumOfPossibleValues;


    property Name : String read fName write fName;
    property Value : String read GetValue write SetValue;
    property Row : String read GetRow write SetRow;
    property Column : String read GetColumn write SetColumn;




  end;

type
  TArrayOfCells = array[sStart..sFinish,sStart..sFinish] of TSudokuCell;



implementation


constructor TSudokuCell.Create(aRow: string; aColumn: string);
begin
  fPossibleValues := OriginalPossibleValues;
  fNumOfPossibleValues := 9;
  fRow := aRow;
  fColumn :=aColumn;
  fValue := '';
  fSector := DetermineSector(aRow, aColumn);
  if fSector = '' then
  begin
    ShowMessage(Format('Error determing sector for row %s and column %s',[aRow,aColumn]));
  end;
end;

function TSudokuCell.DetermineSector(aRow, aColumn : String) : String;  // Determine which sector the cell is in.
var
  RPossibleValues : TValuesSet;
  CPossibleValues : TValuesSet;
  SectorNum : TValuesSet;
  RowNum : Integer;
  ColumnNum : Integer;
  S : Char;
begin
  RowNum := StrToInt(aRow);
  ColumnNum := StrToInt(aColumn);
  if  (RowNum <= 3) then
    RPossibleValues := ['1','2','3']
  else
  if (RowNum <= 6) then
    RPossibleValues := ['4','5','6']
  else
  if (RowNum <= 9) then
    RPossibleValues := ['7','8','9'] ;

  if  (ColumnNum <= 3) then
    CPossibleValues := ['1','4','7']
  else
  if (ColumnNum <= 6) then
    CPossibleValues := ['2','5','8']
  else
  if (ColumnNum <= 9) then
    CPossibleValues := ['3','6','9'];

  // The Sector is going to be the number that
  // is in both sets:
  SectorNum := RPossibleValues * CPossibleValues;

  for S in SectorNum do
    Result := S

end;

function TSudokuCell.GetPossibleValues : TValuesSet;
begin
  Result := fPossibleValues;
end;

procedure TSudokuCell.SetPossibleValues(aValuesSet : TValuesSet);
begin
  fPossibleValues := aValuesSet;
end;


function TSudokuCell.GetNumOfPossibleValues : Integer;
begin
  Result := fNumOfPossibleValues;
end;

procedure TSudokuCell.SetNumOfPossibleValues(aValue : Integer);
begin
  fNumOfPossibleValues := aValue;
end;

function TSudokuCell.GetValue : String;
begin
  Result := fValue;
end;

procedure TSudokuCell.SetValue(aValue: string);
begin
  fValue := aValue;
end;

function TSudokuCell.GetRow : String;
begin
  Result := fRow;
end;

procedure TSudokuCell.SetRow(aRow: string);
begin
  fValue := aRow;
end;

function TSudokuCell.GetColumn : String;
begin
  Result := fColumn;
end;

procedure TSudokuCell.SetColumn(aColumn: string);
begin
  fValue := aColumn;
end;

procedure TSudokuCell.ClearCell;
begin
  fValue := '';
  fPossibleValues := OriginalPossibleValues;
  fNumOfPossibleValues := 9;
end;

end.

unit uDebugForm;

interface

uses
  System.SysUtils, Vcl.StdCtrls, System.Classes, Vcl.Controls, uMainForm, Vcl.Forms,
  Vcl.ExtCtrls, uTypes;

type
  TfrmDebug = class(TForm)
    memoDebug: TMemo;
    btnReturn: TButton;
    btnDebugPossibleValues: TButton;
    rgDebug: TRadioGroup;
    btnDebugExecute: TButton;
    btnClear: TButton;
    rgShowPossibleOptions: TRadioGroup;
    procedure btnReturnClick(Sender: TObject);
    procedure btnDebugPossibleValuesClick(Sender: TObject);
    procedure btnDebugExecuteClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDebug: TfrmDebug;

implementation

{$R *.dfm}


procedure TfrmDebug.btnClearClick(Sender: TObject);
begin
  inherited;
  memoDebug.Clear;
end;

procedure TfrmDebug.btnDebugExecuteClick(Sender: TObject);
begin
  inherited;
  case rgDebug.ItemIndex of
    0 : MySudokuPuzzle.ResetPossibleValues;
    1 : MySudokuPuzzle.Recalculate(VR);
    2 : MySudokuPuzzle.Recalculate(VC);
    3 : MySudokuPuzzle.Recalculate(VS);
    4 : MySudokuPuzzle.Recalculate(CR);
    5 : MySudokuPuzzle.Recalculate(CC);
    6 : MySudokuPuzzle.Recalculate(CS);
  end;
end;

procedure TfrmDebug.btnDebugPossibleValuesClick(Sender: TObject);

begin
  frmMain.ShowPossibleValues(rgShowPossibleOptions.ItemIndex);
end;



procedure TfrmDebug.btnReturnClick(Sender: TObject);
begin
  Close;
end;

end.

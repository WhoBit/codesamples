unit uRecalculateInterface;

interface

uses uCell, uTypes;
type

  IRecalculate = interface
    function ReCalculatePossibleValues(aOption : T_RCS = A) : Integer;
    function ReCalculateByValues(aOption : T_RCS = A) : Integer;
    function ReCalculateByCount(aOption : T_RCS = A) : Integer;
    function ReCalculateByTwoPossible(aOption : T_RCS = A) : Integer;
    function ReCalcByValues(anArray : TArrayOfCells) : Integer;
    function ReCalcByCount(anArray : TArrayOfCells) : Integer;
    function ReCalcByTwoPossible(anArray : TArrayOfCells) : Integer;
    function FindIdenticalTwo(anArray : TValuesSetArray; aSet : TValuesSet) : TArrayOfTwo;


    procedure SetRowArray(anArray : TArrayOfCells);
    procedure SetColumnArray(anArray : TArrayOfCells);
    procedure SetSectorArray(anArray : TArrayOfCells);

    function GetRowArray : TArrayOfCells;
    function GetColumnArray : TArrayOfCells;
    function GetSectorArray : TArrayOfCells;

    function FindGuessCell : TSudokuCell;
    function SearchForGuessCandidate(anArray : TArrayOfCells) : TSudokuCell;

    property RowArray : TArrayOfCells read GetRowArray write SetRowArray;
    property ColumnArray : TArrayOfCells read GetColumnArray write SetColumnArray;
    property SectorArray : TArrayOfCells read GetSectorArray write SetSectorArray;

  end;

implementation

end.

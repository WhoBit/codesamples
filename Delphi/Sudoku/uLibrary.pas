unit uLibrary;

interface

uses uTypes;

  function AddToSet(aValue : Integer; aSet : TValuesSet) : TValuesSet;
  function CountInSet(aSet : TValuesSet) : Integer;
  function PVSetToString(aSet : TValuesSet) : String;
  procedure Show(msg : String);

implementation

uses
  uDebugForm, uPuzzle;

function AddToSet(aValue : Integer; aSet : TValuesSet) : TValuesSet;
begin
    { Is there a better way to add a new member to a set?  This seems to be
      very cumbersome but it seems to work. }
    case aValue of
      1 : Include(aSet, '1');
      2 : Include(aSet, '2');
      3 : Include(aSet, '3');
      4 : Include(aSet, '4');
      5 : Include(aSet, '5');
      6 : Include(aSet, '6');
      7 : Include(aSet, '7');
      8 : Include(aSet, '8');
      9 : Include(aSet, '9');
    end;
    Result := aSet;
end;

function CountInSet(aSet : TValuesSet) : Integer;
var
  P : AnsiChar;
  Counter : Integer;
begin
  Counter := 0;
  for P in aSet do
  begin
    Inc(Counter);
  end;
  Result := Counter;
end;

function PVSetToString(aSet : TValuesSet) : String;
{ A utility procedure to "print" to the memo screen for debugging. }
var
  pString : string;
  S : Char;
begin
  for S in aSet do
    pString := pString + ' ' + S;

  Result := pString;
end;

procedure Show(msg : String);
{ A utility procedure to "print" to the memo screen for debugging. }
begin

  frmDebug.memoDebug.Lines.Add(msg);
end;






end.

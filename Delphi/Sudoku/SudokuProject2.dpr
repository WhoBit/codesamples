program SudokuProject2;

uses
  Vcl.Forms,
  uMainForm in 'uMainForm.pas' {frmMain},
  uCell in 'uCell.pas',
  uLibrary in 'uLibrary.pas',
  uValueEntryForm in 'uValueEntryForm.pas' {dlgCellValue},
  uCellInterface in 'uCellInterface.pas',
  uTypes in 'uTypes.pas',
  uRecalculate in 'uRecalculate.pas',
  uSolvePuzzle in 'uSolvePuzzle.pas',
  uConstants in 'uConstants.pas',
  uRecalculateInterface in 'uRecalculateInterface.pas',
  uSolveInterface in 'uSolveInterface.pas',
  uPuzzleInterface in 'uPuzzleInterface.pas',
  uPuzzle in 'uPuzzle.pas',
  uDebugForm in 'uDebugForm.pas' {frmDebug};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TdlgCellValue, dlgCellValue);
  Application.CreateForm(TfrmDebug, frmDebug);
  Application.Run;
end.

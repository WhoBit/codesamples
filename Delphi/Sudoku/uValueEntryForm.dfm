object dlgCellValue: TdlgCellValue
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Enter Cell Value'
  ClientHeight = 105
  ClientWidth = 384
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblCellValue1: TLabel
    Left = 8
    Top = 8
    Width = 178
    Height = 13
    Caption = 'Enter a value for this cell from 1 to 9 '
  end
  object lblCellValue2: TLabel
    Left = 8
    Top = 32
    Width = 179
    Height = 13
    Caption = 'or enter 0 to clear any current value.'
  end
  object OKBtn: TButton
    Left = 300
    Top = 8
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 300
    Top = 38
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object edtCellValue: TEdit
    Left = 80
    Top = 64
    Width = 57
    Height = 21
    TabOrder = 0
  end
end

<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
## worship_tools.php - A tool to record and query who participated in various aspects of the worship service

## 2012-01-28 - v1.0.0 - Created
## 2012-02-01 - v1.0.1 - Added Gifts List.  Put query results in a chart. - kdg
## 2012-02-02 - v1.0.2 - Added queryWorshipMembers - kdg
## 2012-02-11 - v1.2.0 - Revised to support PHP 5.3 - kdg
## 2012-03-03 - v1.2.1 - Added basic_password for access to updating the worship record. - kdg
## 2012-03-05 - v1.2.2 - Corrected a problem defining giftNameToAdd - kdg
## 2012-03-21 - v1.2.3 - Revised help.  Put request for password inline with selecting update worship record.  Detecting script name to permit using _beta version - kdg
## 2012-05-06 - v1.3.0 - Added serviceForm - kdg
## 2012-05-28 - v1.3.1 - Lengthened permitted service name from 50 to 64 - kdg
## 2012-12-18 - v1.3.2 - Granting can_input_records to either password level - kdg
## 2013-02-16 - v1.4.0 - Showing guests on the members form.  Updated help.  Updated screen labels.  Added links to the top of some pages - kdg
## 2013-03-03 - v1.4.1 - Added $homesite variable to StandardFooter - kdg
##
## 
$script_ver="v1.4.1";

echo "<html>";
echo "<head>";
echo "</head>";
echo "<title>The Worship Tools</title>";
echo "</head>";
echo "<body>";

# Connect to the database  - Need to do this immediately because we are going to reuse the variable password
#include("dbinfo.inc.php");
$basename = basename($_SERVER['REQUEST_URI']);
$tmp_array=explode('?',$basename);
$scriptname=$tmp_array[0];
$homesite=$_SERVER['SERVER_NAME'];


if (preg_match('/beta/',$scriptname)) {   
    # If we are using the beta script, use the beta database
    include 'dbinfo.inc_beta.php';
} else {    
    include 'dbinfo.inc.php';
}

$link = mysql_connect($hostname, $username, $password);
if (!$link) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($database) or die( "Unable to select database");

# Set the timezone if necessary
# Get the version of php
$my_php=phpversion();
$parts=explode('.',$my_php);

if ($parts[0] > 4) {
    if ($parts[1]) {      
        # If PHP is 5.1 or greater, use this function
        date_default_timezone_set('America/New_York');
    }        
}

############
# Variables
############
$password = "giftsurvey";  # The password  to get into editing members and gifts
$basic_password = "laurelville";  # The password to get the inputting worship records
$can_input_records = 0; # This value must be set to enable updating records.
$grzebtw='';   # This is the re-entry variable.  If it is set and is within the limit, then re-entry is permitted.
$tool = "default";      # This variable will determine which section of the code below is to be executed.
$current_time=time();   # The limit to re-entry is by time so we need to know the current time
$time_limit=($current_time + 3600); # The window of access is one hour after initial entry.
$support_email="ksglick@gmail.com";



#$scriptname='worship_tools_beta.php';

############
# Main
############
if (isset($_GET["grzebtw"])) {
    $grzebtw = $_GET['grzebtw'];
}
if (isset($_POST["grzebtw"])) {
    $grzebtw = $_POST['grzebtw'];
}
if ($grzebtw == 42) {
    $can_input_records = 1;
}

if (isset($_GET["tool"])) {
    $tool = $_GET['tool'];
}
if (isset($_POST["tool"])) {
    $tool = $_POST['tool'];
}

if ((isset($_POST["password"]) && ($_POST["password"]=="$password")) ||
    (($grzebtw > 50 ) && ($grzebtw < $time_limit))) {
    # Set the current value for the re-entry variable if it is not set (entering for the first time)
    if (!$grzebtw) {
        $grzebtw=time();         
    }    
    # If you made it this far, the default menu is the edit level menu
    $default_tool='editLevel';
    # You also have access to inputting records
    $can_input_records = 1;    
    if (!$tool) {
        # If a tool has not been requested, send them to the edit level menu
        $tool='editLevel';        
    } elseif (($tool == 'editLevelAccess') || ($tool == 'default')) {
        # If they were requesting edit level access and they entered the correct password, send them on to the edit menu
        $tool=$default_tool; 

    }
} elseif (isset($_POST["password"]) && ($_POST["password"]=="$basic_password"))  {    
    # Gets the ability to input worship service records
    $can_input_records = 1;
    if (!$tool) {
        $tool='default';     
    }
    if (!$grzebtw) {
        $grzebtw=42;         
    }      
} else {
    if (!$tool) {
        # If a tool has not been requested, send them to the default menu
        $tool='default';        
    }    
}


# Tools section
if ($tool == "default") {    
    echo "<b><center>Pilgrims Worship Gifts Query</center></b><br><br>";
    echo "<A HREF = $scriptname?tool=giftsList&grzebtw=$grzebtw>Gifts List</A><br />";
    if ($can_input_records) {
        #echo "<A HREF = $scriptname?tool=updateWorshipForm&grzebtw=$grzebtw>Update Worship Service Data</A><br />";
    }
    echo "<A HREF = $scriptname?tool=updateWorshipForm&grzebtw=$grzebtw>Update Worship Service Data</A><br />";
    echo "<A HREF = $scriptname?tool=queryWorshipForm&grzebtw=$grzebtw>Query Worship Service Data</A><br />";    
    echo "<br />";
    # echo "<A HREF = $scriptname?tool=editLevelAccess>Edit Access</A><br />";  
    echo "<br />";
    echo "<A HREF = $scriptname?tool=help>Help</A><br />";  
    echo "<A HREF = http://$homesite>Main Pilgrim Website</A><br />";  
    echo "<br /><small>$script_ver</small><br />";
    exit; 
} else if (($tool == "editLevelAccess") || ((!$can_input_records) && ($tool == "updateWorshipForm")) ) {    
    # access to the edit tools for gifts and members    
    // Wrong password or no password entered display this message
    
    if (isset($_POST['password']) || $password == "") {        
        print "<p><font color=\"red\"><b>Incorrect password</b><br>Please enter the correct password</font></p>";
        print "If you need assistance: <A HREF = mailto:$support_email?subject=WorshipTool_Access_Request>Request Access</A><br />";
    }
    
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =$tool>";                 
       
    print "<form method=\"post\"><p>Please enter your password for access<br>";
    print "<input name=\"password\" type=\"password\" size=\"25\" maxlength=\"12\"><input value=\"Login\" type=\"submit\"></p></form>";    
    exit;    
} else if ($tool == "editLevel") {    
    # editLevel Menu
    echo "<b><center>Pilgrims Worship Gifts Tool</center></b><br><br>";
    echo "<A HREF = $scriptname?tool=giftForm&grzebtw=$grzebtw>Gifts</A><br />";

    echo "<A HREF = $scriptname?tool=memberForm&grzebtw=$grzebtw>Members</A><br />";    
    echo "<A HREF = $scriptname?tool=associateForm&grzebtw=$grzebtw>Associate a Member and a Gift</A><br />";
    echo "<A HREF = $scriptname?tool=associateFormGift&grzebtw=$grzebtw>Associate a Gift and a Member</A><br />";  
    echo "<br />";
    echo "<A HREF = $scriptname?tool=giftsList&grzebtw=$grzebtw>Gifts List</A><br />";    
    echo "<A HREF = $scriptname?tool=updateWorshipForm&grzebtw=$grzebtw>Update Worship Service Data</A><br />";
    echo "<A HREF = $scriptname?tool=queryWorshipForm&grzebtw=$grzebtw>Query Worship Service Data</A><br />";    
    echo "<br />";
    echo "<A HREF = $scriptname?tool=serviceList&grzebtw=$grzebtw>Serving List</A><br />";    
    echo "<A HREF = $scriptname?tool=serviceForm&grzebtw=$grzebtw>Serving</A><br />";
    echo "<A HREF = $scriptname?tool=associateServiceForm&grzebtw=$grzebtw>Associate a Member and a Service</A><br />";    
    echo "<br />";
    echo "<A HREF = $scriptname?tool=help&grzebtw=$grzebtw>Help</A><br />";   
    echo "<br /><small>$script_ver</small><br />";    
    exit; 


##############
#   Service Form
###############    
} else if ($tool == "serviceForm") {
    # Add Edit or Delete Services
    $serviceName='';
    $serviceEdit='';    
    $oldName='';
    if (isset($_POST['serviceEdit'])) {
        $serviceEdit = $_POST['serviceEdit'];
    }
    if (isset($_POST['serviceName'])) {
        $serviceName = $_POST['serviceName'];
    }
    if (isset($_POST['oldName'])) {
        $oldName = $_POST['oldName'];
    }
    $task='';
    if (isset($_POST['task'])) {
        $task = $_POST['task'];
    }    

    if ($_POST) {
        foreach ($_POST as $var => $key) {              
            # Look for variable names starting as "service_" and the number attached after the "_" is the service id                
            if (preg_match("/service_/",$var)) {        
                $g=$key;            
                $serviceEdit=$g;
            }       
        }
    }
    
    # Show the current services
    echo "<b><center>Pilgrims Worship services</center></b><br><br>";
    # Check to see if we are editing or adding a service
    if (strcmp($task,"update")==0) {
        # We need to update the specified serviceName rather than add a new one
        # Escape any single quotes
        if (!get_magic_quotes_gpc()) {
            $serviceNameToAdd=addslashes($serviceName);
        } else {
            $serviceNameToAdd=$serviceName;
        }
        #$serviceName=str_replace("'","\'",$serviceName);      
        $query = "UPDATE service SET serviceName = '$serviceNameToAdd' WHERE service_id = '$serviceEdit'";
        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully updated service $oldName to $serviceName.<br /><br />";
        } else {
            print "Hmmm, not certain that $serviceName got updated as you expected. <br />";    
        }
        # Reset these variables
        $serviceName='';
        $serviceEdit='';

    } 
    if (strcmp($task,"delete")==0) {
        # First, make certain that they really want to delete this service.  Find how many connections use this service
        #$query = "select count(*) as count from record where service_id = '$serviceEdit'";        
        #$result=mysql_query($query);
        #$num=mysql_result($result,0,"count");

        $query = "select count(*) as count from memberServiceMatch where service_id = '$serviceEdit'";        
        $result=mysql_query($query);
        $num=mysql_result($result,0,"count");

        echo "There were $num records found using $serviceName. <br /><br />";
        echo "Are you CERTAIN you wish to delete $serviceName?  (This cannot be undone!)<br /";        
        echo "<br /><br />You are about to delete the service '$serviceName'<br />"; 
        
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";        
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='delete_verified'>";   
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='serviceForm'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'serviceEdit' VALUE =$serviceEdit>";                       
        echo "<INPUT TYPE = 'hidden' NAME = 'serviceName' VALUE =\"$serviceName\">";  
        echo "<INPUT TYPE = 'Submit' VALUE = 'Yes, Really Delete!'>";
        echo "</FORM>";          
         echo "<br />";
        mysql_close();
        StandardFooter();
        exit;             
    }
    if (strcmp($task,"delete_verified")==0) {
        # We need to delete the specified serviceName rather than add a new one        
        $query = "delete from service WHERE service_id = '$serviceEdit'";        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully deleted member $oldName.<br />";
        } else {
            print "Hmmm, not certain that $serviceName got deleted as you expected. <br />";    
        }
        # Reset these variables
        $serviceName='';
        $serviceEdit='';
    } 
    if ($serviceName) {
        if (!get_magic_quotes_gpc()) {
            $serviceNameToAdd=addslashes($serviceName);
        } else {
            $serviceNameToAdd=$serviceName;
        }     
        
        # Escape any single quotes    
        #$serviceNameToAdd=str_replace("'","\'",$serviceName);        
        # A  service Name has been submitted.  See if it is new
        $query = "SELECT service_id FROM service WHERE serviceName = '$serviceNameToAdd'";
        $result=mysql_query($query);
        $num=mysql_numrows($result);
        
        if ($num) {
            print "Sorry, ".$serviceName." is not a new service.<br /><br />";
        } else {
            # Did not find a match so add this new one
            #print "Insert service into service table: ".$serviceName."<br />";
            $query = "INSERT INTO service VALUES ('','$serviceNameToAdd')";            
            if (mysql_query($query)) {
                print "Added $serviceName to services<br />";
            } else {
                $er_msg=mysql_error();
                print "ERROR: $er_msg<br />";
            }
      
        }       
    }

    if ($serviceEdit) {
        # We have requested to edit a service rather than add a new one
        # Show a form with the existing service allowing edits to be made.
        $query="SELECT serviceName FROM service where service_id = $serviceEdit";
        $result=mysql_query($query);
        $serviceName=mysql_result($result,0,"serviceName");
      
        echo "Editing service: $serviceName<br />";
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='update'>"; 
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='serviceForm'>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'serviceEdit' VALUE =$serviceEdit>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'oldName' VALUE =\"$serviceName\">";  
        echo "<br />";
        echo "<INPUT TYPE = 'TEXT' NAME = 'serviceName' size='64' maxlength='64' VALUE =\"$serviceName\">";
        echo "<br />";
        echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
        echo "</FORM>";
        echo "<br /><br /><br /><br />";
        echo "---------------------------------------------<br />";
        echo "<br /><br /><br /><br />";    
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
        echo "Delete $serviceName<br />";
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='delete'>";   
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='serviceForm'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'serviceEdit' VALUE =$serviceEdit>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'serviceName' VALUE =\"$serviceName\">";  

        echo "<INPUT TYPE = 'Submit' VALUE = 'Delete'>";
        echo "</FORM>";     
        mysql_close();
        StandardFooter();
        exit;      
        
    } 
    ###
    echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";     
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='serviceForm'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
    $query="SELECT * FROM service order by serviceName";
    $result=mysql_query($query);

    $num=mysql_numrows($result); 
    $i=0;
    while ($i < $num) {    
        $serviceName=mysql_result($result,$i,"serviceName");
        $service_id=mysql_result($result,$i,"service_id");      
        echo "<input type='radio' name='service_' value=$service_id /> $serviceName <br />";
        ++$i;
    } 
    
    echo "<INPUT TYPE = 'TEXT' NAME = 'serviceName' VALUE ='' SIZE='35'>";
    echo "<br />";
    echo "--------------------------------------<br />";

    echo "Enter a new service and click 'Submit'.  <br />";
    echo "A letter and colon corresponding to the following service categories should be placed in front of each entry:<br />"; 
    echo "A: Youth Education & Care<br />";
    echo "B: Other Gifts for the Congregation<br />";
    echo "<br />For example, to enter a service in the Youth Education & Care category, you might enter:<br />";
    echo "<b>A: Coordinate Nursery</b><br />";
    
    
    echo "<br /><br />";
    echo "To edit an existing service, check the box";
    echo "<br />";
    echo "next to the give and click 'Submit'.";
    echo "<br />";
    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
    echo "</FORM>";
    echo "</body>";
    echo "</html>";
    mysql_close();
    StandardFooter();
    exit;      
} else if ($tool == "giftForm") {
    # Add Edit or Delete Gifts
    $giftName='';
    $giftEdit='';    
    $oldName='';
    if (isset($_POST['giftEdit'])) {
        $giftEdit = $_POST['giftEdit'];
    }
    if (isset($_POST['giftName'])) {
        $giftName = $_POST['giftName'];
    }
    if (isset($_POST['oldName'])) {
        $oldName = $_POST['oldName'];
    }
    $task='';
    if (isset($_POST['task'])) {
        $task = $_POST['task'];
    }    
    #$oldName = $_POST['oldName'];
    #$task = $_POST['task'];
    if ($_POST) {
        foreach ($_POST as $var => $key) {    
            # Look for variable names starting as "gift_" and the number attached after the "_" is the gift id                
            if (preg_match("/gift_/",$var)) {        
                $g=$key;            
                $giftEdit=$g;
            }       
        }
    }
    # Show the current gifts
    echo "<b><center>Gifts List</center></b><br><br>";
    # Check to see if we are editing or adding a gift
    if (strcmp($task,"update")==0) {
        # We need to update the specified giftName rather than add a new one
        # Escape any single quotes
        if (!get_magic_quotes_gpc()) {
            $giftNameToAdd=addslashes($giftName);
        } else {
            $giftNameToAdd=$giftName;
        }
        #$giftName=str_replace("'","\'",$giftName);      
        $query = "UPDATE gift SET giftName = '$giftNameToAdd' WHERE gift_id = '$giftEdit'";
        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully updated gift $oldName to $giftName.<br /><br />";
        } else {
            print "Hmmm, not certain that $giftName got updated as you expected. <br />";    
        }
        # Reset these variables
        $giftName='';
        $giftEdit='';

    } 
    if (strcmp($task,"delete")==0) {
        # First, make certain that they really want to delete this gift.  Find how many connections use this gift
        $query = "select count(*) as count from record where gift_id = '$giftEdit'";        
        $result=mysql_query($query);
        $num=mysql_result($result,0,"count");

        $query = "select count(*) as count from memberGiftMatch where gift_id = '$giftEdit'";
        $result=mysql_query($query);
        $num+=mysql_result($result,0,"count");

        echo "There were $num records found using $giftName. <br /><br />";
        echo "Are you CERTAIN you wish to delete $giftName?  (This cannot be undone!)<br /";        
        echo "<br /><br />You are about to delete the gift '$giftName'<br />"; 
        
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";        
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='delete_verified'>";   
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='giftForm'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'giftEdit' VALUE =$giftEdit>";                       
        echo "<INPUT TYPE = 'hidden' NAME = 'giftName' VALUE =\"$giftName\">";  
        echo "<INPUT TYPE = 'Submit' VALUE = 'Yes, Really Delete!'>";
        echo "</FORM>";          
         echo "<br />";
        mysql_close();
        StandardFooter();
        exit;             
    }
    if (strcmp($task,"delete_verified")==0) {
        # We need to delete the specified giftName rather than add a new one        
        $query = "delete from gift WHERE gift_id = '$giftEdit'";        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully deleted member $oldName.<br />";
        } else {
            print "Hmmm, not certain that $giftName got deleted as you expected. <br />";    
        }
        # Reset these variables
        $giftName='';
        $giftEdit='';
    } 
    if ($giftName) {
        if (!get_magic_quotes_gpc()) {
            $giftNameToAdd=addslashes($giftName);
        } else {
            $giftNameToAdd=$giftName;
        }        
        # Escape any single quotes    
        #$giftNameToAdd=str_replace("'","\'",$giftName);        
        # A  Gift Name has been submitted.  See if it is new
        $query = "SELECT gift_id FROM gift WHERE giftName = '$giftNameToAdd'";
        $result=mysql_query($query);
        $num=mysql_numrows($result);
        if ($num) {
            print "Sorry, ".$giftName." is not a new gift.<br /><br />";
        } else {
            # Did not find a match so add this new one
            #print "Insert gift into gift table: ".$giftName."<br />";
            $query = "INSERT INTO gift VALUES ('','$giftNameToAdd')";
            if (mysql_query($query)) {
                print "Added $giftName to gifts<br />";
            } else {
                $er_msg=mysql_error();
                print "ERROR: $er_msg<br />";
            }
      
        }       
    }

    if ($giftEdit) {
        # We have requested to edit a gift rather than add a new one
        # Show a form with the existing gift allowing edits to be made.
        $query="SELECT giftName FROM gift where gift_id = $giftEdit";
        $result=mysql_query($query);
        $giftName=mysql_result($result,0,"giftName");
      
        echo "Editing gift: $giftName<br />";
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='update'>"; 
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='giftForm'>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'giftEdit' VALUE =$giftEdit>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'oldName' VALUE =\"$giftName\">";  
        echo "<br />";
        echo "<INPUT TYPE = 'TEXT' NAME = 'giftName' size='50' maxlength='50' VALUE =\"$giftName\">";
        echo "<br />";
        echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
        echo "</FORM>";
        echo "<br /><br /><br /><br />";
        echo "---------------------------------------------<br />";
        echo "<br /><br /><br /><br />";    
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
        echo "Delete $giftName<br />";
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='delete'>";   
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='giftForm'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'giftEdit' VALUE =$giftEdit>";       
        echo "<INPUT TYPE = 'hidden' NAME = 'giftName' VALUE =\"$giftName\">";  

        echo "<INPUT TYPE = 'Submit' VALUE = 'Delete'>";
        echo "</FORM>";     
        mysql_close();
        StandardFooter();
        exit;      
        
    } 
    ###
    echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";     
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='giftForm'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
    $query="SELECT * FROM gift order by giftName";
    $result=mysql_query($query);

    $num=mysql_numrows($result); 
    $i=0;
    while ($i < $num) {    
        $giftName=mysql_result($result,$i,"giftName");
        $gift_id=mysql_result($result,$i,"gift_id");      
        echo "<input type='radio' name='gift_' value=$gift_id /> $giftName <br />";
        ++$i;
    } 
    
    echo "<INPUT TYPE = 'TEXT' NAME = 'giftName' VALUE =''>";
    echo "<br />";
    echo "--------------------------------------<br />";

    echo "Enter a new Gift and click 'Submit'.  (A number can be placed in front of the gift to control the order in which the gifts will appear.)";
    echo "<br /><br />";
    echo "To edit an existing gift, check the box";
    echo "<br />";
    echo "next to the give and click 'Submit'.";
    echo "<br />";
    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
    echo "</FORM>";
    echo "</body>";
    echo "</html>";
    mysql_close();
    StandardFooter();
    exit;  
    
} else if ($tool == "memberForm") {
    # The memberForm goes here       
    echo "<b><center>Member Form</center></b><br><br>";
    ###

    $authlevel = 1;
    if (isset($_POST['authlevel'])) {
        $authlevel = $_POST['authlevel'];
    }
    
    $memberEdit=''; # This variable can be defined specifically here or with "member_" as below in the preg_match loop
    if (isset($_POST['memberEdit'])) {
        $memberEdit = $_POST['memberEdit'];
    }
    $memberName=''; 
    if (isset($_POST['memberName'])) {
        $memberName = $_POST['memberName'];
    }    
    if (isset($_POST['memberEdit'])) {
        $memberEdit = $_POST['memberEdit'];
    }    
    $oldName='';
    if (isset($_POST['oldName'])) {
        $oldName = $_POST['oldName'];
    }      
    $task='';
    if (isset($_POST['task'])) {
        $task = $_POST['task'];
    }       
    if ($_POST) {
        foreach ($_POST as $var => $key) {               
            # Look for variable names starting as "member_" and the number attached after the "_" is the member id                
            if (preg_match("/member_/",$var)) {        
                $g=$key;            
                $memberEdit=$g;                
            }       
        }
    }
    
    echo "<b><center>Pilgrims Worship Members</center></b><br><br>";
    # Show the current members

    # Check for edits
    if (strcmp($task,"update")==0) {
        # We need to update the specified memberName rather than add a new one
        
        $query = "UPDATE member SET memberName = '$memberName' WHERE member_id = '$memberEdit'";
        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully updated member $oldName to $memberName.<br />";
        } else {
            print "Hmmm, not certain that $memberName got updated as you expected. <br />";    
        }
        # Reset these variables
        $memberName='';
        $memberEdit='';

    } 

    if (strcmp($task,"delete")==0) {

        # First, make certain that they really want to delete this member.  Find how many connections use this member
        $record_count=0;
        $query = "select count(*) as count from record where member_id = '$memberEdit'";
        $result=mysql_query($query);
        $record_count+=mysql_result($result,0,"count");

        $query = "select count(*) as count from memberGiftMatch where member_id = '$memberEdit'";
        $result=mysql_query($query);
        $record_count+=mysql_result($result,0,"count");        
        
        echo "There were $record_count records found using $memberName. <br />";
        if ($record_count) {
            echo "<br /> >>>>Are you CERTAIN you wish to deactivate $memberName?<<< <br />";
            # If there no records, you can delete it
            echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
            echo "<br />";
            echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='deactivate_verified'>";   
          
            echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =memberForm>";    
            echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";        
            echo "<INPUT TYPE = 'hidden' NAME = 'memberEdit' VALUE =$memberEdit>";       
            echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =\"$memberName\">";  

            echo "<INPUT TYPE = 'Submit' VALUE = 'Yes, Really De-Activate!'>";
            echo "</FORM>";              
        } else {
            # If there no records, you can delete it
            echo "<br /> >>>>Are you CERTAIN you wish to delete $memberName?<<< <br /> (This cannot be undone!)<br />";

            echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
            echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='delete_verified'>";                 
            echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='memberForm'>";    
            echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";        
            echo "<INPUT TYPE = 'hidden' NAME = 'memberEdit' VALUE =$memberEdit>";       
            echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =\"$memberName\">";  
            echo "<INPUT TYPE = 'Submit' VALUE = 'Yes, Really Delete!'>";
            echo "</FORM>"; 
            echo "<br />";              
            
        
        }
        echo "<br />";            
        #echo "Otherwise, choose the 'home' link below:<br />";
        mysql_close();
        StandardFooter();
        exit;    
    }
    if (strcmp($task,"delete_verified")==0) {    
        # We need to delete the specified memberName rather than add a new one        
        $query = "delete from member WHERE member_id = '$memberEdit'";        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully deleted member $memberName.<br />";
        } else {
            print "Hmmm, not certain that $memberName got deleted as you expected. <br />";    
        }
        # Reset these variables
        $memberName='';
        $memberEdit='';

    } elseif (strcmp($task,"deactivate_verified")==0) {    
        # We need to deactivated the specified memberName rather than add a new one        
        $query = "update member set status = 0 WHERE member_id = '$memberEdit'";        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            print "Successfully deactivated member $memberName.<br />";
        } else {
            print "Hmmm, not certain that $memberName got deactivated as you expected. <br />";    
        }
        # Reset these variables
        $memberName='';
        $memberEdit='';

    } 
    
    if ($memberName) {
        $member_id=0;
        # A  Member Name has been submitted.  See if it is new
        $query = "SELECT member_id,status,guest FROM member WHERE memberName = '$memberName'";
        $result=mysql_query($query);
        if ($result) {
            $num=mysql_numrows($result); 
            $i=0;
            while ($i < $num) {  
                $member_id=mysql_result($result,$i,"member_id");        
                $status=mysql_result($result,$i,"status");
                $guest=mysql_result($result,$i,"guest");
                ++$i;
            }        
        }

        if ($member_id) {        
            if (strcmp($task,"reactivate")==0) {                
                # We have been asked to re-activate
                $query = "update member set status = 1 where memberName = '$memberName'";       
                mysql_query($query);                 
                $num=mysql_affected_rows();
                if ($num) {
                    print "Successfully re-activated member $memberName.<br />";
                } else {
                    print "Hmmm, not certain that $memberName got re-activated as you expected. <br />";    
                }                
            } elseif  (strcmp($task,"no_reactivate")==0) {
                # We have been asked NOT to re-activate               
                echo "Not re-activating $memberName<br />";
            } elseif (strcmp($task,"make_member")==0) {                 
                # We have been asked to convert this guest to a member
                $query = "update member set guest = 0 where memberName = '$memberName'";       
                mysql_query($query);                 
                $num=mysql_affected_rows();
                if ($num) {
                    print "Successfully made $memberName a member.<br />";
                } else {
                    print "Hmmm, not certain that $memberName got made a member as you expected. <br />";    
                }                
            } elseif  (strcmp($task,"skip")==0) {
                # We have been asked NOT to re-activate               
                echo "Not making $memberName a member<br />";                
            } elseif ($status) {
                if ($guest) {
                    echo "$memberName is entered as a guest.  Do you wish to change them to a member?<br />";
                    ###
                    echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";    
                    ###
                    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =memberForm>";         
                    echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =$memberName>";  
                    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
                    echo "<p><INPUT TYPE = 'radio' NAME = 'task' VALUE ='make_member'>Yes</p>";
                    echo "<p><INPUT TYPE = 'radio' NAME = 'task' VALUE ='skip'>No</p>";               
                    echo "<br />";
                    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
                    echo "</FORM>";
                    mysql_close();
                    StandardFooter();
                    exit;                      
                    ###
                } else {
                    print "Sorry, ".$memberName." is not a new member.<br />";
                }
                
            } else {
                print "$memberName already exists in as a member but is currently not active.  Do you wish to re-activiate?<br />";
                
                echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";    
                ###
                echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =memberForm>";         
                echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =$memberName>";  
                echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
                echo "<p><INPUT TYPE = 'radio' NAME = 'task' VALUE ='reactivate'>Yes</p>";
                echo "<p><INPUT TYPE = 'radio' NAME = 'task' VALUE ='no_reactivate'>No</p>";               
                echo "<br />";
                echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
                echo "</FORM>";
                mysql_close();
                StandardFooter();
                exit;  
                ###
            }
        } else {               
            # Did not find a match so add this new one            
            $query = "INSERT INTO member (memberName,authlevel) VALUES ('$memberName','$authlevel')";       
            mysql_query($query);
            $my_error=mysql_error();     
            if ($my_error) {                
                print "Error: Failed to add $memberName - $my_error <br /><br />";  
            } else {
                print "Added $memberName<br /><br />";                   
            }                        
        }        
    }
    if ($memberEdit) {
        # We have requested to edit a member rather than add a new one
        # Show a form with the existing member allowing edits to be made.
        $query="SELECT memberName FROM member where member_id = $memberEdit";
        $result=mysql_query($query);
        $memberName=mysql_result($result,0,"memberName");
      
        echo "Editing member: $memberName<br />";
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='update'>";   
        
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =memberForm>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
        echo "<INPUT TYPE = 'hidden' NAME = 'memberEdit' VALUE =$memberEdit>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'oldName' VALUE =\"$memberName\">";  
        echo "<br />";
        echo "<INPUT TYPE = 'TEXT' NAME = 'memberName' VALUE =\"$memberName\">";
        echo "<br />";
        echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
        echo "</FORM>";
        echo "<br /><br /><br /><br />";
        echo "---------------------------------------------<br />";
        echo "<br /><br /><br /><br />";
        echo "Delete $memberName<br />";        
        echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'task' VALUE ='delete'>";           
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =memberForm>";  
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";  
        echo "<INPUT TYPE = 'hidden' NAME = 'memberEdit' VALUE =$memberEdit>"; 
        echo "<INPUT TYPE = 'hidden' NAME = 'oldName' VALUE =\"$memberName\">";      
        echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =\"$memberName\">";  
        echo "<INPUT TYPE = 'Submit' VALUE = 'Delete'>";
        echo "</FORM>";    
        mysql_close();
        StandardFooter();
        exit;  
    }
    ###
    # Get the existing members who are active and not marked as guests    
    #$query="SELECT * FROM member where status = 1 and guest = 0 order by memberName";
    $query="SELECT * FROM member where status = 1 order by memberName";
    $result=mysql_query($query);

    $num=mysql_numrows($result); 
    $i=0;
    $j=0;
    echo "<FORM NAME ='form1' METHOD ='POST' ACTION = $scriptname>";    
    echo "<table border='0' cellspacing='2' cellpadding='2'>";
    echo "<tr> ";
    while ($i < $num) {  
        $j++;  
        $memberName=mysql_result($result,$i,"memberName");
        $member_id=mysql_result($result,$i,"member_id");      
        $guest=mysql_result($result,$i,"guest");  
        if ($guest) {
            $guest="(Guest)";
        } else {
            $guest='';
        }
        echo "<td><p><input type='radio' name='member_' value=$member_id /> $memberName $guest</p></td>";        
        if ($j > 3) {            
            echo"<tr> ";
            $j=0;
        }
        $i++;        
    } 
    echo "</tr>";  
    echo "</table>";
    echo "--------------------------------------<br />";



    echo "Enter a new Member and click 'Submit'.  <br />";
    echo "<INPUT TYPE = 'TEXT' NAME = 'memberName' VALUE =''>";
    echo "<br />";
    echo "<br />";
    echo "To edit an existing member, check the circle";
    echo "<br />";
    echo "next to the gift above and click 'Submit'.";
    echo "<br />";

    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE =memberForm>";  
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>"; 
    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";
    echo "</FORM>";
   
    echo "NOTE: To add qualifying text to a name, add a | symbol (usually a shift \) after the name and then the text such as:<br />";
    echo "John D | piano<br />";
    
    echo "</body>";
    echo "</html>";
    
    ###
    mysql_close();
    StandardFooter();
    exit;    
} elseif ($tool == "associateServiceForm") {
    # the associateForm goes here
    echo "<b><center>Service Association Form</center></b><br><br>";
    # First we need to show the list of members and then the user can select the member.  Finally they can associate services with the member

    # Show the existing members who are active and not marked as guests
    $query="SELECT * FROM member where status = 1 and guest = 0 order by memberName";
    $result=mysql_query($query);

    $num=mysql_numrows($result); 

    # Display a list of members
    echo "<b><center>Pilgrims Worship Members</center></b><br><br>";
    
    echo "<table border='0' cellspacing='2' cellpadding='2'>";
    echo"<tr> ";
    
    echo "<th><font face='Arial, Helvetica, sans-serif'>Member</font></th>";
    echo "</tr>";
    
    $i=0;
    $column_count=0;
    while ($i < $num) {    
        $memberName=mysql_result($result,$i,"memberName");
        $member_id=mysql_result($result,$i,"member_id");

        $column_count++;
        echo "<td><font face='Arial, Helvetica, sans-serif'><A HREF = $scriptname?tool=associateService&member_id=$member_id&grzebtw=$grzebtw>$memberName</A><br /></font></td>";
        if ($column_count > 3) {
            echo"<tr> ";
            $column_count=0;       
        }              
        ++$i;
    } 
    echo "</table>";
    echo "</head>";
    echo "<body>";
    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    
    echo "<br /><br />";
    echo "Select the member you wish to associate services with.<br />";
    echo "</FORM>";

    echo "</body>";
    echo "</html>";
    mysql_close();        
    StandardFooter();
    exit;    
} elseif ($tool == "associateService") {
    # the associateService goes here
    echo "<b><center>Associate Service</center></b><br><br>";
    $member_id = $_GET["member_id"];    
    $existing_associations=array();
    if (!$member_id) {
        echo "Sorry, I seem to have lost track of what member you wanted!<br />";
        mysql_close();        
        StandardFooter();
        exit;    
    }
    # Determine the member's name
    $query="SELECT memberName FROM member where member_id = $member_id";
    $result=mysql_query($query);

    $memberName;
    if ($result) {
        $memberName = mysql_result($result,0);
    }


    # Get the Services currently associated with this member
    $query="SELECT service_id from memberServiceMatch where member_id = $member_id";
    $result=mysql_query($query);
    if ($result) {
        $num=mysql_numrows($result); 
        $i=0;
        while ($i < $num) {  
            $service_id=mysql_result($result,$i,"service_id");
            $existing_associations[]=$service_id;
            ++$i;
        }
    }

    # Show the existing services
    $query="SELECT * FROM service order by serviceName";
    $result=mysql_query($query);
    $num=mysql_numrows($result); 

    echo "<b>Associate Services with Member: $memberName</b><br><br>";
    # Display a list of services
    echo "<font face='Arial, Helvetica, sans-serif'>Services</font>";    
    echo "</head>";
    echo "<body>";
    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    echo "<table border='0' cellspacing='2' cellpadding='2'>";

    $i=0;
    while ($i < $num) {    
        $serviceName=mysql_result($result,$i,"serviceName");
        $service_id=mysql_result($result,$i,"service_id");
        $checked=0;    
        foreach ($existing_associations as $ea) {
            if ($ea == $service_id) {            
                $checked=1;
            }        
        }   
        echo "<tr>";
        if ($checked) {       
            echo "<td><input type='checkbox' name=service_$service_id value='1' checked/> $serviceName <br /></td>";
        } else {        
            echo "<td><input type='checkbox' name=service_$service_id value='1' /> $serviceName <br /></td>";        
        }    
        echo "</tr>";    
        ++$i;
    } 
    echo "</table>";
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='insertServiceAssociation'>";  
    echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =$memberName>";
    echo "<INPUT TYPE = 'hidden' NAME = 'member_id' VALUE =$member_id>";
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";   
    echo "<br />";
    print "Select the service(s) you wish to associate with $memberName.<br />";

    echo "<br />";
    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";

    echo "</FORM>";  
    echo "</body>";
    echo "</html>";
    mysql_close();
    StandardFooter();
    exit;        
} elseif ($tool == "associateForm") {
    # the associateForm goes here
    echo "<b><center>Gift Association Form</center></b><br><br>";
    # First we need to show the list of members and then the user can select the member.  Finally they can associate gifts with the member

    # Show the existing members who are active and not marked as guests
    $query="SELECT * FROM member where status = 1 and guest = 0 order by memberName";
    $result=mysql_query($query);

    $num=mysql_numrows($result); 

    # Display a list of members
    echo "<b><center>Pilgrims Worship Members</center></b><br><br>";
    
    echo "<table border='0' cellspacing='2' cellpadding='2'>";
    echo"<tr> ";
    
    echo "<th><font face='Arial, Helvetica, sans-serif'>Member</font></th>";
    echo "</tr>";
    
    $i=0;
    $column_count=0;
    while ($i < $num) {    
        $memberName=mysql_result($result,$i,"memberName");
        $member_id=mysql_result($result,$i,"member_id");
        
        #echo "<tr>";
        $column_count++;
        echo "<td><font face='Arial, Helvetica, sans-serif'><A HREF = $scriptname?tool=associateGift&member_id=$member_id&grzebtw=$grzebtw>$memberName</A><br /></font></td>";
        if ($column_count > 3) {
            echo"<tr> ";
            $column_count=0;       
        }      
        #echo "</tr>";
        
        ++$i;
    } 
    echo "</table>";
    echo "</head>";
    echo "<body>";
    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    
    echo "<br /><br />";
    echo "Select the member you wish to associate gifts with.<br />";
    echo "</FORM>";

    echo "</body>";
    echo "</html>";
    mysql_close();        
    StandardFooter();
    exit;    
} elseif ($tool == "associateGift") {
    # the associateGift goes here
    echo "<b><center>Associate Gift</center></b><br><br>";
    $member_id = $_GET["member_id"];    
    $existing_associations=array();
    if (!$member_id) {
        echo "Sorry, I seem to have lost track of what member you wanted!<br />";
        mysql_close();        
        StandardFooter();
        exit;    
    }
    # Determine the member's name
    $query="SELECT memberName FROM member where member_id = $member_id";
    $result=mysql_query($query);

    $memberName;
    if ($result) {
        $memberName = mysql_result($result,0);
    }


    # Get the gifts currently associated with this member
    $query="SELECT gift_id from memberGiftMatch where member_id = $member_id";
    $result=mysql_query($query);
    if ($result) {
        $num=mysql_numrows($result); 
        $i=0;
        while ($i < $num) {  
            $gift_id=mysql_result($result,$i,"gift_id");
            $existing_associations[]=$gift_id;
            ++$i;
        }
    }

    # Show the existing gifts
    $query="SELECT * FROM gift order by giftName";
    $result=mysql_query($query);
    $num=mysql_numrows($result); 

    echo "<b>Associate Gifts with Member: $memberName</b><br><br>";
    # Display a list of gifts
    echo "<font face='Arial, Helvetica, sans-serif'>Gifts</font>";    
    echo "</head>";
    echo "<body>";
    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    echo "<table border='0' cellspacing='2' cellpadding='2'>";

    $i=0;
    while ($i < $num) {    
        $giftName=mysql_result($result,$i,"giftName");
        $gift_id=mysql_result($result,$i,"gift_id");
        $checked=0;    
        foreach ($existing_associations as $ea) {
            if ($ea == $gift_id) {            
                $checked=1;
            }        
        }   
        echo "<tr>";
        if ($checked) {       
            echo "<td><input type='checkbox' name=gift_$gift_id value='1' checked/> $giftName <br /></td>";
        } else {        
            echo "<td><input type='checkbox' name=gift_$gift_id value='1' /> $giftName <br /></td>";        
        }    
        echo "</tr>";    
        ++$i;
    } 
    echo "</table>";
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='insertAssociation'>";  
    echo "<INPUT TYPE = 'hidden' NAME = 'memberName' VALUE =$memberName>";
    echo "<INPUT TYPE = 'hidden' NAME = 'member_id' VALUE =$member_id>";
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";   
    echo "<br />";
    print "Select the gift(s) you wish to associate with $memberName.<br />";

    echo "<br />";
    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";

    echo "</FORM>";  
    echo "</body>";
    echo "</html>";
    mysql_close();
    StandardFooter();
    exit;    
} elseif ($tool == "insertServiceAssociation") {
    # the insertAssociation goes here
    echo "<b><center>Insert Association</center></b><br><br>";
    $services=array();
    # Get the member these services are to be associated with
    $memberName='';
    if (isset($_POST['memberName'])) {
        $memberName = $_POST['memberName'];
    }
    $member_id=0;
    if (isset($_POST['member_id'])) {
        $member_id = $_POST['member_id'];
    }    

    if ($_POST) {
        foreach ($_POST as $var => $key) {
            # Look for variable names starting as "service_" and the number attached after the "_" is the service id                
            if (preg_match("/service_/",$var)) {        
                $g=substr($var,8);
                # Build an array of service id's
                $services[]=$g;
            }       
        }
    }
    if ((!$memberName) || (!$member_id)) {
        echo "Sorry, I seem to have lost track of what member we were working with!<br />";
        mysql_close();        
        StandardFooter();
        exit;          
    }
    $query="delete FROM memberServiceMatch where member_id = '$member_id'";
    $result=mysql_query($query);
    $my_error=mysql_error();     
    if ($my_error) {                
        print "Error: Failed to add clear old service associations for $memberName <br /><br />";  
    }     
    # Insert the associations
    $association_count=0;
    $error_count=0;

    foreach ($services as $g) {

        $query = "INSERT INTO memberServiceMatch VALUES ('$member_id','$g')";      
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            $association_count++;        
        } else {
            $error_count++;
        }            

    }

    echo "Added $association_count service associations for $memberName - $error_count errors<br />";
    echo "<br />";    
    echo "<A HREF = $scriptname?tool=associateServiceForm&grzebtw=$grzebtw>Add Services for another person.</A><br />";

    mysql_close();    
    StandardFooter();
    exit;       
} elseif ($tool == "insertAssociation") {
    # the insertAssociation goes here
    echo "<b><center>Insert Association</center></b><br><br>";
    $gifts=array();
    # Get the member these gifts are to be associated with
    $memberName='';
    if (isset($_POST['memberName'])) {
        $memberName = $_POST['memberName'];
    }
    $member_id=0;
    if (isset($_POST['member_id'])) {
        $member_id = $_POST['member_id'];
    }    

    if ($_POST) {
        foreach ($_POST as $var => $key) {
            # Look for variable names starting as "gift_" and the number attached after the "_" is the gift id                
            if (preg_match("/gift_/",$var)) {        
                $g=substr($var,5);
                # Build an array of gift id's
                $gifts[]=$g;
            }       
        }
    }
    if ((!$memberName) || (!$member_id)) {
        echo "Sorry, I seem to have lost track of what member we were working with!<br />";
        mysql_close();        
        StandardFooter();
        exit;          
    }
    $query="delete FROM memberGiftMatch where member_id = '$member_id'";
    $result=mysql_query($query);
    $my_error=mysql_error();     
    if ($my_error) {                
        print "Error: Failed to add clear old gift associations for $memberName <br /><br />";  
    }     
    # Insert the associations
    $association_count=0;
    $error_count=0;

    foreach ($gifts as $g) {

        $query = "INSERT INTO memberGiftMatch VALUES ('$member_id','$g')";        
        mysql_query($query);
        $num=mysql_affected_rows();
        if ($num) {
            $association_count++;        
        } else {
            $error_count++;
        }            

    }

    echo "Added $association_count gift associations for $memberName - $error_count errors<br />";
    echo "<br />";
    echo "<A HREF = $scriptname?tool=associateForm&grzebtw=$grzebtw>Add Gifts for another person.</A><br />";

    mysql_close();    
    StandardFooter();
    exit;   
} elseif ($tool == "associateFormGift") {
    # the associateFormGift goes here    
    
    ###
    # Show the existing gifts
    $query="SELECT * FROM gift order by giftName";
    $result=mysql_query($query);

    $num=mysql_numrows($result); 

    # Display a list of gifts
    echo "<b><center>Pilgrims Worship Gifts</center></b><br><br>";
    
    echo "<table border='0' cellspacing='2' cellpadding='2'>";
    echo "<tr>";
    
    echo "<th><font face='Arial, Helvetica, sans-serif'>Gift</font></th>";
    echo "</tr>";
    
    $i=0;
    while ($i < $num) {    
        $giftName=mysql_result($result,$i,"giftName");
        $gift_id=mysql_result($result,$i,"gift_id");
        echo "<tr>";
        echo "<td><font face='Arial, Helvetica, sans-serif'><A HREF = $scriptname?tool=associateMember&gift_id=$gift_id&grzebtw=$grzebtw>$giftName</A><br /></font></td>";
        echo "</tr>";    

        ++$i;
    } 
    echo "</table>";

    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";    
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='insertGift'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
    echo "<br /><br />";
    echo "Select the gift you wish to associate members with.";
    echo "</FORM>";    
    echo "</body>";
    echo "</html>";
 
    ###
} elseif ($tool == "associateMember") {
    # the associateMember goes here       

    ###
    $existing_associations=array();
    $gift_id = $_GET["gift_id"];

    # Determine the gift's name
    $query="SELECT giftName FROM gift where gift_id = $gift_id";
    $result=mysql_query($query);
    $giftName=mysql_result($result,0);

    # Get the members currently associated with this gift
    $query="SELECT member_id from memberGiftMatch where gift_id = $gift_id";
    $result=mysql_query($query);
    $num=mysql_numrows($result); 
    $i=0;
    while ($i < $num) {  
        $member_id=mysql_result($result,$i,"member_id");
        $existing_associations[]=$member_id;
        ++$i;
    }


    # Show the existing members who are active and not marked as guests
    $query="SELECT * FROM member where status = 1 and guest = 0 order by memberName";
    $result=mysql_query($query);
    $num=mysql_numrows($result); 

    echo "<b>Associate Member with Gift: $giftName</b><br><br>";
    # Display a list of members


    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    echo "<table border='0' cellspacing='2' cellpadding='2'>";
    echo "<tr> ";
    
    echo "<th><font face='Arial, Helvetica, sans-serif'>Members</font></th>";
    echo "</tr>";
    
    echo "<table border='0' cellspacing='2' cellpadding='2'>";
    echo "<tr> ";
    $i=0;
    $column_count=0;
    while ($i < $num) {    
        $memberName=mysql_result($result,$i,"memberName");
        $member_id=mysql_result($result,$i,"member_id");
        $j=0;
        $checked=0;    
        foreach ($existing_associations as $ea) {
            if ($ea == $member_id) {            
                $checked=1;
            }        
        }    
        #echo "<tr>";
        $column_count++;
        if ($checked) {
            echo "<td><input type='checkbox' name=member_$member_id value='1' checked/> $memberName <br /></td>";
        } else {
            echo "<td><input type='checkbox' name=member_$member_id value='1'/> $memberName <br /></td>";
        }    
        if ($column_count > 3) {
            echo"<tr> ";
            $column_count=0;       
        }    
      
        ++$i;
        
    } 
    echo "</tr>";  
    echo "</table>";
    echo "<INPUT TYPE = 'hidden' NAME = 'giftName' VALUE =\"$giftName\">";
    echo "<INPUT TYPE = 'hidden' NAME = 'gift_id' VALUE =$gift_id>";
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='insertAssociationGift'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";  
    echo "<br />";
    print "Select the members(s) you wish to associate with $giftName. <br />";
    echo "<br />";
    echo "<INPUT TYPE = 'Submit' VALUE = 'Submit'>";    
    echo "</FORM>";
    
    ###
} elseif ($tool == "displayWorshipRecord") {
    # the displayWorshipRecord goes here      
    echo "You are here: $tool - but it has not been developed yet.<br />";
} elseif ($tool == "insertAssociationGift") {
    # the insertAssociationGift goes here         
    ###
    $members=array();
    # Get the gift these gifts are to be associated with
    $giftName = $_POST['giftName'];
    $gift_id = $_POST['gift_id'];
    if ($_POST) {
        foreach ($_POST as $var => $key) {
            # Look for variable names starting as "gift_" and the number attached after the "_" is the gift id              
            if (preg_match("/member_/",$var)) {  
                # Get the member name off of the variable
                $m=substr($var,7);           
                # Build an array of member id's
                $members[]=$m;            
            }       
        }

    }

    # Get the current associations for this gift
    $existing_members = array();
    $query="SELECT member_id FROM memberGiftMatch where gift_id = '$gift_id'";
    $result=mysql_query($query);
    $num=mysql_numrows($result); 
    $i=0;
    while ($i < $num) {
        $member_id=mysql_result($result,$i,"member_id");
        $existing_members[] = $member_id;    
        $i++;
    }
    # Insert the associations
    $association_count=0;
    $error_count=0;
    $existing_count=0;
    $delete_count=0;
    foreach ($members as $m) {
        $insert=1;
        foreach ($existing_members as $e) {
            if ($m == $e) {
                $insert=0;
            }
        }
        if ($insert) {
            # This is not an existing gift for this person so we add it
            $query = "INSERT INTO memberGiftMatch VALUES ('$m','$gift_id')";        
            mysql_query($query);
            $num=mysql_affected_rows();
            if ($num) {
                $association_count++;        
            } else {
                $error_count++;
            }            
        } else {
            $existing_count++;    
        }
    }
    # Remove de-selected associations
    foreach ($existing_members as $e) {
        $delete=1;
        foreach ($members as $m) {
            if ($m == $e) {
                $delete=0;
            }
        }
        if ($delete) {
            # This is not an existing gift for this person so we add it
            $query = "DELETE FROM memberGiftMatch where member_id = $e";        
            mysql_query($query);
            $num=mysql_affected_rows();
            if ($num) {
                $delete_count++;        
            } else {
                $error_count++;
            }            
        }     
    }

    echo "Added $association_count member associations for $giftName and removed $delete_count<br />";
    echo "$existing_count were repeats - $error_count errors<br />";

    echo "<br />";
    echo "<A HREF = $scriptname?tool=associateFormGift&grzebtw=$grzebtw>Add Members for another Gift.</A><br />";
    
    ###
} elseif ($tool == "insertGift") {
    # the insertGift goes here         
    echo "You are here: $tool - but it has not been developed yet.<br />";
} elseif ($tool == "insertMember") {
    # the insertMember goes here        
    echo "You are here: $tool - but it has not been developed yet.<br />";
} elseif ($tool == "insertUpdateWorship") {
    # the insertUpdateWorship goes here        
    ###
    $gift_member=array();    
    
    if (isset($_POST['memberName'])) {
        $memberName = $_POST['memberName'];
    }
    if (isset($_POST['member_id'])) {
        $member_id = $_POST['member_id'];
    }    
    if (isset($_POST['replace_data'])) {
        $replace_data = $_POST['replace_data'];
    }
    
    $recordMonth;
    $recordDay;
    $recordYear;
    $recordDate = $_POST['recordDate'];    
    $gift_guest=array();
    $guest;
    $guestGift;
    
    if ($_POST) {
        $counter=0;
        foreach ($_POST as $var => $key) {
           
            if (strcmp($var,'recordYear')==0) {
                $recordYear=$key;
            }
            if (strcmp($var,'recordMonth')==0) {
                $recordMonth=$key;
            }        
            if (strcmp($var,'recordDay')==0) {
                $recordDay=$key;
            }             
            # Look for variable names starting as "gift_" and the number attached after the "_" is the gift id                
            if (preg_match("/cb_/",$var)) {               
                $tmp=explode("_",$var);           
                # Build an array of gift id's   - the $counter dimension allows for more than one person to be associated with the gift.                
                $gift_member[$tmp[1]][$counter]=$tmp[2];  
                $counter++;
            }       
            if (preg_match("/guestGift_/",$var)) {               
                $tmp=explode("_",$var);           
                # Build an array of gift id's   - the $counter dimension allows for more than one person to be associated with the gift.                                   
                $guest=$_POST[$var];
                $guestGift=$tmp[1];
                $gift_guest[$tmp[1]][$counter]=$guest;                  
                $counter++;
            }             
        }
    }

    # Insert the current record after some basic checking    
    if ($recordDate) {        
        # We need to purge any previous data on this date - this allows someone to be de-selected
        $query="delete from record where dateParticipated = '$recordDate%'";
        mysql_query($query);
        $num=mysql_affected_rows();  
        #echo "Removed $num rows of previous data for $recordDate<br />";
        echo "Entering record for $recordDate<br />";
        $update_count=0;
        $error_count=0;
        foreach ($gift_member as $var => $key) {          
            foreach ($key as $k => $v) {    
                # Determine if this member / gift combo is already present for this date
                $query="
                select 
                    member_id,
                    gift_id,
                    dateParticipated 
                from 
                    record 
                where 
                    member_id = $v
                and
                    gift_id = $var
                and
                    dateParticipated = '$recordDate'
                ";
                mysql_query($query);
                $num=mysql_affected_rows();  
                # If this is already present, don't add it again.
                if ($num) {
                    # Check for duplicates
                    if ($num > 1) {                    
                        $limit=($num - 1);
                        $query="delete from record where member_id = $v and gift_id = $var and dateParticipated = '$recordDate' limit $limit";
                        mysql_query($query);
                        $num=mysql_affected_rows();                      
                        echo "Deleted $num duplicate entries<br />";
                    }
                } else {
                    $query="insert into record (member_id,gift_id,dateParticipated,dateEntered) VALUES ($v,$var,'$recordDate',NOW())";                        
                    mysql_query($query);
                    $num=mysql_affected_rows();        
                    if ($num) {
                        $update_count++;        
                    } else {
                        $error_count++;
                    }           
                }
            }
            
            
        }
        foreach ($gift_guest as $guestGift => $key) {          
            foreach ($key as $k => $guest) {   
                if (!$guest) {
                    continue;
                }
                # Determine if this guest / gift combo is already present for this date                
                # The guest entry if there was one.  This is when there is a person fulfilling a gift but is not associated with that gift.
                # Determine if this guest exists.  A guest is a member with the guest attribute set
                $query="select member_id from member where memberName = '$guest'";                
                $result=mysql_query($query);
                $guest_id;
                if ($result) {
                    $row = mysql_fetch_row($result);
                    $guest_id=$row[0];
                }
                
                
                if (!$guest_id) {
                    # Guest does not exist in the guest table so put them in                    
                    $query = "INSERT INTO member (memberName, guest) VALUES ('$guest','1')";                      
                    mysql_query($query);                     
                    $num=mysql_affected_rows();                      
                    if ($num == 1) {                        
                        # Now, get the guest id
                        $query="select member_id from member where memberName = '$guest'";                
                        $result=mysql_query($query); 
                        $row = mysql_fetch_row($result);                        
                        $guest_id=$row[0];
                          #  $guest_id=mysql_result($result,0,"member_id");                                
                       
                        
                    } else {
                        echo "Error!  Failed to insert $guest into database table.<br />";
                    }
                }

                if ($guest_id) {                    
                    # Determine if this guest / gift combo is already present for this date
                    $query="
                    select 
                        count(*) as count                        
                    from 
                        record 
                    where 
                        member_id = $guest_id
                    and
                        gift_id = $guestGift
                    and
                        dateParticipated = '$recordDate'
                    ";
                    $result=mysql_query($query);
                    $record_count=mysql_result($result,0,"count");   
                    
                    # If this is already present, don't add it again.
                    if ($record_count) {
                        # Check for duplicates
                        if ($record_count > 1) {                    
                            $limit=($record_count - 1);
                            $query="delete from record 
                            where member_id = $guest_id and gift_id = $guestGift and dateParticipated = '$recordDate' 
                            limit $limit";
                            mysql_query($query);
                            $num=mysql_affected_rows();                      
                            echo "Deleted $num duplicate entries<br />";
                        }
                    } else {
                        $query="insert into record (member_id,gift_id,dateParticipated,dateEntered) 
                        VALUES ($guest_id,$guestGift,'$recordDate',NOW())";                             
                        mysql_query($query);
                        $num=mysql_affected_rows();        
                        if ($num) {
                            $update_count++;        
                        } else {
                            $error_count++;
                        }           
                    }   
                } else {
                    echo "Error: Failed to handle $guest correctly<br />";
                }                
            }                        
        }        

        echo "Completed $update_count record updates with $error_count errors.<br />";

        # Show what we have now
        $query="
        select 
            m.memberName,
            g.giftName,
            r.dateParticipated,
            r.dateEntered        
        from 
            record r
            join member m on r.member_id = m.member_id
            join gift g on r.gift_id = g.gift_id
        where 
            dateParticipated = '$recordDate'";
        $result=mysql_query($query);
        $num=mysql_numrows($result); 
        $i=0;

        if ($num) {
            echo "<br />The following data has been entered for $recordDate<br />";
            echo "---------------------------------------------------------<br />";
            while ($i < $num) {
                $member=mysql_result($result,$i,"memberName");
                $gift=mysql_result($result,$i,"giftName");
                $date=mysql_result($result,$i,"dateEntered");            
                $i++;
                echo "Member: $member Participation: $gift  Date Recorded: $date<br />";
            }            
        }    

        echo "<br /><br /><A HREF = $scriptname?tool=updateWorshipForm&recordDate=$recordDate&grzebtw=$grzebtw>Update More Worship Service Data for $recordDate</A><br />";                    
    }
    
    
    ###
} elseif ($tool == "member") {
    # the member goes here        
    echo "You are here: $tool - but it has not been developed yet.<br />";    

} elseif ($tool == "updateWorshipForm") {
    #echo "You are here: $tool <br />"; 
    # the updateWorshipForm goes here        
    ###
    StandardFooter();
    $recordMonth=0;
    $recordDay=0;
    $recordYear=0;
    $recordDate=0;
    if (isset($_GET['recordDate'])) {
        $recordDate=$_GET['recordDate'];
    }
    

    if ($_POST) {
        foreach ($_POST as $var => $key) {       
            if (strcmp($var,'recordYear')==0) {
                $recordYear=$key;
            }
            if (strcmp($var,'recordMonth')==0) {
                $recordMonth=$key;
            }        
            if (strcmp($var,'recordDay')==0) {
                $recordDay=$key;
            }                
            if (strcmp($var,'recordDate')==0) {
                $recordDate=$key;
            }                
        }
    }
    if (($recordDay) && ($recordMonth) && ($recordYear)) {
        $recordDate=$recordYear."-".$recordMonth."-".$recordDay;  
    }
    if (!$recordDate) {
        # If we don't have the date, get it first. 
        echo "<b><center>Update Worship Service Data</center></b><br><br>";        
        echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='updateWorshipForm'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";                  
        echo "<br />";
        echo "Select the date for the record you wish to enter/update:<br /> ";
        recordDateSelector("record");
        echo "<br />";
        echo "<br />";       
        echo "<br /><INPUT TYPE = 'Submit' VALUE = 'Submit'>";        
        echo "</form>";
    } else {        
        # Get the existing record
        $existing_records=array();
        $query = "
            select 
                member_id,
                gift_id
            from
                record
            where
                dateParticipated = '$recordDate'
        ";        
        ###
        $result=mysql_query($query);
        $num=mysql_numrows($result); 

        $i=0;
        $member_list=array();
        while ($i < $num) {  
            $gift_id=mysql_result($result,$i,"gift_id");            
            $member_id=mysql_result($result,$i,"member_id");            
            if (isset($member_list[$member_id])) {
                $member_list[$member_id]++;
            } else {
               $member_list[$member_id]=1;
            }
            $memberIndex=$member_list[$member_id];
            # In case a member did more than one gift on this day, we need to add another dimension
            $existing_records[$member_id][$memberIndex]=$gift_id;            
            ++$i;
        }    

        ###
        echo "<b><center>Update Worship Service Data</center></b><br><br>";
        echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='insertUpdateWorship'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";         
        echo "<INPUT TYPE = 'hidden' NAME = 'recordDate' VALUE =$recordDate>";              
        echo "For each gift/task, select the person involved for $recordDate.<br />";
        echo "<br />";
        ###

        # Show the existing members
        $query="SELECT gift_id,giftName FROM gift order by giftName";
        $result=mysql_query($query);
        $num=mysql_numrows($result);     
        ###
        $i=0;
        while ($i < $num) {    
            $giftName=mysql_result($result,$i,"giftName");
            $gift_id=mysql_result($result,$i,"gift_id");
         
            echo "<h4>$giftName</h4>";
            # Get the list of people associated with this gift
            $query="SELECT 
                distinct(m.member_id),
                m.memberName 
            FROM 
                member m
                join memberGiftMatch mgm on m.member_id = mgm.member_id
            where
                mgm.gift_id = '$gift_id'
            order by
                m.memberName
            ";

            $memberResult=mysql_query($query); 
            $memberNum=mysql_numrows($memberResult); 
            $j=0;
            $column_count=0;

            echo "<table border='0' cellspacing='2' cellpadding='2'>";     
            echo "<tr> ";
            $member_shown=array();    # We will use this to record which members we have shown for the gift            
            while ($j < $memberNum) {
                $checked=0;
                $memberName=mysql_result($memberResult,$j,"memberName");
                $member_id=mysql_result($memberResult,$j,"member_id");                
                ### Determine if there is an existing record for this gift
                foreach ($existing_records as $existing_member => $var) {                
                    foreach ($var as $existing_gift) {                    
                        if ($existing_gift == $gift_id) {
                            if ($existing_member == $member_id) {
                                $checked=1;
                            }
                        }                    
                    }
                }            
                ###                
                $token="$gift_id"."_"."$member_id";
                $column_count++;               
                if ($column_count > 4) {
                    echo"<tr> ";
                    $column_count=1;
                }
                if ($checked) {
                    echo "<td><p><INPUT TYPE=CHECKBOX NAME=cb_$token checked>$memberName</p></td>";
                } else {
                    echo "<td><p><INPUT TYPE=CHECKBOX NAME=cb_$token>$memberName</p></td>";
                }
                array_push($member_shown,$member_id);                
                $j++;
            }
            # Now, we still need to show anyone who was participating as a "guest"
            ###            
            foreach ($existing_records as $existing_member => $var) {    
                $need_to_show=0;
                foreach ($var as $existing_gift) {                        
                    if ($existing_gift == $gift_id) {                     
                        $need_to_show=1;
                        foreach ($member_shown as $done) {                            
                            if ($existing_member == $done) {
                                $need_to_show=0;
                            }
                        }
                    }                    
                }
                $column_count++;
                if ($column_count > 4) {
                    echo"<tr> ";
                    $column_count=1;
                }                
                if ($need_to_show) {
                    
                    # Need to show this guest but first, we need to get their name
                    $query="SELECT memberName FROM member WHERE member_id = '$existing_member'";
                    $result1=mysql_query($query);
                    $memberName=mysql_result($result1,0,"memberName");
                    $token="$gift_id"."_"."$existing_member";
                    echo "<td><p><INPUT TYPE=CHECKBOX NAME=cb_$token checked>$memberName</p></td>";
                }
            }             
            
            ###
            ++$i;        
            echo "<tr><td>Guest:</tr></td>";                        
            echo "<tr><td><INPUT TYPE = 'TEXT' NAME = guestGift_$gift_id VALUE =''></tr></td><br />";
            echo "</tr>";  
            echo "</table>";
        } 
        echo "<br />==============================================================<br />";
        echo "<br /><INPUT TYPE = 'Submit' VALUE = 'Submit'>";        
        echo "</form>";
    }    
    echo "<br />";
    echo "<br />";    
    ###
} elseif ($tool == "worshipForm") {
    # the worshipForm goes here        
    echo "You are here: $tool - but it has not been developed yet.<br />";    
    
} elseif ($tool == "queryWorshipForm") {
    # the queryWorshipForm goes here        
    ###
 # Display a list of members
    echo "<b><center>Query Worship Service Data</center></b><br><br>";

    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='queryWorship'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";          
    echo "<br />";

    echo "Query by Date:<br /><br />";
    echo "Select the date to view:<br /> ";
    DateSelector("date");
    echo "<br /><INPUT TYPE = 'Submit' VALUE = 'Submit'>";
    
    echo "</form>";

    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='queryWorship'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>";      
    echo "<br />";

    echo "Query by Gift:<br /><br />";
    echo "Select the gift to view:<br /> ";
    GiftSelector("gift");
    echo "<br /><INPUT TYPE = 'Submit' VALUE = 'Submit'>";
    echo "</form>";

    echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
    echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='queryWorship'>";                 
    echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>"; 
    echo "<br />";

    echo "Query by Person:<br /><br />";
    echo "Select the person to view:<br /> ";
    MemberSelector("member");
    echo "<br /><INPUT TYPE = 'Submit' VALUE = 'Submit'>";
    
    echo "</form>";              
    ###    
} elseif ($tool == "queryWorship") {
    # the queryWorship goes here        
    ###
    $limit=20;
    $selectionType;
    $selectionLabel;
    $selection;
    $query;
    $where;
    $orderby;
    $label1;
    $label2;
    $key1;
    $key2;
    $value;
    $show_extra_report=0;
    $selected_gift='';

    # Get the parameters
    if ($_POST) {
        foreach ($_POST as $var => $key) {          
            if (strcmp($var,'dateselection') == 0) {
                # If we are querying by date, we want to show gifts and members for that date.
                $label1="Gift";
                $label2="Member";
                $key1="giftName";
                $key2="memberName";
                $selectionType="r.dateParticipated";
                $selectionLabel="Date Participated";
                $selection=$key;
                $query="select                 
                    g.giftName,
                    m.memberName,
                    r.dateParticipated
                from
                    record r
                    join gift g on r.gift_id = g.gift_id
                    join member m on r.member_id = m.member_id
                where
                    $selectionType like '$selection'
                order by
                    g.giftName
                limit $limit"; 
                                    
            }
            if (strcmp($var,'giftselection') == 0) {
                # If we are querying by gift, we want to show member and date for that gift.
                
                if (!get_magic_quotes_gpc()) {
                    $key=addslashes($key);
                }
                
                # Get the gift id
                $query = "select gift_id from gift where giftName like '$key'";
                $result=mysql_query($query); 
                $num=mysql_numrows($result); 
                if ($num) {
                    $selected_gift=mysql_result($result,($num-1),"gift_id"); 
                }
                
                $show_extra_report=1;                              
                $label1="Member";
                $label2="Date";            
                $key1="memberName";
                $key2="dateParticipated";
                $selectionType="g.giftName";
                $selectionLabel="Gift";
                $selection=$key;
                $query="select                 
                    g.giftName,
                    m.memberName,
                    r.dateParticipated,
                    g.gift_id
                from
                    record r
                    join gift g on r.gift_id = g.gift_id
                    join member m on r.member_id = m.member_id
                where
                    $selectionType like '$selection'
                order by
                    r.dateParticipated desc
                limit $limit"; 
            }  
            if (strcmp($var,'memberselection') == 0) {
                # If we are querying by member, we want to show Gift and date for that member.
                $label1="Gift";
                $label2="Date";  
                $key1="giftName";
                $key2="dateParticipated";
                $selectionType="m.memberName";
                $selectionLabel="Member";
                $selection=$key;
                $query="select                 
                    g.giftName,
                    m.memberName,
                    r.dateParticipated
                from
                    record r
                    join gift g on r.gift_id = g.gift_id
                    join member m on r.member_id = m.member_id
                where
                    ($selectionType like '$selection'
                    or
                    $selectionType like '$selection|%'
                    or
                    $selectionType like '$selection |%')
                order by
                    r.dateParticipated desc
                limit $limit";               
            }            
        }
        
    }

    # Display a list of members
    echo "<b><center>Query Worship Service Data Result</center></b><br><br>";
    StandardFooter();
    $result=mysql_query($query); 
    $num=0;
    if ($result) {
        $num=mysql_numrows($result); 
        #$num=mysql_affected_rows();
    }
    # Strip slashes
    $selection=stripslashes($selection);
    $j=0;       
    echo "<b>$selectionLabel: $selection</b><br /><br />";
    # Create a table here:
    echo "<table border='1' cellspacing='1' cellpadding='1'>";     
    echo "<tr> ";  
    echo "<tr>";
    echo "<th>$label1</th>";
    echo "<th>$label2</th>";        
    echo "</tr>";    
    while ($j < $num) {
        $value1=mysql_result($result,$j,"$key1"); 
        $value2=mysql_result($result,$j,"$key2"); 
        #if ($show_extra_report) {
        #    $selected_gift=mysql_result($result,$j,"gift_id");      
        #}
        echo "<tr>";
        echo "<td>$value1</td>";
        echo "<td>$value2</td>";        
        echo "</tr>";
        $j++;
    }      
    echo "</table>";
    echo "<br /><br />";
    
    if ($show_extra_report) {        
        echo "<FORM NAME ='form2' METHOD ='POST' ACTION = $scriptname>";
        echo "<INPUT TYPE = 'hidden' NAME = 'tool' VALUE ='queryWorshipMembers'>";                 
        echo "<INPUT TYPE = 'hidden' NAME = 'grzebtw' VALUE =$grzebtw>"; 
        echo "<INPUT TYPE = 'hidden' NAME = 'gift_id' VALUE =$selected_gift>"; 
        echo "<INPUT TYPE = 'hidden' NAME = 'giftName' VALUE =\"$selection\">"; 
        #echo "<br />";        
        #echo "Show all Members associated with this gift<br /> ";        
        echo "<br /><INPUT TYPE = 'Submit' VALUE = 'Show All Members Associated with this Gift'>";        
        echo "</form>";           
    }
    echo "<A HREF = $scriptname?tool=queryWorshipForm&grzebtw=$grzebtw>Query Worship Service Data Again</A><br />";   
    ### 
} elseif ($tool == "queryWorshipMembers") {
    # the queryWorshipMembers goes here        
    ###
    $gift_id=$_POST['gift_id'];
    $giftName=$_POST['giftName'];
    # Get all of the members associated with this gift and find the last time they shared this gift
    echo "<b><center>Query Pilgrim Worship Record Result</center></b><br />";  
    echo "<h4>All members associated with gift $giftName and the last time they shared that gift.</h4>";

    # Get the list of people associated with this gift
    $member_list=array();
    $seconds_list=array();
    $query="SELECT 
        m.memberName,
        m.member_id       
    FROM 
        member m
        join memberGiftMatch mgm on m.member_id = mgm.member_id
    where
        mgm.gift_id = '$gift_id'    
    order by
        m.memberName
    ";
    ###
    $memberResult=mysql_query($query); 
    $memberNum=mysql_numrows($memberResult);     
   
    $j=0;
    echo "<table border='1' cellspacing='1' cellpadding='1'>";     
    echo "<tr> ";  
    echo "<tr>";
    echo "<th>Member</th>";
    echo "<th>Last Participated</th>"; 
    while ($j < $memberNum) {
        $memberName=mysql_result($memberResult,$j,"memberName");
        $member_id=mysql_result($memberResult,$j,"member_id");  
        $j++;        
        # Get the information about this user and the last time he shared this gift
        $recordquery="select                 
            max(dateParticipated) as dateParticipated
        from
            record 
        where
            gift_id = $gift_id
        and
            member_id = $member_id
        ";
        
        $result=mysql_query($recordquery); 
        $num=mysql_numrows($result); 
        $dateParticipated=mysql_result($result,0,"dateParticipated");  
        if (!$dateParticipated) {
            $dateParticipated = "-------------";
        } 

        $k=0;        
        ###
        while ($k < $num) {   
            $seconds=0;
            $dateParticipated=mysql_result($result,$k,"dateParticipated");  
            if (!$dateParticipated) {
                $dateParticipated = "-------------";                
            }  else {
                # Converting date to seconds so that I can order the results by date
                $seconds=strtotime($dateParticipated);
            }            
            while (isset($seconds_list[$seconds])) {
                $seconds++;
            }
            $seconds_list[$seconds]=1;
            $values=array("$memberName","$dateParticipated");
            $member_list[$seconds]=$values;
            $k++;
        }   
        ###
    }      
    # Sorting here puts them in order by date
    ksort($member_list);
    # Now, reverse the order so the most recent is on the top
    $final_list=array_reverse($member_list);
    foreach ($final_list as $key => $value) {
        echo "<tr>";
        foreach ($value as $v) {
            echo "<td>$v</td>";
        }
        echo "</tr>";                
    }
    echo "</table>";
    echo "<br /><br />";     
        
    echo "<A HREF = $scriptname?tool=queryWorshipForm&grzebtw=$grzebtw>Query Worship Service Data Again</A><br />";     
    ###            
    
} elseif ($tool == "queryWorshipMembers_orig") {
    # the queryWorshipMembers goes here        
    ###
    $gift_id=$_POST['gift_id'];
    $giftName=$_POST['giftName'];
    # Get all of the members associated with this gift and find the last time they shared this gift
    echo "<b><center>Query Pilgrim Worship Record Result</center></b><br />";  
    echo "<h4>All members associated with gift $giftName and the last time they shared that gift.</h4>";
    # Get the list of people associated with this gift
    $member_list=array();
    $query="SELECT 
        m.memberName,
        m.member_id       
    FROM 
        member m
        join memberGiftMatch mgm on m.member_id = mgm.member_id
    where
        mgm.gift_id = '$gift_id'    
    order by
        m.memberName
    ";
    ###
    $memberResult=mysql_query($query); 
    $memberNum=mysql_numrows($memberResult);     
    $j=0;
    echo "<table border='1' cellspacing='1' cellpadding='1'>";     
    echo "<tr> ";  
    echo "<tr>";
    echo "<th>Member</th>";
    echo "<th>Last Participated</th>"; 
    while ($j < $memberNum) {
        $memberName=mysql_result($memberResult,$j,"memberName");
        $member_id=mysql_result($memberResult,$j,"member_id");  
        $member_list[$member_id]=$memberName;
        $j++;
        
        # Get the information about this user and the last time he shared this gift
        $recordquery="select                 
            max(dateParticipated) as dateParticipated
        from
            record 
        where
            gift_id = $gift_id
        and
            member_id = $member_id
        ";
        
        $result=mysql_query($recordquery); 
        $num=mysql_numrows($result); 
        $dateParticipated=mysql_result($result,0,"dateParticipated");  
        if (!$dateParticipated) {
            $dateParticipated = "-------------";
        } 
        /**
        # Get the count of how many times the member has participated in the past 
        $recordquery="select                 
            count(dateParticipated) as count
        from
            record 
        where
            gift_id = $gift_id
        and
            member_id = $member_id
        ";
        
        $result=mysql_query($recordquery); 
        $num=mysql_numrows($result); 
        $count=mysql_result($result,0,"count");  
        if (!$dateParticipated) {
            $count = "-------------";
        }           
        */
        $k=0;
        ###
         
        while ($k < $num) {            
            $dateParticipated=mysql_result($result,$k,"dateParticipated");  
            if (!$dateParticipated) {
                $dateParticipated = "-------------";
            }                    
            echo "<tr>";
            echo "<td>$memberName</td>";
            echo "<td>$dateParticipated</td>";        
            echo "</tr>";
            $k++;
        }   
        ###
    }      
    
    echo "</table>";
    echo "<br /><br />";     
        
    echo "<A HREF = $scriptname?tool=queryWorshipForm&grzebtw=$grzebtw>Query Worship Service Data Again</A><br />";     
    ###            
} elseif ($tool == "help") {
    # the help goes here            
    #echo "You are here: $tool - but it has not been developed yet.<br />";   
    echo "<b><center>Pilgrim Worship Tool Help</center></b><br><br>";    
    if ($grzebtw) {
        # Show the help for the restricted area
        echo "<b>Gifts</b><br />"; 
        echo "<p>Here is where gifts are entered or edited.</p>";
        echo "<b>Members</b><br />"; 
        echo "<p>Here is where members are entered or edited.  A member may need to be entered more than one time.  If a member
        included some qualifying text related to a gift in the gift survey, a separate entry can be made for that utilizing the 
        vertical bar '|' or 'pipe' symbol.  An example may be where John D has checked several gifts on the gift survey and when
        he checked that he was willing to play music, he listed the piano as the intrument.  In this case, John D would be entered
        first as 'John D' and again as 'John D | piano'.</p>"; 
        echo "<b>Associate a Member and a Gift</b><br />"; 
        echo "<p>Here is where you can associate a member with a gift.  
        If a member included some qualifying text related to a gift in the gift survey, a separate entry can be made for that utilizing the 
        vertical bar '|' or 'pipe' symbol.  An example may be where John D has checked several gifts on the gift survey and when
        he checked that he was willing to play music, he listed the piano as the intrument.  In this case, John D would be entered
        first as 'John D' and again as 'John D | piano'.  It is important to correctly associate 'John D | piano' with the gift of
        playing music.</p>";         
        echo "<b>Associate a Gift and a Member</b><br />"; 
        echo "<p>Here is where you can associate a gift with a member.  If a member included some qualifying text related to a gift 
        in the gift survey, a separate entry can be made for that utilizing the 
        vertical bar '|' or 'pipe' symbol.  An example may be where John D has checked several gifts on the gift survey and when
        he checked that he was willing to play music, he listed the piano as the intrument.  In this case, John D would be entered
        first as 'John D' and again as 'John D | piano'.  It is important to correctly associate 'John D | piano' with the gift of
        playing music.</p>";         
        
    }
    echo "<b>Gifts List</b><br />";     
    echo "<p>This link allows you to see each 'Gift' and who has agreed to participate in worship by sharing this gift.  Here is 
    where worship leaders can find people to fill the various roles of a worship service.
    
    </p>";
    echo "<b>Update Worship Service Data</b><br />"; 
    echo "<p>This link is where you can record who did what for a specific worship service. Select the date of the worship service 
    and then click 'submit'. <b>Note</b> that the date defaults to the current date so if the data being entered is for a 
    worship service that occurred in the past, the correct date must be selected.<br /><br /> 

    After the date is selected, an entry form is created. Each gift lists the members who have agreed to contribute 
    these gifts with a check box before their name. Click on the box in front of the name to indicate their participation. 
    If the person is not listed, enter their name in the 'guest' box at the end of each gift. (<b>Contact Sue Glick</b> 
    if this person should be added to the list.)</p>";       
    
    echo "<b>Query Worship Service Data</b><br />"; 
    echo "<p>This tool provides a way to see who did what for a specific worship service. You can query by date, gift, or person.<br /><br />

    To query by date, click on the drop-down list showing all of the worship services for which there is data. 
    Select the desired date, click on Submit and the result will show all of the people who participated during 
    that service and what gift they contributed.<br /><br /> 

    Similarly, to query by gift, select one of the gifts from the drop-down list and all of the members who have shared 
    the selected gift in the recent past will be displayed. <br /><br />

    And to query by person, select a person from the drop-down list and the gifts that person has shared in the recent 
    past will be displayed.<br /><br /></p>";           
     
} elseif ($tool == "serviceList") {
    # the help goes here            
    #echo "You are here: $tool <br />";   

    # Show the services and the associated members
    ###
    echo "<b><center>Pilgrims Serving</center></b><br><br>";
    echo "<br />";
    ###

    # Show the existing members
    $query="SELECT service_id,serviceName FROM service order by serviceName";
    $result=mysql_query($query);
    $num=mysql_numrows($result);     
    ###
    $i=0;
    while ($i < $num) {    
        $serviceName=mysql_result($result,$i,"serviceName");
        $service_id=mysql_result($result,$i,"service_id");     
        echo "<h4>$serviceName</h4>";
        # Get the list of people associated with this service
        $query="SELECT 
            distinct(m.member_id),
            m.memberName 
        FROM 
            member m
            join memberServiceMatch mgm on m.member_id = mgm.member_id
        where
            mgm.service_id = '$service_id'
        order by
            m.memberName
        ";

        $memberResult=mysql_query($query); 
        $memberNum=mysql_numrows($memberResult); 
        $j=0;
        $column_count=0;

        echo "<table border='1' cellspacing='1' cellpadding='1'>";     
        echo "<tr> ";
        $member_shown=array();    # We will use this to record which members we have shown for the service            
        while ($j < $memberNum) {
            $memberName=mysql_result($memberResult,$j,"memberName");
            $member_id=mysql_result($memberResult,$j,"member_id");                             
            $column_count++;
            if ($column_count > 4) {
                echo"<tr> ";
                $column_count=1;
            }            
            echo "<td> $memberName</td>";
            $j++;
        }                    
        ###
        ++$i;        

        echo "</table>";
    } 
    echo "<br />==============================================================<br />";
    
    echo "</form>";
  
    ###    
    StandardFooter();
    exit;        
} elseif ($tool == "giftsList") {
    # the help goes here            
    #echo "You are here: $tool <br />";   

    # Show the gifts and the associated members
    ###
    echo "<b><center>Gifts List</center></b><br><br>";
    echo "<br />";
    ###
    StandardFooter();
    # Show the existing members
    $query="SELECT gift_id,giftName FROM gift order by giftName";
    $result=mysql_query($query);
    $num=mysql_numrows($result);     
    ###
    $i=0;
    while ($i < $num) {    
        $giftName=mysql_result($result,$i,"giftName");
        $gift_id=mysql_result($result,$i,"gift_id");     
        echo "<h4>$giftName</h4>";
        # Get the list of people associated with this gift
        $query="SELECT 
            distinct(m.member_id),
            m.memberName 
        FROM 
            member m
            join memberGiftMatch mgm on m.member_id = mgm.member_id
        where
            mgm.gift_id = '$gift_id'
        order by
            m.memberName
        ";

        $memberResult=mysql_query($query); 
        $memberNum=mysql_numrows($memberResult); 
        $j=0;
        $column_count=0;

        echo "<table border='1' cellspacing='1' cellpadding='1'>";     
        echo "<tr> ";
        $member_shown=array();    # We will use this to record which members we have shown for the gift            
        while ($j < $memberNum) {
            $memberName=mysql_result($memberResult,$j,"memberName");
            $member_id=mysql_result($memberResult,$j,"member_id");                             
            $column_count++;
            if ($column_count > 4) {
                echo"<tr> ";
                $column_count=1;
            }            
            echo "<td> $memberName</td>";
            $j++;
        }                    
        ###
        ++$i;        

        echo "</table>";
    } 
    echo "<br />==============================================================<br />";
    
    echo "</form>";
  
    ###    
    StandardFooter();
    exit;    
} else  {
    # the unrecognized tool goes here
    print "Sorry, $tool is not currently supported<br />";
    StandardFooter();
    exit;
}
mysql_close();    
StandardFooter();
echo "</body>";
echo "</html>";
exit;  

############
# Functions
############

function StandardFooter () {
    global $scriptname;
    global $grzebtw;
    global $homesite;
    echo "<br />";        
    echo "<A HREF = $scriptname?grzebtw=$grzebtw>Home</A><br />";
    echo "<A HREF = http://$homesite>Main Pilgrim Website</A><br />";      
}

function recordDateSelector($inName, $useDate=0) { 

    /* create array so we can name months */ 
    $monthName = array(1=> "January", "February", "March", 
        "April", "May", "June", "July", "August", 
        "September", "October", "November", "December");  
    /* if date invalid or not supplied, use current time */ 
    if($useDate == 0) 
    { 
        $useDate = Time(); 
    } 

    /* make month selector */ 
    echo "<SELECT NAME=" . $inName . "Month>\n"; 
    for($currentMonth = 1; $currentMonth <= 12; $currentMonth++) 
    { 
        echo "<OPTION VALUE=\""; 
        echo intval($currentMonth); 
        echo "\""; 

        if(intval(date( "m", $useDate))==$currentMonth) 
        { 
            echo " SELECTED"; 
        } 
        echo ">" . $monthName[$currentMonth] . "\n"; 
    } 
    echo "</SELECT>"; 

    /* make day selector */ 
    echo "<SELECT NAME=" . $inName . "Day>\n"; 
    for($currentDay=1; $currentDay <= 31; $currentDay++) 
    { 
        echo "<OPTION VALUE=\"$currentDay\""; 
        if(intval(date( "d", $useDate))==$currentDay) 
        { 
            echo " SELECTED"; 
        } 
        echo ">$currentDay\n"; 
    } 
    echo "</SELECT>"; 
    
    /* make year selector */ 
    echo "<SELECT NAME=" . $inName . "Year>\n"; 
    $startYear = date( "Y", $useDate); 
    for($currentYear = $startYear - 5; $currentYear <= $startYear;$currentYear++) 
    { 
        echo "<OPTION VALUE=\"$currentYear\""; 
        if(date( "Y", $useDate)==$currentYear) 
        { 
            echo " SELECTED"; 
        } 
        echo ">$currentYear\n"; 
    } 
    echo "</SELECT>"; 

}

function DateSelector($inName) { 
    # Get a list of valid dates from the records table
    $query="SELECT 
        distinct(dateParticipated)        
    FROM 
        record        
    order by
        dateParticipated desc
    ";
    
    $result=mysql_query($query); 
    $num=mysql_numrows($result); 
    $j=0;
    echo "<SELECT NAME=" . $inName . "selection>\n";     
    while ($j < $num) {
        $date=mysql_result($result,$j,"dateParticipated");
        echo "<OPTION VALUE=\""; 
        echo "$date"; 
        echo "\""; 
        echo ">" . $date. "\n"; 
        $j++;
    }    
    echo "</SELECT>";      

} 

function GiftSelector($inName) { 
    # Get a list of valid dates from the records table
    $query="SELECT 
        giftName       
    FROM 
        gift        
    order by
        giftName 
    ";
    
    $result=mysql_query($query); 
    $num=mysql_numrows($result); 
    $j=0;
    echo "<SELECT NAME=" . $inName . "selection>\n";     
    while ($j < $num) {
        $gift=mysql_result($result,$j,"giftName");        
        echo "<OPTION VALUE=\""; 
        echo "$gift"; 
        echo "\""; 
        echo ">" . $gift. "\n"; 
        $j++;
    }    
    echo "</SELECT>";      

} 
function MemberSelector($inName) { 
    # Get a list of valid dates from the records table
    $query="SELECT 
        memberName       
    FROM 
        member    
    where
        status = 1
    order by
        memberName 
    ";
    
    $result=mysql_query($query); 
    $num=mysql_numrows($result); 
    $j=0;
    # Names may be in here more than once but if they are, they should have a | symbol separating the name from
    # the second part of the entry which is used as an attribute of some sort.  To put all of the names together,
    # we will use a hash to collect just the basic names
    $member_hash=array();
    while ($j < $num) {
        $member=mysql_result($result,$j,"memberName");        
        if (preg_match("/\|/",$member)) {
            $basic_member=preg_split("/\|/", $member);
            $member=preg_replace("/ $/", "", $basic_member[0]);
        }  
        $member_hash["$member"]=1;
       
        $j++;
    }    
    echo "<SELECT NAME=" . $inName . "selection>\n";         
    foreach ($member_hash as $member => $value) {
   
        echo "<OPTION VALUE=\""; 
        echo "$member"; 
        echo "\""; 
        echo ">" . $member. "\n";         
    }
    echo "</SELECT>";  
} 

?>

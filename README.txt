These folders illustrate some of my coding experience.

The Delphi folder is my example of Object Pascal which I studied in 2017. 

When I worked at Intel as a Software Validation Engineer, my work was with Unix and Linux.
I created many tools and utilities using shell scripting.  The Intel folder holds several examples.

I wrote a simple PHP tool that my church used for a while.  This is in the Pilgrims folder.

At Ten Thousand Villages, I worked mostly in Perl though did some PHP.  There are four sub-folders
here representing the four main areas where I did coding.


2017-12-20
Kevin Glick
